﻿SET PLATFORM_PATH=w:\AWG2M\PRJ\VITIS\AWG2M\PLATFORM

copy /Y xadapter.c %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\libsrc\lwip211_v1_1\src\contrib\ports\xilinx\netif\
copy /Y xaxiemacif_dma.c %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\libsrc\lwip211_v1_1\src\contrib\ports\xilinx\netif\
copy /Y xaxiemacif_hw.c %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\libsrc\lwip211_v1_1\src\contrib\ports\xilinx\netif\
copy /Y xaxiemacif_physpeed.c %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\libsrc\lwip211_v1_1\src\contrib\ports\xilinx\netif\
copy /Y etharp.c %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\libsrc\lwip211_v1_1\src\lwip-2.1.1\src\core\ipv4\
copy /Y opt.h %PLATFORM_PATH%\export\PLATFORM\sw\PLATFORM\standalone_domain\bspinclude\include\lwip\
copy /Y opt.h %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\include\lwip\
copy /Y opt.h %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\libsrc\lwip211_v1_1\src\lwip-2.1.1\src\include\lwip\
copy /Y xil_cache.h %PLATFORM_PATH%\export\PLATFORM\sw\PLATFORM\fxps_cpu_cmd_micro_blaze\bspinclude\include\
copy /Y xil_cache.h %PLATFORM_PATH%\export\PLATFORM\sw\PLATFORM\standalone_domain\bspinclude\include\
copy /Y xil_cache.h %PLATFORM_PATH%\fxps_cpu_cmd_micro_blaze\fxps_cpu_cmd_micro_blaze\bsp\fxps_cpu_cmd_micro_blaze\include\
copy /Y xil_cache.h %PLATFORM_PATH%\fxps_cpu_cmd_micro_blaze\fxps_cpu_cmd_micro_blaze\bsp\fxps_cpu_cmd_micro_blaze\libsrc\standalone_v7_1\src\
copy /Y xil_cache.h %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\include\
copy /Y xil_cache.h %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\libsrc\standalone_v7_1\src\

copy /Y etharp.h %PLATFORM_PATH%\export\PLATFORM\sw\PLATFORM\standalone_domain\bspinclude\include\lwip\
copy /Y etharp.h %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\include\lwip\
copy /Y etharp.h %PLATFORM_PATH%\fxps_cpu_io_micro_blaze\standalone_domain\bsp\fxps_cpu_io_micro_blaze\libsrc\lwip211_v1_1\src\lwip-2.1.1\src\include\lwip\

