﻿
#ifndef XIL_CACHE_H
#define XIL_CACHE_H

#if defined XENV_VXWORKS
/* VxWorks environment */
#error "Unknown processor / architecture. Must be PPC for VxWorks."
#else
/* standalone environment */

#include "mb_interface.h"
#include "xil_types.h"
#include "xparameters.h"

#ifdef __cplusplus
extern "C" {
#endif

#define Xil_L1DCacheInvalidate() 
#define Xil_L2CacheInvalidate() 
#define Xil_L1DCacheInvalidateRange(Addr, Len) 
#define Xil_L2CacheInvalidateRange(Addr, Len) 
#define Xil_L1DCacheFlushRange(Addr, Len) 
#define Xil_L2CacheFlushRange(Addr, Len)
#define Xil_L1DCacheFlush() 
#define Xil_L2CacheFlush()
#define Xil_L1ICacheInvalidateRange(Addr, Len) 
#define Xil_L1ICacheInvalidate() 
#define Xil_L1DCacheEnable() microblaze_enable_dcache()
#define Xil_L1DCacheDisable() microblaze_disable_dcache()
#define Xil_L1ICacheEnable() microblaze_enable_icache()
#define Xil_L1ICacheDisable() microblaze_disable_icache()
#define Xil_DCacheEnable() Xil_L1DCacheEnable()
#define Xil_ICacheEnable() Xil_L1ICacheEnable()
#define Xil_DCacheInvalidate() 
#define Xil_DCacheInvalidateRange(Addr, Len) 
#define Xil_DCacheFlush() 
#define Xil_DCacheFlushRange(Addr, Len)
#define Xil_ICacheInvalidate()
#define Xil_ICacheInvalidateRange(Addr, Len) 
void Xil_DCacheDisable(void);
void Xil_ICacheDisable(void);

#ifdef __cplusplus
}
#endif

#endif

#endif
/**
* @} End of "addtogroup microblaze_cache_apis".
*/
