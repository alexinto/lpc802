/***************************************************************************//**
 * @file config.h.
 * @brief  Конфигурация модулей.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CONFIG_H
#define CONFIG_H
#include "Types/System/idn_types.h"
#include "DRV/uart.h"
// Конфигурация SyncClock
#define CMD_OSCILLATOR_NUM 2
#define CMD_OSCILLATOR_1   0
#define CMD_OSCILLATOR_2   1
// Критическая секция
void platform__intr_en();
void platform__intr_dis();
extern uint32_t crit_section;
#define CRITICAL_SECTION_ON {++crit_section;platform__intr_dis();
#define CRITICAL_SECTION_OFF if(--crit_section == 0) platform__intr_en();}
// Конфигурация системы
#define SW_VERSION {0x01,0x07,0x99,0x00}         // Нули не удалять!
#define SW_HASH {"0000000000000000000000000000000000000000"} // Нули не удалять!
#define IDN_DESCR IDN_SYNC_DEBUG                 // Выбрать из списка в idn_types.h!
#define FIRMWARE_NUM   1
#define FW_CMD_PROC_ID 0
// Конфигурация RXIEthernet
#define RXI_ETNERNET_CMD_ID    0                 // основной обработчик команд RXI_EThernet
#define RXI_CMD_BUFF    8000                     // Размер буфера команд
#define RXI_ANSWER_BUFF 2000                     // Размер буфера ответов на команды
#define MIN_CMD_BUFF    8000                     // Для совместимости. todo не забыть удалить!!!

#define WEAK_FUNC __attribute__((weak))          // Аттрибут "слабая" функция
// Конфигурация загрузчика
#define APP_HDR_SIZE 0x2000
#define APP_ADDR  0x00000000
#define APP_SIZE  0x00A00000
// Конфигурация модуля режимов работы fsm_mode.c
#define FSM_MODE_NUM 2                           // Количество режимов работы (0 -рабочий, 1 - Тестовый)
// Конфигурация флеш-памяти
#define FLASH__DEV_NUM             3                                // Количество флеш- накопителей
#define FLASH_MAIN                 0
#define SYNC_CLOCK_1_FLASH_ID      1
#define SYNC_CLOCK_2_FLASH_ID      2
// Конфигурация модуля dev_setts.c
#define DEV_SETTS__NUM             4                                // Количество модулей настроек
#define MAX_SIZE_DATA_BUF          256                              // Размер буфера QSPI
#define PARAMS_ADDR                0                                // Начальный адрес параметров
#define PARAMS_SIZE                (0x8000 * 2)                     // Размер параметров (кратен sector_size * 2)
#define MAX_PARAM_NUM              100                              // максимальное число настроечных параметров
#define SETTINGS_ID                0                                // Идентификатор настроек
#define PARAMS_ID_1                1                                // Идентификатор параметров 1-го SyncClock
#define INT_PARAM_ID               2                                // Идентификатор внутренних параметров
#define PARAMS_ID_2                3                                // Идентификатор параметров 2-го SyncClock

#define DEV_SETTS_PAGE_SIZE        0x10000                          // Размер сектора Flash
#define SETTINGS_ADDR              (APP_ADDR + APP_SIZE * 2)        // Начальный адрес настроек
#define SETTINGS_SIZE              (DEV_SETTS_PAGE_SIZE * 2)        // Размер настроек (кратен 0x10000 * 2)
#define INT_PARAM_ADDR             (SETTINGS_ADDR + SETTINGS_SIZE)
#define INT_PARAM_SIZE             (DEV_SETTS_PAGE_SIZE * 2)
#define INT_PARAM_NUM              FSM_MODE_NUM                     // 0-1 использует модуль fsm_mode.
// Конфигурация журнала log.c
#define LOG_SECTOR_SIZE            0x10000                          // размер страницы Flash
#define LOG__ADDR                  (INT_PARAM_ADDR + INT_PARAM_SIZE)// Начальный адрес журнала
#define LOG__SIZE                  (LOG_SECTOR_SIZE * 2)            // Размер журнала (в байтах) - кратен 4096
// Конфигурация модуля UART
#define HW_UART_COUNT 2
#define UART_DEBUG    (UART__TYPE_HW | 0)
#define UART_CONSOLE  (UART__TYPE_HW | 1)
// Конфигурация Debug
#define FDEBUG__ON
#define FDEBUG__ITF_ID UART_DEBUG
// Конфигурация модуля Resource
#define CMD_PROC__ID      1
#define IO_PROC__ID       2
#define SYS_PROC__ID      3
#define DIAG_PROC__ID     4
#define MUTEX_DEBUG       (void*)XPAR_UARTLITE_0_BASEADDR
#define MUTEX_QSPI        (void*)XPAR_SPI_0_BASEADDR
#define MUTEX_RAM         (void*)0xC0000000
#define MUTEX_SYNCCLOCK   (void*)0x48000104

// Пользовательские параметры
#define ADMIN_PASSWORD "ADMINISTRATORRXI"

#endif
