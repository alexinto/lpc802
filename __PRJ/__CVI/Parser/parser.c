#include <utility.h>
#include <ansi_c.h>
#include "System/crc.h"

#define MAX_LEN 500

static void ver_inc(int* ver);
static int cat_file(char* path, char* out, int cat_bytes);
static int info_file(char* path, char* out);
static int boot_hdr_create(char* hdr_file, int sector_size, int hdr_addr, uint32_t upd_addr);
static int add_crc32(char* file);
static int add_hash(char* path, char* hash);

int main (int argc, char *argv[]) {
	uint32_t upd_addr;
	int cat_len, sector_size, hdr_addr;
	char buff[MAX_LEN], buff_add[MAX_LEN];
	char* path = argv[1];
	char* sw_version = {0};
	int ver[4], res;

	res = SetBreakOnLibraryErrors(0);

	if (argc < 2) {
		printf("Params not be found!");
		return -1;
	}

	if (strcmp(argv[1], "-cat") == 0) {
		if (argc < 5) {
			printf("Params not be found!");
			return -1;
		}
		sscanf(argv[2],"%d", &cat_len);
		return cat_file(argv[3], argv[4], cat_len);
	}

	if (strcmp(argv[1], "-info") == 0) {
		if (argc < 4) {
			printf("Params not be found!");
			return -1;
		}
		return info_file(argv[2], argv[3]);
	}
	if (strcmp(argv[1], "-addhash") == 0) {
		if (argc < 4) {
			printf("Params not be found!");
			return -1;
		}
		return add_hash(argv[2], argv[3]);
	}
	if (strcmp(argv[1], "-boothdr") == 0) {
		if (argc < 6) {
			printf("Params not be found!");
			return -1;
		}
		sscanf(argv[3],"%d", &sector_size);
		sscanf(argv[4],"%d", &hdr_addr);
		sscanf(argv[5],"%d", &upd_addr);
		return boot_hdr_create(argv[2], sector_size, hdr_addr, upd_addr);
	}

	if (strcmp(argv[1], "-addcrc") == 0) {
		if (argc < 3) {
			printf("Params not be found!");
			return -1;
		}
		return add_crc32(argv[2]);
	}



	FILE * file = fopen (path, "r+");
	SetBreakOnLibraryErrors(res);

	fpos_t prev_pos;

	if (file == NULL) {
		printf("File (%s) not open!", path);
		fgets(buff, MAX_LEN, stdin);
		return -1;
	}
	fgetpos(file, &prev_pos);

	while (fgets (buff, MAX_LEN, file)) {
		sw_version = strstr (buff, "SW_VERSION");
		if (sw_version)
			break;
		fgetpos(file, &prev_pos);
	}
	if (sw_version == NULL) {
		printf("SW_VERSION not found!");
		fclose(file);
		return -1;
	}

	sw_version = strstr(sw_version, "{");
	strcpy(buff_add, strstr(sw_version, "}"));

	sw_version++;

	sscanf(sw_version, "%x,%x,%x,%x", &ver[3], &ver[2], &ver[0], &ver[1]);
	uint8_t ver_m[4];
	for(int i = 0; i < 4; i++)
		ver_m[i] = (uint8_t)ver[i];
	uint16_t ver_inc = *(uint16_t*)ver_m;
	ver_inc++;
	*(uint16_t*)ver_m = ver_inc;
	sprintf(sw_version, "0x%02x,0x%02x,0x%02x,0x%02x%s", ver_m[3], ver_m[2], ver_m[0], ver_m[1], buff_add);

	fsetpos(file, &prev_pos);
	fputs (buff, file);
	fseek(file, 0, SEEK_CUR);

	fflush(file);
	fclose(file);
	return 0;
}


static int cat_file(char* path, char* out, int cat_bytes) {
	int res = 0;
	FILE * f_in = NULL;
	FILE * f_out = NULL;
	res = SetBreakOnLibraryErrors(0);
	f_in = fopen (path, "rb");
	f_out = fopen (out, "wb");
	SetBreakOnLibraryErrors(res);

	if ((f_in == NULL) || (f_out == NULL)) {
		if (f_in != NULL)
			fclose(f_in);
		else
			printf("File (%s) not open!", path);
		if (f_out != NULL)
			fclose(f_out);
		else
			printf("File (%s) not open!", out);
		return -1;
	}
	// Trunc cat_bytes from header
	printf("Start truncate %d bytes from %s!", cat_bytes, path);
	for(int i = 0; i < cat_bytes; i++)
		getc(f_in);

	while((res = getc(f_in)) >= 0)
		putc(res, f_out);

	fflush(f_out);
	fclose(f_out);
	fclose(f_in);
	printf("Stop truncate %d bytes from %s!", cat_bytes, path);
	return 0;
}

static int info_file(char* path, char* out) {
	int ver[4];
	char buff[MAX_LEN], *sw_version = {0};
	FILE * file = fopen (path, "r");
	FILE * f_out = fopen (out, "w");
	if ((file == NULL) || (f_out == NULL)) {
		printf("Files (%s) (%s) not open!", path, out);
		return -1;
	}
	while (fgets (buff, MAX_LEN, file)) {
		sw_version = strstr (buff, "SW_VERSION");
		if (sw_version) {
			sw_version = strstr(sw_version, "{");
			sw_version++;
			break;
		}
	}
	uint8_t ver_m[4] = {0};

	if (sw_version != NULL) {
		sscanf(sw_version, "%x,%x,%x,%x", &ver[3], &ver[2], &ver[0], &ver[1]);
		for(int i = 0; i < 4; i++)
			ver_m[i] = (uint8_t)ver[i];
		sprintf(buff, "Software version: %d.%d.%d\n", ver_m[3], ver_m[2], *(uint16_t*)ver_m);
		fputs (buff, f_out);
	}
	else
		printf("SW_VERSION not found!");


	fflush(f_out);
	fclose(f_out);
	fclose(file);
	return 0;
}

static uint8_t boot_tbl[] = {0x20, 0x00, 0x00, 0x00, 0x30, 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x0F, 0x20, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00};

static int boot_hdr_create(char* hdr_file, int sector_size, int hdr_addr, uint32_t upd_addr) {
	int symbol = 0xFF;
	FILE * f_out = fopen (hdr_file, "wb");
	if (f_out == NULL) {
		printf("File (%s) not create!", hdr_file);
		return -1;
	}
	boot_tbl[8] = (uint8_t)(upd_addr >> 24);
	boot_tbl[9] = (uint8_t)(upd_addr >> 16);
	boot_tbl[10] = (uint8_t)(upd_addr >> 8);
	boot_tbl[11] = (uint8_t)upd_addr;

	for(int i = 0; i < hdr_addr; i++)
		putc(symbol, f_out);
	for(int i = 0; i < 32; i++)
		putc(boot_tbl[i], f_out);
	for(int i = 0; i < (sector_size - hdr_addr - 32); i++)
		putc(symbol, f_out);

	fflush(f_out);
	fclose(f_out);
	return 0;
}

static int add_crc32(char* file) {
	uint32_t crc = CRC__32b_THREAD_START_CONST;
	int byte;
	uint8_t crc_tbl[4];
	FILE * f_out = fopen (file, "rb");
	if (f_out == NULL) {
		printf("File (%s) not open!", file);
		return -1;
	}

	while((byte = getc(f_out)) >= 0) {
		crc = crc__32b_thread((uint8_t*)&byte, 1, crc);

	}
	fclose(f_out);
	crc_tbl[0] = (uint8_t)(crc >> 24);
	crc_tbl[1] = (uint8_t)(crc >> 16);
	crc_tbl[2] = (uint8_t)(crc >> 8);
	crc_tbl[3] = (uint8_t)crc;

	f_out = fopen (file, "ab");
	for(int i = 0; i < 4; i++)
		putc(crc_tbl[i], f_out);

	fflush(f_out);
	fclose(f_out);
	return 0;
}

static int add_hash(char* path, char* hash) {
	FILE * file = fopen (path, "r+");
	fpos_t prev_pos;
	char* hash_version = NULL;
	char buff[MAX_LEN], buff_add[MAX_LEN], buff_hash[MAX_LEN];
	if (file == NULL) {
		printf("File (%s) not open!", path);
		fgets(buff, MAX_LEN, stdin);
		return -1;
	}
	fgetpos(file, &prev_pos);

	while (fgets (buff, MAX_LEN, file)) {
		hash_version = strstr (buff, "SW_HASH");
		if (hash_version)
			break;
		fgetpos(file, &prev_pos);
	}
	if (hash_version == NULL) {
		printf("SW_HASH not found!");
		fclose(file);
		return 0;
	}

	hash_version = strstr(hash_version, "{");
	strcpy(buff_add, strstr(hash_version, "}"));
	hash_version++;
	for (int i = 0; i < 40; i++) {
		if (*hash) {
			buff_hash[i] = *hash;
			hash++;
		}
		else
			buff_hash[i] = '0';
	}
	buff_hash[40] = 0;
	sprintf(hash_version, "\"%s\"%s", buff_hash, buff_add);

	fsetpos(file, &prev_pos);
	fputs (buff, file);
	fseek(file, 0, SEEK_CUR);

	fflush(file);
	fclose(file);

	return 0;
}
