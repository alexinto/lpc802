#include <cvirte.h>
#include <userint.h>
#include <windows.h>
#include <ansi_c.h>
#include "target.h"
#include "hiperface.h"
#include "System/sw_timer.h"
#include "System/framework.h"
#include "cvi_uart.h"


static uint8_t data_buff[8*256];


static struct {
	uint8_t dev_addr;
	uint8_t cmd;
	uint8_t field;
	uint8_t addr;
	uint8_t count;
	uint8_t access;
	uint8_t buff[256];
	uint8_t data_cnt;
	uint8_t cur_field;
	uint8_t sizeof_field;
	uint8_t read_flash;
}flash_data;

static char buffer[3000], uart_buff[300];
static int panelhandle, event_close, com_port;

static int CVICALLBACK main_cout(void *functionId);
static void com_ports_check(int panel, int control);
static uint8_t uart_cb(events__e event, uint8_t* data, uint32_t len);
static void buff_save(char* path);
static void buff_load(char* path);



int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelhandle = LoadPanel (0, "hiperface.uir", PANEL)) < 0)
		return -1;

	DisableBreakOnLibraryErrors ();
	RecallPanelState (panelhandle, "settings.ini", 0);
	EnableBreakOnLibraryErrors ();

	sw_timer__init(NULL);
	main_cout(NULL);

	DisplayPanel (panelhandle);
	save_com_port (panelhandle, PANEL_RENEWCOM_2, EVENT_COMMIT, NULL, 0, 0);
	cmd_block_cb (panelhandle, PANEL_CMD_BLOCK, EVENT_COMMIT, NULL, 0, 0);

	while(!event_close) {
		ProcessSystemEvents();
		framework__cout();
		Sleep(1);
	}
	main_cout(NULL);

	SavePanelState (panelhandle, "settings.ini", 0);
	DiscardPanel (panelhandle);
	return 0;
}

int CVICALLBACK quit_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			event_close = 1;
			break;
	}
	return 0;
}

static int CVICALLBACK main_cout(void *functionId) {
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(200));
		return 0;
	}
	while(*(int*)functionId)
		Sleep(1), uart__cout(0, 0, NULL);
	*(int*)functionId = -1;
	return 0;
}

static void com_ports_check(int panel, int control) {
    int com = -1;
	char name[24];
	ClearListCtrl (panel, control);
	while(++com <= 255) {
        char ncom[24];
        sprintf(ncom, "\\\\.\\COM%d", com);
        HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
        uint32_t error = 0;
        if(hport == INVALID_HANDLE_VALUE)
            switch(error = GetLastError())
            {
                case 5: break;
                default: continue;
            }
        else
            CloseHandle(hport);
        sprintf(name, "COM %d%s", com, error != 5? "": " (занят)");
		InsertListItem (panel, control, -1, name, com);
    }
	sprintf(name, "COM %d%s", com_port, " !");
	InsertListItem (panel, control, -1, name, com_port);
}

int CVICALLBACK check_com_port (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			com_ports_check(panel, PANEL_COMPORT);
			break;
	}
	return 0;
}

int CVICALLBACK save_com_port (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int baud, parity;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, PANEL_COMPORT, &com_port);
			GetCtrlVal(panel, PANEL_BAUDRATE, &baud);
			GetCtrlVal(panel, PANEL_PARITY, &parity);
			uart__set_cfg(baud, parity);
			uart__init(com_port, uart_cb);
			uart__cout(UART__INIT, 0, 0);
			break;
	}
	return 0;
}

int CVICALLBACK cmd_add_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char temp;
	int l_cnt;
	switch (event) {
		case EVENT_COMMIT:
			GetNumTextBoxLines (panel, PANEL_CMDBOX, &l_cnt);
			GetCtrlVal(panel, PANEL_PCMD, &temp);
			sprintf(buffer, "%s%02x ", l_cnt ? "\n" : "", temp);
			SetCtrlVal(panel, PANEL_CMDBOX, buffer);
			break;
	}
	return 0;
}

static uint8_t tx_cur_pos;
int CVICALLBACK uart_tx_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int len, temp;
	uint8_t uart_len = 1;
	uint8_t crc;
	int res;
	switch (event) {
		case EVENT_COMMIT:
			tx_cur_pos = 0;
			GetCtrlVal(panel, PANEL_PADDR, uart_buff);
			crc = *uart_buff;
		    res = SetBreakOnLibraryErrors(0);
			DeleteTextBoxLines (panelhandle, PANEL_ANSWERBOX, 0, -1);
			if (GetTextBoxLine (panel, PANEL_CMDBOX, tx_cur_pos, buffer) >= 0) {
				len = strlen(buffer);
				for(int i = 0; i < len; i += 3) {
					sscanf(&buffer[i], "%x", &temp);
					uart_buff[uart_len] = (char)temp;
					crc ^= uart_buff[uart_len];
					uart_len++;
				}
				uart_buff[uart_len] = crc;
				uart__cout(UART__TX, uart_len + 1, uart_buff);
			}
			SetBreakOnLibraryErrors(res);
			break;
	}
	return 0;
}


static uint8_t uart_cb(events__e event, uint8_t* data, uint32_t len) {
	int temp, l_cnt;
	uint8_t* ptr = (uint8_t*)&flash_data;
	uint8_t uart_len = 1;
	uint8_t crc;

	if (event == EVENT__OK) {
		GetCtrlVal(panelhandle, PANEL_CHECK_WR, &temp);
		if ((!temp) || (data[1] != 0x4B)) {
			sprintf(buffer, "%02x ", *data);
			for(int i = 1; i < len; i++)
				sprintf(buffer, "%s%02x ", buffer, data[i]);
			temp = strlen(buffer);
			buffer[temp - 1] = 0x00;
			GetNumTextBoxLines (panelhandle, PANEL_ANSWERBOX, &l_cnt);
			if (l_cnt)
				SetCtrlVal(panelhandle, PANEL_ANSWERBOX, "\n");
			SetCtrlVal(panelhandle, PANEL_ANSWERBOX, buffer);
		}
		if (flash_data.read_flash) {
			switch(data[1]) {
				case 0x4E:
					flash_data.data_cnt = data[3];
					if (flash_data.data_cnt) {
						DeleteTextBoxLines (panelhandle, PANEL_ANSWERBOX, 0, -1);
						flash_data.sizeof_field = (128 - data[2]) / flash_data.data_cnt;
						flash_data.cur_field = flash_data.data_cnt - 1;
						flash_data.addr = 0;
						flash_data.access = 0;
						GetCtrlVal(panelhandle, PANEL_PADDR, &flash_data.dev_addr);
						flash_data.cmd = 0x4A;
						flash_data.field = flash_data.cur_field;
						flash_data.count = flash_data.sizeof_field;
						flash_data.buff[0] = flash_data.dev_addr;
						for(int i = 1; i < 6; i++)
							flash_data.buff[0] ^= ptr[i];
						uart__cout(UART__TX, 7, (char*)&flash_data);
						return 0;
					}
					break;
				case 0x4A:
					if (len < 8)
						break;
					memcpy(&data_buff[flash_data.cur_field * flash_data.sizeof_field], &data[5], len - 5);
					if (flash_data.cur_field) {

						flash_data.cur_field--;
						GetCtrlVal(panelhandle, PANEL_PADDR, &flash_data.dev_addr);
						flash_data.cmd = 0x4A;
						flash_data.field = flash_data.cur_field;
						flash_data.count = flash_data.sizeof_field;

						flash_data.buff[0] = flash_data.dev_addr;
						for(int i = 1; i < 6; i++)
							flash_data.buff[0] ^= ptr[i];
						uart__cout(UART__TX, 7, (char*)&flash_data);
						return 0;
					}
					else {
						 SetCtrlVal(panelhandle, PANEL_MODEL, &data[8]);
					}
					break;
				case 0x4D:
					if (++flash_data.cur_field > 3)
						break;
					GetCtrlVal(panelhandle, PANEL_PADDR, &flash_data.dev_addr);
					flash_data.cmd = 0x4D;
					flash_data.field = flash_data.cur_field;
					flash_data.addr = 0xC1;
					flash_data.count = 0x55;
					flash_data.access = flash_data.dev_addr ^ 0x4D ^ flash_data.field ^ 0xC1 ^ 0x55;
					uart__cout(UART__TX, 6, (char*)&flash_data);
					return 0;
				default:
					break;
			}
			flash_data.read_flash = 0;
			return 0;
		}

		GetCtrlVal(panelhandle, PANEL_CHECK_WR, &temp);
		char str_buff[100];
		if ((temp) && (data[1] == 0x4B)) {
			if (GetTextBoxLine (panelhandle, PANEL_CMDBOX, tx_cur_pos, buffer) >= 0) {
				sprintf(str_buff, "\nWrite = OK");
				for(int i = 0; i < 4; i++) {
					sscanf(&buffer[i * 3], "%x", &temp);
					if (data[1 + i] != temp) {
						sprintf(str_buff, "\nWrite = ERROR");
						break;
					}
				}
				SetCtrlVal(panelhandle, PANEL_ANSWERBOX, str_buff);
			}
		}

		tx_cur_pos++;
		int res = SetBreakOnLibraryErrors(0);
		if (GetTextBoxLine (panelhandle, PANEL_CMDBOX, tx_cur_pos, buffer) >= 0) {
			GetCtrlVal(panelhandle, PANEL_PADDR, uart_buff);
			crc = *uart_buff;
			len = strlen(buffer);
			for(int i = 0; i < len; i += 3) {
				sscanf(&buffer[i], "%x", &temp);
				uart_buff[uart_len] = (char)temp;
				crc ^= uart_buff[uart_len];
				uart_len++;
			}
			uart_buff[uart_len] = crc;
			uart__cout(UART__TX, uart_len + 1, uart_buff);
		}
		SetBreakOnLibraryErrors(res);
	}

	return 0;
}

int CVICALLBACK sett_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char path[MAX_PATHNAME_LEN] = {"*.ini"};
	switch (event) {
		case EVENT_COMMIT:
			if (control == PANEL_SAVEBUTTON) {
				if(FileSelectPopup("", path,  "Settings files (*.ini);", "Save settings", VAL_SAVE_BUTTON, 0, 1, 1, 1, path) == 0)
					return 0;
				buff_save(path);
				break;
			}
			if(FileSelectPopup("", path,  "Settings files (*.ini);", "Open settings", VAL_LOAD_BUTTON, 0, 1, 1, 1, path) == 0)
				return 0;
			buff_load(path);
			break;
	}
	return 0;
}


int CVICALLBACK read_flash_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			flash_data.read_flash = 1;
			GetCtrlVal(panelhandle, PANEL_PADDR, &flash_data.dev_addr);
			flash_data.cmd = 0x4E;
			flash_data.field = flash_data.cmd ^ flash_data.dev_addr;
			uart__cout(UART__TX, 3, (char*)&flash_data);

			break;
	}
	return 0;
}


static void buff_save(char* path) {
	int len;
	int res = 0;
	FILE * f_out = NULL;
	res = SetBreakOnLibraryErrors(0);
	f_out = fopen (path, "w");
	SetBreakOnLibraryErrors(res);
	if (f_out == NULL)
		return;
	res = SetBreakOnLibraryErrors(0);
	for(int i = 0; GetTextBoxLine (panelhandle, PANEL_ANSWERBOX, i, buffer) >= 0; i++) {
		len = strlen(buffer);
		if (len > 3) {
			buffer[len - 3] = 0x0D;
			buffer[len - 2] = 0x0A;
			buffer[len - 1] = 0;
			fputs (&buffer[3], f_out);
		}
	}
	SetBreakOnLibraryErrors(res);
	fflush(f_out);
	fclose(f_out);
}

static void buff_load(char* path) {
	char buff[3000];
	int res = 0;
	FILE * f_out = NULL;
	res = SetBreakOnLibraryErrors(0);
	f_out = fopen (path, "r");
	SetBreakOnLibraryErrors(res);
	if (f_out == NULL)
		return;
	DeleteTextBoxLines (panelhandle, PANEL_CMDBOX, 0, -1);
	res = SetBreakOnLibraryErrors(0);

	while(fgets(buff, sizeof(buff), f_out))	{
		buff[0] = '4';
		buff[1] = 'B';
		buff[2] = ' ';
		memcpy(buffer, buff, 12);
		buffer[12] = '0';
		buffer[13] = '0';
		buffer[14] = ' ';
		strcpy(&buffer[15], &buff[12]);
		SetCtrlVal(panelhandle, PANEL_CMDBOX, buffer);
	}
	SetBreakOnLibraryErrors(res);
	fclose(f_out);
}






int CVICALLBACK cmd_block_cb (int panel, int control, int event,void *callbackData, int eventData1, int eventData2) {
	int temp;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &temp);
			SetCtrlAttribute (panel, PANEL_CMDBOX, ATTR_DIMMED, temp);
			break;
	}
	return 0;
}

int CVICALLBACK init_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			flash_data.read_flash = 1;
			flash_data.cur_field = 0;
			GetCtrlVal(panelhandle, PANEL_PADDR, &flash_data.dev_addr);
			flash_data.cmd = 0x4D;
			flash_data.field = flash_data.cur_field;
			flash_data.addr = 0xC1;
			flash_data.count = 0x55;
			flash_data.access = flash_data.dev_addr ^ 0x4D ^ flash_data.field ^ 0xC1 ^ 0x55;
			uart__cout(UART__TX, 6, (char*)&flash_data);

			break;
	}
	return 0;
}
