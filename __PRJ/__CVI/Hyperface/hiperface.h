/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_COMMANDBUTTON              2       /* control type: command, callback function: quit_cb */
#define  PANEL_DECORATION                 3       /* control type: deco, callback function: (none) */
#define  PANEL_PCMD                       4       /* control type: ring, callback function: (none) */
#define  PANEL_PARITY                     5       /* control type: ring, callback function: (none) */
#define  PANEL_BAUDRATE                   6       /* control type: ring, callback function: (none) */
#define  PANEL_COMPORT                    7       /* control type: ring, callback function: (none) */
#define  PANEL_RENEWCOM_4                 8       /* control type: command, callback function: uart_tx_cb */
#define  PANEL_RENEWCOM_3                 9       /* control type: command, callback function: cmd_add_cb */
#define  PANEL_LOADBUTTON_2               10      /* control type: command, callback function: read_flash_cb */
#define  PANEL_INIT_BUTTON                11      /* control type: command, callback function: init_cb */
#define  PANEL_LOADBUTTON                 12      /* control type: command, callback function: sett_cb */
#define  PANEL_SAVEBUTTON                 13      /* control type: command, callback function: sett_cb */
#define  PANEL_RENEWCOM_2                 14      /* control type: command, callback function: save_com_port */
#define  PANEL_RENEWCOM                   15      /* control type: command, callback function: check_com_port */
#define  PANEL_DECORATION_2               16      /* control type: deco, callback function: (none) */
#define  PANEL_ANSWERBOX                  17      /* control type: textBox, callback function: (none) */
#define  PANEL_CMDBOX                     18      /* control type: textBox, callback function: (none) */
#define  PANEL_PADDR                      19      /* control type: numeric, callback function: (none) */
#define  PANEL_CMD_BLOCK                  20      /* control type: radioButton, callback function: cmd_block_cb */
#define  PANEL_CHECK_WR                   21      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG                    22      /* control type: textMsg, callback function: (none) */
#define  PANEL_MODEL                      23      /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK check_com_port(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cmd_add_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cmd_block_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK init_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK quit_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK read_flash_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK save_com_port(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK sett_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK uart_tx_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
