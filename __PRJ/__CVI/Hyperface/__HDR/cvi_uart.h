/***************************************************************************//**
 * @file cvi_uart.c.
 * @brief ���� � ��������� uart.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef _H_CVI_UART
#define _H_CVI_UART

#include "System/framework.h"

#define UART_BUFF 250
#define UART_TIMEOUT 0.3

typedef enum {
	UART__DEINIT = -2,
	UART__INIT,
	UART__IDLE = 0,
	UART__TX,
	UART__RX,
}uart_state_e;



typedef uint8_t (*uart__cb)(events__e event, uint8_t* data, uint32_t len);

void uart__set_cfg(int baudrate, int parity);

events__e uart__init(int uart_id, uart__cb cb);

void uart__deinit();

events__e uart__cout(uart_state_e state, u8 len, char* buff);





















#endif
