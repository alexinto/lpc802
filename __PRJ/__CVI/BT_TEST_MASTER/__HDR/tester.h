/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_COM_RENEW                  2       /* control type: command, callback function: com_renew_cb */
#define  PANEL_QUITBUTTON                 3       /* control type: command, callback function: QuitCallback */
#define  PANEL_LED_5                      4       /* control type: LED, callback function: (none) */
#define  PANEL_LED_4                      5       /* control type: LED, callback function: (none) */
#define  PANEL_LED_3                      6       /* control type: LED, callback function: (none) */
#define  PANEL_LED_2                      7       /* control type: LED, callback function: (none) */
#define  PANEL_LED_1                      8       /* control type: LED, callback function: (none) */
#define  PANEL_DECORATION_6               9       /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION                 10      /* control type: deco, callback function: (none) */
#define  PANEL_BINARYSWITCH_3             11      /* control type: binary, callback function: buttons_callback */
#define  PANEL_BINARYSWITCH_2             12      /* control type: binary, callback function: buttons_callback */
#define  PANEL_BINARYSWITCH               13      /* control type: binary, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_5             14      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_4             15      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_3             16      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_2             17      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_1             18      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TEXTMSG_5                  19      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_7                  20      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_4                  21      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_3                  22      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG                    23      /* control type: textMsg, callback function: (none) */
#define  PANEL_DECORATION_4               24      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_2               25      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_5               26      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_8               27      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_7               28      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_3               29      /* control type: deco, callback function: (none) */
#define  PANEL_TEXTMSG_2                  30      /* control type: textMsg, callback function: (none) */
#define  PANEL_GRAPH_3                    31      /* control type: graph, callback function: (none) */
#define  PANEL_GRAPH_2                    32      /* control type: graph, callback function: (none) */
#define  PANEL_GRAPH                      33      /* control type: graph, callback function: (none) */
#define  PANEL_TEXTBOX                    34      /* control type: textBox, callback function: (none) */
#define  PANEL_TEXTMSG_6                  35      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_43                36      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_44                37      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_45                38      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_34                39      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_16                40      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_35                41      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_31                42      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_17                43      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_36                44      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_40                45      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_32                46      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_18                47      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_37                48      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_25                49      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_41                50      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_33                51      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_26                52      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_55                53      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_38                54      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_27                55      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_52                56      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_42                57      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_28                58      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_49                59      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_29                60      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_56                61      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_39                62      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_46                63      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_30                64      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_61                65      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_58                66      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_53                67      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_19                68      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_20                69      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_50                70      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_21                71      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_57                72      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_22                73      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_47                74      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_62                75      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_23                76      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_59                77      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_54                78      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_24                79      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_2                 80      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_51                81      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_3                 82      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_4                 83      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_5                 84      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_63                85      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_48                86      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_6                 87      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_60                88      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_7                 89      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_8                 90      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_9                 91      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_10                92      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_11                93      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_12                94      /* control type: textMsg, callback function: (none) */
#define  PANEL_PICTURE                    95      /* control type: picture, callback function: (none) */
#define  PANEL_CHIP_NUM_13                96      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_14                97      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_1                 98      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_15                99      /* control type: textMsg, callback function: (none) */
#define  PANEL_GRAPH_CHIPS_ERROR          100     /* control type: graph, callback function: (none) */
#define  PANEL_TIME_PROGRAM               101     /* control type: string, callback function: (none) */
#define  PANEL_COM_CHANGE                 102     /* control type: ring, callback function: (none) */
#define  PANEL_TIMEOUT_VALUE              103     /* control type: scale, callback function: tester_timeout_cb */
#define  PANEL_TIMEOUT_VIEW               104     /* control type: numeric, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK buttons_callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_renew_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK tester_timeout_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
