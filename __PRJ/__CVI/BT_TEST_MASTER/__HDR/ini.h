 /**************************************************************************//**
 * @file ini.h
 * @brief ����������� modbus rtu (����������)
 * @authors p.adaskin
 ******************************************************************************/
#ifndef INI_H
#define INI_H
//******************************************************************************
#include <inifile.h>
//******************************************************************************

typedef enum
{
    INI__EVENT_OK =     0,
    INI__EVENT_WRITE = -1,
    INI__EVENT_NULL =  -2,
} ini__event_code_e;

typedef struct ini__deskriptor_t
{
    IniText ini; // handle cvi ��������� � ������
    int root;    // HKEY_CLASSES_ROOT; HKEY_CURRENT_USER; HKEY_LOCAL_MACHINE; HKEY_USERS - root �������
                 // 0 - ������� ������ � ������
    char file_or_subkey[MAX_PATHNAME_LEN]; // root != 0 ==> subkey �������
                                           // root == 0 ==> ���� � �����
} ini__deskriptor_t;
/***************************************************************************//**
 * @brief �������� ini ��������� � ������, � (��������) ������ 
 * @param root - ��. ini__deskriptor_t.root
 * @param file_or_subkey ��. ini__deskriptor_t.file_or_subkey
 * @return NULL - ������ ��������
 *         !NULL - ��������� �� ��������� ini__deskriptor_t
 ******************************************************************************/
ini__deskriptor_t* ini__create(int root, char* file_or_subkey);

/***************************************************************************//**
 * @brief ������� INI, ����������� ������ 
 * @param d - ���������� INI
 * @return void
 ******************************************************************************/
void ini__close(ini__deskriptor_t* d);

ini__event_code_e ini__save(ini__deskriptor_t* d);
//******************************************************************************
char* ini__string_get(ini__deskriptor_t* d, char* section, char* key, char* string, int str_len);
void ini__string_put(ini__deskriptor_t* d, char* section, char* key, char* string);
int ini__int_get(ini__deskriptor_t* d, char* section, char* key, int value);
void ini__int_put(ini__deskriptor_t* d, char* section, char* key, int value);
float ini__float_get(ini__deskriptor_t* d, char* section, char* key, float value);
void ini__float_put(ini__deskriptor_t* d, char* section, char* key, float value);

//******************************************************************************
void ini__panel_put(ini__deskriptor_t* d, char* section, int panel);
void ini__panel_get(ini__deskriptor_t* d, char* section, int panel);
//******************************************************************************
#endif
