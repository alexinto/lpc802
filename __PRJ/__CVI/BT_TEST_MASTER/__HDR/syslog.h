/***************************************************************************//**
 * @file syslog.h
 * @brief ������ ����������� �����������
 * @author p.adaskin
 ******************************************************************************/
#ifndef _SYSLOG_H
#define _SYSLOG_H
//******************************************************************************
#include "rtc.h"
// ��������� ��������������
typedef void (*appl__syslog_print_t)(int direction, char* message);

typedef struct syslog__format   // ��������� ��������������
{
    rtc__format_e format;       // ������ ����
    const char* separator;      // ��������� �������
    int16_t str_len;            // ����� ������ �� �������� �� �����
    int16_t hex_in_str;         // ����� ������ �����
    appl__syslog_print_t func;  // ������� ������ ��� �����������. ���� NULL, �� ����� �����������
} syslog__format;

// ���� ���������
typedef enum 
{
    SYSLOG__CONTINIUS,          // ���������� ����������� ���� � ������� ��������� ������� �� �������
    SYSLOG__DEBUG,              // ��� ������� ���������� ������, ������ ������������� ������� ����� �� ����������
    SYSLOG__APPL,
    SYSLOG__CORE,
    SYSLOG__MODBUS,
} syslog__subsystem_e;

typedef struct static___direct_t
{
    int direction;                  // 1... (0 - � ������, �� ���������)
    syslog__subsystem_e subsystem;
    uint16_t msk_level;
} static___direct_t;

//************************************************************************
// ��������� ���������� �����������. �������� ��������������� � "target.h"
//************************************************************************

#ifndef SYSLOG__SUBSUSTEM_NAME
// ������ ��������� ������ � ��������� ����������. ��� - ������ ������, �� ����� ���������� �����������, ���� ���� �����
#define SYSLOG__SUBSUSTEM_NAME(E)           \
    ((E) ==  SYSLOG__APPL?      "appl":     \
     (E) ==  SYSLOG__CORE?      "core":     \
     (E) ==  SYSLOG__MODBUS?    "modbus":   \
                                "debug")    // �� ���������
#endif

#ifndef SYSLOG_STATIC_DIRECT_COUNT
    // ������������� ������� ���� static___direct_t
    // �� ��������� - debug ==> �1
    #define SYSLOG_STATIC_DIRECT_COUNT 1   // ����������� ������� (���� �����, �� �� ������� SYSLOG_STATIC_DIRECT_INIT)
    #define SYSLOG_STATIC_DIRECT_INIT { \
    {.direction = 1, .subsystem = SYSLOG__DEBUG, .msk_level = 0xFFFF}}
#endif

#ifndef SYSLOG__MSG_LEN
    #define SYSLOG__MSG_LEN 255         // ������������ ����� ������ ������� ��������� (�� ��������� �� ������)
#endif

#ifndef SYSLOG__SUBSYSTEM 
    #define SYSLOG__SUBSYSTEM "12"      // ����� �������� ���������� � ������ ��� ������������. �� ������ ��������� 20 (������: "system[ll]   ")
#endif

#ifndef SYSLOG__FORMAT
    // ������������� ��������� ���� syslog__format
    // ������������ ����� ������������� ���� � �� ����� �������
    #define SYSLOG__FORMAT(D) {.format = RTC__HMS_MS, .separator = " | ", .str_len = 120/*SYSLOG__MSG_LEN*/, .hex_in_str = 16}
#endif

//*********************************************************************************
#ifdef SYSLOG
// S = subsysnem, L - level, M - message (� ����� printf), DMP - ����� �����, L - ����� �����, D - direct
// 
    #define SYSLOG__PRINT_DUMP(S, L, M, DMP, LEN, ...) syslog__print(S, L, M, (uint8_t*)DMP, LEN, ##__VA_ARGS__)
    #define SYSLOG__PRINT(S, L, M, ...) syslog__print(S, L, M, NULL, 0,  ##__VA_ARGS__)
    #define SYSLOG__CONNECT(S, L, D) syslog__connect(S, L, D)

    void appl__syslog_print(int direction, char* message);  // ������ ����� ����
    void syslog__level_set(syslog__subsystem_e subsystem, int level, int direction, int add);
    void syslog__print(syslog__subsystem_e subsystem, int level, char* msg, uint8_t* dump, int dump_len, ...);
#else
//���������    
    #define SYSLOG__PRINT_DUMP(S, L, M, DMP, LEN, ...)
    #define SYSLOG__PRINT(S, L, M,...)
    #define SYSLOG__CONNECT(S, L, D)
#endif
//******************************************************************************
#endif
