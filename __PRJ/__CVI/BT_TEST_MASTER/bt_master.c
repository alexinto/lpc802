#include <rs232.h>
#include <windows.h>   
//#include <synchapi.h>
#include "toolbox.h"
#include <cvirte.h>		
#include <userint.h>
#include "main.h"
#include "projects.h"
#include "standard.h"
#include "process.h"
#include "sw_timer.h"
#include "list.h"
#include "ini.h"
#include "crc.h"
#include "bt_pkt.h"
#include "cvi_uart.h"
#include "hw_bt_pkt.h"

#define NULL				0
#define TEST_TIMER 			0
#define TEST_TIMER_1 		1
#define TEST_TIMER_2 		2
#define TEST_TIMER_3 		3
#define TEST_TIMER_4 		4
#define TEST_TIMER_5 		5
#define TEST_TIMER_GRAPH	6
#define TEST_TIMER_COOLER	7    
#define TIMER_CHIP_NUM		8 

#define MAX_TIMER   		9 

typedef struct {
	uint8_t com_port;
	uint8_t buffer[1000];
	char time[80];
	uint16_t param;
}bt_master__t;

static bt_master__t bt_master;
static void TimerCallback (struct sw_timer__t *timer, void *ext_data);
static int CVICALLBACK main_cout(void *functionId); 
static int CVICALLBACK uart_thread(void *functionId); 
static void set_default_panel();
static void com_ports_check(int panel, int control);
static void graph_clear();
static void oper_cb(bt_events_e event, uint8_t* data, uint8_t len);
static void protocol_cb(bt_events_e event, uint8_t* buff, uint8_t len);  

static ini__deskriptor_t* static__ini; 
static int panelHandle;
static sw_timer__t timers[MAX_TIMER + 1];
static long int time_program = 0; 


static void oper_cb(bt_events_e event, uint8_t* data, uint8_t len) { 
	char buff[512];
	char buff_param[512];
	switch (event) {
		case EVENT__OK:
			sprintf(buff, "%s : �������� ��������� �������.\n", bt_master.time + 5);
			if (len > 3) {
				bt_master.buffer[*(uint16_t*)bt_master.buffer] = 0x00;
				if(bt_master.param == 200)
					sprintf(buff_param, "%s : ������� ������ ������� %d%%.\n", bt_master.time + 5, *(uint8_t*)(bt_master.buffer + 4));
				else
					sprintf(buff_param, "%s : ���������� �����: %s.\n", bt_master.time + 5, bt_master.buffer + 4);
				SetCtrlVal(panelHandle, PANEL_TEXTBOX_2, buff_param); 
			}
			break;
		case EVENT__TIMEOUT:
			sprintf(buff, "%s : ���������� �������� �� ��������.\n", bt_master.time + 5);
			break;
		default:
			sprintf(buff, "%s : �������� ��������� � �������.\n", bt_master.time + 5);
			break;
	}
	SetCtrlVal (panelHandle, PANEL_TEXTBOX, buff);
}
static u8 uart_buff[100];

uint8_t uart_test_cb(uart_event_e event, uint8_t* data, uint32_t len) {
	switch (event) {
		case EVENT__OK:
			break;
		case EVENT__TIMEOUT:
		default:
			break;
	}
	return 0;
}


int main (int argc, char *argv[])
{

	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "main.uir", PANEL)) < 0)
		return -1;
	
	static__ini = ini__create(0, "tester.ini");  
	ini__panel_get(static__ini, "windows", panelHandle);
	sw_timer__init(NULL);
	uart_thread(NULL);
	main_cout(NULL);
	
	sw_timer__start(&timers[TEST_TIMER], 1000, TimerCallback, (void*)panelHandle);
	bt_master.com_port = (uint8_t)ini__int_get(static__ini, "UART", "com_port", bt_master.com_port);
//	tester.timeout = ini__int_get(static__ini, "TESTER", "timeout", tester.timeout);
	com_ports_check(panelHandle, PANEL_COM_CHANGE);
	set_default_panel();
	uart__init(bt_master.com_port, uart_test_cb);
	
	DisplayPanel (panelHandle);
	RunUserInterface ();
	main_cout(NULL);
	uart__deinit();
	uart_thread(NULL);
	for(int x = 0; x < 9; x++)
		sw_timer__stop(&timers[x]);
 	ini__panel_put(static__ini, "windows", panelHandle);
	DiscardPanel (panelHandle);
	ini__int_put(static__ini, "UART", "com_port", bt_master.com_port);
//	ini__int_put(static__ini, "TESTER", "timeout", tester.timeout);
	ini__save(static__ini);
	return 0;
}

int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		QuitUserInterface(0);
	return 0;
}

int CVICALLBACK com_renew_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		com_ports_check(panel, PANEL_COM_CHANGE);
	return 0;
}

int CVICALLBACK button_write_callback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)
		return 0;
	GetCtrlVal(panelHandle, PANEL_TEXTBOX_3, bt_master.buffer);
	//bt_master__oper(BT_CMD_WRITE, bt_master.param, bt_master.buffer, (uint16_t)strlen((char*)bt_master.buffer), oper_cb);
	return 0;
}



int CVICALLBACK button_read_callback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)
		return 0;
	uart_buff[0] = 100;
	uart_buff[1] = 2;
	uart_buff[2] = 4;
	uart_buff[3] = 0;
	uart_buff[4] = crc__8_ccitt(uart_buff, 4);
	uart__cout(UART__TX, 5, uart_buff); 
	//bt_master__oper(BT_CMD_READ, bt_master.param, bt_master.buffer, 0, oper_cb);
	return 0;
}

static void TimerCallback (struct sw_timer__t *timer, void *callbackData) {
	int x, buff;
	CRITICAL_SECTION_ON
	if (timer->timeout_ms)
		sw_timer__start(timer, -1, TimerCallback, callbackData);
	CRITICAL_SECTION_OFF
	for(x = 0; (x <= MAX_TIMER) && (&timers[x] != timer); x++);
	switch (x) {
	case TEST_TIMER:
		buff = ++time_program;
		sprintf(bt_master.time, "%3d�:%02d�:%02d�:%02d�", buff/86400, (buff%86400)/3600, (buff%3600)/60, buff%60);
		SetCtrlVal ((int)callbackData, PANEL_TIME_PROGRAM, bt_master.time);
		break;
	default:
		break;
	}
	buff = 0;  
}
   
static int CVICALLBACK main_cout(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId)
        Sleep(1), sw_timer__cout();
    *(int*)functionId = -1;
    return 0;
}

static int CVICALLBACK uart_thread(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, uart_thread, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId)
        Sleep(1), uart__cout(NULL, NULL, NULL);
    *(int*)functionId = -1;
    return 0;
}

static void com_ports_check(int panel, int control) {
    int com = -1;
	char name[24];
	ClearListCtrl (panel, control); 
	while(++com <= 255)
    {
        char ncom[24];
        sprintf(ncom, "\\\\.\\COM%d", com);
        HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
        uint32_t error = 0;
        if(hport == INVALID_HANDLE_VALUE)
            switch(error = GetLastError())
            {
                case 5: break;
                default: continue;
            }
        else
            CloseHandle(hport);
        sprintf(name, "COM %d%s", com, error != 5? "": " (�����)");
		InsertListItem (panel, control, -1, name, com);
    }
	sprintf(name, "COM %d%s", bt_master.com_port, " !");
	InsertListItem (panel, control, -1, name, bt_master.com_port);
	
}

static void set_default_panel() {
	SetCtrlVal (panelHandle, PANEL_COM_CHANGE, bt_master.com_port); 
}

int CVICALLBACK com_port_change (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)	
		return 0;
	GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &bt_master.com_port); 
	uart__init(bt_master.com_port, uart_test_cb);
	return 0;
}

int CVICALLBACK param_change_cb (int panel, int control, int event,
								 void *callbackData, int eventData1, int eventData2)
{
	if (event != EVENT_COMMIT)	
		return 0;
	GetCtrlVal (panelHandle, PANEL_PARAM_CHANGE, &bt_master.param);
	return 0;
}
