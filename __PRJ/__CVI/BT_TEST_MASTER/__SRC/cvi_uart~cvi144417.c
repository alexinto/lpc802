

#include <rs232.h>
#include <ansi_c.h>
#include <utility.h>
#include "cvi_uart.h"

typedef struct {
	char len, buff[UART_BUFF], answer;	
	uart_event_e next_state, cur_state;
	unsigned int uart_com;
	uart__cb cb;
}uart_struct_t;
 

uart_event_e uart__init(int uart_id) {
	int res;
	char comport_str[10];
	CloseCom(uart_id);  
	uart.cb = oper_cb;
	sprintf(comport_str, "COM%d", uart_id);
	res = OpenComConfig (uart_id, comport_str, 9600, 0, 8, 1, 512, 512);
	SetComTime (uart_id, 1);
	return EVENT__OK;
}


uart_event_e uart__cout(uart_event_e state, char len, char* buff) {
	uart_event_e res = EVENT__OK;
	DisableBreakOnLibraryErrors (); 
	if (state) {
		CRITICAL_SECTION_ON
		if ((uart.next_state) && (state != UART__INIT) && (state != UART__DEINIT))
			res = EVENT__BUSY;
		else {
			uart.next_state = state;
			uart.len = len;
			if (len <= UART_BUFF)
				memcpy(uart.buff, buff, len);
		}
		CRITICAL_SECTION_OFF
	}
	else switch(uart.cur_state) {
		case UART__TX:
			GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &uart.uart_com);   
			uart__init(uart.uart_com);
			ComWrt(uart.uart_com, uart.buff, uart.len);
			uart.cur_state = UART__RX;
			break;
		case UART__RX:
		res = (ComRd(uart.uart_com, uart.buff, UART_BUFF) >= 0) ? EVENT__OK : EVENT__ERR;
		CloseCom(uart.uart_com);
		uart.cur_state = UART__IDLE;
		uart.cb(res);
		break;
		case UART__DEINIT:
			if (uart.next_state == UART__INIT)
				uart.cur_state = UART__IDLE;
			break;
		case UART__IDLE:
		CRITICAL_SECTION_ON
			uart.cur_state = uart.next_state;
			uart.next_state = UART__IDLE;
		CRITICAL_SECTION_OFF
			break;
		default:
			uart.cur_state = UART__IDLE;
			break;
	}
	EnableBreakOnLibraryErrors ();
	return res;
}

void uart__deinit() {
	while(uart.cur_state != UART__DEINIT)
		uart__cout(UART__DEINIT, 0, NULL);
}
