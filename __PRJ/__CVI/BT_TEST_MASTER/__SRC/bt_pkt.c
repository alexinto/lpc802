
																   /***************************************************************************//**
 * @file bt_pkt.c.
 * @brief ���� � ��������� ��������� ������ bluetooth.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "bt_pkt.h"
#include "crc.h"
																   
static uint8_t test_buffer[1000];


typedef bt_events_e (*bt_cmd)(uint8_t* data);

static bt_events_e bt_end(uint8_t* data);
static bt_events_e bt_read(uint8_t* data);
static bt_events_e bt_write(uint8_t* data);
static bt_events_e bt_break(uint8_t* data);
static bt_events_e bt_continue(uint8_t* data);
static void bt_pkt__pkt_build(bt_cmd_e cmd, uint8_t* data, uint8_t len);
static void bt_pkt__transport_cb(bt_events_e event, uint8_t* buff, uint8_t len);
static bt_events_e	bt_pkt__check_answer(uint8_t* buff);

typedef struct {
	uint8_t* buffer;
    bt_cmd_e oper;
    uint8_t crc;
    uint8_t len_pkt;
    uint16_t len, param;
    bt_pkt__cb_t cb;
    uint8_t answer[3];
	uint8_t packet[20];
}bt_pkt__t;

static bt_pkt__t bt_pkt_data;

static const bt_cmd bt_commands[] = {bt_end, bt_read, bt_write, bt_break, bt_continue};

bt_events_e bt_master__oper(bt_cmd_e cmd, uint16_t param, uint8_t* data, uint16_t len, bt_pkt__cb_t bt_cb) {
	bt_events_e res = BT_EVENT_FAIL_PKT;
	bt_pkt_data.param = param;
	bt_pkt_data.cb = bt_cb;
	bt_pkt_data.len = len;
	bt_pkt_data.buffer = data;
	if ((bt_pkt_data.oper != BT_CMD_END) || (res = bt_commands[cmd](data)) != BT_EVENT_OK)
		bt_pkt_data.oper = BT_CMD_END;
    return res;
}

static bt_events_e bt_end(uint8_t* data) {
    if (data[1] > 2) {
        if (bt_continue(data) != BT_EVENT_OK)
            return BT_EVENT_ERROR;
    }
    if (bt_pkt_data.crc)
        return BT_EVENT_CRC_ERR;
    bt_pkt_data.oper = BT_CMD_END;
    bt_pkt__pkt_build(BT_CMD_END, bt_pkt_data.answer, NULL);
    return BT_EVENT_OK;
}

static bt_events_e bt_read(uint8_t* data) {
    bt_events_e res = BT_EVENT_OK;
	bt_cmd_e cmd = BT_CMD_CONTINUE;
	uint16_t len = data[PKT_LEN] - 2;
    if (bt_pkt_data.oper != BT_CMD_READ) {
        bt_pkt_data.oper = BT_CMD_READ;
        bt_pkt_data.len_pkt = 0;
		bt_pkt_data.len = 0;
        len = bt_pkt__data_build(bt_pkt_data.param, &bt_pkt_data.packet[PKT_DATA], bt_pkt_data.packet, 0); 
		bt_pkt__pkt_build(BT_CMD_READ, bt_pkt_data.packet, (char)len);
     	res = bt_pkt__oper_transport(BT_CMD_WRITE, NULL, NULL, bt_pkt_data.packet, bt_pkt_data.packet[PKT_LEN] + 1, bt_pkt__transport_cb);
        return res;
    }
	memcpy(bt_pkt_data.buffer + bt_pkt_data.len, &data[PKT_DATA], len);	
	bt_pkt_data.len	+= len;   
	switch(data[PKT_CMD]) {
		case BT_CMD_END:
			res = crc__8_ccitt(bt_pkt_data.buffer, *(uint16_t*)bt_pkt_data.buffer + 1) ? BT_EVENT_ERROR : BT_EVENT_OPER_END;
		case BT_CMD_CONTINUE:
			break;
		default:
			res = BT_EVENT_ERROR;
			break;
	}
	bt_pkt__pkt_build(cmd, bt_pkt_data.packet, 0);
	if (res == BT_EVENT_OK)
		res = bt_pkt__oper_transport(BT_CMD_WRITE, NULL, NULL, bt_pkt_data.packet, bt_pkt_data.packet[PKT_LEN] + 1, bt_pkt__transport_cb);

	return res;
}

static bt_events_e bt_write(uint8_t* data) {
	int len;
	bt_cmd_e pkt_cmd = BT_CMD_CONTINUE;
    if (bt_pkt_data.oper != BT_CMD_WRITE) {
		bt_pkt_data.len = bt_pkt__data_build(bt_pkt_data.param, test_buffer, data, bt_pkt_data.len);
        bt_pkt_data.oper = BT_CMD_WRITE;
        bt_pkt_data.len_pkt = 0; 
		bt_pkt_data.crc = 0;
		pkt_cmd = BT_CMD_WRITE;
    }
    if ((MAX_DATA_LEN - bt_pkt_data.len_pkt) < bt_pkt_data.len)
        return BT_EVENT_ERROR;
	if (bt_pkt_data.len >= (bt_pkt_data.len_pkt + 17)) {
		len = 17;
	}
	else {
		len = bt_pkt_data.len - bt_pkt_data.len_pkt;
		if (pkt_cmd != BT_CMD_WRITE)
			bt_pkt_data.oper = pkt_cmd = BT_CMD_END;
	}
	memcpy(&bt_pkt_data.packet[PKT_DATA], test_buffer + bt_pkt_data.len_pkt, len);
	bt_pkt__pkt_build(pkt_cmd, bt_pkt_data.packet, (char)len);
	bt_pkt_data.len_pkt += len;
	
    if (bt_pkt__oper_transport(BT_CMD_WRITE, NULL, NULL, bt_pkt_data.packet, bt_pkt_data.packet[PKT_LEN] + 1, bt_pkt__transport_cb) != BT_EVENT_OK)
        return BT_EVENT_ERROR;

    return BT_EVENT_OK;
}

static bt_events_e bt_break(uint8_t* data) {
    bt_pkt_data.oper = BT_CMD_END;
    return BT_EVENT_NO_CMD;
}
    
static bt_events_e bt_continue(uint8_t* data) {
    return (bt_pkt_data.oper == BT_CMD_END) ? BT_EVENT_ERROR : bt_commands[bt_pkt_data.oper](data);
}

static void bt_pkt__pkt_build(bt_cmd_e cmd, uint8_t* data, uint8_t len) {
    data[1] = len + 2;
    data[2] = cmd;
    data[0] = crc__8_ccitt(&data[PKT_LEN], len + 2);
}

uint16_t bt_pkt__data_build(uint16_t param, uint8_t* buff_target, uint8_t* buff_source, uint16_t len) {
    memcpy(buff_target + 4, buff_source, len);
	len += 4;
    *(uint16_t*)buff_target = len;
    *(uint16_t*)&buff_target[2] = param;
    buff_target[len] = crc__8_ccitt(buff_target, len);
	return len + 1;
}

static bt_events_e	bt_pkt__check_answer(uint8_t* buff) {
	bt_events_e res = BT_EVENT_ERROR;
	if ((buff[PKT_CRC] != crc__8_ccitt(&buff[PKT_LEN], buff[PKT_LEN])) && (buff[PKT_CMD] == BT_CMD_BREAK))
		return res;
	else if (bt_pkt_data.oper != BT_CMD_END)  {
		res = bt_commands[bt_pkt_data.oper](buff);
		if (res >= BT_EVENT_OK)
			return res;
		bt_pkt__pkt_build(BT_CMD_BREAK, buff, 0);
		bt_pkt_data.oper = BT_CMD_BREAK; 
	    res = bt_pkt__oper_transport(BT_CMD_WRITE, NULL, NULL, buff, buff[PKT_LEN] + 1, bt_pkt__transport_cb);
	}
	else
		res = BT_EVENT_OPER_END;
	return res;
}

static void bt_pkt__transport_cb(bt_events_e event, uint8_t* buff, uint8_t len) {
	switch(bt_pkt_data.oper) {
		case BT_CMD_BREAK:
			event = BT_EVENT_ERROR;
		case BT_CMD_END:
		default:
			if ((event == BT_EVENT_OK) && ((event = bt_pkt__check_answer(buff)) == BT_EVENT_OK))
				return;
			if (event == BT_EVENT_OPER_END)
				event = BT_EVENT_OK;
			break;
	}
	bt_pkt_data.oper = BT_CMD_END;
    bt_pkt_data.cb(event, buff, buff[1] + 1);
}
