/***************************************************************************//**
 * @file hw_bt_pkt.c.
 * @brief ���� � ��������� ������������� ������ ��������� bluetooth.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "bt_pkt.h"
#include "hw_bt_pkt.h"

struct {
	bt_pkt__cb_t cb;	
}hw_data;

bt_events_e bt_pkt__oper_transport(bt_cmd_e cmd, uint16_t param, uint32_t addr_from, uint8_t* buff, uint32_t len, bt_pkt__cb_t cb) {
    bt_events_e res = BT_EVENT_OK;
	hw_data.cb = cb;
    switch(cmd) {
    case BT_CMD_WRITE:
		uart__cout(UART__TX, (char)len, (char*)buff);   
        break;
    case BT_CMD_READ:
        break;
    default:
        res = BT_EVENT_NO_CMD;
        break;
    }
    return res;
}


uint8_t hw_bt_pkt__oper_cb(uart_event_e event, uint8_t* data, uint32_t len) {
	switch (event) {
		case EVENT__OK:
	        hw_data.cb(BT_EVENT_OK, data, (char)len);
			break;
		case EVENT__TIMEOUT:
		default:
	        hw_data.cb(BT_EVENT_ERROR, data, (char)len);
			break;
	}
	return 0;
}
