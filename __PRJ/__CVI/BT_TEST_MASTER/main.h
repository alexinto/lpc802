/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_DATA_WRITE                 2       /* control type: command, callback function: button_write_callback */
#define  PANEL_DATA_READ                  3       /* control type: command, callback function: button_read_callback */
#define  PANEL_COM_RENEW                  4       /* control type: command, callback function: com_renew_cb */
#define  PANEL_QUITBUTTON                 5       /* control type: command, callback function: QuitCallback */
#define  PANEL_DECORATION_9               6       /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_6               7       /* control type: deco, callback function: (none) */
#define  PANEL_TEXTMSG_8                  8       /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_7                  9       /* control type: textMsg, callback function: (none) */
#define  PANEL_DECORATION_5               10      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_10              11      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_8               12      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_3               13      /* control type: deco, callback function: (none) */
#define  PANEL_TEXTMSG_2                  14      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTBOX_2                  15      /* control type: textBox, callback function: (none) */
#define  PANEL_TEXTBOX_3                  16      /* control type: textBox, callback function: (none) */
#define  PANEL_TEXTBOX                    17      /* control type: textBox, callback function: (none) */
#define  PANEL_TIME_PROGRAM               18      /* control type: string, callback function: (none) */
#define  PANEL_PARAM_CHANGE               19      /* control type: ring, callback function: param_change_cb */
#define  PANEL_COM_CHANGE                 20      /* control type: ring, callback function: com_port_change */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK button_read_callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK button_write_callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_port_change(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_renew_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK param_change_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
