/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_COMMANDBUTTON_2            2       /* control type: command, callback function: button_capture_cb */
#define  PANEL_COMMANDBUTTON              3       /* control type: command, callback function: quit_cb */
#define  PANEL_SIZE_Y                     4       /* control type: scale, callback function: (none) */
#define  PANEL_SIZE_X                     5       /* control type: scale, callback function: (none) */
#define  PANEL_POS_Y                      6       /* control type: scale, callback function: (none) */
#define  PANEL_POS_X                      7       /* control type: scale, callback function: (none) */
#define  PANEL_PICTURE                    8       /* control type: picture, callback function: (none) */
#define  PANEL_IMG_CHANGE                 9       /* control type: ring, callback function: img_change_cb */
#define  PANEL_FLOWER                     10      /* control type: binary, callback function: script_cb */
#define  PANEL_SCRIPT                     11      /* control type: binary, callback function: script_cb */
#define  PANEL_BINARYSWITCH               12      /* control type: binary, callback function: video_ctrl_cb */
#define  PANEL_DEAD_TIME                  13      /* control type: numeric, callback function: (none) */
#define  PANEL_AGR_TIME                   14      /* control type: numeric, callback function: (none) */
#define  PANEL_TIME_PET                   15      /* control type: numeric, callback function: (none) */
#define  PANEL_AGR_ALWAYS                 16      /* control type: radioButton, callback function: (none) */
#define  PANEL_SPOT_3                     17      /* control type: radioButton, callback function: three_spot_cb */
#define  PANEL_TYR                        18      /* control type: radioButton, callback function: tyr_cb */
#define  PANEL_FAST_CP                    19      /* control type: radioButton, callback function: fast_cp_cb */
#define  PANEL_FORCE                      20      /* control type: radioButton, callback function: force_cb */
#define  PANEL_LOW_HP                     21      /* control type: radioButton, callback function: (none) */
#define  PANEL_DECORATION_3               22      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_4               23      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_2               24      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION                 25      /* control type: deco, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK button_capture_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK fast_cp_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK force_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK img_change_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK quit_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK script_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK three_spot_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK tyr_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK video_ctrl_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif