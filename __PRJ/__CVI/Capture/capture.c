﻿/***************************************************************************//**
 * @file capture.c.
 * @brief Модуль управления клавиатурой по событиям захвата видео с экрана.
 * @author a.tushentsov
 ******************************************************************************/
#include <ansi_c.h>
#include <cvirte.h>
#include <userint.h>
#include <windows.h>
#include <utility.h>
#include "d3dx9core.h"
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/events.h"
#include "DRV/KBD/key_code.h"
#include "CVI/pixel.h"
#include "capture.h"

typedef struct {
	int x;
	int y;
}mouse_point_t;

typedef struct {
	int handle;
	char* buff;
	int x, y, width, height;
}image_t;

typedef enum {
	SCRIPT__IDLE  = 0,
	SCRIPT__START = 1,
	SCRIPT__WORK  = 2,
	SCRIPT__STOP  = 3,
}script__fsm_e;

typedef enum {
	KEY_CODE__STATE_RELEASE   = 0,
	KEY_CODE__STATE_PRESSED   = 1,
}key_code__state_e;

typedef enum {
	SCRIPT__PIX_CMD_NONE = 0,
	SCRIPT__PIX_CMD_EQUAL,
	SCRIPT__PIX_CMD_NOT_EQ,
	SCRIPT__PIX_CMD_FIND_BUFF,
}script_pix_cmd_e;

typedef struct {
	pixel__t color;
	int pixel_num;
	int img_num;
	script_pix_cmd_e cmd;
	key_code__e key;
	uint8_t one_key;
	int* on_flag;
}script__pix_t;

typedef struct {
	uint16_t start_key;
	uint16_t stop_key;
	key_code__e key;
	script__pix_t* pix;
	script__fsm_e state;
	uint8_t start;
	uint8_t ctrl;
}script__t;

typedef enum {
	SCRIPT_MODE_NORMAL = 0,
	SCRIPT_MODE_FARM   = 1,
	SCRIPT_MODE_FLOWER = 2,
}script_mode_e;

typedef enum {
	BUFF_STONE = 0,
	BUFF_PIG   = 1,

	BUFF_CNT,
}buff_e;

#define IMAGE_SKILL   0
#define IMAGE_HP      1
#define IMAGE_TARGET  2
#define IMAGE_BUFF    3

static image_t img_test;
static image_t image[] = { [IMAGE_SKILL]  = {0, NULL, .x = 1420, .y = 840, .width = 474, .height = 177},
						   [IMAGE_HP]     = {0, NULL, .x = 13,   .y = 0,   .width = 160, .height = 110},
						   [IMAGE_TARGET] = {0, NULL, .x = 850,  .y = 0,   .width = 300, .height = 80},
						   [IMAGE_BUFF]   = {0, NULL, .x = 189,  .y = 0,   .width = 312, .height = 78},
						 };

#define IMAGE_CNT (sizeof(image) / sizeof(image[0]))



static key_code__struct_t keys, old_keys;
static int tyr_mode;
static script_mode_e script_mode;
//                                   b   g   r   y
static script__pix_t mob_no_hp[] = {{18, 22, 109, 0, 300*51 + 24, IMAGE_TARGET, SCRIPT__PIX_CMD_NOT_EQ, key_q},
								    {18, 22, 109, 0, 300*51 + 24, IMAGE_TARGET, SCRIPT__PIX_CMD_NOT_EQ, key_w},
									{18, 22, 109, 0, 300*51 + 24, IMAGE_TARGET, SCRIPT__PIX_CMD_NOT_EQ, key_none},
									{18, 22, 109, 0, 300*51 + 24, IMAGE_TARGET, SCRIPT__PIX_CMD_NOT_EQ, key_none},
};


static script__pix_t mob_full_hp = {18, 22, 109, 0, 300*51 + 208, IMAGE_TARGET, SCRIPT__PIX_CMD_EQUAL, key_e};

static script__pix_t my_low_hp = {16, 26, 119, 0, 160*63 + 28, IMAGE_HP, SCRIPT__PIX_CMD_NOT_EQ, key_tire};
static script__pix_t my_mid_hp = {16, 26, 118, 0, 160*63 + 82, IMAGE_HP, SCRIPT__PIX_CMD_NOT_EQ, key_slash};

static script__pix_t agr_use = {188, 107, 64, 0, 474*26 + 135, IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_tire};    // 135, 150
static script__pix_t stun_use = {105, 194, 221, 0, 474*23 + 174, IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_tire};  // 174, 153
static script__pix_t sound_use = {174, 178, 166, 0, 474*24 + 214, IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_tire}; // 214, 152
static script__pix_t mdmg_use = {65, 61, 195, 0, 474*26 + 254, IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_tire};    // 254, 150


static script__pix_t ramp_use = {225, 191, 139, 0, 474*30 + 295, IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_F8};
//static script__pix_t jump_use = {130, 102, 203, 0, 474*30 + 95, IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_e};

typedef enum {
	FARM__BIGIN = 0,
	FARM__TO_PET,
	FARM__WAIT_PET,
	FARM__FIND,
	FARM__FIND_WAIT,
	FARM__WAIT_HOOK,
	FARM__WAIT_AGR,
	FARM__AGR_DELAY,
	FARM__WAIT_STUN,
	FARM__WAIT_DMG,
	FARM__WAIT_DEAD,
	FARM__ALL_DEAD,
	FARM__END,
}farm__fsm_e;


static int fast_cp;

static script__t script_tbl[] = {
							    {VK_F1,   'Q', key_q, .ctrl = 1},
								{VK_F1,   'Q', key_1, .ctrl = 1},
							 	{VK_F2,   'W', key_w, .ctrl = 1},
							 	{VK_F3,   'E', key_e, .ctrl = 1},
							 	{VK_F3,   'E', key_3, .ctrl = 1},
							 	{VK_F12,  'S', key_none, 	.pix = (script__pix_t[]){
															{205, 255, 255, 0, 474*25 + 454,  IMAGE_SKILL, SCRIPT__PIX_CMD_EQUAL, key_pageup, .one_key = 1,}, // 454-151
															{30, 98, 131, 0, 160*77 + 82,  IMAGE_HP, SCRIPT__PIX_CMD_NOT_EQ, key_delete, .on_flag = &fast_cp}, // 82-32
															{.cmd = SCRIPT__PIX_CMD_NONE}}
								},
								{VK_F7,   'D', key_d,       (script__pix_t[]){
															{128,  61,   5, 0, 300*43 + 29,  IMAGE_TARGET, SCRIPT__PIX_CMD_NOT_EQ, key_slash},
															{.cmd = SCRIPT__PIX_CMD_NONE}}
								},
							 	{VK_F10,  '0', key_none,    (script__pix_t[]){
															{130, 226, 255, 0, 474*27 + 375,  IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_0},
															{130, 226, 255, 0, 474*27 + 375,  IMAGE_SKILL, SCRIPT__PIX_CMD_EQUAL, key_F10},
															{.cmd = SCRIPT__PIX_CMD_NONE}}
								},
							 	{VK_F11,  'O', key_o},
							 	{VK_F8,   'I', key_i,       (script__pix_t[]){
															{157, 145, 154, 0, 474*30 + 295,  IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_8},  // 295-146
															{.cmd = SCRIPT__PIX_CMD_NONE}}
								},
							 	{VK_F5,   'U', key_u,       (script__pix_t[]){
															{130, 226, 255, 0, 474*27 + 375,  IMAGE_BUFF, SCRIPT__PIX_CMD_FIND_BUFF, key_u},
															{.cmd = SCRIPT__PIX_CMD_NONE}}
								},
							 	{VK_F6,   'Y', key_y, 		(script__pix_t[]){
															{88, 127, 80, 0, 474*32 + 215,  IMAGE_SKILL, SCRIPT__PIX_CMD_NOT_EQ, key_6},  // 215-144
															{.cmd = SCRIPT__PIX_CMD_NONE}}
								},

								{0, 0, 0, 0}};

static int panelHandle;
static int event_close;
sw_timer__t key_tmr, button_tmr, farm_tmr, err_tmr, key_tmr, dead_tmr;
int test_key;
static int cur_image;
static int video_on;
int agr_time = 150;
int dead_time = 600;
static uint8_t block_keys;
static int img_ready;
static farm__fsm_e farm_fsm;
static int farm_timeout, all_dead_f;
static script__pix_t* cur_farm = mob_no_hp;
int mouse_block;
int buff_find;
HHOOK hook;
static sw_timer__t buff_tmr;
static int buff__timeout;
static int force_mode;



static int CVICALLBACK main_cout(void *functionId);

void SetNumLock( BOOL bState );
void init_coordinates(int image);
void key_tmr_cb(struct sw_timer__t *timer, void *ext_data);
void button_tmr_cb(struct sw_timer__t *timer, void *ext_data);
void image__scan(int idx);
static void script_fsm();
static int script_img_exec(script__pix_t* pix, uint8_t stop);
static void normal__exec();
static void farm__exec();
static void flower__exec();
static void dead_tmr_cb(struct sw_timer__t *timer, void *ext_data);
void farm_tmr_cb(struct sw_timer__t *timer, void *ext_data);
void pet_tmr_cb(struct sw_timer__t *timer, void *ext_data);
void err_tmr_cb(struct sw_timer__t *timer, void *ext_data);
void buff_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void dx9_create();
static mouse_point_t ConvertScreenPointToAbsolutePoint(int x, int y);
static int get_pix_num_buffs(int str_id, int buff_id, u8 x, u8 y);
static POINT get_xy_buff(int str_id, int buff_id);


LRESULT CALLBACK KeyTrap(int nCode, WPARAM wParam, LPARAM lParam) {
	KBDLLHOOKSTRUCT* key = (KBDLLHOOKSTRUCT*)lParam;
	if (key->vkCode)
	    return  CallNextHookEx(hook, nCode, wParam, lParam);
    return  CallNextHookEx(hook, nCode, wParam, lParam);
}

LRESULT CALLBACK MouseTrap(int nCode, WPARAM wParam, LPARAM lParam) {
	if (mouse_block)
	    return 1;
    return  CallNextHookEx(hook, nCode, wParam, lParam);
}

int CVICALLBACK windows_cb(int panelOrMenuBar, int controlOrMenuItem, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_KEYPRESS:
			break;
		case EVENT_CLOSE:
		case EVENT_END_TASK:
			return -1;
	}
	return 0;
}

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "capture.uir", PANEL)) < 0)
		return -1;
	sw_timer__init(NULL);

	CVI__BREAK_ERR_OFF
	RecallPanelState (panelHandle, "menu_settings.ini", 0);
	CVI__BREAK_ERR_ON

	init_coordinates(cur_image);
	int temp;
	GetCtrlVal(panelHandle, PANEL_SCRIPT, &temp);
	if (temp) {
		script_mode = SCRIPT_MODE_FARM;
		temp = 0;
		SetCtrlVal(panelHandle, PANEL_FLOWER, temp);
	}
	else {
		GetCtrlVal(panelHandle, PANEL_FLOWER, &temp);
		script_mode = temp ? SCRIPT_MODE_FLOWER : SCRIPT_MODE_NORMAL;
	}
	GetCtrlVal(panelHandle, PANEL_FAST_CP, &fast_cp);
	GetCtrlVal(panelHandle, PANEL_FORCE, &force_mode);
	GetCtrlVal(panelHandle, PANEL_SPOT_3, &temp);
	mob_no_hp[2].key = temp ? key_9 : key_none;
	GetCtrlVal(panelHandle, PANEL_TYR, &tyr_mode);

	for(int i = 0; i < IMAGE_CNT; i++) {
		image[i].handle = pixel__create(0, image[i].x,image[i].y, image[i].width, image[i].height);
	}
	img_test.handle = pixel__load("skill.bmp");

	HMODULE currentProcess = GetModuleHandle(NULL);
//    hook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyTrap, currentProcess, 0);
	hook = SetWindowsHookEx(WH_MOUSE_LL, MouseTrap, currentProcess, 0);

	DisplayPanel (panelHandle);
	main_cout(NULL);

	InstallMainCallback (windows_cb, NULL, 0);
	while(!event_close) {
		ProcessSystemEvents();
		Sleep(0);
	}
	main_cout(NULL);
	key_code__deinit();
	LockWindowUpdate (0);
	SavePanelState(panelHandle, "menu_settings.ini", 0);
	DiscardPanel (panelHandle);
	return 0;
}

void init_coordinates(int idx) {
	image_t* img = &image[idx];
	SetCtrlVal(panelHandle, PANEL_POS_X, img->x);
	SetCtrlVal(panelHandle, PANEL_POS_Y, img->y);
	SetCtrlVal(panelHandle, PANEL_SIZE_X, img->width);
	SetCtrlVal(panelHandle, PANEL_SIZE_Y, img->height);
}

void image__scan(int idx) {
	image_t* img = &image[idx];
	int x, y, height, width;
	GetCtrlVal(panelHandle, PANEL_POS_X, &x);
	GetCtrlVal(panelHandle, PANEL_POS_Y, &y);
	GetCtrlVal(panelHandle, PANEL_SIZE_X, &width);
	GetCtrlVal(panelHandle, PANEL_SIZE_Y, &height);
	if ((img->x != x) || (img->y != y) || (img->width != width) || (img->height != height)) {
		img->width = width;
		img->height = height;
		img->x = x;
		img->y = y;
		img->handle = pixel__create(img->handle, x, y, width, height);
	}
	pixel__scan(img->handle);
}

int CVICALLBACK quit_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			event_close = 1;
			break;
	}
	return 0;
}

int CVICALLBACK button_capture_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			sw_timer__start(&button_tmr, 1, button_tmr_cb, NULL);
			break;
	}
	return 0;
}

void button_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	char filename[100];
	image__scan(cur_image);
	sprintf(filename, "image%d.bmp", cur_image);
	pixel__save(image[cur_image].handle, filename);
	int bitmapID;
	GetBitmapFromFile (filename, &bitmapID);
	SetCtrlBitmap (panelHandle, PANEL_PICTURE, 0, bitmapID);

}

void key_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	sw_timer__start(timer, -1, key_tmr_cb, NULL);
	if (video_on) {
		 button_tmr_cb(NULL, NULL);
	}
	for(int i = 0; i < IMAGE_CNT; i++)
		pixel__scan(image[i].handle);
	img_ready = 1;

//	pixel__draw(img_test.handle, 0, 0);

}


static int CVICALLBACK main_cout(void *functionId) {
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(100), ProcessSystemEvents());
		return 0;
	}
	key_code__init(5);
	sw_timer__start(&key_tmr, 50, key_tmr_cb, NULL);
	while(*(int*)functionId)
		Sleep(1), framework__cout(), script_fsm();
	*(int*)functionId = -1;
	return 0;
}


int CVICALLBACK img_change_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &cur_image);
			init_coordinates(cur_image);
			break;
	}
	return 0;
}

int CVICALLBACK video_ctrl_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &video_on);
			break;
	}
	return 0;
}

#include "d3d9.h"
#include "detours.h"


IDirect3DDevice9* videocard = NULL; // устройство (видеокарта)
IDirect3DDevice9* dev_hook;
ID3DXFont* font;
RECT rect;
char str[11] = "fps:  0000";
HDC screen;

HWND find_window() {
	HWND WindowA; // eax
	WindowA = FindWindowA(0, "DesktopOverlayHost");
	return WindowA;
}

u8 dataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask) {
	for (; *szMask; ++szMask, ++pData, ++bMask)
	if (*szMask == 'x' && *pData != *bMask)
		return FALSE;
	return (*szMask) == NULL;
}

DWORD findPattern(DWORD addr, DWORD len, BYTE *bMask, char * szMask) {
	for (DWORD i = 0; i < len; i++)
	if (dataCompare((BYTE*) (addr + i), bMask, szMask))
		return (DWORD) (addr + i);
	return 0;
}

HRESULT (STDMETHODCALLTYPE *RealPresent)(IDirect3DDevice9 *This, CONST RECT *pSourceRect, CONST RECT *pDestRect, HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion) = NULL;

HRESULT STDMETHODCALLTYPE MyPresent(IDirect3DDevice9 *This, CONST RECT *pSourceRect, CONST RECT *pDestRect, HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion)
{
  static u8 bInit = TRUE;
  if (bInit) {
    bInit = FALSE;
  }

  //Renderer::Get ()->Render();

  HRESULT hr = RealPresent(This, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);

  return hr;
}



static void dx9_create() {

	HMODULE hMod = NULL;

	hMod = GetModuleHandle("d3d9.dll");

//	DWORD *vtbl = 0;

	DWORD dwDevice = findPattern((DWORD) hMod, 0x128000, (PBYTE)"\xC7\x06\x00\x00\x00\x00\x89\x86\x00\x00\x00\x00\x89\x86", "xx????xx????xx");
	memcpy(&dev_hook, (void *) (dwDevice + 0x2), 4);

	RealPresent = dev_hook->lpVtbl->Present;

	DetourTransactionBegin();
	DetourUpdateThread (GetCurrentThread());
	DetourAttach((PVOID*)&dev_hook->lpVtbl->Present, MyPresent);
	DetourTransactionCommit();


	dev_hook->lpVtbl->Present = MyPresent;

	IDirect3D9* d3d = NULL;       // объект Direct3D

  	d3d = Direct3DCreate9(D3D_SDK_VERSION);

	D3DPRESENT_PARAMETERS pp;
	ZeroMemory(&pp,sizeof(pp));

	HWND hwnd;
	screen = GetDC(NULL);
	hwnd = WindowFromDC(screen);

	pp.Windowed				    = TRUE;
	pp.SwapEffect				= D3DSWAPEFFECT_COPY;
	pp.BackBufferFormat		    = D3DFMT_UNKNOWN;
	pp.EnableAutoDepthStencil	= TRUE;
	pp.AutoDepthStencilFormat	= D3DFMT_D16;
	pp.PresentationInterval	    = D3DPRESENT_INTERVAL_IMMEDIATE;
	pp.MultiSampleType			= D3DMULTISAMPLE_NONE;
    pp.BackBufferWidth          = 500;
    pp.BackBufferHeight 		= 500;

  	d3d->lpVtbl->CreateDevice(d3d, D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &pp, &videocard);

	D3DXFONT_DESC fontDesc;
	ZeroMemory(&fontDesc,sizeof(D3DXFONT_DESC));
	fontDesc.Height = 18;
	fontDesc.Width = 0;    // ширина задаётся автоматически, в зависмости от высоты
	fontDesc.Weight = 400; // 400 - стандартная толщина шрифта
	fontDesc.MipLevels = 0;
	fontDesc.Italic = 0;
	fontDesc.CharSet = RUSSIAN_CHARSET;
	fontDesc.OutputPrecision = OUT_DEFAULT_PRECIS;
	fontDesc.Quality = ANTIALIASED_QUALITY;   // сглаженный шрифт
	fontDesc.PitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	wchar_t FaceName[] = L"Times New Roman"; // можно подставить Times New Roman, Serif, Arial или любой др.
	memcpy(&fontDesc.FaceName,&FaceName,sizeof(FaceName));

	D3DXCreateFontIndirect(videocard,&fontDesc,&font);
	rect.top = 10;
	rect.left = 10;
	rect.bottom = 100;
	rect.right = 100;
	font->lpVtbl->DrawText(font, NULL, str, 10, &rect, DT_TOP | DT_LEFT, 0xffFF0000);
}


static void script_fsm() {
//	BYTE lpKeyState[256];
//	GetKeyboardState(lpKeyState);
/*
	static int init = 0;
	if (!init) {
		init = 1;
		dx9_create();
	}
	HWND test_wnd = find_window();
	if (test_wnd) {
		screen = GetDC(test_wnd);
	}

	SetBkMode( screen, TRANSPARENT );
	DrawText(screen, "Hello, World!", -1, &rect, DT_TOP | DT_LEFT);
	font->lpVtbl->DrawText(font, NULL, str, 10, &rect, DT_TOP | DT_LEFT, 0xffFF0000);
	videocard->lpVtbl->Present(videocard, NULL, NULL, NULL, NULL);

	pixel__draw(img_test.handle, 0, 0);

*/
	memcpy(&old_keys, &keys, sizeof(old_keys));
	switch(script_mode) {
		case SCRIPT_MODE_NORMAL:
			normal__exec();
			break;
		case SCRIPT_MODE_FARM:
			farm__exec();
			break;
		case SCRIPT_MODE_FLOWER:
			flower__exec();
			break;
	}
	int chk_hp;
	GetCtrlVal(panelHandle, PANEL_LOW_HP, &chk_hp);
	if (chk_hp) {
		if (GetKeyState(VK_F12) < 0) {
			if (script_img_exec(&my_low_hp, 0)) {
				keys.key[6] = my_low_hp.key;
			}
			else if (script_img_exec(&my_mid_hp, 0)) {
				keys.key[6] = my_mid_hp.key;
				keys.key[7] = key_d;
			}
			else {
				keys.key[6] = key_none;
				keys.key[7] = key_none;
			}
		}
	}
	if (memcmp(&old_keys, &keys, sizeof(old_keys))) {
		if (key_code__set(&keys) != EVENT__OK)
			memcpy(&keys, &old_keys, sizeof(old_keys));
	}
}
void buff_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	buff__timeout = 1;
}

static pixel__t b_stone[3] = {
	[0] = {
		.y = 255,
		.r = 27,
		.g = 23,
		.b = 23,
	},
	[1] = {
		.y = 255,
		.r = 77,
		.g = 68,
		.b = 60,
	},
	[2] = {
		.y = 255,
		.r = 139,
		.g = 138,
		.b = 177,
	},
};

#define B_SNOTE_NUM (sizeof(b_stone) / sizeof(b_stone[0]))
#define B_STONE_X 14
#define B_STONE_Y 7
#define BUFF_ACC   2
#define BUFF_GYST  5
#define BUFF_TIMEOUT 1500

static int script_img_exec(script__pix_t* pix, uint8_t stop) {
	static int check_stone;
	int res = 1;
	int str_id, buff_idx = -1;

	if (!pix)
		return res;
	pixel__t cur_pix = pixel__get_color(image[pix->img_num].handle, pix->pixel_num);
	switch(pix->cmd) {
		case SCRIPT__PIX_CMD_EQUAL:
			if (pixel__cmp(&pix->color, &cur_pix, 2, 0))
				res = 0;
			break;
		case SCRIPT__PIX_CMD_NOT_EQ:
			if (!pixel__cmp(&pix->color, &cur_pix, 2, 0))
				res = 0;
			break;
		case SCRIPT__PIX_CMD_FIND_BUFF:
			block_keys = 1;
			memset(&keys,0, sizeof(keys));
			keys.key[0] = pix->key;

			if ((!stop) || (!img_ready))
				break;
			img_ready = 0;

			pixel__t buff_pix[B_SNOTE_NUM];
			int buff_pix_idx;
			POINT buff_xy, cur_xy;
			for(str_id = 1; str_id >= 0; str_id--) {
				for(int buff_id = 11; buff_id >= 0; buff_id--) {
					buff_pix_idx = get_pix_num_buffs(str_id, buff_id, B_STONE_X, B_STONE_Y);
					buff_pix[0] = pixel__get_color(image[IMAGE_BUFF].handle, buff_pix_idx);
					buff_pix[1] = pixel__get_color(image[IMAGE_BUFF].handle, buff_pix_idx + 1);
					buff_pix[2] = pixel__get_color(image[IMAGE_BUFF].handle, buff_pix_idx + 2);
					if ((pixel__cmp(&buff_pix[0], &b_stone[0], BUFF_ACC, BUFF_GYST)) || (pixel__cmp(&buff_pix[1], &b_stone[1], BUFF_ACC, BUFF_GYST)) || (pixel__cmp(&buff_pix[2], &b_stone[2], BUFF_ACC, BUFF_GYST)))
						continue;
					buff_idx = buff_id;
					break;
				}
				if (buff_idx >= 0)
					break;
			}

			if (buff_idx >= 0) {
				buff_xy = get_xy_buff(str_id, buff_idx);
				GetCursorPos(&cur_xy);

				mouse_block = 1;
				if (keys.modifier.l_alt != 1) {
					memset(&keys,0, sizeof(keys));
					keys.modifier.l_alt = 1;
					key_code__set(&keys);
				}
				SetCursorPos(buff_xy.x-check_stone, buff_xy.y+check_stone);
				Sleep(50);
				mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
				Sleep(50);
	    		mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
				Sleep(50);
				SetCursorPos(cur_xy.x, cur_xy.y);
				mouse_block = 0;
			}
			else if (!buff__timeout)
				break;

			memset(&keys,0, sizeof(keys));
			key_code__set(&keys);
			block_keys = 0;
			res = 0;
			break;
		default:
			break;
	}
	return res;
}

int CVICALLBACK script_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)
		return 0;
	int temp = 0;
	switch(control) {
		case PANEL_SCRIPT:
			SetCtrlVal(panelHandle, PANEL_FLOWER, temp);
			GetCtrlVal(panelHandle, PANEL_SCRIPT, &temp);
			SetCtrlVal(panelHandle, PANEL_LOW_HP, temp);
			break;
		case PANEL_FLOWER:
			SetCtrlVal(panelHandle, PANEL_SCRIPT, temp);
			GetCtrlVal(panelHandle, PANEL_FLOWER, &temp);
			SetCtrlVal(panelHandle, PANEL_LOW_HP, temp);
			temp *= 2;
			break;
	}
	script_mode = temp;
	return 0;
}

static void normal__exec() {
	script__pix_t* cur_pix;
	uint8_t idx = 0;
	uint8_t use_ctrl = 0;
	uint8_t one_key = key_none;
	uint8_t one_idx = 0;
	for (script__t* script = script_tbl; script->start_key; script++) {
		if (GetKeyState(script->start_key) < 0)
			script->start = 1;
		else
			script->start = 0;
		cur_pix = script->pix;
		switch(script->state) {
			case SCRIPT__IDLE:
				if (!script->start)
					break;
				script->state = SCRIPT__WORK;
			case SCRIPT__WORK:
				if (script->start) {
					if (!block_keys) {
						keys.key[idx] = script->key;
						if ((script->ctrl) && (force_mode))
							use_ctrl = 1;
						if (cur_pix) {
							while(cur_pix->cmd != SCRIPT__PIX_CMD_NONE) {
								if (((!cur_pix->on_flag) || ((cur_pix->on_flag) && (*cur_pix->on_flag != 0))) && (script_img_exec(cur_pix, 0))) {
									keys.key[idx] = cur_pix->key;
									if (cur_pix->one_key) {
										one_key = cur_pix->key;
										one_idx = idx;
									}
									break;
								}
								cur_pix++;
							}
						}
					}
					break;
				}
				script->state = SCRIPT__STOP;
				if (script->pix) {
					for(cur_pix = script->pix; (cur_pix->cmd != SCRIPT__PIX_CMD_NONE) && (cur_pix->cmd != SCRIPT__PIX_CMD_FIND_BUFF); cur_pix++) {}
					if(cur_pix->cmd == SCRIPT__PIX_CMD_FIND_BUFF) {
						buff__timeout = 0;
						sw_timer__start(&buff_tmr, BUFF_TIMEOUT, buff_tmr_cb, NULL);
					}
				}
			case SCRIPT__STOP:
				keys.key[idx] = key_none;
				if (script->pix) {
					for(cur_pix = script->pix; (cur_pix->cmd != SCRIPT__PIX_CMD_NONE) && (cur_pix->cmd != SCRIPT__PIX_CMD_FIND_BUFF); cur_pix++) {}
					if(cur_pix->cmd == SCRIPT__PIX_CMD_FIND_BUFF) {
						if (script_img_exec(cur_pix, 1))
							break;
						sw_timer__stop(&buff_tmr);
					}
				}
				script->state = SCRIPT__IDLE;
				break;
		}
		idx++;
	}
	if (force_mode)
		keys.modifier.l_ctrl = use_ctrl;
	if (one_key != key_none) {
		memset(&keys, 0, sizeof(keys));
		keys.key[one_idx] = one_key;
	}
}

static void farm__exec() {
	int pet_time, agr_always;
	if (GetKeyState(VK_F12) >= 0) {
		if (farm_fsm != FARM__BIGIN) {
			memset(&keys,0, sizeof(keys));
			if (key_code__set(&keys) == EVENT__OK)
				farm_fsm = FARM__BIGIN;
		}
		normal__exec();
		return;
	}
//	if (!img_ready)
//		return;
//	img_ready = 0;
	GetCtrlVal(panelHandle, PANEL_AGR_ALWAYS, &agr_always);
	GetCtrlVal(panelHandle, PANEL_TIME_PET, &pet_time);
	GetCtrlVal(panelHandle, PANEL_AGR_TIME, &agr_time);
	GetCtrlVal(panelHandle, PANEL_DEAD_TIME, &dead_time);

	keys.key[11] = key_pageup;
	switch(farm_fsm) {
		case FARM__BIGIN:
			cur_farm = mob_no_hp;
			memset(&keys,0, sizeof(keys));
			keys.key[0] = cur_farm->key;
			keys.key[1] = key_tab;
			farm_fsm = pet_time ? FARM__TO_PET : FARM__FIND;
			break;
		case FARM__TO_PET:
			keys.key[0] = cur_farm->key;
			keys.key[1] = key_tab;
			farm_timeout = 0;
			sw_timer__start(&farm_tmr, pet_time * 1000, pet_tmr_cb, NULL);
			sw_timer__start(&err_tmr, 120000, err_tmr_cb, NULL);
			farm_fsm = FARM__WAIT_PET;
			break;
		case FARM__WAIT_PET:
			if (!farm_timeout)
				break;
			farm_fsm = FARM__FIND;
			break;
		case FARM__FIND:
			keys.key[0] = key_o;
			keys.key[1] = key_tab;
			farm_timeout = 0;
			sw_timer__start(&farm_tmr, 150, pet_tmr_cb, NULL);
			farm_fsm = FARM__FIND_WAIT;
			break;
		case FARM__FIND_WAIT:
			if ((!farm_timeout) || (script_img_exec(cur_farm, 0)))
				break;
			keys.key[0] = pet_time ? mob_full_hp.key : key_3;
			farm_timeout = 0;
			sw_timer__start(&farm_tmr, 150, pet_tmr_cb, NULL);
			farm_fsm = FARM__WAIT_HOOK;
		case FARM__WAIT_HOOK:
			if ((!farm_timeout) || (script_img_exec(&mob_full_hp, 0)))
				break;
			if (tyr_mode) {
				keys.key[0] = key_F8;
				farm_fsm = FARM__WAIT_AGR;
			}
			else {
				keys.key[0] = key_d;
				farm_fsm = FARM__WAIT_DEAD;
			}
			break;
		case FARM__WAIT_AGR:
			if (!script_img_exec(&agr_use, 0))
				break;
			keys.key[1] = key_F8;
			keys.key[2] = key_F9;
			farm_timeout = 0;
			sw_timer__start(&farm_tmr, agr_time, pet_tmr_cb, NULL);
			farm_fsm = FARM__AGR_DELAY;
			break;
		case FARM__AGR_DELAY:
			if (!farm_timeout)
				break;
			keys.key[0] = key_u;
			keys.key[1] = key_F10;
			keys.key[2] = key_F8;
			keys.key[3] = key_F9;
			farm_fsm = FARM__WAIT_STUN;
			break;
		case FARM__WAIT_STUN:
			if (!script_img_exec(&stun_use, 0))
				break;
			keys.key[0] = key_y;
			keys.key[1] = key_F10;
			farm_timeout = 0;
			sw_timer__start(&farm_tmr, 5000, pet_tmr_cb, NULL);
			farm_fsm = FARM__WAIT_DMG;
			break;
		case FARM__WAIT_DMG:
			if ((!farm_timeout) && (!script_img_exec(&sound_use, 0)))
				break;
			keys.key[0] = key_d;
			farm_fsm = FARM__WAIT_DEAD;
			break;
		case FARM__WAIT_DEAD:
			if (!script_img_exec(&mdmg_use, 0))
				break;
			keys.key[0] = key_none;
			keys.key[1] = key_u;
			keys.key[2] = key_y;
			keys.key[3] = key_d;
			keys.key[4] = key_F8;
			keys.key[5] = key_F9;
			keys.key[8] = key_F10;
			all_dead_f = 0;
			sw_timer__start(&dead_tmr, dead_time, dead_tmr_cb, NULL);
			farm_fsm = FARM__ALL_DEAD;
			break;
		case FARM__ALL_DEAD:
			if (agr_always) {
				if (script_img_exec(&stun_use, 0) && script_img_exec(&sound_use, 0) && script_img_exec(&mdmg_use, 0))
					keys.key[0] = key_F8;
				else
					keys.key[0] = key_none;
			}
			if (!script_img_exec(mob_no_hp, 0)) {
				all_dead_f = 0;
				sw_timer__start(&dead_tmr, dead_time, dead_tmr_cb, NULL);
			}
			if (!all_dead_f) {
				break;
			}
			farm_fsm = FARM__END;
		case FARM__END:
			cur_farm++;
			if (cur_farm->key == key_none)
				cur_farm = mob_no_hp;
			memset(&keys,0, sizeof(keys));
			farm_fsm = pet_time ? FARM__TO_PET : FARM__FIND;
			break;
	}
}

static void flower__exec() {
    if (GetKeyState(VK_F12) >= 0) {
		if (farm_fsm != FARM__BIGIN) {
			memset(&keys,0, sizeof(keys));
			if (key_code__set(&keys) == EVENT__OK)
				farm_fsm = FARM__BIGIN;
		}
		normal__exec();
		return;
	}
	switch(farm_fsm) {
		case FARM__BIGIN:
			keys.modifier.l_ctrl = 0;
			keys.key[0] = key_q;
			keys.key[1] = key_w;
			keys.key[2] = key_e;
			keys.key[3] = key_F8;
			keys.key[4] = key_u;
			keys.key[5] = key_y;
			keys.key[8] = key_F8;
			keys.key[9] = key_F9;

			keys.key[11] = key_pageup;
			key_code__set(&keys);
			farm_fsm = FARM__FIND_WAIT;
			break;
		case FARM__FIND_WAIT:
			break;
	}

}


void pet_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	farm_timeout = 1;
}

static void dead_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	all_dead_f = 1;
}



void err_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	memset(&keys,0, sizeof(keys));
	if (key_code__set(&keys) != EVENT__OK)
			key_code__set(&keys);
	farm_fsm = FARM__BIGIN;
}


int CVICALLBACK tyr_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panelHandle, PANEL_TYR, &tyr_mode);
			break;
	}
	return 0;
}

int CVICALLBACK three_spot_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int spot3;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panelHandle, PANEL_SPOT_3, &spot3);
			mob_no_hp[2].key = spot3 ? key_9 : key_none;
			break;
	}
	return 0;
}

static mouse_point_t ConvertScreenPointToAbsolutePoint(int x, int y) {
    int screenMaxWidth = GetSystemMetrics(SM_CXSCREEN) - 1;
    int screenMaxHeight = GetSystemMetrics(SM_CYSCREEN) - 1;
	mouse_point_t res;
    res.x = x * (65535.0f / screenMaxWidth);
    res.y = y * (65535.0f / screenMaxHeight);
    return res;
}

static int get_pix_num_buffs(int str_id, int buff_id, u8 x, u8 y) {
	int str_len = 26 * 12;
	x += buff_id * 26;
	int res = ((2 - str_id) * 26 + 25 - y) * str_len + x;
	return res;
}

static POINT get_xy_buff(int str_id, int buff_id) {
	POINT res;
	res.x = 189 + 13 + buff_id * 26;
	res.y = 0 + 13 + str_id * 26;
	return res;
}

int CVICALLBACK force_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &force_mode);
			break;
	}
	return 0;
}

int CVICALLBACK fast_cp_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &fast_cp);
			break;
	}
	return 0;
}
