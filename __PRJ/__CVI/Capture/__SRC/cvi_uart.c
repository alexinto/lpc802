#include <rs232.h>
#include <ansi_c.h>
#include <utility.h>
#include "target.h"
#include "cvi_uart.h"

typedef struct {
	u8 init;
	uint32_t len, next_len;
	char buff[UART_BUFF], next_buff[UART_BUFF];
	uart_state_e next_state, cur_state;
	unsigned int uart_com, uart_com_next;
	uart__cb cb;
}uart_struct_t;

static uart_struct_t uart;

static void uart__close(int uart_id);


events__e uart__init(int uart_id, uart__cb cb) {
	int res;
	char comport_str[10];
	if (cb) {
		uart.uart_com_next = uart_id;
		uart.cb = cb;
		return EVENT__OK;
	}
	if (uart.init)
		return EVENT__OK;
	uart.init = 1;
	sprintf(comport_str, "COM%d", uart_id);
	res = OpenComConfig (uart_id, comport_str, 115200, 0, 8, 1, 512, 512);
	SetComTime (uart_id, UART_TIMEOUT);
	if (res)
		return EVENT__ERROR;
	return EVENT__OK;
}

static void uart__close(int uart_id) {
	uart.init = 0;
	CloseCom(uart_id);
}

events__e uart__cout(uart_state_e state, u8 len, char* buff) {
	events__e res = EVENT__OK;
	DisableBreakOnLibraryErrors ();
	if (state) {
		CRITICAL_SECTION_ON
		if ((uart.next_state) && (state != UART__INIT) && (state != UART__DEINIT))
			res = EVENT__BUSY;
		else {
			uart.next_state = state;
			uart.next_len = len;
			if ((state == UART__TX) && (len <= UART_BUFF))
				memcpy(uart.next_buff, buff, len);
		}
		CRITICAL_SECTION_OFF
	}
	else switch(uart.cur_state) {
		case UART__TX:
			uart.uart_com = uart.uart_com_next;
			if (uart__init(uart.uart_com, NULL) != EVENT__OK) {
				uart__close(uart.uart_com);
				uart.cur_state = UART__IDLE;
				uart.cb(EVENT__ERROR, (uint8_t*)uart.buff, uart.len);
				return EVENT__ERROR;
			}
			ComWrt(uart.uart_com, uart.buff, uart.len);
			uart.cur_state = UART__RX;
		case UART__RX:
			uart.len = res = ComRd(uart.uart_com, uart.buff, UART_BUFF);
			if (res > 0) {
				res = EVENT__OK;
			}
			else if (res == 0)
				res = EVENT__TIMEOUT;
			else
				res = EVENT__ERROR;
			uart.cur_state = UART__IDLE;
			uart.cb(res, (uint8_t*)uart.buff, uart.len);
			if (uart.next_state != UART__RX)
				uart__close(uart.uart_com);
			break;
		case UART__DEINIT:
			if (uart.next_state == UART__INIT)
				uart.cur_state = UART__IDLE;
			break;
		case UART__IDLE:
		CRITICAL_SECTION_ON
			uart.cur_state = uart.next_state;
			uart.len = uart.next_len;
			memcpy(uart.buff, uart.next_buff, uart.len);
			uart.next_state = UART__IDLE;
		CRITICAL_SECTION_OFF
			break;
		default:
			uart.cur_state = UART__IDLE;
			break;
	}
	EnableBreakOnLibraryErrors ();
	return res;
}

void uart__deinit() {
	while(uart.cur_state != UART__DEINIT)
		uart__cout(UART__DEINIT, 0, NULL);
}
