﻿#include <rs232.h>
#include <windows.h>
#include <cvirte.h>
#include <userint.h>
#include <time.h>
#include "toolbox.h"
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/crc.h"
#include "DRV/uart.h"
#include "termodiag.h"


typedef struct {
	uint8_t com_port;

	uart__sub_t uart;
}termodiag__t;

static termodiag__t data;
static int panelHandle;
static sw_timer__t tmr;

static int CVICALLBACK main_cout(void *functionId);
static void com_ports_check(int panel, int control);
static void TimerCallback (struct sw_timer__t *timer, void *callbackData);
static void set_default_panel();
static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data);

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "termodiag.uir", PANEL)) < 0)
		return -1;
	main_cout(NULL);            // Инициализация потока работы framework
	com_ports_check(panelHandle, PANEL_COM_CHANGE);
	set_default_panel();
	GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &data.com_port);


	DisplayPanel (panelHandle);
	RunUserInterface();

	main_cout(NULL);
	DiscardPanel (panelHandle);
	return 0;
}

int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		QuitUserInterface(0);
	return 0;
}

int CVICALLBACK com_renew_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		com_ports_check(panel, PANEL_COM_CHANGE);
	return 0;
}

static void TimerCallback (struct sw_timer__t *timer, void *callbackData) {
	sw_timer__start(timer, -1, TimerCallback, callbackData);
}

static int CVICALLBACK main_cout(void *functionId) {
    volatile static int static__functionId = -1;
    if(functionId == NULL) {        // Start/Stop
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(1), ProcessSystemEvents());
        return 0;
    }
	SetBreakOnLibraryErrors(0);
	sw_timer__init(NULL);
	uart__init();
	sw_timer__start(&tmr, 1000, TimerCallback, (void*)panelHandle);
	uart__open(&data.uart, 0, 0, uart_event_hd, NULL);

    while(*(int*)functionId) {
        Sleep(1); framework__cout();
	}
	sw_timer__stop(&tmr);
//	uart__close(&data.uart);
    *(int*)functionId = -1;
    return 0;
}

static void com_ports_check(int panel, int control) {
    int com = -1;
	char name[24];
	ClearListCtrl (panel, control);
	while(++com <= 255)
    {
        char ncom[24];
        sprintf(ncom, "\\\\.\\COM%d", com);
        HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
        uint32_t error = 0;
        if(hport == INVALID_HANDLE_VALUE)
            switch(error = GetLastError())
            {
                case 5: break;
                default: continue;
            }
        else
            CloseHandle(hport);
        sprintf(name, "COM %d%s", com, error != 5? "": " (занят)");
		InsertListItem (panel, control, -1, name, com);
    }
	sprintf(name, "COM %d%s", data.com_port, " !");
	InsertListItem (panel, control, -1, name, data.com_port);
}

static void set_default_panel() {
	SetCtrlVal (panelHandle, PANEL_COM_CHANGE, data.com_port);
}

int CVICALLBACK com_port_change (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)
		return 0;
	GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &data.com_port);
//	uart__init(data.com_port, termodiag_itf_cb);
	return 0;
}

int CVICALLBACK set_itf_timeout_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_VAL_CHANGED) {
	}
	return 0;
}

int CVICALLBACK log_clear_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		ResetTextBox (panelHandle, PANEL_TEXTBOX, "");
	return 0;
}

int CVICALLBACK termodiag_ctrl_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT) {
	}
	return 0;
}


static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
	switch(event) {
		case EVENT__OPEN:
			break;
		case EVENT__CLOSE:
			break;
	}
}