/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_LOG_CLEAR                  2       /* control type: command, callback function: log_clear_cb */
#define  PANEL_COM_RENEW                  3       /* control type: command, callback function: com_renew_cb */
#define  PANEL_QUITBUTTON                 4       /* control type: command, callback function: QuitCallback */
#define  PANEL_DECORATION_5               5       /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_10              6       /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_8               7       /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_3               8       /* control type: deco, callback function: (none) */
#define  PANEL_TEXTBOX                    9       /* control type: textBox, callback function: (none) */
#define  PANEL_TIME_PROGRAM               10      /* control type: string, callback function: (none) */
#define  PANEL_COM_CHANGE                 11      /* control type: ring, callback function: com_port_change */
#define  PANEL_GRAPH                      12      /* control type: graph, callback function: (none) */
#define  PANEL_TEXTMSG_8                  13      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG                    14      /* control type: textMsg, callback function: (none) */
#define  PANEL_U_PWR                      15      /* control type: textMsg, callback function: (none) */
#define  PANEL_U_OUT                      16      /* control type: textMsg, callback function: (none) */
#define  PANEL_ITF_TIMEOUT                17      /* control type: scale, callback function: set_itf_timeout_cb */
#define  PANEL_DECORATION_9               18      /* control type: deco, callback function: (none) */
#define  PANEL_U_GND                      19      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_10                 20      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_2                  21      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_9                  22      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_7                  23      /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK com_port_change(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_renew_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK log_clear_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK set_itf_timeout_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif