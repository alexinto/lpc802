<distribution version="15.0.0" name="termodiag" type="MSI">
	<prebuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></prebuild>
	<postbuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></postbuild>
	<msi GUID="{AADCD1FC-40B0-45F0-935D-554BDF58CA66}">
		<general appName="termodiag" outputLocation="w:\M_PRJ\__PRJ\__CVI\termodiag\cvidistkit.termodiag" relOutputLocation="cvidistkit.termodiag" outputLocationWithVars="w:\M_PRJ\__PRJ\__CVI\termodiag\cvidistkit.%name" relOutputLocationWithVars="cvidistkit.%name" upgradeBehavior="1" autoIncrement="true" version="1.0.13">
			<arp company="Kurchatovskii" companyURL="" supportURL="" contact="" phone="" comments=""/>
			<summary title="termodiag" subject="" keyWords="" comments="" author="Alex"/></general>
		<userinterface language="English" showPaths="false" showRuntimeOnly="true" readMe="" license="">
			<dlgstrings welcomeTitle="termodiag" welcomeText=""/></userinterface>
		<dirs appDirID="100">
			<installDir name="[Start&gt;&gt;Programs]" dirID="7" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="[Program Files]" dirID="2" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="termodiag" dirID="100" parentID="2" isMSIDir="false" visible="true" unlock="false"/>
			<installDir name="termodiag" dirID="101" parentID="7" isMSIDir="false" visible="true" unlock="false"/>
			<installDir name="[Desktop]" dirID="0" parentID="-1" isMSIDir="true" visible="true" unlock="false"/></dirs>
		<files>
			<simpleFile fileID="0" sourcePath="C:\Windows\SYSTEM32\ADVAPI32.dll" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="1" sourcePath="C:\Windows\SYSTEM32\USER32.dll" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="2" sourcePath="C:\Windows\SYSTEM32\KERNEL32.dll" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="3" sourcePath="w:\M_PRJ\__PRJ\__CVI\termodiag\cvibuild.termodiag\Release\termodiag.exe" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="4" sourcePath="w:\M_PRJ\__PRJ\__CVI\termodiag\termodiag.uir" relSourcePath="termodiag.uir" relSourceBase="0" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/></files>
		<fileGroups>
			<projectOutput targetType="0" dirID="100" projectID="0">
				<fileID>3</fileID></projectOutput>
			<projectDependencies dirID="100" projectID="0">
				<fileID>2</fileID>
				<fileID>0</fileID>
				<fileID>1</fileID></projectDependencies></fileGroups>
		<shortcuts>
			<shortcut name="termodiag" targetFileID="3" destDirID="0" cmdLineArgs="" description="" runStyle="NORMAL"/></shortcuts>
		<mergemodules/>
		<products/>
		<runtimeEngine installToAppDir="false" activeXsup="true" analysis="true" cvirte="true" dotnetsup="true" instrsup="true" lowlevelsup="true" lvrt="true" netvarsup="true" rtutilsup="true">
			<hasSoftDeps/></runtimeEngine><sxsRuntimeEngine>
			<selected>false</selected>
			<doNotAutoSelect>false</doNotAutoSelect></sxsRuntimeEngine>
		<advanced mediaSize="650">
			<launchConditions>
				<condition>MINOS_WINXP</condition>
			</launchConditions>
			<includeConfigProducts>true</includeConfigProducts>
			<maxImportVisible>silent</maxImportVisible>
			<maxImportMode>merge</maxImportMode>
			<custMsgFlag>false</custMsgFlag>
			<custMsgPath>msgrte.txt</custMsgPath>
			<signExe>true</signExe>
			<certificate></certificate>
			<signTimeURL></signTimeURL>
			<signDescURL></signDescURL></advanced>
		<baselineProducts>
			<product name="NI LabWindows/CVI Shared Runtime 2015" UC="{80D3D303-75B9-4607-9312-E5FC68E5BFD2}" productID="{C123DBC1-8C63-4369-A052-EE7D3922EAC9}" path="(None)" flavorID="_full_" flavorName="Full" verRestr="false" coreVer="15.0.408">
				<dependencies>
					<productID>{0416C950-A8C6-4CFE-B206-A8D28091A40B}</productID>
					<productID>{08505CC2-EA7F-4818-9C45-B74EDA7227F8}</productID>
					<productID>{10A1BCD4-CF1D-4198-B037-77FB1AEFF5FE}</productID>
					<productID>{1FEB2C02-C272-4BE7-B7D5-FFAC604291C5}</productID>
					<productID>{20124E21-206B-485F-838F-14BB88161045}</productID>
					<productID>{21B81C08-8854-4C2D-99E2-6CECA2D468A8}</productID>
					<productID>{22F17C52-A282-4383-8E5C-5C38B41DD605}</productID>
					<productID>{22F31177-33F5-4BB5-872D-B43459113AB1}</productID>
					<productID>{340ADF5D-0206-47CC-9F45-D05E324EF415}</productID>
					<productID>{347A3F28-ABCD-4321-8954-9FA35527895A}</productID>
					<productID>{37C0159A-E5E2-4688-9360-0435AE0E55C7}</productID>
					<productID>{5E207DE6-086F-4548-9052-0DCCD3480C6A}</productID>
					<productID>{658224F9-1A2B-4E18-B00F-3F969C9EE080}</productID>
					<productID>{72B959AA-19D8-47E5-91C2-ECA967017494}</productID>
					<productID>{78E56459-2292-4BDF-876E-F4EF35298A5B}</productID>
					<productID>{7F93F26A-E5F7-4AE1-840F-F88DFE2DE3A5}</productID>
					<productID>{87E698D6-02AC-485E-A6BA-9194C94CC547}</productID>
					<productID>{A64EEEED-6B8D-4369-AEDB-A65ABC3D5C9C}</productID>
					<productID>{B3B56C15-80A8-4972-90CB-D80E64B3F39C}</productID>
					<productID>{C1F1FD0A-D051-4CA7-AAF0-2E9C0A881054}</productID>
					<productID>{C23121A8-AF17-4C1D-A2B4-A6DE7A156DCF}</productID>
					<productID>{C67371A2-E288-429E-A2B6-85D36B29BF8E}</productID>
					<productID>{CE9BC910-D93D-4083-93C7-0827EDAABC68}</productID>
					<productID>{DD45527C-36EF-489D-AA4E-0F7673DC8A59}</productID>
					<productID>{E84997A1-4D6F-4C0B-B60D-F85B360D2666}</productID>
					<productID>{ECB572E6-5CE3-4E9E-B1B3-16A00E02153A}</productID>
					<productID>{F2273FA7-117C-43D7-BD59-00B025535442}</productID>
					<productID>{F51098A7-A5A9-4CED-AFB4-4EC8E8117A35}</productID>
					<productID>{F8ABBE50-B96B-4339-B227-A7D0985F223F}</productID></dependencies></product></baselineProducts>
		<Projects NumProjects="1">
			<Project000 ProjectID="0" ProjectAbsolutePath="w:\M_PRJ\__PRJ\__CVI\termodiag\termodiag.prj" ProjectRelativePath="termodiag.prj"/></Projects>
		<buildData progressBarRate="3.809038468515001">
			<progressTimes>
				<Begin>0.000000000000000</Begin>
				<ProductsAdded>1.122187448605494</ProductsAdded>
				<DPConfigured>2.531836651499101</DPConfigured>
				<DPMergeModulesAdded>3.229966288339522</DPMergeModulesAdded>
				<DPClosed>3.978561624024517</DPClosed>
				<DistributionsCopied>4.086197826790869</DistributionsCopied>
				<End>26.253344729014035</End></progressTimes></buildData>
	</msi>
</distribution>
