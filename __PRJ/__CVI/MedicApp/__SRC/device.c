/***************************************************************************//**
 * @file device.c. Файл с функциями- обработчиками событий CVI.
 * @brief Инициализация модулей.
 * @authors A.Tushentsov
 ******************************************************************************/
#include <userint.h>
#include <windows.h>
#include <ansi_c.h>
#include <userint.h>
#include <toolbox.h>
#include <cvirte.h>
#include "device.h"
#include "medic_panel.h"
#include "System/sw_timer.h"
#include "System/framework.h"
#include "json/json.h"
#include "server.h"
#include "Parsers/spiro.h"
#include "rtc_time.h"

typedef struct {
	int name;
	int handle;
	int status;
}panels__struct_t;

#define PANEL_MAIN       0
#define PANEL_APP_SETT   1
#define PANEL_UPD        2
#define PANEL_TCP_SETT   3

static panels__struct_t panel_tbl[] = {{P_MAIN, 0, 1},
									   {P_APP_SETT, 0, 0},
									   {P_UPD, 0, 0},
									   {P_SETT_TCP, 0, 0},
									   {0, 0, 0}};
static int event_close, pr_ver_receive = 1;
static char g_buff[10000];
sw_timer__t pr_upd_tmr;
static char tx_buff[1000], rx_buff[1000];
static struct json_object *tx_obj;

static void server__cb(int id, events__e event, char* rx_buff, int len, void* ext_data);
static int CVICALLBACK windows_cb(int panelOrMenuBar, int controlOrMenuItem, int event, void *callbackData, int eventData1, int eventData2);
panels__struct_t* find_panel(int handle);
static void test_udp__get_version();
static int CVICALLBACK version_cout(void *functionId);
static void device__set_params();

events__e device__init() {
	uint16_t port;
	if (InitCVIRTE (0, NULL, 0) == 0)
		return -1;	/* out of memory */
	for (panels__struct_t* p_panel = panel_tbl; p_panel->name; p_panel++) {
		if ((p_panel->handle = LoadPanel (0, "medic_panel.uir", p_panel->name)) < 0)
			return EVENT__ERROR;
		RecallPanelState (p_panel->handle, "menu_settings.ini", p_panel->name);
		if (p_panel->status)
			DisplayPanel(p_panel->handle);
	}
	InstallMainCallback (windows_cb, NULL, 0);
	sw_timer__init(NULL);

	sprintf(g_buff, "MedicApp (%s)", _TARGET_FILE_VERSION_);
	SetPanelAttribute (panel_tbl->handle, ATTR_TITLE, g_buff);

	GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_SERVER_NAME, g_buff);
	GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_SERVER_PORT, &port);

	server__init(0, g_buff, port);

	device__set_params();
	tx_obj = json_object_new_object();

	version_cout(NULL);

	return EVENT__OK;
}

char* m_param_tbl[] = {"height", "weight", "cigarettes", "FVC", "FEV1", "FEV1/FVC", "PEF", "ELA", "FEF2575", "FET", "EVol", "FIVC", "FEV1/VC", "FEV6", "FEV1/FEV6", "FEV3", "FEV3/FVC", "FEF25", "FEF50", "FEF75", "PIF", "FIV1", "FIV1/FIVC", "PEFTime", "FEV05", "FEV05%", "FEV075", "FEV075%", "FEV2", "FEV2%", "FEF7585", "FIF25", "FIF50", "FIF75", "R50", "FEV1/PEF", "FEV1/FEV05", "SESSIONBESTFVC", "SESSIONBESTFEV1", "SESSIONBESTPEF", "SESSIONBESTFEV1/FVC", "INTERPRETATION", "QUALITYGRADE", "QUALITYREPORT",
	"",
};

static void device__set_params() {
	ClearListCtrl (panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_M_PARAM);
	for(int i = 0; *m_param_tbl[i]; i++)
		InsertListItem (panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_M_PARAM, -1, m_param_tbl[i], m_param_tbl[i]);

}


events__e device__deinit() {
	version_cout(NULL);
	for (panels__struct_t* p_panel = panel_tbl; p_panel->handle; p_panel++) {
		SavePanelState (p_panel->handle, "menu_settings.ini", p_panel->name);
		DiscardPanel (p_panel->handle);
	}
	return EVENT__OK;
}


static int CVICALLBACK windows_cb(int panelOrMenuBar, int controlOrMenuItem, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_KEYPRESS:
			break;
		case EVENT_CLOSE:
		case EVENT_END_TASK:
			return -1;
	}
	return 0;
}

static int CVICALLBACK version_cout(void *functionId) {
	volatile static int static__functionId = -1;
	if(functionId == NULL) { // Srart/Stop
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, version_cout, &static__functionId, &static__functionId);
		else {
			for(static__functionId = 0; static__functionId !=-1; ProcessSystemEvents(), Sleep(200));
		}
		return 0;
	}
	pr_ver_receive = 1;
	while(*(int*)functionId) {
		if (pr_ver_receive) {
			test_udp__get_version();
			pr_ver_receive = 0;
		}
		Sleep(100);
	}
	*(int*)functionId = -1;
	return 0;
}



events__e device__status_get() {
	return event_close ? EVENT__CLOSE : EVENT__OK;
}

int CVICALLBACK close_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	panels__struct_t* p_panel = find_panel(panel);
	switch (event) {
		case EVENT_COMMIT:
			if ((p_panel == NULL) || (p_panel == panel_tbl)) {
				event_close = 1;
				return 0;
			}
			p_panel->status = 0;
			HidePanel(p_panel->handle);
			break;
	}
	return 0;
}

void device__cout() {
	ProcessSystemEvents();
	framework__cout();
	Sleep(1);
}

const char *read_file(const char *path) {
	FILE *file = fopen(path, "r");
	if (file == NULL) {
		fprintf(stderr, "Expected file \"%s\" not found", path);
		return NULL;
	}
	fseek(file, 0, SEEK_END);
	long len = ftell(file);
	fseek(file, 0, SEEK_SET);
	char *buffer = malloc(len + 1);
	if (buffer == NULL) {
		fprintf(stderr, "Unable to allocate memory for file");
		fclose(file);
		return NULL;
	}
	fread(buffer, 1, len, file);
	buffer[len] = '\0';
	return (const char *)buffer;
}

void CVICALLBACK app_sett_con_cb (int menuBar, int menuItem, void *callbackData, int panel) {
	panels__struct_t* p_panel = &panel_tbl[PANEL_APP_SETT];
	p_panel->status = 1;
	DisplayPanel (p_panel->handle);
}

panels__struct_t* find_panel(int handle) {
	panels__struct_t* p_panel = panel_tbl;
	while(p_panel->name) {
		if (p_panel->handle == handle)
			break;
		p_panel++;
	}
	return p_panel->name ? p_panel : NULL;
}

typedef struct {
	int exec_id;
	int button_id;
	int path_id;
}button_app__t;
static const button_app__t button_str[] = {{P_MAIN_BPA_BUTTON, P_APP_SETT_BPA_BUTTON, P_APP_SETT_BPA_PATH},
										   {P_MAIN_ECG_BUTTON, P_APP_SETT_ECG_BUTTON, P_APP_SETT_ECG_PATH},
										   {P_MAIN_MCIN_BUTTON, P_APP_SETT_MCIN_BUTTON, P_APP_SETT_MCIN_PATH},
										   {P_MAIN_SPIRO_BUTTON, P_APP_SETT_SPIRO_BUTTON, P_APP_SETT_SPIRO_PATH},
										   {P_MAIN_SEND_BUTTON, P_APP_SETT_FOLDER_BUTTON, P_APP_SETT_FOLDER_PATH},
										   {-1, -1, -1}};

int CVICALLBACK app_sett__cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	button_app__t* cur_button;
	switch (event) {
		case EVENT_COMMIT:
			for(cur_button = button_str; (cur_button->button_id != control) && (cur_button->button_id != -1); cur_button++) {}
			if (cur_button->button_id == -1)
				return 0;
			switch(cur_button->button_id) {
				case P_APP_SETT_FOLDER_BUTTON:
				if(DirSelectPopup ("", "Папка с результатами", 1, 1, g_buff) == 0)
					return 0;
					break;
				default:
				if(FileSelectPopup ("", "*.exe", "Executable (*.exe);", "Open application", VAL_SELECT_BUTTON, 0, 1, 1, 1, g_buff) == 0)
					return 0;
					break;
			}
			SetCtrlVal(panel, cur_button->path_id, g_buff);
			break;
	}
	return 0;
}

int CVICALLBACK app_exec__cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	FILE * f_out;
	button_app__t* cur_button;
	char* out_str;
	uint16_t port;
	char cur_file[2000];
	int res, file_found = 0, temp;
	switch (event) {
		case EVENT_COMMIT:
			for(cur_button = button_str; (cur_button->exec_id != control) && (cur_button->exec_id != -1); cur_button++) {}
			if (cur_button->exec_id == -1)
				return 0;
			switch(cur_button->exec_id) {
				case P_MAIN_SEND_BUTTON:
					GetCtrlVal(panel_tbl[PANEL_APP_SETT].handle, cur_button->path_id, g_buff);
					strcat(g_buff, "\\*");
					res = GetFirstFile (g_buff, 1, 0, 0, 0, 0, 0, cur_file);
					while(!res) {
						if ((strlen(cur_file) > 3) && (!strcmp(&cur_file[strlen(cur_file) - 3],"csv"))) {
							file_found = 1;
							break;
						}
						res = GetNextFile(cur_file);
					}

					sprintf(&g_buff[strlen(g_buff) - 1], "%s", cur_file);
					if (spiro__create(g_buff) < 0) {
						MessagePopup ("Сообщение", "Результаты не найдены!");
						break;
					}

					GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_SERVER_NAME, g_buff);
					GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_SERVER_PORT, &port);
					server__init(0, g_buff, port);
//					GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_JSON_TX, tx_buff);

					json_object_put(tx_obj);
					tx_obj = json_object_new_object();

					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_HEIGHT), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_HEIGHT)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_WEIGHT), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_WEIGHT)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FVC), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FVC)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FEV1), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FEV1)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_PEF), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_PEF)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FEV2575), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FEV2575)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_EVOL), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_EVOL)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FEV1_VC), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FEV1_VC)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FEV6), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FEV6)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FEV1_FEV6), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FEV1_FEV6)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FEF25), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FEF25)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FEF50), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FEF50)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_FEF75), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_FEF75)));
					json_object_object_add(tx_obj, spiro__get_p_name(SPIRO__PARAM_PEFTIME), json_object_new_double(spiro__get_p_val(SPIRO__PARAM_PEFTIME)));
					out_str = json_object_to_json_string(tx_obj);

					GetCtrlVal(panel_tbl[PANEL_APP_SETT].handle, P_APP_SETT_JSON2FILE, &temp);
					if (temp) {
							GetCtrlVal(panel_tbl[PANEL_APP_SETT].handle, P_APP_SETT_FOLDER_PATH, g_buff);
							sprintf(&g_buff[strlen(g_buff)], "\\result.json");
							f_out = fopen (g_buff, "w");
							if (f_out) {
								fputs (out_str, f_out);
								fflush(f_out);
								fclose(f_out);
							}
					}
					else if (server__txrx(0, out_str, strlen(out_str), rx_buff, 1000, server__cb, (void*)panel) != EVENT__OK)
						MessagePopup ("Сообщение", "Сервер недоступен!");
					break;
				default:
					sprintf(g_buff, "cmd /C \"set __COMPAT_LAYER=RunAsAdmin && cd /d ");
					GetCtrlVal(panel_tbl[PANEL_APP_SETT].handle, cur_button->path_id, &g_buff[strlen(g_buff)]);
					for (char* cur_ptr = &g_buff[strlen(g_buff)]; *cur_ptr != '/' && *cur_ptr != '\\'; cur_ptr--)
						*cur_ptr = 0;
					sprintf(g_buff, "%s && \"", g_buff);
					GetCtrlVal(panel_tbl[PANEL_APP_SETT].handle, cur_button->path_id, &g_buff[strlen(g_buff)]);
					sprintf(g_buff, "%s\"", g_buff);
					if (LaunchExecutableEx (g_buff, LE_HIDE, NULL) == 0)
						return 0;
					break;
			}
			break;
	}
	return 0;
}

void CVICALLBACK program_fw_upd (int menuBar, int menuItem, void *callbackData,int panel) {
	panels__struct_t* p_panel = &panel_tbl[PANEL_UPD];
	p_panel->status = 1;
	DisplayPanel(p_panel->handle);
	pr_ver_receive = 1;
}

static void pr_upd_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	panels__struct_t* p_panel = &panel_tbl[PANEL_UPD];
	int x;
	sw_timer__start(&pr_upd_tmr, -1, pr_upd_tmr_cb, ext_data);
	if (ext_data) {
		p_panel->status = 0;
		HidePanel(p_panel->handle);
		SetCtrlVal(p_panel->handle, P_UPD_UPD_PROC, 0);
		sw_timer__stop(&pr_upd_tmr);
		LaunchExecutableEx ("cmd /C set __COMPAT_LAYER= RunAsHighest  && %TEMP%\\MedicAPP\\Volume\\install.exe", LE_HIDE, NULL);
		event_close = 1;
		return;
	}
	GetCtrlVal(p_panel->handle, P_UPD_UPD_PROC, &x);
	x++;
	SetCtrlVal(p_panel->handle, P_UPD_UPD_PROC, x);
}


static int CVICALLBACK program_upd_cout(void *functionId) {
	char buff[4000], url_addr[3000], *cur_str;
	panels__struct_t* p_panel = &panel_tbl[PANEL_UPD];
	int cur_proc;
	FILE* out_file;
	SetCtrlAttribute (p_panel->handle, P_UPD_UPD_PROC, ATTR_LABEL_TEXT, "Получение адреса...");
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && rd /Q /S %TEMP%\\MedicAPP \"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);

	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && powershell -command (New-Object Net.WebClient).DownloadFile('https://cloud-api.yandex.net/v1/disk/public/resources/download?public_key=https://disk.yandex.ru/d/AVspsemqgk710A', \\\"$Env:TEMP\\\\\\\"+'download_info.txt')\"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	sprintf(buff, "%s\\download_info.txt", getenv("TEMP"));
	out_file = fopen(buff, "r");
	if (out_file) {
		fgets(buff, sizeof(buff), out_file);
		if ((cur_str = strstr(buff, "https://"))) {
			for(int i = 0; i < sizeof(url_addr); i++, cur_str++) {
				if (*cur_str == '\"') {
					url_addr[i] = 0;
					break;
				}
				memcpy(&url_addr[i], cur_str, 1);

			}
		}
		fclose(out_file);
		LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && del /Q %TEMP%\\download_info.txt\"", LE_HIDE, &cur_proc);
	}
	sprintf(buff, "cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && powershell \"-command (New-Object Net.WebClient).DownloadFile('%s', \\\"$Env:TEMP\\\\\\\"+'madicapp.zip')\"\"", url_addr);
	SetCtrlAttribute (p_panel->handle, P_UPD_UPD_PROC, ATTR_LABEL_TEXT, "Загрузка архива...");
	if (LaunchExecutableEx (buff, LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	SetCtrlAttribute (p_panel->handle, P_UPD_UPD_PROC, ATTR_LABEL_TEXT, "Распаковка архива...");
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && powershell -command Expand-Archive $Env:TEMP\\\\madicapp.zip $Env:TEMP\\\\MedicAPP\"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && del /Q %TEMP%\\madicapp.zip \"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	SetCtrlAttribute (p_panel->handle, P_UPD_UPD_PROC, ATTR_LABEL_TEXT, "Выполнено.");
	sw_timer__start(&pr_upd_tmr, 100, pr_upd_tmr_cb, (void*)1);
	return 0;
}

int CVICALLBACK program_fw_upd_bt (int panel, int control, int event,void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			sw_timer__start(&pr_upd_tmr, 350, pr_upd_tmr_cb, NULL);
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, program_upd_cout, NULL, NULL);
			SetCtrlVal(panel_tbl[PANEL_UPD].handle, P_UPD_UPD_PROC, 0);
			break;
	}
	return 0;
}


int cmp_time(char* destt, char* curt) {
	int res = 3000000;
	int time_day[2] = {0, 0};
	char* str_time[] = {destt, curt};
	char* dt;
	int year[2] = {0}, month[2] = {0}, day, h, m, s, gmt_inv, gmt;

	for(int cur_t = 0; cur_t < 2; cur_t++) {
		dt = str_time[cur_t];
		gmt_inv = 1;
		for(int i = 0; i <= strlen(dt); i++) {
			if ((dt[i] == '-') || (dt[i] == ':') || (dt[i] == 'T') || (dt[i] == '+')) {
				if (dt[i] == '+')
					gmt_inv = 0;
				dt[i] = ' ';

			}
		}
		sscanf(dt, "%d%d%d%d%d%d%d", &year[cur_t], &month[cur_t], &day, &h, &m, &s, &gmt);
		if (gmt_inv)
			gmt = gmt * (-1);
		h -= gmt;

		time_day[cur_t] = s + 60*m + 3600*h + 86400*day;
	}

	if ((year[0] != year[1]) || (month[0] != month[1]))
		return res;
	res = time_day[0] - time_day[1];
	if (res < 0)
		res = res * (-1);
	return res;
}

static void test_udp__get_version() {
	static int busy = 0;
	if (busy)
		return;
	panels__struct_t* p_panel = panel_tbl;
	busy = 1;
	char cvi_date[200] = {BUILD_YEAR_CH0, BUILD_YEAR_CH1, BUILD_YEAR_CH2, BUILD_YEAR_CH3, '-', BUILD_MONTH_CH0, BUILD_MONTH_CH1, '-', BUILD_DAY_CH0, BUILD_DAY_CH1, 'T'};
	strcat(cvi_date, __TIME__);
	strcat(cvi_date, "+03:00");
	int cur_proc, year, day, month;
	FILE* out_file;
	char buff[2000], *cur_str;
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && powershell -command (New-Object Net.WebClient).DownloadFile('https://cloud-api.yandex.net/v1/disk/public/resources?public_key=https://disk.yandex.ru/d/AVspsemqgk710A', \\\"$Env:TEMP\\\\\\\"+'test_udp_info.txt')\"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	sprintf(buff, "%s\\test_udp_info.txt", getenv("TEMP"));
	out_file = fopen(buff, "r");
	if (out_file) {
		fgets(buff, sizeof(buff), out_file);
		if ((cur_str = strstr(buff, "\"modified\":\""))) {
			cur_str += 12;
			for(int i = 0; cur_str[i]; i++) {
				if (cur_str[i] == '\"')
					cur_str[i] = 0;
			}
			sscanf(cur_str, "%d-%d-%d", &year, &month, &day);
			if (cmp_time(cur_str, cvi_date) >= 86400) {                  // One day
				GetPanelAttribute (p_panel->handle, ATTR_TITLE, buff);
				if (strstr(buff, "доступно обновление") == NULL) {
					sprintf(buff, "%s (доступно обновление от %02d.%02d.%d)", buff, day, month, year);
					SetPanelAttribute (p_panel->handle, ATTR_TITLE, buff);
				}
			}
			sprintf(buff, "Версия на сайте от %02d.%02d.%d", day, month, year);
			SetCtrlVal(panel_tbl[PANEL_UPD].handle, P_UPD_PR_VER_SITE, buff);
		}
		fclose(out_file);
		LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && del /Q %TEMP%\\test_udp_info.txt\"", LE_HIDE, &cur_proc);
	}
	busy = 0;
}


void CVICALLBACK settings_connect_cb (int menuBar, int menuItem, void *callbackData, int panel) {
	panels__struct_t* p_panel = &panel_tbl[PANEL_TCP_SETT];
	p_panel->status = 1;
	DisplayPanel (p_panel->handle);
}


int CVICALLBACK json_bt_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	uint16_t port;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_SERVER_NAME, g_buff);
			GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_SERVER_PORT, &port);
			server__init(0, g_buff, port);
			GetCtrlVal(panel, P_SETT_TCP_JSON_TX, tx_buff);
			if (server__txrx(0, tx_buff, strlen(tx_buff), rx_buff, 1000, server__cb, (void*)panel) != EVENT__OK)
				MessagePopup ("Сообщение", "Сервер недоступен!");
			break;
	}
	return 0;
}

static void server__cb(int id, events__e event, char* rx_buff, int len, void* ext_data) {
	int res;
	if (event == EVENT__TIMEOUT)
		MessagePopup ("Сообщение", "Сервер был отключен!");
	else if (event == EVENT__OK) {
		GetCtrlVal(panel_tbl[PANEL_APP_SETT].handle, P_APP_SETT_FOLDER_PATH, g_buff);
		strcat(g_buff, "\\*.*");
		res = DeleteFile(g_buff);
		DeleteTextBoxLines (panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_JSON_RX, 0, -1);
		SetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_JSON_RX, rx_buff);
		MessagePopup ("Сообщение", "Результаты успешно отправлены!");
	}
}

int CVICALLBACK tx_clr_butt_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	panels__struct_t* p_panel = &panel_tbl[PANEL_TCP_SETT];
	switch (event) {
		case EVENT_COMMIT:
			json_object_put(tx_obj);
			tx_obj = json_object_new_object();
			DeleteTextBoxLines (p_panel->handle, P_SETT_TCP_JSON_TX, 0, -1);
			break;
	}
	return 0;
}

int CVICALLBACK tx_add_butt_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	panels__struct_t* p_panel = &panel_tbl[PANEL_TCP_SETT];
	int value;
	char buff[100];
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_M_PARAM, buff);
			GetCtrlVal(panel_tbl[PANEL_TCP_SETT].handle, P_SETT_TCP_M_VALUE, &value);
			json_object_object_add(tx_obj, buff, json_object_new_int(value));
			char* auth_str = json_object_to_json_string(tx_obj);
			DeleteTextBoxLines (p_panel->handle, P_SETT_TCP_JSON_TX, 0, -1);
			SetCtrlVal(p_panel->handle, P_SETT_TCP_JSON_TX, auth_str);
			break;
	}
	return 0;
}
