/***************************************************************************//**
 * @file screen.c.
 * @brief Модуль обработки экранных событий.
 * @author a.tushentsov
 ******************************************************************************/
#include "target.h"
#include "CVI/pixel.h"
#include "screen.h"


#define TEST_FILE "test.bmp"

typedef struct {
	int handle;
	char* buff;
	int x, y, height, width;
}image_t;

static image_t m_screen;
#include "System/sw_timer.h"
sw_timer__sys_time_t begin, end;

events__e screen__init(int width, int height) {
	m_screen.handle = pixel__create(0, 0, 0, width, height);
//	m_screen.handle = pixel__load(TEST_FILE);
	sw_timer__sys_time_get(&begin);
	pixel__scan(m_screen.handle);
	sw_timer__sys_time_get(&end);
//	pixel__save(m_screen.handle, "result.bmp");

	return EVENT__OK;
}



events__e screen__deinit() {


	return EVENT__OK;
}

