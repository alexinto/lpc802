/***************************************************************************//**
 * @file medic_app.c.
 * @brief Управление приложениями.
 * @author a.tushentsov
 ******************************************************************************/
#include <utility.h>
#include <ansi_c.h>
#include "target.h"
#include "device.h"
#include "server.h"
#include "screen.h"

char one_buff[1024];
int main (int argc, char *argv[]) {
/*
	if (argc < 2) {
		sprintf(one_buff, "cmd /C set __COMPAT_LAYER= RunAsAdmin && %s -1", argv[0]);
		LaunchExecutableEx (one_buff, LE_HIDE, NULL);
		return 0;
	}
*/
	DisableBreakOnLibraryErrors();
	if (device__init() != EVENT__OK)
		return -1;

	screen__init(1920, 1080);

	while(device__status_get() != EVENT__CLOSE) {
		device__cout();
	}

	device__deinit();

	EnableBreakOnLibraryErrors();
	return 0;
}












