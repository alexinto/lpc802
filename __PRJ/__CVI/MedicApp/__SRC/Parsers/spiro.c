/***************************************************************************//**
 * @file spiro.c.
 * @brief Модуль парсинга файла- результата работы SPIRO.
 * @author a.tushentsov
 ******************************************************************************/

#include <ansi_c.h>
#include "Parsers/spiro.h"

typedef struct {
	float val;
}spiro__param_t;

static const char* spiro__p_descr_tbl[] = {"Height", "Weight", "FVC", "FEV1", "PEF", "FEF2575", "EVol", "FEV1/VC", "FEV6", "FEV1/FEV6",
										   "FEF25", "FEF50", "FEF75", "PEFTime",
										 ""};

static const char* spiro__p_name_tbl[] = {"Height", "Weight", "FVC", "FEV1", "PEF", "FEF25-75%", "Extrapolated volume", "Predicted FEV1/FVC%", "FEV6", "Predicted FEV1/FEV6%",
										  "Blank 1 or FEF25%", "Blank 2 or FEF50%", "Blank 3 or FEF75%", "Time to PEF",
										 ""};

static spiro__param_t spiro__tbl[SPIRO__PARAM_MAX];

static char* spiro__p_next(char* buff, char* param_buff);
static int spiro_idx_by_name(char* buff, char* name);
static float spiro_param_by_idx(char* buff, int idx);

int spiro__create(char* filename) {
	char p_name[10000];
	char p_value[10000];
	FILE * file = fopen (filename, "r+");
	if (file == NULL)
		return -1;
	fgets (p_name, sizeof(p_name), file);
	fgets (p_value, sizeof(p_value), file);
	int idx;
	for(int i = 0; *spiro__p_name_tbl[i]; i++) {
		idx = spiro_idx_by_name(p_name, spiro__p_name_tbl[i]);
		if (idx != -1)
			spiro__tbl[i].val = spiro_param_by_idx(p_value, idx);
	}

	fclose(file);
	return 0;
}

float spiro__get_p_val(spiro__param_e param) {
	if (param >= SPIRO__PARAM_MAX)
		return 0;
	return spiro__tbl[param].val;
}

char* spiro__get_p_name(spiro__param_e param) {
	if (param >= SPIRO__PARAM_MAX)
		return NULL;
	return (char*)spiro__p_descr_tbl[param];
}


static int spiro_idx_by_name(char* buff, char* name) {
	int res = 0;
	char param[10000];
	buff = spiro__p_next(buff, param);
	do {
		if (!strcmp(param, name))
			return res;
		buff = spiro__p_next(buff, param);
		res++;
	} while(*buff);
	return -1;
}

static float spiro_param_by_idx(char* buff, int idx) {
	float res = 0;
	char param[10000];
	for(int i = 0; *buff; i++) {
		buff = spiro__p_next(buff, param);
		if (i == idx) {
			sscanf(param, "%f", &res);
			break;
		}
	}
	return res;
}


static char* spiro__p_next(char* buff, char* param_buff) {
	char* res = buff;
	while(*res && *res != ',') {
		*param_buff = *res;
		param_buff++;
		res++;
	}
	*param_buff = 0;
	return res ? ++res : NULL;
}
