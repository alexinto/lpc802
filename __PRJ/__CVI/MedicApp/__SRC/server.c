/***************************************************************************//**
 * @file server.c.
 * @brief Функции работы с json сервером.
 * @authors A.Tushentsov
 ******************************************************************************/
#include <userint.h>
#include <utility.h>
#include <ansi_c.h>
#include <tcpsupp.h>
#include "Form/System/fstandard.h"
#include "System/framework.h"
#include "server.h"

#ifndef SERVER__COUNT
#define SERVER__COUNT 1
#endif

typedef struct {
	unsigned int s_handle;
	char* buff_rx;
	int len_rx;
	events__e event;
	server__cb_t cb;
	void* ext_data;
	char* name;
	uint16_t port;
}server__struct_t;


static char buff[5000];
static server__struct_t servers[SERVER__COUNT];
static unsigned int s_handle;
static 	framework__sub_t cout_sub;

#define HTTP_POST "POST /api/checkup/auth HTTP/1.1\r\nHOST:%s:%d\r\nAccept: application/json; indent=4\r\nContent-Type: text/html; charset=UTF-8\r\nAuthorization: Bearer secret_token\r\nContent-Length: %zu%s\r\n\r\n"

int CVICALLBACK server__cb (unsigned handle, int event, int error, void *callbackData);

static void server__cout();

events__e server__init(int id, char* name, uint16_t port) {
	server__struct_t* p_srv = &servers[id];
	if (p_srv->name)
		free(p_srv->name);
	p_srv->name = malloc(strlen(name) + 1);

	if (p_srv->name == NULL)
		return EVENT__ERROR;
	strcpy(p_srv->name, name);
	p_srv->event = EVENT__WAIT;
	framework__cout_subscribe(&cout_sub, server__cout);
	p_srv->port = port;
	return EVENT__OK;
}


events__e server__connect() {

	return EVENT__OK;
}



events__e server__disconnect() {

	return EVENT__OK;
}


events__e server__settings_set() {

	return EVENT__OK;
}

int CVICALLBACK server__cb (unsigned handle, int event, int error, void *callbackData) {
	server__struct_t* p_srv = callbackData;
	events__e res = EVENT__ERROR;
    switch (event) {
        case TCP_DATAREADY:
            /* Read the number of points in this data. */
			if (p_srv->buff_rx)
            	p_srv->len_rx = ClientTCPRead (handle, p_srv->buff_rx, p_srv->len_rx, 0);
			if (p_srv->len_rx >= 0)
				res = EVENT__OK;
            /* Clear the graph and update number of points. */
            /* Read the data. Note that the data could be empty. */
            break;
        case TCP_DISCONNECT:
            if (handle == s_handle) {
                /* The server we were talking to has disconnected. */
                res = EVENT__TIMEOUT;
            }
            break;
		case TCP_CONNECT:
			break;
    }
	if ((p_srv->cb) && (p_srv->event == EVENT__WAIT)) {
		p_srv->event = res;
	}
    return 0;
}

events__e server__txrx(int id, char* tx_buff, int tx_len, char* rx_buff, int rx_len, server__cb_t cb, void* ext_data) {
	server__struct_t* p_srv = &servers[id];
	sprintf(buff, HTTP_POST, p_srv->name, p_srv->port, tx_len, tx_buff);
	p_srv->event = EVENT__WAIT;
	p_srv->cb = cb;
 	p_srv->ext_data = ext_data;
	p_srv->buff_rx = rx_buff;
	p_srv->len_rx = rx_len;
	int res = ConnectToTCPServer (&s_handle, p_srv->port, p_srv->name, server__cb, p_srv, 100);
	ClientTCPWrite (s_handle, buff, strlen(buff), 0);
	return res >= 0 ? EVENT__OK : EVENT__ERROR;
}

static void server__cout() {
	server__struct_t* p_srv = &servers[0];
	server__cb_t cb;

	if ((p_srv->cb) && (p_srv->event != EVENT__WAIT)) {
		cb = p_srv->cb;
		p_srv->cb = NULL;
		cb(p_srv - servers, p_srv->event, p_srv->buff_rx, p_srv->len_rx, p_srv->ext_data);
	}

}












