/***************************************************************************//**
 * @file server.h.
 * @brief �������� ������� ������ � ��������.
 * @authors A.Tushentsov
 ******************************************************************************/
#ifndef SREVER_H_
#define SREVER_H_
#include "System/events.h"

typedef void (*server__cb_t)(int id, events__e event, char* rx_buff, int len, void* ext_data);


events__e server__init(int id, char* name, uint16_t port);

events__e server__connect();

events__e server__disconnect();


events__e server__settings_set();

events__e server__settings_get();

events__e server__txrx(int id, char* tx_buff, int tx_len, char* rx_buff, int rx_len, server__cb_t cb, void* ext_data);

#endif