/***************************************************************************//**
 * @file init.h.
 * @brief ������������� �������.
 * @authors A.Tushentsov
 ******************************************************************************/
#ifndef DEVICE_H_
#define DEVICE_H_
#include "System/events.h"

events__e device__init();

events__e device__status_get();

void device__cout();

events__e device__deinit();

#endif