/***************************************************************************//**
 * @file bpa.h.
 * @brief ������ �������� �����- ���������� ������ BPA.
 * @authors A.Tushentsov
 ******************************************************************************/
#ifndef SPIRO_H_
#define SPIRO_H_
#include "System/events.h"

typedef enum {
	SPIRO__PARAM_HEIGHT = 0,
	SPIRO__PARAM_WEIGHT = 1,
	SPIRO__PARAM_FVC    = 2,
	SPIRO__PARAM_FEV1   = 3,
	SPIRO__PARAM_PEF    = 4,
	SPIRO__PARAM_FEV2575 = 5,
	SPIRO__PARAM_EVOL    = 6,
	SPIRO__PARAM_FEV1_VC = 7,
	SPIRO__PARAM_FEV6    = 8,
	SPIRO__PARAM_FEV1_FEV6 = 9,
	SPIRO__PARAM_FEF25   = 10,
	SPIRO__PARAM_FEF50   = 11,
	SPIRO__PARAM_FEF75   = 12,
	SPIRO__PARAM_PEFTIME = 13,

	SPIRO__PARAM_MAX,
}spiro__param_e;


int spiro__create(char* filename);

float spiro__get_p_val(spiro__param_e param);

char* spiro__get_p_name(spiro__param_e param);

#endif