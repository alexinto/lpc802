/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  P_APP_SETT                       1
#define  P_APP_SETT_DECORATION            2       /* control type: deco, callback function: (none) */
#define  P_APP_SETT_QUITBUTTON            3       /* control type: command, callback function: close_cb */
#define  P_APP_SETT_FOLDER_BUTTON         4       /* control type: pictButton, callback function: app_sett__cb */
#define  P_APP_SETT_SPIRO_BUTTON          5       /* control type: pictButton, callback function: app_sett__cb */
#define  P_APP_SETT_BPA_BUTTON            6       /* control type: pictButton, callback function: app_sett__cb */
#define  P_APP_SETT_MCIN_BUTTON           7       /* control type: pictButton, callback function: app_sett__cb */
#define  P_APP_SETT_FOLDER_PATH           8       /* control type: string, callback function: (none) */
#define  P_APP_SETT_SPIRO_PATH            9       /* control type: string, callback function: (none) */
#define  P_APP_SETT_ECG_BUTTON            10      /* control type: pictButton, callback function: app_sett__cb */
#define  P_APP_SETT_MCIN_PATH             11      /* control type: string, callback function: (none) */
#define  P_APP_SETT_ECG_PATH              12      /* control type: string, callback function: (none) */
#define  P_APP_SETT_BPA_PATH              13      /* control type: string, callback function: (none) */
#define  P_APP_SETT_JSON2FILE             14      /* control type: radioButton, callback function: (none) */

#define  P_MAIN                           2
#define  P_MAIN_QUITBUTTON                2       /* control type: command, callback function: close_cb */
#define  P_MAIN_DECORATION                3       /* control type: deco, callback function: (none) */
#define  P_MAIN_SEND_BUTTON               4       /* control type: pictButton, callback function: app_exec__cb */
#define  P_MAIN_SPIRO_BUTTON              5       /* control type: pictButton, callback function: app_exec__cb */
#define  P_MAIN_MCIN_BUTTON               6       /* control type: pictButton, callback function: app_exec__cb */
#define  P_MAIN_ECG_BUTTON                7       /* control type: pictButton, callback function: app_exec__cb */
#define  P_MAIN_BPA_BUTTON                8       /* control type: pictButton, callback function: app_exec__cb */

#define  P_SETT_TCP                       3
#define  P_SETT_TCP_DECORATION            2       /* control type: deco, callback function: (none) */
#define  P_SETT_TCP_JSON_TST_BT_2         3       /* control type: command, callback function: tx_add_butt_cb */
#define  P_SETT_TCP_JSON_TST_BT_3         4       /* control type: command, callback function: tx_clr_butt_cb */
#define  P_SETT_TCP_JSON_TST_BT           5       /* control type: command, callback function: json_bt_cb */
#define  P_SETT_TCP_QUITBUTTON            6       /* control type: command, callback function: close_cb */
#define  P_SETT_TCP_JSON_TX               7       /* control type: textBox, callback function: (none) */
#define  P_SETT_TCP_JSON_RX               8       /* control type: textBox, callback function: (none) */
#define  P_SETT_TCP_SERVER_NAME           9       /* control type: string, callback function: (none) */
#define  P_SETT_TCP_M_PARAM               10      /* control type: ring, callback function: (none) */
#define  P_SETT_TCP_SERVER_PORT           11      /* control type: numeric, callback function: (none) */
#define  P_SETT_TCP_M_VALUE               12      /* control type: numeric, callback function: (none) */
#define  P_SETT_TCP_DECORATION_2          13      /* control type: deco, callback function: (none) */

#define  P_UPD                            4
#define  P_UPD_DECORATION                 2       /* control type: deco, callback function: program_fw_upd_bt */
#define  P_UPD_UPD_BUTTON                 3       /* control type: command, callback function: program_fw_upd_bt */
#define  P_UPD_QUITBUTTON                 4       /* control type: command, callback function: close_cb */
#define  P_UPD_UPD_PROC                   5       /* control type: scale, callback function: (none) */
#define  P_UPD_PR_VER_SITE                6       /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

#define  MAINMENU                         1
#define  MAINMENU_M_SETTINGS              2
#define  MAINMENU_M_SETTINGS_APP_SETT     3       /* callback function: app_sett_con_cb */
#define  MAINMENU_M_SETTINGS_M_CONNECT    4       /* callback function: settings_connect_cb */
#define  MAINMENU_M_ABOUT                 5       /* callback function: program_fw_upd */


     /* Callback Prototypes: */

int  CVICALLBACK app_exec__cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK app_sett__cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK app_sett_con_cb(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK close_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK json_bt_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK program_fw_upd(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK program_fw_upd_bt(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK settings_connect_cb(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK tx_add_butt_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK tx_clr_butt_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif