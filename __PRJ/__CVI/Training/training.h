/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_LED8                       2       /* control type: LED, callback function: led_cb */
#define  PANEL_LED7                       3       /* control type: LED, callback function: led_cb */
#define  PANEL_LED11                      4       /* control type: LED, callback function: led_cb */
#define  PANEL_LED10                      5       /* control type: LED, callback function: led_cb */
#define  PANEL_LED9                       6       /* control type: LED, callback function: led_cb */
#define  PANEL_LED4                       7       /* control type: LED, callback function: led_cb */
#define  PANEL_LED6                       8       /* control type: LED, callback function: led_cb */
#define  PANEL_LED5                       9       /* control type: LED, callback function: led_cb */
#define  PANEL_LED1                       10      /* control type: LED, callback function: led_cb */
#define  PANEL_LED2                       11      /* control type: LED, callback function: led_cb */
#define  PANEL_LED12                      12      /* control type: LED, callback function: led_cb */
#define  PANEL_LED3                       13      /* control type: LED, callback function: led_cb */
#define  PANEL_PROGRAM_BOX                14      /* control type: listBox, callback function: program_box_cb */
#define  PANEL_RECORD_SW                  15      /* control type: binary, callback function: record_cb */
#define  PANEL_DELAY_BUTTON_2             16      /* control type: command, callback function: clear_button_cb */
#define  PANEL_DELAY_BUTTON               17      /* control type: command, callback function: delay_button_cb */
#define  PANEL_DELAY_VALUE                18      /* control type: numeric, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

#define  MENUBAR                          1
#define  MENUBAR_MENU_FILE                2
#define  MENUBAR_MENU_FILE_FILE_OUT       3       /* callback function: menu_out_cb */


     /* Callback Prototypes: */

int  CVICALLBACK clear_button_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK delay_button_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK led_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK menu_out_cb(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK program_box_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK record_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif