﻿/***************************************************************************//**
 * @file script.h.
 * @brief  Файл создания и воспроизведения скриптов.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef SCRIPT_H_
#define SCRIPT_H_
#include "System/framework.h"


events__e script__init(int panelHandle);


events__e script__record(); // запись скрипта

events__e script__start();  // воспроизедение скрипта

events__e script__stop();   // остановка скрипта








#endif
