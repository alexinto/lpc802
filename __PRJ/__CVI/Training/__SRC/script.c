﻿/***************************************************************************//**
 * @file script.c.
 * @brief  Файл создания и воспроизведения скриптов.
 * @author a.tushentsov.
 ******************************************************************************/
#include <ansi_c.h>
#include <userint.h>
#include "target.h"
#include "training.h"
#include "script.h"
#include "System/sw_timer.h"

#define SCRIPT__CMD_MAX 1000

typedef enum {
	SCRIPT__MODE_RECORD = 0,
	SCRIPT__MODE_PLAY   = 1,
}script__mode_e;

typedef enum {
	SCRIPT__CMD_NONE   = 0,
	SCRIPT__CMD_LED    = 1,
	SCRIPT__CMD_DELAY  = 2,
}script__cmd_e;

static char* cmd_descr_tbl[] = {
	[SCRIPT__CMD_NONE] = " ",
	[SCRIPT__CMD_LED] = "Светодиод ",
	[SCRIPT__CMD_DELAY] = "Задержка ",
};


typedef struct {
	script__cmd_e cmd;
	int param[5];
}script__cmd_t;

typedef struct {
	int panelHandle;
	script__mode_e mode;
	script__cmd_t* cur_cmd;
	sw_timer__t tmr;
}script__struct_t;

static script__cmd_t script_tbl[SCRIPT__CMD_MAX];

#define SCRIPT__END_CMD (&script_tbl[SCRIPT__CMD_MAX-1])

static script__struct_t script__data;


static int script__led2id(int led_id);
static int script__id2led(int led_id);
static char* script__descr(script__cmd_e cmd, int p0);
static void script__tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void script__led_clr();

events__e script__init(int panelHandle) {
	script__data.mode = SCRIPT__MODE_PLAY;
	script__data.panelHandle = panelHandle;
	ClearListCtrl (script__data.panelHandle, PANEL_PROGRAM_BOX);
	script__led_clr();
	script__data.cur_cmd = script_tbl;
	script__data.cur_cmd->cmd = SCRIPT__CMD_NONE;

	return EVENT__OK;
}


events__e script__record() {
	script__data.mode = SCRIPT__MODE_RECORD;
	script__data.cur_cmd = script_tbl;
	SetCtrlAttribute (script__data.panelHandle, PANEL_PROGRAM_BOX, ATTR_DIMMED, 0);
	sw_timer__stop(&script__data.tmr);
	script__led_clr();

	int item_count;
	GetNumListItems (script__data.panelHandle, PANEL_PROGRAM_BOX, &item_count);
	if (item_count) {
		item_count--;
		SetCtrlIndex (script__data.panelHandle, PANEL_PROGRAM_BOX, item_count);
		script__data.cur_cmd = &script_tbl[item_count];
	}

/*
	CVI__BREAK_ERR_OFF
	SetCtrlIndex (script__data.panelHandle, PANEL_PROGRAM_BOX, 0);
	CVI__BREAK_ERR_ON
*/
	return EVENT__OK;
}


events__e script__start() {
	script__data.mode = SCRIPT__MODE_PLAY;
	SetCtrlAttribute (script__data.panelHandle, PANEL_PROGRAM_BOX, ATTR_DIMMED, 1);
	script__data.cur_cmd = script_tbl;
	script__led_clr();
	sw_timer__start(&script__data.tmr, 0, script__tmr_cb, NULL);
	return EVENT__OK;
}

static void script__tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	u32 delay = 0;
	switch(script__data.cur_cmd->cmd) {
		case SCRIPT__CMD_LED:
			script__led_clr();
			while(script__data.cur_cmd->cmd == SCRIPT__CMD_LED) {
				SetCtrlVal (script__data.panelHandle, script__id2led(script__data.cur_cmd->param[0]), 1);
				script__data.cur_cmd++;
			}
			break;
		case SCRIPT__CMD_DELAY:
			delay = script__data.cur_cmd->param[0];
			script__data.cur_cmd++;
			break;
		case SCRIPT__CMD_NONE:
			script__data.cur_cmd = script_tbl;
			break;
	}
	sw_timer__start(&script__data.tmr, delay, script__tmr_cb, NULL);
}

int CVICALLBACK led_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int led_id, cmd_idx, item_count;
	switch (event) {
		case EVENT_LEFT_CLICK:
			if (script__data.mode != SCRIPT__MODE_RECORD)
				break;
			if (script__data.cur_cmd) {
				led_id = script__led2id(control);
				script__data.cur_cmd->cmd = SCRIPT__CMD_LED;
				script__data.cur_cmd->param[0] = led_id;
				cmd_idx = script__data.cur_cmd - script_tbl;

				GetNumListItems (script__data.panelHandle, PANEL_PROGRAM_BOX, &item_count);
				if (cmd_idx >= item_count)
					InsertListItem (script__data.panelHandle, PANEL_PROGRAM_BOX, -1, script__descr(script__data.cur_cmd->cmd, script__data.cur_cmd->param[0]), cmd_idx);
				else
					ReplaceListItem (script__data.panelHandle, PANEL_PROGRAM_BOX, cmd_idx, script__descr(script__data.cur_cmd->cmd, script__data.cur_cmd->param[0]), cmd_idx);
				if (script__data.cur_cmd < SCRIPT__END_CMD) {
					script__data.cur_cmd++;
					GetNumListItems (script__data.panelHandle, PANEL_PROGRAM_BOX, &item_count);
					cmd_idx = script__data.cur_cmd - script_tbl;
					while (cmd_idx >= item_count) {
						InsertListItem (script__data.panelHandle, PANEL_PROGRAM_BOX, -1, script__descr(SCRIPT__CMD_NONE, 0), cmd_idx);
						GetNumListItems (script__data.panelHandle, PANEL_PROGRAM_BOX, &item_count);
					}
//					SetCtrlIndex (script__data.panelHandle, PANEL_PROGRAM_BOX, cmd_idx);
				}
			}
			break;
	}
	return 0;
}


static int script__led_tbl[] = {
	PANEL_LED1, PANEL_LED2, PANEL_LED3, PANEL_LED4, PANEL_LED5, PANEL_LED6, PANEL_LED7, PANEL_LED8, PANEL_LED9, PANEL_LED10, PANEL_LED11, PANEL_LED12,
};

static void script__led_clr() {
	for(int i = 0; i < sizeof(script__led_tbl) / sizeof(script__led_tbl[0]); i++) {
		SetCtrlVal (script__data.panelHandle, script__id2led(i), 0);
	}
}

static int script__led2id(int led) {
	int res = sizeof(script__led_tbl) / sizeof(script__led_tbl[0]);
	while((--res > 0) && (script__led_tbl[res] != led)) {}
	return res;
}

static int script__id2led(int led_id) {
	return 	script__led_tbl[led_id];
}

static char cmd_descr_buff[1000];
static char* script__descr(script__cmd_e cmd, int p0) {
	int len = sprintf(cmd_descr_buff, cmd_descr_tbl[cmd]);
	if (cmd != SCRIPT__CMD_NONE)
		sprintf(&cmd_descr_buff[len], "%d", p0);
	return cmd_descr_buff;
}


int CVICALLBACK program_box_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int idx;
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlIndex (panel, control, &idx);
			if (idx < 0)
				idx = 0;
			script__data.cur_cmd = &script_tbl[idx];
			break;
	}
	return 0;
}

int CVICALLBACK delay_button_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int delay, cmd_idx, item_count;
	switch (event) {
		case EVENT_COMMIT:
			if (script__data.mode != SCRIPT__MODE_RECORD)
				break;
			if (script__data.cur_cmd) {
				script__data.cur_cmd->cmd = SCRIPT__CMD_DELAY;
				GetCtrlVal(script__data.panelHandle, PANEL_DELAY_VALUE, &delay);
				script__data.cur_cmd->param[0] = delay;
				cmd_idx = script__data.cur_cmd - script_tbl;

				GetNumListItems (script__data.panelHandle, PANEL_PROGRAM_BOX, &item_count);
				if (cmd_idx >= item_count)
					InsertListItem (script__data.panelHandle, PANEL_PROGRAM_BOX, -1, script__descr(script__data.cur_cmd->cmd, script__data.cur_cmd->param[0]), cmd_idx);
				else
					ReplaceListItem (script__data.panelHandle, PANEL_PROGRAM_BOX, cmd_idx, script__descr(script__data.cur_cmd->cmd, script__data.cur_cmd->param[0]), cmd_idx);
				if (script__data.cur_cmd < SCRIPT__END_CMD) {
					script__data.cur_cmd++;
					GetNumListItems (script__data.panelHandle, PANEL_PROGRAM_BOX, &item_count);
					cmd_idx = script__data.cur_cmd - script_tbl;
					while (cmd_idx >= item_count) {
						InsertListItem (script__data.panelHandle, PANEL_PROGRAM_BOX, -1, script__descr(SCRIPT__CMD_NONE, 0), cmd_idx);
						GetNumListItems (script__data.panelHandle, PANEL_PROGRAM_BOX, &item_count);
					}
//					SetCtrlIndex (script__data.panelHandle, PANEL_PROGRAM_BOX, cmd_idx);
				}
			}
			break;
	}
	return 0;
}

int CVICALLBACK clear_button_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			if (script__data.mode != SCRIPT__MODE_RECORD)
				break;
			ClearListCtrl (script__data.panelHandle, PANEL_PROGRAM_BOX);
			script__data.cur_cmd = script_tbl;
			memset(script_tbl, 0, sizeof(script_tbl));
			break;
	}
	return 0;
}





