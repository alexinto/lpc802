﻿#include <cvirte.h>
#include <userint.h>
#include <windows.h>
#include <utility.h>
#include "target.h"
#include "training.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "script.h"


static int panelHandle;
static u8 event_close;

static int CVICALLBACK windows_cb(int panelOrMenuBar, int controlOrMenuItem, int event, void *callbackData, int eventData1, int eventData2);
static int CVICALLBACK framework_ctrl(void *functionId);

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "training.uir", PANEL)) < 0)
		return -1;
	InstallMainCallback (windows_cb, NULL, 0);

	CVI__BREAK_ERR_OFF
	RecallPanelState (panelHandle, "training.ini", 0);
	CVI__BREAK_ERR_ON

	DisplayPanel (panelHandle);
	framework_ctrl(NULL);    // Запускам Framework

	script__init(panelHandle);
	record_cb (panelHandle, PANEL_RECORD_SW, EVENT_COMMIT, NULL, 0, 0);



	while(!event_close) {
		ProcessSystemEvents();
		Sleep(0);
	}

	framework_ctrl(NULL);    // Останавливаем Framework
	CVI__BREAK_ERR_OFF
	SavePanelState(panelHandle, "training.ini", 0);
	CVI__BREAK_ERR_ON
	DiscardPanel (panelHandle);
	return 0;
}

void CVICALLBACK menu_out_cb (int menuBar, int menuItem, void *callbackData, int panel) {
	event_close = 1;
}


static int CVICALLBACK windows_cb(int panelOrMenuBar, int controlOrMenuItem, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_KEYPRESS:
			break;
		case EVENT_CLOSE:
		case EVENT_END_TASK:
			event_close = 1;
			return 0;
	}
	return 0;
}

static int CVICALLBACK framework_ctrl(void *functionId) {
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1) {
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, framework_ctrl, &static__functionId, &static__functionId);
			while(static__functionId == -1) {Sleep(100), ProcessSystemEvents();}
		}
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(100), ProcessSystemEvents());
		return 0;
	}

	sw_timer__init(NULL);

	while(*(int*)functionId) {
		Sleep(0);
		framework__cout();
	}
	*(int*)functionId = -1;
	return 0;
}


int CVICALLBACK record_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int record;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal (panel, control, &record);
			if (record) {
				script__record();
			}
			else {
				script__start();
			}
			break;
	}
	return 0;
}


