/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_DECORATION_5               2       /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_3               3       /* control type: deco, callback function: (none) */
#define  PANEL_COM_RENEW                  4       /* control type: command, callback function: com_renew_cb */
#define  PANEL_TEXTMSG_2                  5       /* control type: textMsg, callback function: (none) */
#define  PANEL_SCRIPT_BUTTON              6       /* control type: command, callback function: script_exec_button_cb */
#define  PANEL_ADD_BUTTON                 7       /* control type: command, callback function: script_add_cb */
#define  PANEL_CLR_BUTTON                 8       /* control type: command, callback function: script_clr_cb */
#define  PANEL_CMD_BUTTON_1               9       /* control type: command, callback function: cmd_write_cb */
#define  PANEL_QUITBUTTON                 10      /* control type: command, callback function: QuitCallback */
#define  PANEL_TIME_PROGRAM               11      /* control type: string, callback function: (none) */
#define  PANEL_DECORATION_8               12      /* control type: deco, callback function: (none) */
#define  PANEL_TEXTMSG_13                 13      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_7                  14      /* control type: textMsg, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_CMD_8         15      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_CMD_7         16      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_CMD_6         17      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_CMD_5         18      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_CMD_4         19      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_CMD_3         20      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_CMD_2         21      /* control type: ring, callback function: (none) */
#define  PANEL_LED_BUTTON_CMD             22      /* control type: ring, callback function: led_cmd_cb */
#define  PANEL_SERVO_BUTTON_CMD_7         23      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_CMD_6         24      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_CMD_5         25      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_CMD_4         26      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_CMD_3         27      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_CMD_2         28      /* control type: ring, callback function: (none) */
#define  PANEL_KERNEL_BUTTON_CMD_2        29      /* control type: ring, callback function: (none) */
#define  PANEL_KERNEL_BUTTON_CMD_1        30      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_CMD_1         31      /* control type: ring, callback function: (none) */
#define  PANEL_LAMP_CMD_1                 32      /* control type: ring, callback function: (none) */
#define  PANEL_FUN_CMD_1                  33      /* control type: ring, callback function: (none) */
#define  PANEL_FUN_CMD_2                  34      /* control type: ring, callback function: (none) */
#define  PANEL_PELETIER_CMD_1_1           35      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_CMD_1         36      /* control type: ring, callback function: (none) */
#define  PANEL_PELETIER_CMD_1_3           37      /* control type: ring, callback function: (none) */
#define  PANEL_PELETIER_CMD_2_1           38      /* control type: ring, callback function: (none) */
#define  PANEL_PELETIER_CMD_1_2           39      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_ID_7          40      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_ID_8          41      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_ID_6          42      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_ID_7          43      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_ID_5          44      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_ID_6          45      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_ID_4          46      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_ID_5          47      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_ID_3          48      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_ID_4          49      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_ID_2          50      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_ID_3          51      /* control type: ring, callback function: (none) */
#define  PANEL_FUN_BUTTON_ID_2            52      /* control type: ring, callback function: (none) */
#define  PANEL_PELETIER_BUTTON_ID_2       53      /* control type: ring, callback function: (none) */
#define  PANEL_FUN_BUTTON_ID_1            54      /* control type: ring, callback function: (none) */
#define  PANEL_SERVO_BUTTON_ID_1          55      /* control type: ring, callback function: (none) */
#define  PANEL_PELETIER_BUTTON_ID_1       56      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_ID_2          57      /* control type: ring, callback function: (none) */
#define  PANEL_MOTOR_BUTTON_ID_1          58      /* control type: ring, callback function: (none) */
#define  PANEL_COM_CHANGE                 59      /* control type: ring, callback function: com_port_change */
#define  PANEL_DECORATION_9               60      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_10              61      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_11              62      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_17              63      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_16              64      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_12              65      /* control type: deco, callback function: (none) */
#define  PANEL_SERVO_ANSWER_7             66      /* control type: string, callback function: (none) */
#define  PANEL_DECORATION_13              67      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_14              68      /* control type: deco, callback function: (none) */
#define  PANEL_SERVO_ANSWER_6             69      /* control type: string, callback function: (none) */
#define  PANEL_TEXTMSG_8                  70      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_15                 71      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_14                 72      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_9                  73      /* control type: textMsg, callback function: (none) */
#define  PANEL_SERVO_ANSWER_5             74      /* control type: string, callback function: (none) */
#define  PANEL_TEXTMSG_10                 75      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_12                 76      /* control type: textMsg, callback function: (none) */
#define  PANEL_SERVO_ANSWER_4             77      /* control type: string, callback function: (none) */
#define  PANEL_SERVO_PARAM_7              78      /* control type: numeric, callback function: (none) */
#define  PANEL_TEXTMSG_11                 79      /* control type: textMsg, callback function: (none) */
#define  PANEL_MOTOR_ANSWER_8             80      /* control type: string, callback function: (none) */
#define  PANEL_SERVO_ANSWER_3             81      /* control type: string, callback function: (none) */
#define  PANEL_SERVO_PARAM_6              82      /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_ANSWER_7             83      /* control type: string, callback function: (none) */
#define  PANEL_MOTOR_ANSWER_6             84      /* control type: string, callback function: (none) */
#define  PANEL_SERVO_ANSWER_2             85      /* control type: string, callback function: (none) */
#define  PANEL_SERVO_PARAM_5              86      /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_ANSWER_5             87      /* control type: string, callback function: (none) */
#define  PANEL_FUN_ANSWER_2               88      /* control type: string, callback function: (none) */
#define  PANEL_LAMP_ANSWER_1              89      /* control type: string, callback function: (none) */
#define  PANEL_MOTOR_ANSWER_4             90      /* control type: string, callback function: (none) */
#define  PANEL_PELETIER_ANSWER_2          91      /* control type: string, callback function: (none) */
#define  PANEL_FUN_ANSWER_1               92      /* control type: string, callback function: (none) */
#define  PANEL_SERVO_ANSWER_1             93      /* control type: string, callback function: (none) */
#define  PANEL_SERVO_PARAM_4              94      /* control type: numeric, callback function: (none) */
#define  PANEL_PELETIER_ANSWER_1          95      /* control type: string, callback function: (none) */
#define  PANEL_MOTOR_ANSWER_3             96      /* control type: string, callback function: (none) */
#define  PANEL_MOTOR_ANSWER_2             97      /* control type: string, callback function: (none) */
#define  PANEL_MOTOR_ANSWER_1             98      /* control type: string, callback function: (none) */
#define  PANEL_SERVO_PARAM_3              99      /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_PARAM_8              100     /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_PARAM_7              101     /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_PARAM_6              102     /* control type: numeric, callback function: (none) */
#define  PANEL_KERNEL_PARAM_2             103     /* control type: numeric, callback function: (none) */
#define  PANEL_SERVO_PARAM_2              104     /* control type: numeric, callback function: (none) */
#define  PANEL_KERNEL_PARAM_1             105     /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_PARAM_5              106     /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_PARAM_4              107     /* control type: numeric, callback function: (none) */
#define  PANEL_FUN_PARAM_2                108     /* control type: numeric, callback function: (none) */
#define  PANEL_LAMP_PARAM_1               109     /* control type: numeric, callback function: (none) */
#define  PANEL_PELETIER_PARAM_2_1         110     /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_PARAM_3              111     /* control type: numeric, callback function: (none) */
#define  PANEL_PELETIER_PARAM_1_3         112     /* control type: numeric, callback function: (none) */
#define  PANEL_FUN_PARAM_1                113     /* control type: numeric, callback function: (none) */
#define  PANEL_SERVO_PARAM_1              114     /* control type: numeric, callback function: (none) */
#define  PANEL_PELETIER_PARAM_1_2         115     /* control type: numeric, callback function: (none) */
#define  PANEL_LED_PARAM                  116     /* control type: numeric, callback function: (none) */
#define  PANEL_PELETIER_PARAM_1_1         117     /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_PARAM_2              118     /* control type: numeric, callback function: (none) */
#define  PANEL_KERNEL_ID                  119     /* control type: numeric, callback function: (none) */
#define  PANEL_LAMP_ID_1                  120     /* control type: numeric, callback function: (none) */
#define  PANEL_LED_ID                     121     /* control type: numeric, callback function: (none) */
#define  PANEL_MOTOR_PARAM_1              122     /* control type: numeric, callback function: (none) */
#define  PANEL_DECORATION                 123     /* control type: deco, callback function: (none) */
#define  PANEL_TEXT_CMD_ANSWER            124     /* control type: textBox, callback function: (none) */
#define  PANEL_DECORATION_15              125     /* control type: deco, callback function: (none) */
#define  PANEL_LED_8                      126     /* control type: LED, callback function: (none) */
#define  PANEL_LED_7                      127     /* control type: LED, callback function: (none) */
#define  PANEL_LED_6                      128     /* control type: LED, callback function: (none) */
#define  PANEL_LED_5                      129     /* control type: LED, callback function: (none) */
#define  PANEL_LED_4                      130     /* control type: LED, callback function: (none) */
#define  PANEL_LED_3                      131     /* control type: LED, callback function: (none) */
#define  PANEL_LED_2                      132     /* control type: LED, callback function: (none) */
#define  PANEL_LED_1                      133     /* control type: LED, callback function: (none) */
#define  PANEL_LED_CHECK_8                134     /* control type: radioButton, callback function: led_cmd_cb */
#define  PANEL_LED_CHECK_7                135     /* control type: radioButton, callback function: led_cmd_cb */
#define  PANEL_LED_CHECK_6                136     /* control type: radioButton, callback function: led_cmd_cb */
#define  PANEL_LED_CHECK_5                137     /* control type: radioButton, callback function: led_cmd_cb */
#define  PANEL_LED_CHECK_4                138     /* control type: radioButton, callback function: led_cmd_cb */
#define  PANEL_LED_CHECK_3                139     /* control type: radioButton, callback function: led_cmd_cb */
#define  PANEL_LED_CHECK_2                140     /* control type: radioButton, callback function: led_cmd_cb */
#define  PANEL_LED_CHECK_1                141     /* control type: radioButton, callback function: led_cmd_cb */
#define  PANEL_TEMP_PELETIER_2            142     /* control type: scale, callback function: (none) */
#define  PANEL_TEMP_PELETIER_1            143     /* control type: scale, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK cmd_write_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_port_change(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_renew_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK led_cmd_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK script_add_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK script_clr_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK script_exec_button_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
