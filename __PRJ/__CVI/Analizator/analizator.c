#include <rs232.h>
#include <windows.h>
#include "formatio.h"
#include "toolbox.h"
#include <cvirte.h>
#include <userint.h>
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/list.h"
#include "process.h"
#include "ini.h"
#include "System/crc.h"
#include "cvi_uart.h"
#include "analizator.h"

#define PARAM_END 0xFFFF
#define SCRIPT_FILE "script.bin"

static int dev_id[] = {PANEL_MOTOR_BUTTON_ID_1, PANEL_MOTOR_BUTTON_ID_2, PANEL_MOTOR_BUTTON_ID_3, PANEL_MOTOR_BUTTON_ID_4, PANEL_MOTOR_BUTTON_ID_5, PANEL_MOTOR_BUTTON_ID_6, PANEL_MOTOR_BUTTON_ID_7, PANEL_MOTOR_BUTTON_ID_8, PANEL_LED_ID, PANEL_SERVO_BUTTON_ID_1, PANEL_SERVO_BUTTON_ID_2, PANEL_SERVO_BUTTON_ID_3, PANEL_SERVO_BUTTON_ID_4, PANEL_SERVO_BUTTON_ID_5, PANEL_SERVO_BUTTON_ID_6, PANEL_SERVO_BUTTON_ID_7, PANEL_KERNEL_ID, PANEL_KERNEL_ID, PANEL_PELETIER_BUTTON_ID_1, PANEL_PELETIER_BUTTON_ID_1, PANEL_PELETIER_BUTTON_ID_1, PANEL_PELETIER_BUTTON_ID_2, PANEL_FUN_BUTTON_ID_1, PANEL_FUN_BUTTON_ID_2, PANEL_LAMP_ID_1, PARAM_END};
static int dev_cmd[] = {PANEL_MOTOR_BUTTON_CMD_1, PANEL_MOTOR_BUTTON_CMD_2, PANEL_MOTOR_BUTTON_CMD_3, PANEL_MOTOR_BUTTON_CMD_4, PANEL_MOTOR_BUTTON_CMD_5, PANEL_MOTOR_BUTTON_CMD_6, PANEL_MOTOR_BUTTON_CMD_7, PANEL_MOTOR_BUTTON_CMD_8, PANEL_LED_BUTTON_CMD, PANEL_SERVO_BUTTON_CMD_1, PANEL_SERVO_BUTTON_CMD_2, PANEL_SERVO_BUTTON_CMD_3, PANEL_SERVO_BUTTON_CMD_4, PANEL_SERVO_BUTTON_CMD_5, PANEL_SERVO_BUTTON_CMD_6, PANEL_SERVO_BUTTON_CMD_7, PANEL_KERNEL_BUTTON_CMD_1, PANEL_KERNEL_BUTTON_CMD_2, PANEL_PELETIER_CMD_1_1, PANEL_PELETIER_CMD_1_2, PANEL_PELETIER_CMD_1_3, PANEL_PELETIER_CMD_2_1, PANEL_FUN_CMD_1, PANEL_FUN_CMD_2, PANEL_LAMP_CMD_1, PARAM_END};
static int dev_param[] = {PANEL_MOTOR_PARAM_1, PANEL_MOTOR_PARAM_2, PANEL_MOTOR_PARAM_3, PANEL_MOTOR_PARAM_4, PANEL_MOTOR_PARAM_5, PANEL_MOTOR_PARAM_6, PANEL_MOTOR_PARAM_7, PANEL_MOTOR_PARAM_8, PANEL_LED_PARAM, PANEL_SERVO_PARAM_1, PANEL_SERVO_PARAM_2, PANEL_SERVO_PARAM_3, PANEL_SERVO_PARAM_4, PANEL_SERVO_PARAM_5, PANEL_SERVO_PARAM_6, PANEL_SERVO_PARAM_7, PANEL_KERNEL_PARAM_1, PANEL_KERNEL_PARAM_2, PANEL_PELETIER_PARAM_1_1, PANEL_PELETIER_PARAM_1_2, PANEL_PELETIER_PARAM_1_3, PANEL_PELETIER_PARAM_2_1, PANEL_FUN_PARAM_1, PANEL_FUN_PARAM_2, PANEL_LAMP_PARAM_1, PARAM_END};

typedef struct {
	uint8_t com_port, script_exec_flag, script_fsm_state;
	u8 uart_tx_buff[100], uart_rx_buff[100], file_buff[250];
	u16 packet_idx, file_idx, packet_tx_len;
	char time[80];
	uint16_t param;
	long int time_live;
	sw_timer__t tmr;
} analizator__struct_t;

typedef struct {
	u8 id_from, id_to;
	char** cmd_tbl;
}cmd_descr_t;

typedef struct {
	u8 id;
	u8 cmd;
	u16 param;
}msg_t;

static analizator__struct_t all_data;

static char* id_tbl[255] = {"Reserved", "Motor_X", "Motor_Y", "Motor_Z", "MOTOR_M1", "MOTOR_M2", "MOTOR_P", "MOTOR_DOOR1", "MOTOR_DOOR2", [13] = "Пелетье 1", "Пелетье 2", "Модуль светодиодов 1", "Модуль светодиодов 2", "Модуль светодиодов 3", "Модуль светодиодов 4", "Модуль светодиодов 5", "Модуль светодиодов 6", "Серводвигатель 1",  "Серводвигатель 2", "Серводвигатель 3", "Серводвигатель 4", "Серводвигатель 5", "Серводвигатель 6", "Серводвигатель 7", "Серводвигатель 8", "Серводвигатель 9", "Серводвигатель 10", "Серводвигатель 11", "Серводвигатель 12",[33] = "Вентилятор 1", "Вентилятор 2", [48] = "УФ-лампа", [250] = "Система"};

static char* motor_cmd_tbl[] = {"Получить позицию", "Перейти в HOME позицию", "Переместиться", "Установить начальную скорость", "Установить макс. скорость", "Установить ускорение", "Установить ток удержания", "Установить рабочий ток", "", ""};
static char* peletier_cmd_tbl[] = {"Установка температуры", "Текущая температура", "Установка мощности", "Установка скорости", "Установка Kp", "Выключить модуль", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", };
static char* led_cmd_tbl[] = {"Вкл\Выкл светодиоды", "Установка яркости", "Установка частоты", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", };
static char* servo_cmd_tbl[] = {"Переместиться на угол ", "Установка таймаута", "Задать начальную скорость", "Задать макситмальную скорость", "Задать ускорение", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", };
static char* fun_cmd_tbl[] = {"Установка скорости", "Установка частоты", "Включить\выключить", "", ""};
static char* lamp_cmd_tbl[] = {"Включить\выключить", "", ""};
static char* kernel_cmd_tbl[] = {"Получить версию ПО", "Получить версию платы", "Информация", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", };
static char* cmd_none_descr[] = {"Нет команды", ""};
//                                             0                           1                        2                     3                           4                         5						6					  7
static cmd_descr_t cmd_tbl_find[] = {{1, 12, motor_cmd_tbl}, {13, 14, peletier_cmd_tbl}, {15, 20, led_cmd_tbl}, {21, 32, servo_cmd_tbl}, {33, 34, fun_cmd_tbl}, {48, 48, lamp_cmd_tbl}, {250, 250, kernel_cmd_tbl}, {0, 0, cmd_none_descr}};

static char* error_tbl[536] = {"Ошибка CRC", "Устройство занято", "Устройство не существует", "Устройство недоступно", "Устройство зависло", "Неверный параметр/команда", "Таймаут операции", "Неизвестная ошибка", ""};

static int cmd_leds_tbl[] = {PANEL_LED_CHECK_1, PANEL_LED_CHECK_2, PANEL_LED_CHECK_3, PANEL_LED_CHECK_4, PANEL_LED_CHECK_5, PANEL_LED_CHECK_6, PANEL_LED_CHECK_7, PANEL_LED_CHECK_8, PARAM_END};
static int cmd_leds_on_tbl[] = {PANEL_LED_1, PANEL_LED_2, PANEL_LED_3, PANEL_LED_4, PANEL_LED_5, PANEL_LED_6, PANEL_LED_7, PANEL_LED_8, PARAM_END};

static void msg_to_text_box(u8* buff);
static u16 packet__set(u8* buff, u16* len);
static void packet__add(u8* buff, u16* len, u8 id, u8 cmd, u16 param);
static void timer_cb (struct sw_timer__t *timer, void *ext_data);
static int CVICALLBACK main_cout(void *functionId);
static int CVICALLBACK uart_thread(void *functionId);
static void set_default_panel();
static void com_ports_check(int panel, int control);
static void script__cout();
static void cmd_to_uir(cmd_descr_t* cmd_tbl, msg_t* msg);

static ini__deskriptor_t* static__ini;
static int panelHandle;
static int file_handle;



static void cmd_to_uir(cmd_descr_t* cmd_tbl, msg_t* msg) {
	float f_val = 0;
	switch((u8)(cmd_tbl - cmd_tbl_find)) {
		case 1:
			switch(msg->cmd) {
				case 0:
				case 1:
					f_val = ((float)msg->param - 1000) / 10;
					SetCtrlVal(panelHandle, msg->id == 14 ? PANEL_TEMP_PELETIER_2 : PANEL_TEMP_PELETIER_1, f_val);
					break;
				default:
					break;
			}
			break;
		case 2:
			for(int i = 0; cmd_leds_on_tbl[i] < PARAM_END; i++)
				SetCtrlVal(panelHandle, cmd_leds_on_tbl[i] ,(msg->param >> i) & 0x1);
			break;
		default:
			break;
	}
}

uint8_t uart_test_cb(uart_event_e event, uint8_t* data, uint32_t len) {
	u8 buff_out[200];
	int idx = 0;
	uart__cout(UART__RX, UART_BUFF, (char*)all_data.uart_rx_buff);
	if (!data)
		event = EVENT__TIMEOUT;
	switch (event) {
		case EVENT__OK:
			while(len > idx + 4) {
				msg_t* msg = (msg_t*)(data + idx);
				cmd_descr_t* cur_cmd_find = cmd_tbl_find;
				while(cur_cmd_find->id_from > 0) {
					if ((msg->id <= cur_cmd_find->id_to) && (msg->id >= cur_cmd_find->id_from))
						break;
					cur_cmd_find++;
				}
				if (cur_cmd_find->id_from == 0)
					msg->id = 0;
				if (msg->param < 65000)
					cmd_to_uir(cur_cmd_find, msg);
				char* true_cmd = cur_cmd_find->cmd_tbl[cur_cmd_find->id_from ? msg->cmd : 0];
				sprintf((char*)buff_out, "%s %s %d (%s) \n", id_tbl[msg->id], true_cmd, msg->param, msg->param > 65000 ? error_tbl[65535 - msg->param] : "данные");
				msg_to_text_box(buff_out);

				idx += (crc__8_ccitt(data + idx, 5) ? 4 : 5);
			}
			all_data.script_fsm_state = 0;
			break;
		default:
			all_data.script_fsm_state = 2;
		case EVENT__TIMEOUT:
			break;
	}
	return 0;
}


static void msg_to_text_box(u8* buff) {
	static int msg_idx = 0;
	char num_buff[80];
	sw_timer__sys_time_t time;
	sw_timer__sys_time_get(&time);
	msg_idx++;
	sprintf(num_buff, "%03d- %02d:%02d:%02d:%03d ", msg_idx, time.timestamp / 3600, (time.timestamp % 3600) / 60, time.timestamp % 60, time.ms % 1000);
	SetCtrlVal (panelHandle, PANEL_TEXT_CMD_ANSWER, num_buff);
	SetCtrlVal (panelHandle, PANEL_TEXT_CMD_ANSWER, (char*)buff);
}



extern void sort();

int main (int argc, char *argv[]) {

	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "analizator.uir", PANEL)) < 0)
		return -1;

	static__ini = ini__create(0, "tester.ini");
	ini__panel_get(static__ini, "windows", panelHandle);
	sw_timer__init(NULL);
	uart_thread(NULL);
	main_cout(NULL);

	sw_timer__start(&all_data.tmr, 1000, timer_cb, (void*)panelHandle);
	int temp = 0;
	ini__int_get(static__ini, "UART", "com_port", &temp);
	all_data.com_port = (u8)temp;
//	tester.timeout = ini__int_get(static__ini, "TESTER", "timeout", tester.timeout);
	com_ports_check(panelHandle, PANEL_COM_CHANGE);
	set_default_panel();
	uart__init(all_data.com_port, uart_test_cb);

	DisplayPanel (panelHandle);
	RunUserInterface ();
	main_cout(NULL);
	uart__deinit();
	uart_thread(NULL);
	sw_timer__stop(&all_data.tmr);
	ini__panel_put(static__ini, "windows", panelHandle);
	DiscardPanel (panelHandle);
	ini__int_put(static__ini, "UART", "com_port", all_data.com_port);
//	ini__int_put(static__ini, "TESTER", "timeout", tester.timeout);
	ini__save(static__ini);
	return 0;
}

int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		QuitUserInterface(0);
	return 0;
}

int CVICALLBACK com_renew_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		com_ports_check(panel, PANEL_COM_CHANGE);
	return 0;
}

int CVICALLBACK button_write_callback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)
		return 0;
	return 0;
}


static void timer_cb (struct sw_timer__t *timer, void *callbackData) {
	sw_timer__start(timer, -1, timer_cb, callbackData);
	all_data.time_live++;
	sprintf(all_data.time, "%3dд:%02dч:%02dм:%02dс", all_data.time_live/86400, (all_data.time_live%86400)/3600, (all_data.time_live%3600)/60, all_data.time_live%60);
	SetCtrlVal ((int)callbackData, PANEL_TIME_PROGRAM, all_data.time);
}

static int CVICALLBACK main_cout(void *functionId) {
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(200));
		return 0;
	}
	while(*(int*)functionId)
		Sleep(1), framework__cout(), script__cout();
	*(int*)functionId = -1;
	return 0;
}

static int CVICALLBACK uart_thread(void *functionId) {
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, uart_thread, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(200));
		return 0;
	}
	while(*(int*)functionId)
		Sleep(1), uart__cout(0, 0, NULL);
	*(int*)functionId = -1;
	return 0;
}

static void com_ports_check(int panel, int control) {
	int com = -1;
	char name[24];
	ClearListCtrl (panel, control);
	while(++com <= 255) {
		char ncom[24];
		sprintf(ncom, "\\\\.\\COM%d", com);
		HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
		uint32_t error = 0;
		if(hport == INVALID_HANDLE_VALUE)
			switch(error = GetLastError()) {
				case 5:
					break;
				default:
					continue;
			}
		else
			CloseHandle(hport);
		sprintf(name, "COM %d%s", com, error != 5? "": " (занят)");
		InsertListItem (panel, control, -1, name, com);
	}
	sprintf(name, "COM %d%s", all_data.com_port, " !");
	InsertListItem (panel, control, -1, name, all_data.com_port);

}

static void set_default_panel() {
	SetCtrlVal (panelHandle, PANEL_COM_CHANGE, all_data.com_port);
}

int CVICALLBACK com_port_change (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)
		return 0;
	GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &all_data.com_port);
	uart__init(all_data.com_port, uart_test_cb);
	return 0;
}

int CVICALLBACK param_change_cb (int panel, int control, int event,
								 void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)
		return 0;
	return 0;
}


int CVICALLBACK cmd_write_cb (int panel, int control, int event,
							  void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			int i = -1;
			u8 id, cmd;
			u16 param;
			float f_param;
			while(dev_id[++i] < PARAM_END) {
				GetCtrlVal (panelHandle, dev_id[i], &id);
				GetCtrlVal (panelHandle, dev_cmd[i], &cmd);
				if ((id < 255) && (cmd < 255)) {
					if ((id == 13) || (id == 14)) {
						GetCtrlVal (panelHandle, dev_param[i], &f_param);
						param = cmd ? f_param : f_param * 10 + 1000;
					}
					else
						GetCtrlVal (panelHandle, dev_param[i], &param);
					packet__add(all_data.uart_tx_buff, &all_data.packet_idx, id, cmd, param);
				}
			}
			if (all_data.packet_idx) {
				file_handle = OpenFile (SCRIPT_FILE, VAL_READ_WRITE, VAL_APPEND, VAL_BINARY);
				CloseFile(file_handle);
				all_data.packet_tx_len = packet__set(all_data.uart_tx_buff, &all_data.packet_idx);
			}
			break;
	}
	return 0;
}

static void packet__add(u8* buff, u16* len, u8 id, u8 cmd, u16 param) {
	u8 msg_buff[4];
	msg_buff[0] = id;
	msg_buff[1] = cmd;
	*(u16*)&msg_buff[2] = param;
	memcpy(buff + *len, msg_buff, 4);
	*len += 4;
}

static u16 packet__set(u8* buff, u16* length) {
	u16 len = *length;
	u8 crc = crc__8_ccitt(buff, len);
	buff[len] = crc;
	*length = 0;
	return len + 1;
}


int CVICALLBACK led_cmd_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_VAL_CHANGED:
			u8 param;
			u16 cmd_param = 0;
			int buff;
			GetCtrlVal (panelHandle, PANEL_LED_BUTTON_CMD, &param);
			for(int i = 0; cmd_leds_tbl[i] < PARAM_END; i++) {
				SetCtrlAttribute (panelHandle, cmd_leds_tbl[i], ATTR_DIMMED, param != 0);
				GetCtrlVal(panelHandle, cmd_leds_tbl[i], &buff);
				cmd_param |= buff << i;
			}
			if (!param)
				SetCtrlVal (panelHandle, PANEL_LED_PARAM, cmd_param);
			break;
	}
	return 0;
}

int CVICALLBACK script_clr_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			file_handle = OpenFile (SCRIPT_FILE, VAL_READ_WRITE, VAL_TRUNCATE, VAL_BINARY);
			CloseFile(file_handle);
			break;
	}
	return 0;
}

int CVICALLBACK script_add_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			int i = -1;
			u8 id, cmd;
			u16 param, buff_len;
			float f_param;
			while(dev_id[++i] < PARAM_END) {
				GetCtrlVal (panelHandle, dev_id[i], &id);
				GetCtrlVal (panelHandle, dev_cmd[i], &cmd);
				if ((id < 255) && (cmd < 255)) {
					if ((id == 13) || (id == 14)) {
						GetCtrlVal (panelHandle, dev_param[i], &f_param);
						param = cmd ? f_param : f_param * 10 + 1000;
					}
					else
						GetCtrlVal (panelHandle, dev_param[i], &param);
					packet__add(all_data.file_buff, &all_data.file_idx, id, cmd, param);
				}
			}
			if (all_data.file_idx) {
				buff_len = packet__set(all_data.file_buff, &all_data.file_idx);
				all_data.file_buff[buff_len] = 0x00;
				all_data.file_buff[buff_len + 1] = 0x00;
				all_data.file_buff[buff_len + 2] = 0x00;
				all_data.file_buff[buff_len + 3] = 0x00;
				file_handle = OpenFile (SCRIPT_FILE, VAL_READ_WRITE, VAL_APPEND, VAL_BINARY);
				WriteFile (file_handle, (char *)all_data.file_buff, buff_len + 4);
				CloseFile(file_handle);
			}
			break;
	}
	return 0;
}

int CVICALLBACK script_exec_button_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			file_handle = OpenFile (SCRIPT_FILE, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_BINARY);
			all_data.script_fsm_state = 0;
			all_data.script_exec_flag = 1;

			break;
	}
	return 0;
}


static void script__cout() {
	if (all_data.packet_tx_len) {
		if (uart__cout(UART__TX, all_data.packet_tx_len, (char*)all_data.uart_tx_buff) == EVENT__OK)
			all_data.packet_tx_len = 0;
	}
	if (!all_data.script_exec_flag)
		return;
	static u8 cur_idx;
	switch(all_data.script_fsm_state) {
		case 0:
			cur_idx = ReadFile(file_handle, (char *)all_data.file_buff, 5) == 5 ? 5 : 0;
			while ((ReadFile(file_handle, (char *)all_data.file_buff + cur_idx, 4) > 0) && (*(u32*)(all_data.file_buff + cur_idx))) cur_idx += 4;
			if (cur_idx < 5) {
				all_data.script_exec_flag = 0;
				all_data.script_fsm_state = 0;
				CloseFile(file_handle);
				return;
			}
			all_data.script_fsm_state = 1;
			if (uart__cout(UART__TX, cur_idx, (char*)all_data.file_buff) != EVENT__OK)
				all_data.script_fsm_state = 2;
			break;
		case 1:
			break;
		case 2:
			if (uart__cout(UART__TX, cur_idx, (char*)all_data.file_buff) == EVENT__OK)
				all_data.script_fsm_state = 1;
			break;
	}
}
