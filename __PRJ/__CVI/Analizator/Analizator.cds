<distribution version="15.0.0" name="Analizator" type="MSI">
	<prebuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></prebuild>
	<postbuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></postbuild>
	<msi GUID="{510341A6-3E16-4200-8FCF-1EAB06D08A0C}">
		<general appName="Analizator" outputLocation="w:\M_PRJ\__PRJ\__CVI\Analizator\cvidistkit.Analizator" relOutputLocation="cvidistkit.Analizator" outputLocationWithVars="w:\M_PRJ\__PRJ\__CVI\Analizator\cvidistkit.%name" relOutputLocationWithVars="cvidistkit.%name" upgradeBehavior="2" autoIncrement="true" version="1.0.21">
			<arp company="" companyURL="" supportURL="" contact="" phone="" comments=""/>
			<summary title="" subject="" keyWords="" comments="" author=""/></general>
		<userinterface language="English" showPaths="true" showRuntimeOnly="true" readMe="" license="">
			<dlgstrings welcomeTitle="Analizator" welcomeText=""/></userinterface>
		<dirs appDirID="100">
			<installDir name="Analizator" dirID="100" parentID="2" isMSIDir="false" visible="true" unlock="false"/>
			<installDir name="Analizator" dirID="101" parentID="7" isMSIDir="false" visible="true" unlock="false"/>
			<installDir name="[Program Files]" dirID="2" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="[Desktop]" dirID="0" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="[Start&gt;&gt;Programs]" dirID="7" parentID="-1" isMSIDir="true" visible="true" unlock="false"/></dirs>
		<files>
			<simpleFile fileID="0" sourcePath="C:\Windows\SYSTEM32\ADVAPI32.dll" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="1" sourcePath="w:\M_PRJ\__PRJ\__CVI\Analizator\analizator.uir" relSourcePath="analizator.uir" relSourceBase="0" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="2" sourcePath="C:\Windows\SYSTEM32\USER32.dll" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="3" sourcePath="w:\M_PRJ\__PRJ\__CVI\Analizator\cvibuild.analizator\Release\analizator.exe" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="4" sourcePath="C:\Windows\SYSTEM32\KERNEL32.dll" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/></files>
		<fileGroups>
			<projectOutput targetType="0" dirID="100" projectID="0">
				<fileID>3</fileID></projectOutput>
			<projectDependencies dirID="100" projectID="0">
				<fileID>4</fileID>
				<fileID>0</fileID>
				<fileID>2</fileID></projectDependencies></fileGroups>
		<shortcuts>
			<shortcut name="Analizator" targetFileID="3" destDirID="101" cmdLineArgs="" description="" runStyle="NORMAL"/>
			<shortcut name="Analizator" targetFileID="3" destDirID="0" cmdLineArgs="" description="" runStyle="NORMAL"/></shortcuts>
		<mergemodules/>
		<products/>
		<runtimeEngine installToAppDir="false" activeXsup="false" analysis="true" cvirte="true" dotnetsup="true" instrsup="true" lowlevelsup="false" lvrt="true" netvarsup="true" rtutilsup="false">
			<hasSoftDeps/>
			<doNotAutoSelect>
			<component>activeXsup</component>
			<component>lowlevelsup</component>
			<component>rtutilsup</component>
			</doNotAutoSelect></runtimeEngine><sxsRuntimeEngine>
			<selected>false</selected>
			<doNotAutoSelect>false</doNotAutoSelect></sxsRuntimeEngine>
		<advanced mediaSize="650">
			<launchConditions>
				<condition>MINOS_WINXP_SP0</condition>
			</launchConditions>
			<includeConfigProducts>true</includeConfigProducts>
			<maxImportVisible>silent</maxImportVisible>
			<maxImportMode>merge</maxImportMode>
			<custMsgFlag>false</custMsgFlag>
			<custMsgPath>msgrte.txt</custMsgPath>
			<signExe>false</signExe>
			<certificate></certificate>
			<signTimeURL></signTimeURL>
			<signDescURL></signDescURL></advanced>
		<baselineProducts>
			<product name="NI LabWindows/CVI Shared Runtime 2019" UC="{80D3D303-75B9-4607-9312-E5FC68E5BFD2}" productID="{2EB63900-C920-494E-9F7B-56E6B2DCBEE9}" path="(None)" flavorID="_full_" flavorName="Full" verRestr="false" coreVer="19.0.49152">
				<dependencies>
					<productID>{0946CDFA-C3C6-4AD5-B884-FB72701ED351}</productID>
					<productID>{14B23AC5-B7EF-47D1-A57D-8666BAEE13C3}</productID>
					<productID>{16BC0547-DF57-48CF-8CB5-1CC0DF3B4911}</productID>
					<productID>{192594FE-5397-4F45-8137-A5C487CE3849}</productID>
					<productID>{3FFCB06E-2075-4D82-8C79-C8E6EDDED7C1}</productID>
					<productID>{4C351C60-5ECC-4920-AEB6-DFE60EA14283}</productID>
					<productID>{5836AF9F-665E-43CD-A8DC-BEFC5946F5A3}</productID>
					<productID>{5B4D362F-8203-490C-82D1-5F607972196F}</productID>
					<productID>{64ECB814-3A6A-4E48-9D2F-D6C2EDD725B7}</productID>
					<productID>{6E1BCD5B-41A1-4E69-8C6E-389F57A19F00}</productID>
					<productID>{75191165-D39E-42A2-A7A2-D74AE99F8A84}</productID>
					<productID>{7C5B33C5-A647-434E-884E-8C4C20D76B41}</productID>
					<productID>{7EE28BBA-8A24-40B1-B0D0-9066A8CFA4AA}</productID>
					<productID>{84F53873-9E42-4A30-B0A7-60A2C6B182DD}</productID>
					<productID>{93977567-FFEC-453C-A47F-CE30077E9F4B}</productID>
					<productID>{93BC54EA-8C1A-45A5-8DCF-C778C71255FD}</productID>
					<productID>{99D3B9D7-C0E0-41A5-8945-CF13FDE63435}</productID>
					<productID>{9B14BAA8-31A5-463B-88C8-EC81F67BEE57}</productID>
					<productID>{A59D9F6E-27DE-410C-9820-9D844DC78D05}</productID>
					<productID>{AFC999BB-F270-46EF-B748-AE755EC75322}</productID>
					<productID>{C0A9F5E2-DCD5-44C1-8B03-C560F4C06D6C}</productID>
					<productID>{CE61F080-FB64-4F6C-9763-A3060A0E59E6}</productID>
					<productID>{D1027BA0-C959-44E2-B4FA-10386404AF75}</productID>
					<productID>{D37DC7B4-F7DD-45D9-8CD8-30B4893E811E}</productID>
					<productID>{D72C00A8-14F3-4E45-AFAC-5A71F833E210}</productID>
					<productID>{E0DAF96D-BF67-46CD-87A7-45E7E12A8074}</productID>
					<productID>{E12F09FF-07B3-4677-8D5C-BDD01E9A5545}</productID>
					<productID>{E41AC0E5-CC2C-495B-8C26-DFC9B9E8CB04}</productID>
					<productID>{E4776418-A44C-4A5E-A5F6-3A4B710A2FE6}</productID>
					<productID>{EB6C9E35-CBA2-4C2B-8958-55EA6F0EA707}</productID>
					<productID>{EEA5281C-7DA9-492E-8F6F-4127DC67AAD6}</productID>
					<productID>{F2EEE63A-0DCA-41BF-A243-4E4C0DFA38A4}</productID>
					<productID>{F456DB40-F5DF-45FE-A582-83DBED5D6432}</productID>
					<productID>{FB67912C-BB8D-4FE2-B64E-7712EB90DEA4}</productID></dependencies></product></baselineProducts>
		<Projects NumProjects="1">
			<Project000 ProjectID="0" ProjectAbsolutePath="w:\M_PRJ\__PRJ\__CVI\Analizator\analizator.prj" ProjectRelativePath="analizator.prj"/></Projects>
		<buildData progressBarRate="4.384992256111246">
			<progressTimes>
				<Begin>0.000000000000000</Begin>
				<ProductsAdded>0.186518908733181</ProductsAdded>
				<DPConfigured>1.074546315744887</DPConfigured>
				<DPMergeModulesAdded>1.670207656748750</DPMergeModulesAdded>
				<DPClosed>2.433090438769495</DPClosed>
				<DistributionsCopied>2.572986963876596</DistributionsCopied>
				<End>22.805057377383665</End></progressTimes></buildData>
	</msi>
</distribution>
