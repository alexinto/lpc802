/*******************************************************************************
* discrete_inputs functions
*******************************************************************************/

#include "temperature.h"
#include <string.h>
#include <math.h>

//******************************************************************************
// Define section
#define TEMP_SHIFT_ADC  300

#define SCALE_FACTOR_RESISTANCE   183.82
#define Z1 (-3.9083E-3)     // Z1 coef. of positive reverse transfer function
#define Z2 (17.58480889E-6) // Z2 coef. of positive reverse transfer function
#define Z3 (-23.10E-9)      // Z3 coef. of positive reverse transfer function
#define Z4 (-1.155E-6)      // Z4 coef. of positive reverse transfer function
#define TMIN (-50.0)
#define TMAX (200.0)
#define RMIN 80.5
#define RMAX 178.0

#define TEMP_CALIBRATION_MODE_1 100.0
#define TEMP_CALIBRATION_MODE_2 150.0

// ��� ���200-61.2
#define TMIN_405212 (-50.0)   // �� �������� �� ����.405212.003��
#define TMAX_405212 (70.0)   // �� �������� �� ����.405212.003��
#define RMIN_405212 (78.46)   // �� ���� 6651-2009, ������� �.3
#define RMAX_405212 (129.96)  // �� ���� 6651-2009, ������� �.3
#define R0_100M     (100.0)   // ������� ��� 0 �
#define A_100M      (0.00428) // ������������� ����������� 100� ��� ����.405212.003��, 1/C

// ��� ���200-61.3
#define TMIN_DTS015  (-50.0)   // �� ����������� �� ������������ �� ���015
#define TMAX_DTS015  (180.0)   // �� ����������� �� ������������ �� ���015
#define RMIN_DTS015 (39.23)   // �� ���� 6651-2009, ������� �.3, �������� �� 2 
#define RMAX_DTS015 (88.52)  // �� ���� 6651-2009, ������� �.3, �������� �� 2

//******************************************************************************
// Typedef section

//******************************************************************************
//  Prototype section


/*******************************************************************************
* temperature_convert_rsens_to_temp 
*
*
*******************************************************************************/
float temperature_convert_rsens_to_temp (float r) 
{
  float t;

  // first determine if input resistance is within spec'd range
  if (r<RMIN)           // if input is under-range..
    t = TMIN;           // ..then set to minimum of range
  else if (r>RMAX)      // if input is over-range..
    t = TMAX;           // ..then set to maximum of range

  // if input (r) is within range, then solve for output.
  else {
      if (r < 100)   // SV ������ �� 100  
          t=-242.0906+2.227625*r+2.517790E-3*pow(r,2)-5.861951E-6*pow(r,3);
      else 
          t=(Z1+sqrt(Z2+Z3*r))/Z4;
  }
  return (t);
}

/*******************************************************************************
* temperature_convert_rsens_to_temp_100� 
* ������� ������� ����������� ��� 100� ��. ���� 6651-2009, ����. �-2
*******************************************************************************/
#define R0 (100.00)
#define A (3.9690E-3)       // A coef. of positive reverse transfer function
#define B (-5.841E-7) 	    // B coef. of positive reverse transfer function
#define D1 (251.903)        // D1 coef. of positive reverse transfer function
#define D2 (8.80035)        // D2 coef. of positive reverse transfer function
#define D3 (-2.91506)       // D3 coef. of positive reverse transfer function
#define D4 (1.67611)        // D4 coef. of positive reverse transfer function


float temperature_r_to_temp (float r) {
  float t, X;
  if (r < 100.0) {
	  X = r / R0 - 1;
	  t = D1*X + D2*pow(X,2) + D3*pow(X,3) + D4*pow(X,4);
  }
  else {
	  X = 1 - r / R0;	  
	  X = pow(A, 2) - 4 * B * X;
	  X = sqrt(X) - A;
	  t = X / (2 * B);
  }
  return (t);
}


float temperature_temp_to_r (float t) {
	float r, x;
	if (t < 0) {
		
	}
	else {
		x = -B * pow(t, 2) - A * t;
		r = (1 - x) * R0;
	}
	return r;
}
