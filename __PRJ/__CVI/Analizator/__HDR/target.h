#define SUPERVISOR__IDLE_OFF


#include <utility.h>

#define SUPERVISOR__IDLE_OFF

static int critical_section_lock;

#define CRITICAL_SECTION_ON {    if(critical_section_lock == 0)     \
                                    CmtNewLock (NULL, 0, &critical_section_lock);  \
                                 CmtGetLock (critical_section_lock);

#define CRITICAL_SECTION_OFF      CmtReleaseLock (critical_section_lock);}

#define CVI__BREAK_ERR_OFF { int err_off_old = SetBreakOnLibraryErrors(0);
#define CVI__BREAK_ERR_ON  SetBreakOnLibraryErrors(err_off_old);}
