/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_COM_RENEW                  2       /* control type: command, callback function: com_renew_cb */
#define  PANEL_QUITBUTTON                 3       /* control type: command, callback function: QuitCallback */
#define  PANEL_LED_3                      4       /* control type: LED, callback function: (none) */
#define  PANEL_LED_2                      5       /* control type: LED, callback function: (none) */
#define  PANEL_LED_1                      6       /* control type: LED, callback function: (none) */
#define  PANEL_DECORATION_6               7       /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION                 8       /* control type: deco, callback function: (none) */
#define  PANEL_BINARYSWITCH_1             9       /* control type: binary, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_3             10      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_2             11      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_1             12      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TEXTMSG_7                  13      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG                    14      /* control type: textMsg, callback function: (none) */
#define  PANEL_DECORATION_5               15      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_3               16      /* control type: deco, callback function: (none) */
#define  PANEL_TEXTMSG_2                  17      /* control type: textMsg, callback function: (none) */
#define  PANEL_GRAPH_2                    18      /* control type: graph, callback function: (none) */
#define  PANEL_TEXTBOX                    19      /* control type: textBox, callback function: (none) */
#define  PANEL_TEXTMSG_6                  20      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_43                21      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_44                22      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_45                23      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_34                24      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_16                25      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_35                26      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_31                27      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_17                28      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_36                29      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_40                30      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_32                31      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_18                32      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_37                33      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_25                34      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_41                35      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_33                36      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_26                37      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_55                38      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_38                39      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_27                40      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_52                41      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_42                42      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_28                43      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_49                44      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_29                45      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_56                46      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_39                47      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_46                48      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_30                49      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_61                50      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_58                51      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_53                52      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_19                53      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_20                54      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_50                55      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_21                56      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_57                57      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_22                58      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_47                59      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_62                60      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_23                61      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_59                62      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_54                63      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_24                64      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_2                 65      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_51                66      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_3                 67      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_4                 68      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_5                 69      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_64                70      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_63                71      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_48                72      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_6                 73      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_60                74      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_7                 75      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_8                 76      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_9                 77      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_10                78      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_11                79      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_12                80      /* control type: textMsg, callback function: (none) */
#define  PANEL_PICTURE                    81      /* control type: picture, callback function: (none) */
#define  PANEL_CHIP_NUM_13                82      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_14                83      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_1                 84      /* control type: textMsg, callback function: (none) */
#define  PANEL_CHIP_NUM_15                85      /* control type: textMsg, callback function: (none) */
#define  PANEL_GRAPH_CHIPS_ERROR          86      /* control type: graph, callback function: (none) */
#define  PANEL_TIME_PROGRAM               87      /* control type: string, callback function: (none) */
#define  PANEL_COM_CHANGE                 88      /* control type: ring, callback function: com_port_change */
#define  PANEL_TIMEOUT_VALUE              89      /* control type: scale, callback function: tester_timeout_cb */
#define  PANEL_TIMEOUT_VIEW               90      /* control type: numeric, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK buttons_callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_port_change(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_renew_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK tester_timeout_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
