/***************************************************************************//**
 * @file target.h.
 * @brief ������ ��������� ��� �������� ����������.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TARGET_H_
#define TARGET_H_

#include <utility.h>

#define UART_BUFF 50
#define UART_BAUDRATE 19200
#define UART_TIMEOUT 1

#define TEST_TIMER 			0
#define TEST_TIMER_1 		1
#define TEST_TIMER_2 		2
#define TEST_TIMER_3 		3
#define TEST_TIMER_4 		4
#define TEST_TIMER_5 		5
#define TEST_TIMER_GRAPH	6
#define TEST_TIMER_COOLER	7
#define TIMER_CHIP_NUM		8
#define MAX_TIMER   		9


#define SUPERVISOR__IDLE_OFF


static int critical_section_lock;

#define CRITICAL_SECTION_ON  	do {                                                         \
								    if(critical_section_lock == 0)                           \
								        CmtNewLock (NULL, 0, &critical_section_lock);        \
								    CmtGetLock (critical_section_lock);

#define CRITICAL_SECTION_OFF 		CmtReleaseLock (critical_section_lock);                  \
								}while(0);

#define CVI__BREAK_ERR_OFF { int err_off_old = SetBreakOnLibraryErrors(0);
#define CVI__BREAK_ERR_ON  SetBreakOnLibraryErrors(err_off_old);}







#endif
