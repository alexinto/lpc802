/***************************************************************************//**
 * @file drv_asic_check.h.
 * @brief ���� � ��������� ��������� ������ ������� �������� �����.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DRV_ASIC_CHECK_H
#define DRV_ASIC_CHECK_H

#include "System/events.h"

typedef enum {
    CHECK_S9   = 0,
    CHECK_NONE = 1,
}drv_asic_check__id_e;

typedef enum {
    CHECK_SIMPLE       = 0, // ������� ����� �����
    CHECK_RESET        = 1, // ����� ����� �� ������� ����������
    CHECK_CMD_NONE     = 2,
}drv_asic_check__cmd_e;

typedef void (*drv_asic_check__cmd_exec_cb)(int asic_id, events__e event, int param, void* ext_data);


events__e drv_asic_check__cmd_exec(drv_asic_check__id_e id, drv_asic_check__cmd_e cmd, int param, drv_asic_check__cmd_exec_cb cb, void* ext_data);










#endif