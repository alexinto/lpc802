/***************************************************************************//**
 * @file cvi_uart.c.
 * @brief ���� � ��������� uart.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef _H_CVI_UART
#define _H_CVI_UART

#include "target.h"
#include "System/framework.h"

#ifndef UART_BUFF
	#define UART_BUFF 200
#endif
#ifndef UART_TIMEOUT
	#define UART_TIMEOUT 0.2
#endif
#ifndef UART_BAUDRATE
	#define UART_BAUDRATE 9600
#endif


typedef enum {
	UART__DEINIT = -2,
	UART__INIT,
	UART__IDLE = 0,
	UART__TX,
	UART__RX,
}uart_state_e;



typedef uint8_t (*uart__cb)(events__e event, u8* data, uint32_t len);


events__e uart__init(int uart_id, uart__cb cb);

void uart__deinit();

events__e uart__cout(uart_state_e state, u8 len, u8* buff);





















#endif
