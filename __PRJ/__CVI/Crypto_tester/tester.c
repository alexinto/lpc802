#include <rs232.h>
#include <windows.h>
#include <synchapi.h>
#include "toolbox.h"
#include <cvirte.h>
#include <userint.h>
#include "tester.h"
#include "target.h"
#include "process.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "ini.h"
#include "cvi_uart.h"
#include "System/crc.h"
#include "drv_asic_pwr.h"
#include "drv_asic_check.h"


typedef struct
{
	u8 id;
	u8 cmd;
	u16 param;
} msg_t;

typedef enum
{
	ASIC_WAIT = 0,
	ASIC_START,
	ASIC_PROC,
	ASIC_STOP,
} asic_state_e;

typedef struct
{
	asic_state_e state;
	unsigned int asics;
	unsigned int req 	:1;
} asic_struct_t;

typedef struct
{
	char on 	 :1;
	char on_set  :1;
	char off_set :1;
	char online  :1;
	char current_value, current_set_value, fun_value;
	float voltage, current, temperature;
	int timeout, power, uart_id;
	int uart_err, uart_packet, uart_no_cmd;
	u8 tx_len, rx_len;
	asic_struct_t asics;
	u8 buff_tx[UART_BUFF], buff_rx[UART_BUFF];
} tester_struct_t;

static void TimerCallback (struct sw_timer__t *timer, void *ext_data);
static int CVICALLBACK main_cout(void *functionId);
static int CVICALLBACK uart_thread(void *functionId);
static void test_asic();
static void set_default_panel();
static void com_ports_check(int panel, int control);
uint8_t uart_test_cb(events__e event, uint8_t* data, uint32_t len);
static void packet__add(u8* buff, u8* len, u8 id, u8 cmd, u16 param);
static u8 packet__set(u8* buff, u8* length);
static void draw_chip_num(u8 chip_num);

static ini__deskriptor_t* static__ini;
static tester_struct_t tester = {.asics = {.state = ASIC_STOP}};
static int panelHandle;
static sw_timer__t timers[MAX_TIMER + 1];
static long int time_program = 0;
static int temp_y[20], temp_x[20] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50};
static int chip_tbl[] = {PANEL_CHIP_NUM_1, PANEL_CHIP_NUM_2, PANEL_CHIP_NUM_3, PANEL_CHIP_NUM_4, PANEL_CHIP_NUM_5,
						 PANEL_CHIP_NUM_6, PANEL_CHIP_NUM_7, PANEL_CHIP_NUM_8, PANEL_CHIP_NUM_9, PANEL_CHIP_NUM_10,
						 PANEL_CHIP_NUM_11, PANEL_CHIP_NUM_12, PANEL_CHIP_NUM_13, PANEL_CHIP_NUM_14, PANEL_CHIP_NUM_15,
						 PANEL_CHIP_NUM_16, PANEL_CHIP_NUM_17, PANEL_CHIP_NUM_18, PANEL_CHIP_NUM_19, PANEL_CHIP_NUM_20,
						 PANEL_CHIP_NUM_21, PANEL_CHIP_NUM_22, PANEL_CHIP_NUM_23, PANEL_CHIP_NUM_24, PANEL_CHIP_NUM_25,
						 PANEL_CHIP_NUM_26, PANEL_CHIP_NUM_27, PANEL_CHIP_NUM_28, PANEL_CHIP_NUM_29, PANEL_CHIP_NUM_30,
						 PANEL_CHIP_NUM_31, PANEL_CHIP_NUM_32, PANEL_CHIP_NUM_33, PANEL_CHIP_NUM_34, PANEL_CHIP_NUM_35,
						 PANEL_CHIP_NUM_36, PANEL_CHIP_NUM_37, PANEL_CHIP_NUM_38, PANEL_CHIP_NUM_39, PANEL_CHIP_NUM_40,
						 PANEL_CHIP_NUM_41, PANEL_CHIP_NUM_42, PANEL_CHIP_NUM_43, PANEL_CHIP_NUM_44, PANEL_CHIP_NUM_45,
						 PANEL_CHIP_NUM_46, PANEL_CHIP_NUM_47, PANEL_CHIP_NUM_48, PANEL_CHIP_NUM_49, PANEL_CHIP_NUM_50,
						 PANEL_CHIP_NUM_51, PANEL_CHIP_NUM_52, PANEL_CHIP_NUM_53, PANEL_CHIP_NUM_54, PANEL_CHIP_NUM_55,
						 PANEL_CHIP_NUM_56, PANEL_CHIP_NUM_57, PANEL_CHIP_NUM_58, PANEL_CHIP_NUM_59, PANEL_CHIP_NUM_60,
						 PANEL_CHIP_NUM_61, PANEL_CHIP_NUM_62, PANEL_CHIP_NUM_63, PANEL_CHIP_NUM_64
						};
static unsigned int chips_err_count[64];



static void test_asic()
{
	switch(tester.asics.state)
	{
		case ASIC_START:
			break;
		case ASIC_PROC:
			break;
		case ASIC_STOP:
			break;
		case ASIC_WAIT:
			break;
	}
}


int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "tester.uir", PANEL)) < 0)
		return -1;
	static__ini = ini__create(0, "tester.ini");
	ini__panel_get(static__ini, "windows", panelHandle);
	int temp = 0;
	ini__int_get(static__ini, "UART", "com_port", &temp);
	tester.uart_id = temp;
	ini__int_get(static__ini, "TESTER", "timeout", &temp);
	tester.timeout = temp;
	uart_thread(NULL);
	main_cout(NULL);

	sw_timer__init(NULL);
	uart__init(tester.uart_id, uart_test_cb);

	sw_timer__start(&timers[TEST_TIMER], 1000, TimerCallback, (void*)panelHandle);
	com_ports_check(panelHandle, PANEL_COM_CHANGE);
	if (tester.timeout < 200)
		tester.timeout = 200;
	set_default_panel();

	DisplayPanel (panelHandle);
	RunUserInterface ();
	uart__deinit();
	for(int x = 0; x < 9; x++)
		sw_timer__stop(&timers[x]);
	main_cout(NULL);
	uart_thread(NULL);
	ini__panel_put(static__ini, "windows", panelHandle);
	DiscardPanel (panelHandle);
	ini__int_put(static__ini, "UART", "com_port", tester.uart_id);
	ini__int_put(static__ini, "TESTER", "timeout", tester.timeout);
	ini__save(static__ini);
	return 0;
}

int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	if (event == EVENT_COMMIT)
		QuitUserInterface(0);
	return 0;
}

int CVICALLBACK tester_timeout_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	double temp;
	if (event == EVENT_VAL_CHANGED)
	{
		GetCtrlVal(panel, control, &temp);
		SetCtrlVal(panel, PANEL_TIMEOUT_VIEW, temp);
		tester.timeout = temp * 1000;
	}
	return 0;
}

int CVICALLBACK com_renew_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	if (event == EVENT_COMMIT)
		com_ports_check(panel, PANEL_COM_CHANGE);
	return 0;
}

int CVICALLBACK com_port_change (int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	if (event != EVENT_COMMIT)
		return 0;
	GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &tester.uart_id);
	uart__init(tester.uart_id, uart_test_cb);
	return 0;
}


int CVICALLBACK buttons_callback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	int x, temp, cntrls[MAX_TIMER] = {0, PANEL_TOGGLEBUTTON_1, PANEL_TOGGLEBUTTON_2, PANEL_TOGGLEBUTTON_3, 0, 0, 0, 0, PANEL_BINARYSWITCH_1, 0};
	int timeout = 1000;
	if (event != EVENT_COMMIT)
		return 0;
	GetCtrlVal (panel, control, &temp);
	for(x = 0; (x <= MAX_TIMER) && (control != cntrls[x]); x++);
	if ((temp == 1) && (x <= MAX_TIMER))
	{
		if (x == TEST_TIMER_1)
		{
			timeout = tester.timeout;
			tester.tx_len = 0;
			packet__add(tester.buff_tx, &tester.tx_len, 0x01, PWR_ON_OFF, 1);
			uart__cout(UART__TX, packet__set(tester.buff_tx, &tester.tx_len), tester.buff_tx);
		}
		else if (x == TIMER_CHIP_NUM)
		{
			DeleteGraphPlot (panel, PANEL_GRAPH_CHIPS_ERROR, -1, VAL_IMMEDIATE_DRAW);
			for(int count = 0; count < 63; count++)
			{
				SetCtrlAttribute (panel, chip_tbl[count], ATTR_VISIBLE, 0);
				chips_err_count[count] = 0;
			}
			tester.tx_len = 0;
			packet__add(tester.buff_tx, &tester.tx_len, 0x0D, CHECK_SIMPLE, 0);
			uart__cout(UART__TX, packet__set(tester.buff_tx, &tester.tx_len), tester.buff_tx);
		}
		sw_timer__start(&timers[x], timeout, TimerCallback, (void*)panel);
	}
	else
	{
		if (x == TEST_TIMER_1)
		{
			packet__add(tester.buff_tx, &tester.tx_len, 0x01, PWR_ON_OFF, 0);
			uart__cout(UART__TX, packet__set(tester.buff_tx, &tester.tx_len), tester.buff_tx);
		}
		sw_timer__stop(&timers[x]);
	}
	return 0;
}

static void TimerCallback (struct sw_timer__t *timer, void *callbackData)
{
	int x, buff, leds[] = {PANEL_TIME_PROGRAM, PANEL_LED_1, PANEL_LED_2, PANEL_LED_3, 0, 0, 0};
	char temp_char[80];
	sw_timer__start(timer, timer->timeout_ms, TimerCallback, callbackData);
	for(x = 0; (x <= MAX_TIMER) && (&timers[x] != timer); x++);
	switch (x)
	{
		case TEST_TIMER:
			buff = ++time_program;
			sprintf(temp_char, "%3dд:%02dч:%02dм:%02dс", buff/86400, (buff%86400)/3600, (buff%3600)/60, buff%60);
			SetCtrlVal((int)callbackData, leds[x], temp_char);
			break;
		case TEST_TIMER_1:
			sw_timer__start(timer, tester.timeout, TimerCallback, callbackData);
			GetCtrlVal ((int)callbackData, leds[x], &buff);
			buff ^= 1;
			SetCtrlVal ((int)callbackData, leds[x], buff);
			break;
		case TEST_TIMER_2:
		case TEST_TIMER_3:
			GetCtrlVal ((int)callbackData, leds[x], &buff);
			buff ^= 1;
			SetCtrlVal ((int)callbackData, leds[x], buff);
			break;
		case TEST_TIMER_GRAPH:
			temp_x[19] = tester.uart_packet;
			for(x = 0; x < 19; x++)
				temp_x[x] = temp_x[x + 1];
			break;
		case TEST_TIMER_COOLER:
			temp_y[19] = tester.uart_no_cmd;
			for(x = 0; x < 19; x++)
				temp_y[x] = temp_y[x + 1];
			break;
		case TIMER_CHIP_NUM:
//		DeleteGraphPlot ((int)callbackData, PANEL_GRAPH_3, -1, VAL_IMMEDIATE_DRAW);
//		PlotY ((int)callbackData, PANEL_GRAPH_3, crc_errors, 20, VAL_INTEGER, VAL_FAT_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, VAL_CONNECTED_POINTS, VAL_BLUE);
			tester.tx_len = 0;
			packet__add(tester.buff_tx, &tester.tx_len, 0x0D, CHECK_SIMPLE, 0);
			uart__cout(UART__TX, packet__set(tester.buff_tx, &tester.tx_len), tester.buff_tx);
			break;
		default:
			break;
	}
}

static int CVICALLBACK main_cout(void *functionId)
{
	if(functionId == NULL)   // Srart/Stop
	{
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(200));
		return 0;
	}
	while(*(int*)functionId)
		Sleep(1), framework__cout();
	*(int*)functionId = -1;
	return 0;
}

static int CVICALLBACK uart_thread(void *functionId)
{
	if(functionId == NULL)   // Srart/Stop
	{
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, uart_thread, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(200));
		return 0;
	}
	while(*(int*)functionId)
		Sleep(1), uart__cout(0, 0, NULL);
	*(int*)functionId = -1;
	return 0;
}


static void com_ports_check(int panel, int control)
{
	int com = -1;
	char name[24];
	ClearListCtrl (panel, control);
	while(++com <= 255)
	{
		char ncom[24];
		sprintf(ncom, "\\\\.\\COM%d", com);
		HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
		uint32_t error = 0;
		if(hport == INVALID_HANDLE_VALUE)
			switch(error = GetLastError())
			{
				case 5:
					break;
				default:
					continue;
			}
		else
			CloseHandle(hport);
		sprintf(name, "COM %d%s", com, error != 5? "": "(занят)");
		InsertListItem (panel, control, -1, name, com);
	}
}

static void set_default_panel()
{
	SetCtrlVal (panelHandle, PANEL_COM_CHANGE, tester.uart_id);
	SetCtrlVal (panelHandle, PANEL_TIMEOUT_VALUE, (double)tester.timeout / 1000);
	SetCtrlVal(panelHandle, PANEL_TIMEOUT_VIEW, (double)tester.timeout / 1000);
}
//65535
const char* fsm_err_tbl[565] = {"обнаружил ошибку CRC", "занят", "не существует", "недоступен", "завис", "отверг команду", "закончил по таймауту", "выдал ошибку", "выполнил без опций"};
const char* fsm_pwr_cmd_tbl[] = {"включение"};
const char* fsm_asic_check_cmd_tbl[] = {"быстрая проверка"};
const char* fsm_kernel_cmd_tbl[255] = {"версия ПО", "версия платы", ""};
typedef struct
{
	char* id;
	char** cmd_tbl;
} tester_id_cmd_t;

const tester_id_cmd_t fsm_id_tbl[256] = {{NULL, NULL}, {"питания S9", fsm_pwr_cmd_tbl}, [0x0D] = {"проверка чипов", fsm_asic_check_cmd_tbl}, [0xFA] = {"системный", fsm_kernel_cmd_tbl}};


uint8_t uart_test_cb(events__e event, uint8_t* data, uint32_t len)
{
	char buff_out[200];
	int idx = 0;
	if (!data)
	{
		event = EVENT__TIMEOUT;
		uart__cout(UART__RX, UART_BUFF, tester.buff_rx);
	}
	switch (event)
	{
		case EVENT__OK:
			while(len > idx + 4)
			{
				msg_t* msg = (msg_t*)(data + idx);
				if (fsm_id_tbl[msg->id].id)
				{
					if (msg->id >= 0x0D && msg->id <= 0x18) {
						draw_chip_num((u8)msg->param);
					}
					sprintf(buff_out, "Модуль %s %s: %s (%d) \n", fsm_id_tbl[msg->id].id, fsm_id_tbl[msg->id].cmd_tbl[msg->cmd] == NULL ? "" : fsm_id_tbl[msg->id].cmd_tbl[msg->cmd], (msg->param > 65000) ? fsm_err_tbl[65535 - msg->param] : "", msg->param);
					SetCtrlVal(panelHandle, PANEL_TEXTBOX, buff_out);
				}
				idx += (crc__8_ccitt(data + idx, 5) ? 4 : 5);
			}
			break;
		default:
		case EVENT__TIMEOUT:
			break;
	}
	return 0;
}

static void packet__add(u8* buff, u8* len, u8 id, u8 cmd, u16 param)
{
	u8 msg_buff[4];
	msg_buff[0] = id;
	msg_buff[1] = cmd;
	*(u16*)&msg_buff[2] = param;
	memcpy(buff + *len, msg_buff, 4);
	*len += 4;
}

static u8 packet__set(u8* buff, u8* length)
{
	u8 len = *length;
	u8 crc = crc__8_ccitt(buff, len);
	buff[len] = crc;
	*length = 0;
	return len + 1;
}

static void draw_chip_num(u8 chip_num)
{
	char cur_pos = 0;
	if (chip_num > 63)
		return;
//	sprintf(temp_char, "Chain[x] has %2d asic. \n", chip_num);
//	SetCtrlVal(panelHandle, PANEL_TEXTBOX, temp_char);
	chips_err_count[chip_num]++;
	PlotY (panelHandle, PANEL_GRAPH_CHIPS_ERROR, chips_err_count, 64, VAL_UNSIGNED_INTEGER, VAL_VERTICAL_BAR, VAL_EMPTY_SQUARE, VAL_SOLID, 1, VAL_CYAN);
	while(cur_pos < chip_num) SetCtrlAttribute (panelHandle, chip_tbl[cur_pos++], ATTR_VISIBLE, 0);
	while(cur_pos <= 63) SetCtrlAttribute (panelHandle, chip_tbl[cur_pos++], ATTR_VISIBLE, 1);
}
