/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_COMMANDBUTTON              2       /* control type: command, callback function: ExitCallback */
#define  PANEL_TEXTBOX                    3       /* control type: textBox, callback function: (none) */
#define  PANEL_COMMANDBUTTON_2            4       /* control type: command, callback function: send_cb */
#define  PANEL_TEXTBOX_3                  5       /* control type: textBox, callback function: (none) */
#define  PANEL_COMMANDBUTTON_3            6       /* control type: command, callback function: read_cb */
#define  PANEL_COM_CHANGE                 7       /* control type: ring, callback function: (none) */
#define  PANEL_COM_RENEW                  8       /* control type: command, callback function: com_cb */
#define  PANEL_TEXTBOX_2                  9       /* control type: textBox, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK com_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ExitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK read_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK send_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
