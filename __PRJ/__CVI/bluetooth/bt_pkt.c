#include <stdint.h>
#include <cvirte.h>	
#include <userint.h>
#include <ansi_c.h>
#include "bluetooth.h" 
//#include "bt_pkt.h"
#include "crc.h"
#include "myhead.h"
#include "bt_transport_pkt.h" 

#include "bluetooth.h"
extern char *error_events[];
extern char *error[];
extern int panelHandle; 

extern int panelHandle;
bt_events_e bt_data_send(uint8_t* data);
static void bt_data_buffer(uint8_t* data);
static bt_events_e bt_data_pkt(uint8_t* buffer);
bt_events_e bt_data_input(uint8_t* data);

static uint32_t len=0;
static uint8_t buffer[100];


bt_events_e bt_data_send(uint8_t* data){
	bt_data_buffer(data);
	bt_data_pkt(buffer);
	return BT_EVENT_OK;
}

static void bt_data_buffer(uint8_t* data){
	len=(strlen((const char *)data))+4; 
	*(uint32_t *)&buffer[0]=len;
	uint8_t* buff=&buffer[4];
	uint8_t i=0;
	while(i<=len-4){
		buff[i]=data[i];
		i++;
	}
	buffer[i+3]=crc__8_ccitt(buffer,len);
	len++;
 }

static bt_events_e bt_data_pkt(uint8_t* buffer){
	int fix_len;
	uint32_t count=0;
	uint32_t pkt_len=len/MAX_LEN;
	if(pkt_len==0)
		pkt_len+=1;
	for(uint8_t i=0;i<(pkt_len+1);i++){
		uint8_t pkt_buffer[20]={0};
		for(uint8_t j=3;j<(MAX_LEN+3);j++,count++)
			pkt_buffer[j]=buffer[count];
		if(i==0){
			pkt_buffer[2]=BT_CMD_WRITE;
			pkt_buffer[1]=(uint8_t)((len>MAX_LEN)?MAX_LEN:len)+2; 
		}
		if(i<pkt_len && i>1){
			pkt_buffer[2]=BT_CMD_CONTINUE;
			pkt_buffer[1]=MAX_LEN+2; //����������
		}
		if(i>=pkt_len){		
			pkt_buffer[2]=BT_CMD_END;
			fix_len=len-(pkt_len*MAX_LEN);
			pkt_buffer[1]=(uint8_t)((fix_len<1)?0:fix_len)+2;
		}
		pkt_buffer[0]=crc__8_ccitt(&pkt_buffer[PKT_LEN],pkt_buffer[PKT_LEN]);
		if(save_data_uart(pkt_buffer,pkt_buffer[1])==BT_CMD_BREAK)
			return BT_EVENT_ERROR;
	}
	
	return BT_EVENT_OK;
}


/***********************************************
*
*  ����� �����
***********************************************/


uint8_t data_input[100];
static uint8_t count_input;
static uint32_t len_input;
static uint8_t crc8;
static uint8_t len_fix=0;


bt_events_e bt_data_input(uint8_t* data){
	uint8_t crc;
	uint8_t data_out[100]; 
	if(data[PKT_CRC]!=crc__8_ccitt(&data[PKT_LEN],data[PKT_LEN]))
		return BT_EVENT_CRC_ERR; 
	memcpy(&data_input[len_fix],&data[PKT_DATA],data[PKT_LEN]-2); 		
	len_fix+=data[PKT_LEN]-2;
	if(data[PKT_CMD]==BT_CMD_END){   
				//memcpy(&data_input[len_fix],&data[PKT_DATA],data[PKT_LEN]-2);
				crc=data_input[*(uint32_t *)data_input];
				data_input[*(uint32_t *)data_input]=0x00;
				sprintf((char *)data_out,"�����:%d\n������:%s\ncrc:%02X\n",*(uint32_t *)data_input,&data_input[4],crc); ///********* ///
				InsertTextBoxLine (panelHandle, PANEL_TEXTBOX_3, -1,data_out); ///********* ///
				len_fix=0;
			}
	return BT_EVENT_OK;
}
