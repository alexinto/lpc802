#ifndef __bt_pkt_H__
#define __bt_pkt_H__

#ifdef __cplusplus
    extern "C" {
#endif

#include "cvidef.h"


typedef enum {
    BT_EVENT_CRC_ERR = -4,
    BT_EVENT_LEN_ERR = -3,
    BT_EVENT_NO_CMD  = -2,
    BT_EVENT_ERROR   = -1,    
    BT_EVENT_OK      = 0,
}bt_events_e;		
		
		
		
		
		
		
		
#ifdef __cplusplus
    }
#endif

#endif
