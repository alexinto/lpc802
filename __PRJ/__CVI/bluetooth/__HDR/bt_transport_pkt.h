//==============================================================================
//
// Title:		bt_transport_pkt.h
// Purpose:		A short description of the interface.
//
// Created on:	23.07.2020 at 12:47:27 by Alexandr.
// Copyright:	. All Rights Reserved.
//
//==============================================================================

#ifndef __bt_transport_pkt_H__
#define __bt_transport_pkt_H__

#ifdef __cplusplus
    extern "C" {
#endif

//==============================================================================
// Include files

#include "cvidef.h"

//==============================================================================
// Constants

//==============================================================================
// Types

//==============================================================================
// External variables

//==============================================================================
// Global functions

void uart__init(int uart_id);
bt_events_e save_data_uart(uint8_t* pkt_data,uint8_t pkt_len);
void read_data_uart(void);
#ifdef __cplusplus
    }
#endif

#endif  /* ndef __bt_transport_pkt_H__ */
