#include "bt_pkt.h"
#include <rs232.h>
#include <windows.h>
#include <userint.h>
#include <ansi_c.h>
#include "w:\M_PRJ\__PRJ\__CVI\bluetooth\__HDR\myhead.h"
#include "bt_transport_pkt.h"
#include "bt_pkt.h"
#include "crc.h"

#include "bluetooth.h"
extern char *error_events[];
extern char *error[];
extern int panelHandle; 


uint8_t comport;

void uart__init(int uart_id) {
	int res;
	char comport_str[10];
	CloseCom(uart_id);  
	sprintf(comport_str, "COM%d", uart_id);
	res = OpenComConfig (uart_id, comport_str, 9600, 0, 8, 1, 512, 512);
	SetComTime (uart_id, 1);
}
/***********************************************
*
*  �������� �����
***********************************************/
bt_events_e save_data_uart(uint8_t* pkt_data,uint8_t pkt_len){
	uint8_t events[3]={0};
	ComWrt(comport,pkt_data,pkt_len+1);
	char byff[100];															 ///
	sprintf(byff,"��������� �����  %s\n",error[pkt_data[2]]);					  ///
	SetCtrlVal (panelHandle, PANEL_TEXTBOX_2, byff);						  ///
	ComRd (comport, events, 3);
	sprintf(byff,"������� �����  %s\n",error[events[2]]);					  ///
	SetCtrlVal (panelHandle, PANEL_TEXTBOX_2, byff);						  ///
	if(events[PKT_CRC]!=crc__8_ccitt(&events[PKT_LEN],events[PKT_LEN]))
		return BT_EVENT_CRC_ERR;
	return BT_EVENT_OK;
}

/***********************************************
*
*  ����� �����
***********************************************/
void read_data_uart(void){
	uint8_t data_events[3]={0};
	uint8_t read_data[20];
	int result;
	char byff[100];	///	******** ///
	data_events[PKT_CMD]=BT_CMD_BREAK;
	while((result=ComRd (comport, read_data, 20))>0){
	sprintf(byff,"������ �����%s\n",error[read_data[PKT_CMD]]);///********** ///
	SetCtrlVal (panelHandle, PANEL_TEXTBOX_2, byff);///********* ///
	data_events[PKT_LEN]=2;
		if(read_data[PKT_CRC]==crc__8_ccitt(&read_data[PKT_LEN],read_data[PKT_LEN])){
			
			data_events[PKT_CMD]=BT_CMD_CONTINUE;
			break;
		}
	}
	data_events[PKT_LEN]=2;
	data_events[PKT_CRC]=crc__8_ccitt(&data_events[PKT_LEN],data_events[PKT_LEN]);
	if((bt_data_input(read_data)==BT_EVENT_OK) && (read_data[PKT_CMD]!=BT_CMD_END)){
		result=ComWrt(comport,data_events,data_events[PKT_LEN]);
		sprintf(byff,"��������� �����%s\n",error[data_events[PKT_CMD]]);///********* /// 
		SetCtrlVal (panelHandle, PANEL_TEXTBOX_2, byff);///********* /// 
		read_data_uart();
	}
}
