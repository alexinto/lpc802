//==============================================================================
//
// Title:		target.h
// Purpose:		A short description of the interface.
//
// Created on:	15.07.2020 at 12:39:04 by Alexandr.
// Copyright:	. All Rights Reserved.
//
//==============================================================================

#ifndef __target_H__
#define __target_H__

#ifdef __cplusplus
    extern "C" {
#endif

//==============================================================================
// Include files

#include "cvidef.h"

//==============================================================================
// Constants

//==============================================================================
// Types

//==============================================================================
// External variables

//==============================================================================
// Global functions

int Declare_Your_Functions_Here (int x);

#ifdef __cplusplus
    }
#endif

#endif  /* ndef __target_H__ */
