#include <rs232.h>
#include <windows.h> 
#include <stdint.h>
#include <ansi_c.h>
#include <cvirte.h>		
#include <userint.h>
#include "bluetooth.h"
#include "bt_pkt.h"
#include "myhead.h"
#include "bt_transport_pkt.h"
#include "crc.h" 


char *error_events[]= {"BT_EVENT_CRC_ERR","BT_EVENT_LEN_ERR","BT_EVENT_NO_CMD","BT_EVENT_ERROR","BT_EVENT_OK","BT_EVENT_FAIL_PKT"}; //+4
char *error[]={"BT_CMD_END","BT_CMD_READ","BT_CMD_WRITE","BT_CMD_BREAK","BT_CMD_CONTINUE"};
FILE 	*hFile;
uint8_t comport;
int panelHandle; 
char 	tempFileName[80];
uint8_t data[100];
void save_data(uint8_t* pkt_data,uint8_t pkt_len);
void output_data(void);
void output_data1(void);
uint8_t read_fgetc(void);
static void com_ports_check(int panel, int control);


int main (int argc, char *argv[])
{
	sprintf(tempFileName, "out.txt");
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "bluetooth.uir", PANEL)) < 0)
		return -1;
	com_ports_check(panelHandle, PANEL_COM_CHANGE);
	
	DisplayPanel (panelHandle);
	RunUserInterface ();
	DiscardPanel (panelHandle);
	return 0;
}

int CVICALLBACK ExitCallback (int panel, int control, int event,
							  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			QuitUserInterface(0);
			break;
	}
	return 0;
}

int CVICALLBACK send_cb (int panel, int control, int event,
						 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			//GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &comport);
			comport=7;
			uart__init(comport);
			SetCtrlVal (panelHandle, PANEL_TEXTBOX_2, "open comport\n");
			hFile = fopen(tempFileName, "w");
			fclose(hFile); 
			GetCtrlVal (panelHandle, PANEL_TEXTBOX, data);
			bt_data_send(data);
			output_data();
			CloseCom(comport);
			SetCtrlVal (panelHandle, PANEL_TEXTBOX_2, "close comport\n");
			break;
	}
	return 0;
}
int CVICALLBACK read_cb (int panel, int control, int event,
						 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			int res;
			uint8_t start_read[4]={0x2D,0x02,0x01};
			start_read[0]= crc__8_ccitt(&start_read[PKT_LEN],start_read[PKT_LEN]);
			//GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &comport);
			comport=7;
			uart__init(comport);
			SetCtrlVal (panelHandle, PANEL_TEXTBOX_2, "open comport\n");
			res=ComWrt(comport,start_read,3);
			read_data_uart();
			CloseCom(comport);
			SetCtrlVal (panelHandle, PANEL_TEXTBOX_2, "close comport\n");
			break;
	}
	return 0;
}

void save_data(uint8_t* pkt_data,uint8_t pkt_len){
	hFile = fopen(tempFileName, "a");
	ComWrt(comport,pkt_data,pkt_len+1);
	for(uint8_t i=0;i<pkt_len+1;i++)
		fputc(pkt_data[i], hFile);
	fclose(hFile); 
}
void output_data(void){
	hFile = fopen(tempFileName, "rb");
	output_data1();
	fclose(hFile);
}
static uint8_t end_of_file=0; 
void output_data1(void){
	uint8_t buffer_out[20]={0};
	buffer_out[0]=read_fgetc();
	buffer_out[1]=read_fgetc();
	for(int i=2;i<(buffer_out[1]+1);i++)
		 	buffer_out[i]=read_fgetc();
	ComRd(comport, buffer_out, 20); 
	bt_data_input(buffer_out);
	if(end_of_file!=1)
		output_data1();
}
uint8_t read_fgetc(void){
	int buffer_time; 
	if((buffer_time=fgetc(hFile))<0)
		   end_of_file=1;
	else
	{
		end_of_file=0;
		return (uint8_t)buffer_time;
	}
	return 2;
}

int CVICALLBACK com_cb (int panel, int control, int event,
						void *callbackData, int eventData1, int eventData2)
{
if (event == EVENT_COMMIT)
		com_ports_check(panel, PANEL_COM_CHANGE);
	return 0;
}
static void com_ports_check(int panel, int control) {
    int com = -1;
	char name[24];
	ClearListCtrl (panel, control); 
	while(++com <= 255)
    {
        char ncom[24];
        sprintf(ncom, "\\\\.\\COM%d", com);
        HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
        uint32_t error = 0;
        if(hport == INVALID_HANDLE_VALUE)
            switch(error = GetLastError())
            {
                case 5: break;
                default: continue;
            }
        else
            CloseHandle(hport);
        sprintf(name, "COM %d%s", com, error != 5? "": " (caiyo)");
		InsertListItem (panel, control, -1, name, com);
    }
}

