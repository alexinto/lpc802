#ifndef P_CAL_H_
#define P_CAL_H_

#include <stdint.h>

typedef struct {
    uint32_t freq;
    int8_t oscil1_bank;
    int8_t oscil1_d_delay;
    int8_t oscil1_a_delay;
    int8_t oscil2_bank;
    int8_t oscil2_d_delay;
    int8_t oscil3_a_delay;
    uint8_t reserved[6];
}param__t;



#endif
