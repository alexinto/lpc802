/***************************************************************************//**
 * @file menu_box.h.
 * @brief ������ ����.
 * @author a.tushentsov
 ******************************************************************************/

#ifndef MENU_BOX_H_
#define MENU_BOX_H_
#include "System/events.h"

typedef enum {
	MENU_BOX__READ_ITEM  = 0,
	MENU_BOX__WRITE_ITEM = 0,
}menu_box__event_e;

typedef enum {
	MENU_BOX__ACTIVE_ITEM  = 0, // ���������� �������� ������� �������
	MENU_BOX__LAST_ITEM    = 1, // ���������� ��������� ������� �������
	MENU_BOX__SEL_ITEM     = 2, // ���������� ���-�� ���������� ���������
}menu_box__item_e;

typedef struct {
	uint32_t addr;
	uint32_t data;
	int format_addr, format_data;
}menu_box__item_t;


typedef void (*menu_box__handler_t)(int panel, menu_box__event_e event, int item_num);  


int menu_box__create(int panel, int column_num, char* descr, menu_box__handler_t menu_handler);



void menu_box__set_item(int panel, int item_num, int addr, int data);

menu_box__item_t* menu_box__get_item(int panel, int item_num);     

int menu_box__add_item(int panel, int item_num, int format_addr, int format_data);

void menu_box__del_item(int panel, int item_num);     //  item_num = -1 : ��������� �������

int menu_box__get_item_num(int panel, menu_box__item_e item);

char* menu_box__get_name(int panel);

#endif
