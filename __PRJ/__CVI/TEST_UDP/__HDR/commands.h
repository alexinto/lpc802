
#ifndef _COMMANDS_H
#define _COMMANDS_H

#include <stdint.h>
#include <stddef.h>
#include "System/events.h"

#define CMD_MAX_PARAM_NUM 20

typedef enum {
	ui8  = 0,
	ui16 = 1,
	ui32 = 2,
}commands__p_types_e;


typedef struct {
	char* cmd_name;
	uint16_t cmd_id;
	uint16_t proc_type;
	int pin_num;
	int pout_num;
	commands__p_types_e p_types[CMD_MAX_PARAM_NUM];
}commands__struct_t;

















#endif
