/***************************************************************************//**
 * @file events.h.
 * @brief ����, ���������� �������� �������.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef EVENTS_H_
#define EVENTS_H_

/**
 * @defgroup GRP_EVENTS ������ ������� �������
 * @brief ������ ���������� �������� �������
 * @{
 */

typedef enum {
    EVENT__TIMEOUT   = -6,
    EVENT__BLOCK     = -5,
    EVENT__PROTECT   = -4,
    EVENT__PARAM_NA  = -3,
    EVENT__BUSY      = -2,
    EVENT__ERROR     = -1,
    EVENT__OK        =  0,
    EVENT__CONTINUE  =  1, // ���������� ������� ������
}events__e;

/**
 * @}
 */

#endif
