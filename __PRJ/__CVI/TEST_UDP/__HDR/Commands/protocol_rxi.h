﻿/***************************************************************************//**
 * @file protocol_rxi.h.
 * @brief Файл с описанием типов стурктур протокола RXI.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef PROTOCOL_RXI_H_
#define PROTOCOL_RXI_H_

/**
* Порт устройства по- умолчанию.
*/
#define UDP_CONNECT_PORT  1010

/**
 * \brief Типы пакетов протокола RXI
 */
typedef enum {
    /**
    * Зарезервировано.
    */
    PKT_TYPE__RESERVED       = 0,
    /**
    * Пакет предназначен для интерфейсного процессора.
    */
    PKT_TYPE__IO_PROC        = 1,
    /**
    * Подтверждение получения пакета.
    */
    PKT_TYPE__ACK            = 2,
    /**
    * Ошибка - неверный ID пакета.
    */
    PKT_TYPE__ERR_ID         = 3,
    /**
    * Ответный пакет, завершающий транзакцию.
    */
    PKT_TYPE__ANSWER         = 4,
    /**
    * Ответный пакет, начинающий\продолжающий транзакцию.
    * Транзакция завершается \ref PKT_TYPE__ANSWER
    */
    PKT_TYPE__ANSWER_PART    = 5,
    /**
    * Пакет предназначен для командного процессора.
    */
    PKT_TYPE__CMD_PROC       = 17,
    /**
    * Пакет с игнорированием ID пакета. Не поддерживает ответ (PKT_TYPE__ANSWER) и подтверждение (PKT_TYPE__ACK).
    * Обработка пакета происходит всеми процессорами в системе.
    */
    PKT_TYPE__IGNORE_ID      = 25,
}rxi__pkt_type_e;

/**
 * \brief Типы ответов команд протокола RXI
 */
typedef enum {
    /**
    * Команда выполнена успешно.
    */
    ANSWER_CMD__DONE           =  0,
    /**
    * Команда выполнена успешно. Структура ответа зависит от команды.
    */
    ANSWER_CMD__ANSWER         =  1,
    /**
    * Команда выполнена с ошибкой. Код ошибки \ref rxi__cmd_error_e.
    */
    ANSWER_CMD__ERROR          =  2,
}rxi__answer_status_e;

/**
 * \brief Типы ошибок команд протокола RXI
 */
typedef enum {
    /**
    * Неверные параметры (неверная длина параметров, параметр вне диапазона и т.д.).
    */
    CMD_ERR__PARAM_NA       	=  1,
    /**
    * Команда заблокирована. (задел на будущее)
    */
    CMD_ERR__BLOCK          	=  2,
    /**
    * Команда защищена паролем. Для доступа необходимо выполнить \ref CMD__PASS_EN
    */
    CMD_ERR__PROTECT        	=  3,
    /**
    * Команды не существует.
    */
    CMD_ERR__NOT_EXIST      	=  4,
    /**
    * Параметр команды не найден в таблице параметров для конкретной конфигурации.
    */
    CMD_ERR__PARAM_NOT_FOUND    =  5,
    /**
    * Ошибка конфигурации "железа". Например, неправильно подключен разъем или отсутствует питание модуля.
    */
    CMD_ERR__HW_ERROR           =  6,
    /**
    * Недостаточно памяти для выполнения команды.
    */
    CMD_ERR__NO_MEMORY          =  7,
    /**
    * Модуль занят и не способен выполнить команду.
    */
    CMD_ERR__BUSY               =  8,
    /**
    * Истекло время ожидания и не удалось выполнить команду.
    */
    CMD_ERR__TIMEOUT            =  9,
}rxi__cmd_error_e;

#endif
