/*******************************************************************************
 * @file awg2m_types.h.
 * @brief Типы данных модуля AWG2M.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef AWG2M_TYPES_H_
#define AWG2M_TYPES_H_

#pragma pack(push, 1)
/**
 * \brief Режимы работы.
 */
typedef enum {
    AWG2M__WORK_MODE_V   =  0,  /*!< постоянное напряжение */
    AWG2M__WORK_MODE_IMG =  1,  /*!< воспроизведение цифр. формы ОЗУ */
    AWG2M__WORK_MODE_DDS =  2,  /*!< DDS */
}awg2m__work_mode_e;

/**
 * \brief Тип структуры конфигурации триггера.
 */
typedef struct {
    uint32_t soft        : 1;         /*!< софт. */
    uint32_t ext_trig    : 4;         /*!< внешний триггер(0..7). */
    uint32_t int_trig    : 4;         /*!< внутренний триггер(0..31). */
    uint32_t active_lvl  : 2;         /*!< активный уровень 0- спад, 1- фронт. */
}awg2m__trig_cfg_t;

/**
 * \brief Перечень DAC модуля AWG2M.
 */
typedef enum {
    AWG2M__DAC1    =  0,
    AWG2M__DAC2    =  1,
    AWG2M__DAC_OCM =  2,
}awg2m__dac_e;






#pragma pack(pop)



#endif
