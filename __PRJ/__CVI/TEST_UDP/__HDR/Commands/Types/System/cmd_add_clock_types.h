﻿/***************************************************************************//**
 * @file cmd_add_clock_types.h.
 * @brief Модуль с описанием типов ADD_CLOCK.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef ADD_CLOCK_TYPES_H_
#define ADD_CLOCK_TYPES_H_

#pragma pack(push,1)

/**
 * Каналы модуля.
 */
typedef enum {
    /**
     * Каналы модуля.
     */
    ADD_CLOCK_CH_LF = 0,
    /**
     * Каналы модуля.
     */
    ADD_CLOCK_CH_LF = 1,
}cmd_add_clock_ch_e;

/**
 * Настройки модуля ADD_CLOCK.
 */
typedef struct{
	

}cmd_add_clock_settings_t;

#pragma pack(pop)

#endif
