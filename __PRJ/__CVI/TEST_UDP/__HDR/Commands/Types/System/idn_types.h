﻿/***************************************************************************//**
 * @file idn_types.h.
 * @brief Модуль содержит дескрипторы устройств. Возвращаются командой IDN_REQ.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef IDN_TYPES_H_
#define IDN_TYPES_H_

/**
 * Строковые константы модулей. Возвращаются командой \ref IDN_REQ.
 */

#define IDN_MS20GT      "MS20GT"
#define IDN_SYNC1200    "Sync1200"
#define IDN_SYNCMM      "SyncMM"
#define IDN_MEASURE     "MeasureModul"
#define IDN_PSAC2C      "PSAC2C"
#define IDN_PIN400      "Pin400M"
#define IDN_SYNC_DEBUG  "SyncDebug"
#define IDN_DPS_SOM_MM  "DPS_SOM_MM"
#define IDN_AWG         "AWG2M"

/**
 * Строковые константы прошивок СЧ модулей. Возвращаются командой \ref FW_VER.
 */
#define FW_PG          "PG"
#define FW_MAIN        "Main"
#define FW_EXTIF       "ExtIF"
#define FW_ADDCLOCK    "AddClock"
#define FW_PSP_UU      "UU"
#define FW_PSP_UPV      "UPV"
#define FW_PSP_PDU      "PDU"
#define FW_PSP_UPP0     "UPP_SECT_0"
#define FW_PSP_UPP1     "UPP_SECT_1"
#define FW_PSP_UPSM     "UPSM"
#define FW_PSP_PSDC0    "PSDC_SLOT0"
#define FW_PSP_PSDC1    "PSDC_SLOT1"
#define FW_PSP_PSDC2    "PSDC_SLOT2"
#define FW_PSP_PSDC3    "PSDC_SLOT3"
#define FW_PSP_PSDC4    "PSDC_SLOT4"
#define FW_PSP_PSDC5    "PSDC_SLOT5"
#define FW_PSP_PSDC6    "PSDC_SLOT6"
#define FW_PSP_PSDC7    "PSDC_SLOT7"

/**
 * Перечень типов модулей крейта.
 */
typedef enum {
    /**
    * Ихзмерительные модули.
    */
    MODUL__MEASURE       = 0,
    /**
    * Модули первичного питания.
    */
    MODUL__FIRST_SUPPLY  = 1,
    /**
    * Модули вторичного питания.
    */
    MODUL__SECOND_SUPPLY = 2,
    /**
    * Модули информационного обмена.
    */
    MODUL__IO            = 3,
    /**
    * Модули системы тактирования.
    */
    MODUL__CLOCK_SYSTEM  = 4,
    /**
    * Неизвестные модули.
    */
    MODUL__UNCKNOWN     = 15,
}idn_types__modules__e;


#endif
