/***************************************************************************//**
 * @file log_types.h.
 * @brief Модуль c описанием типов данных журнала.
 * @author a.tushentsov.
 ******************************************************************************/
/**
 * \defgroup Grp_Journal Модуль журнала
 */
#ifndef LOG_TYPES_H_
#define LOG_TYPES_H_

#include <stdint.h>

/**
 * \defgroup GRP_JRNL_TYPE JRNL_TYPE
 * \ingroup Grp_Journal
 * Тип записи журнала.
 */
/**@{*/
/**
 *  Запись журнала - 16 байт.
 */
#pragma pack(push,1)

typedef struct {
    /**
     *  Индекс записи.
     */
    uint16_t index;
    /**
     *  Время события.
     */
    uint32_t timestamp;
    /**
     *  Код события.    \ref log__msg_code_e
     */
    uint16_t code;
    /**
     *  Параметры.
     */
    uint16_t data[3];
    /**
     *  Тип записи. 0x80- запись не читалась ранее.
     */
    uint8_t type;
    /**
     *  Контрольная сумма. CRC8 CCITT инициализация = \ref CRC__8_THREAD_CCITT
     */
    uint8_t crc;
}log__msg_t;

/**
 *  Коды событий журнала.
 */
typedef enum {
	/**
	 *  Авария системы питания.
	 */
	LOG__PWR_ALARM      	= 0,

}log__msg_code_e;

/**@}*/

#pragma pack(pop)


#endif
