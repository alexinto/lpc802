﻿/***************************************************************************//**
 * @file cmd_select_types.h.
 * @brief Модуль с описанием типов cmd_select.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_SELECT_TYPES_H_
#define CMD_SELECT_TYPES_H_

#include <stdint.h>

// Константы протокола RXI
#define PKT_HEADER_SIZE           3                      // Размер хэдера пакета в байтах
#define CMD_BUFF_RECEIVE          1500                   //размер буфера
#define CMD_BUFF_TRANSMIT         1440
#define DIGIT_PARAM_SIZE          192                    //максимально возможный размер в байтах параметров чиловой команды (DGT-команды)
#define CMD_HDR_SIZE              (sizeof(cmd__t) - DIGIT_PARAM_SIZE)

// Enum результатов выполнения команд
typedef enum {
    CMD__DONE            =  0, // Выполнена успешно, требуется ответ
    CMD__ANSWER          =  1, // Выполнена успешно, ответ
    CMD__ERROR           =  2, // Команды не существует
    CMD__OK_NO_ANSWER    =  3, // Команда выполнена, ответа не требуется
    CMD__PARAM_NA        =  4, // Неверные параметры
    CMD__BLOCK           =  5, // Команда заблокирована
    CMD__PROTECT         =  6, // Команда защищена паролем
    CMD__PARAM_NOT_FOUND =  7, // Параметр не найден
    CMD__HW_ERROR        =  8, // Ошибка при конфигурации железа
    CMD__NO_MEMORY       =  9, // Недостаточно памяти для выполнения команды
    CMD__BUSY            = 10, // Модуль занят и не способен выполнить команду
    CMD__TIMEOUT         = 11, // Превышен таймаут ожидания выполнения команды
}cmd_select__result_e;

// Структура команды протокола RXI
typedef struct {
    uint16_t    func_num;                   //идентификатор команды (соотносится с функцией в cmd_tbl.c).
    uint16_t    param_byte_num;             //число байт параметра(ов), размещенных в области param[DIGIT_PARAM_SIZE]
    uint8_t     param[DIGIT_PARAM_SIZE];    //параметры функции - область хранения параметров
} cmd__t;


#endif
