﻿/***************************************************************************//**
 * @file RxiEthernet_types.h.
 * @brief Модуль с описанием типов RxiEthernet.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef RXIETHERNET_TYPES_H_
#define RXIETHERNET_TYPES_H_

#include <stdint.h>

#define RXIETHERNET_CMD_SIZE      192     /*!< Максимально возможный размер в байтах параметров RXI команды. */

/**
* Порт устройства по- умолчанию.
*/
#define UDP_CONNECT_PORT  1010         /*!< основной порт для приема RXI команд. */

/**
 * \brief Типы пакетов протокола RXI
 */
typedef enum {
    /**
    * Зарезервировано.
    */
    PKT_TYPE__RESERVED       = 0,
    /**
    * Пакет предназначен для интерфейсного процессора.
    */
    PKT_TYPE__IO_PROC        = 1,
    /**
    * Подтверждение получения пакета.
    */
    PKT_TYPE__ACK            = 2,
    /**
    * Ошибка - неверный ID пакета.
    */
    PKT_TYPE__ERR_ID         = 3,
    /**
    * Ответный пакет, завершающий транзакцию.
    */
    PKT_TYPE__ANSWER         = 4,
    /**
    * Ответный пакет, начинающий\продолжающий транзакцию.
    * Транзакция завершается \ref PKT_TYPE__ANSWER
    */
    PKT_TYPE__ANSWER_PART    = 5,
    /**
    * Подтверждение получения ответа на пакеты \ref PKT_TYPE__ANSWER, \ref PKT_TYPE__ANSWER_PART.
    */
    PKT_TYPE__RACK           = 6,
    /**
    * Пакет предназначен для командного процессора.
    */
    PKT_TYPE__CMD_PROC       = 17,
    /**
    * Пакет с игнорированием ID пакета. Не поддерживает ответ (\ref PKT_TYPE__ANSWER) и подтверждение (\ref PKT_TYPE__ACK).
    * Обработка пакета происходит всеми процессорами в системе.
    */
    PKT_TYPE__IGNORE_ID      = 25,
}RxiEthernet__pkt_type_e;

/**
 * \brief Типы ответов команд протокола RXI
 */
typedef enum {
    /**
    * Команда выполнена успешно.
    */
    ANSWER_CMD__DONE           =  0,
    /**
    * Команда выполнена успешно. Структура ответа зависит от команды.
    */
    ANSWER_CMD__ANSWER         =  1,
    /**
    * Команда выполнена с ошибкой. Код ошибки \ref rxi__cmd_error_e.
    */
    ANSWER_CMD__ERROR          =  2,
}RxiEthernet__answer_status_e;

/**
 * \brief Типы ошибок команд протокола RXI
 */
typedef enum {
    /**
    * Неверные параметры (неверная длина параметров, параметр вне диапазона и т.д.).
    */
    CMD_ERR__PARAM_NA           =  1,
    /**
    * Команда заблокирована. (задел на будущее)
    */
    CMD_ERR__BLOCK              =  2,
    /**
    * Команда защищена паролем. Для доступа необходимо выполнить \ref CMD__PASS_EN
    */
    CMD_ERR__PROTECT            =  3,
    /**
    * Команды не существует.
    */
    CMD_ERR__NOT_EXIST          =  4,
    /**
    * Параметр команды не найден в таблице параметров для конкретной конфигурации.
    */
    CMD_ERR__PARAM_NOT_FOUND    =  5,
    /**
    * Ошибка конфигурации "железа". Например, неправильно подключен разъем или отсутствует питание модуля.
    */
    CMD_ERR__HW_ERROR           =  6,
    /**
    * Недостаточно памяти для выполнения команды.
    */
    CMD_ERR__NO_MEMORY          =  7,
    /**
    * Модуль занят и не способен выполнить команду.
    */
    CMD_ERR__BUSY               =  8,
    /**
    * Истекло время ожидания и не удалось выполнить команду.
    */
    CMD_ERR__TIMEOUT            =  9,
}RxiEthernet__cmd_error_e;


#endif
