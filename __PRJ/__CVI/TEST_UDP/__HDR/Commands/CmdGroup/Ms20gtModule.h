#ifndef MS20GTMODULE_H_
#define MS20GTMODULE_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_DEV_CONFIG DEV_CONFIG
 * \ingroup IO_Commands Ms20gtModule
 * \brief Возвращает конфигурацию модуля
 * Возвращает \ref CMD_ERR__PARAM_NOT_FOUND, если модуля не существует.
 * \image html DEV_CONFIG.png Алгоритм работы команды DEV_CONFIG
 */
/**@{*/
#define CMD__DEV_CONFIG              0x0089
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t  device_id; /*!< Номер устройства. Получается при помощи команды \ref CMD__SYS_CONFIG */
}cmd_dev_config__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t device_id; /*!< Номер устройства. */
    uint8_t crate_id;   /*!< Номер крейта (0-3). */
    uint8_t slot_type;  /*!< Тип слота. \ref idn_types__modules__e */
    uint8_t slot_id;    /*!< Номер слота. */
    uint8_t mac_addr[8];/*!< MAC адрес (64 бита). */
    uint32_t ip_addr;   /*!< IP адрес. */
    uint16_t port;      /*!< UDP порт. */
}cmd_dev_config__out_t;
/**@}*/

/**
 * \defgroup GRP_SYS_CONFIG SYS_CONFIG
 * \ingroup IO_Commands Ms20gtModule
 * \brief Возвращает количество устройств в системе.
 * \image html SYS_CONFIG.png Алгоритм работы команды SYS_CONFIG
 */
/**@{*/
#define CMD__SYS_CONFIG              0x0090
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_sys_config__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t device_num;    /*!< Количество устройств. */
}cmd_sys_config__out_t;
/**@}*/


#pragma pack(pop)

#endif
