#ifndef MULTISITECOMMANDS_H_
#define MULTISITECOMMANDS_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_SITE_INIT SITE_INIT
 * \ingroup CMD_Commands MultisiteCommands
 * \brief Команда инициализации группы "Настройка и управление мультисайта". Производит настройку всех регистров мультисайта в соответствии с начальными значениями.
 *        Состояние модуля идентично состоянию "при включении питания".
 */
/**@{*/
#define CMD__SITE_INIT                   0x01BE
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_site_init__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_site_init__out_t;
/**@}*/

/**
 * \defgroup GRP_SITE SITE
 * \ingroup CMD_Commands MultisiteCommands
 * \brief Производит настройку сайта в соответствии с site_id.
 * site_id = 0 - зарезервирован и не может использоваться.
 * Если pg_mask равно 0, то сайт освобождает ресурс и данные очищаются.
 */
/**@{*/
#define CMD__SITE                   0x01C0
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t site_id;    /*!< Номер сайта (1..255, 0 - не используется). */
    uint8_t part_id;    /*!< Номер домена. \ref cmd_site__dom_e */
    uint8_t link_id;    /*!< Номер линка (1..16). При значении 0- не назначает линк сайту. */
    uint8_t pg_sync;    /*!< Признак синхронного запуска 1го ГТП. Необходим для случая 2х доменной синхронизации- организация "виртуального" сайта. */
    uint16_t pg_mask;   /*!< Битовое поле с ГТП, принадлежащих сайту. Номер бита= номер ГТП. 1- принадлежит сайту, 0- нет. */
    uint16_t err_cnt;   /*!< Значение счетчика ошибок. */
}cmd_site__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_site__out_t;
/**@}*/

/**
 * \defgroup GRP_COUNT_ERR COUNT_ERR
 * \ingroup CMD_Commands MultisiteCommands
 * \brief Запрашивает текущее значение счетчика ошибок ФК.
 */
/**@{*/
#define CMD__COUNT_ERR              0x01C1
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t site_id;    /*!< Номер сайта (1..255, 0 - не используется). */
    uint8_t part_id;    /*!< Номер домена. */
}cmd_cnt_err__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t site_id;    /*!< Номер сайта (1..255, 0 - не используется). */
    uint8_t part_id;    /*!< Номер домена. */
    uint16_t err_cnt;   /*!< Значение счетчика ошибок. */
}cmd_cnt_err__out_t;
/**@}*/

/**
 * \defgroup GRP_SITE_CMD SITE_CMD
 * \ingroup CMD_Commands MultisiteCommands
 * \brief Подготавливает сайт к запуску, запускает либо останавливает все сайты.
 */
/**@{*/
#define CMD__SITE_CMD               0x01C2
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t site_id;    /*!< Номер сайта (1..255, 0 - не используется). */
    uint8_t site_state; /*!< Команда. \ref cmd_site__status_e */
}cmd_site_cmd__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_site_cmd__out_t;
/**@}*/

/**
 * \defgroup GRP_SITE_CSTD SITE_CSTD
 * \ingroup CMD_Commands MultisiteCommands
 * \brief Устанавливает значения ССВД модуля для всех доменов. Должна быть вызвана до команды FSALIGMENT
 */
/**@{*/
#define CMD__SITE_CSTD              0x01C4
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t cstd[2];   /*!< Значения ССВД 2х доменов. */
}cmd_site_cstd__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_site_cstd__out_t;
/**@}*/







#pragma pack(pop)

#endif
