/***************************************************************************//**
 * @file PWRCommands.h.
 * @brief Модуль функций управления подсистемой контроля питания.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef PWRCOMMANDS_H_
#define PWRCOMMANDS_H_

#include "Types/DRV/pwr_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_PWR_MEASURE_V PWR_MEASURE_V
 * \ingroup CMD_Commands PWRCommands
 * \brief Измерение напряжения выбранной цепи питания.
 */
/**@{*/
#define CMD__PWR_MEASURE_V                    0x1130
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Идентификатор параметра. */
}cmd_pwr_measure_v__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Идентификатор параметра. */
    double value;       /*!< Измеренное значение параметра, В. */
}cmd_pwr_measure_v__out_t;
/**@}*/

/**
 * \defgroup GRP_PWR_MEASURE_I PWR_MEASURE_I
 * \ingroup CMD_Commands PWRCommands
 * \brief Измерение тока выбранной цепи питания.
 */
/**@{*/
#define CMD__PWR_MEASURE_I                    0x1131
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Идентификатор параметра. */
}cmd_pwr_measure_i__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Идентификатор параметра. */
    double value;       /*!< Измеренное значение параметра, А. */
}cmd_pwr_measure_i__out_t;
/**@}*/

/**
 * \defgroup GRP_PWR_STATUS_GET PWR_STATUS_GET
 * \ingroup CMD_Commands PWRCommands
 * \brief Чтение статуса питания.
 */
/**@{*/
#define CMD__PWR_STATUS_GET                    0x1132
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_pwr_status_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pwr_state;    /*!< Состояние питания. \ref pwr__state_e */
    uint8_t err_state;    /*!< Статус ошибок \ref pwr__err_state_e */
}cmd_pwr_status_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PWR_CTRL PWR_CTRL
 * \ingroup CMD_Commands PWRCommands
 * \brief Изменение режима работы модуля питания.
 */
/**@{*/
#define CMD__PWR_CTRL                          0x1133
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t cmd;           /*!< Команда модулю \ref pwr__cmd_e */
}cmd_pwr_ctrl__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pwr_ctrl__out_t;
/**@}*/







#pragma pack(pop)

#endif
