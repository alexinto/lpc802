#ifndef ADDCLOCKCOMMANDS_H_
#define ADDCLOCKCOMMANDS_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_ADDCLOCK_SETTINGS_WR ADDCLOCK_SETTINGS_WR
 * \ingroup CMD_Commands AddClockCommands
 * \brief Команда записи настроек в энергонезависимую память мезонина AddClock.
 */
/**@{*/
#define CMD__ADDCLOCK_SETTINGS_WR                    0x01F0
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t settings_id;               /*!< Идентификатор параметра AddClock  \ref add_clock__settings_e */
    uint32_t data;                     /*!< Данные для записи параметра */
}cmd_addclock_settings_wr__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_addclock_settings_wr__out_t;
/**@}*/

/**
 * \defgroup GRP_ADDCLOCK_SETTINGS_RD ADDCLOCK_SETTINGS_RD
 * \ingroup CMD_Commands AddClockCommands
 * \brief Команда чтения настроек из энергонезависимой памяти мезонина AddClock.
 */
/**@{*/
#define CMD__ADDCLOCK_SETTINGS_RD                    0x01F1
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t settings_id;               /*!< Идентификатор параметра AddClock  \ref add_clock__settings_e */
}cmd_addclock_settings_rd__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t settings_id;               /*!< Идентификатор параметра AddClock  \ref add_clock__settings_e */
    uint32_t data;                     /*!< Считанные данные */
}cmd_addclock_settings_rd__out_t;
/**@}*/

/**
 * \defgroup GRP_ADDCLOCK_SIN_SET ADDCLOCK_SIN_SET
 * \ingroup CMD_Commands AddClockCommands
 * \brief Команда установки параметров выходного синусоидального сигнала мезонина AddClock. В случае запуска генератора
 *        по триггерным сигналам необходимо предварительно настроить триггеры командой \ref CMD__ADDCLOCK_TRIG_CFG.
 *        Тактирование осуществляется от внутреннего генератора.
 */
/**@{*/
#define CMD__ADDCLOCK_SIN_SET                    0x01F2
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t frequency;          /*!< Частота сигнала, Гц (8000..200000000) */
    uint32_t phase;              /*!< Начальная фаза сигнала, Градусы (0..360) */
    uint32_t voltage;            /*!< Амплитуда сигнала, мВ (125..2000) */
    uint32_t v_offset;           /*!< Постоянная составляющая сигнала, мВ (0..4000) */
    uint8_t out_mode;            /*!< Режим выходов \ref add_clock__out_mode_e */
    uint8_t start_stop_mode;     /*!< Способ запуска и остановки генератора \ref add_clock__start_stop_mode_e */
}cmd_addclock_sin_set__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t frequency;          /*!< Частота сигнала, Гц */
    uint32_t phase;              /*!< Начальная фаза сигнала, Градусы (0..360) */
    uint32_t voltage;            /*!< Амплитуда сигнала, мВ */
    uint32_t v_offset;           /*!< Постоянная составляющая сигнала, мВ */
}cmd_addclock_sin_set__out_t;
/**@}*/

/**
 * \defgroup GRP_ADDCLOCK_TRIG_CFG ADDCLOCK_TRIG_CFG
 * \ingroup CMD_Commands AddClockCommands
 * \brief Команда настройки триггерных сигналов для генераторов сигналов мезонина AddClock.
 */
/**@{*/
#define CMD__ADDCLOCK_TRIG_CFG                    0x01F3
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t gen_id;                     /*!< Идентификатор генератора \ref add_clock__gen_id_e */
    uint8_t start_trig_id;              /*!< Номер триггера для старта(0..255) */
    uint8_t start_event;                /*!< Событие старта \ref add_clock__trig_mode_e */
    uint8_t stop_trig_id;               /*!< Номер триггера для остановки(0..255) */
    uint8_t stop_event;                 /*!< Событие остановки \ref add_clock__trig_mode_e */
}cmd_addclock_trig_cfg__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_addclock_trig_cfg__out_t;
/**@}*/

/**
 * \defgroup GRP_ADDCLOCK_RECT_SET ADDCLOCK_RECT_SET
 * \ingroup CMD_Commands AddClockCommands
 * \brief Команда установки параметров выходного прямоугольного сигнала мезонина AddClock. В случае запуска генератора
 *        по триггерным сигналам необходимо предварительно настроить триггеры командой \ref CMD__ADDCLOCK_TRIG_CFG.
 */
/**@{*/
#define CMD__ADDCLOCK_RECT_SET                    0x01F4
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t frequency;          /*!< Частота сигнала, Гц (50000..200000000) */
    uint32_t phase;              /*!< Начальная фаза сигнала, Градусы (0..360) */
    uint32_t voltage;            /*!< Амплитуда сигнала, мВ (0..2000) */
    uint32_t v_offset;           /*!< Постоянная составляющая сигнала, мВ (0..4000) */
    uint8_t clk_mode;            /*!< Режим тактирования ПЛИС и ЦАП \ref add_clock__clk_mode_e */
    uint8_t start_stop_mode;     /*!< Способ запуска и остановки генератора \ref add_clock__start_stop_mode_e */
}cmd_addclock_rect_set__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t frequency;          /*!< Частота сигнала, Гц */
    uint32_t phase;              /*!< Начальная фаза сигнала, Градусы (0..360) */
    uint32_t voltage;            /*!< Амплитуда сигнала, мВ */
    uint32_t v_offset;           /*!< Постоянная составляющая сигнала, мВ */
}cmd_addclock_rect_set__out_t;
/**@}*/

/**
 * \defgroup GRP_ADDCLOCK_SYNC_SET ADDCLOCK_SYNC_SET
 * \ingroup CMD_Commands AddClockCommands
 * \brief Команда установки параметров выходного сигнала синхронизации  мезонина AddClock.
 *        Выходной сигнал синхронизации – альтернатива синтезу прямоугольника. Достоинства – более крутые фронты,
 *        минимальная задержка относительно ОЧФК (при тактировании от SYNC_CLOCK - \ref ADD_CLOCK__CLK_EXT).
 *        В случае запуска генератора по триггерным сигналам необходимо предварительно настроить триггеры командой \ref CMD__ADDCLOCK_TRIG_CFG.
 *        Параметр work_mode поддерживается частично- только режимы \ref ADD_CLOCK__CLK_INT и \ref ADD_CLOCK__CLK_EXT могут быть установлены.
 */
/**@{*/
#define CMD__ADDCLOCK_SYNC_SET                    0x01F5
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t frequency;          /*!< Частота сигнала, Гц (50000..200000000). Только для режима \ref ADD_CLOCK__CLK_INT */
    uint32_t phase;              /*!< Начальная фаза сигнала, Градусы (0..360). Только для режима \ref ADD_CLOCK__CLK_INT */
    uint8_t clk_mode;            /*!< Режим тактирования \ref add_clock__clk_mode_e */
    uint8_t start_stop_mode;     /*!< Способ запуска и остановки генератора \ref add_clock__start_stop_mode_e */
}cmd_addclock_sync_set__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t frequency;          /*!< Частота сигнала, Гц. (только для режима \ref ADD_CLOCK__CLK_INT)*/
    uint32_t phase;              /*!< Начальная фаза сигнала, Градусы (0..360). (только для режима \ref ADD_CLOCK__CLK_INT)*/
}cmd_addclock_sync_set__out_t;
/**@}*/

/**
 * \defgroup GRP_ADDCLOCK_GEN_CTRL ADDCLOCK_GEN_CTRL
 * \ingroup CMD_Commands AddClockCommands
 * \brief Команда управления генераторами выходных сигналов мезонина AddClock.
 */
/**@{*/
#define CMD__ADDCLOCK_GEN_CTRL                    0x01F6
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t gen_id;                     /*!< Идентификатор генератора \ref add_clock__gen_id_e */
    uint8_t gen_cmd;                    /*!< Команда генератору \ref add_clock__gen_cmd_e */
}cmd_addclock_gen_ctrl__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_addclock_gen_ctrl__out_t;
/**@}*/

/**
 * \defgroup GRP_ADDCLOCK_TEST ADDCLOCK_TEST
 * \ingroup CMD_Commands AddClockCommands
 * \brief Команда тестирования мезонина AddClock. При ошибке тестирования возвращает \ref CMD_ERR__HW_ERROR.
 */
/**@{*/
#define CMD__ADDCLOCK_TEST                    0x01F7
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_addclock_test__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_addclock_test__out_t;
/**@}*/









#pragma pack(pop)

#endif
