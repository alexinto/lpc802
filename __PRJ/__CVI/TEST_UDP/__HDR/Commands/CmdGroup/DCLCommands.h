/***************************************************************************//**
 * @file DCLCommands.h.
 * @brief Модуль функций управления устройствами подсистемы канальной электроники.
 *        Предназначен для реализации функций управления драйверами, компараторами, активной нагрузкой.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DCLCOMMANDS_H_
#define DCLCOMMANDS_H_
#include "Types/DRV/dcl_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_DRV_CFG_SET DRV_CFG_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает конфигурацию драйверов по маске
 */
/**@{*/
#define CMD__DRV_CFG_SET              0x1000
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2]; /*!< Маска драйверов. 64 штуки.*/
    uint8_t vrng;         /*!<Диапазон работы драйверов \ref dcl__v_range_e. */
    double vil;            /*!< Напряжение низкого уровня драйвера, В. */
    double vih;            /*!< Напряжение высокого уровня драйвера, В. */
    uint8_t tmode;         /*!< Режим работы драйвера \ref dcl__term_mode_e*/
    double vtt;            /*!< Напряжение среднего уровня драйвера, напряжение переключения активной нагрузки, В. */
    double iol;            /*!< Ток, втекающий в активную нагрузку (0,000..0,024), A. */
    double ioh;            /*!< Ток, вытекающий из активной нагрузки (0,000… 0,024), А. */
}cmd_drv_cfg_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_cfg_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_CFG_GET DRV_CFG_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает конфигурацию драйвера
 */
/**@{*/
#define CMD__DRV_CFG_GET              0x1001
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_cfg_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    uint8_t vrng;         /*!<Диапазон работы драйверов \ref dcl__v_range_e. */
    double vil;           /*!< Напряжение низкого уровня драйвера, В. */
    double vih;           /*!< Напряжение высокого уровня драйвера, В. */
    uint8_t tmode;         /*!< Режим работы драйвера \ref dcl__term_mode_e*/
    double vtt;           /*!< Напряжение среднего уровня драйвера, напряжение переключения активной нагрузки, В. */
    double iol;           /*!< Ток, втекающий в активную нагрузку (0,000..0,024), A. */
    double ioh;           /*!< Ток, вытекающий из активной нагрузки (0,000… 0,024), А. */
}cmd_drv_cfg_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ID_GET DRV_ID_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает версию драйвера
 */
/**@{*/
#define CMD__DRV_ID_GET              0x1002
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_id_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    uint8_t rev;          /*!< Ревизия.*/
    uint16_t prod_id;     /*!< Версия драйвера.*/
}cmd_drv_id_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_TEMP_GET DRV_TEMP_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает температуру драйвера
 */
/**@{*/
#define CMD__DRV_TEMP_GET              0x1003
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_temp_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;        /*!< Идентификатор драйвера.*/
    double temp;           /*!< Температура драйвера, градус.*/
}cmd_drv_temp_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ALD_SET DRV_ALD_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает конфигурацию активной нагрузки драйверов по маске
 */
/**@{*/
#define CMD__DRV_ALD_SET              0x1004
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2]; /*!< Маска драйверов. 64 штуки.*/
    uint8_t vrng;         /*!<Диапазон работы драйверов \ref dcl__v_range_e. */
    double vil;            /*!< Напряжение низкого уровня драйвера, В. */
    double vih;            /*!< Напряжение высокого уровня драйвера, В. */
    uint8_t tmode;         /*!< Режим работы драйвера \ref dcl__term_mode_e*/
    double vtt;            /*!< Напряжение среднего уровня драйвера, напряжение переключения активной нагрузки, В. */
    double iol;            /*!< Ток, втекающий в активную нагрузку (0,000..0,024), A. */
    double ioh;            /*!< Ток, вытекающий из активной нагрузки (0,000… 0,024), А. */
}cmd_drv_ald_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_ald_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_VRNG_SET DRV_VRNG_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает диапазон напряжения драйверов по маске
 */
/**@{*/
#define CMD__DRV_VRNG_SET              0x1005
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2]; /*!< Маска драйверов. 64 штуки.*/
    uint8_t vrng;         /*!<Диапазон работы драйверов \ref dcl__v_range_e. */
    double vil;            /*!< Напряжение низкого уровня драйвера, В. */
    double vih;            /*!< Напряжение высокого уровня драйвера, В. */
}cmd_drv_vrng_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_vrng_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_VRNG_GET DRV_VRNG_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает диапазон напряжения драйвера
 */
/**@{*/
#define CMD__DRV_VRNG_GET              0x1006
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;        /*!< Идентификатор драйвера.*/
}cmd_drv_vrng_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    uint8_t vrng;         /*!<Диапазон работы драйверов \ref dcl__v_range_e. */
    double vil;           /*!< Напряжение низкого уровня драйвера, В. */
    double vih;           /*!< Напряжение высокого уровня драйвера, В. */
}cmd_drv_vrng_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_VIL_SET DRV_VIL_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает напряжение низкого уровня драйверов по маске
 */
/**@{*/
#define CMD__DRV_VIL_SET              0x1007
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2]; /*!< Маска драйверов. 64 штуки.*/
    double vil;            /*!< Напряжение низкого уровня драйвера, В. */
}cmd_drv_vil_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_vil_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_VIL_GET DRV_VIL_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает напряжение низкого уровня драйвера
 */
/**@{*/
#define CMD__DRV_VIL_GET              0x1008
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;        /*!< Идентификатор драйвера.*/
}cmd_drv_vil_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double vil;           /*!< Напряжение низкого уровня драйвера, В. */
}cmd_drv_vil_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_VIH_SET DRV_VIH_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает напряжение высокого уровня драйверов по маске
 */
/**@{*/
#define CMD__DRV_VIH_SET              0x1009
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2]; /*!< Маска драйверов. 64 штуки.*/
    double vih;            /*!< Напряжение высокого уровня драйвера, В. */
}cmd_drv_vih_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_vih_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_VIH_GET DRV_VIH_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает напряжение высокого уровня драйвера
 */
/**@{*/
#define CMD__DRV_VIH_GET              0x100A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;        /*!< Идентификатор драйвера.*/
}cmd_drv_vih_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double vih;           /*!< Напряжение высокого уровня драйвера, В. */
}cmd_drv_vih_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_TMODE_SET DRV_TMODE_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает режим работы драйверов по маске
 */
/**@{*/
#define CMD__DRV_TMODE_SET              0x100B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2]; /*!< Маска драйверов. 64 штуки.*/
    uint8_t tmode;         /*!< Режим работы драйвера \ref dcl__term_mode_e*/
}cmd_drv_tmode_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_tmode_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_TMODE_GET DRV_TMODE_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает режим работы драйвера
 */
/**@{*/
#define CMD__DRV_TMODE_GET              0x100C
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;        /*!< Идентификатор драйвера.*/
}cmd_drv_tmode_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    uint8_t tmode;         /*!< Режим работы драйвера \ref dcl__term_mode_e*/
}cmd_drv_tmode_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_IOL_SET DRV_IOL_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает ток, втекающий в активную нагрузку драйверов по маске
 */
/**@{*/
#define CMD__DRV_IOL_SET              0x100D
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double iol;            /*!< Ток, втекающий в активную нагрузку (0,000..0,024), A. */
}cmd_drv_iol_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_iol_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_IOL_GET DRV_IOL_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает ток, втекающий в активную нагрузку драйвера
 */
/**@{*/
#define CMD__DRV_IOL_GET              0x100E
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;        /*!< Идентификатор драйвера.*/
}cmd_drv_iol_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double iol;            /*!< Ток, втекающий в активную нагрузку (0,000..0,024), A. */
}cmd_drv_iol_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_IOH_SET DRV_IOH_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает ток, вытекающий из активной нагрузки драйверов по маске
 */
/**@{*/
#define CMD__DRV_IOH_SET              0x100F
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double ioh;            /*!< Ток, вытекающий из активной нагрузки (0,000… 0,024), А. */
}cmd_drv_ioh_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_ioh_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_IOL_GET DRV_IOL_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает ток, вытекающий из активной нагрузки драйвера
 */
/**@{*/
#define CMD__DRV_IOH_GET              0x1010
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;        /*!< Идентификатор драйвера.*/
}cmd_drv_ioh_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double ioh;            /*!< Ток, вытекающий из активной нагрузки (0,000… 0,024), А. */
}cmd_drv_ioh_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ILOAD_SET DRV_ILOAD_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает ток активной нагрузки драйверов по маске
 */
/**@{*/
#define CMD__DRV_ILOAD_SET              0x1011
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double iol;            /*!< Ток, втекающий в активную нагрузку (0,000..0,024), A. */
    double ioh;            /*!< Ток, вытекающий из активной нагрузки (0,000… 0,024), А. */
}cmd_drv_iload_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_iload_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ILOAD_GET DRV_ILOAD_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает ток активной нагрузки драйвера
 */
/**@{*/
#define CMD__DRV_ILOAD_GET              0x1012
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;        /*!< Идентификатор драйвера.*/
}cmd_drv_iload_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double iol;            /*!< Ток, втекающий в активную нагрузку (0,000..0,024), A. */
    double ioh;            /*!< Ток, вытекающий из активной нагрузки (0,000… 0,024), А. */
}cmd_drv_iload_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ROUT_SET DRV_ROUT_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Задаёт величину коррекции выходного сопротивления драйверов по маске
 */
/**@{*/
#define CMD__DRV_ROUT_SET              0x1013
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double rol;            /*!< todo разобраться  */
    double roh;            /*!< todo разобраться */
    double rvtt;            /*!< todo разобраться */
}cmd_drv_rout_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_rout_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ROUT_GET DRV_ROUT_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает величину коррекции выходного сопротивления драйвера
 */
/**@{*/
#define CMD__DRV_ROUT_GET              0x1014
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_rout_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double rol;            /*!< todo разобраться  */
    double roh;            /*!< todo разобраться */
    double rvtt;            /*!< todo разобраться */
}cmd_drv_rout_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ROH_SET DRV_ROH_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief разобраться что делает драйверов по маске
 */
/**@{*/
#define CMD__DRV_ROH_SET              0x1015
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double roh;            /*!< todo разобраться */
}cmd_drv_roh_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_roh_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ROH_GET DRV_ROH_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief разобраться что возвращает драйвера
 */
/**@{*/
#define CMD__DRV_ROH_GET              0x1016
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_roh_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double roh;            /*!< todo разобраться */
}cmd_drv_roh_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ROL_SET DRV_ROL_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief  драйверов по маске
 */
/**@{*/
#define CMD__DRV_ROL_SET              0x1017
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double rol;            /*!< todo разобраться  */
}cmd_drv_rol_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_rol_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ROL_GET DRV_ROL_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief  драйвера
 */
/**@{*/
#define CMD__DRV_ROL_GET              0x1018
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_rol_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double rol;            /*!< todo разобраться  */
}cmd_drv_rol_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_RVTT_SET DRV_RVTT_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief драйверов по маске
 */
/**@{*/
#define CMD__DRV_RVTT_SET              0x1019
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double rvtt;            /*!< todo разобраться */
}cmd_drv_rvtt_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_rvtt_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_RVTT_GET DRV_RVTT_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief  драйвера
 */
/**@{*/
#define CMD__DRV_RVTT_GET              0x101A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_rvtt_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double rvtt;            /*!< todo разобраться */
}cmd_drv_rvtt_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ROUTI_SET DRV_ROUTI_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Корректирует выходное сопротивление драйверов (по маске) канальной электроники в зависимости от предполагаемого тока нагрузки.
 */
/**@{*/
#define CMD__DRV_ROUTI_SET              0x101B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double ril;            /*!< Ток нагрузки низкого уровня драйвера  */
    double rih;            /*!< Ток нагрузки высокого уровня драйвера */
    double rit;            /*!< Ток нагрузки среднего уровня драйвера */
}cmd_drv_routi_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_routi_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_ROUTI_GET DRV_ROUTI_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданные значения токов нагрузки драйвера канальной электроники
 */
/**@{*/
#define CMD__DRV_ROUTI_GET              0x101C
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_routi_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double ril;            /*!< Ток нагрузки низкого уровня драйвера  */
    double rih;            /*!< Ток нагрузки высокого уровня драйвера */
    double rit;            /*!< Ток нагрузки среднего уровня драйвера */
}cmd_drv_routi_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_RIL_SET DRV_RIL_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Корректирует выходное сопротивление низкого уровня драйверов канальной элек-троники в зависимости от предполагаемого тока нагрузки.
 */
/**@{*/
#define CMD__DRV_RIL_SET              0x101D
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double ril;            /*!< Ток нагрузки низкого уровня драйвера */
}cmd_drv_ril_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_ril_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_RIL_GET DRV_RIL_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданное значение тока нагрузки низкого уровня драйвера канальной электроники
 */
/**@{*/
#define CMD__DRV_RIL_GET              0x101E
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_ril_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double ril;            /*!< Ток нагрузки низкого уровня драйвера  */
}cmd_drv_ril_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_RIH_SET DRV_RIH_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Корректирует выходное сопротивление высокого уровня драйверов канальной элек-троники в зависимости от предполагаемого тока нагрузки
 */
/**@{*/
#define CMD__DRV_RIH_SET              0x101F
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double rih;            /*!< Ток нагрузки высокого уровня драйвера */
}cmd_drv_rih_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_rih_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_RIH_GET DRV_RIH_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданное значение тока нагрузки высокого уровня драйвера канальной электроники
 */
/**@{*/
#define CMD__DRV_RIH_GET              0x1020
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_rih_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double rih;            /*!< Ток нагрузки высокого уровня драйвера */
}cmd_drv_rih_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_RIT_SET DRV_RIT_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Корректирует выходное сопротивление среднего уровня драйверов канальной элек-троники в зависимости от предполагаемого тока нагрузки
 */
/**@{*/
#define CMD__DRV_RIT_SET              0x1021
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double rit;            /*!< Ток нагрузки среднего уровня драйвера */
}cmd_drv_rit_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_rit_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_RIT_GET DRV_RIT_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданное значение тока нагрузки среднего уровня драйвера канальной электроники
 */
/**@{*/
#define CMD__DRV_RIT_GET              0x1022
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_rit_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double rit;            /*!< Ток нагрузки среднего уровня драйвера */
}cmd_drv_rit_get__out_t;
/**@}*/

/**
 * \defgroup GRP_CMP_SET CMP_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает параметры компараторов канальной электроники для указанной группы каналов
 */
/**@{*/
#define CMD__CMP_SET              0x1023
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска компараторов. 64 штуки.*/
    uint8_t rng;           /*!< Диапазон \ref dcl__v_range_e */
    double cpl;            /*!< Напряжение низкого уровня контроля компаратора, В */
    double cph;            /*!< Напряжение высокого уровня контроля компаратора, В */
}cmd_cmp_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_cmp_set__out_t;
/**@}*/

/**
 * \defgroup GRP_CMP_GET CMP_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданные параметры режима работы компараторов канальной электро-ники для одного канала
 */
/**@{*/
#define CMD__CMP_GET              0x1024
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор компаратора.*/
}cmd_cmp_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор компаратора.*/
    uint8_t rng;           /*!< Диапазон \ref dcl__v_range_e */
    double cpl;            /*!< Напряжение низкого уровня контроля компаратора, В */
    double cph;            /*!< Напряжение высокого уровня контроля компаратора, В */
}cmd_cmp_get__out_t;
/**@}*/

/**
 * \defgroup GRP_CMP_RNG_SET CMP_RNG_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает диапазон работы компараторов канальной электроники для группы каналов
 */
/**@{*/
#define CMD__CMP_RNG_SET              0x1025
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска компараторов. 64 штуки.*/
    uint8_t rng;           /*!< Диапазон \ref dcl__v_range_e */
}cmd_cmp_rng_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_cmp_rng_set__out_t;
/**@}*/

/**
 * \defgroup GRP_CMP_RNG_GET CMP_RNG_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданный диапазон работы компараторов канальной электроники для одного канала
 */
/**@{*/
#define CMD__CMP_RNG_GET              0x1026
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор компаратора.*/
}cmd_cmp_rng_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор компаратора.*/
    uint8_t rng;          /*!< Диапазон \ref dcl__v_range_e */
}cmd_cmp_rng_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_CPL_SET DRV_CPL_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает напряжение контроля низкого уровня компараторов канальной элек-троники для указанной группы каналов.
 */
/**@{*/
#define CMD__DRV_CPL_SET              0x1027
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double cpl;            /*!< Напряжение низкого уровня контроля компаратора, В */
}cmd_drv_cpl_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_cpl_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_CPL_GET DRV_CPL_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданное напряжение контроля низкого уровня компараторов каналь-ной электроники для одного канала
 */
/**@{*/
#define CMD__DRV_CPL_GET              0x1028
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_cpl_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double cpl;            /*!< Напряжение низкого уровня контроля компаратора, В */
}cmd_drv_cpl_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_CPH_SET DRV_CPH_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает напряжение контроля высокого уровня компараторов канальной электроники для указанной группы каналов
 */
/**@{*/
#define CMD__DRV_CPH_SET              0x1029
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    double cph;            /*!< Напряжение высокого уровня контроля компаратора, В */
}cmd_drv_cph_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_cph_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_CPH_GET DRV_CPH_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданное напряжение контроля высокого уровня компараторов каналь-ной электроники для одного канала
 */
/**@{*/
#define CMD__DRV_CPH_GET              0x102A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_cph_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    double cph;            /*!< Напряжение высокого уровня контроля компаратора, В */
}cmd_drv_cph_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_MODE_SET DRV_MODE_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает режим выдачи напряжений драйвером канальной электроники для указанной группы каналов
 */
/**@{*/
#define CMD__DRV_MODE_SET              0x102B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    uint8_t mode;          /*!< Режим выдачи напряжений \ref dcl__mode_e */
    uint8_t state;         /*!< Уровень воспроизводимого напряжения в режиме DCL__MODE_STATIC \ref dcl__state_e */
}cmd_drv_mode_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_mode_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_MODE_GET DRV_MODE_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданный режим выдачи драйверов канальной электроники для одного канала
 */
/**@{*/
#define CMD__DRV_MODE_GET              0x102C
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_mode_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    uint8_t mode;          /*!< Режим выдачи напряжений \ref dcl__mode_e */
    uint8_t state;         /*!< Уровень воспроизводимого напряжения в режиме DCL__MODE_STATIC \ref dcl__state_e */
}cmd_drv_mode_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_STATE_SET DRV_STATE_SET
 * \ingroup CMD_Commands DCLCommands
 * \brief Устанавливает уровень напряжения драйвера канальной электроники для указанной группы каналов
 */
/**@{*/
#define CMD__DRV_STATE_SET              0x102D
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];  /*!< Маска драйверов. 64 штуки.*/
    uint8_t state;         /*!< Уровень воспроизводимого напряжения в режиме DCL__MODE_STATIC \ref dcl__state_e */
}cmd_drv_state_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_drv_state_set__out_t;
/**@}*/

/**
 * \defgroup GRP_DRV_STATE_GET DRV_STATE_GET
 * \ingroup CMD_Commands DCLCommands
 * \brief Возвращает заданный уровень драйвера канальной электроники для одного канала
 */
/**@{*/
#define CMD__DRV_STATE_GET              0x102E
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
}cmd_drv_state_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t drv_id;       /*!< Идентификатор драйвера.*/
    uint8_t state;         /*!< Уровень воспроизводимого напряжения в режиме DCL__MODE_STATIC \ref dcl__state_e */
}cmd_drv_state_get__out_t;
/**@}*/



#pragma pack(pop)

#endif

