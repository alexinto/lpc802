#ifndef TRIGGERCOMMANDS_H_
#define TRIGGERCOMMANDS_H_

#include "Types/DRV/trigger_types.h"

#pragma pack(push, 1)


/**
 * \defgroup GRP_TRIGGER TRIGGER
 * \ingroup CMD_Commands TriggerCommands
 * \brief Настройки триггерного сигнала.
 * @details - Num – номер триггера 1…32,
 *          - Script – номер скрипта 0…32 (0- нет скрипта),
 *          - delay – задержка выполнения скрипта после получения сигнала триггера,
 *          - slope – срабатывание триггера по переднему, по заднему, по обоим фронтам:
 *              * 0 - POSitive
 *              * 1 - NEGative
 *              * 2 - EITHer
 *          - mode – режим работы триггера
 *              * 1 - ARM
 *              * 2 - AUTO
 *          - ARM – однократное срабатывание <tt>ARM&rarr;OFF</tt>
 *          - AUTO – автоматический перезапуск
 */
/**@{*/
#define CMD__TRIGGER                0x0170
/**
 * Входные параметры команды.
 */
typedef struct {
    cmd_trigger__io_t data;
}cmd_trigger__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    cmd_trigger__io_t data;
}cmd_trigger__out_t;
/**@}*/

/**
 * \defgroup GRP_TRIGGER_REQ TRIGGER_REQ
 * \ingroup CMD_Commands TriggerCommands
 * \brief Запрос настроек триггерного сигнала
 */
/**@{*/
#define CMD__TRIGGER_REQ            0x0171
/**
 * Входные параметры команды.
 */
typedef struct{
    uint8_t trigger_num;        /*!< Номер запрашиваемого триггера */
}cmd_trigger_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t num;                /*!< Номер триггера */
    uint8_t script;             /*!< Номер скрипта */
    uint16_t delay;             /*!< Задержка выполнения [мкс] */
    uint8_t slope;              /*!< Настройки срабатывания */
    uint8_t mode;               /*!< Режим работы */
    uint8_t state;              /*!< Состояние триггера */
}cmd_trigger_req__out_t;
/**@}*/

/**
 * \defgroup GRP_TRIGGER_STATE TRIGGER_STATE
 * \ingroup CMD_Commands TriggerCommands
 * \brief Изменение состояния триггерного сигнала.
 * @details Состояние:
 *          - 1 – READY
 *          - 2 – OFF
 *          - 3 – RUN
 */
/**@{*/
#define CMD__TRIGGER_STATE          0x0172
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t trigger_num;    /*!< номер запрашиваемого триггера */
    uint8_t trigger_state;  /*!< состояние триггера */
}cmd_trigger_state__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t trigger_num;    /*!< номер запрашиваемого триггера */
    uint8_t trigger_state;  /*!< состояние триггера */
}cmd_trigger_state__out_t;
/**@}*/

/**
 * \defgroup GRP_TRIGGER_STATE_REQ TRIGGER_STATE_REQ
 * \ingroup CMD_Commands TriggerCommands
 * \brief Запрос состояния триггерного сигнала.
 */
/**@{*/
#define CMD__TRIGGER_STATE_REQ      0x0173
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t trigger_num;        /*!< Номер запрашиваемого триггера */
}cmd_trigger_state_req__in_t;   /*!< Входные параметры команды */
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t trigger_num;    /*!< Номер запрашиваемого триггера */
    uint8_t trigger_state;  /*!< Сотояние запрашиваемого триггера */
}cmd_trigger_state_req__out_t;
/**@}*/

/**
 * \defgroup GRP_TRIGGER_OUTPUT TRIGGER_OUTPUT
 * \ingroup CMD_Commands TriggerCommands
 * \brief Запускает формирование триггерного сигнала.
 */
/**@{*/
#define CMD__TRIGGER_OUTPUT         0x0174
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t trigger_num;        /*!< Номер триггера */
    uint16_t trigger_duration;  /*!< Длительность триггера <tt>[мкс]</tt> */
}cmd_trigger_output__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_trigger_output__out_t;
/**@}*/






#pragma pack(pop)

#endif
