/***************************************************************************//**
 * @file PGCommands.h.
 * @brief Модуль функций управления ГВП.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef PGCOMMANDS_H_
#define PGCOMMANDS_H_

#include "Types/System/cmd_oscillator_types.h"
#include "Types/DRV/pg_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_PG_FC_SET PG_FC_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Задание настроек частоты ФК. Частота ФК формируется в ПЛИС PG из частоты Fin (задаётся \ref CMD__ROSCILLATOR_CH) с помощью делителя clkd_divider.
 */
/**@{*/
#define CMD__PG_FC_SET              0x1100
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pg_mask[2];   /*!< Маска ГВП. */
    uint8_t domain;        /*!< Выбор частотного домена, в котором должны работать PG (\ref OSCIL_CH_ID__FDOM1 или \ref OSCIL_CH_ID__FDOM2) */
    uint8_t clk_div;       /*!< Делитель частоты ОЧФК (1..15). Значение по умолчанию = 1. */
}cmd_pg_fc_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_fc_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_FC_GET PG_FC_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает настройки частоты ФК.
 */
/**@{*/
#define CMD__PG_FC_GET              0x1101
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
}cmd_pg_fc_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
    uint8_t domain;        /*!< Выбор частотного домена, в котором должны работать PG (\ref OSCIL_CH_ID__FDOM1 или \ref OSCIL_CH_ID__FDOM2) */
    uint8_t clk_div;       /*!< Делитель частоты ОЧФК (1..15). Значение по умолчанию = 1. */
}cmd_pg_fc_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_ADDR_SET PG_ADDR_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Устанавливает диапазон векторов ФК для выбранных ГВП.
 */
/**@{*/
#define CMD__PG_ADDR_SET              0x1102
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pg_mask[2];   /*!< Маска ГВП. */
    uint32_t start_addr;   /*!< Адрес первого вектора ГВП*/
    uint32_t stop_addr;    /*!< Адрес последнего вектора ГВП*/
}cmd_pg_addr_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_addr_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_ADDR_GET PG_ADDR_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает диапазон векторов ФК для выбранного ГВП.
 */
/**@{*/
#define CMD__PG_ADDR_GET              0x1103
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
}cmd_pg_addr_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
    uint32_t start_addr;   /*!< Адрес первого вектора ГВП*/
    uint32_t stop_addr;    /*!< Адрес последнего вектора ГВП*/
}cmd_pg_addr_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_PATTERN_SET PG_PATTERN_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Устанавливает формат векторов ТП для выбранных ГВП.
 */
/**@{*/
#define CMD__PG_PATTERN_SET           0x1104
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pg_mask[2];   /*!< Маска ГВП. */
    uint8_t pattern_type;  /*!< Формат векторов ТП \ref pg__pattern_type_e */
}cmd_pg_pattern_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_pattern_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_PATTERN_GET PG_PATTERN_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает формат векторов ТП для выбранного ГВП.
 */
/**@{*/
#define CMD__PG_PATTERN_GET           0x1105
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
}cmd_pg_pattern_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
    uint8_t pattern_type;  /*!< Формат векторов ТП \ref pg__pattern_type_e */
}cmd_pg_pattern_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_FLAG_SET PG_FLAG_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Устанавливает флаги по маске для выбранных ГВП.
 */
/**@{*/
#define CMD__PG_FLAG_SET           0x1106
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pg_mask[2];   /*!< Маска ГВП. */
    uint32_t flag_mask[2];   /*!< Маска флагов. */
}cmd_pg_flag_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_flag_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_FLAG_GET PG_FLAG_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Считывает флаги для выбранного ГВП.
 */
/**@{*/
#define CMD__PG_FLAG_GET           0x1107
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;           /*!< Идентификатор ГВП.*/
}cmd_pg_flag_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;           /*!< Идентификатор ГВП.*/
    uint32_t flag_mask[2];   /*!< Маска флагов. */
}cmd_pg_flag_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_STATE_GET PG_STATE_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает текущее состояние выбранного ГВП.
 */
/**@{*/
#define CMD__PG_STATE_GET           0x1108
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;           /*!< Идентификатор ГВП.*/
}cmd_pg_state_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;           /*!< Идентификатор ГВП.*/
    uint8_t state;           /*!< Состояние ГВП \ref pg__fc_ctrl_e */
}cmd_pg_state_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_BREAK_SET PG_BREAK_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Устанавливает режим остановки ФК.
 */
/**@{*/
#define CMD__PG_BREAK_SET              0x1109
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pg_mask[2];   /*!< Маска ГВП. */
    uint8_t pg_break;      /*!< 0- PASSIVE_BREAK, 1- ACTIVE_BREAK */
}cmd_pg_break_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_break_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_BREAK_GET PG_BREAK_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает режим остановки ФК.
 */
/**@{*/
#define CMD__PG_BREAK_GET              0x110A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;           /*!< Идентификатор ГВП.*/
}cmd_pg_break_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;           /*!< Идентификатор ГВП.*/
    uint8_t pg_break;        /*!< 0- PASSIVE_BREAK, 1- ACTIVE_BREAK */
}cmd_pg_break_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_FORMAT_SET PG_FORMAT_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Устанавливает метки драйвера для текущего набора временных параметров и каналов, а также задает форму сигнала.
 */
/**@{*/
#define CMD__PG_FORMAT_SET              0x110B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_mask[2];   /*!< Маска каналов. */
    uint8_t form;          /*!<  \ref pg__drv_mode_e */
    double t1;             /*!< Смещение метки T1 относительно вектора ФК, нс. */
    double t2;             /*!< Смещение метки T2 относительно вектора ФК, нс. */
}cmd_pg_format_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_format_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_FORMAT_GET PG_FORMAT_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает метки драйвера для текущего набора временных параметров и каналов, а также настройки формы сигнала.
 */
/**@{*/
#define CMD__PG_FORMAT_GET              0x110C
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ch_id;         /*!< Идентификатор канала (0..63).*/
}cmd_pg_format_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ch_id;         /*!< Идентификатор канала (0..63).*/
    uint8_t form;          /*!< \ref pg__drv_mode_e */
    double t1;             /*!< Смещение метки T1 относительно вектора ФК, нс. */
    double t2;             /*!< Смещение метки T2 относительно вектора ФК, нс. */
}cmd_pg_format_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_CMP_SET PG_CMP_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Устанавливает метки компараторов для текущего набора временных параметров и каналов.
 */
/**@{*/
#define CMD__PG_CMP_SET                  0x110D
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_mask[2];   /*!< Маска каналов. */
    uint8_t mode;          /*!<  \ref pg__cmp_mode_e */
    double t3;             /*!< Смещение метки T3 относительно вектора ФК, нс. */
    double t4;             /*!< Смещение метки T4 относительно вектора ФК, нс. */
}cmd_pg_cmp_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_cmp_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_CMP_GET PG_CMP_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает метки компараторов для текущего набора временных параметров и каналов.
 */
/**@{*/
#define CMD__PG_CMP_GET                  0x110E
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ch_id;         /*!< Идентификатор канала (0..63).*/
}cmd_pg_cmp_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ch_id;         /*!< Идентификатор канала (0..63).*/
    uint8_t mode;          /*!<  \ref pg__cmp_mode_e */
    double t3;             /*!< Смещение метки T3 относительно вектора ФК, нс. */
    double t4;             /*!< Смещение метки T4 относительно вектора ФК, нс. */
}cmd_pg_cmp_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_MUX_MODE_SET PG_MUX_MODE_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Устанавливает режим мультиплексирования (DDR) для заданных каналов. Настройка применима только для четных каналов.
 */
/**@{*/
#define CMD__PG_MUX_MODE_SET                  0x110F
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_mask[2];   /*!< Маска каналов. */
    uint8_t mode;          /*!< 0- выключен, 1- включен */
}cmd_pg_mux_mode_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_mux_mode_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_MUX_MODE_GET PG_MUX_MODE_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвроащает режим мультиплексирования (DDR) для канала. Настройка применима только для четных каналов.
 */
/**@{*/
#define CMD__PG_MUX_MODE_GET                  0x1110
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ch_id;         /*!< Идентификатор канала (0..63).*/
}cmd_pg_mux_mode_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ch_id;         /*!< Идентификатор канала (0..63).*/
    uint8_t mode;          /*!< 0- выключен, 1- включен  */
}cmd_pg_mux_mode_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_DIFF_MODE_SET PG_DIFF_MODE_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Устанавливает дифференциальный режим для заданных каналов. Настройка применима только для четных каналов.
 */
/**@{*/
#define CMD__PG_DIFF_MODE_SET                  0x1111
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_mask[2];   /*!< Маска каналов. */
    uint8_t mode;          /*!< 0- выключен, 1- включен */
}cmd_pg_diff_mode_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_diff_mode_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_DIFF_MODE_GET PG_DIFF_MODE_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает настройку дифференциального режима для канала. Настройка применима только для четных каналов.
 */
/**@{*/
#define CMD__PG_DIFF_MODE_GET                  0x1112
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ch_id;         /*!< Идентификатор канала (0..63).*/
}cmd_pg_diff_mode_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ch_id;         /*!< Идентификатор канала (0..63).*/
    uint8_t mode;          /*!< 0- выключен, 1- включен */
}cmd_pg_diff_mode_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_ERR_RAM_MODE_SET PG_ERR_RAM_MODE_SET
 * \ingroup CMD_Commands PGCommands
 * \brief Выбор режима записи карты ошибок.
 */
/**@{*/
#define CMD__PG_ERR_RAM_MODE_SET                  0x1113
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pg_mask[2];   /*!< Маска ГВП. */
    uint8_t mode;          /*!< Режима записи карты ошибок \ref pg__err_ram_mode_e */
}cmd_pg_err_ram_mode_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pg_err_ram_mode_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_ERR_RAM_MODE_GET PG_ERR_RAM_MODE_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает режим записи карты ошибок.
 */
/**@{*/
#define CMD__PG_ERR_RAM_MODE_GET                  0x1114
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
}cmd_pg_err_ram_mode_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
    uint8_t mode;          /*!< Режима записи карты ошибок \ref pg__err_ram_mode_e */
}cmd_pg_err_ram_mode_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_FIRST_ERR_CLK_GET PG_FIRST_ERR_CLK_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает номер первого такта с ошибкой.
 */
/**@{*/
#define CMD__PG_FIRST_ERR_CLK_GET                 0x1115
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
}cmd_pg_first_err_clk_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
    uint32_t idx[2];       /*!< Номер первого такта с ошибкой - uint64_t */
}cmd_pg_first_err_clk_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PG_ERR_RAM_NUM_GET PG_ERR_RAM_NUM_GET
 * \ingroup CMD_Commands PGCommands
 * \brief Возвращает количество записей в памяти ошибок ГВП.
 */
/**@{*/
#define CMD__PG_ERR_RAM_NUM_GET                 0x1116
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;         /*!< Идентификатор ГВП.*/
}cmd_pg_err_ram_num_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;          /*!< Идентификатор ГВП.*/
    uint32_t err_num;       /*!< Количество записей в памяти ошибок ГВП */
}cmd_pg_err_ram_num_get__out_t;
/**@}*/







#pragma pack(pop)

#endif
