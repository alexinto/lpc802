/***************************************************************************//**
 * @file PPMUCommands.h.
 * @brief Модуль функций управления устройствами подсистемы канальной электроники.
 *        Предназначен для реализации функций управления подсистемой измерений.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef PPMUCOMMANDS_H_
#define PPMUCOMMANDS_H_

#include "Types/DRV/dcl_types.h"
#include "Types/DRV/ppmu_types.h"

#pragma pack(push, 1)


/**
 * \defgroup GRP_PPMU_CONNECT PPMU_CONNECT
 * \ingroup CMD_Commands PPMUCommands
 * \brief Подключает\отключает PPMU от группы каналов.
 */
/**@{*/
#define CMD__PPMU_CONNECT              0x1050
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ppmu_mask[2]; /*!< Маска PPMU. 64 штуки.*/
    uint8_t state;         /*!< Состояние PPMU \ref ppmu__state_e */
}cmd_ppmu_connect__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_ppmu_connect__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_IS_CONNECT PPMU_IS_CONNECT
 * \ingroup CMD_Commands PPMUCommands
 * \brief Возвращает ранее заданное состояние выхода PPMU.
 */
/**@{*/
#define CMD__PPMU_IS_CONNECT              0x1051
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ppmu_id;       /*!< Идентификатор PPMU.*/
}cmd_ppmu_is_connect__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ppmu_id;       /*!< Идентификатор PPMU.*/
    uint8_t state;         /*!< Состояние PPMU \ref ppmu__state_e */
}cmd_ppmu_is_connect__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_FN_SET PPMU_FN_SET
 * \ingroup CMD_Commands PPMUCommands
 * \brief Устанавливает высокоимпедансное состояние выхода PPMU.
 */
/**@{*/
#define CMD__PPMU_FN_SET                  0x1052
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ppmu_mask[2];     /*!< Маска PPMU. 64 штуки.*/
}cmd_ppmu_fn_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_ppmu_fn_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_FN_GET PPMU_FN_GET
 * \ingroup CMD_Commands PPMUCommands
 * \brief Возвращает состояние выхода PPMU.
 */
/**@{*/
#define CMD__PPMU_FN_GET                  0x1053
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ppmu_id;       /*!< Идентификатор PPMU.*/
}cmd_ppmu_fn_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ppmu_id;       /*!< Идентификатор PPMU.*/
    uint8_t state;         /*!< 0- нормальный режим работы, 1- высокоимпедансное состояние. */
}cmd_ppmu_fn_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_FV_SET PPMU_FV_SET
 * \ingroup CMD_Commands PPMUCommands
 * \brief Переводит PPMU в режим воспроизведения напряжения для указанной группы каналов.
 */
/**@{*/
#define CMD__PPMU_FV_SET                  0x1054
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ppmu_mask[2];     /*!< Маска PPMU. 64 штуки.*/
    uint8_t u_rng;             /*!< Диапазон работы PPMU \ref dcl__v_range_e */
    uint8_t i_rng;             /*!< диапазон тока PPMU \ref ppmu__i_rng_e */
    double u;                  /*!< Величина воспроизводимого напряжения PPMU, В */
}cmd_ppmu_fv_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_ppmu_fv_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_FV_GET PPMU_FV_GET
 * \ingroup CMD_Commands PPMUCommands
 * \brief Возвращает настройки PPMU в режиме воспроизведения напряжения.
 */
/**@{*/
#define CMD__PPMU_FV_GET                  0x1055
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ppmu_id;       /*!< Идентификатор PPMU.*/
}cmd_ppmu_fv_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ppmu_id;       /*!< Идентификатор PPMU.*/
    uint8_t u_rng;         /*!< Диапазон работы PPMU \ref dcl__v_range_e */
    uint8_t i_rng;         /*!< диапазон тока PPMU \ref ppmu__i_rng_e */
    double u;              /*!< Величина воспроизводимого напряжения PPMU, В */
}cmd_ppmu_fv_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_FI_SET PPMU_FI_SET
 * \ingroup CMD_Commands PPMUCommands
 * \brief Переводит PPMU в режим воспроизведения тока для указанной группы каналов.
 */
/**@{*/
#define CMD__PPMU_FI_SET                  0x1056
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ppmu_mask[2];     /*!< Маска PPMU. 64 штуки.*/
    uint8_t u_rng;             /*!< Диапазон работы PPMU \ref dcl__v_range_e */
    uint8_t i_rng;             /*!< диапазон тока PPMU \ref ppmu__i_rng_e */
    double u_clamp[2];         /*!< Уровни ограничения напряжения PPMU: [0] – нижний уровень, [1] – верхний (В). Зависят от \ref dcl__v_range_e */
}cmd_ppmu_fi_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_ppmu_fi_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_FI_GET PPMU_FI_GET
 * \ingroup CMD_Commands PPMUCommands
 * \brief Возвращает настройки PPMU в режиме воспроизведения тока.
 */
/**@{*/
#define CMD__PPMU_FI_GET                  0x1057
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ppmu_id;           /*!< Идентификатор PPMU.*/
}cmd_ppmu_fi_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ppmu_id;           /*!< Идентификатор PPMU.*/
    uint8_t u_rng;             /*!< Диапазон работы PPMU \ref dcl__v_range_e */
    uint8_t i_rng;             /*!< диапазон тока PPMU \ref ppmu__i_rng_e */
    double u_clamp[2];         /*!< Уровни ограничения напряжения PPMU: [0] – нижний уровень, [1] – верхний (В). Зависят от \ref dcl__v_range_e */
}cmd_ppmu_fi_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_FR_SET PPMU_FR_SET
 * \ingroup CMD_Commands PPMUCommands
 * \brief Переводит PPMU в режим резистивной нагрузки для указанной группы каналов.
 */
/**@{*/
#define CMD__PPMU_FR_SET                  0x1058
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ppmu_mask[2];     /*!< Маска PPMU. 64 штуки.*/
    uint8_t u_rng;             /*!< Диапазон работы PPMU \ref dcl__v_range_e */
    uint8_t r_load;            /*!< Величина сопротивления нагрузки \ref ppmu__r_load_e */
    double u;                  /*!< Величина напряжения Vload, В */
}cmd_ppmu_fr_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_ppmu_fr_set__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_FR_GET PPMU_FR_GET
 * \ingroup CMD_Commands PPMUCommands
 * \brief Возвращает настройки PPMU в режим резистивной нагрузки.
 */
/**@{*/
#define CMD__PPMU_FR_GET                  0x1059
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ppmu_id;           /*!< Идентификатор PPMU.*/
}cmd_ppmu_fr_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ppmu_id;           /*!< Идентификатор PPMU.*/
    uint8_t u_rng;             /*!< Диапазон работы PPMU \ref dcl__v_range_e */
    uint8_t r_load;            /*!< Величина сопротивления нагрузки \ref ppmu__r_load_e */
    double u;                  /*!< Величина напряжения Vload, В */
}cmd_ppmu_fr_get__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_MEAS_U PPMU_MEAS_U
 * \ingroup CMD_Commands PPMUCommands
 * \brief Измеряет напряжение на канале с помощью PPMU канальной электроники для заданного канала.
 */
/**@{*/
#define CMD__PPMU_MEAS_U                  0x105A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ppmu_id;           /*!< Идентификатор PPMU.*/
    uint8_t u_rng;             /*!< Диапазон измерения напряжения: \ref ppmu__meas_u_rng_e */
    uint8_t count;             /*!< Количество измерений для усреднения результата 1..255. */
}cmd_ppmu_meas_u__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ppmu_id;           /*!< Идентификатор PPMU.*/
    double u;                  /*!< Измеренное напряжения, В. Если count больше «1», то u – среднеарифметическое выполненных измерений */
    double u_rms;              /*!< СКО измерений. Если count задан «1», то возвращает u_rms равным «0» */
}cmd_ppmu_meas_u__out_t;
/**@}*/

/**
 * \defgroup GRP_PPMU_MEAS_I PPMU_MEAS_I
 * \ingroup CMD_Commands PPMUCommands
 * \brief Измеряет ток через выход PPMU канальной электроники для заданного канала.
 */
/**@{*/
#define CMD__PPMU_MEAS_I                  0x105B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t ppmu_id;           /*!< Идентификатор PPMU.*/
    uint8_t count;             /*!< Количество измерений для усреднения результата 1..255. */
}cmd_ppmu_meas_i__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t ppmu_id;           /*!< Идентификатор PPMU.*/
    double i;                  /*!< Измеренное напряжения, В. Если count больше «1», то i – среднеарифметическое выполненных измерений */
    double i_rms;              /*!< СКО измерений. Если count задан «1», то возвращает i_rms равным «0» */
}cmd_ppmu_meas_i__out_t;
/**@}*/







#pragma pack(pop)

#endif
