#ifndef EMULCOMMANDS_H_
#define EMULCOMMANDS_H_

#include "Types/System/cmd_emul_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_EMUL_INIT EMUL_INIT
 * \ingroup CMD_Commands EmulCommands
 * \brief Команда инициализации группы "Настройка и управление эмуляторами ГТП".
 *        Приводит модуль в начальное состояние (состояние после включения питания).
 */
/**@{*/
#define CMD__EMUL_INIT           0x01BD
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_emul_init__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_emul_init__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_TP_SET EMUL_TP_SET
 * \ingroup CMD_Commands EmulCommands
 * \brief Устанавливает режим работы тестовых выводов, связанных со счетчиком ошибок.
 */
/**@{*/
#define CMD__EMUL_TP_SET           0x01C5
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t tp_num;     /*!< Номер тестового вывода для вывода результата ТП. \ref cmd_emul__tp_e */
    uint8_t site_id;    /*!< Номер сайта, к счетчику ошибок которого подключен вывод */
}cmd_emul_tp_set__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_emul_tp_set__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_TP_PG_SET EMUL_TP_PG_SET
 * \ingroup CMD_Commands EmulCommands
 * \brief Устанавливает режим работы тестовых выводов ГТП.
 */
/**@{*/
#define CMD__EMUL_TP_PG_SET        0x01C6
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t tp_num;     /*!< Номер тестового вывода. \ref cmd_emul__pg_tp_e */
    uint8_t tp_mode;    /*!< Режим работы тестовго вывода. \ref cmd_emul__pg_state_e */
}cmd_emul_tp_pg_set__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_emul_tp_pg_set__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_TP_CLR EMUL_TP_CLR
 * \ingroup CMD_Commands EmulCommands
 * \brief Сбрасывает режим работы тестовых выводов, связанных со счетчиком ошибок.
 */
/**@{*/
#define CMD__EMUL_TP_CLR           0x01C7
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_emul_tp_clr__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_emul_tp_clr__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_TP_PG_CLR EMUL_TP_PG_CLR
 * \ingroup CMD_Commands EmulCommands
 * \brief Сбрасывает режим работы тестовых выводов ГТП. Переводит все тестовые выводы в состояние \ref EMUL_PG_STATE_OFF
 */
/**@{*/
#define CMD__EMUL_TP_PG_CLR         0x01C8
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_emul_tp_pg_clr__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_emul_tp_pg_clr__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_PG_CFG EMUL_PG_CFG
 * \ingroup CMD_Commands EmulCommands
 * \brief Конфигурирует эмулятор ГТП
 */
/**@{*/
#define CMD__EMUL_PG_CFG       0x01C9
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;          /*!< Номер ГТП (0..7). */
    uint32_t max_vectors;   /*!< Кол-во векторов тестовой последовательности, которое будет выполнено в ходе ФК.
                                 Значение обязательно должно быть четным, если clk_div = 1. */
    uint32_t tmbs_ready;    /*!< Время (в тактах ОЧФК) появления сигнала tmbs_ready в состоянии PRERUN (имитирует сигнал готовности буферов реального ГТП).
                                 Полное время появления сигнала после входа в состояние PRERUN -  512 + tmbs_ready. */
    uint32_t pause_vector;  /*!< Номер вектора ФК, после исполнения которого, эмулятор ГТП перейдет в состояние PAUSE.
                                 Значение обязательно должно быть четным, если clk_div = 1. */
    uint8_t clk_div;        /*!< Делитель частоты ОЧФК. Корректные значения от 1 до 15 (0- недопустимо). */
    uint8_t pause_en;       /*!< Включено (1) - при следующем выполнении ФК эмулятор ГТП войдет в состояние PAUSE, а затем через 50 тактов ОЧФК в состояние PRERUN, имитируя ветвление d. */
    uint8_t continue_en;    /*!< Разрешения продолжения ФК в состоянии PRERUN после ветвления (состояние PAUSE):
                                 1 – продолжение выполнение ФК разрешено;
                                 0 – продолжение выполнение ФК не разрешено. */
}cmd_emul_pg_emul__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_emul_pg_emul__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_PRBS_CFG EMUL_PRBS_CFG
 * \ingroup CMD_Commands EmulCommands
 * \brief Конфигурирует генератор\чекеры ГТП
 */
/**@{*/
#define CMD__EMUL_PRBS_CFG         0x01CA
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;      /*!< Номер ГТП (0..7). */
    uint8_t gen_chk_id; /*!< Номер генератора\чекера.  \ref cmd_emul__prbs_id_e */
    uint8_t seed;       /*!< Начальная инициализация. */
}cmd_emul_prbs_cfg__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_emul_prbs_cfg__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_PRBS_CFG_GET EMUL_PRBS_CFG_GET
 * \ingroup CMD_Commands EmulCommands
 * \brief Возвращает конфигурирацию генератора\чекера ГТП
 */
/**@{*/
#define CMD__EMUL_PRBS_CFG_GET         0x01CB
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;      /*!< Номер ГТП (0..7). */
    uint8_t gen_chk_id; /*!< Номер генератора\чекера. \ref cmd_emul__prbs_id_e */
}cmd_emul_prbs_cfg_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;      /*!< Номер ГТП (0..7). */
    uint8_t gen_chk_id; /*!< Номер генератора\чекера.  \ref cmd_emul__prbs_id_e */
    uint8_t seed;       /*!< Текущее значение. */
}cmd_emul_prbs_cfg_get__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_PG_CFG_GET EMUL_PG_CFG_GET
 * \ingroup CMD_Commands EmulCommands
 * \brief Конфигурирует генератор\чекеры ГТП
 */
/**@{*/
#define CMD__EMUL_PG_CFG_GET         0x01CC
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_id;                 /*!< Номер ГТП (0..7). */
}cmd_emul_pg_cfg_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_id;                  /*!< Номер ГТП (0..7). */
    uint32_t max_vectors;           /*!< Кол-во векторов тестовой последовательности, которое будет выполнено в ходе ФК.
                                         Значение обязательно должно быть четным, если clk_div = 1. */
    uint32_t tmbs_ready;            /*!< Время (в тактах ОЧФК) появления сигнала tmbs_ready в состоянии PRERUN (имитирует сигнал готовности буферов реального ГТП).
                                         Полное время появления сигнала после входа в состояние PRERUN -  512 + tmbs_ready. */
    uint32_t pause_vector;          /*!< Номер вектора ФК, после исполнения которого, эмулятор ГТП перейдет в состояние PAUSE.
                                         Значение обязательно должно быть четным, если clk_div = 1. */
    uint8_t clk_div;                /*!< Делитель частоты ОЧФК. Корректные значения от 1 до 15 (0- недопустимо). */
    uint8_t pause_en;               /*!< Включено (1) - при следующем выполнении ФК эмулятор ГТП войдет в состояние PAUSE, а затем через 50 тактов ОЧФК в состояние PRERUN, имитируя ветвление d. */
    uint8_t continue_en;            /*!< Разрешения продолжения ФК в состоянии PRERUN после ветвления (состояние PAUSE):
                                         1 – продолжение выполнение ФК разрешено;
                                         0 – продолжение выполнение ФК не разрешено.*/
    uint8_t stop_reason;            /*!< Причина остановки ФК \ref cmd_emul__prbs_stop_src_e */
    cmd_emul__prbs_status_t status; /*!< Текущее состояние ГТП. */
}cmd_emul_pg_cfg_get__out_t;
/**@}*/

/**
 * \defgroup GRP_EMUL_TP_USR_SET EMUL_TP_USR_SET
 * \ingroup CMD_Commands EmulCommands
 * \brief Устанавливает режим работы пользовательских тестовых выводов.
 */
/**@{*/
#define CMD__EMUL_TP_USR_SET           0x01CD
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t tp_num;          /*!< Номер тестового вывода для вывода. \ref cmd_emul__tp_usr_e */
    uint8_t mode;            /*!< Режим работы тестового вывода. \ref cmd_emul__tp_usr_state_e */
    uint32_t channel_num;    /*!< В режиме тестового вывода EMUL_TP_USR_STATE_TRIGGER - в этом параметре передается номер триггерной линии. */
}cmd_emul_tp_usr_set__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_emul_tp_usr_set__out_t;
/**@}*/






#pragma pack(pop)

#endif
