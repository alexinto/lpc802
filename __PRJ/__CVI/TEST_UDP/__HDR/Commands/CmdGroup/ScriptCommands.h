#ifndef SCRIPTCOMMANDS_H_
#define SCRIPTCOMMANDS_H_

#include "Types/DRV/scmd_types.h"
#include "Types/RXI_Ethernet/cmd_tbl_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_SCRIPT_BEGIN SCRIPT_BEGIN
 * \ingroup CMD_Commands ScriptCommands
 * \brief Осуществляет переход в режим записи сриптов. Обрабатываются только команды имеющие статус "скрипт",
 *        остальные будут сохранены в соответствующем скрипте, при этом стандартный ответ команды меняется на
 *         \ref ANSWER_CMD__DONE или \ref ANSWER_CMD__ERROR.
 *        Формат команд аналогичен полям протокола RXI_Ethernet.
 */
/**@{*/
#define CMD__SCRIPT_BEGIN            0x0160
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t script_num;         /*!< Номер скрипта */
}cmd_script_begin__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_script_begin__out_t;
/**@}*/

/**
 * \defgroup GRP_SCRIPT_END SCRIPT_END
 * \ingroup CMD_Commands ScriptCommands
 * \brief Выход из режима записи скриптов. Дальнейшие команды будут выполняться в штатном режиме.
 */
/**@{*/
#define CMD__SCRIPT_END             0x0161
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_script_end__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_script_end__out_t;
/**@}*/

/**
 * \defgroup GRP_SCRIPT_READ SCRIPT_READ
 * \ingroup CMD_Commands ScriptCommands
 * \brief Читает скрипт по указанному номеру. Возвращает полное содержимое скрипта \ref ANSWER_CMD__ANSWER.
 *        В случае, сели скрипт пуст- возвращает нулевую длину.
 */
/**@{*/
#define CMD__SCRIPT_READ            0x0162
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t script_num;         /*!< Номер скрипта */
}cmd_script_read__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t script_data[CMD_SIZE_NA]; /*!< Содержимое скрипта */
}cmd_script_read__out_t;
/**@}*/

/**
 * \defgroup GRP_SCRIPT_STATE SCRIPT_STATE
 * \ingroup CMD_Commands ScriptCommands
 * \brief Управление состоянием скрипта.<br>
 * @details - Номер скрита &mdash; 1...32
 *  - Состояние:<br>
 *          1 &mdash; <tt>RUN</tt> &mdash; запустить скрипт;<br>
 *          2 &mdash; <tt>PAUSe</tt> &mdash; приостановить скрипт;<br>
 *          3 &mdash; <tt>CONTinue</tt> &mdash; продолжить выполнение скрипта;<br>
 *          4 &mdash; <tt>STOP</tt> &mdash; остановить запущенный скрипт;<br>
 *          Должен соблюдаться граф перехода команд-состояний:<br>
 *          <tt>(PAUSe | STOP)&rarr;RUN</tt><br>
 *          <tt>(PAUSe | CONTinue)&rarr;(STOP | PAUSe)</tt><br>
 *          <b><u>Команда с параметром <tt>RUN</tt> &mdash; синхрокоманда</u></b>
 */
/**@{*/
#define CMD__SCRIPT_STATE           0x0163
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t script_num;         /*!< Номер скрипта */
    uint8_t script_state;       /*!< Состояние скрипта */
}cmd_script_state__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct{
}cmd_script_state__out_t;
/**@}*/

/**
 * \defgroup GRP_SCRIPT_STATE_REQ SCRIPT_STATE_REQ
 * \ingroup CMD_Commands ScriptCommands
 * \brief Чтение состояния скрипта.
 */
/**@{*/
#define CMD__SCRIPT_STATE_REQ       0x0164
/**
 * Входные параметры команды.
 */
typedef struct {
        uint32_t script_num;        /*!< Номер скрипта */
}cmd_script_state_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t script_state;       /*!< Состояние скрипта */
}cmd_script_state_req__out_t;
/**@}*/

/**
 * \defgroup GRP_SCMD SCMD_GENERATE
 * \ingroup CMD_Commands ScriptCommands
 * \brief Настройка отправки синхрокоманды на IP-адрес
 * @details Настраивает (привязывает) битовое поле регистра управляемого  ГТП с настройками адресата синхропакета.
 * Т.е. считаем, что ИМ может формировать до 32-х различных синхропакетов.
 * Формирование синхропакета происходит по записи 1 в соотв. бит регистра.
 * По окончании формирования синхропакета соотв. бит регистра автоматически сбрасывается.
 */
/**@{*/
#define CMD__SCMD              0x0165
/**
 * Входные параметры команды.
 */
typedef struct{
    cmd_scmd_generate__in_out_t data;
}cmd_scmd_generate__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct{
    cmd_scmd_generate__in_out_t data;
}cmd_scmd_generate__out_t;
/**@}*/

/**
 * \defgroup GRP_SCMD_REQ SCMD_REQ
 * \ingroup CMD_Commands ScriptCommands
 * \brief Запрос результата приема синхрокоманды.
 */
/**@{*/
#define CMD__SCMD_REQ          0x0166
/**
 * Входные параметры команды.
 */
typedef struct{
    uint8_t     scmd_number;        /*!< Условный номер синхрокоманды */
}cmd_scmd_generate_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct{
    uint8_t     scmd_number;        /*!< Условный номер синхрокоманды */
    uint32_t    scmd_ip_address;    /*!< IP адрес получателя синхрокоманд */
    uint16_t    scmd_port;          /*!< Порт получателя синхрокоманды */
    uint32_t     scmd_script;       /*!< Номер скрипта получателя синхрокоманды */
}cmd_scmd_generate_req__out_t;
/**@}*/
















#pragma pack(pop)

#endif
