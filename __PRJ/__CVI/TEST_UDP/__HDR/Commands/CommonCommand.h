#ifndef COMMONCOMMAND_H_
#define COMMONCOMMAND_H_

#include <stdint.h>
#include "CmdGroup/SystemCommands.h"
#include "CmdGroup/HwCommands.h"
#include "CmdGroup/JournalCommands.h"
#include "CmdGroup/ScriptCommands.h"
#include "CmdGroup/TriggerCommands.h"
#include "CmdGroup/SyncClockCommands.h"
#include "CmdGroup/ExtIfCommands.h"
#include "CmdGroup/AddClockCommands.h"
#include "CmdGroup/PSPCommands.h"
#include "CmdGroup/MultisiteCommands.h"
#include "CmdGroup/EmulCommands.h"
#include "CmdGroup/FirmwareCommands.h"
#include "CmdGroup/TestCommands.h"
#include "CmdGroup/PGCommands.h"
#include "CmdGroup/PinCommands.h"
#include "CmdGroup/DCLCommands.h"
#include "CmdGroup/PPMUCommands.h"
#include "CmdGroup/PWRCommands.h"
#include "CmdGroup/DiagCommands.h"

#include "CmdGroup/RoboCal.h"

#include "CmdGroup/Ms20gtModule.h"
#include "CmdGroup/SyncmmModule.h"
#include "CmdGroup/Pin400Module.h"
#include "CmdGroup/DPSModuleCommands.h"
#include "CmdGroup/AWG2M.h"

#pragma pack(push, 1)


/**
 * \defgroup ModulesGroup ������
 */

/**
 * \defgroup GRP_COMMANDS ������ ������
 */

/**
 * \defgroup IO_Commands ������� ������������� ����������
 * \ingroup GRP_COMMANDS
 */

/**
 * \defgroup CMD_Commands ������� ���������� ����������
 * \ingroup GRP_COMMANDS
 */

/**
 * \defgroup HwCommands ������� ��������������� �������
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule Ms20gtModule PspModule Pin400Module
 */

/**
 * \defgroup SystemCommands ��������� ���������
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule Ms20gtModule PspModule Pin400Module
 */

/**
 * \defgroup JournalCommands ������-������ �������
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule PspModule Pin400Module
 */

/**
 * \defgroup ScriptCommands ��������/��������/���������� ��������
 * \ingroup GRP_COMMANDS SyncmmModule
 */

/**
 * \defgroup TriggerCommands ��������� � ���������� ����������� ���������
 * \ingroup GRP_COMMANDS SyncmmModule
 */

/**
 * \defgroup SyncClockCommands ��������� SYNC_CLOCK
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule Pin400Module
 */

/**
 * \defgroup ExtIfCommands ������ ������ ���������� ExtIf
 * \ingroup GRP_COMMANDS Sync1200Module
 * \brief ��������� ��������� ������������ I2C, SPI, �������� ������ ����������,
 *  � ����� ��������������� ����������� ������� �������� Ext If.
 */

/**
 * \defgroup AddClockCommands ������ ������ ���������� AddClock
 * \ingroup GRP_COMMANDS Sync1200Module
 */

/**
 * \defgroup PSPCommands ������ ������ ���������� ����������� �������
 * \ingroup GRP_COMMANDS PspModule
 */

/**
 * \defgroup MultisiteCommands ��������� � ���������� �����������
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule Pin400Module
 */

/**
 * \defgroup EmulCommands ��������� � ���������� ����������� ���
 * \ingroup GRP_COMMANDS SyncmmModule
 */

/**
 * \defgroup PGCommands ��������� � ���������� ���
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup PinCommands ��������� ������� ������� (PIN-��)
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup FirmwareCommands ������ ������ ������ � ����������
 * \ingroup GRP_COMMANDS SyncmmModule Ms20gtModule Sync1200Module PspModule Pin400Module
 */

/**
 * \defgroup TestCommands �������� �������.
 * \ingroup GRP_COMMANDS SyncmmModule
 * \brief ����������� ������ � �������� ������ \ref CMD__OP_MODE_SET
 */

/**
 * \defgroup DCLCommands ������ ������ ���������� ������������ ���������� ��������� ����������� �DCL Control�
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup PPMUCommands ������ ������ ���������� PPMU
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup PWRCommands ������ ������ ���������� �������� PWR
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup DiagCommands ������ ������ �����������
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup RoboCommands ������ ������ ���������� �������
 * \ingroup GRP_COMMANDS
 */



/**
 * \defgroup MeasurementModule ������������� ������
 * \ingroup ModulesGroup
 */

/**
 * \defgroup SystemModule ��������� ������
 * \ingroup ModulesGroup
 */

/**
 * \defgroup SyncmmModule SyncMM
 * \ingroup MeasurementModule
 */

/**
 * \defgroup Pin400Module Pin400M
 * \ingroup MeasurementModule
 */

/**
 * \defgroup Ms20gtModule MS20GT
 * \ingroup SystemModule
 */

/**
 * \defgroup Sync1200Module Sync1200
 * \ingroup SystemModule
 */

/**
 * \defgroup PspModule ���������� �������
 * \ingroup SystemModule
 */

/**
 * \defgroup DPSModule DPS
 * \ingroup MeasurementModule
 */

/**
 * \defgroup AWGModule AWG2M
 * \ingroup MeasurementModule
 */
















#pragma pack(pop)

#endif
