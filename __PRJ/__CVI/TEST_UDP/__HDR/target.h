
#ifndef _TARGET_H
#define _TARGET_H
#include <stdint.h>
#include <stddef.h>
#include <utility.h>

#define SUPERVISOR__IDLE_OFF

static int critical_section_lock;

#define CRITICAL_SECTION_ON {    if(critical_section_lock == 0)     \
        							CmtNewLock (NULL, 0, &critical_section_lock);  \
    						     CmtGetLock (critical_section_lock);

#define CRITICAL_SECTION_OFF 	 CmtReleaseLock (critical_section_lock);}

#define CVI__BREAK_ERR_OFF { int err_off_old = SetBreakOnLibraryErrors(0);
#define CVI__BREAK_ERR_ON  SetBreakOnLibraryErrors(err_off_old);}

#define _M_LN2        0.693147180559945309417
#define log2(x) (log(x)/_M_LN2)

#define IO_PROCESSOR 1
#define CMD_PROCESSOR 17


extern long main__round(double x);
#define round(x) main__round(x)


// ������������ SPI AddClock � ExtIf
#define ADD_CLK__SPI_ID 0
#define EXT_IF__SPI_ID  1


typedef enum {
	STATE_IDLE         = 0,
	STATE_NORMAL =		 1,
	STATE_CALIBRATION =	 2,
	STATE_ONE_TRANSMIT = 3,
	STATE_FW_UPD = 		 4,
	STATE_FREQ_CYCLE =   5,
	STATE_OPER_REG   =   6,
	STATE_ONE_CMD    =   7,
	STATE_FW_UPD_SUB =   8,
}tester__state_e;


typedef struct {
   uint32_t* write;
   uint32_t* read;
   uint32_t* ctrl;
   uint32_t* status;
}ext_spi_t;

ext_spi_t* triple_spi__add_clock_get_tbl();
void triple_spi__set_default();
void debug_itf(char* msg);

#endif

