/***************************************************************************//**
 * @file cmd_oscillator.h.
 * @brief Модуль, реализующий управление генераторами частот
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_OSCILLATOR_H_
#define CMD_OSCILLATOR_H_

/**
 * @defgroup GRP_CMD_OSCILLATOR Модуль, реализующий управление генераторами частот
 * @brief 
 * @{
 */

#include <stdio.h>
#include "System/events.h"
#include "Types/System/cmd_oscillator_types.h"

typedef events__e (*oscillator__transport_t)(int spi_id, int oper, uint16_t addr, uint8_t* data, int size);

/***************************************************************************//**
 * @brief Функция иницализации модуля. Устанавливает параметры всех генераторов
 *        по- умолчанию. Частота 100Мгц, точность определяется каналом OSCIL_SETUP__FDOM1.
 * @param oscil_hw_func - пользовательская функция транспортного уровня.
 * @return EVENT__OK - инициализация прошла успешно.
 ******************************************************************************/
events__e cmd_oscillator__init(oscillator__transport_t oscil_hw_func);

/***************************************************************************//**
 * @brief Функция- возвращает указатель на таблицу калибровки.
 * @param oscil_id - идентификатор SyncClock-а.
 * @return param__t* - указатель на таблицу калибровки.
 ******************************************************************************/
cmd_oscillator__pcal_t* cmd_oscillator__get_tbl(uint8_t oscil_id);

/***************************************************************************//**
 * @brief Функция настройки генератора.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param mode -  выбор опорной частоты.
 * @param setup - режим работы генераторов (выбор выходной частоты). \ref cmd_oscillator__setup_e
 * @return EVENT__OK - настройки записаны успешно.
 ******************************************************************************/
events__e cmd_oscillator__set(uint8_t oscil_id, uint8_t mode, cmd_oscillator__setup_e setup);

/***************************************************************************//**
 * @brief Функция возвращает настройки генератора.
 * @param oscil_id - идентификатор SyncClock-а.
 * @return cmd_oscillator__t - текущие настройки.
 ******************************************************************************/
cmd_oscillator__t cmd_oscillator__get(uint8_t oscil_id);

/***************************************************************************//**
 * @brief Функция настройки канала генератора.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @param mode - режим работы канала.
 * @param freq - требуемая частота.
 * @return EVENT__OK - частота установлена.
 *         EVENT__ERROR - ошибка установки частоты.
 ******************************************************************************/
events__e cmd_oscillator__set_ch(uint8_t oscil_id, cmd_oscillator__ch_id_e ch, cmd_oscillator__ch_mode_e mode, uint32_t freq);

/***************************************************************************//**
 * @brief Функция возвращает настройки канала генератора.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @return cmd_oscillator__ch_t - текущие настройки канала.
 ******************************************************************************/
cmd_oscillator__ch_t* cmd_oscillator__get_ch(uint8_t oscil_id, cmd_oscillator__ch_id_e ch);

/***************************************************************************//**
 * @brief Функция выравнивания фаз генераторов.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param mode - "ступень" синхронизации (1 или 2).
 * @return EVENT__OK - синхронизация прошла успешно.
 ******************************************************************************/
events__e cmd_oscillator__align(uint8_t oscil_id, uint8_t mode);

/***************************************************************************//**
 * @brief Функция формирования синхроимпульса.
 * @param mode - "ступень" синхронизации (1 или 2).
 * @return EVENT__OK - синхронизация прошла успешно.
 ******************************************************************************/
events__e cmd_oscillator__pulse(uint8_t mode);

/***************************************************************************//**
 * @brief Функция возвращает, считанные из микросхемы параметры.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @return cmd_oscillator__pcal_t - считанные параметры.
 ******************************************************************************/
cmd_oscillator__pcal_t  cmd_oscillator__get_pcal(uint8_t oscil_id, cmd_oscillator__ch_id_e ch);

/***************************************************************************//**
 * @brief Функция установки режима работы с калибровочными параметрами.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param mode - 0- нормальный режим работы, 1- отключение калибровочных параметров.
 * @return void.
 ******************************************************************************/
void cmd_oscillator__pcal_mode(uint8_t oscil_id, uint8_t mode);

/***************************************************************************//**
 * @brief Функция установки задержек канала.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @param d_delay - цифровая задержка.
 * @param a_delay - аналоговая задержка.
 * @return void.
 ******************************************************************************/
void cmd_oscillator__ch_delay(uint8_t oscil_id, cmd_oscillator__ch_id_e ch, uint8_t d_delay, uint8_t a_delay);

/***************************************************************************//**
 * @brief Функция возвращает делитель канала.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @return uint16_t делитель канала.
 ******************************************************************************/
uint16_t oscil_ch_div_get(uint8_t oscil_id, uint8_t ch);

/**
 * @}
 */

#endif
