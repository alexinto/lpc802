/***************************************************************************//**
 * @file add_clock.h.
 * @brief ������, � ��������� ������� ���������� ��������� Add Clock.
 *        ������ ��������� ��������� �������� ��������� ������ - add_clock__struct_t!
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef ADD_CLOCK_H_
#define ADD_CLOCK_H_

#include <stdint.h>
#include "System/events.h"
#include "Types/DRV/add_clock_types.h"

/*******************************************************************************
 * ��������� ��������� �������� Add Clk.
 ******************************************************************************/

typedef struct {
    uint32_t const_a5        :8;
    uint32_t id              :8;
    uint32_t version_low     :8;
    uint32_t version_high    :8;
}add_clock__info_t;

typedef struct {
    struct {
        uint32_t start_rw      :1;     // 0 � ������. 1 � ������
        uint32_t reserved      :8;
        uint32_t addr          :7;     // ����� �������� ��� ���������� ���������� (������ ��� ������).
        uint32_t data_w        :8;     // ���� ��� ������ ������ � �������� ���������� AD9139.
    }ctrl;
    struct {
        uint32_t busy          :1;     // 0 � ������. 1 � ������
        uint32_t irq1          :1;     // ���������� 1. 1 � ������� ������� ����������
        uint32_t irq2          :1;     // ���������� 2. 1 � ������� ������� ����������
    }status;
    uint32_t data_r;                   // ���� ��� ��������� ������ �� ��������� ���������� AD9139.
    struct {
        uint32_t reserved      :2;
        uint32_t parity        :1;     // ���������� ������ FRAME/PARITY
    }frame_sel;
    struct {
        uint32_t rmax          :1;     // 1 - �������� 80��� ������������
        uint32_t rmid          :1;     // 1 - �������� 40��� ������������
        uint32_t rmin          :1;     // 1 - �������� 20��� ������������
    }current_ctrl;
}add_clock__ad9139_t;

typedef struct {
    uint32_t reserved          :1;
    uint32_t mode              :2;     // 0- ������ �����. 1 � DDS ���������� ��������������� �������. 2 � DDS ���������� ������������ �������. 3 - ���������� ������������ ������� � ������������� ��������
}add_clock__gen_ctrl_t;

typedef struct {
    struct {
        uint32_t max           :16;    // �������� ������������� ������ �������.
        uint32_t min           :16;    // �������� ������������ ������ �������.
    } amplitude;
    uint32_t inverted;                 // 1 � ��������������� ������. 0 � �� ��������������� ������.
}add_clock__rect_gen_ctrl_t;

typedef struct {
    uint32_t freq;                     // ������� ���������� ��������. �� 0 �� 0x1000000
    uint32_t phase;                    // ������� ���������� ������� ����. �� 0 �� 0x1000000
}add_clock__sin_gen_ctrl_t;

typedef struct {
    struct {
        uint32_t rw         :1;        // 1 � ������. 0 � ������.
        uint32_t reserved   :7;
        uint32_t addr       :13;       // ����� �������� HMC7044
        uint32_t reserved1  :3;
        uint32_t data_w     :8;        // ������ ��� ������. ��� �������� ������ ���� ������������
    }ctrl;
    struct {
        uint32_t busy       :1;        // 1 � ���������� ������, � ������ ������ ������������ ���������� ������/������.
        uint32_t reserved   :7;
        uint32_t data_r     :8;        // ��������� ������ �� SPI
    }read_status;
}add_clock__spi_hmc7044_t;

typedef struct {
    struct {
        uint32_t vcc_int    :12;       // ������� ���������� �� ������� VCCINT
        uint32_t reserved   :4;
        uint32_t vocm_diff  :12;       // ������� ���������� �� �������� VOCM ������������� ��������� DA10 (ADA4938-1)
    } val_0;
    struct {
        uint32_t vocm_se    :12;       // ������� ���������� �� �������� VOCM_SE ������������� ��������� DA11 (AD8009ARZ)
        uint32_t reserved   :4;
        uint32_t mdac_dvdd  :12;       // ������� ���������� MDAC_DVDD_1V8
    } val_1;
    struct {
        uint32_t mdac_avdd  :12;       // ������� ���������� MDAC_AVDD_3V3
        uint32_t reserved   :4;
        uint32_t relay_5v   :12;       // ������� ���������� 5V_RELAY
    } val_2;
    struct {
        uint32_t vss_amp    :12;       // ������� ���������� VSS_AMP (-4V)
        uint32_t reserved   :4;
        uint32_t vcc_amp    :12;       // ������� ���������� VCC_AMP(6V)
    } val_3;
    struct {
        uint32_t adac_5v    :12;       // ������� ���������� 5V_ADAC
        uint32_t reserved   :4;
        uint32_t diff       :12;       // DIFF(������� ���������� ������� �� ������ ������������� ��������� D10 ADA4938-1)
    } val_4;
    struct {
        uint32_t diag_se    :12;       // DIAG_SE (������� ���������� ������� �� ������ ������������� ��������� D11 AD8009ARZ)
        uint32_t reserved   :4;
        uint32_t vcc_aux    :12;       // ������� ���������� �� ������� VCCAUX
    } val_5;
    struct {
        uint32_t vrefp      :12;       // ������� ���������� �� ������� VREFP
        uint32_t reserved   :4;
        uint32_t vrefn      :12;       // ������� ���������� �� ������� VREFN
    } val_6;
    struct {
        uint32_t bram       :12;       // ������� ���������� �� ������� BRAM
    } val_7;
    struct {
        uint32_t temp       :12;       // �����������
    } val_8;
}add_clock__diag_values_t;


typedef struct {
    add_clock__diag_values_t value;
    add_clock__diag_values_t threshold;
    struct {
        uint32_t temp      : 1;
        uint32_t vcc_int   : 1;
        uint32_t vcc_aux   : 1;
        uint32_t vrefp     : 1;
        uint32_t vrefn     : 1;
        uint32_t vbram     : 1;
        uint32_t mdac_dvdd : 1;
        uint32_t relay_5v  : 1;
        uint32_t vcc_amp   : 1;
        uint32_t diff      : 1;
        uint32_t mdac_avdd : 1;
        uint32_t vss_amp   : 1;
        uint32_t adac_5v   : 1;
        uint32_t diag_se   : 1;
        uint32_t vocm_diff : 1;
        uint32_t vocm_se   : 1;
    }alarm;

}add_clock__diag_t;

typedef struct {
    uint32_t sw1xn2x        :1;                     // ����� ������������� 0 - 200 ��,  1 - 149.9 ��
    uint32_t en_1x2x        :1;                     // ����� �������������  0 - 300 ��,  1 - ������������ ����� sw1xn2x
    uint32_t clk_in_en      :1;                     // 0 - ������ ��������� � ������� X9, 1 -  �������� ������ � HMC7044LP10BE
    uint32_t clk_out_ext    :1;                     // ����� ������� SINC_OUT. 0 - ����� clk_in_en 1- ������� X16
    uint32_t diff_diag      :1;                     // 0 - DIFF_OUT �� ������� DIFF, 1 - � ������� ���� diff_se
    uint32_t diff_se        :1;                     // 0 - DIFF_OUT �� ������ �2 � DIFF_OUT�, 1 - ������ �� ���� �� DA11
    uint32_t se_diag        :1;                     // 0 - SE_OUT �� D11 ��������� �� DIAG_SE, 1 - SE_OUT �� ������ X1�SE_OUT�)
    uint32_t lfp_en         :1;                     // 0 - ������ MDAC_OUT �� D9(����� ���), 1 - � ������� ���� lfp200
    uint32_t lfp200         :1;                     // 0 - MDAC_OUT ����� ��� 50���, 1 - ����� ��� 200���
}add_clock__relay_t;

typedef struct {
    uint32_t sw1_fs_ext        :1;                  //0 - CLK_MUX(����)<-CLK_EXT_2, 1 - CLK_MUX<-SCLKOUT3(HMC7044LP10BE)
    uint32_t sw_fs_ext         :1;                  //0 - DACCLK(AD9139)<-SCLKOUT7(HMC7044LP10BE), 1 - DACCLK (AD9139)<-CLK_EXT_0
}add_clock__mux_clk_t;

typedef struct {
    struct {
        uint32_t mode              :2;              // 0 - ���������� �� SPI, 1 - �������� ����������, 2 - �� ������� SYNC_CLOCK(CLK_MUX).
        uint32_t spi               :2;              // 2 - ����, 1 - �����
        uint32_t trigger           :3;              // 0 - ����� � ����� �������� 1, ���� � ���� �������� 1
        uint32_t tx_en_pin         :1;              // ���������� ������� TXEN (AD9139)
    }ctrl;
    uint32_t state_trigg           :2;              // ��������� ���������� ������� ���������
}add_clock__dac_cfg__t;

typedef struct {
    struct {
        uint32_t buff_sel          :1;              // 0 � ������������ ����� A, 1 � ������������ ����� B
        uint32_t ldb_lda           :2;              // 0 � �� ���������, 1 � �������� � ��� �, 2 � �������� � ��� �, 3 � �������� � ��� � � �
        uint32_t op_mode           :2;              // 0 � � ������ �� ���������� �������������� ��������, 1 - ����� ��������� � ��������� 1��� � ��������� � GND, 2 - ����� ��������� � ��������� 100��� � ��������� � GND, 3 - ����������������� ���������
        uint32_t reserved          :3;
        uint32_t data              :16;             // ������ ��� ������ � ���
    }ctrl;
    struct {
        uint32_t busy              :1;              // 0 ����������� �� SPI ���������, 1 � ���������� ������
    }status;
}add_clock__dac8552_t;

typedef struct {
    struct {
        uint32_t start             :1;              // 1 � ����� ����������
        uint32_t rw                :1;              // 0 � ������, 1 � ������
        uint32_t reserved          :6;              //
        uint32_t addr              :16;             //
    }ctrl;
    struct {
        uint32_t busy              :1;              // 0 ����������� ���������, 1 � ���������� ������
    }status;
    uint32_t data_rd;                               // ��������� ������- 1 ����
    uint32_t data_wr;                               // ������ ��� ������- 1 ����
}add_clock__m95640_t;

typedef struct {
    uint32_t global                :1;              //
    uint32_t dac8552               :1;              //
    uint32_t dac_sin               :1;              //
    uint32_t meander               :1;              //
    uint32_t ad9139_ctrl           :1;              //
    uint32_t spi_slave             :1;              //
    uint32_t area_reg              :1;              //
    uint32_t ad9139_dac            :1;              //
    uint32_t hmc7044               :1;              //
    uint32_t diag                  :1;              //
    uint32_t m95640                :1;              //
}add_clock__reset_t;


typedef struct {
    add_clock__info_t info;                         // 0x0000
    uint32_t test;                                  // 0x0004
    uint32_t reserved[62];                          // 0x0008
    add_clock__ad9139_t ad9139;                     // 0x0100
    uint32_t reserved1[59];                         // 0x0114
    add_clock__gen_ctrl_t gen_ctrl;                 // 0x0200
    add_clock__rect_gen_ctrl_t rect_gen_ctrl;       // 0x0204
    add_clock__sin_gen_ctrl_t sin_gen_ctrl;         // 0x020C
    uint32_t reserved2[59];                         // 0x0214
    add_clock__dac8552_t dac8552;                   // 0x0300
    uint32_t reserved3[190];                        // 0x0308
    add_clock__reset_t reset;                       // 0x0600
    uint32_t reserved4[63];                         // 0x0604
    add_clock__dac_cfg__t ad9139_cfg;               // 0x0700
    uint32_t reserved5[62];                         // 0x0708
    add_clock__spi_hmc7044_t spi_hmc7044;           // 0x0800
    uint32_t reserved6[62];                         // 0x0808
    add_clock__relay_t relay;                       // 0x0900
    uint32_t reserved7[63];                         // 0x0904
    add_clock__mux_clk_t mux_ctrl;                  // 0x0A00
    uint32_t reserved8[63];                         // 0x0A04
    add_clock__diag_t diag;                         // 0x0B00
    uint32_t reserved9[109];                        // 0x0B4C
    add_clock__m95640_t m95640;                     // 0x0D00
}add_clock__struct_t;


#define ADD_CLK_REG (uint32_t)&((add_clock__struct_t*)0x00)

/***************************************************************************//**
 * @brief ������� ������ � SPI. ������ ���� ����������� �������������.
 * @param spi_id - ����� ����������.
 * @param oper - ��� �������� (������, ������).
 * @param addr - ����� �������� �������� Add Clock �� ���� SPI (��������, HMC7044).
 * @param data - ��������� �� ���������������� ������. ����� ���� NULL, � ���� ������
 *               ������� ������ ��������� ���������� (����� ��� ��������).
 * @param size - ������ ������ � ������.
 * @return EVENT__OK - �������� ������ �������.
 *         EVENT__BUSY - ��������� �����.
 ******************************************************************************/
typedef events__e (*add_clock__transport_t)(int spi_id, int oper, uint32_t addr, uint8_t* data, int size);

/***************************************************************************//**
 * @brief ������� ������������� �������� Add Clock.
 * @param void.
 * @return EVENT__OK - ����� ����������� ������.
 *         EVENT__ERROR - ��� ������������� ������ ��������� ������ ������������.
 ******************************************************************************/
events__e add_clock__init(add_clock__transport_t hw_spi_func);

/***************************************************************************//**
 * @brief ������� ������ �������� �������� Add Clock.
 * @param sett_id - ������������� ���������.
 * @param data    - �������� ���������.
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__sett_set(add_clock__settings_e sett_id, uint32_t data);

/***************************************************************************//**
 * @brief ������� ������ �������� �������� Add Clock.
 * @param sett_id - ������������� ���������.
 * @return uin32_t* - ��������� �� ������ �� ��������� ���������.
 *         NULL     - ��������� �� ����������.
 ******************************************************************************/
uint32_t* add_clock__sett_get(add_clock__settings_e sett_id);

/***************************************************************************//**
 * @brief ������� ��������� ���������� ��������� ��������������� ������� �������� AddClock.
 * @param freq - ������� �������, �� (8000..200000000)
 * @param phase - ��������� ���� �������, ������� (0..360)
 * @param voltage - ��������� �������, �� (125..2000)
 * @param v_offset - ���������� ������������ �������, �� (0..4000)
 * @param out_mode - ����� �������
 * @param ctrl_mode - ������ ������� � ��������� ����������
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__sin_set(uint32_t freq, uint32_t phase, uint32_t voltage, uint32_t v_offset, add_clock__out_mode_e out_mode, add_clock__start_stop_mode_e ctrl_mode);

/***************************************************************************//**
 * @brief ������� ������ ���������� ��������� ��������������� ������� �������� AddClock.
 * @param freq - ������� �������, �� (8000..200000000)
 * @param phase - ��������� ���� �������, ������� (0..360)
 * @param voltage - ��������� �������, �� (125..2000)
 * @param v_offset - ���������� ������������ �������, �� (0..4000)
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__sin_get(uint32_t* freq, uint32_t* phase, uint32_t* voltage, uint32_t* v_offset);

/***************************************************************************//**
 * @brief ������� ������ ���������� ��������� ��������������� ������� �������� AddClock.
 * @param gen_id - ������������� ����������
 * @param start_trig_id - ����� �������� ��� ������(0..255)
 * @param start_event - ������� ������
 * @param stop_trig_id - ����� �������� ��� ���������(0..255)
 * @param stop_event - ������� ���������
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__trig_cfg(add_clock__gen_id_e gen_id, uint8_t start_trig_id, add_clock__trig_mode_e start_event, uint8_t stop_trig_id, add_clock__trig_mode_e stop_event);

/***************************************************************************//**
 * @brief ������� ��������� ���������� ��������� �������������� ������� �������� AddClock.
 * @param freq - ������� �������, �� (50000..200000000)
 * @param phase - ��������� ���� �������, ������� (0..360)
 * @param voltage - ��������� �������, �� (0..2000)
 * @param v_offset - ���������� ������������ �������, �� (0..4000)
 * @param clk_mode - ����� ������������ \ref add_clock__clk_mode_e
 * @param ctrl_mode - ������ ������� � ��������� ����������
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__rect_set(uint32_t freq, uint32_t phase, uint32_t voltage, uint32_t v_offset, add_clock__clk_mode_e clk_mode, add_clock__start_stop_mode_e ctrl_mode);

/***************************************************************************//**
 * @brief ������� ������ ���������� ��������� �������������� ������� �������� AddClock.
 * @param freq - ������� �������, �� (50000..200000000)
 * @param phase - ��������� ���� �������, ������� (0..360)
 * @param voltage - ��������� �������, �� (0..2000)
 * @param v_offset - ���������� ������������ �������, �� (0..4000)
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__rect_get(uint32_t* freq, uint32_t* phase, uint32_t* voltage, uint32_t* v_offset);

/***************************************************************************//**
 * @brief ������� ��������� ���������� ��������� ������� ������������� �������� AddClock.
 * @param freq - ������� �������, �� (50000..200000000)
 * @param phase - ��������� ���� �������, ������� (0..360)
 * @param clk_mode - ����� ������������ \ref add_clock__clk_mode_e
 * @param ctrl_mode - ������ ������� � ��������� ����������
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__sync_set(uint32_t freq, uint32_t phase, add_clock__clk_mode_e clk_mode, add_clock__start_stop_mode_e ctrl_mode);

/***************************************************************************//**
 * @brief ������� ������ ���������� ��������� ������� ������������� �������� AddClock.
 * @param freq - ������� �������, �� (50000..200000000)
 * @param phase - ��������� ���� �������, ������� (0..360)
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__sync_get(uint32_t* freq, uint32_t* phase);

/***************************************************************************//**
 * @brief ������� ���������� ������������ �������� �������� �������� AddClock.
 * @param gen_id - ������������� ����������
 * @param gen_cmd - ������� ����������
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__gen_ctrl(add_clock__gen_id_e gen_id, add_clock__gen_cmd_e gen_cmd);

/***************************************************************************//**
 * @brief ������� ������������ �������� AddClock.
 * @param void
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__test();

/***************************************************************************//**
 * @brief ������� ������ � ����������� SPI ���������� AD9139 �������� AddClock.
 * @param oper -  0 = ������ �������� �� ������, 1 = ������ ��������
 * @param addr - ����� �������� AD9139
 * @param data - ��������� �� ����� � �������
 * @param len  - ����� ������
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__ad9139_spi(int oper, uint16_t addr, uint8_t* data, int len);

/***************************************************************************//**
 * @brief ������� ������ � ����������� SPI ���������� HMC7044 �������� AddClock.
 * @param oper -  0 = ������ �������� �� ������, 1 = ������ ��������
 * @param addr - ����� �������� HMC7044
 * @param data - ��������� �� ����� � �������
 * @param len  - ����� ������
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__hmc7044_spi(int oper, uint16_t addr, uint8_t* data, int len);

/***************************************************************************//**
 * @brief ������� ������ � ����������� ���������� dac8552 �������� AddClock. ������ ������.
 * @param data - ��������� �� ����� � �������
 * @param len  - ������ ������ � ������(uint16_t)
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__dac8552_wr(uint16_t* data, int len);

/***************************************************************************//**
 * @brief ������� ������ � ����������� SPI ���������� m95640 �������� AddClock.
 * @param oper -  0 = ������ �������� �� ������, 1 = ������ ��������
 * @param addr - �����
 * @param data - ��������� �� ����� � �������
 * @param len  - ����� ������
 * @return EVENT__OK - �������� ����������� �������.
 *         EVENT__ERROR - ��������� ������.
 ******************************************************************************/
events__e add_clock__m95640(int oper, uint16_t addr, uint8_t* data, int len);

uint32_t add_clock__get_version();

#endif
