/***************************************************************************//**
 * @file freq.h.
 * @brief ������ ������� ������� ��� SyncClock-�.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef FREQ_H_
#define FREQ_H_

typedef struct {
    double freqreal;                                 // FreqReal - ���������� ������� ����� �����������
    double freqreal2;                                 // FreqReal - ���������� ������� ����� �����������
    int n[3];                                        // N[0]     - ��������� ������� ��� = 32
    int d[3];                                        // D[0]     - �������� �������  ���
    uint32_t freq_gun[2];                            // [0] - ������� 1�� ���, [1] - ������� 2-�� ���
    uint16_t sysref[2];
}freq_cal_t;

uint32_t find_freq(uint32_t frequency, int max_cnt);




#endif
