<distribution version="15.0.0" name="Test_udp" type="MSI">
	<prebuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></prebuild>
	<postbuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></postbuild>
	<msi GUID="{5CC2DEF5-F269-4DDB-84A9-34ED52FBF735}">
		<general appName="Test_udp" outputLocation="w:\M_PRJ\__PRJ\__CVI\TEST_UDP\cvidistkit.Test_udp" relOutputLocation="cvidistkit.Test_udp" outputLocationWithVars="w:\M_PRJ\__PRJ\__CVI\TEST_UDP\cvidistkit.%name" relOutputLocationWithVars="cvidistkit.%name" upgradeBehavior="2" autoIncrement="true" version="1.0.79">
			<arp company="Form" companyURL="" supportURL="alexinto@yandex.ru" contact="" phone="" comments=""/>
			<summary title="Form" subject="" keyWords="" comments="" author="Alexinto"/></general>
		<userinterface language="English" showPaths="false" showRuntimeOnly="false" readMe="" license="">
			<dlgstrings welcomeTitle="Test_udp" welcomeText=""/></userinterface>
		<dirs appDirID="101">
			<installDir name="[Program Files]" dirID="2" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="[Desktop]" dirID="0" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="Test_udp" dirID="100" parentID="7" isMSIDir="false" visible="true" unlock="false"/>
			<installDir name="[Start&gt;&gt;Programs]" dirID="7" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="Test_udp" dirID="101" parentID="2" isMSIDir="false" visible="true" unlock="false"/></dirs>
		<files>
			<simpleFile fileID="0" sourcePath="w:\M_PRJ\__PRJ\__CVI\TEST_UDP\cvibuild.test_udp\Release\test_udp.exe" targetDir="101" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="1" sourcePath="C:\WINDOWS\SYSTEM32\USER32.dll" targetDir="101" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="2" sourcePath="C:\WINDOWS\SYSTEM32\ADVAPI32.dll" targetDir="101" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="3" sourcePath="w:\M_PRJ\__PRJ\__CVI\TEST_UDP\test_udp.uir" relSourcePath="test_udp.uir" relSourceBase="0" targetDir="101" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="4" sourcePath="C:\WINDOWS\SYSTEM32\KERNEL32.dll" targetDir="101" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/></files>
		<fileGroups>
			<projectOutput targetType="0" dirID="101" projectID="0">
				<fileID>0</fileID></projectOutput>
			<projectDependencies dirID="101" projectID="0">
				<fileID>4</fileID>
				<fileID>2</fileID>
				<fileID>1</fileID></projectDependencies></fileGroups>
		<shortcuts>
			<shortcut name="Test_udp" targetFileID="0" destDirID="100" cmdLineArgs="" description="" runStyle="NORMAL"/>
			<shortcut name="Tester UDP" targetFileID="0" destDirID="0" cmdLineArgs="" description="" runStyle="NORMAL"/></shortcuts>
		<mergemodules/>
		<products>
			<product name="NI LabWindows/CVI Shared Runtime 2015" UC="{80D3D303-75B9-4607-9312-E5FC68E5BFD2}" productID="{C123DBC1-8C63-4369-A052-EE7D3922EAC9}" path="C:\ProgramData\National Instruments\MDF\ProductCache\" flavorID="_full_" flavorName="Full" verRestr="false" coreVer="15.0.408">
				<cutSoftDeps>
					<softDep name=".NET Support" UC="{0DDB211A-941B-4125-9518-E81E10409F2E}" depKey="CVI_DotNet.CVI1500.RTE"/>
					<softDep name="ActiveX Container Support" UC="{1038A887-23E1-4289-B0BD-0C4B83C6BA21}" depKey="ActiveX_Container.CVI1500.RTE"/>
					<softDep name="Analysis Support" UC="{86208B51-159E-4F6F-9C62-0D5EFC9324D8}" depKey="CVI_Analysis.CVI1500.RTE"/>
					<softDep name="Dev (Patch 1) Support" UC="{C6F76062-CDCB-4931-8580-012AF2152602}" depKey="CVIRTEUpdated.CVI1500.RTE"/>
					<softDep name="Low-Level Driver Support" UC="{20931852-6AA9-4918-926B-130D07A4CF8B}" depKey="CVI_LowLevelDriver.CVI1500.RTE"/>
					<softDep name="Mesa Graphics Support" UC="{5D6378B2-B24D-4AD6-5DA2-FAEFDB81A250}" depKey="Mesa.CVI1500.RTE"/>
					<softDep name="Network Streams Support" UC="{40A5AD7F-4BAF-4A5C-8B56-426B84F75C05}" depKey="CVI_NetworkStreams.CVI1500.RTE"/>
					<softDep name="Network Variable Support" UC="{15CE39FE-1354-484D-B8CA-459077449FB3}" depKey="CVI_NetworkVariable.CVI1500.RTE"/>
					<softDep name="Real-Time Utility Support" UC="{DA6BBF6F-0910-4860-AD46-F14DADF645E9}" depKey="Remote_Configuration.CVI1500.RTE"/>
					<softDep name="TDMS Support" UC="{5A8AF88D-486D-4E30-A7A5-8D8A039BBEBF}" depKey="CVI_Tdms.CVI1500.RTE"/></cutSoftDeps></product>
			<product name="NI LabWindows/CVI Side-By-Side Runtime 2015" UC="{D2182B72-BC1A-4543-9369-17CE072E0516}" productID="{0566DA07-292F-4A97-88A4-24F98A768791}" path="C:\ProgramData\National Instruments\MDF\ProductCache\" flavorID="_full_" flavorName="Full" verRestr="false" coreVer="15.0.408"/></products>
		<runtimeEngine installToAppDir="false" activeXsup="false" analysis="false" cvirte="true" dotnetsup="false" instrsup="true" lowlevelsup="false" lvrt="true" netvarsup="false" rtutilsup="false">
			<hasSoftDeps/></runtimeEngine><sxsRuntimeEngine>
			<selected>false</selected>
			<doNotAutoSelect>false</doNotAutoSelect></sxsRuntimeEngine>
		<advanced mediaSize="650">
			<launchConditions>
				<condition>MINOS_WINXP</condition>
			</launchConditions>
			<includeConfigProducts>true</includeConfigProducts>
			<maxImportVisible>silent</maxImportVisible>
			<maxImportMode>merge</maxImportMode>
			<custMsgFlag>false</custMsgFlag>
			<custMsgPath>msgrte.txt</custMsgPath>
			<signExe>true</signExe>
			<certificate>alexinto</certificate>
			<signTimeURL>http://timestamp.sectigo.com</signTimeURL>
			<signDescURL>Global</signDescURL></advanced>
		<baselineProducts>
			<product name="NI LabWindows/CVI Side-By-Side Runtime 2015" UC="{D2182B72-BC1A-4543-9369-17CE072E0516}" productID="{0566DA07-292F-4A97-88A4-24F98A768791}" path="(None)" flavorID="_full_" flavorName="Full" verRestr="false" coreVer="15.0.408">
				<dependencies>
					<productID>{054A4C14-7D56-4383-949B-F9B91E094F4B}</productID>
					<productID>{08505CC2-EA7F-4818-9C45-B74EDA7227F8}</productID>
					<productID>{1268324B-6C67-48D3-B58A-845E81AE6AFE}</productID>
					<productID>{195D8F29-2704-44BD-A978-57A0382327E5}</productID>
					<productID>{21B81C08-8854-4C2D-99E2-6CECA2D468A8}</productID>
					<productID>{56F2F3B5-8222-44E6-AE2F-B3C0B6041105}</productID>
					<productID>{64ECB814-3A6A-4E48-9D2F-D6C2EDD725B7}</productID>
					<productID>{70D25297-8E78-49EF-B639-FA51D33A4CE1}</productID>
					<productID>{71A3C9EA-E231-454D-9F2E-607B6018D9C6}</productID>
					<productID>{74E23E08-6C6D-4F8F-A62A-020FA19F7FCB}</productID>
					<productID>{7F93F26A-E5F7-4AE1-840F-F88DFE2DE3A5}</productID>
					<productID>{87749B2B-2175-4F7E-BE4C-FB13D0CCB0EF}</productID>
					<productID>{87E698D6-02AC-485E-A6BA-9194C94CC547}</productID>
					<productID>{89579637-4330-4FD3-BC3C-3D9042B6EFA5}</productID>
					<productID>{89CBC3A3-22E1-436A-B013-73A434A26D2F}</productID>
					<productID>{9C029F24-E4DA-4C8B-B059-F85234368053}</productID>
					<productID>{A5250560-F2D6-4275-9BCB-04FA5D3C3005}</productID>
					<productID>{AA58C320-050D-4D79-B568-765C4C954B4F}</productID>
					<productID>{BC9B5AF5-B10A-447C-8872-634518DC70B6}</productID>
					<productID>{C03716B7-FB4A-4479-A370-12D79AC30EAA}</productID>
					<productID>{E0DAF96D-BF67-46CD-87A7-45E7E12A8074}</productID>
					<productID>{E84997A1-4D6F-4C0B-B60D-F85B360D2666}</productID>
					<productID>{EB6C9E35-CBA2-4C2B-8958-55EA6F0EA707}</productID>
					<productID>{ECB572E6-5CE3-4E9E-B1B3-16A00E02153A}</productID>
					<productID>{F13AF45F-DDCC-45C0-AE87-436378E43F68}</productID>
					<productID>{F2273FA7-117C-43D7-BD59-00B025535442}</productID>
					<productID>{FD58B244-C5D6-4842-8940-6F315FCF717F}</productID>
					<productID>{FF36AF5C-D2AE-4698-90EB-7775F5E028D4}</productID></dependencies></product>
			<product name="NI LabWindows/CVI Shared Runtime 2015" UC="{80D3D303-75B9-4607-9312-E5FC68E5BFD2}" productID="{C123DBC1-8C63-4369-A052-EE7D3922EAC9}" path="(None)" flavorID="_full_" flavorName="Full" verRestr="false" coreVer="15.0.408">
				<dependencies>
					<productID>{0416C950-A8C6-4CFE-B206-A8D28091A40B}</productID>
					<productID>{054A4C14-7D56-4383-949B-F9B91E094F4B}</productID>
					<productID>{08505CC2-EA7F-4818-9C45-B74EDA7227F8}</productID>
					<productID>{10A1BCD4-CF1D-4198-B037-77FB1AEFF5FE}</productID>
					<productID>{1268324B-6C67-48D3-B58A-845E81AE6AFE}</productID>
					<productID>{195D8F29-2704-44BD-A978-57A0382327E5}</productID>
					<productID>{21B81C08-8854-4C2D-99E2-6CECA2D468A8}</productID>
					<productID>{37C0159A-E5E2-4688-9360-0435AE0E55C7}</productID>
					<productID>{56F2F3B5-8222-44E6-AE2F-B3C0B6041105}</productID>
					<productID>{64ECB814-3A6A-4E48-9D2F-D6C2EDD725B7}</productID>
					<productID>{70D25297-8E78-49EF-B639-FA51D33A4CE1}</productID>
					<productID>{71A3C9EA-E231-454D-9F2E-607B6018D9C6}</productID>
					<productID>{74E23E08-6C6D-4F8F-A62A-020FA19F7FCB}</productID>
					<productID>{7F93F26A-E5F7-4AE1-840F-F88DFE2DE3A5}</productID>
					<productID>{87749B2B-2175-4F7E-BE4C-FB13D0CCB0EF}</productID>
					<productID>{87E698D6-02AC-485E-A6BA-9194C94CC547}</productID>
					<productID>{A5250560-F2D6-4275-9BCB-04FA5D3C3005}</productID>
					<productID>{BC9B5AF5-B10A-447C-8872-634518DC70B6}</productID>
					<productID>{C03716B7-FB4A-4479-A370-12D79AC30EAA}</productID>
					<productID>{C1F1FD0A-D051-4CA7-AAF0-2E9C0A881054}</productID>
					<productID>{C23121A8-AF17-4C1D-A2B4-A6DE7A156DCF}</productID>
					<productID>{C67371A2-E288-429E-A2B6-85D36B29BF8E}</productID>
					<productID>{E0DAF96D-BF67-46CD-87A7-45E7E12A8074}</productID>
					<productID>{E84997A1-4D6F-4C0B-B60D-F85B360D2666}</productID>
					<productID>{EB6C9E35-CBA2-4C2B-8958-55EA6F0EA707}</productID>
					<productID>{ECB572E6-5CE3-4E9E-B1B3-16A00E02153A}</productID>
					<productID>{F13AF45F-DDCC-45C0-AE87-436378E43F68}</productID>
					<productID>{F2273FA7-117C-43D7-BD59-00B025535442}</productID>
					<productID>{FD58B244-C5D6-4842-8940-6F315FCF717F}</productID></dependencies></product></baselineProducts>
		<Projects NumProjects="1">
			<Project000 ProjectID="0" ProjectAbsolutePath="w:\M_PRJ\__PRJ\__CVI\TEST_UDP\test_udp.prj" ProjectRelativePath="test_udp.prj"/></Projects>
		<buildData progressBarRate="7.926789133204300">
			<progressTimes>
				<Begin>0.000000000000000</Begin>
				<ProductsAdded>0.186669784171163</ProductsAdded>
				<DPConfigured>0.520660498028793</DPConfigured>
				<DPMergeModulesAdded>0.994286676641852</DPMergeModulesAdded>
				<DPClosed>1.497480384531306</DPClosed>
				<DistributionsCopied>1.597961300961213</DistributionsCopied>
				<End>12.615448489869987</End></progressTimes></buildData>
	</msi>
</distribution>
