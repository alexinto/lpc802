#include <inttypes.h>
#include <cvirte.h>
#include <userint.h>
#include <windows.h>
#include <udpsupp.h>
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/list.h"
#include "System/crc.h"
#include "test_udp.h"
#include "ini.h"
#include "p_cal.h"
#include "menu_box.h"
#include "DRV/SyncClock/triple_spi.h"
#include "DRV/EXT_IF/ext_if.h"
#include "DRV/ADD_CLOCK/add_clock.h"
#include "commands.h"
#include "freq.h"
#include "DRV/SyncClock/cmd_oscillator.h"
#include "Types/DRV/dev_setts_types.h"
#include "CommonCommand.h"
#include "Types/System/fdebug_types.h"

#define BUFF_SIZE 1600
#define CMD_SIZE 128
#define READER_PORT 1020
#define PANEL_NUM 9
#define DEF_SETT_NAME "test_udp.ini"

typedef struct {
	char ip[16];
	char mask[16];
	int port_in;
	int port_out;
	int type;
}cur_settings_struct_t;


extern param__t p_cal__tbl[];

static char received_buff[10000000];
static param__t p_cal__tbl[1000];


static char* rxi_cmd_err_tbl[] = {
	"CMD_ERR__UNCKNOWN",
    "CMD_ERR__PARAM_NA",
    "CMD_ERR__BLOCK",
    "CMD_ERR__PROTECT",
    "CMD_ERR__NOT_EXIST",
    "CMD_ERR__PARAM_NOT_FOUND",
    "CMD_ERR__HW_ERROR",
    "CMD_ERR__NO_MEMORY",
    "CMD_ERR__BUSY",
    "CMD_ERR__TIMEOUT"
};



typedef struct {
	uint16_t cmd;
	uint8_t data[255];
	uint16_t data_len;
	int panel;
	void* ext_data;
}cmd_struct_t;

typedef struct {
	int len;
	int param;
}param_struct_t;

static int cmd_area_tbl[] = {PANEL_CMD, PANEL_PARAM_LEN, PANEL_PARAM_1, PANEL_PARAM_2, PANEL_PARAM_3, PANEL_PARAM_4, PANEL_PARAM_5, PANEL_PARAM_6};
static param_struct_t param_tbl[] = {{PANEL_TYPE_1, PANEL_PARAM_1}, {PANEL_TYPE_2, PANEL_PARAM_2}, {PANEL_TYPE_3, PANEL_PARAM_3}, {PANEL_TYPE_4, PANEL_PARAM_4}, {PANEL_TYPE_5, PANEL_PARAM_5}, {PANEL_TYPE_6, PANEL_PARAM_6}};

static struct {
	int pr_ver_receive;
	int close_program;
	uint8_t test_timeout;
	uint16_t calib_pkt;
	int cur_panel;
	double cur_time;
	char* cur_buf;
	uint32_t pkt_num;
	tester__state_e state;
	uint8_t auto_id;
	uint8_t listen;
	param__t* p_cal;
	uint32_t speed_cnt;
	int test_speed;
	char buff_time[100];
	uint32_t test_time;
	uint8_t id_pkt_block;
	uint16_t pkt_id;
	uint8_t tx_buff[BUFF_SIZE + 3], rx_buff[BUFF_SIZE];
	cur_settings_struct_t settings[PANEL_NUM];
	int timeout, tx_len;
	uint32_t readerChannel, answer_channel;
	ini__deskriptor_t* static__ini;
	sw_timer__t udp_timer, test_timer, print_timer, led_tmr, one_cmd_tmr, fw_upd_tmr, pr_upd_tmr, reg_tmr, sc_ch_tmr, fdebug_tmr;
	uint32_t all_pkt, err_pkt;
	FILE* f_fw_upd;
	uint32_t fw_upd_pkt;
	int fw_size, download_size;
	struct {
		uint32_t reg_buff;
		menu_box__item_t* cur_item;
		int menu, item_idx, item_max;
		triple_spi__oper_e oper;
	}reg_oper;
	cmd_struct_t one_cmd_buff;
}tester__data;

static cmd_oscillator__ch_t sc_ch_tbl[SYNCCLOCK__CH_MAX];
extern commands__struct_t cmd_tbl[];

typedef struct {
	int active;
	int id;
}panel_struct_t;


#define MENU_ITEM_MAX 11
static panel_struct_t panel_menu[MENU_ITEM_MAX];
#define MENU_AD9139    panel_menu[0]
#define MENU_HMC7044   panel_menu[1]
#define PANELHANDLE    panel_menu[2]
#define AUTORIZEPANEL  panel_menu[3]
#define COMMANDPANEL   panel_menu[4]
#define TABDESCRPANEL  panel_menu[5]
#define ADDCLOCKPANEL  panel_menu[6]
#define FWUPDPANEL     panel_menu[7]
#define PRUPDPANEL     panel_menu[8]
#define SYNCCLOCKPANEL panel_menu[9]
#define FDEBUGPANEL    panel_menu[10]

static panel_struct_t* find_panel(int panel);
int CVICALLBACK UDPCallback (unsigned channel, int eventType, int errCode, void *callbackData);
static int CVICALLBACK main_cout(void *functionId);
static int CVICALLBACK udp__cout(void *functionId);
static int CVICALLBACK version_cout(void *functionId);
static int CVICALLBACK add_clock_cout(void *functionId);
static void tester__init(char* sett_name);
static void tester__deinit(char* sett_name);
static int buff_dec_to_char(char* buff_dec, char* buff);
static void udp_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void test_timer_cb(struct sw_timer__t *timer, void *ext_data);
static void print_timer_cb(struct sw_timer__t *timer, void *ext_data);
static void led_timer_cb(struct sw_timer__t *timer, void *ext_data);
static void one_cmd_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void reg_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void fw_upd_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void sc_ch_tmr_cb(struct sw_timer__t *timer, void *ext_data);
void transmit_data(uint8_t* tx_buff, int len, uint8_t processor, tester__state_e state, triple_spi__oper_e oper);
uint32_t* transmit_get_state();
static void fw_upd__begin();
uint32_t add_clk_spi_get_param(int param);
int add_clk_spi_set_param(int param, uint32_t data);
static events__e reg_oper_set(int* p_menu, triple_spi__oper_e oper);
static void pr_upd_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void test_udp__get_version();
int cmp_time(char* destt, char* curt);
int start_udp_tester ();
void sc_ch_renew();
static void fdebug__renew();


int CVICALLBACK my_close_cb(int panelOrMenuBar, int controlOrMenuItem, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_CLOSE:
		case EVENT_END_TASK:
			if (panelOrMenuBar != PANELHANDLE.id)
				return 0;
			return -1;
	}
	return 0;
}


#include "DRV/AD5522/ad5522.h"
#include "DRV/AD7768/ad7768.h"

static ad7768__ch_reg_t* ad7768_reg;

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	InstallMainCallback (my_close_cb, NULL, 0);

	ad5522__data_t test;
	int t_size = sizeof(test);
	t_size = sizeof(ad5522__pmu_t);
	t_size = sizeof(ad5522__dac_t);
	t_size = sizeof(ad5522__scr_t);
	t_size = sizeof(ad5522__alarm_t);

	memset(&test, 0, t_size);
	test.comp.comp0 = AD5522_COMP_HI;
	if (test.comp.comp0 & AD5522_COMP_HI)
		test.comp.comp2 = AD5522_COMP_LOW;

	ad7768_reg = ad7768__get_regs(0);
	ad7768_reg->gpio_write.data = 1;

	uint32_t addr = AD7768_REG->sync_offset;
	addr = AD7768_REG->itf_cfg;
	addr = AD7768_REG->chop_ctrl;

	t_size = sizeof(ad7768_reg->sync_offset);

	start_udp_tester ();

	return 0;

}

int start_udp_tester () {

	if ((PANELHANDLE.id = LoadPanel (0, "test_udp.uir", PANEL)) < 0)
		return -1;
	if ((AUTORIZEPANEL.id = LoadPanel (0, "test_udp.uir", Autorize)) < 0)
		return -1;
	if ((COMMANDPANEL.id = LoadPanel (0, "test_udp.uir", CMD_PANEL)) < 0)
		return -1;
	if ((TABDESCRPANEL.id = LoadPanel (0, "test_udp.uir", PANEL_DESC)) < 0)
		return -1;
	if ((MENU_AD9139.id = LoadPanel (0, "test_udp.uir", PANEL_MENU)) < 0)
		return -1;
	menu_box__create(MENU_AD9139.id, 2, "AD9139", NULL);
	if ((MENU_HMC7044.id = LoadPanel (0, "test_udp.uir", PANEL_MENU)) < 0)
		return -1;
	menu_box__create(MENU_HMC7044.id, 2, "HMC7044", NULL);
	if ((ADDCLOCKPANEL.id = LoadPanel (0, "test_udp.uir", P_ADDCLOCK)) < 0)
		return -1;
	if ((FWUPDPANEL.id = LoadPanel (0, "test_udp.uir", P_FW_UPD)) < 0)
		return -1;
	if ((PRUPDPANEL.id = LoadPanel (0, "test_udp.uir", PANEL_PR)) < 0)
		return -1;
	if ((SYNCCLOCKPANEL.id = LoadPanel (0, "test_udp.uir", P_SYNCCLK)) < 0)
		return -1;
	if ((FDEBUGPANEL.id = LoadPanel (0, "test_udp.uir", FDEBUG)) < 0)
		return -1;


	sw_timer__init(NULL);
	tester__init(NULL);
	main_cout(NULL);
	version_cout(NULL);
	add_clock_cout(NULL);
	if (tester__data.listen)
		CreateUDPChannelConfig(tester__data.settings[tester__data.cur_panel].port_in, UDP_ANY_ADDRESS, 0, UDPCallback, NULL, &tester__data.readerChannel);
	PANELHANDLE.active = 1;
	for(int i = 0; i < MENU_ITEM_MAX; i++)
		if (panel_menu[i].active)
			DisplayPanel (panel_menu[i].id);

	while(!tester__data.close_program) {
		ProcessSystemEvents();
		Sleep(1);
	}
	sw_timer__stop(&tester__data.print_timer);
	main_cout(NULL);
	version_cout(NULL);
	add_clock_cout(NULL);
	tester__deinit(NULL);
	if (tester__data.listen)
		DisposeUDPChannel(tester__data.readerChannel);
	DiscardPanel (PANELHANDLE.id);
	return 0;
}

int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			tester__data.close_program = 1;
			break;
	}
	return 0;
}

static void tester__init(char* sett_name) {
	int tabpanel, temp, addr, f_addr, f_data, data;
	uint16_t time;
	char buff[BUFF_SIZE * 4 + 1];
	if (sett_name == NULL)
		sett_name = DEF_SETT_NAME;
	tester__data.static__ini = ini__create(0, sett_name);

	sprintf(buff, "Test_UDP (%s)", _TARGET_FILE_VERSION_);
	SetPanelAttribute (PANELHANDLE.id, ATTR_TITLE, buff);

	for(int i = 0; cmd_tbl[i].cmd_id != 0; i++) {
		InsertListItem (PANELHANDLE.id, PANEL_CMD_CHANGE, i, cmd_tbl[i].cmd_name, i);
	}
	RecallPanelState (ADDCLOCKPANEL.id, "menu_settings.ini", ADDCLOCKPANEL.id);
	RecallPanelState (SYNCCLOCKPANEL.id, "menu_settings.ini", SYNCCLOCKPANEL.id);
	RecallPanelState (FWUPDPANEL.id, "menu_settings.ini", FWUPDPANEL.id);
	RecallPanelState (PRUPDPANEL.id, "menu_settings.ini", PRUPDPANEL.id);
	RecallPanelState (FDEBUGPANEL.id, "menu_settings.ini", FDEBUGPANEL.id);

	ini__panel_get(tester__data.static__ini, "windows", PANELHANDLE.id);
	ini__panel_get(tester__data.static__ini, "cmd windows", COMMANDPANEL.id);
	ini__panel_get(tester__data.static__ini, "window add_clock", ADDCLOCKPANEL.id);
	ini__panel_get(tester__data.static__ini, "window fw_upd", FWUPDPANEL.id);
	ini__panel_get(tester__data.static__ini, "window pr_upd", PRUPDPANEL.id);
	ini__panel_get(tester__data.static__ini, "window sync_clock", SYNCCLOCKPANEL.id);
	ini__panel_get(tester__data.static__ini, "window add_clock_ad9139", MENU_AD9139.id);
	ini__panel_get(tester__data.static__ini, "window add_clock_hmc7044", MENU_HMC7044.id);
	ini__panel_get(tester__data.static__ini, "window fdebug", FDEBUGPANEL.id);


	ini__int_get(tester__data.static__ini, "TESTER", "timeout", &tester__data.timeout);
	if (tester__data.timeout < 50)
		tester__data.timeout = 1000;
	SetCtrlVal(PANELHANDLE.id, PANEL_NUMERICSLIDE, tester__data.timeout);
	temp = 1;
	SetCtrlVal(PANELHANDLE.id, PANEL_AUTO_ID, temp);
	auto_id (PANELHANDLE.id, PANEL_AUTO_ID, EVENT_COMMIT,NULL, 0, 0);
	GetCtrlVal(PANELHANDLE.id, PANEL_LISTEN, &temp);
	tester__data.listen = (uint8_t)temp;
	for(int i = 0; i < 4; i++) {
		sprintf(buff, "SPI_ADDR_REG_%d", i);
		if (ini__int_get(tester__data.static__ini, "ADD_CLOCK", buff, &temp)) {
			add_clk_spi_set_param(i, temp);
		}
	}
	for(int i = 0; i < MENU_ITEM_MAX; i++) {
		sprintf(buff, "MENU_ACTIVE_%d", i);
		temp = 0;
		ini__int_get(tester__data.static__ini, "PANEL MENU SETTIGS", buff, &temp);
		if (temp)
			panel_menu[i].active = 1;
	}

	cur_settings_struct_t* p_sett = tester__data.settings;
	DefaultCtrl (PANELHANDLE.id, PANEL_TAB);
	for(int i = 0; i < PANEL_NUM; i++) {
		p_sett = &tester__data.settings[i];
		GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, i, &tabpanel);

		sprintf(buff, "IP %d", i);
		if (ini__string_get(tester__data.static__ini, "UDP", buff, p_sett->ip, 16) != NULL)
			SetCtrlVal(tabpanel, TABPANEL_IP, p_sett->ip);
		sprintf(buff, "PORT_OUT %d", i);
		if (ini__int_get(tester__data.static__ini, "UDP", buff, &p_sett->port_out) != NULL)
			SetCtrlVal(tabpanel, TABPANEL_PORT, p_sett->port_out);
		sprintf(buff, "PORT_IN %d", i);
		if (ini__int_get(tester__data.static__ini, "UDP", buff, &p_sett->port_in) != NULL)
			SetCtrlVal(tabpanel, TABPANEL_PORT_IN, p_sett->port_in);
		sprintf(buff, "MASK %d", i);
		if (ini__string_get(tester__data.static__ini, "UDP", buff, p_sett->mask, 16) != NULL)
			SetCtrlVal(tabpanel, TABPANEL_MASK, p_sett->mask);
		sprintf(buff, "BUFF %d", i);
		if (ini__string_get(tester__data.static__ini, "UDP", buff, &buff[50], BUFF_SIZE * 4 + 1) != NULL)
			SetCtrlVal(tabpanel, TABPANEL_TRANSMIT, &buff[50]);

		GetCtrlVal(tabpanel, TABPANEL_IP, p_sett->ip);
		GetCtrlVal(tabpanel, TABPANEL_PORT, &p_sett->port_out);
		GetCtrlVal(tabpanel, TABPANEL_PORT_IN, &p_sett->port_in);
		GetCtrlVal(tabpanel, TABPANEL_MASK, p_sett->mask);

		if (ini__string_get(tester__data.static__ini, "TAB_PANEL", NULL, buff, i + 1)) {
			ini__string_get(tester__data.static__ini, "TAB_PANEL", buff, &buff[100], -1);
			if (strlen(&buff[100]) > 0)
				SetTabPageAttribute (PANELHANDLE.id, PANEL_TAB, i, ATTR_LABEL_TEXT, &buff[100]);
		}

	}

	GetNumListItems (COMMANDPANEL.id, CMD_PANEL_LISTBOX, &temp);
	if ((temp) && (ini__string_get(tester__data.static__ini, "CMD MENU", NULL, buff, 1)))
		DeleteListItem (COMMANDPANEL.id, CMD_PANEL_LISTBOX, 0, temp);
	temp = 1;
	while (ini__string_get(tester__data.static__ini, "CMD MENU", NULL, buff, temp)) {
		ini__string_get(tester__data.static__ini, "CMD MENU", buff, &buff[100], BUFF_SIZE * 4 + 1);
		InsertListItem (COMMANDPANEL.id, CMD_PANEL_LISTBOX, -1, buff, &buff[100]);
		temp++;
	}
	temp = 0;
	ini__int_get(tester__data.static__ini, "CMD MENU CTRL", "BUTTON", &temp);
	SetCtrlVal(PANELHANDLE.id, PANEL_TOGGLEBUTTON, temp);
	if (temp) {
		COMMANDPANEL.active = 1;
		DisplayPanel (COMMANDPANEL.id);
	}
	ini__int_get(tester__data.static__ini, "CMD FORMAT", "HEX", &temp);
	SetCtrlVal(PANELHANDLE.id, PANEL_HEX_DEC, temp);
	hex_dec_ctrl_cb(PANELHANDLE.id, PANEL_HEX_DEC, EVENT_VAL_CHANGED, 0, 0, 0);

	int menu_item_num = 0;
	for(int i = 0; i < MENU_ITEM_MAX; i++) {
		if (menu_box__get_name(panel_menu[i].id)) {
			menu_item_num = menu_box__get_item_num(panel_menu[i].id, MENU_BOX__LAST_ITEM);
			while(menu_item_num > 0) {
				menu_box__del_item(panel_menu[i].id, menu_item_num);
				menu_item_num--;
			}
			menu_item_num = 1;
			while(ini__key_get(tester__data.static__ini, menu_box__get_name(panel_menu[i].id), buff, menu_item_num)) {
				ini__int_get(tester__data.static__ini, menu_box__get_name(panel_menu[i].id), buff, &data);
				sscanf(buff, "%d %d %d %d", &addr, &addr, &f_addr, &f_data);
				menu_box__add_item(panel_menu[i].id, menu_item_num, f_addr, f_data);
				menu_box__set_item(panel_menu[i].id, menu_item_num, addr, data);
				menu_item_num++;
			}
		}

	}
	temp = 1;
	while (ini__string_get(tester__data.static__ini, "SC_CH_TBL", NULL, buff, temp)) {
		if (temp > SYNCCLOCK__CH_MAX)
			break;
		ini__string_get(tester__data.static__ini, "SC_CH_TBL", buff, &buff[100], BUFF_SIZE * 4 + 1);
		sscanf(&buff[100], "%d", &menu_item_num);
		sscanf(&buff[100], "%d %d %d %d", &menu_item_num, &sc_ch_tbl[menu_item_num].ch, &sc_ch_tbl[menu_item_num].state, &sc_ch_tbl[menu_item_num].freq);
		temp++;
	}
	sc_ch_renew();


	GetCtrlVal(PANELHANDLE.id, PANEL_DRAW_TIME, &time);
	sw_timer__start(&tester__data.print_timer, time, print_timer_cb, NULL);
	if (sett_name) {
		ini__close(tester__data.static__ini);
		tester__data.static__ini = ini__create(0, DEF_SETT_NAME);
	}
	sc_ch_ctrl(SYNCCLOCKPANEL.id, P_SYNCCLK_SYNC_CH_ON, EVENT_COMMIT, NULL, 0, 0);
}

static void tester__deinit(char* sett_name) {
	char buff[BUFF_SIZE * 20 + 1];
	int tabpanel, max_index, temp;
	if (sett_name)
		tester__data.static__ini = ini__create(0, sett_name);

	ini__panel_put(tester__data.static__ini, "windows", PANELHANDLE.id);
	ini__panel_put(tester__data.static__ini, "cmd windows", COMMANDPANEL.id);
	ini__panel_put(tester__data.static__ini, "window add_clock", ADDCLOCKPANEL.id);
	ini__panel_put(tester__data.static__ini, "window fw_upd", FWUPDPANEL.id);
	ini__panel_put(tester__data.static__ini, "window pr_upd", PRUPDPANEL.id);
	ini__panel_put(tester__data.static__ini, "window sync_clock", SYNCCLOCKPANEL.id);
	ini__panel_put(tester__data.static__ini, "window add_clock_ad9139", MENU_AD9139.id);
	ini__panel_put(tester__data.static__ini, "window add_clock_hmc7044", MENU_HMC7044.id);
	ini__panel_put(tester__data.static__ini, "window fdebug", FDEBUGPANEL.id);

	SavePanelState (ADDCLOCKPANEL.id, "menu_settings.ini", ADDCLOCKPANEL.id);
	SavePanelState (SYNCCLOCKPANEL.id, "menu_settings.ini", SYNCCLOCKPANEL.id);
	SavePanelState (FWUPDPANEL.id, "menu_settings.ini", FWUPDPANEL.id);
	SavePanelState (PRUPDPANEL.id, "menu_settings.ini", PRUPDPANEL.id);
	SavePanelState (FDEBUGPANEL.id, "menu_settings.ini", FDEBUGPANEL.id);


	ini__int_put(tester__data.static__ini, "TESTER", "timeout", tester__data.timeout);

	for(int i = 0; i < 4; i++) {
		sprintf(buff, "SPI_ADDR_REG_%d", i);
		temp = add_clk_spi_get_param(i);
		ini__int_put(tester__data.static__ini, "ADD_CLOCK", buff, temp);
	}
	for(int i = 0; i < MENU_ITEM_MAX; i++) {
		sprintf(buff, "MENU_ACTIVE_%d", i);
		ini__int_put(tester__data.static__ini, "PANEL MENU SETTIGS", buff, panel_menu[i].active);
	}


	cur_settings_struct_t* p_sett = tester__data.settings;
	for(int i = 0; i < PANEL_NUM; i++) {
		p_sett = &tester__data.settings[i];
		sprintf(buff, "IP %d", i);
		ini__string_put(tester__data.static__ini, "UDP", buff, p_sett->ip);
		sprintf(buff, "MASK %d", i);
		ini__string_put(tester__data.static__ini, "UDP", buff, p_sett->mask);
		sprintf(buff, "PORT_OUT %d", i);
		ini__int_put(tester__data.static__ini, "UDP", buff, p_sett->port_out);
		sprintf(buff, "PORT_IN %d", i);
		ini__int_put(tester__data.static__ini, "UDP", buff, p_sett->port_in);

		GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, i, &tabpanel);
		sprintf(buff, "BUFF %d", i);
		GetCtrlVal(tabpanel, TABPANEL_TRANSMIT, &buff[50]);
		ini__string_put(tester__data.static__ini, "UDP", buff, &buff[50]);

		GetTabPageAttribute (PANELHANDLE.id, PANEL_TAB, i, ATTR_LABEL_TEXT, buff);
		sprintf(&buff[100], "LABEL %d", i);
		ini__string_put(tester__data.static__ini, "TAB_PANEL", &buff[100], buff);

	}
	menu_box__item_t* menu_item;
	int menu_item_num = 0;
	for(int i = 0; i < MENU_ITEM_MAX; i++) {
		menu_item_num = menu_box__get_item_num(panel_menu[i].id, MENU_BOX__LAST_ITEM);
		if ((menu_item_num > 0) && (menu_box__get_name(panel_menu[i].id))) {
			ini__remove_section(tester__data.static__ini, menu_box__get_name(panel_menu[i].id));
			for(int j = 1; j <= menu_item_num; j++) {
				menu_item = menu_box__get_item(panel_menu[i].id, j);
				if (menu_item) {
					sprintf(buff, "%04d %d %d %d", j, menu_item->addr, menu_item->format_addr, menu_item->format_data);
					ini__int_put(tester__data.static__ini, menu_box__get_name(panel_menu[i].id), buff, menu_item->data);
				}
			}
		}
	}

	GetNumListItems (COMMANDPANEL.id, CMD_PANEL_LISTBOX, &max_index);
	ini__remove_section(tester__data.static__ini, "CMD MENU");
	for (int i = 0; i < max_index; i++) {
		GetLabelFromIndex (COMMANDPANEL.id, CMD_PANEL_LISTBOX, i, buff);
		GetValueFromIndex (COMMANDPANEL.id, CMD_PANEL_LISTBOX, i, &buff[100]);
		ini__string_put(tester__data.static__ini, "CMD MENU", buff, &buff[100]);
	}

	GetCtrlVal(PANELHANDLE.id, PANEL_TOGGLEBUTTON, &temp);
	ini__int_put(tester__data.static__ini, "CMD MENU CTRL", "BUTTON", temp);
	GetCtrlVal(PANELHANDLE.id, PANEL_HEX_DEC, &temp);
	ini__int_put(tester__data.static__ini, "CMD FORMAT", "HEX", temp);

	ini__remove_section(tester__data.static__ini, "SC_CH_TBL");
	for (int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
		sprintf(buff, "CH%d", i + 1);
		sprintf(&buff[100], "%d %d %d %d", i, sc_ch_tbl[i].ch, sc_ch_tbl[i].state, sc_ch_tbl[i].freq);
		ini__string_put(tester__data.static__ini, "SC_CH_TBL", buff, &buff[100]);
	}


	ini__save(tester__data.static__ini);
	FILE * f_out, *f_in;
	int byte;
	if (sett_name) {
		ini__close(tester__data.static__ini);
		f_out = fopen (DEF_SETT_NAME, "wb");
		f_in = fopen (sett_name, "rb");
		while((byte = getc(f_in)) >= 0)
			putc(byte, f_out);
		fflush(f_out);
		fclose(f_in);
		fclose(f_out);
		tester__data.static__ini = ini__create(0, DEF_SETT_NAME);
	}
}

static int CVICALLBACK main_cout(void *functionId) {
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(200));
		return 0;
	}
	while(*(int*)functionId) {
		Sleep(1), framework__cout();
	}
	*(int*)functionId = -1;
	return 0;
}

static int CVICALLBACK version_cout(void *functionId) {
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, version_cout, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(200));
		return 0;
	}
	tester__data.pr_ver_receive = 1;
	while(*(int*)functionId) {
		if (tester__data.pr_ver_receive) {
			test_udp__get_version();
			tester__data.pr_ver_receive = 0;
		}
		Sleep(100);
	}
	*(int*)functionId = -1;
	return 0;
}




static int CVICALLBACK add_clock_cout(void *functionId) {
	static int init;
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, add_clock_cout, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(200));
		return 0;
	}

	while(*(int*)functionId) {
		Sleep(1);
		if (tester__data.reg_oper.cur_item) {
			if (!init) {
				add_clock__init(triple_spi__ext_module);
				init = 1;
			}
			if (tester__data.reg_oper.menu == MENU_AD9139.id){
					add_clock__ad9139_spi(tester__data.reg_oper.oper, (uint16_t)tester__data.reg_oper.cur_item->addr, (uint8_t*)&tester__data.reg_oper.cur_item->data, 1);
			}
			else if (tester__data.reg_oper.menu == MENU_HMC7044.id) {
				add_clock__hmc7044_spi(tester__data.reg_oper.oper, (uint16_t)tester__data.reg_oper.cur_item->addr, (uint8_t*)&tester__data.reg_oper.cur_item->data, 1);
			}
			if (tester__data.reg_oper.oper == TSPI__OPER_READ) {
				menu_box__set_item(tester__data.reg_oper.menu, tester__data.reg_oper.item_idx, tester__data.reg_oper.cur_item->addr, tester__data.reg_oper.cur_item->data & 0xFF);
			}
			if (tester__data.reg_oper.item_idx < tester__data.reg_oper.item_max) {
				tester__data.reg_oper.item_idx++;
				tester__data.reg_oper.cur_item = menu_box__get_item(tester__data.reg_oper.menu, tester__data.reg_oper.item_idx);
				tester__data.download_size = tester__data.reg_oper.item_idx;
			}
			else
				reg_oper_set(NULL, TSPI__OPER_NONE);
		}
	}
	*(int*)functionId = -1;
	return 0;
}



int CVICALLBACK timeout_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlVal(panel, control, &tester__data.timeout);
			break;
	}
	return 0;
}

int CVICALLBACK udp_sett_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {

	cur_settings_struct_t* p_sett = &tester__data.settings[tester__data.cur_panel];
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			switch(control) {
				case TABPANEL_IP:
					GetCtrlVal(panel, control, (char*)p_sett->ip);
					break;
				case TABPANEL_PORT:
					GetCtrlVal(panel, control, &p_sett->port_out);
					break;
				case TABPANEL_MASK:
					GetCtrlVal(panel, control, (char*)p_sett->mask);
					break;
				case TABPANEL_PORT_IN:
					GetCtrlVal(panel, control, &p_sett->port_in);
					break;
			}

			break;
	}
	return 0;
}


static void cmd_add_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	cmd_add_cb ((int)ext_data, PANEL_ADD_CMD_BUTTON, EVENT_COMMIT, NULL, 0, 0);

}

int CVICALLBACK cmd_add_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	static sw_timer__t cmd_add_tmr;
	char buff[CMD_SIZE * 6 + 128] = {0};
	uint16_t param, param_len;
	uint32_t param_data;
	int tabpanel, data_len, all_len = 0;

	switch (event)
	{
		case EVENT_KEYPRESS:
			if (eventData1 == 1280)
				sw_timer__start(&cmd_add_tmr, 1, cmd_add_tmr_cb, (void*)panel);
			break;
		case EVENT_COMMIT:
			if (control != PANEL_ADD_CMD_BUTTON)
				break;
			GetCtrlVal(panel, PANEL_CMD, &param);
			GetCtrlVal(panel, PANEL_PARAM_LEN, &param_len);
			for(int i = 0; i < param_len; i++) {
				GetCtrlVal(panel, param_tbl[i].len, &data_len);
				all_len += data_len;
			}

			sprintf(buff, "%05d %05d ", param, all_len);

			for(int i = 0; i < param_len; i++) {
				if (i >= sizeof(param_tbl) / sizeof(param_tbl[0]))
					break;
				GetCtrlVal(panel, param_tbl[i].param, &param_data);
				GetCtrlVal(panel, param_tbl[i].len, &data_len);
				for (int j = 0; j < data_len; j++)
					sprintf(buff, "%s%d ", buff, (param_data >> (j * 8)) & 0xFF);
			}
			GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
			SetCtrlVal (tabpanel, TABPANEL_TRANSMIT, buff);
			break;
	}
	return 0;
}






int CVICALLBACK test_ctrl_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	static int panel_init;
	int test_on;
	uint16_t pkt_id;
	cur_settings_struct_t* p_sett = &tester__data.settings[tester__data.cur_panel];
	switch (event)
	{
		case EVENT_COMMIT:
			CRITICAL_SECTION_ON
			GetCtrlVal (panel, control, &test_on);
			GetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, &pkt_id);
			SetCtrlAttribute (PANELHANDLE.id, PANEL_LISTEN, ATTR_DIMMED, test_on);
			if (test_on) {
				if (!tester__data.listen)
					CreateUDPChannelConfig(p_sett->port_in, UDP_ANY_ADDRESS, 0, UDPCallback, NULL, &tester__data.readerChannel);
				SetUDPAttribute(tester__data.readerChannel, ATTR_UDP_PORT, p_sett->port_out);
				if ((callbackData == NULL) || (!panel_init)) {
					ResetTextBox (panel, PANEL_RECEIVE, "");
					tester__data.cur_time = Timer();
					panel_init = 1;
				}
				tester__data.test_time = 0;
				tester__data.all_pkt = tester__data.err_pkt = 0;
				sprintf(tester__data.buff_time, "%0.3f", Timer() - tester__data.cur_time);
				switch(tester__data.state) {
				    case STATE_IDLE:
						tester__data.state = STATE_NORMAL;
					case STATE_FREQ_CYCLE:
					case STATE_NORMAL:
						sw_timer__start(&tester__data.test_timer, 100, test_timer_cb, NULL);
					case STATE_ONE_TRANSMIT:
						transmit_data(NULL, 0, 0, 0, 0);
						break;
					case STATE_ONE_CMD:
						tester__data.one_cmd_buff.cmd = *(uint16_t*)&tester__data.tx_buff[3];
					case STATE_OPER_REG:
						break;
					default:
						tester__data.test_speed = 1;
						SetCtrlVal(PANELHANDLE.id, PANEL_TEST_SPEED, tester__data.test_speed);
						break;

				}
				tester__data.test_timeout = 1;
				sw_timer__start(&tester__data.udp_timer, 1, udp_tmr_cb, (void*)PANELHANDLE.id);
				SetCtrlVal(PANELHANDLE.id, PANEL_TRANSFET_LED, 1);
				sw_timer__start(&tester__data.led_tmr, 100, led_timer_cb, NULL);
			}
			else {
				if (!tester__data.listen)
					DisposeUDPChannel(tester__data.readerChannel);
				test_on = 0;
				SetCtrlVal(SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_TEST, test_on);
				freq_test_cb (SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_TEST, EVENT_COMMIT, NULL, 0, 0);
				sw_timer__stop(&tester__data.test_timer);
				sw_timer__stop(&tester__data.udp_timer);
				tester__data.test_speed = 0;
				pkt_id = *(uint16_t*)&tester__data.tx_buff[1] + 1;
				SetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, pkt_id);
				if (tester__data.f_fw_upd) {
					fclose(tester__data.f_fw_upd);
					tester__data.f_fw_upd = NULL;
				}
				tester__data.state = STATE_IDLE;
			}
			CRITICAL_SECTION_OFF
			break;
	}
	return 0;
}


int CVICALLBACK UDPCallback (unsigned channel, int eventType, int errCode, void *callbackData) {
    int size, tabpanel;
    unsigned char   msg[1600];
	char buff[BUFF_SIZE * 20 + 128], src_addr[16];
    uint32_t src_port, index;
	uint16_t pkt_id, tx_ready = 0;
	int temp;
	cur_settings_struct_t* p_sett = &tester__data.settings[tester__data.cur_panel];
	char* cmd_err = rxi_cmd_err_tbl[0];

    if (eventType == UDP_DATAREADY) {
		CRITICAL_SECTION_ON
        // Read the waiting message into the allocated buffer.
		size = UDPRead(channel, msg, sizeof(msg), UDP_DO_NOT_WAIT, &src_port, src_addr);
        // Display the message, preceeded by the sender's IP address and port number.
        sprintf(buff, "%0.3f[%s:%d]", (Timer() - tester__data.cur_time), src_addr, src_port);
		tester__data.test_timeout = 0;
		if (size < 4) {
			sprintf(buff, "%s[A]: %05d ", buff, *(uint16_t*)&msg[1]);
			switch(tester__data.state) {
				case STATE_CALIBRATION:
					if (tester__data.p_cal->freq == 0xFFFFFFFF) {
						GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
						GetCtrlVal(tabpanel, TABPANEL_TRANSMIT, buff);
						tester__data.tx_len = buff_dec_to_char(buff, (char*)&tester__data.tx_buff[3]) + 3;
						sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, NULL);
					}
					else {
						tester__data.calib_pkt++;
						*(uint16_t*)&tester__data.tx_buff[7] = tester__data.calib_pkt;
						tester__data.tx_len = 9;
						for(int i = 0; i < 8 && (tester__data.p_cal->freq != 0xFFFFFFFF); i++) {
							memcpy(&tester__data.tx_buff[9] + i * sizeof(param__t), tester__data.p_cal, sizeof(param__t));
							tester__data.p_cal++;
							tester__data.tx_len += sizeof(param__t);
						}
						*(uint16_t*)&tester__data.tx_buff[5] = (uint16_t)tester__data.tx_len - 7;
					}
					break;
				case STATE_FW_UPD:
				case STATE_FW_UPD_SUB:
					tester__data.tx_len = 3;
					for(int i = 3; i < 1152; i+= index + 8)
					{
						index = fread (&tester__data.tx_buff[i+8], 1, 184, tester__data.f_fw_upd);
						tester__data.download_size += index;
						if (index == 0)
						{
							if (i == 3)
							{
								if (tester__data.state == STATE_FW_UPD_SUB)
									sw_timer__start(&tester__data.fw_upd_tmr, 5000, fw_upd_tmr_cb, NULL);
								sprintf(buff, "%s Передача завершена\n", buff);
								sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, NULL);
							}
							break;
						}
						else
							tester__data.tx_len += index + 8;


						*(uint16_t*)&tester__data.tx_buff[i+2] = (uint16_t)index + 4;

						*(uint16_t*)&tester__data.tx_buff[i+4] = (uint16_t)tester__data.fw_upd_pkt;
						*(uint16_t*)&tester__data.tx_buff[i+6] = (uint16_t)index;
						if (tester__data.state == STATE_FW_UPD) {
							*(uint32_t*)&tester__data.tx_buff[i+4] = tester__data.fw_upd_pkt;
							tester__data.fw_upd_pkt++;
						}
						*(uint16_t*)&tester__data.tx_buff[i] = (tester__data.state == STATE_FW_UPD) ? 0x18 : 0x1C;
						GetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, &pkt_id);
					}
					break;
				case STATE_ONE_CMD:
					break;
				case STATE_OPER_REG:
					if (tester__data.reg_oper.menu)
						break;
				case STATE_ONE_TRANSMIT:
					sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, NULL);
					break;
				default:
					break;
			}

			if (tester__data.test_speed) {
				sw_timer__stop(&tester__data.udp_timer);
				pkt_id = *(uint16_t*)&tester__data.tx_buff[1];
				pkt_id++;
				*(uint16_t*)&tester__data.tx_buff[1] = pkt_id;
				if (!tester__data.id_pkt_block)
					SetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, pkt_id);
				tx_ready = 0;
				if ((tester__data.state == STATE_FW_UPD) || (tester__data.state == STATE_FW_UPD_SUB))
					sw_timer__start(&tester__data.udp_timer, 1, udp_tmr_cb, (void*)1);
				else
					while(UDPWrite(tester__data.readerChannel, p_sett->port_out, p_sett->ip, tester__data.tx_buff, tester__data.tx_len));
			}
		}
		else {
			if (tester__data.test_speed)
				tester__data.speed_cnt += size;
			if (msg[0] == 3) {
				if (tester__data.auto_id) {
					pkt_id = *(uint16_t*)&msg[3];
					SetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, pkt_id);
					*(uint16_t*)&tester__data.tx_buff[1] = pkt_id;
					sw_timer__start(&tester__data.udp_timer, 100, udp_tmr_cb, (void*)1);
				}
				else {
					tester__data.err_pkt++;
				}
				sprintf(buff, "%s[E]: Текущий ID = %05d , требуется ID %05d", buff, *(uint16_t*)&msg[1], *(uint16_t*)&msg[3]);
			}
			else {
				switch(tester__data.state) {
				case STATE_FW_UPD:
				case STATE_FW_UPD_SUB:
						if (msg[4] == 2) {     // Ошибка
							sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, NULL);
						}
						break;
					case STATE_ONE_CMD:
						if (((msg[0] == 4) && (msg[4] < 2)) && ((msg[4] == 1) && (*(uint16_t*)&msg[5] < 256))) {
							memcpy(&tester__data.one_cmd_buff.data, &msg[7], *(uint16_t*)&msg[5]);
							tester__data.one_cmd_buff.data_len = *(uint16_t*)&msg[5];
						}

						if (msg[4] == 2) {
							sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, NULL);
							break;
						}
						sw_timer__start(&tester__data.one_cmd_tmr, 1, one_cmd_tmr_cb, NULL);
						break;
					case STATE_OPER_REG:
						if (((msg[0] == 4) && (msg[4] < 2)) && ((tester__data.reg_oper.menu) && (msg[4] == 1) && (msg[7] <= 4))) {
							if (tester__data.reg_oper.cur_item) {
								tester__data.reg_oper.reg_buff = 0;
								memcpy(&tester__data.reg_oper.reg_buff, &msg[12], msg[7]);
							}
							else
								tabpanel = 1;
						}
						sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, NULL);
						break;
					default:
						break;
				}
				tester__data.all_pkt++;
				if (tester__data.test_speed) {
					sprintf(buff, "");
				}
				else {
					sprintf(buff, "%s[R]: %05d ", buff, *(uint16_t*)&msg[1]);
					for(int i = 4; i < size; i++)
						sprintf(buff, "%s%02x ", buff, msg[i]);
				}
			}
			if ((msg[0] == 4) || (msg[0] == 5)) {
				temp = 0;
				for(int i = 4; i < size; i+= *(uint16_t*)&msg[i + 1] + 3, temp++) {
					if (msg[i] != 2)
						continue;
					if (*(uint16_t*)&msg[i + 1] < (sizeof(rxi_cmd_err_tbl) / sizeof(rxi_cmd_err_tbl[0])))
						cmd_err = rxi_cmd_err_tbl[*(uint16_t*)&msg[i + 1]];
					sprintf(buff, "%s\n[E]: Номер команды в пакете = %d, ошибка = %s", buff, temp, cmd_err);
					break;
				}
				msg[0] = 0x06;
				while(UDPWrite(tester__data.readerChannel, p_sett->port_out, p_sett->ip, msg, 3));  // RACK
			}

		}
		if (tester__data.cur_buf) {
			if (strlen(buff)) {
				strcpy(tester__data.cur_buf, buff);
				tester__data.cur_buf += strlen(buff) + 1;
				tester__data.pkt_num++;
			}

			if (tx_ready) {
				sprintf(buff, "%0.3f[%s:%d][T]: %05d ", (Timer() - tester__data.cur_time), p_sett->ip, p_sett->port_out, *(uint16_t*)&tester__data.tx_buff[1]);
				for(int i = 3; i < tester__data.tx_len; i++)
					sprintf(buff, "%s%02x ", buff, tester__data.tx_buff[i]);
				strcpy(tester__data.cur_buf, buff);
				tester__data.cur_buf += strlen(buff) + 1;
				tester__data.pkt_num++;
			}
		}
		CRITICAL_SECTION_OFF
    }


    return 0;
}

static int buff_dec_to_char(char* buff_dec, char* buff) {
	int num, idx, res = 0, its16;
	while(*buff_dec) {
		num = 0;
		for(idx = 0; (*buff_dec >= '0') && (*buff_dec <= '9'); idx++, buff_dec++);
		if (idx) {
			its16 = idx / 4;
			while(idx--)
				num += (*(buff_dec - idx - 1) - '0') * pow(10, idx);
			if(its16)
				*(uint16_t*)(buff + res) = (uint16_t)num;
			else
				*(buff + res) = (char)num;
			res += its16 + 1;
		}
		if (*buff_dec)
			buff_dec++;
	}
	return res;
}

static void udp_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	char buff[BUFF_SIZE * 20 + 128], buff_freq[300];
	uint16_t pkt_id;
	uint32_t freq, old_freq;
	int ch;
	cur_settings_struct_t* p_sett = &tester__data.settings[tester__data.cur_panel];
	if (tester__data.tx_len < 4)
		return;
	CRITICAL_SECTION_ON
	if ((tester__data.state == STATE_NORMAL) || (tester__data.state == STATE_FREQ_CYCLE) || (tester__data.state == STATE_FW_UPD_SUB))
		sw_timer__start(&tester__data.udp_timer, tester__data.timeout, udp_tmr_cb, NULL);
	if (ext_data == NULL) {
		pkt_id = *(uint16_t*)&tester__data.tx_buff[1];
		pkt_id++;
		*(uint16_t*)&tester__data.tx_buff[1] = pkt_id;
		if (!tester__data.id_pkt_block)
			SetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, pkt_id);
		if (tester__data.state == STATE_FREQ_CYCLE) {
			GetCtrlVal(SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_CH, &ch);
			sprintf(buff_freq, "00420 00008 %1d 0 1 0 ", ch);
			GetCtrlVal(SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_SET, &freq);
			old_freq = freq;
			for(uint32_t cur_freq = freq; old_freq == freq; cur_freq -= 1000)
				freq = find_freq(cur_freq, 0);
			if (freq < 55000000)
				freq = old_freq;
			SetCtrlVal(SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_SET, freq);

			for(int i = 0; i < 4; i++) {
				sprintf(buff_freq, "%s%d ", buff_freq, ((uint8_t*)&freq)[i]);
			}
			sprintf(buff_freq, "%s %s", buff_freq, "00418 00000 00419 00000 ");

			tester__data.tx_len = buff_dec_to_char(buff_freq, (char*)&tester__data.tx_buff[3]) + 3;
		}

	}
	sprintf(buff, "%0.3f[%s:%d][T]: %05d ", (Timer() - tester__data.cur_time), p_sett->ip, p_sett->port_out, *(uint16_t*)&tester__data.tx_buff[1]);
	for(int i = 3; i < tester__data.tx_len; i++)
		sprintf(buff, "%s%02x ", buff, tester__data.tx_buff[i]);
	if (tester__data.cur_buf) {
		strcpy(tester__data.cur_buf, buff);
		tester__data.cur_buf += strlen(buff) + 1;
		tester__data.pkt_num++;
	}
	CRITICAL_SECTION_OFF
	while(UDPWrite(tester__data.readerChannel, p_sett->port_out, p_sett->ip, tester__data.tx_buff, tester__data.tx_len));
}

static void test_timer_cb(struct sw_timer__t *timer, void *ext_data) {
	static uint8_t check_sec;
	uint32_t time_temp;
	sw_timer__start(timer, -1, test_timer_cb, NULL);
	time_temp = ++tester__data.test_time / 10;
	sprintf(tester__data.buff_time, "%0.3f", Timer() - tester__data.cur_time);

	if (++check_sec > 9) {
		SetCtrlVal(PANELHANDLE.id, PANEL_SPEED, tester__data.speed_cnt);
		tester__data.speed_cnt = 0;
		check_sec = 0;
	}

	SetCtrlVal(PANELHANDLE.id, PANEL_TEST_TIME, tester__data.buff_time);
}



int CVICALLBACK set_pkt_id (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	uint16_t pkt_id;
	switch (event)
	{
		case EVENT_LEFT_CLICK:
		case EVENT_KEYPRESS:
			tester__data.id_pkt_block = 1;
			break;
		case EVENT_LOST_FOCUS:
			tester__data.id_pkt_block = 0;
			break;
		case EVENT_VAL_CHANGED:
			tester__data.id_pkt_block = 0;
			GetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, &pkt_id);
			*(uint16_t*)&tester__data.tx_buff[1] = --pkt_id;
			break;
	}
	return 0;
}

int CVICALLBACK test_speed_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int test_on;
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(PANELHANDLE.id, PANEL_TEST_SPEED, &tester__data.test_speed);
			if (!tester__data.test_speed) {
				GetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, &test_on);
				if (test_on)
					sw_timer__start(&tester__data.udp_timer, tester__data.timeout, udp_tmr_cb, NULL);
			}

			break;
	}
	return 0;
}

int CVICALLBACK password_set (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char buff[3000];
	char p_new[1000] = {0};
	char p_old[1000] = {0};
	int len, tabpanel;
	panel_struct_t* p_panel;
	switch (event) {
		case EVENT_COMMIT:
			if (control != Autorize_ADD_CMD_BUTTON_2)
				break;
			eventData1 = 1280;
		case EVENT_KEYPRESS:
			if (eventData1 != 1280)
				break;

			GetCtrlVal(panel, Autorize_PASS_OLD, p_old);
			GetCtrlVal(panel, Autorize_PASSWORD, p_new);
			len = strlen(p_new);
			sprintf(buff, "%05d %05d ", 54, len + strlen(p_old));
			for(int i = 0; i < strlen(p_old); i++)
				sprintf(buff, "%s%d ", buff, p_old[i]);
			for(int i = 0; i < strlen(p_new); i++)
				sprintf(buff, "%s%d ", buff, p_new[i]);
			DisplayPanel (PANELHANDLE.id);
			p_panel = find_panel(panel);
			if (p_panel)
				p_panel->active = 0;
			HidePanel (panel);
			GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
			SetCtrlVal (tabpanel, TABPANEL_TRANSMIT, buff);
			break;
	}
	return 0;
}


static void print_timer_cb(struct sw_timer__t *timer, void *ext_data) {
	uint32_t len;

	sw_timer__start(timer, -1, print_timer_cb, NULL);
	SetCtrlVal(PANELHANDLE.id, PANEL_LOST_PKT, tester__data.all_pkt);
	SetCtrlVal(PANELHANDLE.id, PANEL_MSG_CNT, tester__data.err_pkt);
	SetCtrlVal(PANELHANDLE.id, PANEL_LOST_PKT, tester__data.all_pkt);
	if (tester__data.fw_size)
		SetCtrlVal(PANELHANDLE.id, PANEL_DOWNLOAD, tester__data.download_size * 100 / tester__data.fw_size);

	CRITICAL_SECTION_ON
	tester__data.cur_buf = received_buff;
	while(tester__data.pkt_num) {
		len = strlen(tester__data.cur_buf);
		SetCtrlVal(PANELHANDLE.id, PANEL_RECEIVE, tester__data.cur_buf);
        SetCtrlVal(PANELHANDLE.id, PANEL_RECEIVE, "\n");
		tester__data.cur_buf += len + 1;
		tester__data.pkt_num--;
	}
	tester__data.cur_buf = received_buff;
	CRITICAL_SECTION_OFF
}


int CVICALLBACK sc_ch_change_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int temp;
	int cur_ch;
	uint32_t cur_freq = 0;
	switch (event) {
		case EVENT_MARK_STATE_CHANGE:
			IsListItemChecked(panel, P_SYNCCLK_BOX_CH, eventData2, &temp);
			GetCtrlVal (panel, P_SYNCCLK_FREQ_SET, cur_freq);
			if (temp) {
				GetCtrlVal (panel, P_SYNCCLK_FREQ_CH, &cur_ch);
				sc_ch_tbl[eventData2].ch = cur_ch;
				sc_ch_tbl[eventData2].freq = cur_freq;
			}
			else {
				sc_ch_tbl[eventData2].ch = 0;
				sc_ch_tbl[eventData2].freq = 0;
			}
			sc_ch_renew();
			break;
		case EVENT_LEFT_CLICK:
			if (control != P_SYNCCLK_FREQ_CH)
				break;
			for(int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
				IsListItemChecked(panel, P_SYNCCLK_BOX_CH, i, &temp);
				if (temp) {
					GetCtrlVal (panel, P_SYNCCLK_FREQ_CH, &cur_ch);
					GetCtrlVal (panel, P_SYNCCLK_FREQ_SET, &temp);
					sc_ch_tbl[i].ch = cur_ch;
					sc_ch_tbl[i].freq = temp;
				}
			}
			break;
		case EVENT_VAL_CHANGED:
			if (control == P_SYNCCLK_FREQ_SET) {
				GetCtrlVal (panel, P_SYNCCLK_FREQ_SET, &cur_freq);
				GetCtrlVal (panel, P_SYNCCLK_FREQ_CH, &cur_ch);
				for(int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
					if (sc_ch_tbl[i].ch == cur_ch) {
						sc_ch_tbl[i].freq = cur_freq;
					}
				}
			}
			if (control != P_SYNCCLK_FREQ_CH)
				break;
			GetCtrlVal (panel, P_SYNCCLK_FREQ_CH, &cur_ch);
			for(int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
				if (sc_ch_tbl[i].ch == cur_ch) {
					CheckListItem(panel, P_SYNCCLK_BOX_CH, i, 1);
					if (!cur_freq) {
						cur_freq = sc_ch_tbl[i].freq;
						SetCtrlVal (panel, P_SYNCCLK_FREQ_SET, cur_freq);
					}
				}
				else
					CheckListItem(panel, P_SYNCCLK_BOX_CH, i, 0);
			}
			sc_ch_renew();
			break;
	}
	return 0;
}

static char* sc_group_tbl[255] = {NULL, "SYNC", "FDOM1", "FDOM2", "RUSER1", "RUSER2", "RUSER3", "RUSER4"};
void sc_ch_renew() {
	char buff[1000];
	int temp = 0;
	char* p_ch;
	int cur_ch;
	GetCtrlVal (SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_CH, &cur_ch);
	for(int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
		p_ch = sc_group_tbl[sc_ch_tbl[i].ch];
		CheckListItem (SYNCCLOCKPANEL.id, P_SYNCCLK_BOX_CH, i, (sc_ch_tbl[i].ch == cur_ch) ? 1 : 0);
		if ((cur_ch == sc_ch_tbl[i].ch) && (!temp))
			temp = sc_ch_tbl[i].freq;
		if (p_ch == NULL) {
			sprintf(buff, "канал%d", i);
			ReplaceListItem (SYNCCLOCKPANEL.id, P_SYNCCLK_BOX_CH, i, buff, i);
			continue;
		}
		sprintf(buff, "%d[%s]", i, p_ch);
		ReplaceListItem (SYNCCLOCKPANEL.id, P_SYNCCLK_BOX_CH, i, buff, i);
	}
	if (temp)
		SetCtrlVal(SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_SET, temp);
}

static void sc_ch_set_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	char buff[3000];
	int tabpanel;
	int panel = (int)ext_data;
	uint8_t* freq;
 	int len = sizeof(sc_ch_tbl);
	sprintf(buff, "00130 %05d %05d %05d ", len + 4, DEV_SETTS__SYNCCLOCK, len);
	for(int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
		freq = &sc_ch_tbl[i].freq;
		sprintf(buff, "%s%05d %05d %02d %02d %02d %02d ", buff, sc_ch_tbl[i].ch, sc_ch_tbl[i].state, freq[0], freq[1], freq[2], freq[3]);
	}

	GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
	SetCtrlVal (tabpanel, TABPANEL_TRANSMIT, buff);
}

static void freq_set_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	char buff[3000];
	uint32_t freq;
	int tabpanel;
	int ch;
	int panel = (int)ext_data;
	GetCtrlVal(panel, P_SYNCCLK_FREQ_CH, &ch);
	sprintf(buff, "00420 00008 %1d 0 1 0 ", ch);
	GetCtrlVal(panel, P_SYNCCLK_FREQ_SET, &freq);
	for(int i = 0; i < 4; i++) {
		sprintf(buff, "%s%d ", buff, ((uint8_t*)&freq)[i]);
	}
	GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
	SetCtrlVal (tabpanel, TABPANEL_TRANSMIT, buff);
}

int CVICALLBACK freq_set_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	static sw_timer__t freq_set_tmr;
	int temp;
	switch (event) {
		case EVENT_COMMIT:
			if (control != P_SYNCCLK_SC_SET_BT)
				break;
			GetCtrlVal(SYNCCLOCKPANEL.id, P_SYNCCLK_SYNC_CH_ON, &temp);
			if (temp) {
				SetCtrlVal(PANELHANDLE.id, PANEL_Type, 0);
				sc_ch_change_cb (SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_SET, EVENT_VAL_CHANGED, NULL, 0, 0);
				sw_timer__start(&freq_set_tmr, 1, sc_ch_set_tmr_cb, (void*)SYNCCLOCKPANEL.id);
				return 0;
			}
			eventData1 = 1280;
		case EVENT_KEYPRESS:
			if (eventData1 != 1280)
				break;
				sw_timer__start(&freq_set_tmr, 1, freq_set_tmr_cb, (void*)panel);
			break;
	}
	return 0;
}

int CVICALLBACK draw_time_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	uint16_t time;
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(PANELHANDLE.id, PANEL_DRAW_TIME, &time);
			sw_timer__start(&tester__data.print_timer, time, print_timer_cb, NULL);
			break;
	}
	return 0;
}

int CVICALLBACK auto_id (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int auto_id;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PANELHANDLE.id, PANEL_AUTO_ID, &auto_id);
			SetCtrlAttribute (PANELHANDLE.id, PANEL_PKT_ID, ATTR_DIMMED, auto_id);
			tester__data.auto_id = (uint8_t)auto_id;
			break;
	}
	return 0;
}

int CVICALLBACK listen_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int listen;
	cur_settings_struct_t* p_sett = &tester__data.settings[tester__data.cur_panel];
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PANELHANDLE.id, PANEL_LISTEN, &listen);
			tester__data.listen = (uint8_t)listen;
			if (listen)
				CreateUDPChannelConfig(p_sett->port_in, UDP_ANY_ADDRESS, 0, UDPCallback, NULL, &tester__data.readerChannel);
			else
				DisposeUDPChannel(tester__data.readerChannel);
			break;
	}
	return 0;
}

int CVICALLBACK panel_switch (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int index;
	switch (event) {
		case EVENT_RIGHT_CLICK:
			GetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, &index);
			if (index) {
				index = 0;
				SetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, index);
				test_ctrl_cb(PANELHANDLE.id, PANEL_BINARYSWITCH, EVENT_COMMIT, NULL, 0, 0);
			}
			tester__data.state = STATE_ONE_TRANSMIT;
			sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, (void*)1);
			break;
		case EVENT_ACTIVE_TAB_CHANGE:
			tester__data.cur_panel = eventData2;
			break;
		case EVENT_LEFT_DOUBLE_CLICK:
			if (control != PANEL_TAB)
				break;
			DisplayPanel (TABDESCRPANEL.id);
			HidePanel(PANELHANDLE.id);
			break;
	}
	return 0;
}


uint32_t* transmit_get_state() {
	uint32_t* data = &tester__data.reg_oper.reg_buff;
	if (tester__data.state != STATE_IDLE) {
		Sleep(1);
		data = NULL;
	}
	return data;
}

void transmit_data(uint8_t* tx_buff, int len, uint8_t processor, tester__state_e state, triple_spi__oper_e oper) {
	int buff_on_panel_only = 0;
	uint16_t cmd, param_len;
	char buff[BUFF_SIZE * 20 + 128];
	int tabpanel, ch;
	uint16_t pkt_id;
	uint32_t freq;

	CRITICAL_SECTION_ON
	GetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, &pkt_id);
	*(uint16_t*)&tester__data.tx_buff[1] = pkt_id;
	if (tx_buff) {
		if (tester__data.reg_oper.cur_item) {
			GetCtrlVal(tester__data.reg_oper.menu, PANEL_MENU_ON_PANEL, &buff_on_panel_only);
			if(buff_on_panel_only) {
				cmd = *(uint16_t*)tx_buff;
				param_len = *(uint16_t*)&tx_buff[2];
				sprintf(buff, "%05d %05d ", cmd, param_len);
				for(int i = 0; i < param_len; i++) {
					sprintf(buff, "%s%d ", buff, tx_buff[4 + i]);
				}
				GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
				SetCtrlVal (tabpanel, TABPANEL_TRANSMIT, buff);
			}
		}
		if (buff_on_panel_only == 0) {
			memcpy(&tester__data.tx_buff[3], tx_buff, len);
			tester__data.tx_len = len + 3;
			*tester__data.tx_buff = processor;
			tester__data.state = state;
			sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, (void*)0xF001);
		}
	}
	else {
		SetCtrlVal(PANELHANDLE.id, PANEL_TEST_SPEED, 0);
		GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
		if (tester__data.state == STATE_FREQ_CYCLE) {
			GetCtrlVal(SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_CH, &ch);
			sprintf(buff, "00420 00008 %1d 0 1 0 ", ch);
			GetCtrlVal(SYNCCLOCKPANEL.id, P_SYNCCLK_FREQ_SET, &freq);
			for(int i = 0; i < 4; i++) {
				sprintf(buff, "%s%d ", buff, ((uint8_t*)&freq)[i]);
			}
		}
		else
			GetCtrlVal(tabpanel, TABPANEL_TRANSMIT, buff);
		GetCtrlVal(PANELHANDLE.id, PANEL_Type, &tabpanel);
		*tester__data.tx_buff = (uint8_t)tabpanel;
		tester__data.tx_len = buff_dec_to_char(buff, (char*)&tester__data.tx_buff[3]) + 3;
	}
	CRITICAL_SECTION_OFF
}



static void led_timer_cb(struct sw_timer__t *timer, void *ext_data) {
	int index;
	SetCtrlVal(PANELHANDLE.id, PANEL_TRANSFET_LED, 0);
	if (tester__data.test_timeout) {
		index = 0;
		SetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, index);
		test_ctrl_cb(PANELHANDLE.id, PANEL_BINARYSWITCH, EVENT_COMMIT, NULL, 0, 0);
	}
}

int CVICALLBACK cmd_add_list (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char buff[3000];
	int res, tabpanel;
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(PANELHANDLE.id, PANEL_Type_2, &res);
			GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
			switch(res) {
				case 1:
					DisplayPanel (AUTORIZEPANEL.id);
					HidePanel(PANELHANDLE.id);
					break;
				case 2:
					sprintf(buff, "00055 00017 65 68 77 73 78 73 83 84 82 65 84 79 82 82 88 73 01 ");
					SetCtrlVal (tabpanel, TABPANEL_TRANSMIT, buff);
					break;
				case 3:
					sprintf(buff, "00145 00000 ");
					SetCtrlVal (tabpanel, TABPANEL_TRANSMIT, buff);
					break;
			}
			break;
	}
	return 0;
}

int CVICALLBACK cmd_menu_ctrl_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int res;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &res);
			if (res) {
				COMMANDPANEL.active = 1;
				DisplayPanel (COMMANDPANEL.id);
			}
			else {
				COMMANDPANEL.active = 0;
				HidePanel(COMMANDPANEL.id);
			}
			break;
	}
	return 0;
}

int CVICALLBACK cmd_add_list_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char buff[3000];
	int item_idx;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlIndex (panel, CMD_PANEL_LISTBOX, &item_idx);
			GetCtrlVal(panel, CMD_PANEL_CMD_NAME, buff);
			GetCtrlVal(panel, CMD_PANEL_CMD_DATA, &buff[100]);
			InsertListItem (panel, CMD_PANEL_LISTBOX, item_idx + 1, buff, &buff[100]);
			break;
	}
	return 0;
}

int CVICALLBACK cmd_del_list_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int item_idx;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlIndex (panel, CMD_PANEL_LISTBOX, &item_idx);
			DeleteListItem (panel, CMD_PANEL_LISTBOX, item_idx, 1);
			break;
	}
	return 0;
}

int CVICALLBACK list_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char buff[3000];
	int tabpanel;
	switch (event) {
		case EVENT_COMMIT:
			GetValueFromIndex (panel, control, eventData1, buff);
			GetPanelHandleFromTabPage(PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, &tabpanel);
			SetCtrlVal (tabpanel, TABPANEL_TRANSMIT, buff);
			break;
		case EVENT_VAL_CHANGED:
			GetLabelFromIndex (panel, control, eventData1, buff);
			SetCtrlVal (panel, CMD_PANEL_CMD_NAME, buff);
			GetValueFromIndex (panel, control, eventData1, buff);
			SetCtrlVal (panel, CMD_PANEL_CMD_DATA, buff);
			break;
	}
	return 0;
}

int CVICALLBACK tab_descr_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char buff[100];
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, PANEL_DESC_CMD_NAME, buff);
			DisplayPanel (PANELHANDLE.id);
			HidePanel(TABDESCRPANEL.id);
			if (strlen(buff))
				SetTabPageAttribute (PANELHANDLE.id, PANEL_TAB, tester__data.cur_panel, ATTR_LABEL_TEXT, buff);
			break;
	}
	return 0;
}

int CVICALLBACK settings_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char path[MAX_PATHNAME_LEN] = {"*.ini"};
	switch (event) {
		case EVENT_COMMIT:
			if (control == PANEL_ADD_SAVE_BUTTON) {
				if(FileSelectPopup("", path,  "Settings files (*.ini);", "Save settings", VAL_SAVE_BUTTON, 0, 1, 1, 1, path) == 0)
					return 0;
				tester__deinit(path);
				break;
			}

			if(FileSelectPopup("", path,  "Settings files (*.ini);", "Open settings", VAL_LOAD_BUTTON, 0, 1, 1, 1, path) == 0)
				return 0;
			tester__init(path);
			break;
	}
	return 0;
}

int CVICALLBACK hex_dec_ctrl_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int buff;
	int format = VAL_DECIMAL_FORMAT, color = VAL_BLACK;
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlVal(panel, control, &buff);
			if (buff) {
				format = VAL_HEX_FORMAT;
				color = VAL_BLUE;

			}
			for(int i = 0; i < (sizeof(cmd_area_tbl) / sizeof(cmd_area_tbl[0])); i++) {
				SetCtrlAttribute (panel, cmd_area_tbl[i], ATTR_FORMAT, format);
				SetCtrlAttribute (panel, cmd_area_tbl[i], ATTR_TEXT_COLOR, color);
			}
			break;
	}
	return 0;
}

int CVICALLBACK freq_test_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int buff;

	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &buff);
			if (buff) {
				tester__data.state = STATE_FREQ_CYCLE;
				SetCtrlAttribute (panel, P_SYNCCLK_FREQ_SET, ATTR_DIMMED, 1);
				SetCtrlAttribute (panel, P_SYNCCLK_FREQ_CH, ATTR_DIMMED, 1);
			}
			else {
				tester__data.state = STATE_IDLE;
				SetCtrlAttribute (panel, P_SYNCCLK_FREQ_SET, ATTR_DIMMED, 0);
				SetCtrlAttribute (panel, P_SYNCCLK_FREQ_CH, ATTR_DIMMED, 0);
			}


			break;
	}
	return 0;
}




typedef struct {
	uint16_t cmd_num;
	uint8_t reserved;
}cmd_num__struct_t;

static cmd_num__struct_t cmd_num_tbl[65536] = {{.reserved = 1}};

static uint16_t find_cmd_num(uint16_t cmd_num) {
	uint16_t first = 0, last = 65535;
	while (first + 1 < last)                                               // Бинарный поиск
		if (cmd_num > cmd_num_tbl[(first + ((last - first) / 2))].cmd_num)
			first += (last - first) / 2;
		else
			last -= (last - first) / 2;
	return (cmd_num_tbl[last].cmd_num == cmd_num) ? last : first;  // Ищем ближайшую частоту
}



#include "commands.h"
static FILE* f_cmd_tbl;
static char file_include[MAX_PATHNAME_LEN];
static void cmd__search(int panel, FILE* file);
static char* cmd__file_search(FILE* file);

int CVICALLBACK cmd_num_list_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char path[MAX_PATHNAME_LEN] = {"*.h"};
	FILE* cmd_file, *next_file;
	int temp;
	char buff[1024], str_buff[200];
	char path_folder[MAX_PATHNAME_LEN];

	switch (event) {
		case EVENT_COMMIT:
			if(FileSelectPopup("", path,  "RXI_Ethernet (*.h);", "Open RXI_Ethernet commands", VAL_LOAD_BUTTON, 0, 1, 1, 1, path) == 0)
				return 0;
			strcpy(path_folder, path);
			for(int i = strlen(path); (i > 0) && (path_folder[i] != '\\'); i--)
				path_folder[i] = 0x00;

			cmd_file = fopen(path, "rb");
			if (cmd_file == NULL) {
				return -1;
			}
			f_cmd_tbl = fopen("cmd_tbl.c", "w");
			if (f_cmd_tbl) {
				sprintf(str_buff, "#include \"commands.h\"\n");
				fputs (str_buff, f_cmd_tbl);
				sprintf(str_buff, "commands__struct_t cmd_tbl[] = {\n\t");
				fputs (str_buff, f_cmd_tbl);
			}
			GetNumListItems (panel, CMD_PANEL_LISTBOX_CMD_NUM, &temp);
			if (temp)
				DeleteListItem (panel, CMD_PANEL_LISTBOX_CMD_NUM, 0, temp);
			for(uint16_t i = 1; i > 0; i++) {
				cmd_num_tbl[i].cmd_num = i;
				cmd_num_tbl[i].reserved = 0;
			}
			cmd__search(panel, cmd_file);
			fclose(cmd_file);
			cmd_file = fopen(path, "rb");
			if (cmd_file != NULL) {
				while(cmd__file_search(cmd_file)) {
					strcpy(buff, path_folder);
					strcat(buff, file_include);
					next_file = fopen(buff, "rb");
					if (next_file)
						cmd__search(panel, next_file);
					fclose(next_file);
				}
			}


			for(int i = 0; i < 65536; i++) {
				if (cmd_num_tbl[i].reserved == 0) {
					sprintf(buff, "0x%04x", cmd_num_tbl[i].cmd_num);
					InsertListItem (panel, CMD_PANEL_LISTBOX_CMD_NUM, -1, buff, "");
				}
			}

			if (f_cmd_tbl) {
				sprintf(str_buff, "{NULL, 0, 0, 0}};\n");
				fputs (str_buff, f_cmd_tbl);
			}

			if (f_cmd_tbl)
				fclose(f_cmd_tbl);
			fclose(cmd_file);

			break;
	}
	return 0;
}


static char* cmd__file_search(FILE* file) {
	char buff[1024];
	char* cur_str_num = NULL;
	while (fgets(buff, 1024, file)) {
		if(strstr(buff, "#include")) {
			if ((cur_str_num = strstr(buff, "\""))) {
				for(int i = strlen(cur_str_num); (i > 0) && (cur_str_num[i] != '\"'); i--)
					cur_str_num[i] = 0x00;
				cur_str_num[strlen(cur_str_num) - 1] = 0x00;
				cur_str_num++;
				strcpy(file_include, cur_str_num);
				return file_include;
			}
		}
	}
	return NULL;
}

static void cmd__search(int panel, FILE* file) {
	char buff[1024], str_buff[200];
	commands__struct_t cur_cmd;
	char* cur_str_num = NULL;
	uint8_t param_find = 0;
	int cur_num = 0, p_cycle = 0, cur_proc = IO_PROCESSOR;
	memset(&cur_cmd, 0, sizeof(cur_cmd));
	while(fgets(buff, 1024, file)) {
		if(strstr(buff, "CMD_Commands")) {
			cur_proc = CMD_PROCESSOR;
		}
		if(strstr(buff, "IO_Commands")) {
			cur_proc = IO_PROCESSOR;
		}
		if(strstr(buff, "#define")) {
			if (f_cmd_tbl) {
				if ((cur_str_num = strstr(buff, "CMD__"))) {
					param_find = 1; p_cycle = 0;
					memset(&cur_cmd, 0, sizeof(cur_cmd));
					cur_str_num += 5;
					memset(str_buff, 0, 200);
					*str_buff = '{';
					str_buff[1] = '"';
					for(int i = 2; *cur_str_num != ' '; i++, cur_str_num++)
						str_buff[i] = *cur_str_num;
					fputs (str_buff, f_cmd_tbl);
					fputs ("\", ", f_cmd_tbl);

				}
			}
			cur_str_num = strstr(buff, "0x");
			if (cur_str_num) {
				sscanf(cur_str_num + 2, "%x", &cur_num);
				cur_cmd.cmd_id = (uint16_t)cur_num;
				cur_cmd.proc_type = (uint16_t)cur_proc;
				if (cmd_num_tbl[cur_num].reserved) {
					sprintf(buff, "Err %04x", cmd_num_tbl[cur_num].cmd_num);
					SetCtrlAttribute (panel, CMD_PANEL_LISTBOX_CMD_NUM, ATTR_TEXT_COLOR, VAL_RED);
					InsertListItem (panel, CMD_PANEL_LISTBOX_CMD_NUM, -1, buff, "");
				}
				cmd_num_tbl[cur_num].reserved = 1;
			}
		}
		if (f_cmd_tbl) {
			switch(param_find) {
				case 1:
					if(strstr(buff, "typedef struct"))
						p_cycle++;
					if(strstr(buff, "}")) {
						p_cycle--;
						if (p_cycle <= 0) {
							param_find = 2;
							break;
						}
					}
					if(strstr(buff, "uint8_t")) {
						cur_cmd.p_types[cur_cmd.pin_num] = ui8;
						cur_cmd.pin_num++;
					}
					if(strstr(buff, "uint16_t")) {
						cur_cmd.p_types[cur_cmd.pin_num] = ui16;
						cur_cmd.pin_num++;
					}
					if(strstr(buff, "uint32_t")) {
						cur_cmd.p_types[cur_cmd.pin_num] = ui32;
						cur_cmd.pin_num++;
					}
					break;
				case 2:
					if(strstr(buff, "typedef struct"))
						p_cycle++;
					if(strstr(buff, "}")) {
						p_cycle--;
						if (p_cycle <= 0) {
							param_find = 0;
							sprintf(str_buff, "%d, %d, %d, %d", cur_cmd.cmd_id, cur_cmd.proc_type, cur_cmd.pin_num, cur_cmd.pout_num);
							fputs (str_buff, f_cmd_tbl);
							for(int p = 0; p < cur_cmd.pin_num; p++) {
								sprintf(str_buff, ", %d", cur_cmd.p_types[p]);
								fputs (str_buff, f_cmd_tbl);
							}
							for(int p = 0; p < cur_cmd.pout_num; p++) {
								sprintf(str_buff, ", %d", cur_cmd.p_types[cur_cmd.pin_num + p]);
								fputs (str_buff, f_cmd_tbl);
							}
							fputs ("},\n\t", f_cmd_tbl);
							break;
						}
					}
					if(strstr(buff, "uint8_t")) {
						cur_cmd.p_types[cur_cmd.pin_num + cur_cmd.pout_num] = ui8;
						cur_cmd.pout_num++;
					}
					if(strstr(buff, "uint16_t")) {
						cur_cmd.p_types[cur_cmd.pin_num + cur_cmd.pout_num] = ui16;
						cur_cmd.pout_num++;
					}
					if(strstr(buff, "uint32_t")) {
						cur_cmd.p_types[cur_cmd.pin_num + cur_cmd.pout_num] = ui32;
						cur_cmd.pout_num++;
					}
					break;
				default:
					break;
			}
		}
	}
}


static panel_struct_t* find_panel(int panel) {
	panel_struct_t* res = NULL;
	for (int i = 0; i < MENU_ITEM_MAX; i++)
		if (panel_menu[i].id == panel) {
			res = &panel_menu[i];
			break;
		}
	return res;
}

int CVICALLBACK panel_menu_quit_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	panel_struct_t* p_panel;
	switch (event) {
		case EVENT_COMMIT:
			p_panel = find_panel(panel);
			if (p_panel)
				p_panel->active = 0;
			HidePanel(panel);
			break;
	}
	return 0;
}

int CVICALLBACK panel_menu_add_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int cur_item;
	switch (event) {
		case EVENT_COMMIT:
			cur_item = menu_box__get_item_num(panel, MENU_BOX__ACTIVE_ITEM) + 1;
			menu_box__add_item(panel, cur_item, 0, 0);
			break;
	}
	return 0;
}

int CVICALLBACK panel_menu_del_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int cur_item;
	switch (event) {
		case EVENT_COMMIT:
			cur_item = menu_box__get_item_num(panel, MENU_BOX__ACTIVE_ITEM);
			menu_box__del_item(panel, cur_item);
			break;
	}
	return 0;
}





void CVICALLBACK menu_ext_if_cb (int menuBar, int menuItem, void *callbackData, int panel) {


}


void CVICALLBACK menu_add_clock_cb (int menuBar, int menuItem, void *callbackData, int panel) {

	ADDCLOCKPANEL.active = 1;
	DisplayPanel (ADDCLOCKPANEL.id);
	add_clk_spi_reg_cb(ADDCLOCKPANEL.id, P_ADDCLOCK_ADD_CLK_SPI_TBL, EVENT_VAL_CHANGED, NULL, 0, 0);
}

int CVICALLBACK panel_add_clock_ad9139_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			panel_menu[0].active = 1;
			DisplayPanel (panel_menu[0].id);

			break;
	}
	return 0;
}

int CVICALLBACK panel_add_clock_hmc7044_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			panel_menu[1].active = 1;
			DisplayPanel (panel_menu[1].id);
			break;
	}
	return 0;
}

void main_sleep(int ms) {
	Sleep(ms);
}

long main__round(double x) {
	return RoundRealToNearestInteger(x);
}


int CVICALLBACK panel_menu_read_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			reg_oper_set(&panel, TSPI__OPER_READ);
			break;
	}
	return 0;
}

int CVICALLBACK panel_menu_write_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			reg_oper_set(&panel, TSPI__OPER_WRITE);
			break;
	}
	return 0;
}

static events__e reg_oper_set(int* p_menu, triple_spi__oper_e oper) {
	int item_idx;
	int panel = tester__data.reg_oper.menu;
	if (oper == TSPI__OPER_NONE) {
		tester__data.reg_oper.cur_item = NULL;
		return EVENT__OK;
	}
	if (tester__data.reg_oper.cur_item)
		return EVENT__BUSY;
	if (p_menu)
		panel = *p_menu;
	item_idx = menu_box__get_item_num(panel, MENU_BOX__ACTIVE_ITEM);
	tester__data.reg_oper.menu = panel;
	tester__data.reg_oper.oper = oper;
	tester__data.reg_oper.item_idx = item_idx;
	tester__data.reg_oper.item_max = item_idx + menu_box__get_item_num(panel, MENU_BOX__SEL_ITEM) - 1;
	tester__data.reg_oper.cur_item = menu_box__get_item(panel, item_idx);
	tester__data.download_size = 0;
	tester__data.fw_size = tester__data.reg_oper.item_max;
	return EVENT__OK;
}

void CVICALLBACK sw_data_cb (int menuBar, int menuItem, void *callbackData,int panel) {
	char path[MAX_PATHNAME_LEN] = {"*.bin"};
	char buff[BUFF_SIZE * 20 + 128];
	uint16_t pkt_id = 0;
	int index = 0;
	if (transmit_get_state() == NULL)
		return;

	if(FileSelectPopup("", path,  "Firmware files (*.bin);", "Open firmware", VAL_LOAD_BUTTON, 0, 1, 1, 1, path) == 0)
		return;
	tester__data.f_fw_upd = fopen(path, "rb");
	FileExists (path, &tester__data.fw_size);
	if ((tester__data.f_fw_upd == NULL) || (tester__data.fw_size < 0)) {
		sprintf(buff, "Не удалось открыть файл\n");
		SetCtrlVal (PANELHANDLE.id, PANEL_RECEIVE, buff);
		return;
	}
	tester__data.fw_upd_pkt = 0;
	tester__data.download_size = 0;
	tester__data.tx_len = 3;
	for(int i = 3; i < 1152; i+= index + 8) {
		index = fread (&tester__data.tx_buff[i+8], 1, 184, tester__data.f_fw_upd);
		tester__data.download_size += index;
		if (index == 0) {
			if (i == 3) {
				sprintf(buff, "Файл пуст\n");
				SetCtrlVal (PANELHANDLE.id, PANEL_RECEIVE, buff);
				CRITICAL_SECTION_ON
				fclose(tester__data.f_fw_upd);
				tester__data.f_fw_upd = NULL;
				CRITICAL_SECTION_OFF
				return;
			}
			else
				break;
		}
		else
			tester__data.tx_len += index + 8;
		*(uint16_t*)&tester__data.tx_buff[i+2] = (uint16_t)index + 4;

		*(uint32_t*)&tester__data.tx_buff[i+4] = tester__data.fw_upd_pkt;
		tester__data.fw_upd_pkt++;
		GetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, &pkt_id);
		*(uint16_t*)&tester__data.tx_buff[i] = 0x18;
	}
	*tester__data.tx_buff = IO_PROCESSOR;
	*(uint16_t*)&tester__data.tx_buff[1] = pkt_id;

	GetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, &index);
	if (index) {
		index = 0;
		SetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, index);
		test_ctrl_cb(PANELHANDLE.id, PANEL_BINARYSWITCH, EVENT_COMMIT, NULL, 0, 0);
	}
	tester__data.state = STATE_FW_UPD;
	sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, (void*)1);
}

void CVICALLBACK fw_sub_upd_cb(int menubar, int menuItem, void *callbackData, int panel) {
	FWUPDPANEL.active = 1;
	DisplayPanel(FWUPDPANEL.id);
}

int CVICALLBACK fw_upd_quit_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	panel_struct_t* p_panel;
	switch (event) {
		case EVENT_COMMIT:
			sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, NULL);
			p_panel = find_panel(panel);
			if (p_panel)
				p_panel->active = 0;
			HidePanel(panel);
			break;
	}
	return 0;
}

static uint8_t cmd_buff[512];
static FILE* fw_upd_sub_file;

int CVICALLBACK fw_upd_num_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			if (transmit_get_state() == NULL)
				return 0;
			*(uint16_t*)cmd_buff = 0x0019;		// fw_num
			*(uint16_t*)&cmd_buff[2] = 0;		// len
			tester__data.one_cmd_buff.panel = panel;
			transmit_data(cmd_buff, 4, IO_PROCESSOR, STATE_ONE_CMD, 0);

//			*(uint32_t*)tester__data.one_cmd_buff.data = 4;
//			sw_timer__start(&tester__data.one_cmd_tmr, 200, one_cmd_tmr_cb, NULL);
			break;
	}
	return 0;
}

static void one_cmd_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	char buff[255];
	cmd_struct_t* cmd = &tester__data.one_cmd_buff;
	int fw_num;
	char item_name[100];
	int temp = 0;
	SetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, temp);
	test_ctrl_cb(PANELHANDLE.id, PANEL_BINARYSWITCH, EVENT_COMMIT, NULL, 0, 0);
	switch(cmd->cmd) {
		case CMD__SETTINGS_WR:

			break;
		case CMD__SETTINGS_RD:
			if (tester__data.one_cmd_buff.panel == SYNCCLOCKPANEL.id) {
				if (*(uint16_t*)&cmd->data[2] != sizeof(sc_ch_tbl))
					break;
				memcpy(sc_ch_tbl, &cmd->data[4], sizeof(sc_ch_tbl));
				sc_ch_renew();
			}
			else if (tester__data.one_cmd_buff.panel == FDEBUGPANEL.id) {
				if ((*(uint16_t*)&cmd->data[2] != sizeof(fdebug__struct_t)) || (!tester__data.one_cmd_buff.ext_data))
					break;
				memcpy(tester__data.one_cmd_buff.ext_data, &cmd->data[4], sizeof(fdebug__struct_t));
				fdebug__renew();
			}
			break;
		case 0x0013:
			sprintf(item_name, "%d.%d.%d", cmd->data[0], cmd->data[1], *(uint16_t*)&cmd->data[2]);
			SetCtrlVal(PANELHANDLE.id, PANEL_CTRL_FW_VER, item_name);
			if (strlen((char*)&cmd->data[4]) < 30)
				SetCtrlVal(PANELHANDLE.id, PANEL_IDN_CTRL, &cmd->data[4]);
			memset(&buff[88], 0, 41);
			for(int i = 88; i < 128; i++) {
				if ((cmd->data[i] >= 0x30) && (cmd->data[i] < 0x67))
					buff[i]	= cmd->data[i];
			}
			SetCtrlVal(PANELHANDLE.id, PANEL_CTRL_FW_HASH, &buff[88]);
			break;
		case 0x0019:		// fw_num
			SetCtrlAttribute (cmd->panel, P_FW_UPD_B_FW_SUB_UPD, ATTR_DIMMED, 1);
			ClearListCtrl (cmd->panel, P_FW_UPD_SUB_MODUL);
			fw_num = *(uint16_t*)cmd->data;
			tester__data.fw_upd_pkt = 0;
			for(int i = 0; i < fw_num; i++) {
				sprintf(item_name, "Submodul %d", i);
				InsertListItem (cmd->panel, P_FW_UPD_SUB_MODUL, i, item_name, 0);
			}
			if (fw_num) {
				SetCtrlAttribute (cmd->panel, P_FW_UPD_B_FW_SUB_UPD, ATTR_DIMMED, 0);
			}
			*(uint16_t*)cmd_buff = 0x001A;						// fw_ver
			*(uint16_t*)&cmd_buff[2] = 2;						// len
			*(uint16_t*)&cmd_buff[4] = (uint16_t)tester__data.fw_upd_pkt; // номер сабмодуля
			transmit_data(cmd_buff, 6, IO_PROCESSOR, STATE_ONE_CMD, 0);
			break;
		case 0x001A:      // fw_ver
			tester__data.fw_upd_pkt++;
			GetNumListItems(cmd->panel, P_FW_UPD_SUB_MODUL, &fw_num);
			if (tester__data.fw_upd_pkt <= fw_num) {
				ReplaceListItem (cmd->panel, P_FW_UPD_SUB_MODUL, tester__data.fw_upd_pkt - 1, (char*)&cmd->data[4], cmd->data[0] | (cmd->data[1] << 8) | (*(uint16_t*)&cmd->data[2] << 16));
				if (tester__data.fw_upd_pkt == 1) {
					GetValueFromIndex(cmd->panel, P_FW_UPD_SUB_MODUL, 0, &temp);
					sprintf(item_name, "Версия прошивки: %d.%d.%d", temp & 0xFF, (temp >> 8) & 0xFF, temp >> 16);
					SetCtrlVal(cmd->panel, P_FW_UPD_CTRL_FW_VER, item_name);
				}
				if (tester__data.fw_upd_pkt < fw_num) {
					*(uint16_t*)cmd_buff = 0x001A;						// fw_ver
					*(uint16_t*)&cmd_buff[2] = 2;						// len
					*(uint16_t*)&cmd_buff[4] = (uint16_t)tester__data.fw_upd_pkt; // номер сабмодуля
					transmit_data(cmd_buff, 6, IO_PROCESSOR, STATE_ONE_CMD, 0);
				}
			}
			break;
		case 0x001B:
			if (tester__data.one_cmd_buff.ext_data) {  // fw_stop
				sprintf(buff, "Перезагрузка...\n", buff);
				SetCtrlVal(PANELHANDLE.id, PANEL_RECEIVE, buff);
				SetCtrlVal(PANELHANDLE.id, PANEL_RECEIVE, "\n");
				break;
			}
			fw_upd__begin(); // fw_start
			break;
		default:
			break;
	}
}

int CVICALLBACK fw_upd_exec_button_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char path[MAX_PATHNAME_LEN] = {"*.bin"};
	int fw_num = 0;
	switch (event) {
		case EVENT_COMMIT:
			if (transmit_get_state() == NULL)
				return -1;
			GetNumListItems(panel, P_FW_UPD_SUB_MODUL, &fw_num);
			if (fw_num == 0)
				return -1;
			if(FileSelectPopup("", path,  "Firmware files (*.*);", "Open firmware", VAL_LOAD_BUTTON, 0, 1, 1, 1, path) == 0)
				return -1;
			fw_upd_sub_file = fopen(path, "rb");
			FileExists (path, &tester__data.fw_size);
			if ((fw_upd_sub_file == NULL) || (tester__data.fw_size < 0)) {
				SetCtrlVal (PANELHANDLE.id, PANEL_RECEIVE, "Не удалось открыть файл\n");
				return -1;
			}
			GetCtrlIndex(panel, P_FW_UPD_SUB_MODUL, &fw_num);
			tester__data.fw_upd_pkt = fw_num;
			*(uint16_t*)cmd_buff = 0x001B;		// fw_ctrl
			*(uint16_t*)&cmd_buff[2] = 4;		// len
			*(uint16_t*)&cmd_buff[4] = (uint16_t)fw_num;	// номер сабмодуля
			*(uint16_t*)&cmd_buff[6] = 0;		// start
			tester__data.one_cmd_buff.panel = panel;
			tester__data.one_cmd_buff.ext_data = NULL;
			transmit_data(cmd_buff, 8, IO_PROCESSOR, STATE_ONE_CMD, 0);
			break;
	}
	return 0;
}


static void fw_upd__begin() {
	char buff[BUFF_SIZE * 20 + 128];
	uint16_t pkt_id = 0;
	int index = 0;
	tester__data.f_fw_upd = fw_upd_sub_file;
	tester__data.download_size = 0;
	tester__data.tx_len = 3;
	for(int i = 3; i < 1152; i+= index + 8) {
		index = fread (&tester__data.tx_buff[i+8], 1, 184, tester__data.f_fw_upd);
		tester__data.download_size += index;
		if (index == 0) {
			if (i == 3) {
				sprintf(buff, "Файл пуст\n");
				SetCtrlVal (PANELHANDLE.id, PANEL_RECEIVE, buff);
				CRITICAL_SECTION_ON
				fclose(tester__data.f_fw_upd);
				tester__data.f_fw_upd = NULL;
				CRITICAL_SECTION_OFF
				return;
			}
			else
				break;
		}
		else
			tester__data.tx_len += index + 8;
		*(uint16_t*)&tester__data.tx_buff[i+2] = (uint16_t)index + 4;
		*(uint16_t*)&tester__data.tx_buff[i+4] = (uint16_t)tester__data.fw_upd_pkt;
		*(uint16_t*)&tester__data.tx_buff[i+6] = (uint16_t)index;
		GetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, &pkt_id);
		*(uint16_t*)&tester__data.tx_buff[i] = 0x1C;
	}
	*tester__data.tx_buff = IO_PROCESSOR;
	*(uint16_t*)&tester__data.tx_buff[1] = pkt_id;

	GetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, &index);
	if (index) {
		index = 0;
		SetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, index);
		test_ctrl_cb(PANELHANDLE.id, PANEL_BINARYSWITCH, EVENT_COMMIT, NULL, 0, 0);
	}
	tester__data.state = STATE_FW_UPD_SUB;
	sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, (void*)1);
}

int CVICALLBACK fw_sub_idx_change_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int temp;
	char item_name[100];
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlIndex(panel, P_FW_UPD_SUB_MODUL, &temp);
			GetValueFromIndex(panel, P_FW_UPD_SUB_MODUL, temp, &temp);
			sprintf(item_name, "Версия прошивки: %d.%d.%d", temp & 0xFF, (temp >> 8) & 0xFF, temp >> 16);
			SetCtrlVal(panel, P_FW_UPD_CTRL_FW_VER, item_name);
			break;
	}
	return 0;
}

void CVICALLBACK calib_sync_clock_cb (int menuBar, int menuItem, void *callbackData, int panel) {
	char buff[BUFF_SIZE * 20 + 128];
	uint16_t pkt_id;
	FILE* f_cal;
	int index;
	char path[MAX_PATHNAME_LEN] = {"*.txt"};

	if(FileSelectPopup("", path,  "Calibration files (*.txt);", "Open calibration file", VAL_LOAD_BUTTON, 0, 1, 1, 1, path) == 0)
		return;
	f_cal = fopen(path, "r");
	if (f_cal == NULL) {
		sprintf(buff, "Не удалось открыть файл\n");
		SetCtrlVal (PANELHANDLE.id, PANEL_RECEIVE, buff);
		return;
	}

	for(index = 0; fgets(buff, 255, f_cal); index++)
		sscanf(buff, "%" SCNd32 "%" SCNd8 "%" SCNd8 "%" SCNd8 "%" SCNd8 "%" SCNd8 "%" SCNd8 "", &p_cal__tbl[index].freq, &p_cal__tbl[index].oscil1_bank, &p_cal__tbl[index].oscil1_d_delay, &p_cal__tbl[index].oscil1_a_delay, &p_cal__tbl[index].oscil2_bank, &p_cal__tbl[index].oscil2_d_delay, &p_cal__tbl[index].oscil3_a_delay);

	while(index % 8) {
		sscanf(buff, "%" SCNd32 "%" SCNd8 "%" SCNd8 "%" SCNd8 "%" SCNd8 "%" SCNd8 "%" SCNd8 "", &p_cal__tbl[index].freq, &p_cal__tbl[index].oscil1_bank, &p_cal__tbl[index].oscil1_d_delay, &p_cal__tbl[index].oscil1_a_delay, &p_cal__tbl[index].oscil2_bank, &p_cal__tbl[index].oscil2_d_delay, &p_cal__tbl[index].oscil3_a_delay);
		index++;
	}
	p_cal__tbl[index].freq = 0xFFFFFFFF;
	fclose(f_cal);
	tester__data.state = STATE_NORMAL;
	tester__data.calib_pkt = 0;
	tester__data.p_cal = p_cal__tbl;
	GetCtrlVal(PANELHANDLE.id, PANEL_PKT_ID, &pkt_id);
	*tester__data.tx_buff = 1;
	*(uint16_t*)&tester__data.tx_buff[3] = 0x81;
	*(uint16_t*)&tester__data.tx_buff[1] = pkt_id;
	if (tester__data.p_cal->freq == 0xFFFFFFFF) {
		tester__data.tx_len = buff_dec_to_char(buff, (char*)&tester__data.tx_buff[3]) + 3;
	}
	else {
		*(uint16_t*)&tester__data.tx_buff[7] = tester__data.calib_pkt;
		tester__data.tx_len = 9;
		for(int i = 0; i < 8 && (tester__data.p_cal->freq != 0xFFFFFFFF); i++) {
			memcpy(&tester__data.tx_buff[9] + i * sizeof(param__t), tester__data.p_cal, sizeof(param__t));
			tester__data.p_cal++;
			tester__data.tx_len += sizeof(param__t);
		}
		*(uint16_t*)&tester__data.tx_buff[5] = (uint16_t)tester__data.tx_len - 7;
		GetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, &index);
		if (index) {
			index = 0;
			SetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, index);
			test_ctrl_cb(PANELHANDLE.id, PANEL_BINARYSWITCH, EVENT_COMMIT, NULL, 0, 0);
		}
		tester__data.state = STATE_CALIBRATION;
		sw_timer__start(&tester__data.reg_tmr, 1, reg_tmr_cb, (void*)1);
	}
}

int CVICALLBACK fw_ver_button_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			*(uint16_t*)cmd_buff = 0x0013;		// idn_req
			*(uint16_t*)&cmd_buff[2] = 0;		// len
			tester__data.one_cmd_buff.panel = panel;
			transmit_data(cmd_buff, 4, IO_PROCESSOR, STATE_ONE_CMD, 0);
			break;
	}
	return 0;
}

int CVICALLBACK add_clk_spi_reg_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int temp;
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlIndex(panel, P_ADDCLOCK_ADD_CLK_SPI_TBL, &temp);
			SetCtrlVal(panel, P_ADDCLOCK_ADD_CLK_REG_ADDR, add_clk_spi_get_param(temp));
			break;
	}
	return 0;
}

int CVICALLBACK add_clk_spi_reg_set_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int temp, data;
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlVal(panel, P_ADDCLOCK_ADD_CLK_REG_ADDR, &data);
			GetCtrlIndex(panel, P_ADDCLOCK_ADD_CLK_SPI_TBL, &temp);
			add_clk_spi_set_param(temp, data);
			break;
	}
	return 0;
}

uint32_t add_clk_spi_get_param(int param) {
	uint32_t res = 0;
	ext_spi_t* add_clk_spi_tbl = triple_spi__add_clock_get_tbl();
	switch(param) {
		case 0:
			res = (uint32_t)add_clk_spi_tbl->write;
			break;
		case 1:
			res = (uint32_t)add_clk_spi_tbl->read;
			break;
		case 2:
			res = (uint32_t)add_clk_spi_tbl->ctrl;
			break;
		case 3:
			res = (uint32_t)add_clk_spi_tbl->status;
			break;
		default:
			break;
	}
	return res;
}

int add_clk_spi_set_param(int param, uint32_t data) {
	int res = 0;
	ext_spi_t* add_clk_spi_tbl = triple_spi__add_clock_get_tbl();
	switch(param) {
		case 0:
			add_clk_spi_tbl->write = (uint32_t*)data;
			break;
		case 1:
			add_clk_spi_tbl->read = (uint32_t*)data;
			break;
		case 2:
			add_clk_spi_tbl->ctrl = (uint32_t*)data;
			break;
		case 3:
			add_clk_spi_tbl->status = (uint32_t*)data;
			break;
		default:
			res = -1;
			break;
	}
	return res;
}

static void fw_upd_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	int fw_num;
	tester__data.one_cmd_buff.ext_data = (void*)1;
	GetCtrlIndex(tester__data.one_cmd_buff.panel, P_FW_UPD_SUB_MODUL, &fw_num);
	tester__data.fw_upd_pkt = fw_num;
	*(uint16_t*)cmd_buff = 0x001B;		// fw_ctrl
	*(uint16_t*)&cmd_buff[2] = 4;		// len
	*(uint16_t*)&cmd_buff[4] = (uint16_t)fw_num;	// номер сабмодуля
	*(uint16_t*)&cmd_buff[6] = 1;		// stop
	transmit_data(cmd_buff, 8, IO_PROCESSOR, STATE_ONE_CMD, 0);
}

int CVICALLBACK panel_add_clock_default_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int temp;
	switch (event) {
		case EVENT_COMMIT:
			triple_spi__set_default();
			GetCtrlIndex(panel, P_ADDCLOCK_ADD_CLK_SPI_TBL, &temp);
			SetCtrlVal(panel, P_ADDCLOCK_ADD_CLK_REG_ADDR, add_clk_spi_get_param(temp));
			break;
	}
	return 0;
}

int CVICALLBACK cmd_change_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	commands__struct_t* cur_cmd;
	int temp;
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlIndex (PANELHANDLE.id, PANEL_CMD_CHANGE, &temp);
			cur_cmd = &cmd_tbl[temp];
			SetCtrlIndex(PANELHANDLE.id, PANEL_Type, cur_cmd->proc_type == IO_PROCESSOR ? 0 : 1);
			SetCtrlVal(PANELHANDLE.id, PANEL_CMD, cur_cmd->cmd_id);
			SetCtrlVal(PANELHANDLE.id, PANEL_PARAM_LEN, cur_cmd->pin_num);
			for(int i = 0; i < cur_cmd->pin_num; i++) {
				SetCtrlIndex(PANELHANDLE.id, param_tbl[i].len, cur_cmd->p_types[i]);
			}
			break;
	}
	return 0;
}

void CVICALLBACK program_fw_upd (int menuBar, int menuItem, void *callbackData,int panel) {
	PRUPDPANEL.active = 1;
	DisplayPanel(PRUPDPANEL.id);
	tester__data.pr_ver_receive = 1;
}


static int CVICALLBACK program_upd_cout(void *functionId) {
	char buff[4000], url_addr[3000], *cur_str;
	int cur_proc;
	FILE* out_file;
	SetCtrlAttribute (PRUPDPANEL.id, PANEL_PR_DOWNLOAD, ATTR_LABEL_TEXT, "Получение адреса...");
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && rd /Q /S %TEMP%\\Test_UDP \"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && powershell -command (New-Object Net.WebClient).DownloadFile('https://cloud-api.yandex.net/v1/disk/public/resources/download?public_key=https://disk.yandex.ru/d/QjobDnOr-itLjg', \\\"$Env:TEMP\\\\\\\"+'download_info.txt')\"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	sprintf(buff, "%s\\download_info.txt", getenv("TEMP"));
	out_file = fopen(buff, "r");
	if (out_file) {
		fgets(buff, sizeof(buff), out_file);
		if ((cur_str = strstr(buff, "https://"))) {
			for(int i = 0; i < sizeof(url_addr); i++, cur_str++) {
				if (*cur_str == '\"') {
					url_addr[i] = 0;
					break;
				}
				memcpy(&url_addr[i], cur_str, 1);

			}
		}
		fclose(out_file);
		LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && del /Q %TEMP%\\download_info.txt\"", LE_HIDE, &cur_proc);
	}
	sprintf(buff, "cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && powershell \"-command (New-Object Net.WebClient).DownloadFile('%s', \\\"$Env:TEMP\\\\\\\"+'Test_UDP.zip')\"\"", url_addr);
	SetCtrlAttribute (PRUPDPANEL.id, PANEL_PR_DOWNLOAD, ATTR_LABEL_TEXT, "Загрузка архива...");
	if (LaunchExecutableEx (buff, LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	SetCtrlAttribute (PRUPDPANEL.id, PANEL_PR_DOWNLOAD, ATTR_LABEL_TEXT, "Распаковка архива...");
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && powershell -command Expand-Archive $Env:TEMP\\\\Test_UDP.zip $Env:TEMP\\\\Test_UDP\"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && del /Q %TEMP%\\Test_UDP.zip \"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	SetCtrlAttribute (PRUPDPANEL.id, PANEL_PR_DOWNLOAD, ATTR_LABEL_TEXT, "Выполнено.");
	sw_timer__start(&tester__data.pr_upd_tmr, 100, pr_upd_tmr_cb, (void*)1);
	return 0;
}


int CVICALLBACK program_fw_upd_bt (int panel, int control, int event,void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			sw_timer__start(&tester__data.pr_upd_tmr, 350, pr_upd_tmr_cb, NULL);
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, program_upd_cout, NULL, NULL);
			SetCtrlVal(PRUPDPANEL.id, PANEL_PR_DOWNLOAD, 0);
			break;
	}
	return 0;
}

static void pr_upd_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	int x;
	sw_timer__start(&tester__data.pr_upd_tmr, -1, pr_upd_tmr_cb, ext_data);
	if (ext_data) {
		PRUPDPANEL.active = 0;
		HidePanel(PRUPDPANEL.id);
		SetCtrlVal(PRUPDPANEL.id, PANEL_PR_DOWNLOAD, 0);
		sw_timer__stop(&tester__data.pr_upd_tmr);
		LaunchExecutableEx ("cmd /C set __COMPAT_LAYER= RunAsHighest  && %TEMP%\\Test_UDP\\Volume\\setup.exe", LE_HIDE, NULL);
		tester__data.close_program = 1;
		return;
	}
	GetCtrlVal(PRUPDPANEL.id, PANEL_PR_DOWNLOAD, &x);
	x++;
	SetCtrlVal(PRUPDPANEL.id, PANEL_PR_DOWNLOAD, x);
}

#include "CVI\time.h"
static void test_udp__get_version() {
	static int busy = 0;
	if (busy)
		return;
	busy = 1;
	char cvi_date[100] = {BUILD_YEAR_CH0, BUILD_YEAR_CH1, BUILD_YEAR_CH2, BUILD_YEAR_CH3, '-', BUILD_MONTH_CH0, BUILD_MONTH_CH1, '-', BUILD_DAY_CH0, BUILD_DAY_CH1, 'T'};
	strcat(cvi_date, __TIME__);
	strcat(cvi_date, "+03:00");
	int cur_proc, year, day, month;
	FILE* out_file;
	char buff[2000], *cur_str;
	if (LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && powershell -command (New-Object Net.WebClient).DownloadFile('https://cloud-api.yandex.net/v1/disk/public/resources?public_key=https://disk.yandex.ru/d/QjobDnOr-itLjg', \\\"$Env:TEMP\\\\\\\"+'test_udp_info.txt')\"", LE_HIDE, &cur_proc) == 0)
		while(ExecutableHasTerminated(cur_proc) == 0);
	sprintf(buff, "%s\\test_udp_info.txt", getenv("TEMP"));
	out_file = fopen(buff, "r");
	if (out_file) {
		fgets(buff, sizeof(buff), out_file);
		if ((cur_str = strstr(buff, "\"modified\":\""))) {
			cur_str += 12;
			for(int i = 0; cur_str[i]; i++) {
				if (cur_str[i] == '\"')
					cur_str[i] = 0;
			}
			sscanf(cur_str, "%d-%d-%d", &year, &month, &day);
			if (cmp_time(cur_str, cvi_date) >= 86400) {                  // One day
				GetPanelAttribute (PANELHANDLE.id, ATTR_TITLE, buff);
				if (strstr(buff, "доступно обновление") == NULL) {
					sprintf(buff, "%s (доступно обновление от %02d.%02d.%d)", buff, day, month, year);
					SetPanelAttribute (PANELHANDLE.id, ATTR_TITLE, buff);
				}
			}
			sprintf(buff, "Версия на сайте от %02d.%02d.%d", day, month, year);
			SetCtrlVal(PRUPDPANEL.id, PANEL_PR_PR_VER_SITE, buff);
		}
		fclose(out_file);
		LaunchExecutableEx ("cmd /C \"set __COMPAT_LAYER=RUNASINVOKER && del /Q %TEMP%\\test_udp_info.txt\"", LE_HIDE, &cur_proc);
	}
	busy = 0;
}


int cmp_time(char* destt, char* curt) {
	int res = 3000000;
	int time_day[2] = {0, 0};
	char* str_time[] = {destt, curt};
	char* dt;
	int year[2] = {0}, month[2] = {0}, day, h, m, s, gmt_inv, gmt;

	for(int cur_t = 0; cur_t < 2; cur_t++) {
		dt = str_time[cur_t];
		gmt_inv = 1;
		for(int i = 0; i <= strlen(dt); i++) {
			if ((dt[i] == '-') || (dt[i] == ':') || (dt[i] == 'T') || (dt[i] == '+')) {
				if (dt[i] == '+')
					gmt_inv = 0;
				dt[i] = ' ';

			}
		}
		sscanf(dt, "%d%d%d%d%d%d%d", &year[cur_t], &month[cur_t], &day, &h, &m, &s, &gmt);
		if (gmt_inv)
			gmt = gmt * (-1);
		h -= gmt;

		time_day[cur_t] = s + 60*m + 3600*h + 86400*day;
	}

	if ((year[0] != year[1]) || (month[0] != month[1]))
		return res;
	res = time_day[0] - time_day[1];
	if (res < 0)
		res = res * (-1);
	return res;
}

void CVICALLBACK menu_sync_clock_cb (int menuBar, int menuItem, void *callbackData, int panel) {
	SYNCCLOCKPANEL.active = 1;
	DisplayPanel(SYNCCLOCKPANEL.id);
}

static void reg_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	int tabpanel = 0;
	if (ext_data) {
		tabpanel = 1;
		SetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, tabpanel);
	}
	else {
		SetCtrlVal (PANELHANDLE.id, PANEL_BINARYSWITCH, tabpanel);
	}
	test_ctrl_cb(PANELHANDLE.id, PANEL_BINARYSWITCH, EVENT_COMMIT, (void*)1, 0, 0);
}

void debug_itf(char* msg) {
	char buff[1000];
	CRITICAL_SECTION_ON
	if (tester__data.cur_buf) {
		sprintf(buff, "%0.3f Debug: %s\n", (Timer() - tester__data.cur_time), msg);
		strcpy(tester__data.cur_buf, buff);
		tester__data.cur_buf += strlen(buff) + 1;
		tester__data.pkt_num++;
	}
	CRITICAL_SECTION_OFF
}

int CVICALLBACK sc_ch_ctrl (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int temp;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &temp);
			SetCtrlAttribute (panel, P_SYNCCLK_BOX_CH, ATTR_DIMMED, !temp);
			SetCtrlAttribute (panel, P_SYNCCLK_SC_CH_BT, ATTR_DIMMED, !temp);
			SetCtrlAttribute (panel, P_SYNCCLK_FREQ_TEST, ATTR_DIMMED, temp);
			break;
	}
	return 0;
}

static void sc_ch_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	if (transmit_get_state() == NULL)
		return;
	int panel = (int)ext_data;
	*(uint16_t*)cmd_buff = CMD__SETTINGS_RD;		// settings read
	*(uint16_t*)&cmd_buff[2] = 2;		// len
	*(uint16_t*)&cmd_buff[4] = DEV_SETTS__SYNCCLOCK;
	tester__data.one_cmd_buff.panel = panel;
	tester__data.one_cmd_buff.ext_data = sc_ch_tbl;
	transmit_data(cmd_buff, 6, IO_PROCESSOR, STATE_ONE_CMD, 0);
}

int CVICALLBACK sc_ch_get_bt_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			sw_timer__start(&tester__data.sc_ch_tmr, 1, sc_ch_tmr_cb, (void*)panel);
			break;
	}
	return 0;
}

static fdebug__struct_t fdebug__buff;

void CVICALLBACK menu_fdebug_cb (int menuBar, int menuItem, void *callbackData,int panel) {
	FDEBUGPANEL.active = 1;
	DisplayPanel (FDEBUGPANEL.id);
}

static void fdebug_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
	if (transmit_get_state() == NULL)
		return;
	int panel = (int)ext_data;
	*(uint16_t*)cmd_buff = CMD__SETTINGS_RD;		// settings read
	*(uint16_t*)&cmd_buff[2] = 2;		// len
	*(uint16_t*)&cmd_buff[4] = DEV_SETTS__FDEBUG;
	tester__data.one_cmd_buff.panel = panel;
	tester__data.one_cmd_buff.ext_data = &fdebug__buff;
	transmit_data(cmd_buff, 6, IO_PROCESSOR, STATE_ONE_CMD, 0);
}

static void fdebug_tmr_wr_cb(struct sw_timer__t *timer, void *ext_data) {
	if (transmit_get_state() == NULL)
		return;
	int panel = (int)ext_data;
	*(uint16_t*)cmd_buff = CMD__SETTINGS_WR;		// settings read
	*(uint16_t*)&cmd_buff[2] = 4 + sizeof(fdebug__buff);	// len
	*(uint16_t*)&cmd_buff[4] = DEV_SETTS__FDEBUG;
	*(uint16_t*)&cmd_buff[6] = sizeof(fdebug__buff);
	memcpy(&cmd_buff[8], &fdebug__buff, sizeof(fdebug__buff));
	tester__data.one_cmd_buff.panel = panel;
	tester__data.one_cmd_buff.ext_data = &fdebug__buff;
	transmit_data(cmd_buff, 8 + sizeof(fdebug__buff), IO_PROCESSOR, STATE_ONE_CMD, 0);
}


int CVICALLBACK fdebug_change_cb (int panel, int control, int event,void *callbackData, int eventData1, int eventData2) {
	int temp;
	switch (event) {
		case EVENT_COMMIT:
			IsListItemChecked(panel, FDEBUG_BOX_CH, eventData1, &temp);
			if (temp) {
				fdebug__buff.status |= (1 << eventData1);
			}
			else
				fdebug__buff.status &= ~(1 << eventData1);
			break;
	}
	return 0;
}

static void fdebug__renew() {
	int panel = FDEBUGPANEL.id;
	for(int i = 0; i < 8; i++) {
		CheckListItem (panel, FDEBUG_BOX_CH, i, (fdebug__buff.status & (1 << i)) ? 1 : 0);
	}

}

int CVICALLBACK fdebug_write_cb (int panel, int control, int event,void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			sw_timer__start(&tester__data.fdebug_tmr, 1, fdebug_tmr_wr_cb, (void*)panel);
			break;
	}
	return 0;
}

int CVICALLBACK fdebug_read_cb (int panel, int control, int event,void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			sw_timer__start(&tester__data.fdebug_tmr, 1, fdebug_tmr_cb, (void*)panel);
			break;
	}
	return 0;
}

