/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  Autorize                         1
#define  Autorize_ADD_CMD_BUTTON_2        2       /* control type: command, callback function: password_set */
#define  Autorize_PASS_OLD                3       /* control type: string, callback function: (none) */
#define  Autorize_PASSWORD                4       /* control type: string, callback function: password_set */

#define  CMD_PANEL                        2
#define  CMD_PANEL_ADD_CMD_BUTTON_FIND    2       /* control type: command, callback function: cmd_num_list_cb */
#define  CMD_PANEL_ADD_CMD_BUTTON_2       3       /* control type: command, callback function: cmd_del_list_cb */
#define  CMD_PANEL_ADD_CMD_BUTTON         4       /* control type: command, callback function: cmd_add_list_cb */
#define  CMD_PANEL_LISTBOX_CMD_NUM        5       /* control type: listBox, callback function: (none) */
#define  CMD_PANEL_LISTBOX                6       /* control type: listBox, callback function: list_cb */
#define  CMD_PANEL_CMD_DATA               7       /* control type: string, callback function: (none) */
#define  CMD_PANEL_CMD_NAME               8       /* control type: string, callback function: (none) */

#define  FDEBUG                           3
#define  FDEBUG_READBUTTON_3              2       /* control type: command, callback function: fdebug_write_cb */
#define  FDEBUG_READBUTTON_4              3       /* control type: command, callback function: fdebug_read_cb */
#define  FDEBUG_QUITBUTTON                4       /* control type: command, callback function: panel_menu_quit_cb */
#define  FDEBUG_DECORATION_4              5       /* control type: deco, callback function: (none) */
#define  FDEBUG_BOX_CH                    6       /* control type: listBox, callback function: fdebug_change_cb */

#define  P_ADDCLOCK                       4
#define  P_ADDCLOCK_QUITBUTTON_5          2       /* control type: command, callback function: panel_menu_quit_cb */
#define  P_ADDCLOCK_DECORATION_7          3       /* control type: deco, callback function: (none) */
#define  P_ADDCLOCK_DECORATION_6          4       /* control type: deco, callback function: (none) */
#define  P_ADDCLOCK_QUITBUTTON_4          5       /* control type: command, callback function: panel_add_clock_hmc7044_cb */
#define  P_ADDCLOCK_ADD_CLK_SPI_TBL       6       /* control type: ring, callback function: add_clk_spi_reg_cb */
#define  P_ADDCLOCK_DESCRIPTION_2         7       /* control type: textMsg, callback function: (none) */
#define  P_ADDCLOCK_QUITBUTTON_6          8       /* control type: command, callback function: panel_add_clock_default_cb */
#define  P_ADDCLOCK_QUITBUTTON_3          9       /* control type: command, callback function: panel_add_clock_ad9139_cb */
#define  P_ADDCLOCK_DECORATION_5          10      /* control type: deco, callback function: (none) */
#define  P_ADDCLOCK_DESCRIPTION           11      /* control type: textMsg, callback function: (none) */
#define  P_ADDCLOCK_ADD_CLK_REG_ADDR      12      /* control type: numeric, callback function: add_clk_spi_reg_set_cb */

#define  P_FW_UPD                         5
#define  P_FW_UPD_B_FW_SUB_UPD            2       /* control type: command, callback function: fw_upd_exec_button_cb */
#define  P_FW_UPD_QUITBUTTON_2            3       /* control type: command, callback function: fw_upd_num_cb */
#define  P_FW_UPD_QUITBUTTON              4       /* control type: command, callback function: fw_upd_quit_cb */
#define  P_FW_UPD_SUB_MODUL               5       /* control type: ring, callback function: fw_sub_idx_change_cb */
#define  P_FW_UPD_DECORATION_3            6       /* control type: deco, callback function: (none) */
#define  P_FW_UPD_CTRL_FW_VER             7       /* control type: textMsg, callback function: (none) */

#define  P_SYNCCLK                        6
#define  P_SYNCCLK_QUITBUTTON             2       /* control type: command, callback function: panel_menu_quit_cb */
#define  P_SYNCCLK_SC_CH_BT               3       /* control type: command, callback function: sc_ch_get_bt_cb */
#define  P_SYNCCLK_DECORATION_4           4       /* control type: deco, callback function: (none) */
#define  P_SYNCCLK_SC_SET_BT              5       /* control type: command, callback function: freq_set_cb */
#define  P_SYNCCLK_DECORATION_5           6       /* control type: deco, callback function: (none) */
#define  P_SYNCCLK_BOX_CH                 7       /* control type: listBox, callback function: sc_ch_change_cb */
#define  P_SYNCCLK_FREQ_SET               8       /* control type: numeric, callback function: sc_ch_change_cb */
#define  P_SYNCCLK_FREQ_CH                9       /* control type: ring, callback function: sc_ch_change_cb */
#define  P_SYNCCLK_SYNC_CH_ON             10      /* control type: radioButton, callback function: sc_ch_ctrl */
#define  P_SYNCCLK_FREQ_TEST              11      /* control type: radioButton, callback function: freq_test_cb */
#define  P_SYNCCLK_ON_PANEL               12      /* control type: radioButton, callback function: (none) */

#define  PANEL                            7
#define  PANEL_ADD_LOAD_BUTTON            2       /* control type: command, callback function: settings_cb */
#define  PANEL_BUTTON_FW_VER              3       /* control type: command, callback function: fw_ver_button_cb */
#define  PANEL_ADD_SAVE_BUTTON            4       /* control type: command, callback function: settings_cb */
#define  PANEL_ADD_CMD_BUTTON             5       /* control type: command, callback function: cmd_add_cb */
#define  PANEL_QUITBUTTON                 6       /* control type: command, callback function: QuitCallback */
#define  PANEL_PARAM_6                    7       /* control type: numeric, callback function: cmd_add_cb */
#define  PANEL_IDN_CTRL                   8       /* control type: string, callback function: (none) */
#define  PANEL_CTRL_FW_VER                9       /* control type: string, callback function: (none) */
#define  PANEL_CTRL_FW_HASH               10      /* control type: string, callback function: (none) */
#define  PANEL_PARAM_5                    11      /* control type: numeric, callback function: cmd_add_cb */
#define  PANEL_CTRL_FW_VER_2              12      /* control type: textMsg, callback function: (none) */
#define  PANEL_PARAM_4                    13      /* control type: numeric, callback function: cmd_add_cb */
#define  PANEL_PARAM_3                    14      /* control type: numeric, callback function: cmd_add_cb */
#define  PANEL_PARAM_2                    15      /* control type: numeric, callback function: cmd_add_cb */
#define  PANEL_PARAM_1                    16      /* control type: numeric, callback function: cmd_add_cb */
#define  PANEL_DRAW_TIME                  17      /* control type: numeric, callback function: draw_time_cb */
#define  PANEL_PKT_ID                     18      /* control type: numeric, callback function: set_pkt_id */
#define  PANEL_SPEED                      19      /* control type: numeric, callback function: (none) */
#define  PANEL_LOST_PKT                   20      /* control type: numeric, callback function: (none) */
#define  PANEL_MSG_CNT                    21      /* control type: numeric, callback function: (none) */
#define  PANEL_PARAM_LEN                  22      /* control type: numeric, callback function: cmd_add_cb */
#define  PANEL_CMD                        23      /* control type: numeric, callback function: cmd_add_cb */
#define  PANEL_RECEIVE                    24      /* control type: textBox, callback function: (none) */
#define  PANEL_DECORATION_5               25      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_2               26      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_3               27      /* control type: deco, callback function: (none) */
#define  PANEL_BINARYSWITCH               28      /* control type: binary, callback function: test_ctrl_cb */
#define  PANEL_NUMERICSLIDE               29      /* control type: scale, callback function: timeout_cb */
#define  PANEL_TEST_TIME                  30      /* control type: string, callback function: (none) */
#define  PANEL_TEST_SPEED                 31      /* control type: radioButton, callback function: test_speed_cb */
#define  PANEL_TYPE_6                     32      /* control type: ring, callback function: (none) */
#define  PANEL_TYPE_5                     33      /* control type: ring, callback function: (none) */
#define  PANEL_TYPE_4                     34      /* control type: ring, callback function: (none) */
#define  PANEL_TYPE_3                     35      /* control type: ring, callback function: (none) */
#define  PANEL_TYPE_2                     36      /* control type: ring, callback function: (none) */
#define  PANEL_CMD_CHANGE                 37      /* control type: ring, callback function: cmd_change_cb */
#define  PANEL_TYPE_1                     38      /* control type: ring, callback function: (none) */
#define  PANEL_Type_2                     39      /* control type: ring, callback function: cmd_add_list */
#define  PANEL_Type                       40      /* control type: ring, callback function: (none) */
#define  PANEL_LISTEN                     41      /* control type: radioButton, callback function: listen_cb */
#define  PANEL_AUTO_ID                    42      /* control type: radioButton, callback function: auto_id */
#define  PANEL_TAB                        43      /* control type: tab, callback function: panel_switch */
#define  PANEL_DECORATION_4               44      /* control type: deco, callback function: (none) */
#define  PANEL_TRANSFET_LED               45      /* control type: LED, callback function: (none) */
#define  PANEL_DOWNLOAD                   46      /* control type: scale, callback function: (none) */
#define  PANEL_TOGGLEBUTTON               47      /* control type: textButton, callback function: cmd_menu_ctrl_cb */
#define  PANEL_DECORATION                 48      /* control type: deco, callback function: (none) */
#define  PANEL_HEX_DEC                    49      /* control type: radioButton, callback function: hex_dec_ctrl_cb */
#define  PANEL_TEXTMSG_2                  50      /* control type: textMsg, callback function: (none) */

#define  PANEL_DESC                       8
#define  PANEL_DESC_ADD_CMD_BUTTON_2      2       /* control type: command, callback function: tab_descr_cb */
#define  PANEL_DESC_CMD_NAME              3       /* control type: string, callback function: (none) */

#define  PANEL_MENU                       9
#define  PANEL_MENU_READBUTTON_2          2       /* control type: command, callback function: panel_menu_write_cb */
#define  PANEL_MENU_READBUTTON            3       /* control type: command, callback function: panel_menu_read_cb */
#define  PANEL_MENU_QUITBUTTON_2          4       /* control type: command, callback function: panel_menu_del_cb */
#define  PANEL_MENU_QUITBUTTON_3          5       /* control type: command, callback function: panel_menu_add_cb */
#define  PANEL_MENU_QUITBUTTON            6       /* control type: command, callback function: panel_menu_quit_cb */
#define  PANEL_MENU_ON_PANEL              7       /* control type: radioButton, callback function: (none) */

#define  PANEL_PR                         10
#define  PANEL_PR_DOWNLOAD                2       /* control type: scale, callback function: (none) */
#define  PANEL_PR_PR_UPD_BUTTON           3       /* control type: command, callback function: program_fw_upd_bt */
#define  PANEL_PR_DECORATION_4            4       /* control type: deco, callback function: (none) */
#define  PANEL_PR_QUITBUTTON              5       /* control type: command, callback function: panel_menu_quit_cb */
#define  PANEL_PR_DECORATION_5            6       /* control type: deco, callback function: (none) */
#define  PANEL_PR_PR_VER_SITE             7       /* control type: textMsg, callback function: (none) */
#define  PANEL_PR_CMD_NAME                8       /* control type: string, callback function: (none) */

     /* tab page panel controls */
#define  TABPANEL_TRANSMIT                2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_PORT                    3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_MASK                    4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_PORT_IN                 5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_IP                      6       /* control type: string, callback function: udp_sett_cb */

     /* tab page panel controls */
#define  TABPANEL_2_TRANSMIT              2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_2_PORT                  3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_2_MASK                  4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_2_PORT_IN               5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_2_IP                    6       /* control type: string, callback function: udp_sett_cb */

     /* tab page panel controls */
#define  TABPANEL_3_TRANSMIT              2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_3_PORT                  3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_3_MASK                  4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_3_PORT_IN               5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_3_IP                    6       /* control type: string, callback function: udp_sett_cb */

     /* tab page panel controls */
#define  TABPANEL_4_TRANSMIT              2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_4_PORT                  3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_4_MASK                  4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_4_PORT_IN               5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_4_IP                    6       /* control type: string, callback function: udp_sett_cb */

     /* tab page panel controls */
#define  TABPANEL_5_TRANSMIT              2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_5_PORT                  3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_5_MASK                  4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_5_PORT_IN               5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_5_IP                    6       /* control type: string, callback function: udp_sett_cb */

     /* tab page panel controls */
#define  TABPANEL_6_TRANSMIT              2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_6_PORT                  3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_6_MASK                  4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_6_PORT_IN               5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_6_IP                    6       /* control type: string, callback function: udp_sett_cb */

     /* tab page panel controls */
#define  TABPANEL_7_TRANSMIT              2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_7_PORT                  3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_7_MASK                  4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_7_PORT_IN               5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_7_IP                    6       /* control type: string, callback function: udp_sett_cb */

     /* tab page panel controls */
#define  TABPANEL_8_TRANSMIT              2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_8_PORT                  3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_8_MASK                  4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_8_PORT_IN               5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_8_IP                    6       /* control type: string, callback function: udp_sett_cb */

     /* tab page panel controls */
#define  TABPANEL_9_TRANSMIT              2       /* control type: textBox, callback function: panel_switch */
#define  TABPANEL_9_PORT                  3       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_9_MASK                  4       /* control type: string, callback function: udp_sett_cb */
#define  TABPANEL_9_PORT_IN               5       /* control type: numeric, callback function: udp_sett_cb */
#define  TABPANEL_9_IP                    6       /* control type: string, callback function: udp_sett_cb */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

#define  MENU_MODUL                       1
#define  MENU_MODUL_Modules               2
#define  MENU_MODUL_Modules_SYNC_CLOCK    3       /* callback function: menu_sync_clock_cb */
#define  MENU_MODUL_Modules_MENU_ADD_CLOCK 4      /* callback function: menu_add_clock_cb */
#define  MENU_MODUL_Modules_MENU_EXT_IF   5       /* callback function: menu_ext_if_cb */
#define  MENU_MODUL_Modules_FDEBUG        6       /* callback function: menu_fdebug_cb */
#define  MENU_MODUL_FW_UPD_BUTTON         7
#define  MENU_MODUL_FW_UPD_BUTTON_SW_DATA_BUTTON 8 /* callback function: sw_data_cb */
#define  MENU_MODUL_FW_UPD_BUTTON_FW_UPD_BUTTON 9 /* callback function: fw_sub_upd_cb */
#define  MENU_MODUL_CALIB_MENU            10
#define  MENU_MODUL_CALIB_MENU_SyncClock  11      /* callback function: calib_sync_clock_cb */
#define  MENU_MODUL_PROGRAM_UPD           12      /* callback function: program_fw_upd */

#define  MENUBAR                          2
#define  MENUBAR_MENU1                    2


     /* Callback Prototypes: */

int  CVICALLBACK add_clk_spi_reg_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK add_clk_spi_reg_set_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK auto_id(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK calib_sync_clock_cb(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK cmd_add_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cmd_add_list(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cmd_add_list_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cmd_change_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cmd_del_list_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cmd_menu_ctrl_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cmd_num_list_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK draw_time_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK fdebug_change_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK fdebug_read_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK fdebug_write_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK freq_set_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK freq_test_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK fw_sub_idx_change_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK fw_sub_upd_cb(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK fw_upd_exec_button_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK fw_upd_num_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK fw_upd_quit_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK fw_ver_button_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK hex_dec_ctrl_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK list_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK listen_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK menu_add_clock_cb(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK menu_ext_if_cb(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK menu_fdebug_cb(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK menu_sync_clock_cb(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK panel_add_clock_ad9139_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panel_add_clock_default_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panel_add_clock_hmc7044_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panel_menu_add_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panel_menu_del_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panel_menu_quit_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panel_menu_read_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panel_menu_write_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panel_switch(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK password_set(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK program_fw_upd(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK program_fw_upd_bt(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK sc_ch_change_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK sc_ch_ctrl(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK sc_ch_get_bt_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK set_pkt_id(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK settings_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK sw_data_cb(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK tab_descr_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK test_ctrl_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK test_speed_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK timeout_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK udp_sett_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
