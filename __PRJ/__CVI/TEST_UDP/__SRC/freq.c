#include <utility.h>
#include <ansi_c.h>
#include "freq.h"

#define FREQ_CHECK        2800000000                 // Проверка 2.8Ггц
#define FREQ_FIN          ((double)100)              // Входная частота 100Мгц
#define FREQ_FGUN_MAX     ((double)3200)             // Максимальная частота ГУНа
#define FREQ_FGUN_MIN     ((double)2400)             // Минимальная частота ГУНа
#define FREQ_M            (FREQ_FGUN_MAX / FREQ_FIN) //
#define FREQ_N            (FREQ_FGUN_MIN / FREQ_FIN) //
#define FREQ_OUT_MIN      ((double)55)               // Минимальная выходная частота
#define FREQ_OUT_MAX      ((double)110)              // Максимальная выходная частота
#define OSCIL_CH_MAX      7                          // Количество каналов модуля
#define FREQ_MAX_COUNT    764

struct {
    freq_cal_t p_cal;                                                   // Расчетные параметры микросхем
}data_cal;

typedef struct {
    freq_cal_t p_cal;                                                   // Расчетные параметры микросхем
}oscil_data__t;

static oscil_data__t oscil_data[2];
uint32_t max_divider;

double delta_min, delta_min_2, percent_mid, percent_mid2;

/*******************************************************************************
 * Функция получения коэффициентов n1, d1, n2, d2 и ближайшей частоты.
 ******************************************************************************/
static freq_cal_t* find_rational_fraction(uint8_t oscil_id, uint32_t frequency, uint32_t frequency2, double precision) {
    oscil_data__t* data = oscil_data;
    freq_cal_t* res = NULL;
    double f1, f2, f22;
    int n1, n2, n22, d1, d2, d22, d_1, i, j, max_div = 0;
    double delta, pmin, pmin2, delta2;
    double freq = (double)frequency / 1000000;
	double freq2 = (double)frequency2 / 1000000;
    n1 = FREQ_M;
    pmin = pmin2 = 100;
    for (d_1 = 2 * n1; d_1 >= n1; d_1--) {
        d1 = 2 * d_1;
        f1 = FREQ_FIN * n1 / d1;
        i = RoundRealToNearestInteger(FREQ_FGUN_MAX / f1);
        j = RoundRealToNearestInteger(FREQ_FGUN_MIN / f1);
        for (n2 = i; n2 >= j; n2--) {
            d2 = RoundRealToNearestInteger(FREQ_FIN * n1 * n2 / (freq * d1));
            if ((d2 % 2) == 1)
                continue;
            f2 = (FREQ_FIN * n1 * n2) / (d1 * d2);
            delta = fabs(f2 - freq);
            if (delta < pmin) {
		        for (n22 = i; n22 >= j; n22--) {
	    	        d22 = RoundRealToNearestInteger(FREQ_FIN * n1 * n22 / (freq2 * d1));
					if ((d22 % 2) == 1)
						continue;
		            f22 = (FREQ_FIN * n1 * n22) / (d1 * d22);
		            delta2 = fabs(f22 - freq2);
					if (delta2 < pmin2) {
						pmin2 = delta2;
						data->p_cal.freqreal2 = f22;
						data->p_cal.d[2] = d22;
						data->p_cal.n[2] = n22;
					}
				}
				pmin = delta;
                if (pmin < precision) {
                    data->p_cal.freqreal = f2;
                    data->p_cal.n[0] = n1;
                    data->p_cal.d[0] = d1;
                    data->p_cal.n[1] = n2;
                    data->p_cal.d[1] = d2;
                    data->p_cal.freq_gun[0] = FREQ_FIN * n1 * 1000000;
                    data->p_cal.freq_gun[1] = ( FREQ_FIN * n1 / d1) * n2 * 1000000;
                    data->p_cal.sysref[0] = (uint16_t)((((data->p_cal.freq_gun[0] / 3) / d1) / 1000000 + 1) * d1);
                    max_div = d2;
                    res = &data->p_cal;
                }
            }
        }
    }

    int div;
    double d_div;
    d_div = (double)data->p_cal.freq_gun[1] / frequency2;
    div = TruncateRealNumber(d_div);
    if (div % 2) {
        if ((d_div - TruncateRealNumber(d_div) > 0.5))
            div += 1;
        else
            div -= 1;
    }
	
	data->p_cal.d[2] = div;  
	data->p_cal.n[2] = data->p_cal.n[1]; 
	f22 = ((double)data->p_cal.freq_gun[1] / 1000000) / div;     	
	data->p_cal.freqreal2 = f22;       
	delta2 = fabs(f22 - freq2);
	if (delta2 < pmin2)
		pmin2 = delta2;
	
	if (pmin > delta_min)
		delta_min = pmin;
	if (pmin2 > delta_min_2)
		delta_min_2 = pmin2;
    return res;
}





/*******************************************************************************
 * Функция поиска частоты в соответствии с таблицей частот.
 ******************************************************************************/
uint32_t find_freq(uint32_t frequency, int max_cnt) {
    int count = max_cnt ? max_cnt : FREQ_MAX_COUNT;
    double f1 = 0, lf0, step, f_prev = FREQ_OUT_MIN;
    double freq = (double)frequency / 1000000;
    if ((freq < FREQ_OUT_MIN) || (freq > FREQ_OUT_MAX))
        return 0;
	lf0 = log(FREQ_OUT_MIN);    
	double step_stage = (log(FREQ_OUT_MAX) - lf0);
    for(int i = 0; i <= count-1; i++) {
        step = step_stage / (count - 1);
        f_prev = f1;
        f1 = exp(lf0 + i * step);
        if (freq < f1)
            break;
    }
    freq = (f1 - freq > freq - f_prev) ? f_prev : f1;
    return freq * 1000000;
}

static uint32_t find_new_freq(uint32_t freq) {
	double fin = 0, lf0, step;
	fin = (double)freq / 1000000;
	lf0 = log(FREQ_OUT_MIN);
	step = (log(FREQ_OUT_MAX) - lf0) /  (FREQ_MAX_COUNT - 1);

	return RoundRealToNearestInteger((log(fin) - lf0)/step); 
}


/*******************************************************************************
 * Функция пересчета делителя канала
 ******************************************************************************/
static uint32_t oscil_ch_div_set(uint32_t freq) {
    oscil_data__t* data = oscil_data;
    int div;
    double d_div;
    d_div = (double)data->p_cal.freq_gun[1] / freq;
    div = TruncateRealNumber(d_div);
    if (div % 2) {
        if ((d_div - TruncateRealNumber(d_div) > 0.5))
            div += 1;
        else
            div -= 1;
    }
    return div;
}


