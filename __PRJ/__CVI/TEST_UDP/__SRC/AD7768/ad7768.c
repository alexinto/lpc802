/***************************************************************************//**
 * @file ad7768.c.
 * @brief Модуль, реализующий управление микросхемой AD7768.
 * @author a.tushentsov.
 ******************************************************************************/
#include "stddef.h"
#include "System/framework.h"
#include "DRV/AD7768/ad7768.h"

/*******************************************************************************
 * Структура транзакции миксрохемы AD7768.
 ******************************************************************************/
#pragma pack(push,1)
typedef enum {
    WRITE       = 0,
    READ        = 1,                  // каналы можно выбирать для записи одновременно несколько каналов битовой маской.
}ad7768_rw_e;
typedef struct {
    uint16_t data      :8;            // Данные
    uint16_t addr      :7;            // Адрес регистра
    uint16_t oper      :1;            // Запись\чтение
}ad7768_struct_t;
#pragma pack(pop)

typedef enum {
    AD7768_IDLE    = 0,
    AD7768_WAIT    = 1,
    AD7768_CHECK   = 2,
}ad7768__state_e;

static ad7768__ch_reg_t ad7768_regs;

static struct {
    framework__sub_t cout_sub;
    ad7768_hw_func_t hw_transport;       // Указатель на функцию-обработчик транспортного уровня.
    ad7768_struct_t tx_buff, rx_buff;
    ad7768__state_e state;
    events__e event;
    uint8_t id;
    uint32_t addr, cur_addr;
    int len;
    ad7768__cb_t cb;
    void* ext_data;
}ad7768_data;

static void ad7768__cout();
static void ad7768__rw_cb(uint8_t spi_id, events__e event, uint8_t* buff, int len);
static events__e ad7768__tx(uint8_t id, ad7768_rw_e oper);

events__e ad7768__init(ad7768_hw_func_t hw_transport) {
    ad7768_data.hw_transport = hw_transport;
    framework__cout_subscribe(&ad7768_data.cout_sub, ad7768__cout);
    return EVENT__OK;
}

ad7768__ch_reg_t* ad7768__get_regs(uint8_t id) {
    return &ad7768_regs;
}

events__e ad7768__read(uint8_t id, uint32_t addr, int len, ad7768__cb_t cb, void* ext_data) {
    if (ad7768_data.state != AD7768_IDLE)
        return EVENT__BUSY;
    ad7768_data.state = AD7768_WAIT;
    ad7768_data.cb = cb;
    ad7768_data.ext_data = ext_data;
    ad7768_data.addr = ad7768_data.cur_addr = addr;
    ad7768_data.len = len;
    ad7768_data.cb = cb;
    ad7768_data.ext_data = ext_data;
    return ad7768__tx(id, READ);
}

events__e ad7768__write(uint8_t id, uint32_t addr, int len, ad7768__cb_t cb, void* ext_data) {
    if (ad7768_data.state != AD7768_IDLE)
        return EVENT__BUSY;
    ad7768_data.state = AD7768_WAIT;
    ad7768_data.cb = cb;
    ad7768_data.ext_data = ext_data;
    ad7768_data.addr = ad7768_data.cur_addr = addr;
    ad7768_data.len = len;
    ad7768_data.cb = cb;
    ad7768_data.ext_data = ext_data;
    events__e res = ad7768__tx(id, WRITE);
    ad7768_data.cur_addr++;
    return res;
}

static void ad7768__rw_cb(uint8_t spi_id, events__e event, uint8_t* buff, int len) {
    ad7768_data.event = event;
    if ((ad7768_data.tx_buff.oper == READ) && (event == EVENT__OK)) {
        if (ad7768_data.cur_addr > ad7768_data.addr)
            *(((uint8_t*)&ad7768_regs) + ad7768_data.cur_addr - 1) = ad7768_data.rx_buff.data;
    }
    else
        ad7768_data.state = AD7768_CHECK;
}

static events__e ad7768__tx(uint8_t id, ad7768_rw_e oper) {
    uint8_t* cur_buff = (uint8_t*)&ad7768_data.rx_buff;
    // Заполняем буфер передачи
    ad7768_data.tx_buff.data = 0;
    if (oper == WRITE) {
        ad7768_data.tx_buff.data = *cur_buff;
        cur_buff = NULL;
    }
    ad7768_data.tx_buff.addr = (uint16_t)ad7768_data.cur_addr;
    ad7768_data.tx_buff.oper = oper;
    if (ad7768_data.hw_transport(id, (uint8_t*)&ad7768_data.tx_buff, sizeof(ad7768_data.tx_buff), cur_buff, sizeof(ad7768_data.rx_buff), ad7768__rw_cb) != EVENT__OK) {
        ad7768_data.state = AD7768_IDLE;
        return EVENT__ERROR;
    }
    ad7768_data.id = id;
    return EVENT__OK;
}

static void ad7768__cout() {
    if (ad7768_data.state == AD7768_CHECK) {
        if ((ad7768_data.cur_addr < ad7768_data.addr + ad7768_data.len) && (ad7768_data.event == EVENT__OK)) {
            ad7768_data.state = AD7768_IDLE;
            ad7768_data.cur_addr++;
            ad7768_data.event = ad7768__tx(ad7768_data.id, ad7768_data.tx_buff.oper);
            if (ad7768_data.event == EVENT__OK)
                return;
        }
        ad7768_data.state = AD7768_IDLE;
        if (ad7768_data.cb) {
            ad7768_data.cb(ad7768_data.id, ad7768_data.event, ad7768_data.addr, ad7768_data.len, ad7768_data.ext_data);
        }
    }
}
