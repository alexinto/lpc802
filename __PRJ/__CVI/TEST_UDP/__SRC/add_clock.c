/***************************************************************************//**
 * @file add_clock.c.
 * @brief Модуль, реализующий управление мезонином Add Clock.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include <string.h>
#include <stddef.h>
#include <math.h>
#include "DRV/ADD_CLOCK/add_clock.h"
#include "DRV/SyncClock/triple_spi.h"
#include "DRV/SyncClock/hmc7044.h"
#include "System/rxiTime.h"

#ifndef ADD_CLK__SPI_ID
#define ADD_CLK__SPI_ID 0
#endif

#define NO_PARAM          0xFFFF                     // критерий окончания таблиц
#define ROUT_AD9139       100                        // выходное сопротивлене, Ом
#define REF_AD9139        1.2                        // Опорное напряжение, В
#define REF_DAC8552       5                          // Опорное напряжение, В

#pragma pack(push, 1)
typedef struct {
    uint32_t rset;      // R, kOm
    uint32_t min;       // U, mV
    uint32_t max;       // U, mV
}volt_range_tbl_t;

typedef struct {
    uint16_t reg;
    uint8_t data;
}add_clk_reg_tbl__t;

typedef struct {
    uint8_t start_trig;
    add_clock__trig_mode_e start_event;
    uint8_t stop_trig;
    add_clock__trig_mode_e stop_event;
}trig_cfg_t;
#pragma pack(pop)

static volt_range_tbl_t volt_range_tbl[] = {{80, 180, 360}, {40, 360, 720}, {20, 720, 1440}, {10, 1440, 2880}};

// Таблица событий запсука\остановки по триггерным сигналам
//                                       START          |         STOP
static trig_cfg_t trg_tbl[] = {{0, ADD_CLOCK__TRIG_RISE, 0, ADD_CLOCK__TRIG_FALL},      // 0
                               {1, ADD_CLOCK__TRIG_RISE, 1, ADD_CLOCK__TRIG_FALL},      // 1
                               {0, ADD_CLOCK__TRIG_FALL, 0, ADD_CLOCK__TRIG_RISE},      // 2
                               {1, ADD_CLOCK__TRIG_FALL, 1, ADD_CLOCK__TRIG_RISE},      // 3
                               {0, ADD_CLOCK__TRIG_ANY,  1, ADD_CLOCK__TRIG_ANY},       // 4
                               {1, ADD_CLOCK__TRIG_ANY,  0, ADD_CLOCK__TRIG_ANY},       // 5
};

#ifndef ADD_CLOCK_SETT_MAX
#define ADD_CLOCK_SETT_MAX 2
#endif

static uint32_t add_clock__settings_tbl[ADD_CLOCK_SETT_MAX] = {25000000, 0};

static events__e hmc7044_init();
static events__e ad9139_init();
static events__e add_clock__gen_change(add_clock__gen_id_e gen_id);
static events__e add_clock__set_voltage(uint32_t voltage, uint32_t offset);
static uint32_t add_clock__get_voltage();
static events__e add_clock__set_clk(uint32_t freq, uint32_t phase);
static events__e add_clock__get_clk(uint32_t* freq, uint32_t* phase);
static events__e add_clock__set_ctrl_mode(add_clock__start_stop_mode_e ctrl_mode);
static events__e add_clock__set_clk_mode(add_clock__clk_mode_e clk_mode);

static struct {
    uint32_t v_offset, version;
    add_clock__transport_t hw_spi;
}add_clock_data;

/*******************************************************************************
 * Функция инициализации мезонина Add Clock.
 ******************************************************************************/
events__e add_clock__init(add_clock__transport_t hw_spi_func) {
    if (hw_spi_func)
        add_clock_data.hw_spi = hw_spi_func;
//    add_clock__info_t buff = {0};
//    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->info, (uint8_t*)&buff, sizeof(buff));
//    add_clock_data.version = (buff.version_low << 8) | buff.version_high;

//    add_clock__reset_t reset = {0};
//    reset.global = 1;
//    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->reset, (uint8_t*)&reset, sizeof(reset));
//    hmc7044_init();
//    ad9139_init();
    return EVENT__OK;
}

uint32_t add_clock__get_version () {
    add_clock__info_t buff = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->info, (uint8_t*)&buff, sizeof(buff));
    add_clock_data.version = (buff.version_low << 8) | buff.version_high;
    return add_clock_data.version;
}

/*******************************************************************************
 * Функция записи настроек мезонина Add Clock.
 ******************************************************************************/
events__e add_clock__sett_set(add_clock__settings_e sett_id, uint32_t data) {
    if (sett_id >= ADD_CLOCK_SETT_MAX)
        return EVENT__PARAM_NA;
    add_clock__settings_tbl[sett_id] = data;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция чтения настроек мезонина Add Clock.
 ******************************************************************************/
uint32_t* add_clock__sett_get(add_clock__settings_e sett_id) {
    if (sett_id >= ADD_CLOCK_SETT_MAX)
        return NULL;
    return &add_clock__settings_tbl[sett_id];
}
/*******************************************************************************
 * Функция выбора режима работы генератора.
 ******************************************************************************/
static events__e add_clock__gen_change(add_clock__gen_id_e gen_id) {
    add_clock__gen_ctrl_t gen_data = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->gen_ctrl, (uint8_t*)&gen_data, sizeof(add_clock__gen_ctrl_t));
    switch(gen_id) {
        case ADD_CLOCK__GEN_SIN:
            gen_data.mode = 1;                  // генератор синусоидального сигнала
            break;
        case ADD_CLOCK__GEN_RECT:
            gen_data.mode = 2;                  // генератор прямоугольного сигнала
            break;
        case ADD_CLOCK__GEN_SYNC:
            gen_data.mode = 3;                  // генератор прямоугольного сигнала с фиксированным периодом
            break;
        default:
            return EVENT__ERROR;
    }
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->gen_ctrl, (uint8_t*)&gen_data, sizeof(add_clock__gen_ctrl_t));
    return EVENT__OK;
}
/*******************************************************************************
 * Функция установки параметров выходного синусоидального сигнала мезонина AddClock.
 ******************************************************************************/
events__e add_clock__sin_set(uint32_t freq, uint32_t phase, uint32_t voltage, uint32_t v_offset, add_clock__out_mode_e out_mode, add_clock__start_stop_mode_e ctrl_mode) {
    add_clock__relay_t relay = {0};
    add_clock__gen_change(ADD_CLOCK__GEN_SIN); // Выбор режима работы генератора
    add_clock__set_clk(freq, phase); // Установка частоты и фазы сигнала
    add_clock__set_voltage(voltage, v_offset); // вычисляем и устанавливаем амплитуду и смещение сигнала
    // задаем режим работы выхода
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->relay, (uint8_t*)&relay, sizeof(relay));
    relay.diff_se = (out_mode == ADD_CLOCK__OUT_DIFF) ? 1 : 0;
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->relay, (uint8_t*)&relay, sizeof(relay));
    add_clock__set_clk_mode(ADD_CLOCK__CLK_INT);
    add_clock__set_ctrl_mode(ctrl_mode);

    return EVENT__OK;
}

/*******************************************************************************
 * Функция чтения параметров выходного синусоидального сигнала мезонина AddClock.
 ******************************************************************************/
events__e add_clock__sin_get(uint32_t* freq, uint32_t* phase, uint32_t* voltage, uint32_t* v_offset) {
    add_clock__get_clk(freq, phase);
    *voltage = add_clock__get_voltage();
    *v_offset = add_clock_data.v_offset;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция настройки триггерных сигналов для генераторов сигналов мезонина AddClock.
 ******************************************************************************/
events__e add_clock__trig_cfg(add_clock__gen_id_e gen_id, uint8_t start_trig_id, add_clock__trig_mode_e start_event, uint8_t stop_trig_id, add_clock__trig_mode_e stop_event) {
    trig_cfg_t cur_trig_cfg = {start_trig_id, start_event, stop_trig_id, stop_event};
    add_clock__dac_cfg__t cfg_data = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->ad9139_cfg.ctrl, (uint8_t*)&cfg_data.ctrl, sizeof(cfg_data.ctrl));
    for(int i = 0; i < (sizeof(trg_tbl) / sizeof(trg_tbl[0])); i++) {
        if (memcmp(&trg_tbl[i], &cur_trig_cfg, sizeof(trig_cfg_t)))
            continue;
        cfg_data.ctrl.trigger = i;
        add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->ad9139_cfg.ctrl, (uint8_t*)&cfg_data.ctrl, sizeof(cfg_data.ctrl));
        return EVENT__OK;
    }
    return EVENT__ERROR;
}

/*******************************************************************************
 * Функция установки параметров выходного прямоугольного сигнала мезонина AddClock.
 ******************************************************************************/
events__e add_clock__rect_set(uint32_t freq, uint32_t phase, uint32_t voltage, uint32_t v_offset, add_clock__clk_mode_e clk_mode, add_clock__start_stop_mode_e ctrl_mode) {
    add_clock__gen_change(ADD_CLOCK__GEN_RECT); // Выбор режима работы генератора
    add_clock__set_clk(freq, phase);            // Установка частоты и фазы сигнала
    add_clock__set_voltage(voltage, v_offset);  // вычисляем и устанавливаем амплитуду сигнала
    add_clock__set_ctrl_mode(ctrl_mode);
    add_clock__set_clk_mode(clk_mode);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция чтения параметров выходного прямоугольного сигнала мезонина AddClock.
 ******************************************************************************/
events__e add_clock__rect_get(uint32_t* freq, uint32_t* phase, uint32_t* voltage, uint32_t* v_offset) {
    add_clock__get_clk(freq, phase);
    *voltage = add_clock__get_voltage();
    *v_offset = add_clock_data.v_offset;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция установки параметров выходного сигнала синхронизации мезонина AddClock.
 ******************************************************************************/
events__e add_clock__sync_set(uint32_t freq, uint32_t phase, add_clock__clk_mode_e clk_mode, add_clock__start_stop_mode_e ctrl_mode) {

    return EVENT__OK;
}

/*******************************************************************************
 * Функция чтения параметров выходного сигнала синхронизации мезонина AddClock.
 ******************************************************************************/
events__e add_clock__sync_get(uint32_t* freq, uint32_t* phase) {

    return EVENT__OK;
}

/*******************************************************************************
 * Функция управления генераторами выходных сигналов мезонина AddClock.
 ******************************************************************************/
events__e add_clock__gen_ctrl(add_clock__gen_id_e gen_id, add_clock__gen_cmd_e gen_cmd) {
    add_clock__dac_cfg__t buff = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->ad9139_cfg.ctrl, (uint8_t*)&buff.ctrl, sizeof(buff.ctrl));
    buff.ctrl.tx_en_pin = (gen_cmd == ADD_CLOCK__GEN_START) ? 1 : 0;
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->ad9139_cfg.ctrl, (uint8_t*)&buff.ctrl, sizeof(buff.ctrl));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция тестирования мезонина AddClock.
 ******************************************************************************/
events__e add_clock__test() {
//    add_clock__diag_t buff;
    add_clock__relay_t relay = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->relay, (uint8_t*)&relay, sizeof(relay));
    // диагностика тактирования
    relay.diff_diag = 1;
    relay.se_diag = 1;
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->relay, (uint8_t*)&relay, sizeof(relay));
    // todo проверка тактирования
    relay.diff_diag = 0;
    relay.se_diag = 0;
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->relay, (uint8_t*)&relay, sizeof(relay));

    return EVENT__OK;
}

/*******************************************************************************
 * Функция настройки собтия старта и остановки генератора выходного сигнала.
 ******************************************************************************/
static events__e add_clock__set_ctrl_mode(add_clock__start_stop_mode_e ctrl_mode) {
    add_clock__dac_cfg__t cfg_data = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->ad9139_cfg.ctrl, (uint8_t*)&cfg_data.ctrl, sizeof(cfg_data.ctrl));
    switch(ctrl_mode) {
        case ADD_CLOCK__START_ALWAYS:
            cfg_data.ctrl.mode = 0;
            cfg_data.ctrl.spi = 1;
            add_clock__gen_ctrl(ADD_CLOCK__GEN_SIN, ADD_CLOCK__GEN_START);
            break;
    //        case ADD_CLOCK__START_EXT:
    //            cfg_data.ctrl.mode = 2;
    //            break;
        case ADD_CLOCK__START_STOP_TRIG:
            cfg_data.ctrl.mode = 1;
            break;
        default:
            return EVENT__PARAM_NA;
    }
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->ad9139_cfg.ctrl, (uint8_t*)&cfg_data.ctrl, sizeof(cfg_data.ctrl));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция выбора режима тактирования генератора выходного сигнала и ЦАП(ad9139).
 ******************************************************************************/
static events__e add_clock__set_clk_mode(add_clock__clk_mode_e clk_mode) {
    add_clock__mux_clk_t buff = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->mux_ctrl, (uint8_t*)&buff, sizeof(buff));
    switch(clk_mode) {
        case ADD_CLOCK__CLK_INT:
            buff.sw1_fs_ext = 1; // тактовый сигнал ЦАП (ad9139)
            buff.sw_fs_ext = 1;  // тактовый сигнал ПЛИС
            break;
        case ADD_CLOCK__CLK_EXT:
            buff.sw1_fs_ext = 0; // тактовый сигнал ЦАП (ad9139)
            buff.sw_fs_ext = 0;  // тактовый сигнал ПЛИС
            break;
        default:
            return EVENT__PARAM_NA;
    }
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->mux_ctrl, (uint8_t*)&buff, sizeof(buff));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция установки амплитуды выходного сигнала мезонина AddClock.
 ******************************************************************************/
static events__e add_clock__set_voltage(uint32_t voltage, uint32_t offset) {
    add_clock__ad9139_t ad9139_data = {0};
    add_clock__relay_t relay = {0};
    uint16_t dac_gain = 0;
    uint16_t dac_offset = round((double)offset * 65536 / (REF_DAC8552 * 1000));
    add_clock__dac8552_wr(&dac_offset, 2);
    add_clock_data.v_offset = round((double)dac_offset * REF_DAC8552 * 1000 / 65536); // не уверен можно ли читать DAC...

    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->relay, (uint8_t*)&relay, sizeof(relay));
    relay.en_1x2x = 1; relay.sw1xn2x = 0; // 1 к 1 при помощи реле
    if (voltage < volt_range_tbl[1].min) {
        voltage *= 2;
        relay.sw1xn2x = 1;                // умножаем при помощи реле
    }
    else if (voltage > volt_range_tbl[2].max) {
        voltage /= 2;
        relay.en_1x2x = 0;                // делим при помощи реле
    }
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->relay, (uint8_t*)&relay, sizeof(relay));
    for(int cur_range = 0;cur_range < (sizeof(volt_range_tbl)/sizeof(volt_range_tbl[0])); cur_range++) {
        if ((voltage >= volt_range_tbl[cur_range].min) && (voltage <= volt_range_tbl[cur_range].max)) {
            // выбираем резистор
            ad9139_data.current_ctrl.rmax = cur_range ? 1 : 0;
            ad9139_data.current_ctrl.rmid = cur_range > 1 ? 1 : 0;
            ad9139_data.current_ctrl.rmin = cur_range > 2 ? 1 : 0;
            add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->ad9139.current_ctrl, (uint8_t*)&ad9139_data.current_ctrl, sizeof(ad9139_data.current_ctrl));
            // задаем DAC GAIN
            add_clock__ad9139_spi(TSPI__OPER_READ, 0x18, (uint8_t*)&dac_gain, 2);
            cur_range = round((voltage * volt_range_tbl[cur_range].rset / (ROUT_AD9139 * REF_AD9139) - 72) * 16 / 3);
            dac_gain &= ~0x3FF;
            dac_gain |= cur_range & 0x3FF;
            add_clock__ad9139_spi(TSPI__OPER_WRITE, 0x18, (uint8_t*)&dac_gain, 2);
            return EVENT__OK;
        }
    }
    return EVENT__ERROR;
}

/*******************************************************************************
 * Функция чтения амплитуды выходного сигнала мезонина AddClock.
 ******************************************************************************/
static uint32_t add_clock__get_voltage() {
    add_clock__ad9139_t ad9139_data = {0};
    uint32_t voltage, r_idx;
    uint16_t dac_gain;
    add_clock__ad9139_spi(TSPI__OPER_READ, 0x18, (uint8_t*)&dac_gain, 2);
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->ad9139.current_ctrl, (uint8_t*)&ad9139_data.current_ctrl, sizeof(ad9139_data.current_ctrl));
    r_idx = ad9139_data.current_ctrl.rmax + ad9139_data.current_ctrl.rmid + ad9139_data.current_ctrl.rmin;
    voltage = round((ROUT_AD9139 * REF_AD9139)*(72 + 3 * (double)(dac_gain & 0x3FF) / 16) / volt_range_tbl[r_idx].rset);
    return voltage;
}

/*******************************************************************************
 * Функция установки частоты и фазы выходного сигнала мезонина AddClock.
 ******************************************************************************/
static events__e add_clock__set_clk(uint32_t freq, uint32_t phase) {
    add_clock__sin_gen_ctrl_t g_data = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG ->sin_gen_ctrl, (uint8_t*) &g_data, sizeof(add_clock__sin_gen_ctrl_t));
    g_data.freq = round(0.08388608 * freq);
    g_data.phase = round((double)phase * 1677721.6 / 36);
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG ->sin_gen_ctrl, (uint8_t*) &g_data, sizeof(add_clock__sin_gen_ctrl_t));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция чтения частоты и фазы выходного сигнала мезонина AddClock.
 ******************************************************************************/
static events__e add_clock__get_clk(uint32_t* freq, uint32_t* phase) {
    add_clock__sin_gen_ctrl_t buff = {0};
    add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG ->sin_gen_ctrl, (uint8_t*)&buff, sizeof(buff));
    *freq = round(buff.freq / 0.08388608);
    *phase = round(buff.phase * 36 / 1677721.6);
    return EVENT__OK;
}

// Таблица значений по- умолчанию HMC7044
static add_clk_reg_tbl__t hmc7044_tbl[] = {
    {HMC7044_REG->out_ctrl[0].channel, 0xF0},     {HMC7044_REG->out_ctrl[2].channel, 0xF0},     {HMC7044_REG->out_ctrl[8].channel, 0xF0},
    {HMC7044_REG->out_ctrl[9].channel, 0xF0},     {HMC7044_REG->out_ctrl[10].channel, 0xF0},    {HMC7044_REG->out_ctrl[11].channel, 0xF0},
    {HMC7044_REG->clk_out_drv_pw[0], 0x4D},       {HMC7044_REG->clk_out_drv_pw[1], 0xDF},       {HMC7044_REG->pll1_delay, 0x06},
    {HMC7044_REG->pll1_holdover, 0x06},           {HMC7044_REG->vtune_preset, 0x09},            {HMC7044_REG->gpi_ctrl[2], 0x00},
    {HMC7044_REG->gpi_ctrl[3], 0x00},             {HMC7044_REG->en_ctrl[0], 0x2E},              {HMC7044_REG->pll2_r2[0], 0x01},
    {HMC7044_REG->pll2_r2[1], 0x00},              {HMC7044_REG->pll2_n2[0], 0x80},              {HMC7044_REG->pll2_n2[1], 0x00},
    {HMC7044_REG->pll2_doubler, 0x01},            {HMC7044_REG->glob_mode, 0x40},               {HMC7044_REG->sync, 0x02},
    {HMC7044_REG->pulse_gen, 0x01},               {HMC7044_REG->oscin_buf_ctrl, 0x07},          {HMC7044_REG->sysref_tmr[0], 0x40},
    {HMC7044_REG->sysref_tmr[1], 0x04},           {HMC7044_REG->out_ctrl[12].ch_div[0], 32},    {HMC7044_REG->out_ctrl[12].ch_div[1], 0},
    {HMC7044_REG->out_ctrl[12].driver_mode, 0x10},{HMC7044_REG->out_ctrl[12].channel, 0xC1},    {HMC7044_REG->out_ctrl[12].mux_mode, 0x01},
    {HMC7044_REG->out_ctrl[12].a_delay, 0x0A},    {HMC7044_REG->mode_ctrl[0], 0x62},            {HMC7044_REG->mode_ctrl[0], 0x60},
    {NO_PARAM, 0}
};

/*******************************************************************************
 * Функция инициализации микросхемы HMC7044 мезонина AddClock.
 ******************************************************************************/
static events__e hmc7044_init() {
    add_clk_reg_tbl__t* cur_tbl = hmc7044_tbl;
    uint8_t buff[2] = {0x0, 0x1};
    uint8_t check = 0x13; // Результат конфигурации HMC_7044
    add_clock__hmc7044_spi(TSPI__OPER_WRITE, HMC7044_REG->soft_reset, &buff[1], 1);
    DELAY_MS(40);
    add_clock__hmc7044_spi(TSPI__OPER_WRITE, HMC7044_REG->soft_reset, &buff[0], 1);
    DELAY_MS(40);
    while (cur_tbl->reg != NO_PARAM) {
        add_clock__hmc7044_spi(TSPI__OPER_WRITE, cur_tbl->reg, &cur_tbl->data, 1);
        if (cur_tbl->reg == HMC7044_REG->pll2_doubler)
            DELAY_MS(40);
        cur_tbl++;
    }
    DELAY_MS(100);
    add_clock__hmc7044_spi(TSPI__OPER_READ, HMC7044_REG->alarm_read[0], buff, 1);
    if (*buff != check)
        return EVENT__ERROR;

    return EVENT__OK;
}

// Таблица значений по- умолчанию ad9139
static add_clk_reg_tbl__t ad9139_tbl[] = {
    {0x0000, 0x20}, {0x0020, 0x01}, {0x0012, 0x00}, {0x0026, 0x80}, {0x005E, 0x00}, {0x005F, 0x60},
    {0x005D, 0x16}, {0x000A, 0x00}, {0x0028, 0x00}, {0x0027, 0x00}, {0x0001, 0x00},
    {NO_PARAM, 0}
};

/*******************************************************************************
 * Функция инициализации микросхемы HMC7044 мезонина AddClock.
 ******************************************************************************/
static events__e ad9139_init() {
    add_clk_reg_tbl__t* cur_tbl = ad9139_tbl;
    while (cur_tbl->reg != NO_PARAM) {
        add_clock__ad9139_spi(TSPI__OPER_WRITE, cur_tbl->reg, &cur_tbl->data, 1);
        cur_tbl++;
    }
    DELAY_MS(100);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция работы с интерфейсом SPI микросхемы HMC7044 мезонина AddClock.
 ******************************************************************************/
events__e add_clock__hmc7044_spi(int oper, uint16_t addr, uint8_t* data, int len) {
    add_clock__spi_hmc7044_t hdata = {0};
    for (int i = 0; i < len; i++) {
        do {
            add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->spi_hmc7044.read_status, (uint8_t*) &hdata.read_status, sizeof(hdata.read_status));
        } while (hdata.read_status.busy);
        hdata.ctrl.rw = oper;
        hdata.ctrl.addr = addr + i;
        hdata.ctrl.data_w = data[i];
        add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->spi_hmc7044.ctrl, (uint8_t*) &hdata.ctrl, sizeof(hdata.ctrl));
        if (oper == TSPI__OPER_READ) {
            do {
                add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->spi_hmc7044.read_status, (uint8_t*) &hdata.read_status, sizeof(hdata.read_status));
            } while (hdata.read_status.busy);
            data[i] = hdata.read_status.data_r;
        }
    }
    return EVENT__OK;
}

/*******************************************************************************
 * Функция работы с интерфейсом SPI микросхемы AD9139 мезонина AddClock.
 ******************************************************************************/
events__e add_clock__ad9139_spi(int oper, uint16_t addr, uint8_t* data, int len) {
    add_clock__ad9139_t ad_data = {0};
    for (int i = 0; i < len; i++) {
        do {
            add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->ad9139.status, (uint8_t*)&ad_data.status, sizeof(ad_data.status));
        } while (ad_data.status.busy);
        ad_data.ctrl.start_rw = oper;
        ad_data.ctrl.addr = addr + i;
        ad_data.ctrl.data_w = data[i];
        add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->ad9139.ctrl, (uint8_t*)&ad_data.ctrl, sizeof(ad_data.ctrl));
        if (oper == TSPI__OPER_READ) {
            do {
                add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->ad9139.status, (uint8_t*)&ad_data.status, sizeof(ad_data.status));
            } while (ad_data.status.busy);
            add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->ad9139.data_r, (uint8_t*)&ad_data.data_r, 4);
            data[i] = ad_data.data_r;
        }
    }
    return EVENT__OK;
}

/*******************************************************************************
 * Функция записи микросхемы dac8552 мезонина AddClock.
 ******************************************************************************/
events__e add_clock__dac8552_wr(uint16_t* data, int len) {
    add_clock__dac8552_t dac_data = {0};
    dac_data.ctrl.buff_sel = 0;
    dac_data.ctrl.ldb_lda = 1;
    dac_data.ctrl.op_mode = 1;
    for (int i = 0; i < len; i++) {
        do {
            add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->dac8552.status, (uint8_t*)&dac_data.status, sizeof(dac_data.status));
        } while (dac_data.status.busy);
        dac_data.ctrl.data = data[i];
        add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->dac8552.ctrl, (uint8_t*)&dac_data.ctrl, sizeof(dac_data.ctrl));
    }
    return EVENT__OK;
}

/*******************************************************************************
 * Функция работы с интерфейсом SPI микросхемы m95640 мезонина AddClock.
 ******************************************************************************/
events__e add_clock__m95640(int oper, uint16_t addr, uint8_t* data, int len) {
    add_clock__m95640_t m_data = {0};
    for (int i = 0; i < len; i++) {
        do {
            add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->m95640.status, (uint8_t*)&m_data.status, sizeof(m_data.status));
        } while (m_data.status.busy);
        m_data.ctrl.start = 1;
        m_data.ctrl.addr = addr + i;
        m_data.ctrl.rw = oper;
        m_data.data_wr = data[i];
        if (oper == TSPI__OPER_WRITE) {
            add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->m95640.data_wr, (uint8_t*)&m_data.data_wr, sizeof(m_data.data_wr));
            do {
                add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->m95640.status, (uint8_t*)&m_data.status, sizeof(m_data.status));
            } while (m_data.status.busy);
        }
        add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_WRITE, ADD_CLK_REG->m95640.ctrl, (uint8_t*) &m_data.ctrl, sizeof(m_data.ctrl));
        if (oper == TSPI__OPER_READ) {
            do {
                add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->m95640.status, (uint8_t*)&m_data.status, sizeof(m_data.status));
            } while (m_data.status.busy);
            add_clock_data.hw_spi(ADD_CLK__SPI_ID, TSPI__OPER_READ, ADD_CLK_REG->m95640.data_rd, (uint8_t*)&m_data.data_rd, sizeof(m_data.data_rd));
            data[i] = m_data.data_rd;
        }
    }
    return EVENT__OK;
}
