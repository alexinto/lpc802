/***************************************************************************//**
 * @file process.c
 * @brief контексты выполнения
 * @authors p.adaskin
 ******************************************************************************/
#include <userint.h>
#include <utility.h>
#include "process.h"

int process__context_get()
{
    return CmtGetCurrentThreadID();
}
static int critical_section_lock;
void process__critical_section_on()
{
}
void process__critical_section_off()
{
    CmtReleaseLock (critical_section_lock);
}
