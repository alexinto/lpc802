/***************************************************************************//**
 * @file ad5522.c.
 * @brief Модуль, реализующий управление микросхемой AD5522.
 * @author a.tushentsov.
 ******************************************************************************/
#include "stddef.h"
#include "System/framework.h"
#include "DRV/AD5522/ad5522.h"

/*******************************************************************************
 * Структура транзакции миксрохемы AD5022.
 ******************************************************************************/
#pragma pack(push,1)
#define AD5522_NOP 0xFFFFFF
typedef enum {
    SCR_PMU    = 0,                  // если ch = SCR нули, то SCR. Иначе = PMU
    DAC_GAIN   = 1,
    DAC_OFFSET = 2,
    DAC_DATA   = 3,                  // если ch = SCR, то NOP. Иначе = DAC_DATA
}ad5522_mode_e;
typedef enum {
    SCR        = 0,
    CH0        = 1,                  // каналы можно выбирать для записи одновременно несколько каналов битовой маской.
    CH1        = 2,
    CH2        = 4,
    CH3        = 8,
}ad5522_ch_e;
typedef enum {
    WRITE       = 0,
    READ        = 1,                  // каналы можно выбирать для записи одновременно несколько каналов битовой маской.
}ad5522_rw_e;
typedef struct {
    uint32_t data      :22;           // Данные
    ad5522_mode_e mode :2;            // Выбор режима
    ad5522_ch_e ch     :4;            // выбор канала
    ad5522_rw_e oper   :1;            // Запись\чтение
}ad5522_struct_t;
#pragma pack(pop)

typedef enum {
    AD5522_IDLE    = 0,
    AD5522_WAIT    = 1,
    AD5522_NEED_CB = 2,
}ad5522__state_e;

static struct {
    framework__sub_t cout_sub;
    ad5522_hw_func_t hw_transport;       // Указатель на функцию-обработчик транспортного уровня.
    ad5522_struct_t tx_buff;
    ad5522__state_e state;
    events__e event;
    uint8_t ch_id;
    ad5522__ch_reg_e reg;
    ad5522__data_t* buff;               // Указатель на пользовательский буфер
    ad5522_cb_t cb;
    void* ext_data;
}ad5522_data;

static void ad5522__cout();
static void ad5522__rw_cb(uint8_t spi_id, events__e event, uint8_t* buff, int len);
static events__e ad5522__tx(uint8_t ch_id, ad5522__ch_reg_e reg, ad5522__data_t* data, ad5522_rw_e oper, ad5522_cb_t cb, void* ext_data);

events__e ad5522__init(ad5522_hw_func_t hw_transport) {
    ad5522_data.hw_transport = hw_transport;
    framework__cout_subscribe(&ad5522_data.cout_sub, ad5522__cout);
    return EVENT__OK;
}

events__e ad5522__read(uint8_t ch_id, ad5522__ch_reg_e reg, ad5522__data_t* data, ad5522_cb_t cb, void* ext_data) {
    if (ad5522_data.state != AD5522_IDLE)
        return EVENT__BUSY;
    ad5522_data.state = AD5522_WAIT;
    ad5522_data.cb = cb;
    ad5522_data.ext_data = ext_data;
    return ad5522__tx(ch_id, reg, data, READ, cb, ext_data);
}

events__e ad5522__write(uint8_t ch_id, ad5522__ch_reg_e reg, ad5522__data_t* data, ad5522_cb_t cb, void* ext_data) {
    if (ad5522_data.state != AD5522_IDLE)
        return EVENT__BUSY;
    ad5522_data.state = AD5522_WAIT;
    return ad5522__tx(ch_id, reg, data, WRITE, cb, ext_data);
}

static void ad5522__rw_cb(uint8_t spi_id, events__e event, uint8_t* buff, int len) {
    ad5522_data.event = event;
    if ((ad5522_data.tx_buff.oper == READ) && (event == EVENT__OK)) {
        *(uint32_t*)&ad5522_data.tx_buff = AD5522_NOP;
        ad5522_data.hw_transport(spi_id, (uint8_t*)&ad5522_data.tx_buff, sizeof(ad5522_data.tx_buff), (uint8_t*)ad5522_data.buff, sizeof(ad5522__data_t), ad5522__rw_cb);
    }
    else
        ad5522_data.state = AD5522_NEED_CB;
}

static events__e ad5522__tx(uint8_t ch_id, ad5522__ch_reg_e reg, ad5522__data_t* data, ad5522_rw_e oper, ad5522_cb_t cb, void* ext_data) {
    ad5522_data.tx_buff.data = (oper == READ) ? 0 : *(uint32_t*)data;
    ad5522_data.tx_buff.ch = SCR;
    switch(reg) {
        case AD5522_SCR:
            ad5522_data.tx_buff.mode = SCR_PMU;
        case AD5522_ALARM:
            ad5522_data.tx_buff.mode = DAC_DATA;
            break;
        case AD5522_COMP:
            ad5522_data.tx_buff.mode = DAC_GAIN;
            break;
        default:
            ad5522_data.tx_buff.mode = reg;
            ad5522_data.tx_buff.ch = 1 << (ch_id % 4);
            break;
    }
    ad5522_data.tx_buff.oper = oper;
    if (ad5522_data.hw_transport(ch_id / 4, (uint8_t*)&ad5522_data.tx_buff, sizeof(ad5522_data.tx_buff), NULL, 0, ad5522__rw_cb) != EVENT__OK) {
        ad5522_data.state = AD5522_IDLE;
        return EVENT__ERROR;
    }
    ad5522_data.buff = data;
    ad5522_data.reg = reg;
    ad5522_data.ch_id = ch_id;
    ad5522_data.cb = cb;
    ad5522_data.ext_data = ext_data;
    return EVENT__OK;
}

static void ad5522__cout() {
    if (ad5522_data.state == AD5522_NEED_CB) {
        ad5522_data.state = AD5522_IDLE;
        if (ad5522_data.cb) {
            ad5522_data.cb(ad5522_data.ch_id, ad5522_data.event, ad5522_data.reg, ad5522_data.buff, ad5522_data.ext_data);
        }
    }
}
