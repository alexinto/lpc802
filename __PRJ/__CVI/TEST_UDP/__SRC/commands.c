#include <stddef.h>
#include "target.h"
#include "CommonCommand.h"
#include "commands.h"

extern commands__struct_t cmd_tbl[];



static commands__struct_t* commands__find_tbl(uint16_t cmd_id);


commands__struct_t* cmd_get(uint16_t cmd_id) {
	return commands__find_tbl(cmd_id);   
}




static commands__struct_t* commands__find_tbl(uint16_t cmd_id) {
	commands__struct_t* cur_tbl = cmd_tbl;
	while((cur_tbl->cmd_id != NULL) && (cur_tbl->cmd_id != cmd_id));
	return cur_tbl->cmd_id ? cur_tbl : NULL;
}










