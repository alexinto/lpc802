#include "test_udp.h"

/***************************************************************************//**
 * @file menu_box.c.
 * @brief Модуль меню.
 * @author a.tushentsov
 ******************************************************************************/
#include <userint.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "System/fstandard.h"
#include "target.h"
#include "menu_box.h"

typedef struct {
	int panelHandle;
	int tableCtrl, format_data;
	int column_num;
	menu_box__item_t cur_item;
	menu_box__handler_t handler;
	char* title;
}menu_box__struct_t;

typedef struct {
	int format;
	int color;
}format_ctrl_t;
static format_ctrl_t format_tbl[] = {{VAL_DECIMAL_FORMAT, VAL_BLACK}, {VAL_HEX_FORMAT, VAL_BLUE}, {VAL_BINARY_FORMAT, VAL_RED},};


static int CVICALLBACK format_data_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
static int CVICALLBACK table_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);

int menu_box__create(int panelhandle, int column_num, char* descr, menu_box__handler_t menu_handler) {
	menu_box__struct_t* menu_box_handle = malloc(sizeof(menu_box__struct_t));
	if (menu_box_handle == NULL)
		return -1;

	if (descr) {
		menu_box_handle->title = malloc(strlen(descr) + 1);
		if (menu_box_handle->title  == NULL)
			return -1;
		strcpy(menu_box_handle->title, descr);
	}
	SetPanelAttribute (panelhandle, ATTR_TITLE, descr ? descr : "");
	SetPanelAttribute (panelhandle, ATTR_CALLBACK_DATA, menu_box_handle);

	int tableCtrl = NewCtrl (panelhandle, CTRL_TABLE_LS, "", 25, 5);
	int format_data = NewCtrl (panelhandle, CTRL_MENU_RING, "", 5, 123);
	SetCtrlAttribute (panelhandle, format_data, ATTR_LABEL_FONT, "Lucida Console");
	SetCtrlAttribute (panelhandle, format_data, ATTR_TEXT_FONT, "Lucida Console");
	InsertListItem (panelhandle, format_data, 0, "Dec", 0);
	InsertListItem (panelhandle, format_data, 1, "Hex", 1);
	InsertListItem (panelhandle, format_data, 2, "Bin", 2);
	InstallCtrlCallback (panelhandle, format_data, format_data_cb, menu_box_handle);
	InstallCtrlCallback (panelhandle, tableCtrl, table_cb, menu_box_handle);


	InsertTableColumns (panelhandle, tableCtrl, -1, column_num, VAL_USE_MASTER_CELL_TYPE);

	SetTableColumnAttribute (panelhandle, tableCtrl, 1, ATTR_TEXT_COLOR, format_tbl[0].color);
	SetTableColumnAttribute (panelhandle, tableCtrl, 2, ATTR_TEXT_COLOR, format_tbl[0].color);
	SetTableColumnAttribute (panelhandle, tableCtrl, 1, ATTR_FORMAT, format_tbl[0].format);
	SetTableColumnAttribute (panelhandle, tableCtrl, 2, ATTR_FORMAT, format_tbl[0].format);

	SetCtrlAttribute (panelhandle, tableCtrl, ATTR_ENABLE_COLUMN_SIZING, 1);
	SetCtrlAttribute (panelhandle, tableCtrl, ATTR_ENABLE_ROW_SIZING, 1);
	SetCtrlAttribute (panelhandle, tableCtrl, ATTR_SCROLL_BARS, VAL_BOTH_SCROLL_BARS);
	SetCtrlAttribute (panelhandle, tableCtrl, ATTR_WIDTH, 280);
	SetCtrlAttribute (panelhandle, tableCtrl, ATTR_HEIGHT, 400);
	SetCtrlAttribute (panelhandle, tableCtrl, ATTR_LABEL_FONT, "Lucida Console");
	SetCtrlAttribute (panelhandle, tableCtrl, ATTR_TEXT_FONT, "Lucida Console");

	SetTableColumnAttribute (panelhandle, tableCtrl, -1, ATTR_USE_LABEL_TEXT, 1);
	SetTableColumnAttribute (panelhandle, tableCtrl, -1, ATTR_SIZE_MODE, VAL_USE_EXPLICIT_SIZE);
	SetTableColumnAttribute (panelhandle, tableCtrl, 1, ATTR_COLUMN_WIDTH, 75);
	SetTableColumnAttribute (panelhandle, tableCtrl, 2, ATTR_COLUMN_WIDTH, 143);
	SetTableColumnAttribute (panelhandle, tableCtrl, 1, ATTR_LABEL_TEXT, "Адрес");
	SetTableColumnAttribute (panelhandle, tableCtrl, 2, ATTR_LABEL_TEXT, "Данные");

	SetTableColumnAttribute (panelhandle, tableCtrl, -1, ATTR_CELL_TYPE, VAL_CELL_NUMERIC);
	SetTableColumnAttribute (panelhandle, tableCtrl, -1, ATTR_DATA_TYPE, VAL_UNSIGNED_INTEGER);
	SetTableColumnAttribute (panelhandle, tableCtrl, -1, ATTR_CHECK_RANGE, VAL_IGNORE);
	SetTableColumnAttribute (panelhandle, tableCtrl, -1, ATTR_LABEL_FONT, "Lucida Console");
	SetTableColumnAttribute (panelhandle, tableCtrl, -1, ATTR_TEXT_FONT, "Lucida Console");

	SetTableRowAttribute (panelhandle, tableCtrl, -1, ATTR_CELL_TYPE, VAL_CELL_NUMERIC);
	SetTableRowAttribute (panelhandle, tableCtrl, -1, ATTR_DATA_TYPE, VAL_UNSIGNED_INTEGER);
	SetTableRowAttribute (panelhandle, tableCtrl, -1, ATTR_CHECK_RANGE, VAL_IGNORE);
	SetTableRowAttribute (panelhandle, tableCtrl, -1, ATTR_LABEL_FONT, "Lucida Console");
	SetTableRowAttribute (panelhandle, tableCtrl, -1, ATTR_TEXT_FONT, "Lucida Console");

	menu_box_handle->panelHandle = panelhandle;
	menu_box_handle->tableCtrl = tableCtrl;
	menu_box_handle->column_num = column_num;
	menu_box_handle->format_data = format_data;

	return 0;
}

char* menu_box__get_name(int panel) {
	menu_box__struct_t* menu = NULL;
	GetPanelAttribute (panel, ATTR_CALLBACK_DATA, &menu);
	if (menu == NULL)
		return NULL;
	return menu->title;
}

int menu_box__get_item_num(int panel, menu_box__item_e item) {
	menu_box__struct_t* menu = NULL;
	GetPanelAttribute (panel, ATTR_CALLBACK_DATA, &menu);
	Point cur_item;
	static Rect sel_items;
	if (menu == NULL)
		return 0;
	switch(item) {
		case MENU_BOX__ACTIVE_ITEM:
			GetTableSelection (menu->panelHandle, menu->tableCtrl, &sel_items);
			cur_item.y = sel_items.top;
			if (cur_item.y == 0)
				GetActiveTableCell (menu->panelHandle, menu->tableCtrl, &cur_item);
			break;
		case MENU_BOX__LAST_ITEM:
			GetNumTableRows (menu->panelHandle, menu->tableCtrl, &cur_item.y);
			break;
		case MENU_BOX__SEL_ITEM:
			cur_item.y = sel_items.height;
			break;
//		default:
//			cur_item.y = -1;
//			break;
	}
	return cur_item.y;
}


int menu_box__add_item(int panel, int item_num, int format_addr, int format_data) {
	menu_box__struct_t* menu = NULL;
	GetPanelAttribute (panel, ATTR_CALLBACK_DATA, &menu);
	if (menu == NULL)
		return 0;
	InsertTableRows (menu->panelHandle, menu->tableCtrl, item_num, 1, VAL_USE_MASTER_CELL_TYPE);
	menu->cur_item.format_addr = format_addr < (sizeof(format_tbl) / sizeof(format_tbl[0])) ? format_addr : 0;
	menu->cur_item.format_data = format_data < (sizeof(format_tbl) / sizeof(format_tbl[0])) ? format_data : 0;
	SetTableCellAttribute (menu->panelHandle, menu->tableCtrl, MakePoint (1, item_num), ATTR_TEXT_COLOR, format_tbl[menu->cur_item.format_addr].color);
	SetTableCellAttribute (menu->panelHandle, menu->tableCtrl, MakePoint (1, item_num), ATTR_FORMAT, format_tbl[menu->cur_item.format_addr].format);
	SetTableCellAttribute (menu->panelHandle, menu->tableCtrl, MakePoint (2, item_num), ATTR_TEXT_COLOR, format_tbl[menu->cur_item.format_data].color);
	SetTableCellAttribute (menu->panelHandle, menu->tableCtrl, MakePoint (2, item_num), ATTR_FORMAT, format_tbl[menu->cur_item.format_data].format);
	if (item_num == -1)
		return GetNumTableRows (menu->panelHandle, menu->tableCtrl, &item_num);
	return item_num +1;
}


void menu_box__del_item(int panel, int item_num) {
	menu_box__struct_t* menu = NULL;
	GetPanelAttribute (panel, ATTR_CALLBACK_DATA, &menu);
	DeleteTableRows (menu->panelHandle, menu->tableCtrl, item_num, 1);
}


menu_box__item_t* menu_box__get_item(int panel, int item_num) {
	int temp;
	menu_box__struct_t* menu = NULL;
	GetPanelAttribute (panel, ATTR_CALLBACK_DATA, &menu);
	if (menu == NULL)
		return NULL;

	GetTableCellVal (menu->panelHandle, menu->tableCtrl, MakePoint (1, item_num), &menu->cur_item.addr);
	GetTableCellVal (menu->panelHandle, menu->tableCtrl, MakePoint (2, item_num), &menu->cur_item.data);
	menu->cur_item.format_addr = menu->cur_item.format_data = 0;
	GetTableCellAttribute (menu->panelHandle, menu->tableCtrl, MakePoint (1, item_num), ATTR_FORMAT, &temp);
	for(int i = 0; i < (sizeof(format_tbl) / sizeof(format_tbl[0])); i++) {
		if (format_tbl[i].format == temp)
			menu->cur_item.format_addr = i;
	}
	GetTableCellAttribute (menu->panelHandle, menu->tableCtrl, MakePoint (2, item_num), ATTR_FORMAT, &temp);
	for(int i = 0; i < (sizeof(format_tbl) / sizeof(format_tbl[0])); i++) {
		if (format_tbl[i].format == temp)
			menu->cur_item.format_data = i;
	}

	return &menu->cur_item;
}

void menu_box__set_item(int panel, int item_num, int addr, int data) {
	menu_box__struct_t* menu = NULL;
	GetPanelAttribute (panel, ATTR_CALLBACK_DATA, &menu);
	if (menu == NULL)
		return;
	SetTableCellVal (menu->panelHandle, menu->tableCtrl, MakePoint (1, item_num), addr);
	SetTableCellVal (menu->panelHandle, menu->tableCtrl, MakePoint (2, item_num), data);
}




static int CVICALLBACK format_data_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	menu_box__struct_t* menu = callbackData;
	int temp;
	Point cur_cell;
	Rect sel_items;
	switch (event) {
		case EVENT_VAL_CHANGED:
			if (menu == NULL)
				return 0;
			GetCtrlVal(panel, control, &temp);
			GetTableSelection (menu->panelHandle, menu->tableCtrl, &sel_items);
			if (sel_items.top) {
				for (cur_cell.y = sel_items.top; cur_cell.y < (sel_items.top + sel_items.height); cur_cell.y++) {
					for (cur_cell.x = sel_items.left; cur_cell.x < (sel_items.left + sel_items.width); cur_cell.x++) {
						SetTableCellAttribute (panel, menu->tableCtrl, cur_cell, ATTR_TEXT_COLOR, format_tbl[temp].color);
						SetTableCellAttribute (panel, menu->tableCtrl, cur_cell, ATTR_FORMAT, format_tbl[temp].format);
					}
				}
			}
			else {
				GetActiveTableCell (panel, menu->tableCtrl, &cur_cell);
				SetTableCellAttribute (panel, menu->tableCtrl, cur_cell, ATTR_TEXT_COLOR, format_tbl[temp].color);
				SetTableCellAttribute (panel, menu->tableCtrl, cur_cell, ATTR_FORMAT, format_tbl[temp].format);
			}
		default:
			break;
	}

	return 0;
}

static int CVICALLBACK table_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	menu_box__struct_t* menu = callbackData;
	int temp;
	Point cur_cell;
	switch (event) {
		case EVENT_ACTIVE_CELL_CHANGE:
			if (menu == NULL)
				return 0;
			GetActiveTableCell (panel, menu->tableCtrl, &cur_cell);
			GetTableCellAttribute (panel, menu->tableCtrl, cur_cell, ATTR_FORMAT, &temp);
			for(int i = 0; i < (sizeof(format_tbl) / sizeof(format_tbl[0])); i++) {
				if (format_tbl[i].format != temp)
					continue;
				SetCtrlVal(panel, menu->format_data, i);
				break;
			}
		default:
			break;
	}

	return 0;
}

