/***************************************************************************//**
 * @file add_clock.c.
 * @brief Модуль, реализующий управление мезонином Add Clock.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/framework.h"
#include "DRV/ISL55161/isl55161.h"

typedef enum {
    ISL55161_IDLE    = 0,
    ISL55161_WAIT    = 1,
    ISL55161_NEED_CB = 2,
}isl55161__state_e;

static isl55161__struct_t isl55161__hw_data;
static struct {
    framework__sub_t cout_sub;
    isl55161_hw_func_t hw_transport;       // Указатель на функцию-обработчик транспортного уровня.
    isl55161__state_e state;
    events__e event;
    uint8_t id;
    uint32_t addr, len;
    isl55161_cb_t cb;
    void* ext_data;
}isl55161_data;

static void isl55161__cout();

events__e isl55161__init(isl55161_hw_func_t hw_transport) {
    isl55161_data.hw_transport = hw_transport;
    framework__cout_subscribe(&isl55161_data.cout_sub, isl55161__cout);
    return EVENT__OK;
}

isl55161__struct_t* isl55161__get_struct(uint8_t id) {
    return &isl55161__hw_data;
}

events__e isl55161__read(uint8_t id, uint32_t addr, int len, isl55161_cb_t cb, void* ext_data) {
    uint32_t cur_addr = addr >> 1;
    uint16_t* buff = (uint16_t*)&isl55161__hw_data + cur_addr;
    if ((id % 2) || (cur_addr == 0x56)  || (cur_addr == 0x57))
        cur_addr += 0x80;
    if (isl55161_data.state != ISL55161_IDLE)
        return EVENT__BUSY;
    isl55161_data.state = ISL55161_WAIT;
    if (isl55161_data.hw_transport(id / 2, ISL55161_OP_RD, cur_addr, buff, len) != EVENT__OK) {
        isl55161_data.state = ISL55161_IDLE;
        return EVENT__BUSY;
    }
    isl55161_data.id = id;
    isl55161_data.addr = addr;
    isl55161_data.len = len;
    isl55161_data.cb = cb;
    isl55161_data.ext_data = ext_data;
    return EVENT__OK;
}

events__e isl55161__write(uint8_t id, uint32_t addr, int len, isl55161_cb_t cb, void* ext_data) {
    uint32_t cur_addr = addr >> 1;
    uint16_t* buff = (uint16_t*)&isl55161__hw_data + cur_addr;
    if ((id % 2) || (cur_addr == 0x56)  || (cur_addr == 0x57))
        cur_addr += 0x80;
    if (isl55161_data.state != ISL55161_IDLE)
        return EVENT__BUSY;
    isl55161_data.state = ISL55161_WAIT;
    if (isl55161_data.hw_transport(id / 2, ISL55161_OP_WR, cur_addr, buff, len) != EVENT__OK) {
        isl55161_data.state = ISL55161_IDLE;
        return EVENT__BUSY;
    }
    isl55161_data.id = id;
    isl55161_data.addr = addr;
    isl55161_data.len = len;
    isl55161_data.cb = cb;
    isl55161_data.ext_data = ext_data;
    return EVENT__OK;
}

void isl55161__rw_cb(uint8_t spi_id, events__e event, uint32_t addr, uint16_t* buff, int len) {
    isl55161_data.event = event;
    isl55161_data.state = ISL55161_NEED_CB;
}

static void isl55161__cout() {
    if (isl55161_data.state == ISL55161_NEED_CB) {
        isl55161_data.state = ISL55161_IDLE;
        if (isl55161_data.cb)
            isl55161_data.cb(isl55161_data.id, isl55161_data.event, isl55161_data.addr, isl55161_data.len, isl55161_data.ext_data);
    }
}
