/***************************************************************************//**
 * @file ext_if.c.
 * @brief Модуль, реализующий управление мезонином EXT IF.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "DRV/EXT_IF/ext_if.h"
#include "DRV/SyncClock/triple_spi.h"
#include "math.h"

#ifndef EXT_IF__SPI_ID
#define EXT_IF__SPI_ID 1
#endif

static struct {
    ext_if__transport_t hw_spi;
}ext_if__data;

/*******************************************************************************
 * Функция инициализации мезонина Ext If.
 ******************************************************************************/
events__e ext_if__init(ext_if__transport_t hw_spi_func) {
    ext_if__data.hw_spi = hw_spi_func;

    return EVENT__OK;
}

/*******************************************************************************
 * Функция инициализации интерфейса SPI мезонина Ext If.
 ******************************************************************************/
events__e ext_if__spi_init(uint8_t spi_id, uint8_t spi_mode, uint8_t spi_pwr, uint8_t spi_clk_mode, uint32_t spi_freq) {
    ext_if__spi_struct_t buff;
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->spi.ctrl, (uint8_t*)&buff.ctrl, sizeof(buff.ctrl));
    buff.ctrl.clk_free_run = spi_clk_mode;
    buff.ctrl.read_count = buff.ctrl.write_count = 0;
    buff.ctrl.ss1 = buff.ctrl.ss2 = buff.ctrl.ss3 = buff.ctrl.ss4 = buff.ctrl.start = 0;
    switch(spi_id) {                 // Выбор CS вывода
        case 0:
            buff.ctrl.ss1 = 1;
            break;
        case 1:
            buff.ctrl.ss2 = 1;
            break;
        case 2:
            buff.ctrl.ss3 = 1;
            break;
        case 3:
            buff.ctrl.ss4 = 1;
            break;
        default:
            return EVENT__PARAM_NA;
    }
    switch(spi_mode) {               // выбор режима работы SPI
        case 0:
            buff.ctrl.cpha = 0;
            buff.ctrl.cpol = 0;
            break;
        case 1:
            buff.ctrl.cpha = 1;
            buff.ctrl.cpol = 0;
            break;
        case 2:
            buff.ctrl.cpha = 0;
            buff.ctrl.cpol = 1;
            break;
        case 3:
            buff.ctrl.cpha = 1;
            buff.ctrl.cpol = 1;
            break;
        default:
            return EVENT__PARAM_NA;
    }
    buff.ctrl.clk_div = 0;
    int x = log2(48000000 / spi_freq); // Установка частоты
    if ((x > 0) && (x <= 16)) {
        x -= 1;
        buff.ctrl.clk_div = x;
    }
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->spi.ctrl, (uint8_t*)&buff.ctrl, sizeof(buff.ctrl));
    ext_if__pwr pwr_buff;
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->pwr_cfg, (uint8_t*)&pwr_buff, sizeof(pwr_buff));
    pwr_buff.relay.ext_pwr_spi = pwr_buff.relay.int_pwr_spi = pwr_buff.relay.spi_2v5_3v3 = 0;
    switch(spi_pwr) {               // выбор способа питания SPI
        case EXT_IF__SPI_PWR_EXT:
            pwr_buff.relay.ext_pwr_spi = 1;
            break;
        case EXT_IF__SPI_PWR_INT_2_5_EXT:
            pwr_buff.relay.ext_pwr_spi = 1; // без break
        case EXT_IF__SPI_PWR_INT_2_5:
            pwr_buff.relay.int_pwr_spi = 1;
            pwr_buff.relay.spi_2v5_3v3 = 1;
            break;
        case EXT_IF__SPI_PWR_INT_3_3_EXT:
            pwr_buff.relay.ext_pwr_spi = 1; // без break
        case EXT_IF__SPI_PWR_INT_3_3:
            pwr_buff.relay.int_pwr_spi = 1;
            break;
        default:
            return EVENT__PARAM_NA;
    }
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->pwr_cfg, (uint8_t*)&pwr_buff, sizeof(pwr_buff));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция отправки\приема данных по внешнему интерфейсу SPI мезонина ExtIf.
 ******************************************************************************/
events__e ext_if__spi_txrx(uint8_t spi_id, uint8_t* tx_buff, uint32_t tx_len, uint8_t* rx_buff, uint32_t rx_len) {
    if ((tx_len > EXT_IF__BUFF_SIZE) || (rx_len > EXT_IF__BUFF_SIZE))
        return EVENT__PARAM_NA;
    ext_if__spi_struct_t buff;
    do {
        ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->spi.status, (uint8_t*)&buff.status, sizeof(buff.status));
    }while(buff.status.busy);
    for(int i = 0; i < tx_len; i+= 4) {
        ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->spi_data_write, (uint8_t*)&tx_buff[i], 4);
    }
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->spi.ctrl, (uint8_t*)&buff.ctrl, sizeof(buff.ctrl));
    buff.ctrl.write_count = tx_len;
    buff.ctrl.read_count = rx_len;
    buff.ctrl.start = 1;                           // Старт транзакции
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->spi.ctrl, (uint8_t*)&buff.ctrl, sizeof(buff.ctrl));
    do {
        ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->spi.status, (uint8_t*)&buff.status, sizeof(buff.status));
    }while(buff.status.busy);
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->spi.ctrl, (uint8_t*)&buff.ctrl, sizeof(buff.ctrl));
    if (buff.ctrl.read_count != rx_len)
        return EVENT__ERROR;
    if ((rx_buff) && (rx_len)) {                   // Читаем данные
        for(int i = 0; i < rx_len; i+= 4) {
            ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->spi_data_read, (uint8_t*)&rx_buff[i], 4);
        }
    }
    return EVENT__OK;
}

/*******************************************************************************
 * Функция инициализации интерфейса I2C мезонина Ext If.
 ******************************************************************************/
events__e ext_if__i2c_init(ext_if__speed_e speed, ext_if__i2c_pwr_mode_e pwr_mode, uint32_t timeout) {
    ext_if__i2c_struct_t buff;
    ext_if__pwr pwr_buff;
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->pwr_cfg, (uint8_t*)&pwr_buff, sizeof(pwr_buff));
    pwr_buff.relay.ext_pwr_i2c = pwr_buff.relay.int_pwr_i2c = pwr_buff.relay.i2c_2v5_3v3 = 0;
    switch(pwr_mode) {               // выбор способа питания I2C
        case EXT_IF__I2C_PWR_EXT:
            pwr_buff.relay.ext_pwr_i2c = 1;
            break;
        case EXT_IF__I2C_PWR_INT_2_5_EXT:
            pwr_buff.relay.ext_pwr_i2c = 1; // без break
        case EXT_IF__I2C_PWR_INT_2_5:
            pwr_buff.relay.int_pwr_i2c = 1;
            pwr_buff.relay.i2c_2v5_3v3 = 1;
            break;
        case EXT_IF__I2C_PWR_INT_3_3_EXT:
            pwr_buff.relay.ext_pwr_i2c = 1; // без break
        case EXT_IF__I2C_PWR_INT_3_3:
            pwr_buff.relay.int_pwr_i2c = 1;
            break;
        default:
            return EVENT__PARAM_NA;
    }
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->pwr_cfg, (uint8_t*)&pwr_buff, sizeof(pwr_buff));
    // Установка таймаута
    if (timeout) {
        ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->i2c.control, (uint8_t*)&buff.timeout, sizeof(buff.timeout));
        buff.timeout.mext = buff.timeout.sext = timeout;
        ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->i2c.control, (uint8_t*)&buff.timeout, sizeof(buff.timeout));
    }
    // Установка скорости I2C и включение интерфейса
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->i2c.control, (uint8_t*)&buff.control, sizeof(buff.control));
    buff.control.speed = 400000 / (1 << speed);
    buff.control.en = 1;
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->i2c.control, (uint8_t*)&buff.control, sizeof(buff.control));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция управления интерфейсом I2C мезонина Ext If.
 ******************************************************************************/
events__e ext_if__i2c_ctrl(ext_if__i2c_op_e cmd, uint8_t* data) {
    ext_if__i2c_struct_t buff;
    switch(cmd) {
        case EXT_IF__I2C_OP_RD:
        case EXT_IF__I2C_OP_RD_NACK:
        case EXT_IF__I2C_OP_WRITE:
            if (data == NULL)
                return EVENT__PARAM_NA;
            buff.cmd.data_w = *data;  // без break!
        case EXT_IF__I2C_OP_START:
        case EXT_IF__I2C_OP_STOP:
            buff.cmd.oper = cmd;
            break;
        default:
            return EVENT__PARAM_NA;
    }
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->i2c.cmd, (uint8_t*)&buff.cmd, sizeof(buff.cmd));
    do {
        ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->i2c.result, (uint8_t*)&buff.result, sizeof(buff.result));
    }while(buff.result.busy);
    *data = buff.result.data_r;
    if(buff.result.nack)
        return EVENT__ERROR;
    if (buff.result.timeout_mext || buff.result.timeout_sext)
        return EVENT__TIMEOUT;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция установки состояния внешнего вывода мезонина Ext If.
 ******************************************************************************/
events__e ext_if__gpio_set(uint32_t gpio, ext_if__gpio_state_e state) {
    ext_if__gpio_ctrl buff;
    if (gpio > 15)
        return EVENT__PARAM_NA;
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->gpio, (uint8_t*)&buff, sizeof(buff));
    switch(state) {
        case EXT_IF__GPIO_LOW:
            buff.gnd_0_15 |= (1 << gpio);
            break;
        case EXT_IF__GPIO_HI_Z:
            buff.gnd_0_15 &= ~(1 << gpio);
            break;
        default:
            return EVENT__PARAM_NA;
    }
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->gpio, (uint8_t*)&buff, sizeof(buff));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция запроса состояния внешнего вывода мезонина Ext If.
 ******************************************************************************/
ext_if__gpio_state_e ext_if__gpio_get(uint32_t gpio) {
    ext_if__gpio_ctrl buff;
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->gpio, (uint8_t*)&buff, sizeof(buff));
    if (buff.gnd_0_15 & (1 << gpio))
        return EXT_IF__GPIO_LOW;
    return EXT_IF__GPIO_HI_Z;
}

/*******************************************************************************
 * Функция управления внешними источниками питания мезонина ExtIf.
 ******************************************************************************/
events__e ext_if__pwr_ctrl(ext_if__pwr_id_e pwr, uint8_t pwr_on) {
    ext_if__aux_pwr_ctrl buff;
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_READ, EXT_IF_REG->aux_pwr, (uint8_t*)&buff, sizeof(buff));
    switch(pwr) {
        case EXT_IF__PWR_5V:
            buff.aux_5v = pwr_on;
            break;
        case EXT_IF__PWR_12V:
            buff.aux_12v = pwr_on;
            break;
        case EXT_IF__PWR_N12V:
            buff.aux_neg_12v = pwr_on;
            break;
        default:
            return EVENT__PARAM_NA;
    }
    ext_if__data.hw_spi(EXT_IF__SPI_ID, TSPI__OPER_WRITE, EXT_IF_REG->aux_pwr, (uint8_t*)&buff, sizeof(buff));
    return EVENT__OK;
}
