/***************************************************************************//**
 * @file triple_spi.с.
 * @brief Модуль, реализующий управление аппаратным SPI.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "DRV/SyncClock/triple_spi.h"
#include "DRV/ISL55161/isl55161.h"
#include "target.h"

#define SPI_WR (*spi_addr)
#define SPI_RD (*(spi_addr + 1))
#define ERROR_CNTR 5

typedef enum {
	CMD_REG_READ = 0,
	CMD_REG_WRITE = 1,
}cmd_reg_rw_e;

static uint8_t cmd_buff[100];


static uint32_t* spi_addr_tbl[] = {(uint32_t*)0x48000100, (uint32_t*)0x48000108, (uint32_t*)0x48000110, (uint32_t*)0x4800000C, (uint32_t*)0x48000200, (uint32_t*)0x48000208, (uint32_t*)0x48000110, (uint32_t*)0x4800000C};

extern void transmit_data(uint8_t* tx_buff, int len, uint8_t processor, tester__state_e state, triple_spi__oper_e oper);
extern uint32_t* transmit_get_state();
uint32_t triple_spi__cmd_rw(cmd_reg_rw_e cmd, uint32_t addr, uint32_t data);


void triple_spi__ini () {
	
}


/*******************************************************************************
 * Функция работы с SPI.
 ******************************************************************************/
events__e spi_exec(triple_spi__id_e spi_id, triple_spi__oper_e oper, uint16_t addr, uint8_t* data, int size) {
    uint32_t* spi_addr = spi_addr_tbl[spi_id];
    if ((spi_id % 4) == TSPI__SYNC_PULSE_1) {   // Формирование синхроимпульса
        SPI_WR = *data;
        return EVENT__OK;
    }
    if (data) {
        for (int i = 0; i < size; i++) {
            while(SPI_RD & 0x02);           // BUSY
            if ((spi_id % 4) != TSPI__HMC987)
                SPI_WR = (data[i] << 16) | (oper << 5) | (((addr + i) >> 8) & 0x1F) | (((addr + i) & 0xFF) << 8);
            else
                SPI_WR = (data[i] << 16) | (oper << 5) | (((addr + i) & 0xF) << 1);
            if (oper == TSPI__OPER_READ) {
                while (SPI_RD & 0x02);           // BUSY
                data[i] = (uint8_t)(SPI_RD >> 8);
            }
        }
    }
    return (SPI_RD & 0x02) ? EVENT__BUSY : EVENT__OK;
}


static ext_spi_t ext_spi_default_addr_tbl = {(uint32_t*)0x48050004, (uint32_t*)0x48050008, (uint32_t*)0x48050000, (uint32_t*)0x4805000C};
static ext_spi_t ext_spi_addr_tbl = {(uint32_t*)0x48050004, (uint32_t*)0x48050008, (uint32_t*)0x48050000, (uint32_t*)0x4805000C};


ext_spi_t* triple_spi__add_clock_get_tbl() {
	return  &ext_spi_addr_tbl;
}


void triple_spi__set_default() {
	memcpy(&ext_spi_addr_tbl, &ext_spi_default_addr_tbl, sizeof(ext_spi_addr_tbl));
	
}

/*******************************************************************************
 * Функция работы с SPI мезонина Add Clock.
 ******************************************************************************/
events__e triple_spi__ext_module(int spi_id, int oper, uint32_t addr, uint8_t* data, int size) {
	int error = 10;
    uint32_t buff = 0, offset;
	uint8_t cur_spi = (spi_id == ADD_CLK__SPI_ID) ? 1 : 0; // 0- Ext If, 1- Add Clock
    for(offset = 0; offset < size; offset += 4) {
		for(error = ERROR_CNTR; (triple_spi__cmd_rw(CMD_REG_READ, (uint32_t)ext_spi_addr_tbl.status, buff) & 0x01) && error; error--);
		if (!error)
			return EVENT__ERROR;
		memcpy(&buff, data + offset, 4);
		triple_spi__cmd_rw(CMD_REG_WRITE, (uint32_t)ext_spi_addr_tbl.write, buff); //*ext_spi_addr_tbl.write = buff;
		for(error = ERROR_CNTR; (triple_spi__cmd_rw(CMD_REG_READ, (uint32_t)ext_spi_addr_tbl.status, buff) & 0x01) && error; error--);
		if (!error)
			return EVENT__ERROR;
        buff = ((addr + offset) & 0x7FFF) | (oper << 15) | (cur_spi << 16);
        triple_spi__cmd_rw(CMD_REG_WRITE, (uint32_t)ext_spi_addr_tbl.ctrl, buff); //*ext_spi_addr_tbl.ctrl = buff;
		if (oper == TSPI__OPER_READ) {
			for(error = ERROR_CNTR; (triple_spi__cmd_rw(CMD_REG_READ, (uint32_t)ext_spi_addr_tbl.status, buff) & 0x01) && error; error--);
			if (!error)
				return EVENT__ERROR;
			buff = triple_spi__cmd_rw(CMD_REG_READ, (uint32_t)ext_spi_addr_tbl.read, buff); // buff = *ext_spi_addr_tbl.read;
            memcpy(data + offset, &buff, 4);
		}
	}
    return EVENT__OK;
}

uint32_t triple_spi__cmd_rw(cmd_reg_rw_e cmd, uint32_t addr, uint32_t data) {
	uint32_t res = 0;
	int cmd_len = 0;
	while(transmit_get_state() == NULL); 
	switch(cmd) {
		case CMD_REG_READ:
				*(uint16_t*)cmd_buff = 0x0181;		// memory_rd
				*(uint16_t*)&cmd_buff[2] = 5;		// len
				cmd_buff[4] = 4;                    // 4 ????? ??????
				*(uint32_t*)&cmd_buff[5] = addr;
				cmd_len = 9;
				transmit_data(cmd_buff, cmd_len, CMD_PROCESSOR, STATE_OPER_REG, TSPI__OPER_READ);	
				while(transmit_get_state() == NULL);
				res = *transmit_get_state();
			break;
		case CMD_REG_WRITE:
				*(uint16_t*)cmd_buff = 0x0180;		// memory_wr
				*(uint16_t*)&cmd_buff[2] = 9;		// len
				cmd_buff[4] = 4;                    // 4 ????? ??????
				*(uint32_t*)&cmd_buff[5] = addr;
				*(uint32_t*)&cmd_buff[9] = data;	
				cmd_len = 13;
				transmit_data(cmd_buff, cmd_len, CMD_PROCESSOR, STATE_OPER_REG, TSPI__OPER_WRITE);	
				while(transmit_get_state() == NULL);
			break;
	}
	return res;
}


events__e spi_isl55161_exec(uint8_t spi_id, uint8_t oper, uint32_t addr, uint16_t* buff, int len) {
	
	isl55161__rw_cb(spi_id, EVENT__OK, addr, buff, len);	
    return EVENT__OK;
}



	

