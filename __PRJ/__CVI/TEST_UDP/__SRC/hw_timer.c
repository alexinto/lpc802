/***************************************************************************//**
 * @file hw_timer.c.
 * @brief Молуль аппаратного таймера (реализация для CVI).
 * @date 20.12.2016
 * @author p.adaskin
 ******************************************************************************/
#include <userint.h>
#include <asynctmr.h>
#include <utility.h>

#include "target.h"
#include "System/framework.h"
#include "System/hw_timer.h"
#include "windows_api.h"

// Внутренняя структура модуля аппаратного таймера.
typedef struct
{
    hw_timer__isr_handler_t isr_handler;    // Указатель на обработчик, вызывемый в прерывании аппаратного таймера.
    sw_timer__event_handler_t event_handler;
    sw_timer__sys_time_t alarm;
} hw_timer__t;

static hw_timer__t hw_timer = {.alarm.ms = -1};
static int hw_timer__sys_timer_ms_cb (int reserved, int timerId, int event, void *callbackData, int eventData1, int eventData2);

/***************************************************************************//**
 * Функция инициализации аппаратного таймера, используемого для реализации программных таймеров.
 ******************************************************************************/
events__e hw_timer__init(hw_timer__isr_handler_t isr_handler, sw_timer__event_handler_t event_handler)
{
    if (isr_handler == NULL)
        return EVENT__PARAM_NA;
    hw_timer.isr_handler = isr_handler;
    hw_timer.event_handler = event_handler;
    hw_timer.alarm.ms = -1;
    Timer();
    return EVENT__OK;
}
/***************************************************************************//**
 * Указание  hw_timer запуститься до указанного времени
 ******************************************************************************/
void hw_timer__start(sw_timer__sys_time_t alarm)
{
    CRITICAL_SECTION_ON
        hw_timer.alarm = alarm;
    CRITICAL_SECTION_OFF
}
/***************************************************************************//**
 * Функция внешнего вызова для обработки текущего состояния модуля hw_timer
 ******************************************************************************/
void hw_timer__cout()
{
    if(hw_timer.alarm.ms < 0)
        return;
    sw_timer__sys_time_t now;
	hw_timer__sys_time_get(&now);
    int i;
    CRITICAL_SECTION_ON
        i = sw_timer__time_compare(now, hw_timer.alarm);
    CRITICAL_SECTION_OFF
    if(i >= 0)
    {
        hw_timer.alarm.ms = -1;
        hw_timer.isr_handler();
    }
}
/*******************************************************************************
 * Функция получения системного времени.
 ******************************************************************************/
void hw_timer__sys_time_get(sw_timer__sys_time_t* time)
{
    double timer = Timer();
    sw_timer__sys_time_t ret;
    ret.timestamp = (uint32_t)timer;
    ret.ms = (int16_t)((timer - ret.timestamp)*1000);
	if (time)
		*time = ret;
}
