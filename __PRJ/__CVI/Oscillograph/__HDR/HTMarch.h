
#ifndef _HTMARCH_H
#define _HTMARCH_H

#define HTMARCH_API extern "C"__declspec(dllimport)

typedef enum {
	CH1 = 0,
	CH2 = 1,
}ch_num_e;

typedef enum {
	d20mV  = 0, 
	d50mV  = 1, 
	d100mV = 2, 
	d200mV = 3,
	d500mV = 4,
	d1V    = 5,
	d2V    = 6,
	d5V    = 7,
}volt_div_e;

typedef enum {
	t96Ms  = 8,
	t48Ms  = 9,
	t24Ms  = 10,
	t16Ms  = 11,
	t8Ms   = 12,
	t4Ms   = 13, 
	t1Ms   = 24, 
	t500Ks = 25, 
	t200Ks = 26, 
	t100Ks = 27, 	
}time_div_e;

typedef enum {
	AUTO   = 0,
	NORMAL = 1,
	SINGLE = 2,
}trigger_mode_e;

typedef enum {
	RISE = 0,
	FALL = 1,
}trigger_front_e;

short WINAPI dsoOpenDevice(WORD DeviceIndex);
HTMARCH_API short WINAPI dsoSetVoltDIV(WORD DeviceIndex,int nCH,int nVoltDIV);
HTMARCH_API short WINAPI dsoSetTimeDIV(WORD DeviceIndex,int nTimeDIV);
HTMARCH_API short WINAPI dsoReadHardData(WORD DeviceIndex,short* pCH1Data, short* pCH2Data,ULONG nReadLen,short* pCalLevel,int nCH1VoltDIV,int nCH2VoltDIV,short nTrigSweep,short nTrigSrc,short nTrigLevel,short nSlope,int nTimeDIV,short nHTrigPos,ULONG nDisLen,ULONG* nTrigPoint,short nInsertMode);
HTMARCH_API short WINAPI dsoReadHardData_LA(WORD DeviceIndex,short* pCH1Data, short* pCH2Data,ULONG nReadLen,int nTimeDIV);
HTMARCH_API WORD WINAPI dsoGetCalLevel(WORD DeviceIndex,short* level,short nLen);

HTMARCH_API short WINAPI dsoCalibrate(WORD nDeviceIndex,int nTimeDIV,int nCH1VoltDIV,int nCH2VoltDIV,short* pCalLevel);
HTMARCH_API WORD WINAPI dsoSetCalLevel(WORD DeviceIndex,short* level,short nLen);

HTMARCH_API short WINAPI dsoSetSquareFreq(WORD DeviceIndex,int nFreq);
HTMARCH_API short WINAPI dsoChooseDevice(WORD DeviceIndex,USHORT deviceType);//0:LA  1:6022D

#endif
