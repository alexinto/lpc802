#include <userint.h>
#include <cvirte.h>
#include <windows.h>
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/list.h"
#include "process.h"
#include "ini.h"
#include "HTMarch.h"
#include "oscillograph.h"

#define OSCIL_BUFF_LEN 1000
#define OSCIL_BUFF_NUM 1300

typedef i16(*ptr_buff)[OSCIL_BUFF_LEN];
typedef u32(*ptr_x)[OSCIL_BUFF_LEN];

typedef struct {
	i32 scale;
}graph_t;

struct {
	int y_offset, x_offset;
	u16 buff_wr_offset;
	volt_div_e volt_div;
	u32 trig_value, x_buff[OSCIL_BUFF_LEN * OSCIL_BUFF_NUM];
    i16 ch1_buff[OSCIL_BUFF_LEN * OSCIL_BUFF_NUM], ch2_buff[OSCIL_BUFF_LEN * OSCIL_BUFF_NUM], cal[32];
	ptr_buff ch1_ptr, ch2_ptr;
	ptr_x x_ptr;
    sw_timer__t timer;
	graph_t graph;
} oscil = {.ch1_ptr = (ptr_buff)oscil.ch1_buff, .ch2_ptr = (ptr_buff)oscil.ch2_buff, .x_ptr = (ptr_x)oscil.x_buff};

static int panelHandle;

static int CVICALLBACK main_cout(void *functionId);
static int CVICALLBACK oscil_cout(void *functionId);
static void timer_cb (struct sw_timer__t *timer, void *callbackData);




int main (int argc, char *argv[]) {
    if (InitCVIRTE (0, argv, 0) == 0)
        return -1;  /* out of memory */
    if ((panelHandle = LoadPanel (0, "oscillograph.uir", PANEL)) < 0)
		return -1;

	sw_timer__init(NULL);
	main_cout(NULL);

	for(int i = 0; i < OSCIL_BUFF_LEN * OSCIL_BUFF_NUM; i++)
		oscil.x_buff[i] = i;
	int device = 0;

	oscil.volt_div = d200mV;

	device = dsoOpenDevice(0);
	device = dsoChooseDevice(0, 1);
	dsoSetVoltDIV(0, CH1, oscil.volt_div);
	dsoSetVoltDIV(0, CH2, oscil.volt_div);
	dsoSetTimeDIV(0, t48Ms);

	SetAxisScalingMode (panelHandle, PANEL_GRAPH, VAL_BOTTOM_XAXIS, VAL_MANUAL, 0, OSCIL_BUFF_LEN);
//	dsoCalibrate(0, t1Ms, oscil.volt_div, oscil.volt_div, oscil.cal);

	dsoGetCalLevel(0, oscil.cal, 32);
//	memset(oscil.cal, 0, 64);
//	dsoSetCalLevel(0, oscil.cal, 32);


    oscil_cout(NULL);

	DisplayPanel (panelHandle);
    RunUserInterface ();
    DiscardPanel (panelHandle);
    return 0;
}

int  CVICALLBACK QuitCallback(int panel, int event, void *callbackData, int eventData1, int eventData2) {
    if (event == EVENT_CLOSE) {
		oscil_cout(NULL);
		main_cout(NULL);
        QuitUserInterface(0);
	}
    return 0;
}

static int CVICALLBACK main_cout(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId) {

        framework__cout();
    }
    *(int*)functionId = -1;
    return 0;
}

static int CVICALLBACK oscil_cout(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, oscil_cout, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId) {
		dsoReadHardData(0, (i16*)(oscil.ch1_ptr), (i16*)(oscil.ch2_ptr), OSCIL_BUFF_LEN * OSCIL_BUFF_NUM, oscil.cal, oscil.volt_div, oscil.volt_div, AUTO, CH1, 0, RISE, t48Ms, 50, OSCIL_BUFF_LEN, &oscil.trig_value, 0);
//		dsoReadHardData(0, (i16*)(oscil.ch1_ptr + oscil.buff_wr_offset), (i16*)(oscil.ch2_ptr + oscil.buff_wr_offset), OSCIL_BUFF_LEN, oscil.cal, oscil.volt_div, oscil.volt_div, AUTO, CH1, 0, RISE, t48Ms, 50, OSCIL_BUFF_LEN, &oscil.trig_value, 0);
		oscil.buff_wr_offset = OSCIL_BUFF_NUM - 1;
		sw_timer__start(&oscil.timer, 0, timer_cb, NULL);
		while(oscil.buff_wr_offset);
	}
    *(int*)functionId = -1;
    return 0;
}


static void timer_cb (struct sw_timer__t *timer, void *callbackData) {

	u32 offset = OSCIL_BUFF_NUM - 1;//(u8)(oscil.buff_wr_offset - 1);
	i32 scale_def = oscil.graph.scale;
//	SetAxisRange (panelHandle, PANEL_GRAPH, VAL_MANUAL, 0, OSCIL_BUFF_LEN * (scale + 1), VAL_MANUAL, -100, 100);
	SetAxisScalingMode (panelHandle, PANEL_GRAPH, VAL_BOTTOM_XAXIS, VAL_MANUAL, 0, OSCIL_BUFF_LEN + OSCIL_BUFF_LEN * scale_def);

	DeleteGraphPlot (panelHandle, PANEL_GRAPH, -1, VAL_DELAYED_DRAW);
	for (i32 scale = scale_def; scale >= 0; scale--)
		PlotXY (panelHandle, PANEL_GRAPH, oscil.x_buff + OSCIL_BUFF_LEN * scale, &oscil.ch1_buff[(offset - scale_def + scale) * OSCIL_BUFF_LEN], OSCIL_BUFF_LEN, VAL_UNSIGNED_INTEGER, VAL_SHORT_INTEGER, VAL_THIN_LINE, VAL_EMPTY_SQUARE, VAL_THIN_LINE, 1, VAL_YELLOW);
	oscil.buff_wr_offset = 0;
	RefreshGraph (panelHandle, PANEL_GRAPH);
}


int CVICALLBACK bt_pause_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			oscil_cout(NULL);
			break;
	}
	return 0;
}


int CVICALLBACK graph_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			break;
		case EVENT_MOUSE_WHEEL_SCROLL:
			if (eventData1 == MOUSE_WHEEL_SCROLL_UP)
				eventData2 *= -1;
			oscil.graph.scale += eventData2;
			if (oscil.graph.scale < 0)
				oscil.graph.scale = OSCIL_BUFF_NUM - 1;
			else if (oscil.graph.scale > OSCIL_BUFF_NUM - 1)
				oscil.graph.scale = 0;
			sw_timer__start(&oscil.timer, 0, timer_cb, NULL);
			break;
		case EVENT_LEFT_CLICK:
			event = eventData2;  // x
		case EVENT_LEFT_CLICK_UP:
			oscil_cout(NULL);
			event = eventData2;  // x
			break;
	}
	return 0;
}
