/***************************************************************************//**
 * @file process.c
 * @brief контексты выполнения
 * @authors p.adaskin
 ******************************************************************************/
#include <userint.h>
#include <utility.h>
//#include "target.h"
#include "process.h"

int process__context_get()
{
    return CmtGetCurrentThreadID();
}
static int critical_section_lock;
void process__critical_section_on()
{
    if(critical_section_lock == 0)
        CmtNewLock (NULL, 0, &critical_section_lock);
    CmtGetLock (critical_section_lock);
}
void process__critical_section_off()
{
    CmtReleaseLock (critical_section_lock);
}
