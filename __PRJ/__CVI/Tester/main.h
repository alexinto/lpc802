/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_COM_RENEW                  2       /* control type: command, callback function: com_renew_cb */
#define  PANEL_QUITBUTTON                 3       /* control type: command, callback function: QuitCallback */
#define  PANEL_LED_5                      4       /* control type: LED, callback function: (none) */
#define  PANEL_LED_4                      5       /* control type: LED, callback function: (none) */
#define  PANEL_LED_3                      6       /* control type: LED, callback function: (none) */
#define  PANEL_LED_2                      7       /* control type: LED, callback function: (none) */
#define  PANEL_LED_1                      8       /* control type: LED, callback function: (none) */
#define  PANEL_DECORATION_6               9       /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION                 10      /* control type: deco, callback function: (none) */
#define  PANEL_BINARYSWITCH_3             11      /* control type: binary, callback function: buttons_callback */
#define  PANEL_BINARYSWITCH_2             12      /* control type: binary, callback function: buttons_callback */
#define  PANEL_BINARYSWITCH               13      /* control type: binary, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_5             14      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_4             15      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_3             16      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_2             17      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TOGGLEBUTTON_1             18      /* control type: textButton, callback function: buttons_callback */
#define  PANEL_TEXTMSG_5                  19      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_7                  20      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_4                  21      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_3                  22      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG                    23      /* control type: textMsg, callback function: (none) */
#define  PANEL_DECORATION_4               24      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_2               25      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_5               26      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_8               27      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_7               28      /* control type: deco, callback function: (none) */
#define  PANEL_DECORATION_3               29      /* control type: deco, callback function: (none) */
#define  PANEL_TEXTMSG_2                  30      /* control type: textMsg, callback function: (none) */
#define  PANEL_GRAPH_3                    31      /* control type: graph, callback function: (none) */
#define  PANEL_GRAPH_2                    32      /* control type: graph, callback function: (none) */
#define  PANEL_TEXTBOX                    33      /* control type: textBox, callback function: (none) */
#define  PANEL_TIME_PROGRAM               34      /* control type: string, callback function: (none) */
#define  PANEL_COM_CHANGE                 35      /* control type: ring, callback function: (none) */
#define  PANEL_TIMEOUT_VALUE              36      /* control type: scale, callback function: tester_timeout_cb */
#define  PANEL_TIMEOUT_VIEW               37      /* control type: numeric, callback function: (none) */
#define  PANEL_CURRENT_VIEW               38      /* control type: scale, callback function: (none) */
#define  PANEL_VOLTAGE                    39      /* control type: scale, callback function: (none) */
#define  PANEL_POWER                      40      /* control type: scale, callback function: (none) */
#define  PANEL_DECORATION_9               41      /* control type: deco, callback function: (none) */
#define  PANEL_FUN_CTRL                   42      /* control type: scale, callback function: tester_fun__cb */
#define  PANEL_CURRENT                    43      /* control type: scale, callback function: tester_current_set_cb */
#define  PANEL_TEXTMSG_6                  44      /* control type: textMsg, callback function: (none) */
#define  PANEL_DECORATION_10              45      /* control type: deco, callback function: (none) */
#define  PANEL_TEMPERATURE                46      /* control type: scale, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK buttons_callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK com_renew_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK tester_current_set_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK tester_fun__cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK tester_timeout_cb(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
