#include <rs232.h>
#include <windows.h>
#include <synchapi.h>
#include "toolbox.h"
#include <cvirte.h>
#include <userint.h>
#include "target.h"
#include "main.h"
#include "projects.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "process.h"
#include "ini.h"


#define NULL				0
#define TEST_TIMER 			0
#define TEST_TIMER_1 		1
#define TEST_TIMER_2 		2
#define TEST_TIMER_3 		3
#define TEST_TIMER_4 		4
#define TEST_TIMER_5 		5
#define TEST_TIMER_GRAPH	6
#define TEST_TIMER_COOLER	7
#define TIMER_CHIP_NUM		8

#define MAX_TIMER   		9

static void TimerCallback (struct sw_timer__t *timer, void *ext_data);
static events__e uart__cout(uart_event_e state, char cmd, char* buff);
static events__e uart__init(int uart_id);
static void uart__deinit();
static int CVICALLBACK main_cout(void *functionId);
static int CVICALLBACK uart_thread(void *functionId);
static void test_asic();
static void set_default_panel();
static void com_ports_check(int panel, int control);
static void graph_clear();

static ini__deskriptor_t* static__ini;
static uart_struct_t uart;
static tester_struct_t tester = {.current_set_value = 0};
static int panelHandle;
static sw_timer__t timers[MAX_TIMER + 1];
static long int time_program = 0;
static float mass_voltage[1000], mass_current[1000], mass_temperature[1000];
static char buff_uart[UART_BUFF], buff_uart_last[UART_BUFF];
static int uart_err, uart_no_cmd, leds[] = {PANEL_TIME_PROGRAM, PANEL_LED_1, PANEL_LED_2, PANEL_LED_3, PANEL_LED_4, PANEL_LED_5, 0, 0, 0};

static events__e uart__cout(uart_event_e state, char cmd, char* buff) {
	char temp[50];
	float cur_buff;
	events__e res = EVENT__OK;
	DisableBreakOnLibraryErrors ();
	if (state) {
		CRITICAL_SECTION_ON
		if ((uart.next_state) && (state != UART__INIT) && (state != UART__DEINIT))
			res = EVENT__BUSY;
		else {
			uart.next_state = state;
			uart.buff[0] = cmd;
			if (buff)
				memcpy(&uart.buff[1], buff, UART_BUFF - 1);
			else
				memset(&uart.buff[1], 0x00, UART_BUFF - 1);
		}
		CRITICAL_SECTION_OFF
	}
	else switch(uart.cur_state) {
		case UART__TX:
			GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &uart.uart_com);
			uart__init(uart.uart_com);
			ComWrt(uart.uart_com, uart.buff, UART_BUFF);
			uart.cur_state = UART__RX;
			break;
		case UART__RX:
		if (ComRd(uart.uart_com, uart.rx_str, UART_BUFF) >= 0) {
			switch(uart.rx_str[0]) {
			case 0x20:
				tester.on_set = 1;
				tester.current_set_value = uart.rx_str[1];
				sprintf(buff_uart, "Begin tester work, %d A\n", tester.current_set_value);
				break;
			case 0x21:
				tester.on_set = 0;
				sprintf(buff_uart, "Tester not work.\n");
				break;
			case 0x22:
				tester.current_set_value = uart.rx_str[1];
				sprintf(buff_uart, "Current set is %d A.\n", tester.current_set_value);
				break;
			case 0x23:
				cur_buff = (float)*(uint16_t*)&uart.rx_str[1];
				tester.voltage = (2.048 / 32768) * cur_buff * 11;
				SetCtrlVal (panelHandle, PANEL_VOLTAGE, tester.voltage);
				sprintf(temp, "Напряжение:%3.2fВ", tester.voltage);
				SetCtrlAttribute (panelHandle, PANEL_VOLTAGE, ATTR_LABEL_TEXT, temp);
				cur_buff = (float)*(int16_t*)&uart.rx_str[3];
				//tester.current = (cur_buff > 0) ? (0.512 / 32768 * cur_buff / 0.005) : 0;
				tester.current = cur_buff / 10;
				sprintf(temp, "Ток:%3.1fA", tester.current);
				SetCtrlVal (panelHandle, PANEL_CURRENT_VIEW, tester.current);
				SetCtrlAttribute (panelHandle, PANEL_CURRENT_VIEW, ATTR_LABEL_TEXT, temp);
				tester.power = tester.voltage * tester.current;
				sprintf(temp, "Мощность:%4dВт", tester.power);
				SetCtrlVal (panelHandle, PANEL_POWER, tester.power);
				SetCtrlAttribute (panelHandle, PANEL_POWER, ATTR_LABEL_TEXT, temp);
				tester.temperature = (float)*(int16_t*)&uart.rx_str[5];
				SetCtrlVal (panelHandle, PANEL_TEMPERATURE, tester.temperature);
				int temp_fun = uart.rx_str[7];
				sprintf(temp, "Скорость кулера: %3d %%", temp_fun);
				SetCtrlAttribute (panelHandle, PANEL_FUN_CTRL, ATTR_LABEL_TEXT, temp);
				break;
			case 0x29:
			default:
				uart_no_cmd++;
				sprintf(buff_uart, "Command not support.\n");
				break;
			}
			tester.online = 1;
		}
		else {
			tester.online = 0;
			uart_err++;
			sprintf(buff_uart, "Error communication.\n");
		}
		sprintf(uart.rx_str, "");
		CloseCom(uart.uart_com);
		uart.cur_state = UART__IDLE;
		break;
		case UART__DEINIT:
			if (uart.next_state == UART__INIT)
				uart.cur_state = UART__IDLE;
			break;
		case UART__IDLE:
		CRITICAL_SECTION_ON
			uart.cur_state = uart.next_state;
			uart.next_state = UART__IDLE;
		CRITICAL_SECTION_OFF
			break;
		default:
			uart.cur_state = UART__IDLE;
			break;
	}
	EnableBreakOnLibraryErrors ();
	return res;
}


static void test_asic() {
	int buff;
	if (tester.current_value != tester.current_set_value)
		uart__cout(UART__TX, 0x72, &tester.current_value);
	if (tester.on && (!tester.on_set))
		uart__cout(UART__TX, 0x70, &tester.current_value);
	if ((!tester.on) && tester.on_set)
		uart__cout(UART__TX, 0x71, NULL);
	if (strcmp(buff_uart, buff_uart_last)) {
		strcpy(buff_uart_last, buff_uart);
		SetCtrlVal (panelHandle, PANEL_TEXTBOX, buff_uart);
	}
	GetCtrlAttribute(panelHandle, leds[1], ATTR_ON_COLOR, &buff);
	if ((tester.online) || (!tester.on)) {
		if (buff != VAL_RED)
			SetCtrlAttribute (panelHandle, leds[1], ATTR_ON_COLOR, VAL_RED);
	}
	else if (buff != VAL_BLACK)
		SetCtrlAttribute (panelHandle, leds[1], ATTR_ON_COLOR, VAL_BLACK);
}


int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "main.uir", PANEL)) < 0)
		return -1;

	static__ini = ini__create(0, "tester.ini");
	ini__panel_get(static__ini, "windows", panelHandle);
	sw_timer__init(NULL);
	uart_thread(NULL);
	main_cout(NULL);
	sw_timer__start(&timers[TEST_TIMER], 1000, TimerCallback, (void*)panelHandle);
	com_ports_check(panelHandle, PANEL_COM_CHANGE);
	int temp = 0;
	ini__int_get(static__ini, "UART", "com_port", &temp);
	uart.uart_com = temp;
	ini__int_get(static__ini, "TESTER", "timeout", &temp);
	tester.timeout = temp;
	if (tester.timeout < 200)
		tester.timeout = 200;
	set_default_panel();

	DisplayPanel (panelHandle);
	RunUserInterface ();
	main_cout(NULL);
	uart__deinit();
	uart_thread(NULL);

	for(int x = 0; x < 9; x++)
		sw_timer__stop(&timers[x]);
 	ini__panel_put(static__ini, "windows", panelHandle);
	DiscardPanel (panelHandle);
	ini__int_put(static__ini, "UART", "com_port", uart.uart_com);
	ini__int_put(static__ini, "TESTER", "timeout", tester.timeout);
	ini__save(static__ini);
	return 0;
}

int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		QuitUserInterface(0);
	return 0;
}

int CVICALLBACK tester_timeout_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	double temp;
	if (event == EVENT_VAL_CHANGED) {
		GetCtrlVal(panel, control, &temp);
		if (temp < 0.2)
			temp = 0.2;
		else if (temp > 10)
			temp = 10;
		SetCtrlVal(panel, PANEL_TIMEOUT_VIEW, temp);
		tester.timeout = temp * 1000;
		sw_timer__start(&timers[TEST_TIMER_1], tester.timeout, TimerCallback, (void*)panel);
		sw_timer__start(&timers[TEST_TIMER_GRAPH], tester.timeout, TimerCallback, (void*)panel);
	}
	return 0;
}


int CVICALLBACK tester_fun__cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_VAL_CHANGED)
		return 0;
	GetCtrlVal(panel, control, &tester.fun_value);
	return 0;
}

int CVICALLBACK tester_current_set_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	char buff[15];
	if (event != EVENT_VAL_CHANGED)
		return 0;
	GetCtrlVal(panel, control, &tester.current_value);
	sprintf(buff, "Ток:%3dА", tester.current_value);
	SetCtrlAttribute (panel, control, ATTR_LABEL_TEXT, buff);
	return 0;
}

int CVICALLBACK com_renew_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		com_ports_check(panel, PANEL_COM_CHANGE);
	return 0;
}

int CVICALLBACK buttons_callback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int buff, x, temp, cntrls[MAX_TIMER] = {0, PANEL_TOGGLEBUTTON_1, PANEL_TOGGLEBUTTON_2, PANEL_TOGGLEBUTTON_3, PANEL_TOGGLEBUTTON_4, PANEL_TOGGLEBUTTON_5, PANEL_BINARYSWITCH,
								PANEL_BINARYSWITCH_2, PANEL_BINARYSWITCH_3, 0};
	int timeout = 1000;
	if (event != EVENT_COMMIT)
		return 0;
	GetCtrlVal (panel, control, &temp);
	for(x = 0; (x <= MAX_TIMER) && (control != cntrls[x]); x++);
	if (x <= TEST_TIMER_5) {
		tester.on = temp ? 1 : 0;
		buff = 0;
		CRITICAL_SECTION_ON
		for (int y = 1; y <= 5; y++) {
			SetCtrlVal (panel, cntrls[y], buff);
			SetCtrlVal (panel, leds[y], buff);
			sw_timer__stop(&timers[y]);
			timers[y].timeout_ms = 0;
		}
		CRITICAL_SECTION_OFF
	}
	switch (x) {
		case TEST_TIMER_1:
		case TEST_TIMER_GRAPH:
			timeout = tester.timeout;
			break;
		case TEST_TIMER_2:
		case TEST_TIMER_3:
		case TEST_TIMER_4:
		case TEST_TIMER_5:
		default:
			break;
	}

	if ((temp == 1) && (x <= MAX_TIMER)) {
		SetCtrlVal (panel, control, temp);
		sw_timer__start(&timers[x], timeout, TimerCallback, (void*)panel);
	}
	else {
		if (control == PANEL_BINARYSWITCH)
			graph_clear();
		sw_timer__stop(&timers[x]);
	}
	return 0;
}

static void TimerCallback (struct sw_timer__t *timer, void *callbackData) {
	int x, buff;
	char temp_char[80], uart_buff_temp[UART_BUFF];
	float temp_max = 0, temperature_max = 0;
	CRITICAL_SECTION_ON
	if (timer->timeout_ms)
		sw_timer__start(timer, -1, TimerCallback, callbackData);
	CRITICAL_SECTION_OFF
	for(x = 0; (x <= MAX_TIMER) && (&timers[x] != timer); x++);
	switch (x) {
	case TEST_TIMER:
		buff = ++time_program;
		sprintf(temp_char, "%3dд:%02dч:%02dм:%02dс", buff/86400, (buff%86400)/3600, (buff%3600)/60, buff%60);
		SetCtrlVal((int)callbackData, leds[x], temp_char);
		break;
	case TEST_TIMER_1:
		uart_buff_temp[0] = tester.fun_value;
		uart__cout(UART__TX, 0x73, uart_buff_temp);
		sw_timer__start(timer, tester.timeout, TimerCallback, callbackData);
		break;
	case TEST_TIMER_2:
	case TEST_TIMER_3:
	case TEST_TIMER_4:
	case TEST_TIMER_5:
		break;
	case TEST_TIMER_GRAPH:
		sw_timer__start(timer, tester.timeout, TimerCallback, callbackData);
		mass_voltage[999] = (tester.voltage > 0.01) ? tester.voltage : 0;
		mass_current[999] = (tester.current > 0.01) ? tester.current : 0;
		mass_temperature[999] = (tester.temperature > 1) ? tester.temperature : 0;
		temperature_max = 0;
		for(x = 0; x < 999; x++) {
			mass_voltage[x] = mass_voltage[x + 1];
			mass_current[x] = mass_current[x + 1];
			mass_temperature[x] = mass_temperature[x + 1];
			if (temp_max < mass_voltage[x])
				temp_max = mass_voltage[x];
			if (temp_max < mass_current[x])
				temp_max = mass_current[x];
			if (temperature_max < mass_temperature[x])
				temperature_max = mass_temperature[x];
		}
		temp_max += temp_max / 10 + 1;
		temperature_max += temperature_max / 10 + 1;
		SetAxisScalingMode ((int)callbackData, PANEL_GRAPH_3, VAL_LEFT_YAXIS, VAL_MANUAL, 0.0, temp_max);
		SetAxisScalingMode ((int)callbackData, PANEL_GRAPH_2, VAL_LEFT_YAXIS, VAL_MANUAL, 0.0, temperature_max);
		DeleteGraphPlot ((int)callbackData, PANEL_GRAPH_3, -1, VAL_IMMEDIATE_DRAW);
		PlotY ((int)callbackData, PANEL_GRAPH_3, mass_voltage, 1000, VAL_FLOAT, VAL_FAT_LINE, VAL_SOLID_SQUARE, VAL_SOLID, VAL_CONNECTED_POINTS, VAL_BLUE);
		PlotY ((int)callbackData, PANEL_GRAPH_3, mass_current, 1000, VAL_FLOAT, VAL_THIN_LINE, VAL_SOLID_SQUARE, VAL_SOLID, VAL_CONNECTED_POINTS, VAL_RED);
		DeleteGraphPlot ((int)callbackData, PANEL_GRAPH_2, -1, VAL_IMMEDIATE_DRAW);
		PlotY ((int)callbackData, PANEL_GRAPH_2, mass_temperature, 1000, VAL_FLOAT, VAL_THIN_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, VAL_CONNECTED_POINTS, VAL_YELLOW);
		GetCtrlVal((int)callbackData, PANEL_BINARYSWITCH, &buff);
		if (!buff)
			graph_clear();
		break;
	case TEST_TIMER_COOLER:
	case TIMER_CHIP_NUM:
	default:
		break;
	}
	buff = 0;
	CRITICAL_SECTION_ON
	if ((x >= TEST_TIMER_1) && (x <= TEST_TIMER_5)) {
		if (timer->timeout_ms) {
			GetCtrlVal ((int)callbackData, leds[x], &buff);
			buff ^= 1;
		}
		SetCtrlVal ((int)callbackData, leds[x], buff);
	}
	CRITICAL_SECTION_OFF
}

static int CVICALLBACK main_cout(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId)
        Sleep(1), framework__cout(), test_asic();
    *(int*)functionId = -1;
    return 0;
}

static int CVICALLBACK uart_thread(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, uart_thread, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId)
        Sleep(1), uart__cout(NULL, NULL, NULL);
    *(int*)functionId = -1;
    return 0;
}

static void com_ports_check(int panel, int control) {
    int com = -1;
	char name[24];
	ClearListCtrl (panel, control);
	while(++com <= 255)
    {
        char ncom[24];
        sprintf(ncom, "\\\\.\\COM%d", com);
        HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
        uint32_t error = 0;
        if(hport == INVALID_HANDLE_VALUE)
            switch(error = GetLastError())
            {
                case 5: break;
                default: continue;
            }
        else
            CloseHandle(hport);
        sprintf(name, "COM %d%s", com, error != 5? "": " (занят)");
		InsertListItem (panel, control, -1, name, com);
    }
}

static void set_default_panel() {
	SetCtrlVal (panelHandle, PANEL_COM_CHANGE, uart.uart_com);
	SetCtrlVal (panelHandle, PANEL_TIMEOUT_VALUE, (double)tester.timeout / 1000);
	SetCtrlVal(panelHandle, PANEL_TIMEOUT_VIEW, (double)tester.timeout / 1000);
}

static events__e uart__init(int uart_id) {
	int res;
	char comport_str[10];
	CloseCom(uart_id);
	sprintf(comport_str, "COM%d", uart_id);
	res = OpenComConfig (uart_id, comport_str, 9600, 0, 8, 1, 512, 512);
	SetComTime (uart_id, 1);
	return EVENT__OK;
}

static void uart__deinit() {
	while(uart.cur_state != UART__DEINIT)
		uart__cout(UART__DEINIT, 0, NULL);
}

static void graph_clear() {
	memset(mass_temperature, 0x00, sizeof(mass_temperature));
	memset(mass_voltage, 0x00, sizeof(mass_voltage));
	memset(mass_current, 0x00, sizeof(mass_current));
}
