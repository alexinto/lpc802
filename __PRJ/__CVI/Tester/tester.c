#include <rs232.h>
#include <windows.h>
#include <synchapi.h>
#include <toolbox.h>
#include <cvirte.h>
#include <userint.h>
#include "target.h"
#include "tester.h"
#include "projects.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/list.h"
#include "process.h"
#include "ini.h"


#define NULL				0
#define TEST_TIMER 			0
#define TEST_TIMER_1 		1
#define TEST_TIMER_2 		2
#define TEST_TIMER_3 		3
#define TEST_TIMER_4 		4
#define TEST_TIMER_5 		5
#define TEST_TIMER_GRAPH	6
#define TEST_TIMER_COOLER	7
#define TIMER_CHIP_NUM		8

#define MAX_TIMER   		9

static void TimerCallback (struct sw_timer__t *timer, void *ext_data);
static events__e uart__cout(uart_event_e state, char cmd);
static events__e uart__init(int uart_id);
static void uart__deinit();
static int CVICALLBACK main_cout(void *functionId);
static void test_asic();
static void set_default_panel();
static void com_ports_check(int panel, int control);

static ini__deskriptor_t* static__ini;
static uart_struct_t uart;
static tester_struct_t tester;
static int panelHandle;
static sw_timer__t timers[MAX_TIMER + 1];
static long int time_program = 0;
static int temp_y[20], crc_errors[20], temp_x[20] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50};
static int chip_num[] = {PANEL_CHIP_NUM_1, PANEL_CHIP_NUM_2, PANEL_CHIP_NUM_3, PANEL_CHIP_NUM_4, PANEL_CHIP_NUM_5,
						PANEL_CHIP_NUM_6, PANEL_CHIP_NUM_7, PANEL_CHIP_NUM_8, PANEL_CHIP_NUM_9, PANEL_CHIP_NUM_10,
						PANEL_CHIP_NUM_11, PANEL_CHIP_NUM_12, PANEL_CHIP_NUM_13, PANEL_CHIP_NUM_14, PANEL_CHIP_NUM_15,
						PANEL_CHIP_NUM_16, PANEL_CHIP_NUM_17, PANEL_CHIP_NUM_18, PANEL_CHIP_NUM_19, PANEL_CHIP_NUM_20,
						PANEL_CHIP_NUM_21, PANEL_CHIP_NUM_22, PANEL_CHIP_NUM_23, PANEL_CHIP_NUM_24, PANEL_CHIP_NUM_25,
						PANEL_CHIP_NUM_26, PANEL_CHIP_NUM_27, PANEL_CHIP_NUM_28, PANEL_CHIP_NUM_29, PANEL_CHIP_NUM_30,
						PANEL_CHIP_NUM_31, PANEL_CHIP_NUM_32, PANEL_CHIP_NUM_33, PANEL_CHIP_NUM_34, PANEL_CHIP_NUM_35,
						PANEL_CHIP_NUM_36, PANEL_CHIP_NUM_37, PANEL_CHIP_NUM_38, PANEL_CHIP_NUM_39, PANEL_CHIP_NUM_40,
						PANEL_CHIP_NUM_41, PANEL_CHIP_NUM_42, PANEL_CHIP_NUM_43, PANEL_CHIP_NUM_44, PANEL_CHIP_NUM_45,
						PANEL_CHIP_NUM_46, PANEL_CHIP_NUM_47, PANEL_CHIP_NUM_48, PANEL_CHIP_NUM_49, PANEL_CHIP_NUM_50,
						PANEL_CHIP_NUM_51, PANEL_CHIP_NUM_52, PANEL_CHIP_NUM_53, PANEL_CHIP_NUM_54, PANEL_CHIP_NUM_55,
						PANEL_CHIP_NUM_56, PANEL_CHIP_NUM_57, PANEL_CHIP_NUM_58, PANEL_CHIP_NUM_59, PANEL_CHIP_NUM_60,
						PANEL_CHIP_NUM_61, PANEL_CHIP_NUM_62, PANEL_CHIP_NUM_63};
static unsigned int chips_err_count[64];
static asic_struct_t asics = {.state = ASIC_STOP};
static char buff_uart[100], buff_uart_last[100];
static int uart_err, uart_packet, uart_no_cmd;

static events__e uart__cout(uart_event_e state, char cmd) {
	events__e res = EVENT__OK;
	DisableBreakOnLibraryErrors ();
	if (state) {
		CRITICAL_SECTION_ON
		if ((uart.next_state) && (state != UART__INIT))
			res = EVENT__BUSY;
		else {
			uart.cur_cmd = cmd;
			uart.next_state = state;
		}
		CRITICAL_SECTION_OFF
	}
	else switch(uart.cur_state) {
		case UART__TX:
			GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &uart.uart_com);
			uart__init(uart.uart_com);
			ComWrtByte(uart.uart_com, uart.cur_cmd);
			uart.cur_state = UART__RX;
			break;
		case UART__RX:
		if (ComRd(uart.uart_com, uart.rx_str, UART_BUFF) >= 0) {
			uart_packet++;
			switch(uart.rx_str[0]) {
			case 0x23:
				sprintf(buff_uart, "Begin test ASICs...\n");
				break;
			case 0x24:
				sprintf(buff_uart, "ADC: %dmV.\n", uart.rx_str[2] * 256 + uart.rx_str[1]);
				break;
			case 0x25:
				uart_no_cmd++;
				sprintf(buff_uart, "Command not support.\n");
				break;
			case 0x26:
				sprintf(buff_uart, "ADC not work.\n");
				break;
			default:
				uart_err++;
				sprintf(buff_uart, "Error communication.\n");
				break;
			}
			sprintf(uart.rx_str, "");
			CloseCom(uart.uart_com);
			uart.cur_state = UART__IDLE;
		}
		break;
		case UART__DEINIT:
			if (uart.next_state == UART__INIT)
				uart.cur_state = UART__IDLE;
			break;
		case UART__IDLE:
		CRITICAL_SECTION_ON
			uart.cur_state = uart.next_state;
			uart.next_state = UART__IDLE;
		CRITICAL_SECTION_OFF
			break;
		default:
			break;
	}
	EnableBreakOnLibraryErrors ();
	return res;
}


static void test_asic() {
	switch(asics.state) {
	case ASIC_START:
		break;
	case ASIC_PROC:
		break;
	case ASIC_STOP:
		break;
	case ASIC_WAIT:
		break;
	}
}


int main (int argc, char *argv[])
{

	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "tester.uir", PANEL)) < 0)
		return -1;

	static__ini = ini__create(0, "tester.ini");
	ini__panel_get(static__ini, "windows", panelHandle);
	sw_timer__init(NULL);
	main_cout(NULL);
	sw_timer__start(&timers[TEST_TIMER], 1000, TimerCallback, (void*)panelHandle);
	com_ports_check(panelHandle, PANEL_COM_CHANGE);
	int temp = 0;
	ini__int_get(static__ini, "UART", "com_port", &temp);
	uart.uart_com = temp;
	ini__int_get(static__ini, "TESTER", "timeout", &temp);
	tester.timeout = temp;
	if (tester.timeout < 200)
		tester.timeout = 200;
	set_default_panel();

	DisplayPanel (panelHandle);
	RunUserInterface ();
	uart__deinit();
	main_cout(NULL);
	for(int x = 0; x < 9; x++)
		sw_timer__stop(&timers[x]);

	ini__panel_put(static__ini, "windows", panelHandle);
	DiscardPanel (panelHandle);
	ini__int_put(static__ini, "UART", "com_port", uart.uart_com);
	ini__int_put(static__ini, "TESTER", "timeout", tester.timeout);
	ini__save(static__ini);
	return 0;
}

int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		QuitUserInterface(0);
	return 0;
}

int CVICALLBACK tester_timeout_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	double temp;
	if (event == EVENT_VAL_CHANGED) {
		GetCtrlVal(panel, control, &temp);
		SetCtrlVal(panel, PANEL_TIMEOUT_VIEW, temp);
		tester.timeout = temp * 1000;
	}
	return 0;
}

int CVICALLBACK com_renew_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		com_ports_check(panel, PANEL_COM_CHANGE);
	return 0;
}

int CVICALLBACK buttons_callback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int x, temp, cntrls[MAX_TIMER] = {0, PANEL_TOGGLEBUTTON_1, PANEL_TOGGLEBUTTON_2, PANEL_TOGGLEBUTTON_3, PANEL_TOGGLEBUTTON_4, PANEL_TOGGLEBUTTON_5, PANEL_BINARYSWITCH,
								PANEL_BINARYSWITCH_2, PANEL_BINARYSWITCH_3, 0};
	int timeout = 1000;
	if (event != EVENT_COMMIT)
		return 0;
	GetCtrlVal (panel, control, &temp);
	for(x = 0; (x <= MAX_TIMER) && (control != cntrls[x]); x++);
	if ((temp == 1) && (x <= MAX_TIMER)) {
		if (x == TEST_TIMER_1) {
			timeout = tester.timeout;
			uart__cout(UART__TX, 0x77);
		}
		else if (x == TIMER_CHIP_NUM) {
			DeleteGraphPlot (panel, PANEL_GRAPH_CHIPS_ERROR, -1, VAL_IMMEDIATE_DRAW);
			for(int count = 0; count < 63; count++) {
				SetCtrlAttribute (panel, chip_num[count], ATTR_VISIBLE, 0);
				chips_err_count[count] = 0;
			}
		}
		sw_timer__start(&timers[x], timeout, TimerCallback, (void*)panel);
	}
	else {
		sw_timer__stop(&timers[x]);
	}
	return 0;
}

static void TimerCallback (struct sw_timer__t *timer, void *callbackData) {
	int x, buff, temp, leds[] = {PANEL_TIME_PROGRAM, PANEL_LED_1, PANEL_LED_2, PANEL_LED_3, PANEL_LED_4, PANEL_LED_5, 0, 0, 0};
	char temp_char[80];
	sw_timer__start(timer, timer->timeout_ms, TimerCallback, callbackData);
	for(x = 0; (x <= MAX_TIMER) && (&timers[x] != timer); x++);
	switch (x) {
	case TEST_TIMER:
		buff = ++time_program;
		sprintf(temp_char, "%3dд:%02dч:%02dм:%02dс", buff/86400, (buff%86400)/3600, (buff%3600)/60, buff%60);
		SetCtrlVal((int)callbackData, leds[x], temp_char);
		break;
	case TEST_TIMER_1:
		uart__cout(UART__TX, 0x78);
		sw_timer__start(timer, tester.timeout, TimerCallback, callbackData);
		if (strcmp(buff_uart, buff_uart_last)) {
			strcpy(buff_uart_last, buff_uart);
			SetCtrlVal ((int)callbackData, PANEL_TEXTBOX, buff_uart);
		}
		GetCtrlVal ((int)callbackData, leds[x], &buff);
		buff ^= 1;
		SetCtrlVal ((int)callbackData, leds[x], buff);
		break;
	case TEST_TIMER_2:
	case TEST_TIMER_3:
	case TEST_TIMER_4:
	case TEST_TIMER_5:
		GetCtrlVal ((int)callbackData, leds[x], &buff);
		buff ^= 1;
		SetCtrlVal ((int)callbackData, leds[x], buff);
		break;
	case TEST_TIMER_GRAPH:
//		((int)Random (0, 2)) ? ((temp_x[19] < 100) ? temp_x[19]++: (temp_x[19] = 100)) : ((temp_x[19] > 0) ? temp_x[19]-- : (temp_x[19] = 1));
		temp_x[19] = uart_packet;
		for(x = 0; x < 19; x++)
			temp_x[x] = temp_x[x + 1];
		DeleteGraphPlot ((int)callbackData, PANEL_GRAPH, -1, VAL_IMMEDIATE_DRAW);
		PlotY ((int)callbackData, PANEL_GRAPH, temp_x, 20, VAL_INTEGER, VAL_FAT_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, VAL_CONNECTED_POINTS, VAL_RED);
		break;
	case TEST_TIMER_COOLER:
//		if (!temp_y[19])
		temp_y[19] = uart_no_cmd;
//		((int)Random (0, 2)) ? ((temp_y[19] < 4000) ? temp_y[19]+= 10: (temp_y[19] = 4000)) : ((temp_y[19] > 0) ? temp_y[19]-- : (temp_y[19] = 1));
		for(x = 0; x < 19; x++)
			temp_y[x] = temp_y[x + 1];
		DeleteGraphPlot ((int)callbackData, PANEL_GRAPH_2, -1, VAL_IMMEDIATE_DRAW);
		PlotY ((int)callbackData, PANEL_GRAPH_2, temp_y, 20, VAL_INTEGER, VAL_FAT_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, VAL_CONNECTED_POINTS, VAL_YELLOW);
		break;
	case TIMER_CHIP_NUM:
//		(crc_errors[19] < 10000) ? crc_errors[19] += Random(0, 20): (crc_errors[19] = 10100);
		crc_errors[19] = uart_err;
		for(x = 0; x < 19; x++)
			crc_errors[x] = crc_errors[x + 1];
		DeleteGraphPlot ((int)callbackData, PANEL_GRAPH_3, -1, VAL_IMMEDIATE_DRAW);
		PlotY ((int)callbackData, PANEL_GRAPH_3, crc_errors, 20, VAL_INTEGER, VAL_FAT_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, VAL_CONNECTED_POINTS, VAL_BLUE);
		buff = Random(0, 63);
		if ((buff >= 0) && (buff <= 63)) {
			sprintf(temp_char, "Chain[5] has %2d asic. \n", buff);
	//		SetCtrlVal ((int)callbackData, PANEL_TEXTBOX, temp_char);
			chips_err_count[buff]++;
			PlotY ((int)callbackData, PANEL_GRAPH_CHIPS_ERROR, chips_err_count, 63, VAL_UNSIGNED_INTEGER, VAL_VERTICAL_BAR, VAL_EMPTY_SQUARE, VAL_SOLID, 1, VAL_CYAN);
			for(x = 0; x < 63; x++) {
				(x >= buff) ? (temp = 1) : (temp = 0);
				SetCtrlAttribute ((int)callbackData, chip_num[x], ATTR_VISIBLE, temp);
			}
		}

		break;
	default:
		break;
	}
}

static int CVICALLBACK main_cout(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId)
        Sleep(1), framework__cout(), uart__cout(NULL, NULL), test_asic();
    *(int*)functionId = -1;
    return 0;
}

static void com_ports_check(int panel, int control) {
    int com = -1;
	char name[24];
	ClearListCtrl (panel, control);
	while(++com <= 255)
    {
        char ncom[24];
        sprintf(ncom, "\\\\.\\COM%d", com);
        HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
        uint32_t error = 0;
        if(hport == INVALID_HANDLE_VALUE)
            switch(error = GetLastError())
            {
                case 5: break;
                default: continue;
            }
        else
            CloseHandle(hport);
        sprintf(name, "COM %d%s", com, error != 5? "": "(занят)");
		InsertListItem (panel, control, -1, name, com);
    }
}

static void set_default_panel() {
	SetCtrlVal (panelHandle, PANEL_COM_CHANGE, uart.uart_com);
	SetCtrlVal (panelHandle, PANEL_TIMEOUT_VALUE, (double)tester.timeout / 1000);
	SetCtrlVal(panelHandle, PANEL_TIMEOUT_VIEW, (double)tester.timeout / 1000);
}

static events__e uart__init(int uart_id) {
	char comport_str[10];
	CloseCom(uart_id);
	sprintf(comport_str, "COM%d", uart_id);
	OpenComConfig (uart_id, comport_str, 19200, 0, 8, 1, 512, 512);
	OpenCom (uart_id, comport_str);
	SetComTime (uart_id, 0.1);
	return EVENT__OK;
}

static void uart__deinit() {
	while(uart.cur_state != UART__DEINIT)
		uart__cout(UART__DEINIT, 0);
}
