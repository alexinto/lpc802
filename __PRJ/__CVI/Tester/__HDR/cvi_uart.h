/***************************************************************************//**
 * @file cvi_uart.c.
 * @brief ���� � ��������� uart.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef _H_CVI_UART
#define _H_CVI_UART

#include "System/framework.h"

#define UART_BUFF 200
#define UART_TIMEOUT 0.2

typedef enum {
	EVENT__ERR = -1,
	EVENT__GET_COM_PORT = 2,
}uart_event_e;

typedef enum {
	UART__DEINIT = -2,
	UART__INIT,
	UART__IDLE = 0,
	UART__TX,
	UART__RX,
}uart_state_e;



typedef uint8_t (*uart__cb)(uart_event_e event, uint8_t* data, uint32_t len);


uart_event_e uart__init(int uart_id, uart__cb cb);

void uart__deinit();

uart_event_e uart__cout(uart_state_e state, u8 len, char* buff);





















#endif
