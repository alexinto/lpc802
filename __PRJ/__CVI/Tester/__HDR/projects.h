#ifndef PRJ
#define PRJ

#define UART_BUFF 50

typedef enum {
	ASIC_WAIT = 0,
	ASIC_START,
	ASIC_PROC,
	ASIC_STOP,
}asic_state_e;

typedef struct {
	asic_state_e state;
	unsigned int asics;
	unsigned int req 	:1;
}asic_struct_t;

typedef enum {
	UART__DEINIT = -2,
	UART__INIT,
	UART__IDLE = 0,
	UART__TX,
	UART__RX,
}uart_event_e;

typedef struct {
	char cur_cmd, rx_str[UART_BUFF], current, buff[UART_BUFF];
	int rx_buff;
	uart_event_e next_state, cur_state;
	unsigned int uart_com;
}uart_struct_t;

typedef struct {
	char on 	 :1;
	char on_set  :1;
	char off_set :1;
	char online  :1;
	char current_value, current_set_value, fun_value;
	float voltage, current, temperature;
	int timeout, power;
}tester_struct_t;

#endif
