#include <rs232.h>
#include <ansi_c.h>
#include <utility.h>
#include "cvi_uart.h"
#include "standard.h"

typedef struct {
	uint32_t len_tx, len_rx, next_len_tx;
	char buff[UART_BUFF], next_buff[UART_BUFF];	
	uart_event_e next_state, cur_state;
	unsigned int uart_com, uart_com_next;
	uart__cb cb;
}uart_struct_t;

static uart_struct_t uart; 


uart_event_e uart__init(int uart_id, uart__cb cb) {
	int res;
	char comport_str[10];
	if (cb) {
		uart.uart_com_next = uart_id;
		uart.cb = cb;
		return EVENT__OK;
	}
	CloseCom(uart_id);  
	sprintf(comport_str, "COM%d", uart_id);
	res = OpenComConfig (uart_id, comport_str, 9600, 0, 8, 1, 512, 512);
	SetComTime (uart_id, 1);
	if (res)
		return EVENT__ERR;
	return EVENT__OK;
}


uart_event_e uart__cout(uart_event_e state, char len, char* buff) {
	uart_event_e res = EVENT__OK;
	DisableBreakOnLibraryErrors (); 
	if (state) {
		CRITICAL_SECTION_ON
		if ((uart.next_state) && (state != UART__INIT) && (state != UART__DEINIT))
			res = EVENT__BUSY;
		else {
			if (len <= UART_BUFF) {
				uart.next_state = state;
				uart.next_len_tx = len;
				memcpy(uart.next_buff, buff, len);
			}
			else
				res = EVENT__ERR;
		}
		CRITICAL_SECTION_OFF
	}
	else switch(uart.cur_state) {
		case UART__TX:
			uart.uart_com = uart.uart_com_next;
			if (uart__init(uart.uart_com, NULL) != EVENT__OK) {
				uart.cur_state = UART__IDLE;
				uart.cb(EVENT__ERR, (uint8_t*)uart.buff, 0);
				return EVENT__ERR;
			}
			ComWrt(uart.uart_com, uart.buff, uart.len_tx);
			uart.cur_state = UART__RX;
			break;
		case UART__RX:
		uart.len_rx = res = ComRd(uart.uart_com, uart.buff, UART_BUFF);
		if (res > 0) {
			res = EVENT__OK;
		}
		else if (res == 0)
			res = EVENT__TIMEOUT;
		else
			res = EVENT__ERR;
		CloseCom(uart.uart_com);
		uart.cur_state = UART__IDLE;
		uart.cb(res, (uint8_t*)uart.buff, uart.len_rx);
		break;
		case UART__DEINIT:
			if (uart.next_state == UART__INIT)
				uart.cur_state = UART__IDLE;
			break;
		case UART__IDLE:
		CRITICAL_SECTION_ON
		if (uart.next_state) {
			uart.len_tx = uart.next_len_tx;
			memcpy(uart.buff, uart.next_buff, uart.len_tx);
			uart.cur_state = uart.next_state;
			uart.next_state = UART__IDLE;
			
		}
		CRITICAL_SECTION_OFF
			break;
		default:
			uart.cur_state = UART__IDLE;
			break;
	}
	EnableBreakOnLibraryErrors ();
	return res;
}

void uart__deinit() {
	while(uart.cur_state != UART__DEINIT)
		uart__cout(UART__DEINIT, 0, NULL);
}
