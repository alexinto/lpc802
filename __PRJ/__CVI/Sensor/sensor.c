#include <rs232.h>
#include <windows.h>   
#include <cvirte.h>		
#include <userint.h>
#include <time.h>
#include "toolbox.h"
#include "sensor.h"
#include "standard.h"
#include "process.h"
#include "sw_timer.h"
#include "list.h"
#include "ini.h"
#include "cvi_uart.h"
#include "crc.h"

#define u8 uint8_t
#define u16 uint16_t

typedef struct {
	int cmd_ctrl;
	uint32_t errors;
	uint8_t com_port;
	uint8_t buffer[100], time[100];
	uint16_t supply_buff[100], out_buff[100], pwr_buff[100];
	uint16_t* buffs[4];
	uint16_t timeout;
	long int time_program;
	sw_timer__t itf_timer;
	time_t timestamp;
}sensor__t;

typedef struct {
	u8 value_log    :1;
	u8 value_log_wr :1;
	u16 value_sett, value_gyst;
	char* descr;
}value_settings__t;

typedef struct {
	value_settings__t supply, out, pwr;
}sensor_settings__t;


static void settings_init();
static void settings_save();
static void TimerCallback (struct sw_timer__t *timer, void *ext_data);
static int CVICALLBACK main_cout(void *functionId); 
static int CVICALLBACK uart_thread(void *functionId); 
static void set_default_panel();
static void com_ports_check(int panel, int control);
static uint8_t sensor_itf_cb(uart_event_e event, uint8_t* data, uint32_t len); 

static ini__deskriptor_t* static__ini; 
static int panelHandle;
static sw_timer__t timer_time;
static sensor__t sensor = {.buffs = sensor.supply_buff, sensor.out_buff, sensor.pwr_buff, NULL};
static sensor_settings__t settings = {.supply.descr = "Питание", .out.descr = "Выход", .pwr.descr = "Мощность"};
static 	value_settings__t* value_descr[] = {&settings.supply, &settings.out, &settings.pwr, NULL};     

#define _CVI__1970_1_1 (-2208988800)  

int main (int argc, char *argv[])
{
	float buff;
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "sensor.uir", PANEL)) < 0)
		return -1;
	
	static__ini = ini__create(0, "sensor.ini");  
	ini__panel_get(static__ini, "windows", panelHandle);
	sw_timer__init(NULL);
	uart_thread(NULL);
	main_cout(NULL);
	sw_timer__start(&timer_time, 1000, TimerCallback, (void*)panelHandle);
	settings_init();
	if ((sensor.timeout <= 0) || (sensor.timeout > 10000))
		sensor.timeout = 1000;
	buff = (float)sensor.timeout / 1000;
	SetCtrlVal (panelHandle, PANEL_ITF_TIMEOUT, buff);
	com_ports_check(panelHandle, PANEL_COM_CHANGE);
	set_default_panel();
	uart__init(sensor.com_port, sensor_itf_cb);
	sw_timer__start(&sensor.itf_timer, sensor.timeout, TimerCallback, (void*)panelHandle);
	
	
	DisplayPanel (panelHandle);
	RunUserInterface ();
	main_cout(NULL);
	uart__deinit();
	uart_thread(NULL);
	sw_timer__stop(&timer_time);
	sw_timer__stop(&sensor.itf_timer);
 	ini__panel_put(static__ini, "windows", panelHandle);
	DiscardPanel (panelHandle);
	settings_save();
	ini__save(static__ini);
	return 0;
}

int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		QuitUserInterface(0);
	return 0;
}

int CVICALLBACK com_renew_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)
		com_ports_check(panel, PANEL_COM_CHANGE);
	return 0;
}


static void TimerCallback (struct sw_timer__t *timer, void *callbackData) {
	int buff;
	CRITICAL_SECTION_ON
		sw_timer__start(timer, -1, TimerCallback, callbackData);
		if (timer == &timer_time) {
			buff = ++sensor.time_program;
			sprintf((char*)sensor.time, "%3dд:%02dч:%02dм:%02dс", buff/86400, (buff%86400)/3600, (buff%3600)/60, buff%60);
			SetCtrlVal ((int)callbackData, PANEL_TIME_PROGRAM, sensor.time);
			sensor.timestamp = time(NULL);
		}
		else {
			switch(sensor.cmd_ctrl) {
				case 1:
					*sensor.buffer = 0x70;
					break;
				case 2:
					*sensor.buffer = 0x71;
					break;
				default:
					*sensor.buffer = 0x73;
					break;
			}
			if (uart__cout(UART__TX, UART_BUFF, (char*)sensor.buffer) == EVENT__OK)
				sensor.cmd_ctrl = 0;
				
		}
	CRITICAL_SECTION_OFF
}
   
static int CVICALLBACK main_cout(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, main_cout, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId)
        Sleep(1), sw_timer__cout();
    *(int*)functionId = -1;
    return 0;
}

static int CVICALLBACK uart_thread(void *functionId) {
    if(functionId == NULL) // Srart/Stop
    {
        volatile static int static__functionId = -1;
        if(static__functionId == -1)
            CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, uart_thread, &static__functionId, &static__functionId);
        else
            for(static__functionId = 0; static__functionId !=-1; Sleep(200));
        return 0;
    }
    while(*(int*)functionId)
        Sleep(1), uart__cout((uart_event_e)NULL, (char)NULL, NULL);
    *(int*)functionId = -1;
    return 0;
}

static void com_ports_check(int panel, int control) {
    int com = -1;
	char name[24];
	ClearListCtrl (panel, control); 
	while(++com <= 255)
    {
        char ncom[24];
        sprintf(ncom, "\\\\.\\COM%d", com);
        HANDLE hport = CreateFile(ncom, 0, 0,NULL, OPEN_EXISTING, 0, NULL);
        uint32_t error = 0;
        if(hport == INVALID_HANDLE_VALUE)
            switch(error = GetLastError())
            {
                case 5: break;
                default: continue;
            }
        else
            CloseHandle(hport);
        sprintf(name, "COM %d%s", com, error != 5? "": " (занят)");
		InsertListItem (panel, control, -1, name, com);
    }
	sprintf(name, "COM %d%s", sensor.com_port, " !");
	InsertListItem (panel, control, -1, name, sensor.com_port);
}

static void set_default_panel() {
	SetCtrlVal (panelHandle, PANEL_COM_CHANGE, sensor.com_port); 
}

int CVICALLBACK com_port_change (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event != EVENT_COMMIT)	
		return 0;
	GetCtrlVal (panelHandle, PANEL_COM_CHANGE, &sensor.com_port); 
	uart__init(sensor.com_port, sensor_itf_cb);
	return 0;
}

typedef struct {
	int graph, color;
}graph__t;


static uint8_t sensor_itf_cb(uart_event_e event, uint8_t* data, uint32_t len) {
	static char* voltage_postfix[] = {"B", "B", "Bт", ""};
	char buff[100] = {0x00};
	char crc;
	CRITICAL_SECTION_ON
		crc = (crc__8_ccitt(data, 8) == data[8]) ? 1 : 0;
	CRITICAL_SECTION_OFF

	const graph__t graph_tbl[] = {PANEL_U_PWR, VAL_RED, PANEL_U_OUT, VAL_YELLOW, PANEL_U_GND, VAL_BLUE, 0, 0};
	
	if (((event != EVENT__OK) && (event != EVENT__TIMEOUT)) || (!len)) {	
		sprintf(buff, "%s Датчик не обнаружен\n", ctime(&sensor.timestamp));
	
	}
	else if (crc) {
		switch(*data) {
			case 0x20:
				sprintf(buff, "%s Датчик включен №%d\n", ctime(&sensor.timestamp), ++sensor.errors);
				break;
			case 0x21:
			case 0x23:		
				for(int x = 0; x < 99; x++)
					for(int i = 0; i < 3; i++)
						sensor.buffs[i][x] = sensor.buffs[i][x + 1];
				DeleteGraphPlot (panelHandle, PANEL_GRAPH, -1, VAL_IMMEDIATE_DRAW);
				for(int i = 0; sensor.buffs[i] != NULL; i++) {
					sensor.buffs[i][99] = *(uint16_t*)&data[2 + i*2];
					PlotY (panelHandle, PANEL_GRAPH, sensor.buffs[i], 100, VAL_SHORT_INTEGER, VAL_THIN_LINE, VAL_SOLID_SQUARE, VAL_THIN_LINE, VAL_EMPTY_SQUARE, graph_tbl[i].color);
					sprintf(buff, "%d.%03d %s", sensor.buffs[i][99] / 1000, sensor.buffs[i][99] % 1000, voltage_postfix[i]);
					SetCtrlVal (panelHandle, graph_tbl[i].graph, buff);
					if (value_descr[i]->value_log) {
						if (value_descr[i]->value_log_wr) {
							if (sensor.buffs[i][99] >= value_descr[i]->value_sett) {
								value_descr[i]->value_log_wr = 0;
								sprintf(buff, "%s %s: %dмВ\n", ctime(&sensor.timestamp), value_descr[i]->descr, sensor.buffs[i][99]);
								SetCtrlVal (panelHandle, PANEL_TEXTBOX, buff);
							}
						}
						else if ((value_descr[i]->value_log_wr == 0) && (sensor.buffs[i][99] < value_descr[i]->value_sett - value_descr[i]->value_gyst)) {
							value_descr[i]->value_log_wr = 1;
							sprintf(buff, "%s %s: %dмВ\n", ctime(&sensor.timestamp), value_descr[i]->descr, sensor.buffs[i][99]);
							SetCtrlVal (panelHandle, PANEL_TEXTBOX, buff);
						}
					}
				}
				if (*data == 0x23)
					*buff = 0x00;
				else
					sprintf(buff, "%s Датчик выключен №%d\n", ctime(&sensor.timestamp), ++sensor.errors);
				break;
		}
	}
	else 
		sprintf(buff, "%s Ошибка данных №%d\n", ctime(&sensor.timestamp), ++sensor.errors);
	if (*buff)
		SetCtrlVal (panelHandle, PANEL_TEXTBOX, buff);
	return 0;	
}

int CVICALLBACK set_itf_timeout_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	
	float buff;
	if (event == EVENT_VAL_CHANGED) {
		GetCtrlVal (panelHandle, PANEL_ITF_TIMEOUT, &buff);
		sensor.timeout = buff * 1000;
		sw_timer__start(&sensor.itf_timer, sensor.timeout, TimerCallback, (void*)panelHandle); 
	}
	return 0;
}

int CVICALLBACK log_clear_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT)	
		ResetTextBox (panelHandle, PANEL_TEXTBOX, "");
	return 0;
}

int CVICALLBACK settings_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	int cntrl_tbl[] = {PANEL_U_SET_1, PANEL_U_SET_2, PANEL_U_SET_3, PANEL_U_GYST_1, PANEL_U_GYST_2, PANEL_U_GYST_3, 0};
	u16* cntrl_values[] = {&settings.supply.value_sett, &settings.out.value_sett, NULL, &settings.supply.value_gyst, &settings.out.value_gyst, NULL, NULL};
	int buff = 0, idx = 0;
	if (event != EVENT_COMMIT)	
		return 0;
	GetCtrlVal (panelHandle, control, &buff);
	switch(control) {
		case PANEL_LOG_1:
			 settings.supply.value_log = (u8)buff;
			break;
		case PANEL_LOG_2:
			 settings.out.value_log = (u8)buff;
			break;
		case PANEL_LOG_3:
			break;
		default:
			while((cntrl_tbl[idx] != control) && (cntrl_tbl[idx])) idx++;
			if (cntrl_values != NULL)
				*cntrl_values[idx] = (u16)buff;
			break;
	}
	return 0;
}

typedef struct {
	int panel_log, panel_sett, panel_gyst;
}settings_table__t;

static settings_table__t settings_cntrl_tbl[] = {PANEL_LOG_1, PANEL_U_SET_1, PANEL_U_GYST_1,  
											     PANEL_LOG_2, PANEL_U_SET_2, PANEL_U_GYST_2, 
												 PANEL_LOG_3, PANEL_U_SET_3, PANEL_U_GYST_3, 0, 0, 0};

static void settings_init() {
	int buff = 0;
	sensor.timestamp = time(NULL);
	sensor.com_port = (u8)ini__int_get(static__ini, "SENSOR", "com_port", 0);
	sensor.timeout = (u16)ini__int_get(static__ini, "SENSOR", "timeout", 0);
	for(int x = 0; x < 3; x++) {
		buff = (ini__int_get(static__ini, "SENSOR", "logs",  0) >> x) & 0x01;
		value_descr[x]->value_log  = (u8)buff;
		SetCtrlVal(panelHandle, settings_cntrl_tbl[x].panel_log, buff);
		buff = ini__int_get(static__ini, "SENSOR", value_descr[x]->descr, 0); 
		value_descr[x]->value_sett = buff >> 16;
		value_descr[x]->value_gyst = buff & 0xFFFF;
		SetCtrlVal(panelHandle, settings_cntrl_tbl[x].panel_sett, value_descr[x]->value_sett);
		SetCtrlVal(panelHandle, settings_cntrl_tbl[x].panel_gyst, value_descr[x]->value_gyst);
	}
}

static void settings_save() {
	int buff = settings.supply.value_log | (settings.out.value_log << 1) | (settings.pwr.value_log << 2);
	ini__int_put(static__ini, "SENSOR", "com_port", sensor.com_port);
	ini__int_put(static__ini, "SENSOR", "timeout", sensor.timeout);
	ini__int_put(static__ini, "SENSOR", "logs", buff);
	for(int x = 0; x < 3; x++) {
		buff = (value_descr[x]->value_sett << 16) | value_descr[x]->value_gyst;
		ini__int_put(static__ini, "SENSOR", value_descr[x]->descr, buff);
	}
	
}

int CVICALLBACK sensor_ctrl_cb (int panel, int control, int event, void *callbackData, int eventData1, int eventData2) {
	if (event == EVENT_COMMIT) {
		GetCtrlVal(panel, control, &sensor.cmd_ctrl);
		
	}
	return 0;
}
