﻿/***************************************************************************//**
 * @file game.c.
 * @brief Модуль игры в пятнашки.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdlib.h>

// todo Мб как-нибудь по-другому задавать размеры игрового поля?
#define GAME_COL 4
#define GAME_ROW 4

#define u8 char
#define RANDOM(max) (char)(rand() % (max + 1))
#define GAME_SIZE (GAME_COL * GAME_ROW)
#define GAME_SPACE GAME_SIZE

// Енум для работы со списками. Определяет как включать новый элемент.
typedef enum {
    LIST_ORDER_NEXT   = 0,
    LIST_ORDER_PREV   = 1,
}game__list_order_e;

// Игровой элемент списка.
typedef struct game__list_item_t {
    struct game__list_item_t* prev;
    struct game__list_item_t* next;
    u8 data, number;
}game__list_item_t;

struct {
    u8 input;
    game__list_item_t* cur_pos;
}game__data;

static game__list_item_t game__list[GAME_SIZE + 1];  // +1 = head

static game__list_item_t* game__init(game__list_item_t* list_head);
static void game__set_screen(game__list_item_t* list_head);
static u8 game__check(game__list_item_t* list_head, game__list_item_t* cur_pos);

static u8 ftouch(void);

static game__list_item_t* game__list_create(game__list_item_t array[], u8 num);
static void game__list_exclude(game__list_item_t* menu_item);
static void game__list_switch(game__list_item_t* list_item_1, game__list_item_t* list_item_2);
static game__list_item_t* game__list_search(game__list_item_t* list_head, u8 number);

void main()
{
    srand(8);
    game__init(game__list);

    while(1) {
        if (game__data.input = ftouch()) {      // todo Сделать буфер значений???
            game__data.cur_pos = game__list_search(game__list, game__data.input);
            if (game__data.cur_pos->data == GAME_SPACE)
                game__init(game__list);
            else if (game__check(game__list, game__data.cur_pos))
                game__set_screen(game__list);
        }
    }
}

/*******************************************************************************
 * Функция инициализации игры. Возвращает указатель на space-элемент.
 ******************************************************************************/
static game__list_item_t* game__init(game__list_item_t* list_head) {
// составляем список для инита
    game__list_item_t* list_item;
    game__list_create(list_head, GAME_SIZE);
// инициализируем матрицу исключением эелементов из списка
    for(u8 i = 1, j = RANDOM(GAME_SIZE - i); i <= GAME_SIZE; i++, j = RANDOM(GAME_SIZE - i)) {
        list_item = list_head->next;
        while(j-- && list_item)
            list_item = list_item->next;
        list_item->data = i;
        game__list_exclude(list_item);
    }
// строим проинициализированную игровую матрицу и выводим на дисплей
    game__list_create(list_head, GAME_SIZE);
    game__set_screen(list_head);
    return list_item;
}

/*******************************************************************************
 * Функция работы со списками. Включает новый элемент в список.
 ******************************************************************************/
static game__list_item_t* game__list_include(game__list_item_t* menu_item, game__list_item_t* new_item, game__list_order_e order) {
    switch(order) {
        case LIST_ORDER_NEXT:
            new_item->next = menu_item->next;
            new_item->prev = menu_item;
            menu_item->next->prev = new_item;
            menu_item->next = new_item;
            break;
        case LIST_ORDER_PREV:
            new_item->prev = menu_item->prev;
            new_item->next = menu_item;
            menu_item->prev->next = new_item;
            menu_item->prev = new_item;
            break;
    }
    return new_item;
}

/*******************************************************************************
 * Функция работы со списками. Исключает элемент из списка.
 ******************************************************************************/
static void game__list_exclude(game__list_item_t* menu_item) {
    menu_item->next->prev = menu_item->prev;
    menu_item->prev->next = menu_item->next;
    menu_item->next = menu_item->prev = NULL;
}

/*******************************************************************************
 * Функция работы со списками. Возвращает элемент списка по номеру.
 ******************************************************************************/
static game__list_item_t* game__list_search(game__list_item_t* list_head, u8 number) {
    game__list_item_t* list_item = list_head;
    while(number--)
        list_item = list_item->next;
    return list_item;
}

/*******************************************************************************
 * Функция работы со списками. Меняет местами элементы списка.
 ******************************************************************************/
static void game__list_switch(game__list_item_t* list_item_1, game__list_item_t* list_item_2) {
    game__list_item_t* list_item = list_item_1->prev;
    game__list_order_e order = LIST_ORDER_NEXT;
    if (list_item == list_item_2) {
        order = LIST_ORDER_PREV;
        list_item = list_item_1->next;
    }
    game__list_exclude(list_item_1);
    game__list_include(list_item_2, list_item_1, order);
    game__list_exclude(list_item_2);
    game__list_include(list_item, list_item_2, order);
// смена номера без использования стека
    list_item_1->number ^= list_item_2->number;
    list_item_2->number ^= list_item_1->number;
    list_item_1->number ^= list_item_2->number;
}

/*******************************************************************************
 * Функция работы со списками. Создает список из заданного количества массива элементов.
 ******************************************************************************/
static game__list_item_t* game__list_create(game__list_item_t array[], u8 num) {
    game__list_item_t* list_item = array;
    list_item->next = list_item->prev = list_item;
    for(u8 i = 1; i <= num; i++) {
        list_item = game__list_include(list_item, &array[i], LIST_ORDER_NEXT);
        list_item->number = i;
    }
    return array;
}

/*******************************************************************************
 * Функция проверки и перестановки. Возвращает 0 в случае отсутствия изменений.
 ******************************************************************************/
static u8 game__check(game__list_item_t* list_head, game__list_item_t* cur_pos) {
    game__list_item_t* list_item[4] = {cur_pos, cur_pos};
    u8 wall = cur_pos->number % 4; //для проверки границ экрана
    list_item[2] = wall ? cur_pos->next : list_head;
    list_item[3] = wall != 1 ? cur_pos->prev : list_head;
    // Поиск "верхнего" элемента
    for(u8 target = GAME_ROW; (list_item[0] != list_head) && target; target--)
        list_item[0] = list_item[0]->prev;
    // Поиск "нижнего" элемента
    for(u8 target = GAME_ROW; (list_item[1] != list_head) && target; target--)
        list_item[1] = list_item[1]->next;
    // Проверка и перестановка при совпадении
    for(u8 i = 0; i < 4; i++)
        if ((list_item[i] != list_head) && (list_item[i]->data == GAME_SPACE)) {
            game__list_switch(list_item[i], cur_pos);
            return 1;
        }
    return 0;
}


__weak static u8 ftouch(void) {return 5;}
__weak void fpicture_1(void){}
__weak void fpicture_2(void){}
__weak void fpicture_3(void){}
__weak void fpicture_4(void){}
__weak void fpicture_5(void){}
__weak void fpicture_6(void){}
__weak void fpicture_7(void){}
__weak void fpicture_8(void){}
__weak void fpicture_9(void){}
__weak void fpicture_10(void){}
__weak void fpicture_11(void){}
__weak void fpicture_12(void){}
__weak void fpicture_13(void){}
__weak void fpicture_14(void){}
__weak void fpicture_15(void){}
__weak void fpicture_space(void){}

typedef void (*display_func_t)(void);
static const display_func_t display_func[] = {NULL, fpicture_1, fpicture_2, fpicture_3, fpicture_4,
                                              fpicture_5, fpicture_6, fpicture_7, fpicture_8, fpicture_9,
                                              fpicture_10, fpicture_11, fpicture_12, fpicture_13, fpicture_14,
                                              fpicture_15, fpicture_space};

/*******************************************************************************
 * Функция вывода данных на дисплей. Выводит список элементов.
 ******************************************************************************/
static void game__set_screen(game__list_item_t* list_head) {
    game__list_item_t* list_item = list_head->next;
    for(u8 i = 0; (i < 16) && (list_item != list_head); i++) {
        display_func[list_item->data](); // todo Можно переписать под разные типы матриц...
        list_item = list_item->next;
    }
}

