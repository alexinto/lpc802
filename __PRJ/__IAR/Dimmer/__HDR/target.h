﻿#ifndef TARGET_H
#define TARGET_H

#include "mcu.h"

#define UART_BUFF      20

#define U_OUT    MCU__GPIO_PIN_9
#define V_OUT    MCU__GPIO_PIN_8
#define W_OUT    MCU__GPIO_PIN_7

#define AC_IN    MCU__GPIO_PIN_12
#define BC_IN    MCU__GPIO_PIN_11
#define AB_IN    MCU__GPIO_PIN_10

#define ADC_VOLT ADC_8

#define UART_BRD UART__BR_9600
#define UART0_RX RX__GPIO_PIN_17
#define UART0_TX TX__GPIO_PIN_13
#define UART0_SETTINGS UART_BRD | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FRAME_3_5 | UART0_TX | UART0_RX

#define DEBUG_PIN0 MCU__GPIO_PIN_17
#define DEBUG_PIN1 MCU__GPIO_PIN_13

#define I2C_0_SETTINGS SDA__GPIO_PIN_10 | SCL__GPIO_PIN_16

//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif