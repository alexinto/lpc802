﻿/***************************************************************************//**
 * @file fsm.c.
 * @brief  Алгоритм работы фазорезки
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "DRV/gpio.h"
#include "mcu.h"
#include "adc.h"
#include "fsm.h"

typedef enum {
    FSM__CH_UOUT = 0,
    FSM__CH_VOUT = 1,
    FSM__CH_WOUT = 2,
}fsm__ch_e;

// Период таймера 1 секунда
#define CTIMER__RELOAD_VALUE 1000000
// Никогда не сработает...
#define CTIMER__OFF_VALUE (CTIMER__RELOAD_VALUE + 1)
// Период сигнала при переходе через 0, мкс
#define CH__PERIOD_MAX       20000
// Минимальное время, необходимое для срабатывания прерывания, мкс
#define TMR__MIN_TIME        500
#define PERCENT_MIN 5

static u32 stop_cnt[3];

static u32 start_offset;
static u16 adc_value;
static u8 percent;
static framework__sub_t cout_sub;

static void adc__handler(events__e event);
static void fsm__cout();
static void gpio__isr_cb(u8 id, u8 gpio, gpio__state_e pin_state);

/*******************************************************************************
 * Функция запуска фазорезки.
 ******************************************************************************/
void fsm__init() {
    framework__cout_subscribe(&cout_sub, fsm__cout);
    percent = 0;
    start_offset = CH__PERIOD_MAX * percent / 100;
    // Инициализация таймера
    u8 tmr_pins[4] = {U_OUT, V_OUT, W_OUT};
    LPC_SYSCON->SYSAHBCLKCTRL[0] |= CTIMER0;    // Включаем тактирование
    LPC_CTIMER0->EMR = 0x157;                   // Три канала, при совпадении LOW
    LPC_SWM->PINASSIGN4 = *(u32*)tmr_pins;      // Назначаем пины таймеру
    for(u8 i = 0; i < 3; i++) {
        stop_cnt[i] = CTIMER__OFF_VALUE;
        LPC_CTIMER0->MR[i] = stop_cnt[i];
    }
    LPC_CTIMER0->MR[3] = CTIMER__RELOAD_VALUE;  // Период счета = 1 секунда
    LPC_CTIMER0->PR = 11;                       // делитель тактовой таймера, 1 тик = 1 мкс
    // Включаем прерывания
    LPC_CTIMER0->MCR = (1 << 0) | (1 << 3) | (1 << 6) | (1 << 10); // Сброс таймера по 3му каналу
    LPC_CTIMER0->TCR = 0x2;                    // сбрасываем таймер
    LPC_CTIMER0->TCR = 0x1;                    // разрешаем счет
    NVIC_EnableIRQ(CTIMER0_IRQn);

    // Инициализация прерываний по переходу через 0
    gpio__init(0, AC_IN, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);
    gpio__init(0, BC_IN, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);
    gpio__init(0, AB_IN, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);  // Иначе ложное прерывание...
    gpio__init(0, AC_IN, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | (1 << GPIO__INTR_POS), gpio__isr_cb);
    gpio__init(0, BC_IN, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | (2 << GPIO__INTR_POS), gpio__isr_cb);
    gpio__init(0, AB_IN, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | (3 << GPIO__INTR_POS),  gpio__isr_cb);


    // Запуск внешнего интерфейса управления (АЦП)
    adc__init(ADC_VOLT, &adc_value);
    adc__start(adc__handler);
}

static void fsm__cout() {


}

/*******************************************************************************
 * Функция - обработчик результата преобразования АЦП.
 ******************************************************************************/
static void adc__handler(events__e event) {
    u32 temp;
    if (event != EVENT__OK) {
//        adc__deinit();
        adc__init(ADC_VOLT, &adc_value);
    }
    else {
        temp = (u32)adc_value * 100 / 0xFFF;
        percent = temp < PERCENT_MIN ? 0 : temp;
        start_offset = CH__PERIOD_MAX - CH__PERIOD_MAX * percent / 100;
    }
    adc__start(adc__handler);
}


/*******************************************************************************
 * Функция - обработчик прерывания таймера. Контекст прерывания!!!
 ******************************************************************************/
void ctimer0_IRQHandler(u8 ch_mask) {
    for(int i = 0; ch_mask; i++, ch_mask >>= 1) {
        if (ch_mask & 0x01) {
            switch(i) {
                case FSM__CH_UOUT:
                case FSM__CH_VOUT:
                case FSM__CH_WOUT:
                    LPC_CTIMER0->MR[i] = stop_cnt[i];       // Устанавливаем время на прерывание по остановке
                    if (stop_cnt[i] != CTIMER__OFF_VALUE) { // Ждем прерывания по остановке...
                        stop_cnt[i] = CTIMER__OFF_VALUE;
                    }
                    else {
                        LPC_CTIMER0->EMR |= (1 << i);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

/*******************************************************************************
 * Функция - обработчик прерывания перехода через 0. Контекст прерывания!!!
 ******************************************************************************/
static void gpio__isr_cb(u8 id, u8 gpio, gpio__state_e pin_state) {
    u32 calc_start_cnt, calc_stop_cnt;
    switch(gpio) {
        case AC_IN:
            if (start_offset < TMR__MIN_TIME) {                          // 100%
                calc_start_cnt = CTIMER__OFF_VALUE;
                calc_stop_cnt = CTIMER__OFF_VALUE;
                LPC_CTIMER0->EMR &= ~(1 << 0);
            }
            else if (start_offset < (CH__PERIOD_MAX - (TMR__MIN_TIME * 2))) {  // регулируем мощность
                calc_start_cnt = LPC_CTIMER0->TC + start_offset;
                if (calc_start_cnt >= CTIMER__RELOAD_VALUE)
                    calc_start_cnt -= CTIMER__RELOAD_VALUE;
                calc_stop_cnt = calc_start_cnt + (CH__PERIOD_MAX - start_offset) - TMR__MIN_TIME;
                if (calc_stop_cnt >= CTIMER__RELOAD_VALUE)
                    calc_stop_cnt -= CTIMER__RELOAD_VALUE;
            }
            else {                                                       // 0%
                calc_start_cnt = CTIMER__OFF_VALUE;
                calc_stop_cnt = CTIMER__OFF_VALUE;
                LPC_CTIMER0->EMR |= (1 << 0);
            }
            stop_cnt[0] = calc_stop_cnt;
            LPC_CTIMER0->MR[0] = calc_start_cnt;
            LPC_CTIMER0->IR = (1 << 0);
            break;
        case BC_IN:
            if (start_offset < TMR__MIN_TIME) {                          // 111%
                calc_start_cnt = CTIMER__OFF_VALUE;
                calc_stop_cnt = CTIMER__OFF_VALUE;
                LPC_CTIMER0->EMR &= ~(1 << 1);
            }
            else if (start_offset < (CH__PERIOD_MAX - (TMR__MIN_TIME * 2))) {  // регулируем мощность
                calc_start_cnt = LPC_CTIMER0->TC + start_offset;
                if (calc_start_cnt >= CTIMER__RELOAD_VALUE)
                    calc_start_cnt -= CTIMER__RELOAD_VALUE;
                calc_stop_cnt = calc_start_cnt + (CH__PERIOD_MAX - start_offset) - TMR__MIN_TIME;
                if (calc_stop_cnt >= CTIMER__RELOAD_VALUE)
                    calc_stop_cnt -= CTIMER__RELOAD_VALUE;
            }
            else {                                                       // 1%
                calc_start_cnt = CTIMER__OFF_VALUE;
                calc_stop_cnt = CTIMER__OFF_VALUE;
                LPC_CTIMER0->EMR |= (1 << 1);
            }
            stop_cnt[1] = calc_stop_cnt;
            LPC_CTIMER0->MR[1] = calc_start_cnt;
            LPC_CTIMER0->IR = (1 << 1);
            break;
        case AB_IN:
            if (start_offset < TMR__MIN_TIME) {                          // 122%
                calc_start_cnt = CTIMER__OFF_VALUE;
                calc_stop_cnt = CTIMER__OFF_VALUE;
                LPC_CTIMER0->EMR &= ~(1 << 2);
            }
            else if (start_offset < (CH__PERIOD_MAX - (TMR__MIN_TIME * 2))) {  // регулируем мощность
                calc_start_cnt = LPC_CTIMER0->TC + start_offset;
                if (calc_start_cnt >= CTIMER__RELOAD_VALUE)
                    calc_start_cnt -= CTIMER__RELOAD_VALUE;
                calc_stop_cnt = calc_start_cnt + (CH__PERIOD_MAX - start_offset) - TMR__MIN_TIME;
                if (calc_stop_cnt >= CTIMER__RELOAD_VALUE)
                    calc_stop_cnt -= CTIMER__RELOAD_VALUE;
            }
            else {                                                       // 2%
                calc_start_cnt = CTIMER__OFF_VALUE;
                calc_stop_cnt = CTIMER__OFF_VALUE;
                LPC_CTIMER0->EMR |= (1 << 2);
            }
            stop_cnt[2] = calc_stop_cnt;
            LPC_CTIMER0->MR[2] = calc_start_cnt;
            LPC_CTIMER0->IR = (1 << 2);

            break;
        default:
            break;
    }
}










