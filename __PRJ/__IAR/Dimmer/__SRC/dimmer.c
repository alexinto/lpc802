﻿/***************************************************************************//**
 * @file dimmer.c.
 * @brief  Алгоритм работы фазорезки
 * @author a.tushentsov.
 ******************************************************************************/

#include <stddef.h>
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/supervisor.h"
#include "DRV/gpio.h"
#include "adc.h"
#include "fsm.h"

static sw_timer__t timer;

static void timer_cb(struct sw_timer__t *timer, void *ext_data);

 void main() {
    supervisor__init();
    sw_timer__init(NULL);
    fsm__init();

    gpio__init(0, DEBUG_PIN0, GPIO__DIR_OUT, NULL);
    gpio__set(0, DEBUG_PIN0, GPIO__STATE_HIGH);
    gpio__init(0, DEBUG_PIN1, GPIO__DIR_OUT, NULL);
    gpio__set(0, DEBUG_PIN1, GPIO__STATE_HIGH);

    sw_timer__start(&timer, 100, timer_cb, NULL);


    while(1) {
        framework__cout();
        wdt__reset();
   }
}



static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, timer_cb, NULL);
}


