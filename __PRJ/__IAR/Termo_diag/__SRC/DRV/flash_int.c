﻿/***************************************************************************//**
 * @file flash.с.
 * @brief Драйвер микросхемы флеш-памяти FLASH.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "System/sw_timer.h"
#include "DRV/flash.h"

/*******************************************************************************
 * Описание FLASH памяти МК.
 ******************************************************************************/
#define MCU__FLASH_INT_MAIN_MEM_BEGIN     ((uint32_t)0x08100000)
#define MCU__FLASH_INT_MAIN_MEM_END       ((uint32_t)0x0810FFFF)
#define FLASH_PDKEY1                      ((uint32_t)0x04152637U) /*!< Flash power down key1 */
#define FLASH_PDKEY2                      ((uint32_t)0xFAFBFCFDU) /*!< Flash power down key2: used with FLASH_PDKEY1 to unlock the RUN_PD bit in FLASH_ACR */
#define FLASH_PEKEY1                      ((uint32_t)0x45670123U) /*!< Flash program erase key1 */
#define FLASH_PEKEY2                      ((uint32_t)0xCDEF89ABU)  /*!< The following values must be written consecutively to unlock the FLASH_CR register allowing flash programming/erasing operations */
#define FLASH_OPTKEY1                     ((uint32_t)0x08192A3BU) /*!< Flash option key1 */
#define FLASH_OPTKEY2                     ((uint32_t)0x4C5D6E7FU) /*!< The following values must be written consecutively to unlock the FLASH_OPTR
                                                                       register allowing option byte programming/erasing operations. */
#define FLASH_PAGE_SIZE                   (0x4000)                /*!< FLASH Page Size in bytes */

static events__e flash_int__unlock(void);
static events__e flash_int__lock(void);
static int get_page(int addr);
static events__e flash_int__op_page_erase(int addr, int size);

events__e flash__init(int dev_id, flash__type_e type, uint32_t settings, flash__cb_t cb) {
    events__e res = flash_int__unlock();


    res = flash_int__lock();
    return EVENT__OK;
}

events__e flash__exec(uint8_t dev_id, flash__oper_e oper, uint32_t addr, uint8_t *buff, uint32_t len, flash__cb_t cb, void* ext_data) {
    events__e res = EVENT__OK;
    if ((addr < MCU__FLASH_INT_MAIN_MEM_BEGIN) || (addr > MCU__FLASH_INT_MAIN_MEM_END))
        return EVENT__PARAM_NA;
    switch(oper) {
        break;
    case FLASH__OP_READ:
        memcpy(buff, (uint8_t*)addr, len);  // Копирование данных.
        break;
    case FLASH__OP_WRITE:
        res = flash_int__unlock();
        SET_BIT(FLASH->CR, FLASH_CR_PG);
//        memcpy((uint8_t*)addr, buff, len);
        for(int i = 0; i < len; i++) {
//            for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 10000000); x++);
            (*(uint8_t**)&addr)[i] = buff[i];
            __DSB();
            for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 10000000); x++);
        }
        res = flash_int__lock();
        break;
    case FLASH__OP_ERASE:
        flash_int__op_page_erase(addr, len);
        break;
    default:
        res = EVENT__PARAM_NA;
    case FLASH__OP_INIT:
        break;
    }
    return res;
}

/*******************************************************************************
 * Функция осуществления операции стирания страницы.
 ******************************************************************************/
static events__e flash_int__op_page_erase(int addr, int size) {
    events__e res = EVENT__OK;
    int page_end = get_page(addr + size);
    res = flash_int__unlock();
    for(int i = get_page(addr); i < page_end; i++) {
        MODIFY_REG(FLASH->CR, FLASH_CR_SNB, ((i & FLASH_CR_SNB_Msk) << FLASH_CR_SNB_Pos));
        SET_BIT(FLASH->CR, FLASH_CR_SER);
        SET_BIT(FLASH->CR, FLASH_CR_STRT);
        for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 10000000); x++);
    }
    res = flash_int__lock();
    return res;
}


static events__e flash_int__unlock(void) {
    if ((READ_BIT(FLASH->CR, FLASH_CR_LOCK)) || (READ_BIT(FLASH->CR, FLASH_OPTCR_OPTLOCK))) {
        CRITICAL_SECTION_ON
        /* Unlocking FLASH_CR register access*/
        WRITE_REG(FLASH->KEYR, FLASH_PEKEY1);
        WRITE_REG(FLASH->KEYR, FLASH_PEKEY2);
        CRITICAL_SECTION_OFF
    }
    for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 10000000); x++);
    return EVENT__OK;
}

static events__e flash_int__lock(void) {
  FLASH->CR = FLASH_CR_LOCK | FLASH_OPTCR_OPTLOCK;
  return EVENT__OK;
}

static int get_page(int addr) {
  addr -= MCU__FLASH_INT_MAIN_MEM_BEGIN;
  int page = 12 + (addr/FLASH_PAGE_SIZE);
  return page;
}

