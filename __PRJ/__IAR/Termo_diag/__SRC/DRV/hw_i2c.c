﻿/***************************************************************************//**
 * @file hw_i2c.с
 * @brief Драйвер I2C (аппаратно зависимая часть для stm32f767).
 * @authors a.tushentsov
 ******************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include "standard.h"
#include "target.h"
#include "mcu.h"
#include "hw_i2c.h"
#include "i2c.h"
#include "supervisor.h"

#ifndef HW_I2C__ARB_LOST_DELAY
    #define HW_I2C__ARB_LOST_DELAY 5000     //Тайм-аут потери арбитража при формировании старт- последовательности 1.. 9000. 1= 1 прохождение cout().
#endif

#if (HW_I2C__ARB_LOST_DELAY == 0) || (HW_I2C__ARB_LOST_DELAY >= 10000)
    #error "HW_I2C__ARB_LOST_DELAY must be > 0 and < 10000"
#endif

#define I2C_MODE_READ	                1
#define I2C_MODE_WRITE	                0
#define I2C_ADDRESS_7BITS(addr, mode)	((addr << 1) | mode)
#define I2C_ADDRESS_10BITS(addr, mode)	(((addr << 1) | mode) & 0xFF)
#define I2C_HEADER_10BITS(addr)	        (0xF1 | (addr & 0x03))

typedef struct {
     uint8_t SB: 1;
     uint8_t ADDR: 1;
     uint8_t BTF: 1;
     uint8_t ADD10: 1;
     uint8_t STOPF: 1;
     uint8_t RESEVED: 1;
     uint8_t RxNE: 1;
     uint8_t TxE: 1;
     uint8_t BERR: 1;
     uint8_t ARLO: 1;
     uint8_t AF: 1;
     uint8_t OVR: 1;
     uint8_t PECERR: 1;
     uint8_t RESEVED1: 1;
     uint8_t TIMEOUT: 1;
     uint8_t SMBALERT: 1;
 } hw_i2c__sr1_t;

typedef struct {
     uint8_t MSL: 1;
     uint8_t BUSY: 1;
     uint8_t TRA: 1;
     uint8_t RESEVED2: 1;
     uint8_t GENCALL: 1;
     uint8_t SMBDEFAULT: 1;
     uint8_t SMBHOST: 1;
     uint8_t DUALF: 1;
     uint8_t PEC: 1;
 } hw_i2c__sr2_t;

 typedef union {
     uint16_t sr2_u16;
     hw_i2c__sr2_t sr2;
 } hw_i2c__sr2_u;

  typedef union {
     uint16_t sr1_u16;
     hw_i2c__sr1_t sr1;
 } hw_i2c__sr1_u;
// Виды операций.
typedef enum {
    HW_I2C__NO_INIT = 0,
    HW_I2C__OPER_NONE,
    HW_I2C__OPER_WAIT,
    HW_I2C__OPER_TX,
    HW_I2C__OPER_RX,
} hw_i2c__oper_e;

// Структура интерфейса I2C.
typedef struct {
    uint8_t i2c_cur_port;                               // i2c_port for rx\tx.
    uint16_t settings;                                  // Текущие настройки.
    int addr;                                           // Адрес приемника.
    uint8_t *buff;                                      // Указатель на буфер с пользовательскими данными.
    int len;                                            // Размер буфера с пользовательскими данными.
    hw_i2c__cb_t isr_cb;                                   // Указатель на пользовательский коллбэк (из прерывания).
    int bytes_count;                                    // Счетчик байтов.
    int i2c_delay;                                      // Задержка для старт последовательности (выдерживаем частоту 100кГц).
    events__e event;
    supervisor__idle_sub_t sub;
    hw_i2c__oper_e cur_oper;                            // Текущая выполняемая операция.
} hw_i2c__itf_t;



// Макрос для получения указателя на структуру аппаратного I2C по ID.
#define HW_I2C__PTR_FROM_ID_GET(i2c_id)   (&hw_i2c.itf[i2c_id - HW_I2C_1])

// Макрос для получения ID аппаратного I2C по указателю на структуру.
#define HW_I2C__ID_FROM_PTR_GET(i2c_ptr)   (i2c_ptr + HW_I2C_1)




static const I2C_TypeDef* const i2c_regs[] = {I2C1, I2C2, I2C3};
static hw_i2c__itf_t hw_i2c[I2C__COUNT] = {{.cur_oper = HW_I2C__OPER_NONE},
#if I2C__COUNT > 1
{.cur_oper = HW_I2C__OPER_NONE},
#endif
#if I2C__COUNT > 2
{.cur_oper = HW_I2C__OPER_NONE},
#endif
};

static events__e hw_i2c__itf_en(int i2c_id);
static events__e hw_i2c__itf_dis(int i2c_id);

static events__e hw_i2c__init_continue(int i2c_id);
static void hw_i2c__deinit_short(int i2c_id);
void hw_i2c__I2C_EV_IRQHandler(int i2c_id);
void hw_i2c__I2C_ER_IRQHandler(int i2c_id);

/*******************************************************************************
 * Функция инициализации аппаратного I2C на работу с указанными настройками, а так же указание функции-обработчика событий.
 *        Ex: while(hw_i2c__init(I2C_2, I2C__FREQ_100KHZ | I2C__ADDR_7BIT, 0, NULL) != 0){};
 *        Вызывает функцию инициализации до полного освобождения шины и полной инициализации I2C.
 ******************************************************************************/
events__e hw_i2c__init(int i2c_id, uint16_t settings, int own_addr, hw_i2c__cb_t event_handler) {
    hw_i2c__deinit(i2c_id);
    hw_i2c__itf_t *i2c = &hw_i2c[i2c_id];
    i2c->settings = settings;
    i2c->cur_oper = HW_I2C__OPER_NONE;
    // Инициализируем порты I2C и разрешаем глобальные прерывания
    hw_i2c__itf_en(i2c_id);
    return hw_i2c__init_continue(i2c_id);
}

/*******************************************************************************
 * Функция todo
 ******************************************************************************/
static events__e hw_i2c__init_continue(int i2c_id) {
    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[i2c_id];
    hw_i2c__itf_t *i2c = &hw_i2c[i2c_id];
    hw_i2c__deinit_short(i2c_id);
    I2C->TIMINGR = (uint32_t)(0x0 << I2C_TIMINGR_PRESC_Pos) | (0x0A << I2C_TIMINGR_SCLDEL_Pos) | (0x0A << I2C_TIMINGR_SDADEL_Pos) | (70 << I2C_TIMINGR_SCLH_Pos) | (70 << I2C_TIMINGR_SCLL_Pos);
//Включаем широковещательные команды, пробуждение из режима STOP.
    I2C->CR1 = 0;//I2C_CR1_GCEN | I2C_CR1_WUPEN; // I2C_CR1_PE
    /*I2C в stm32l1 изначально находится в slave mode,
    для перевода в master mode достаточно после
    инициализации перед передачей сгенерить start condition */
  // При занятости SCL в течении 25 мс будет давать ERROR.
    I2C->TIMEOUTR = I2C_TIMEOUTR_TIMOUTEN | 98;
    if ((I2C->ISR & I2C_ISR_BUSY) == I2C_ISR_BUSY)
        return EVENT__ERROR;
    i2c->i2c_delay = 10000;
    i2c->bytes_count = 0;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция прекращения выполняемых аппаратным I2C операций и освобождение ресурсов.
 ******************************************************************************/
events__e hw_i2c__deinit(int i2c_id) {
    hw_i2c__deinit_short(i2c_id);
    hw_i2c__itf_dis(i2c_id);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция прекращения выполняемых аппаратным I2C операций и освобождение ресурсов.
 ******************************************************************************/
static void hw_i2c__deinit_short(int i2c_id) {
    hw_i2c__itf_t *i2c = &hw_i2c[i2c_id];
    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[i2c_id];
    I2C->CR1 = 0; I2C->CR1;
    i2c->i2c_delay = 10000;
}

/*******************************************************************************
 * Функция чтения данных по аппаратному I2C.
 ******************************************************************************/
events__e hw_i2c__rx(int i2c_id, int addr, uint8_t *buff, int len, hw_i2c__cb_t isr_cb) {
    if (buff == NULL || len == 0)
        return EVENT__PARAM_NA;
    hw_i2c__itf_t *i2c = &hw_i2c[i2c_id];

    if (i2c->cur_oper != HW_I2C__OPER_NONE)
        return EVENT__BUSY;
    i2c->cur_oper = HW_I2C__OPER_RX;
    hw_i2c__init_continue(i2c_id);

    i2c->buff = buff;
    i2c->len = len;
    i2c->isr_cb = isr_cb;
    i2c->addr = addr;
    i2c->i2c_delay = 0x00;

    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[i2c_id];
    //Включаем I2C, Разрешаем прерывания
    I2C->CR1 |= (len <= 1) ? (I2C_CR1_PE | I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_ERRIE) : (I2C_CR1_PE | I2C_CR1_RXIE | I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_ERRIE);

    if (len < 255)
        I2C->CR2 = I2C_CR2_RD_WRN | (addr << 1) | (len << I2C_CR2_NBYTES_Pos) | I2C_CR2_AUTOEND;
    else
        I2C->CR2 = I2C_CR2_RD_WRN | (addr << 1) | (0xFF << I2C_CR2_NBYTES_Pos);

    I2C->CR2 |= I2C_CR2_START; // Формирование сигнала START

    return EVENT__OK;
}

/*******************************************************************************
 * Функция записи данных по аппаратному I2C.
 ******************************************************************************/
events__e hw_i2c__tx(int i2c_id, int addr, uint8_t *buff, int len, hw_i2c__cb_t isr_cb) {
    if (buff == NULL || len == 0)
        return EVENT__PARAM_NA;
    hw_i2c__itf_t *i2c = &hw_i2c[i2c_id];

    if (i2c->cur_oper != HW_I2C__OPER_NONE)
        return EVENT__BUSY;
    i2c->cur_oper = HW_I2C__OPER_TX;
    hw_i2c__init_continue(i2c_id);

    i2c->buff = buff;
    i2c->len = len;
    i2c->isr_cb = isr_cb;
    i2c->addr = addr;
    i2c->i2c_delay = 0x00;

    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[i2c_id];
    //Разрешаем прерывания
    I2C->CR1 |= I2C_CR1_PE | I2C_CR1_TXIE | I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_ERRIE;

    I2C->CR2 = (addr << 1) | (0xFF << I2C_CR2_NBYTES_Pos);
    I2C->CR2 |= I2C_CR2_START; // формирование сигнала START

    return EVENT__OK;
}

/*******************************************************************************
 * Функция внешнего вызова для обработки текущего состояния драйвера I2C.
 ******************************************************************************/
void hw_i2c__cout(void) {
    for(int i2c_id = 0; i2c_id < I2C__COUNT; i2c_id++) {
        hw_i2c__itf_t *i2c = &hw_i2c[i2c_id];
        if (i2c->cur_oper != HW_I2C__OPER_NONE){
            if ((i2c->i2c_delay == HW_I2C__ARB_LOST_DELAY) || (i2c->cur_oper == HW_I2C__OPER_WAIT)) {
                i2c->cur_oper = HW_I2C__OPER_NONE;
                i2c->isr_cb(i2c_id, i2c->event, i2c->addr, i2c->buff, i2c->bytes_count);
            }
            CRITICAL_SECTION_ON
            (i2c->i2c_delay >= HW_I2C__ARB_LOST_DELAY) ? (i2c->i2c_delay = 10000):(i2c->i2c_delay++);
            CRITICAL_SECTION_OFF
        }
    }
}

/*******************************************************************************
 * * Функция-обработчик прерывания по приему и передаче I2C.
 ******************************************************************************/
void hw_i2c__I2C_EV_IRQHandler(int i2c_id) {
    hw_i2c__itf_t *i2c = &hw_i2c[i2c_id];
    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[i2c_id];
    events__e event = EVENT__OK;
    uint32_t flags = I2C->ISR;
    if (I2C->ISR & (I2C_ISR_STOPF)) {
        flags = I2C->ISR & (~I2C_ISR_STOPF);
        if (flags & (~0xF)) {
            if (I2C->ISR & I2C_ISR_NACKF)
                event = EVENT__NOT_EXIST;
            else
                event = EVENT__ERROR;
        }
        if (i2c->cur_oper == HW_I2C__OPER_RX)
            i2c->buff[i2c->bytes_count] = I2C->RXDR;
        I2C->CR1 = 0;
        i2c->cur_oper = HW_I2C__OPER_WAIT;
    }
    else if  (I2C->ISR & (I2C_ISR_NACKF)) {
        I2C->CR1 &= ~(I2C_CR1_RXIE | I2C_CR1_TXIE | I2C_CR1_NACKIE);
        I2C->CR2 |= I2C_CR2_STOP;
    }
    else if ((I2C->ISR & I2C_ISR_TXE) && (I2C->CR1 & I2C_CR1_TXIE)){
        i2c->bytes_count++;
        if (i2c->bytes_count <= i2c->len)
            I2C->TXDR = i2c->buff[i2c->bytes_count - 1];
        //Если закончили передавть байтики
        else {
                i2c->bytes_count--;
                I2C->CR1 &= ~(I2C_CR1_RXIE | I2C_CR1_TXIE | I2C_CR1_NACKIE);
                I2C->CR2 |= I2C_CR2_STOP;
        }
    }
    else if ((I2C->ISR & I2C_ISR_RXNE) && (I2C->CR1 & I2C_CR1_RXIE)) {
        //Формируем STOP на предпоследнем байте.
        if (i2c->len <= (i2c->bytes_count + 2)) {
            I2C->CR1 &= ~(I2C_CR1_RXIE | I2C_CR1_TXIE | I2C_CR1_NACKIE);
            I2C->CR2 |= I2C_CR2_STOP;
        }
        i2c->buff[i2c->bytes_count] = I2C->RXDR;
        i2c->bytes_count++;
    }
    else {
        I2C->CR1 = 0;
        i2c->cur_oper = HW_I2C__OPER_WAIT;
    }
    i2c->event = event;
}

/*******************************************************************************
 * Функция-обработчик прерывания по ошибкам.
 ******************************************************************************/
void hw_i2c__I2C_ER_IRQHandler(int i2c_id) {
    hw_i2c__itf_t *i2c = &hw_i2c[i2c_id];
    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[i2c_id];
    i2c->i2c_delay = 10000;
    events__e event = EVENT__ERROR;
    if (I2C->ISR & I2C_ISR_TIMEOUT)
       event = EVENT__BUSY;
    else if (I2C->ISR & I2C_ISR_ARLO)
        event = EVENT__ERROR;
    I2C->CR1 = 0;
    i2c->event = event;
    i2c->cur_oper = HW_I2C__OPER_WAIT;
}

/*******************************************************************************
 *  Функция инициализации интерфейса I2C.
 ******************************************************************************/
typedef struct {
    int gpio[2];
    int i2c_clk_msk;
    int i2c_clk_source;
    int i2c_clock_en;
    int i2c_irq[2];
}hw_i2c_desc__t;

static const hw_i2c_desc__t hw_i2c_desc[] = {
    //     SCL                  SDA
    { { MCU__GPIO_P_B_10, MCU__GPIO_P_B_11}, RCC_DCKCFGR2_I2C1SEL_Msk, RCC_DCKCFGR2_I2C1SEL_1, RCC_APB1ENR_I2C1EN, { I2C1_EV_IRQn, I2C1_ER_IRQn } },
    { { MCU__GPIO_P_B_10, MCU__GPIO_P_B_11}, RCC_DCKCFGR2_I2C2SEL_Msk, RCC_DCKCFGR2_I2C2SEL_1, RCC_APB1ENR_I2C2EN, { I2C2_EV_IRQn, I2C2_ER_IRQn } },
    { { MCU__GPIO_P_H_7,  MCU__GPIO_P_H_8},  RCC_DCKCFGR2_I2C3SEL_Msk, RCC_DCKCFGR2_I2C3SEL_1, RCC_APB1ENR_I2C3EN, { I2C3_EV_IRQn, I2C3_ER_IRQn } },
};

static events__e hw_i2c__itf_en(int i2c_id) {
    MODIFY_REG(RCC->DCKCFGR2, hw_i2c_desc[i2c_id].i2c_clk_msk, hw_i2c_desc[i2c_id].i2c_clk_source);   // I2C_CLK = 16MHz
    RCC->APB1ENR |= hw_i2c_desc[i2c_id].i2c_clock_en;
    IRQn_Type irq_num;
    for(int x = 0; x < 2; x++) {
        gpio__af_init(hw_i2c_desc[i2c_id].gpio[x], 1, 0x4);
        irq_num = (IRQn_Type)hw_i2c_desc[i2c_id].i2c_irq[x];
        NVIC_SetPriority(irq_num, 3);
        NVIC_EnableIRQ(irq_num);
    }
    return EVENT__OK;
}

/*******************************************************************************
 *  Функция де-инициализации интерфейса I2C.
 ******************************************************************************/
static events__e hw_i2c__itf_dis(int i2c_id) {
    RCC->APB1ENR &= ~hw_i2c_desc[i2c_id].i2c_clock_en;
    for(int x = 0; x < 2; x++) {
        NVIC_DisableIRQ((IRQn_Type)hw_i2c_desc[i2c_id].i2c_irq[x]);
        gpio__init(hw_i2c_desc[i2c_id].gpio[x], GPIO__DIR_IN | GPIO__PULL_UP, NULL);
    }
    return EVENT__OK;
}


