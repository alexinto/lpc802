﻿/***************************************************************************//**
 * @file termo_diag.c.
 * @brief  Проект диагностического термометра.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "device.h"
#include "System/sw_timer.h"
#include "System/framework.h"
#include "supervisor.h"
#include "gpio.h"


static void test_gpio_cb(int gpio, gpio__state_e pin_state);


void main() {
#ifdef _DEBUG_
    DBGMCU->CR = DBGMCU_CR_DBG_STOP;
    DBGMCU->APB1FZ = 0xFFFFFFFF;
    DBGMCU->APB2FZ = 0xFFFFFFFF;
#endif
    supervisor__init();
    sw_timer__init(NULL);
    device__init();

    gpio__init(USER_BT, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | GPIO__INT_EDGE_FALING, test_gpio_cb);

    while(1) {
        framework__cout();
    }
}



static void test_gpio_cb(int gpio, gpio__state_e pin_state) {

}

