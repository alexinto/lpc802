﻿/***************************************************************************//**
 * @file device__data.c.
 * @brief  Алгоритм работы анализатора.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/sw_timer.h"
#include "System/framework.h"
#include "gpio.h"
#include "DRV/dev_setts.h"
#include "device.h"
#include "termo.h"

#ifndef DEVICE__LED_1WIRE
#define DEVICE__LED_1WIRE    200    // Поиск датчиков 1wire
#endif
#ifndef DEVICE__LED_COMPLETE
#define DEVICE__LED_COMPLETE 1000   // Переход в штатный режим работы
#endif

typedef enum {
    DEVICE__STATE_IDLE = 0,
    DEVICE__STATE_INIT,
    DEVICE__STATE_WORK,
}device__state_e;

static struct {
    device__state_e fsm;
    framework__sub_t sub;
    sw_timer__t led_tmr;
    uart__sub_t usb;
    u8 usb_buff[1000];
    u8 termo__init;
}device__data;

static void device__cout();
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void device__termo_cb(events__e event, float temp, void* ext_data);
static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data);
static void uart_rx_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void uart_tx_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);

events__e device__init() {
    framework__cout_subscribe(&device__data.sub, device__cout);
    gpio__init(LED_BLUE, GPIO__DIR_OUT, NULL);
    sw_timer__start(&device__data.led_tmr, DEVICE__LED_1WIRE, timer_cb, NULL);
    dev_setts__init(DEV_SETTS__ID, 0, DEV_SETTS__ADDR, DEV_SETTS__SIZE, DEV_SETTS_PARAM_MAX, flash__exec);  // 0 - в проекте только одна флеш- флеш МК
    uart__init();
    device__data.fsm = DEVICE__STATE_IDLE;
    return EVENT__OK;
}

static void device__cout() {
    switch(device__data.fsm) {
        case DEVICE__STATE_IDLE:
            device__data.fsm = DEVICE__STATE_INIT;
            device__data.termo__init = 0;
            termo__init(device__termo_cb, NULL);  // Запускаем процесс поиска устройств 1wire
            break;
        case DEVICE__STATE_INIT: // Ищем устройства 1wire
            if (!device__data.termo__init)
                break;
            sw_timer__start(&device__data.led_tmr, DEVICE__LED_COMPLETE, timer_cb, NULL);
            device__data.fsm = DEVICE__STATE_WORK;
            break;
        case DEVICE__STATE_WORK:
            break;
        default:
            break;
    }
}

static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, timer_cb, NULL);
    gpio__set(LED_BLUE, GPIO__STATE_TOGGLE);
}

static void device__termo_cb(events__e event, float temp, void* ext_data) {
    device__data.termo__init = 1;
    uart__open(&device__data.usb, USB_UART_1, UART_SETTINGS, uart_event_hd, NULL);
}

static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
    switch(event) {
    case EVENT__OPEN:
        uart__rx(&device__data.usb, UART__FRAME_3_5, device__data.usb_buff, sizeof(device__data.usb_buff), 0, uart_rx_cb, (void*)1);
        break;
    case EVENT__CLOSE:
        uart__open(sub, sub->uart_id, UART_SETTINGS, uart_event_hd, NULL);
        break;
    }
}

static void uart_rx_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    uart__tx(sub, buff, len, uart_tx_cb, NULL);
}

static void uart_tx_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    uart__rx(&device__data.usb, UART__FRAME_3_5, device__data.usb_buff, sizeof(device__data.usb_buff), 0, uart_rx_cb, (void*)1);
}