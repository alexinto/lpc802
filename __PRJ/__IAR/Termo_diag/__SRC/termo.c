﻿/***************************************************************************//**
 * @file termo.c.
 * @brief  Обработка показаний термодатчиков.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include <math.h>
#include "System/sw_timer.h"
#include "System/framework.h"
#include "DRV/dev_setts.h"
#include "IC/ds18b20.h"
#include "onewire.h"
#include "termo.h"

#ifndef TERMO__TIMEOUT
#define TERMO__TIMEOUT 5000              // Интервал опроса всех термодатчиков, мс.
#endif

#define TERMO__K     0.5                 // Коэффициент фильтрации Каллмана для TERMO__AVG

typedef enum {
    TERMO__STATE_IDLE = 0,
    TERMO__STATE_RESET_TERMO,
    TERMO__STATE_CMD_FIND_TERMO,
    TERMO__STATE_FIND_TERMO,
    TERMO__STATE_MEASURE,
}termo__state_e;

static struct {
    termo__cb_t cb;
    void* ext_data;
    termo__state_e state;
    sw_timer__t tmr;
    termo__e mode;
    float temp_avg;
    u64 uuid;
    u8 pos[DS18B20__CNT];
    u8 termo_cnt;
}termo__data;

static void device__1wire_cb(uint8_t id, events__e event, u8* buff, int len, void* ext_data);
static void ds18b20__get_temp_cb(uint8_t id, events__e event, float temp, void* ext_data);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);

events__e termo__init(termo__cb_t cb, void* ext_data) {
    termo__data.state = TERMO__STATE_CMD_FIND_TERMO;
    sw_timer__stop(&termo__data.tmr);
    termo__data.cb = cb;
    termo__data.ext_data = ext_data;
    onewire__init(ONEWIRE_1, HW_UART_1);
    for(int i = 0; i < DS18B20__CNT; i++) {
        ds18b20__init(i, ONEWIRE_1);
        ds18b20__set_mode(i, 0);
        termo__data.pos[i] = 0;
    }
    termo__data.termo_cnt = 0;  // Обнуляем позицию для поиска устройств 1wire
    return onewire__reset(ONEWIRE_1, device__1wire_cb, (void*)0);  // 0- первый термодатчик
}

void termo__set_mode(termo__e calc) {
    if (calc <= TERMO__AVG)
        termo__data.mode = calc;
}

float termo__get() {
    uint8_t valid = 0;
    float temp, temp_res;
    for(int i = 0; i < DS18B20__CNT; i++) {
        if (!ds18b20__is_valid(i))
            continue;
        temp = ds18b20__get_temp(i);
        if (!valid)
            temp_res = termo__data.temp_avg = temp;
        switch(termo__data.mode) {
            case TERMO__MIN:
                if (temp < temp_res)
                    temp_res = temp;
                break;
            case TERMO__MAX:
                if (temp > temp_res)
                    temp_res = temp;
                break;
            case TERMO__AVG:
                termo__data.temp_avg = TERMO__K * temp + (1 - TERMO__K) * termo__data.temp_avg;
                break;
        }
        valid = 1;
    }
    if (!valid)
        return termo__data.mode == TERMO__MIN ? -200 : 200;
    return termo__data.mode == TERMO__AVG ? termo__data.temp_avg : temp_res;
}


static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    if (termo__data.state != TERMO__STATE_IDLE) {
        termo__data.state = TERMO__STATE_IDLE;
        if (termo__data.cb)
            termo__data.cb(EVENT__OK, 0, termo__data.ext_data);
        sw_timer__start(timer, TERMO__TIMEOUT, timer_cb, NULL);
    }
    else if (ds18b20__meas_temp(0, ds18b20__get_temp_cb, (void*)0) != EVENT__OK)
        sw_timer__start(timer, TERMO__TIMEOUT, timer_cb, NULL);
}


static void ds18b20__get_temp_cb(uint8_t id, events__e event, float temp, void* ext_data) {
    int temperature = (int)round(temp);
    int dev_id = (int)ext_data;
    if (event == EVENT__OK) {
        if ((++dev_id < DS18B20__CNT) && (ds18b20__get_uuid(dev_id)))
            event = ds18b20__meas_temp(dev_id, ds18b20__get_temp_cb, (void*)dev_id);
        else {
            event = EVENT__CONTINUE;
        }
    }
    if (event != EVENT__OK)
        sw_timer__start(&termo__data.tmr, TERMO__TIMEOUT, timer_cb, NULL);
}


static void device__1wire_cb(uint8_t id, events__e event, u8* buff, int len, void* ext_data) {
    int dev_id = (int)ext_data;
    if (event == EVENT__FIND) {
        if (++termo__data.termo_cnt < DS18B20__CNT)
            termo__data.pos[termo__data.termo_cnt] = len;
        event = onewire__search(ONEWIRE_1, 0, &termo__data.uuid, len, device__1wire_cb, (void*)dev_id);
    }
    else if (event == EVENT__OK) {
        switch(termo__data.state) {
            case TERMO__STATE_CMD_FIND_TERMO:
                termo__data.state = TERMO__STATE_FIND_TERMO;
                termo__data.uuid = 1 << termo__data.pos[dev_id];
                event = onewire__search(ONEWIRE_1, ONEWIRE__CMD_SEARCH, &termo__data.uuid, 0, device__1wire_cb, (void*)dev_id);
                break;
            case TERMO__STATE_FIND_TERMO:
                if (len >= 63)    // Валидный uuid
                    ds18b20__set_uuid(dev_id, termo__data.uuid);
                termo__data.state = TERMO__STATE_RESET_TERMO;
                break;
            default:
                break;
        }
    }
    if ((event != EVENT__OK) || (termo__data.state == TERMO__STATE_RESET_TERMO)) {
        dev_id++;
        if (dev_id >= DS18B20__CNT) {
            termo__data.state = TERMO__STATE_MEASURE;
            event = ds18b20__meas_temp(0, ds18b20__get_temp_cb, (void*)0);
            if (event != EVENT__OK)
                sw_timer__start(&termo__data.tmr, 0, timer_cb, NULL);
            return;
        }
        termo__data.state = TERMO__STATE_CMD_FIND_TERMO;
        onewire__reset(ONEWIRE_1, device__1wire_cb, (void*)dev_id);
    }
}
