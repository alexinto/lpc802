﻿/***************************************************************************//**
 * @file termo.h.
 * @brief Модуль работы с устройствами.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TERMO_H_
#define TERMO_H_
#include "System/events.h"

typedef enum {
    TERMO__MIN = 0,    // Минимальное значение
    TERMO__MAX = 1,    // Максимальное значение
    TERMO__AVG = 2,    // Усредненное значение
}termo__e;


typedef void (*termo__cb_t)(events__e event, float temp, void* ext_data);

events__e termo__init(termo__cb_t cb, void* ext_data);


void termo__set_mode(termo__e calc);

float termo__get();





#endif /* TERMO_H_ */