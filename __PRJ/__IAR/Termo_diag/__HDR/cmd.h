#ifndef AWG2M_H_
#define AWG2M_H_

#include "Types/DRV/awg2m_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_AWG2M_CH_PART_GET AWG2M_CH_PART_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Чтение кол-ва записанных частей образа канала. 0- буфер канала пуст, читать нечего.
 */
/**@{*/
#define CMD__AWG2M_CH_PART_GET               0x1200
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;        /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_part_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;        /*!< Идентификатор выходного канала (0..3). */
    uint32_t part;      /*!< Кол-во записанных частей. 0- буфер канала пуст, читать нечего. */
}cmd_awg2m_ch_part_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_IMG_GET AWG2M_CH_IMG_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Чтение части цифрового образа канала. Кол-во частей возвращает команда \ref CMD__AWG2M_CH_PART_GET
 */
/**@{*/
#define CMD__AWG2M_CH_IMG_GET               0x1201
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;        /*!< Идентификатор выходного канала (0..3). */
    uint32_t part;         /*!< Порядковый номер части (от 0). */
}cmd_awg2m_ch_img_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;                          /*!< Идентификатор выходного канала (0..3). */
    uint32_t part;                           /*!< Порядковый номер части (от 0). */
    uint16_t len;                            /*!< Размер полезных данных в data. */
    uint8_t data[RXIETHERNET_CMD_SIZE - 10]; /*!< Данные образа*/
}cmd_awg2m_ch_img_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_IMG_SET AWG2M_CH_IMG_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Запись части цифрового образа канала.
 */
/**@{*/
#define CMD__AWG2M_CH_IMG_SET               0x1202
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;                          /*!< Идентификатор выходного канала (0..3). */
    uint32_t part;                           /*!< Порядковый номер части (от 0). */
    uint16_t len;                            /*!< Размер полезных данных в data. */
    uint8_t data[RXIETHERNET_CMD_SIZE - 10]; /*!< Данные образа*/
}cmd_awg2m_ch_img_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_img_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_SYNCCLK_MUX_DIV_SET AWG2M_SYNCCLK_MUX_DIV_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Установка множителя частоты 2й ступени и делителя группы каналов мезонина SYNCCLOCK. Костыль! В библиотеку не выносить!
 */
/**@{*/
#define CMD__AWG2M_SYNCCLK_MUX_DIV_SET         0x1203
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t id;                /*!< Идентификатор канала \ref cmd_oscillator__ch_id_e. */
    uint16_t mux;               /*!< Значение множителя частоты. */
    uint16_t div;               /*!< Значение делителя частоты. */
}cmd_awg2m_syncclk_mux_div_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_syncclk_mux_div_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_SYNCCLK_MUX_DIV_GET AWG2M_SYNCCLK_MUX_DIV_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Возвращает множитель частоты 2й ступени и делитель канала мезонина SYNCCLOCK. Костыль! В библиотеку не выносить!
 */
/**@{*/
#define CMD__AWG2M_SYNCCLK_MUX_DIV_GET         0x1204
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t id;                /*!< Идентификатор канала \ref cmd_oscillator__ch_id_e. */
}cmd_awg2m_syncclk_mux_div_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t id;                /*!< Идентификатор канала \ref cmd_oscillator__ch_id_e. */
    uint16_t mux;               /*!< Значение множителя частоты. */
    uint16_t div;               /*!< Значение делителя частоты. */
}cmd_awg2m_syncclk_mux_div_get__out_t;
/**@}*/


/**
 * \defgroup GRP_AWG2M_FPGA_MUX_DIV_SET AWG2M_FPGA_MUX_DIV_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Установка множителя частоты и делителя каналов ПЛИС.
 */
/**@{*/
#define CMD__AWG2M_FPGA_MUX_DIV_SET         0x1205
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t id;                /*!< Идентификатор канала синхронизации (0..2). */
    uint16_t mux;               /*!< Значение множителя частоты. */
    uint16_t div;               /*!< Значение делителя частоты. */
}cmd_awg2m_fpga_mux_div_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_fpga_mux_div_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_FPGA_MUX_DIV_GET AWG2M_FPGA_MUX_DIV_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Возвращает множитель частоты и делителя каналов ПЛИС.
 */
/**@{*/
#define CMD__AWG2M_FPGA_MUX_DIV_GET         0x1206
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t id;                /*!< Идентификатор канала синхронизации (0..2). */
}cmd_awg2m_fpga_mux_div_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t id;                /*!< Идентификатор канала синхронизации (0..2). */
    uint16_t mux;               /*!< Значение множителя частоты. */
    uint16_t div;               /*!< Значение делителя частоты. */
}cmd_awg2m_fpga_mux_div_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_DAC_CLK_CHANGE_SET AWG2M_DAC_CLK_CHANGE_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда назначения тактовой частоты ЦАП канала.
 */
/**@{*/
#define CMD__AWG2M_DAC_CLK_CHANGE_SET         0x1207
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t source;            /*!< Источник (канал синхронизации) 0..2. */
}cmd_awg2m_dac_clk_change_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_dac_clk_change_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_DAC_CLK_CHANGE_GET AWG2M_DAC_CLK_CHANGE_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда возвращает настройку тактовых частот ЦАП канала.
 */
/**@{*/
#define CMD__AWG2M_DAC_CLK_CHANGE_GET         0x1208
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;                          /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_dac_clk_change_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t ch_id;             /*!< Идентификатор канала. */
    uint16_t source;            /*!< Источник (канал синхронизации) 0..2. */
}cmd_awg2m_dac_clk_change_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_CTRL AWG2M_CH_CTRL
 * \ingroup CMD_Commands AWGModule
 * \brief Команда подключения\отключения выхода канала.
 */
/**@{*/
#define CMD__AWG2M_CH_CTRL         0x1209
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t out_on;            /*!< 0- выход отключен, 1- выход подключен. */
}cmd_awg2m_ch_ctrl__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_ctrl__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_DIFF_MODE_SET AWG2M_CH_DIFF_MODE_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда задания дифф./не дифф. сигнала канала.
 */
/**@{*/
#define CMD__AWG2M_CH_DIFF_MODE_SET         0x120A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t diff_on;           /*!< 0- diff режим выключен, 1- diff режим включен. */
}cmd_awg2m_ch_diff_mode_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_diff_mode_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_DIFF_MODE_GET AWG2M_CH_DIFF_MODE_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения дифф./не дифф. режима сигнала канала.
 */
/**@{*/
#define CMD__AWG2M_CH_DIFF_MODE_GET         0x120B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;              /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_diff_mode_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t diff_on;           /*!< 0- diff режим выключен, 1- diff режим включен. */
}cmd_awg2m_ch_diff_mode_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_RES_SET AWG2M_CH_RES_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда подключения\отключения резистора согласования канала
 */
/**@{*/
#define CMD__AWG2M_CH_RES_SET         0x120C
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t res_on;            /*!< 0- резистор отключен, 1- резистор подключен. */
}cmd_awg2m_ch_res_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_res_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_RES_GET AWG2M_CH_RES_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда возвращает состояние подключения резистора согласования канала.
 */
/**@{*/
#define CMD__AWG2M_CH_RES_GET         0x120D
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_res_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t res_on;            /*!< 0- резистор отключен, 1- резистор подключен. */
}cmd_awg2m_ch_res_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_V_OFFSET_SET AWG2M_CH_V_OFFSET_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда задания напряжения смещения канала
 */
/**@{*/
#define CMD__AWG2M_CH_V_OFFSET_SET         0x120E
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint8_t ext_offset;         /*!< Сумматор внешнего напряжения смещения. 0- выключен, 1- включен . */
    double v_offset;            /*!< Напряжение смещения, В. */
}cmd_awg2m_ch_v_offset_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_v_offset_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_V_OFFSET_GET AWG2M_CH_V_OFFSET_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения напряжения смещения канала
 */
/**@{*/
#define CMD__AWG2M_CH_V_OFFSET_GET         0x120F
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_v_offset_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint8_t ext_offset;         /*!< Сумматор внешнего напряжения смещения. 0- выключен, 1- включен . */
    double v_offset;            /*!< Напряжение смещения, В. */
}cmd_awg2m_ch_v_offset_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_RANGE_SET AWG2M_CH_RANGE_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда задания поддиапазона формирования сигнала канала
 */
/**@{*/
#define CMD__AWG2M_CH_RANGE_SET         0x1210
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t range;             /*!< Поддиапазон \ref awg2m__dac_rng_e */
}cmd_awg2m_ch_range_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_range_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_RANGE_GET AWG2M_CH_RANGE_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения поддиапазона формирования сигнала канала
 */
/**@{*/
#define CMD__AWG2M_CH_RANGE_GET         0x1211
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_range_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t range;             /*!< Поддиапазон \ref awg2m__dac_rng_e */
}cmd_awg2m_ch_range_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_DAC_CHG_SET AWG2M_CH_DAC_CHG_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда выбора целевого ЦАП  канала
 */
/**@{*/
#define CMD__AWG2M_CH_DAC_CHG_SET         0x1212
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t dac;               /*!< \ref awg2m__dac_e */
}cmd_awg2m_ch_dac_chg_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_dac_chg_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_DAC_CHG_GET AWG2M_CH_DAC_CHG_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения целевого ЦАП канала
 */
/**@{*/
#define CMD__AWG2M_CH_DAC_CHG_GET         0x1213
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_dac_chg_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t dac;               /*!< \ref awg2m__dac_e */
}cmd_awg2m_ch_dac_chg_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_LPF_SET AWG2M_CH_LPF_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда выбора фильтра канала
 */
/**@{*/
#define CMD__AWG2M_CH_LPF_SET         0x1214
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t lpf;               /*!< LPF 0..2, 0- без фильтра. */
}cmd_awg2m_ch_lpf_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_lpf_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_LPF_GET AWG2M_CH_LPF_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения фильтра канала
 */
/**@{*/
#define CMD__AWG2M_CH_LPF_GET         0x1215
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_lpf_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t lpf;               /*!< LPF 0..2, 0- без фильтра. */
}cmd_awg2m_ch_lpf_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_DAC_WR AWG2M_DAC_WR
 * \ingroup CMD_Commands AWGModule
 * \brief Команда записи данных в ЦАП канала
 */
/**@{*/
#define CMD__AWG2M_DAC_WR         0x1216
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    double v_offset;            /*!< Выходное напряжение, В. */
}cmd_awg2m_dac_wr__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_dac_wr__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_DAC_RD AWG2M_DAC_RD
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения данных из ЦАП-а канала
 */
/**@{*/
#define CMD__AWG2M_DAC_RD         0x1217
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_dac_rd__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    double v_offset;            /*!< Выходное напряжение, В. */
}cmd_awg2m_dac_rd__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_IMG_CTRL_SET AWG2M_IMG_CTRL_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда воспроизведения цифрового образа
 */
/**@{*/
#define CMD__AWG2M_IMG_CTRL_SET         0x1218
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t start;             /*!< 0- стоп, 1 - start. */
    uint32_t addr[2];           /*!< [0] -Начальный адрес, [1] - конечный адрес. */
    uint32_t count;             /*!< Число повторов. */
}cmd_awg2m_img_ctrl_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_img_ctrl_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_IMG_CTRL_GET AWG2M_IMG_CTRL_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения состояния выполнения воспроизведения цифрового образа
 */
/**@{*/
#define CMD__AWG2M_IMG_CTRL_GET         0x1219
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_img_ctrl_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t start;             /*!< 0- стоп, 1 - start. */
    uint32_t addr[2];           /*!< [0] -Начальный адрес, [1] - конечный адрес. */
    uint32_t count;             /*!< Число повторов. */
}cmd_awg2m_img_ctrl_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_DDS_CTRL_SET AWG2M_DDS_CTRL_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда прямого цифрового синтеза
 */
/**@{*/
#define CMD__AWG2M_DDS_CTRL_SET         0x121A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t start;             /*!< 0- стоп, 1 - start. */
    uint32_t freq;              /*!< Частота, Гц. */
    double amp;                 /*!< Амплитуда, В. */
    double phase;               /*!< Фаза, градус. */
}cmd_awg2m_dds_ctrl_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_dds_ctrl_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_DDS_CTRL_GET AWG2M_DDS_CTRL_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения настроек прямого цифрового синтеза
 */
/**@{*/
#define CMD__AWG2M_DDS_CTRL_GET         0x121B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_dds_ctrl_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t start;             /*!< 0- стоп, 1 - start. */
    uint32_t freq;              /*!< Частота, Гц. */
    double amp;                 /*!< Амплитуда, В. */
    double phase;               /*!< Фаза, градус. */
}cmd_awg2m_dds_ctrl_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CFG_SET AWG2M_CFG_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда формирования конфигурации
 */
/**@{*/
#define CMD__AWG2M_CFG_SET         0x121C
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t cfg_id;            /*!< Идентификатор конфигурации. */
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t work_mode;         /*!< Режим работы \ref awg2m__work_mode_e. */
    struct {
        uint32_t source   : 2;         /*!< источник тактирования. */
        uint32_t on       : 1;         /*!< вкл./выкл.выход. */
        uint32_t diff     : 1;         /*!< дифф./не дифф.выход. */
        uint32_t resistor : 1;         /*!< резистор вкл./выкл. */
        uint32_t lfp      : 2;         /*!< фильтр(0..2). */
        uint32_t dac      : 2;         /*!< ЦАП(0..1). */
        uint32_t range    : 8;         /*!< поддиапазон. */
    }ch;
    double ch_amp;                     /*!< [вых. напряжение/амплитуда], В. */
    double ch_offset;                  /*!< смещение, В. */
    awg2m__trig_cfg_t start;           /*!< событие старта . */
    awg2m__trig_cfg_t stop;            /*!< событие стопа . */
}cmd_awg2m_cfg_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_cfg_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CFG_GET AWG2M_CFG_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда получения конфигурации
 */
/**@{*/
#define CMD__AWG2M_CFG_GET         0x121D
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t cfg_id;            /*!< Идентификатор конфигурации. */
}cmd_awg2m_cfg_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t cfg_id;            /*!< Идентификатор конфигурации. */
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t work_mode;         /*!< Режим работы \ref awg2m__work_mode_e. */
    struct {
        uint32_t source   : 2;         /*!< источник тактирования. */
        uint32_t on       : 1;         /*!< вкл./выкл.выход. */
        uint32_t diff     : 1;         /*!< дифф./не дифф.выход. */
        uint32_t resistor : 1;         /*!< резистор вкл./выкл. */
        uint32_t lfp      : 2;         /*!< фильтр(0..2). */
        uint32_t dac      : 2;         /*!< ЦАП(0..1). */
        uint32_t range    : 8;         /*!< поддиапазон. */
    }ch;
    double ch_amp;                     /*!< [вых. напряжение/амплитуда], В. */
    double ch_offset;                  /*!< смещение, В. */
    awg2m__trig_cfg_t start;           /*!< событие старта . */
    awg2m__trig_cfg_t stop;            /*!< событие стопа . */
}cmd_awg2m_cfg_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_START AWG2M_START
 * \ingroup CMD_Commands AWGModule
 * \brief Команда программного старта
 */
/**@{*/
#define CMD__AWG2M_START           0x121E
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_start_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_start_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_STOP AWG2M_STOP
 * \ingroup CMD_Commands AWGModule
 * \brief Команда программной остановки
 */
/**@{*/
#define CMD__AWG2M_STOP           0x121F
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_stop_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_stop_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_IO_DIAG AWG2M_IO_DIAG
 * \ingroup CMD_Commands AWGModule
 * \brief Команда диагностики внутреннего информационного обмена.
 *        Возвращает \ref ANSWER_CMD__DONE в случае успешного выполнения, иначе - \ref RxiEthernet__cmd_error_e
 */
/**@{*/
#define CMD__AWG2M_IO_DIAG         0x1220
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t itf_id;             /*!< тестируемый интерфейс (0-все интерфейсы, по умолчанию). */
}cmd_awg2m_io_diag__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_io_diag__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_TO_ACC_SET AWG2M_ACC_TO_ACC_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда управления мультиплексором АСС. Самоконтроль АСС.
 * Подключает ACC к каналу питания ACC для самодиагностики.
 */
/**@{*/
#define CMD__AWG2M_ACC_TO_ACC_SET       0x1221
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pwr;                       /*!< Канал питания ACC, \ref awg2m__acc_pwr_e */
}cmd_awg2m_acc_to_acc_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_acc_to_acc_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_TO_ACC_GET AWG2M_ACC_ACC_TO_ACC_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда управления мультиплексором АСС. Самоконтроль АСС.
 * Возвращает канал питания ACC, к которому подключен ACC.
 */
/**@{*/
#define CMD__AWG2M_ACC_TO_ACC_GET         0x1222
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_acc_to_acc_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t pwr;                       /*!< Канал питания ACC, \ref awg2m__acc_pwr_e */
}cmd_awg2m_acc_to_acc_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_RANGE_SET AWG2M_ACC_RANGE_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда установки поддиапазона АСС
 */
/**@{*/
#define CMD__AWG2M_ACC_RANGE_SET         0x1223
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t range;             /*!< Поддиапазон ACC \ref awg2m__acc_rng_e */
}cmd_awg2m_acc_range_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_acc_range_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_RANGE_GET AWG2M_ACC_RANGE_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда возвращает поддиапазон АСС
 */
/**@{*/
#define CMD__AWG2M_ACC_RANGE_GET         0x1224
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_acc_range_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t range;             /*!< Поддиапазон ACC \ref awg2m__acc_rng_e */
}cmd_awg2m_acc_range_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_MEASURE AWG2M_ACC_MEASURE
 * \ingroup CMD_Commands AWGModule
 * \brief Команда выполнения преобразования ACC
 */
/**@{*/
#define CMD__AWG2M_ACC_MEASURE         0x1225
/**
 * Входные параметры команды.
 */
typedef struct {
    double time;             /*!< Время измерения, с. */
}cmd_awg2m_acc_measure__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    double voltage;          /*!< Измеренное напряжение, В. */
}cmd_awg2m_acc_measure__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_PWR_DIAG AWG2M_PWR_DIAG
 * \ingroup CMD_Commands AWGModule
 * \brief Команда диагностики питания
 */
/**@{*/
#define CMD__AWG2M_PWR_DIAG         0x1226
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_pwr_diag__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t err_12v_bp      :1; /*!< Ошибка по питанию 12V_BP */
    uint32_t err_5v_sc       :1;
    uint32_t err_5v_relay    :1;
    uint32_t err_3v3         :1;
    uint32_t err_2v5         :1;
    uint32_t err_acc_pwr     :1;
    uint32_t err_acc_p15v    :1;
    uint32_t err_acc_n15v    :1;
    uint32_t err_acc_5v      :1;
    uint32_t err_acc_5vd     :1;
    uint32_t err_acc_2v5     :1;
    uint32_t err_chx_pwr     :1;
    uint32_t err_chx_p15v    :1;
    uint32_t err_chx_n15v    :1;
    uint32_t err_chx_5v      :1;
    uint32_t err_acc_3v3     :1;
    uint32_t err_acc_1v8     :1;
}cmd_awg2m_pwr_diag__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_UNIT_DIAG AWG2M_UNIT_DIAG
 * \ingroup CMD_Commands AWGModule
 * \brief Команда диагностики функциональных узлов
 */
/**@{*/
#define CMD__AWG2M_UNIT_DIAG         0x1227
/**
 * Входные параметры команды.
 */
typedef struct {
    double time;                /*!< время измерения, с. */
}cmd_awg2m_unit_diag__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t err_lfp1      :1;  /*!< Сигнал постоянного напряжения, сформированный без использования фильтра, изменяется более чем на ±2мВ при использовании фильтра. */
    uint32_t err_lfp2      :1;  /*!< Сигнал постоянного напряжения, сформированный без использования фильтра, изменяется более чем на ±2мВ при использовании фильтра. */
    uint32_t err_ext_sig   :1;  /*!< Изменение сигнала встроенного задатчика смещения приводит к изменению смещения выходного сигнала канала. */
    uint32_t err_resistor  :1;  /*!< Переключение на согласованный выход не приводит к изменению выходного напряжения более чем на 10 мкВ. */
}cmd_awg2m_unit_diag__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_DC_DIAG AWG2M_DC_DIAG
 * \ingroup CMD_Commands AWGModule
 * \brief Команда частной диагностики метрологических характеристик каналов
 */
/**@{*/
#define CMD__AWG2M_DC_DIAG         0x1228
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;            /*!< Идентификатор выходного канала (0..3). */
    uint8_t dac;               /*!< Идентификатор DAC \ref awg2m__dac_e. */
    uint8_t range;             /*!< поддиапазон (только для DAC1 и DAC2). */
    uint8_t diff;              /*!< 1- diff, 0- не diff. */
    uint32_t spot_num;         /*!< число точек контроля . */
    double time;               /*!< время усреднения АЦП. */
}cmd_awg2m_dc_diag__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    double v_range;            /*!< диапазон размаха напряжений (максимальное выходное напряжение по модулю) */
    double v_max;              /*!< наибольшее напряжение относительно общего потенциала */
    double v_min;              /*!< наименьшее напряжение относительно общего потенциала */
    double step;               /*!< разрешение (усредненное значение разницы между соседними «ступенями» функции преобразования) */
    double deviation;          /*!< максимальное отклонение от аппроксимирующей прямой */
    uint32_t part_num;         /*!< Кол-во частей измерений в памяти, штук */
    struct {
        uint32_t abs_dac1 : 1;    /*!< 1 - Абсолютная допускаемая  погрешность воспроизведения выходного постоянного напряжения DAC1 не в допуске. */
        uint32_t abs_dac2 : 1;    /*!< 1 - Абсолютная допускаемая  погрешность воспроизведения выходного постоянного напряжения DAC2 не в допуске. */
        uint32_t abs_dac_ocm : 1; /*!< 1 - Абсолютная допускаемая  погрешность воспроизведения напряжения смещения DACOCM не в допуске. */
        uint32_t inl_dac1 : 1;    /*!< 1 - Нелинейность воспроизведения выходного постоянного напряжения DAC1 не в допуске. */
        uint32_t inl_dac2 : 1;    /*!< 1 - Нелинейность воспроизведения выходного постоянного напряжения DAC2 не в допуске. */
        uint32_t inl_dac_ocm : 1; /*!< 1 - Нелинейность воспроизведения напряжения смещения DACOCM не в допуске. */
    }err;
}cmd_awg2m_dc_diag__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_DC_DIAG_MEAS AWG2M_DC_DIAG_MEAS
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения результатов частной диагностики метрологических характеристик
 */
/**@{*/
#define CMD__AWG2M_DC_DIAG_MEAS         0x1229
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t part_num;         /*!< Идентификатор части измерений в памяти (от 0) */
}cmd_awg2m_dc_diag_meas__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t len;                            /*!< Размер полезных данных в data. */
    uint8_t data[RXIETHERNET_CMD_SIZE - 10]; /*!< Данные измерений*/
}cmd_awg2m_dc_diag_meas__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_FAST_DIAG AWG2M_FAST_DIAG
 * \ingroup CMD_Commands AWGModule
 * \brief Команда экспресс диагностики модуля.
 */
/**@{*/
#define CMD__AWG2M_FAST_DIAG         0x122A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t spot_num;                   /*!< число точек на поддиапазон. */
    double time;                         /*!< время одного измерения. */
}cmd_awg2m_fast_diag__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_fast_diag__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_TUNE AWG2M_ACC_TUNE
 * \ingroup CMD_Commands AWGModule
 * \brief Команда настройки АСС.
 *        В случае успешной настройки возвращает \ref ANSWER_CMD__DONE, иначе ошибку - \ref RxiEthernet__cmd_error_e
 */
/**@{*/
#define CMD__AWG2M_ACC_TUNE         0x122B
/**
 * Входные параметры команды.
 */
typedef struct {
    double time;                         /*!< время измерения. */
}cmd_awg2m_acc_tune__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_acc_tune__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_TUNE AWG2M_CH_TUNE
 * \ingroup CMD_Commands AWGModule
 * \brief Команда настройки каналов.
 *        В случае успешной настройки возвращает \ref ANSWER_CMD__DONE, иначе ошибку - \ref RxiEthernet__cmd_error_e
 */
/**@{*/
#define CMD__AWG2M_CH_TUNE         0x122C
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;                      /*!< Идентификатор выходного канала (0..3). */
    double time;                         /*!< время измерения. */
}cmd_awg2m_ch_tune__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_tune__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_CFG2_SET AWG2M_CH_CFG2_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда записи поправок второго уровня в энергонезависимую память.
 */
/**@{*/
#define CMD__AWG2M_CH_CFG2_SET         0x122D
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;                          /*!< Идентификатор выходного канала (0..3). */
    awg2m__ch_cc_t cc;                       /*!< Данные калибровочных коэффициентов*/
}cmd_awg2m_ch_cfg2_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_cfg2_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_CFG2_GET AWG2M_CH_CFG2_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда чтения поправок второго уровня из энергонезависимой памяти.
 */
/**@{*/
#define CMD__AWG2M_CH_CFG2_GET         0x122E
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;                         /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_cfg2_get__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
    uint32_t ch_id;                          /*!< Идентификатор выходного канала (0..3). */
    awg2m__ch_cc_t cc;                       /*!< Данные калибровочных коэффициентов*/
}cmd_awg2m_ch_cfg2_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_FIX2 AWG2M_CH_FIX2
 * \ingroup CMD_Commands AWGModule
 * \brief Команда управления коррекцией второго уровня.
 *        В случае успешной настройки возвращает \ref ANSWER_CMD__DONE, иначе ошибку - \ref RxiEthernet__cmd_error_e
 */
/**@{*/
#define CMD__AWG2M_CH_FIX2         0x122F
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;                          /*!< Идентификатор выходного канала (0..3). */
    uint16_t on;                             /*!< 0- выключен, 1- включен. */
    uint16_t len;                            /*!< Размер полезных данных в data. */
    uint8_t data[RXIETHERNET_CMD_SIZE - 10]; /*!< Данные для расчета*/
}cmd_awg2m_ch_fix2__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_ch_fix2__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_CH_CTRL_GET AWG2M_CH_CTRL_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда получения состояния выхода канала.
 */
/**@{*/
#define CMD__AWG2M_CH_CTRL_GET         0x1230
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
}cmd_awg2m_ch_ctrl_get__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
    uint32_t ch_id;             /*!< Идентификатор выходного канала (0..3). */
    uint16_t ch_on;             /*!< 0- выход отключен, 1- выход подключен. */
}cmd_awg2m_ch_ctrl_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_PWR_DIAG_MEAS AWG2M_PWR_DIAG_MEAS
 * \ingroup CMD_Commands AWGModule
 * \brief Команда возвращает измеренные значения последней диагностики питания
 */
/**@{*/
#define CMD__AWG2M_PWR_DIAG_MEAS         0x1231
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_pwr_diag_meas__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    double val_12v_bp;       /*!< Значение по питанию 12V_BP, В */
    double val_5v_sc;        /*!< Значение по питанию 5v_sc, В */
    double val_5v_relay;     /*!< Значение по питанию 5v_relay, В */
    double val_3v3;          /*!< Значение по питанию 3v3, В */
    double val_2v5;          /*!< Значение по питанию 2v5, В */
    double val_acc_pwr;      /*!< Значение по питанию acc_pwr, В */
    double val_acc_p15v;     /*!< Значение по питанию acc +15v, В */
    double val_acc_n15v;     /*!< Значение по питанию acc -15v, В */
    double val_acc_5v;       /*!< Значение по питанию acc_5v, В */
    double val_acc_5vd;      /*!< Значение по питанию acc_5vd, В */
    double val_acc_2v5;      /*!< Значение по питанию acc_2v5, В */
    double val_chx_pwr;      /*!< Значение по питанию chx_pwr, В */
    double val_chx_p15v;     /*!< Значение по питанию chx +15v, В */
    double val_chx_n15v;     /*!< Значение по питанию chx -15v, В */
    double val_chx_5v;       /*!< Значение по питанию chx_5v, В */
    double val_acc_3v3;      /*!< Значение по питанию acc_3v3, В */
    double val_acc_1v8;      /*!< Значение по питанию acc_1v8, В */
}cmd_awg2m_pwr_diag_meas__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_SYNCCLK_DUT_SET AWG2M_SYNCCLK_DUT_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Выбор внешнего входа для тактирования мезонина SyncCLock (0..3). Выполняется совместно с командой
 *        \ref CMD__ROSCILLATOR_SET в случае выбора внешнего источника тактирования.
 */
/**@{*/
#define CMD__AWG2M_SYNCCLK_DUT_SET         0x1232
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t dut_id;            /*!< Идентификатор входа (0..3). */
}cmd_awg2m_syncclk_dut_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_syncclk_dut_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_SYNCCLK_DUT_GET AWG2M_SYNCCLK_DUT_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Возвращает настройки внешнего входа для тактирования мезонина SyncCLock (0..3).
 */
/**@{*/
#define CMD__AWG2M_SYNCCLK_DUT_GET         0x1233
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_syncclk_dut_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t dut_id;            /*!< Идентификатор входа (0..3). */
}cmd_awg2m_syncclk_dut_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_TO_PWR_SET AWG2M_ACC_TO_PWR_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда управления мультиплексором АСС. Контроль цифрового питания.
 * Подключает ACC к каналу питания.
 */
/**@{*/
#define CMD__AWG2M_ACC_TO_PWR_SET       0x1234
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pwr;                       /*!< Канал питания ACC, \ref awg2m__pwr_e */
}cmd_awg2m_acc_to_pwr_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_acc_to_pwr_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_TO_PWR_GET AWG2M_ACC_ACC_TO_PWR_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда управления мультиплексором АСС. Контроль цифрового питания.
 * Возвращает канал питания, к которому подключен ACC.
 */
/**@{*/
#define CMD__AWG2M_ACC_TO_PWR_GET         0x1235
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_acc_to_pwr_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t pwr;                       /*!< Канал питания ACC, \ref awg2m__pwr_e */
}cmd_awg2m_acc_to_pwr_get__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_TO_CH_SET AWG2M_ACC_TO_CH_SET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда управления мультиплексором АСС. Контроль каналов.
 * Подключает ACC к компоненту канала.
 */
/**@{*/
#define CMD__AWG2M_ACC_TO_CH_SET       0x1236
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_id;                     /*!< Идентификатор выходного канала (0..3). */
    uint32_t unit;                      /*!< Канал питания ACC, \ref awg2m__ch_unit_e */
}cmd_awg2m_acc_to_ch_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_awg2m_acc_to_ch_set__out_t;
/**@}*/

/**
 * \defgroup GRP_AWG2M_ACC_TO_CH_GET AWG2M_ACC_ACC_TO_CH_GET
 * \ingroup CMD_Commands AWGModule
 * \brief Команда управления мультиплексором АСС. Контроль каналов.
 * Возвращает компонент канала, к которому подключен ACC.
 */
/**@{*/
#define CMD__AWG2M_ACC_TO_CH_GET         0x1237
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_awg2m_acc_to_ch_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t ch_id;                     /*!< Идентификатор выходного канала (0..3). */
    uint32_t unit;                      /*!< Канал питания ACC, \ref awg2m__ch_unit_e */
}cmd_awg2m_acc_to_ch_get__out_t;
/**@}*/










#pragma pack(pop)

#endif
