/*******************************************************************************
 * @file dev_setts_types.h.
 * @brief Типы данных модуля настроек.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DEV_SETTS_TYPES_H_
#define DEV_SETTS_TYPES_H_

/**
 * \brief Перечень настроечных параметров.
 */
typedef enum {
    /**
    * Серийный номер.
    */
    DEV_SETTS__SER_NUM    = 0,
    /**
    * Версия платы.
    */
    DEV_SETTS__MB_MODE    = 1,
    /**
    * Модбас адрес
    */
    DEV_SETTS__MB_ADDR    = 2,

    /**
    * Максимальное число параметров.
    */
    DEV_SETTS_PARAM_MAX,
    /**
    * Принудительный размер enum в 4 байта.
    */
    DEV_SETTS_PARAM_CLOSE = 0xFFFFFFFE,
    DEV_SETTS_PARAM_NONE = 0xFFFFFFFF
}dev_setts__params_e;











#endif
