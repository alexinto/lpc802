﻿/***************************************************************************//**
 * @file target.h.
 * @brief  Конфигурация целевого приложения.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TARGET_H
#define TARGET_H

#include "mcu.h"
#include "encoder.h"

#define SW_VERSION {0x00,0x00,0x83,0x00}    // Нули не удалять!

// Светодиодная индикация, интервалы переключения светодиода в мс.
#define DEVICE__LED_1WIRE       200         // Режим поиска датчиков 1wire
#define DEVICE__LED_MODBUS      500         // Инициализация modbus устройств
#define DEVICE__LED_COMPLETE    1000        // Загрузка завершена. Штатный режим работы.

//Конфигурация Модуля настроек
#define DEV_SETTS__ID   0                   // В проекте только одна флеш- флеш МК
#define DEV_SETTS__ADDR 0x08100000          // Начальный адрес флеш
#define DEV_SETTS__SIZE 0x8000              // Размер области памяти для настроек, кратен размеру сектора флеш!

// Конфигурация UART
#define HW_UART_COUNT 3
#define HW_UART_1 MCU__UART_1
#define HW_UART_1_TX MCU__GPIO_P_B_14
#define HW_UART_1_RX MCU__GPIO_P_B_15
#define HW_UART_2 MCU__UART_2
#define HW_UART_2_TX MCU__GPIO_P_A_2
#define HW_UART_2_RX MCU__GPIO_P_A_3
#define PSU_UART__DIR_PIN MCU__GPIO_P_A_1
#define HW_UART_3 MCU__UART_3
#define HW_UART_3_TX MCU__GPIO_P_D_8
#define HW_UART_3_RX MCU__GPIO_P_D_9
#define SYNC_UART__DIR_PIN MCU__GPIO_P_D_10
#define UART_SETTINGS (UART__BR_115200 | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FC_NONE)

#define USB_UART_COUNT          1
#define USB_UART_1 MCU__USB_COM_1

#define USER_BT          MCU__GPIO_P_C_13   // Синяя кнопка на отладке
#define LED_BLUE         MCU__GPIO_P_B_7    // Синий светодиод на отладке

// Кнфигурация 1Wire
#define ONEWIRE__CNT 1                      // Количество интерфейсов 1wire
#define ONEWIRE_1    0                      // Один интерфейс на HW_UART_1

// Конфигурация DS18b20
#define DS18B20__CNT 8                      // Количество термодатчиков

#define SPI__COUNT          3               // кол-во интерфейсов SPI в проекте

//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif