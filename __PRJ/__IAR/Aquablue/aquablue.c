#include <time.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "DRV/gpio.h"
#include "target.h"
#include "System/sw_timer.h"
#include "adc.h"
#include "System/supervisor.h"
#include "DRV/hw_uart.h"
#include "DRV/uart.h"
#include "pwm.h"
#include "DRV/gpio.h"
#include "ssd1306.h"
#include "DRV/i2c.h"
#include "fonts.h"
#include "local_time.h"


typedef enum {
    wait_press_buttons=0,
    process_button,
    bt_up_complete,
    bt_down_complete,
    bt_left_complete,
    bt_right_complete,
}button_set__e;

button_set__e button_state;   //if(button_state==bt_up_complete) ��������  button_count=0

typedef struct menu_list{
    uint8_t     *description;   //��������� �� ������ ������� ���� ��������
    struct menu_list *next;    // ��������� �� ��������� ����
    struct menu_list *prev;    // ��������� �� ���������� ����
    uint8_t active;  // ������� �� ������� ��� ����� ������  ���� 1 ���� 0
    uint8_t choise;  //������� ������ enter �� ��� ������
    my_time events_start; // ����� ������ �������
    my_time events_stop;  // ����� ����� �������
}menu_list;

menu_list main_menu[]={
    [0].description="���������",[0].next=&main_menu[5],[0].prev=&main_menu[0],
    [1].description="������ ���������",[1].next=&main_menu[8],[1].prev=&main_menu[1],
    [2].description="������ ��2",[2].next=&main_menu[11],[2].prev=&main_menu[2],
    [3].description="��������� �������",[3].next=&main_menu[14],
    [4].description=NULL,[4].prev=&main_menu[0],
    [5].description="TIME ON",[5].next=&main_menu[14],[5].prev=&main_menu[0],[5].choise=1,[5].active=1,
    [6].description="TIME OFF",[6].next=&main_menu[14],[6].prev=&main_menu[0],[6].choise=1,
    [7].description=NULL,[7].prev=&main_menu[0],
    [8].description="TIME ON",[8].next=&main_menu[14],[8].prev=&main_menu[1],[8].choise=1,[8].active=1,
    [9].description="TIME OFF",[9].next=&main_menu[14],[9].prev=&main_menu[1],[9].choise=1,
    [10].description=NULL,[10].prev=&main_menu[1],
    [11].description="TIME ON",[11].next=&main_menu[14],[11].prev=&main_menu[2],[11].choise=1,[11].active=1,
    [12].description="TIME OFF",[12].next=&main_menu[14],[12].prev=&main_menu[2],[12].choise=1,
    [13].description=NULL,[13].prev=&main_menu[2],
    [14].description="��������� �������",[14].prev=&main_menu[3],[14].choise=1,[14].active=1,
    [15].description=NULL,[13].prev=&main_menu[3]};

menu_list* curent_page;
extern my_time timers;
static uint8_t i=0;
static uint8_t out_watch[9]={0};
uint8_t index_string_display;
uint8_t button_count;
static uint8_t sleep_menu=0;
static sw_timer__t timer_display_update;
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
void bt_handler_left(int gpio, gpio__state_e pin_state);
void bt_handler_right(int gpio, gpio__state_e pin_state);
void bt_handler_up(int gpio, gpio__state_e pin_state);
void bt_handler_down(int gpio, gpio__state_e pin_state);
void main_cout(void);
void button_cout(void);



int main()
{
    supervisor__init();
    sw_timer__init(NULL);
    ssd1306_init();
    ssd1306_clr();
    set_time(23,20,58);
    gpio__init(bt_left, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    gpio__init(bt_right, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    gpio__init(bt_up, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    gpio__init(bt_down, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    sw_timer__start(&timer_display_update, 1000, timer_cb, NULL);
    curent_page=&main_menu[0];
    curent_page->active=1;


    while(1){
        wdt__reset();
        sw_timer__cout();
        adc__cout();
        i2c__cout();
        main_cout();
        button_cout();
    }
}
static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    uint8_t buff[20]={' '};
    if(sleep_menu==0){
        if(curent_page[i].description!=NULL){
            if(curent_page[i].next==NULL)
                sprintf((char *)buff+1,"%s %d:%d",curent_page[i].description,curent_page[i].events_start.hours,curent_page[i].events_start.minuts);
            else
                sprintf((char *)buff+1,"%s",curent_page[i].description);
            if(curent_page[i].active)
                buff[0]='>';
            ssd1306_putc(buff,0, i+2,SMALL_SIZE);
            i++;
        }
        else
            i=0;
        sprintf((char *)out_watch,"%02d:%02d",get_time(HOURS_SET),get_time(MINUT_SET));
        ssd1306_putc(out_watch, 13, 0,SMALL_SIZE);
        sw_timer__start(&timer_display_update, 500, timer_cb, NULL);
    }
    else{
        sw_timer__start(&timer_display_update, 500, timer_cb, NULL);
        sprintf((char *)out_watch,"%02d:%02d:%02d",get_time(HOURS_SET),get_time(MINUT_SET),get_time(SECONDS_SET));
        ssd1306_putc(out_watch, 0, 1,BIG_SIZE);
    }
}

void main_cout(void){

}
void button_cout(void){
    //uint8_t button=gpio__get(bt_up);
    //button&=gpio__get(bt_up)<1;
    //button&=gpio__get(bt_left)<2;
    //button&=gpio__get(bt_down)<3;
    //button&=gpio__get(bt_right)<4;


    //const uint8_t gpio_button[4]
    //uint8_t button_masiv[4]={bt_up,bt_left,bt_down,bt_right};
    //for(int i=0;i<4;i++)
    //    button[i]=gpio__get(button_masiv[i]);


    uint8_t button[4];
    button[0]=gpio__get(bt_up);
    button[1]=gpio__get(bt_down);
    button[2]=gpio__get(bt_left);
    button[3]=gpio__get(bt_right);
    uint8_t i;

    switch(button_state){
    case wait_press_buttons:
        for(i=0;i<4;i++)
            if(button[i]==GPIO__STATE_LOW){
                if(button_count==0)
                    button_state=process_button;
                else
                    break;
            }
        if(i>3)
            button_count=0;
        break;
    case process_button:
        for(i=0;i<4;i++)
            if(button[i]==GPIO__STATE_LOW)
                break;
        if(i>3)
            button_state=wait_press_buttons;
        else if(button_count++>100)
            button_state=(button_set__e)(i+2);
        break;
    case bt_up_complete:
        for(i=0;!(curent_page[i].active);i++);
        if(curent_page[i].prev!=&main_menu[0]){
            curent_page[i].active=0;
            curent_page[i-1].active=1;
        }
        button_state=wait_press_buttons;
        break;
    case bt_down_complete:
        for(i=0;!(curent_page[i].active);i++);
        if(curent_page[i+1].description!=NULL){
            curent_page[i].active=0;
            curent_page[i+1].active=1;
        }
        button_state=wait_press_buttons;
        break;
    case bt_left_complete:

        if(button[2])
            button_state=wait_press_buttons;
        break;
    case bt_right_complete:

        //if(button[3])
        button_state=wait_press_buttons;
        break;
    }
}

/*
if(gpio__get(bt_down)==GPIO__STATE_LOW && button_down<200)
button_down++;
    else if(button_down>100 && button_down<200) {
button_down=2 00;
button_down_events=1;
    }
    else
button_down=0;


if(gpio__get(bt_left)==GPIO__STATE_LOW && button_left<200)
button_left++;
    else if(button_left>100 && button_left<200) {
button_left=200;
button_left_events=1;
    }
    else
button_left=0;

if(gpio__get(bt_right)==GPIO__STATE_LOW && button_right<200)
button_right++;
    else if(button_right>100 && button_right<200) {
button_right=200;
button_right_events=1;
    }
    else
button_right=0;
*/

