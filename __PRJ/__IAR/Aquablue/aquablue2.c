#include <time.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "gpio.h"
#include "target.h"
#include "sw_timer.h"
#include "adc.h"
#include "supervisor.h"
#include "hw_uart.h"
#include "uart.h"
#include "pwm.h"
#include "gpio.h"
#include "ssd1306.h"
#include "i2c.h"
#include "fonts.h"
#include "local_time.h"
//#include "menu.h"



typedef enum {
  wait_press_buttons=0,
  process_button,
  bt_up_complete,
  bt_down_complete,
  bt_left_complete,
  bt_right_complete,
}button_set__e;

button_set__e button_state;   //if(button_state==bt_up_complete) ��������  button_count=0

typedef struct{
  uint8_t     *string1;   //��������� �� ������ ������� ���� ��������
  uint8_t     *string2; // ���
  uint8_t     *string3; // ����
  my_time     param1; // ����� ������ �������
  my_time     param2;  // ����� ����� ������� 
}page_t;

page_t menu[] = {
  [0].string1="���������    ",[0].string2="���",[0].string3="����  ",
  [1].string1="������ ��2   ",[1].string2="���",[1].string3="����  ",
  [2].string1="������ ���   ",[2].string2="���",[2].string3="������/��",
  [3].string1="���. ������� ",[3].string2="���",[3].string3="����      ��  ",
};

struct {
  uint8_t curent_page;
  uint8_t cursor;  
  uint8_t next;
  uint8_t sleep_menu;
  uint8_t button_count; 
  sw_timer__t timer_display_update;
}menu_data;


extern my_time timers;
static uint8_t out_watch[5]={0};
static uint16_t sleep_count;
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
void main_cout(void);
void button_cout(void);




int main()
{   
  *(uint32_t*)0xE000ED30 = 0xFFFFFFFF;
  *(uint32_t*)0xE000EDF0 = 0xFFFFFFFF;
  *(uint32_t*)0xE000EDF4 = 0xFFFFFFFF;
  *(uint32_t*)0xE000EDF8 = 0xFFFFFFFF;
  *(uint32_t*)0xE000EDFC = 0xFFFFFFFF;
  
  supervisor__init();
  sw_timer__init(NULL);
  ssd1306_init();
  ssd1306_clr();
  set_time_local(&menu[3].param1);

  gpio__init(bt_left, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
  gpio__init(bt_right, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
  gpio__init(bt_up, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
  gpio__init(bt_down, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
  //gpio__init(PUMP_GPIO,GPIO__DIR_OUT,NULL);
  //gpio__init(CARBON_GPIO,GPIO__DIR_OUT,NULL);
  //gpio__init(LIGHT_GPIO,GPIO__DIR_OUT,NULL);
        
  sw_timer__start(&menu_data.timer_display_update, 1000, timer_cb, NULL);
  menu_data.curent_page=0;
  
  
  while(1){
    wdt__reset();
    sw_timer__cout();
    adc__cout();
    i2c__cout();
    main_cout();
    button_cout();
  }
}
uint8_t activ_cursor=0;
uint8_t set_next_cursor;
typedef  struct {
  uint8_t cursor_x;
  uint8_t cursor_y;
  uint8_t* cursor;
}cursor_type_t;
cursor_type_t cursor_data[5] = {[0].cursor_x = 0, [0].cursor_y = 2, [0].cursor = ">",
                                [1].cursor_x = 10,[1].cursor_y = 5, [1].cursor = "--",
                                [2].cursor_x = 14,[2].cursor_y = 5, [2].cursor = "--",
                                [3].cursor_x = 10,[3].cursor_y = 7, [3].cursor = "--",
                                [4].cursor_x = 14,[4].cursor_y = 7, [4].cursor = "--"};
uint8_t *empty_string="  ";
static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
  uint8_t time_string[5]={"    "};
  uint8_t time_hours[3];
  uint8_t time_minuts[3];
  if(menu_data.sleep_menu==0){
      switch(menu_data.next){
      case 0:
        ssd1306_putc(menu[menu_data.curent_page].string1, 3, 2,SMALL_SIZE);
        break;
      case 1:
        if(menu_data.curent_page<3){
        ssd1306_putc(menu[menu_data.curent_page].string2, 3, 4,SMALL_SIZE);
        }
        else
          ssd1306_putc(time_string, 3, 4,SMALL_SIZE);
        break;
      case 2:
        sprintf((char *)time_hours,"%02d:",menu[menu_data.curent_page].param1.hours); // ����� ���������
        ssd1306_putc(time_hours, 10, 4,SMALL_SIZE);
        break;
      case 3:
        sprintf((char *)time_minuts,"%02d",menu[menu_data.curent_page].param1.minuts); // ����� ����������
        ssd1306_putc(time_minuts, 14, 4,SMALL_SIZE);
        break;
      case 4:
        ssd1306_putc(menu[menu_data.curent_page].string3, 3, 6,SMALL_SIZE);
        break;
      case 5:
        if(menu_data.curent_page<2){
          sprintf((char *)time_hours,"%02d:",menu[menu_data.curent_page].param2.hours); // ����� ���������
          ssd1306_putc(time_hours, 10, 6,SMALL_SIZE);
        }
          break;
      case 6:
        if(menu_data.curent_page<3){
          sprintf((char *)time_minuts,"%02d ",menu[menu_data.curent_page].param2.minuts); // ����� ����������
          ssd1306_putc(time_minuts, 14, 6,SMALL_SIZE);
        }
        break;
      case 7:
        if(set_next_cursor!=activ_cursor){
          ssd1306_putc(empty_string, cursor_data[activ_cursor].cursor_x, cursor_data[activ_cursor].cursor_y,SMALL_SIZE);
          activ_cursor=set_next_cursor;
        }
        else
          ssd1306_putc(cursor_data[activ_cursor].cursor, cursor_data[activ_cursor].cursor_x, cursor_data[activ_cursor].cursor_y,SMALL_SIZE);
        break;
      case 8:
        sprintf((char *)out_watch,"%02d:%02d",get_time(&timers,HOURS_SET),get_time(&timers,MINUT_SET));
        ssd1306_putc(out_watch, 13, 0,SMALL_SIZE);
        break;
      default:
        menu_data.next=255;
      }
      menu_data.next+=1;
      sleep_count+=1;
      if(sleep_count==3500){
        sleep_count=0;
        ssd1306_clr();
        menu_data.sleep_menu=1;
      }
      sw_timer__start(&menu_data.timer_display_update, 20, timer_cb, NULL);
  }  
  else{
    sw_timer__start(&menu_data.timer_display_update, 300, timer_cb, NULL);
    sprintf((char *)out_watch,"%02d:%02d",get_time(&timers,HOURS_SET),get_time(&timers,MINUT_SET));
    ssd1306_putc(out_watch, 3, 1,BIG_SIZE);
  }
}

typedef struct {
  uint8_t* param;
  uint8_t min;
  uint8_t max;
}param_table_t;

void button_cout(void){  
  uint8_t button[4];
  button[0]=gpio__get(bt_up);
  button[1]=gpio__get(bt_down);
  button[2]=gpio__get(bt_left);
  button[3]=gpio__get(bt_right);
  uint8_t i; 
  
 param_table_t param_table[5] = {
    &menu_data.curent_page, 0, 3,
    &menu[menu_data.curent_page].param1.hours, 0, 23,
    &menu[menu_data.curent_page].param1.minuts, 0, 59,
    &menu[menu_data.curent_page].param2.hours, 0 , 23,
    &menu[menu_data.curent_page].param2.minuts, 0, 59,
  };
  

  switch(button_state){    
  case wait_press_buttons:       
    for(i=0;i<4;i++)
      if(button[i]==GPIO__STATE_LOW){
        if(menu_data.button_count==0)
          button_state=process_button;
        else
          break;
      }
    if(i>3)
      menu_data.button_count=0;
    break;
  case process_button:
    for(i=0;i<4;i++)
      if(button[i]==GPIO__STATE_LOW)
        break;
    if(i>3)
      button_state=wait_press_buttons;
    else if(menu_data.button_count++>100) 
      button_state=(button_set__e)(i+2); 
    if(menu_data.sleep_menu==1){
      menu_data.sleep_menu=0;
      ssd1306_clr();
    }
    break;
  case bt_up_complete:
    if(menu_data.curent_page==3 || menu_data.curent_page==2)
      if(set_next_cursor==4)
        set_next_cursor-=1;
    if(set_next_cursor>0)   
      set_next_cursor-=1;
    button_state=wait_press_buttons;
    break;
  case bt_down_complete:
    if(menu_data.curent_page==3 || menu_data.curent_page==2)
      if(set_next_cursor==2)
        set_next_cursor+=1;
    if(set_next_cursor<4)
      set_next_cursor+=1;
    button_state=wait_press_buttons;
    break;
  case bt_left_complete:
    if (*param_table[activ_cursor].param > param_table[activ_cursor].min)
      *param_table[activ_cursor].param -= 1;
    button_state=wait_press_buttons;
    set_time(&menu[menu_data.curent_page].param1);
    set_time(&menu[menu_data.curent_page].param2);
    break;
  case bt_right_complete:
    if (*param_table[activ_cursor].param < param_table[activ_cursor].max)
      *param_table[activ_cursor].param += 1;
    button_state=wait_press_buttons;
    set_time(&menu[menu_data.curent_page].param1);
    set_time(&menu[menu_data.curent_page].param2);
    break;
  }
  if (menu[3].param2.minuts>0){
      set_time_local(&menu[3].param1);
      menu[3].param2.minuts=0;
    }
}


void main_cout(void){
  if(menu[0].param1.time<timers.time && menu[0].param2.time>timers.time)
    gpio__set(LIGHT_GPIO,GPIO__STATE_HIGH);
  else
    gpio__set(LIGHT_GPIO,GPIO__STATE_LOW);
  if(menu[1].param1.time<timers.time && menu[1].param2.time>timers.time)
    gpio__set(CARBON_GPIO,GPIO__STATE_HIGH);
  else
    gpio__set(CARBON_GPIO,GPIO__STATE_LOW);
  if(menu[2].param1.time<timers.time && ((menu[2].param1.time+(menu[2].param2.minuts*PUMP_ML))>timers.time))
    gpio__set(PUMP_GPIO,GPIO__STATE_HIGH);
  else
    gpio__set(PUMP_GPIO,GPIO__STATE_LOW);
  
  
}