#ifndef MENU_H
#define MENU_H

#include <stdint.h>
#include "local_time.h"


typedef struct menu_list{
    uint8_t     *description;   // ��������� �� ������ ������� ���� ��������
    struct set_times *next;     // ��������� �� ��������� ����
    uint8_t active;             // ������� �� ������� ��� ����� ������  ���� 1 ���� 0
    uint8_t choise;             // ������� ������ enter �� ��� ������     
}menu_list;

typedef struct set_times{
    uint8_t *description;       // ��������� �� ������ ������� ���� ��������
    struct menu_list *prev;     // ��������� �� ���������� ����
    uint8_t active;             // ������� �� ������� ��� ����� ������  ���� 1 ���� 0
    my_time events_start;       // ����� ������ �������
    my_time events_stop;        // ����� ����� ������� 
}set_times;

set_times set_time_ligth[];
set_times set_time_pump[];
set_times set_time_c02[];
set_times set_time_local[];


menu_list main_menu[]={
    [0].description="���������",[0].next=&set_time_ligth[0],
    [1].description="������ ���������",[1].next=&set_time_pump[0],
    [2].description="������ ��2",[2].next=&set_time_c02[0],
    [3].description="��������� �������",[3].next=&set_time_local[0]};


set_times set_time_ligth[]={
    [0].description="���������",[0].prev=&main_menu[0],
    [1].description="TIME ON",[1].choise=1,[1].active=1,
    [2].description="TIME OFF"};

set_times set_time_pump[]={
    [0].description="������ ���������",[0].prev=&main_menu[1],
    [1].description="TIME ON",[1].choise=1,[1].active=1,
    [2].description="TIME OFF"};

set_times set_time_c02[]={
    [0].description="������ ��2",[0].prev=&main_menu[2],
    [1].description="TIME ON",[1].choise=1,[1].active=1,
    [2].description="TIME OFF"};

set_times set_time_local[]={
    [0].description="��������� �������",[0].prev=&main_menu[3],
    [1].description="TIME ON",[1].choise=1,[1].active=1,
    [2].description="TIME OFF"};


#endif