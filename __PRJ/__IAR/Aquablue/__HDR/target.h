#ifndef __TARGET_H
#define __TARGET_H
#define UART_DISABLE
#include "LPC802/mcu.h"

#define RUSSIAN_FONT


#define SSD1306_I2C     I2C_0
#define SSD1306_SCL     SCL__GPIO_PIN_10
#define SSD1306_SDA     SDA__GPIO_PIN_16

#define bt_left         MCU__GPIO_PIN_8
#define bt_right        MCU__GPIO_PIN_7
#define bt_up           MCU__GPIO_PIN_9
#define bt_down         MCU__GPIO_PIN_12
#define PUMP_GPIO       MCU__GPIO_PIN_15
#define CARBON_GPIO     MCU__GPIO_PIN_16
#define LIGHT_GPIO      MCU__GPIO_PIN_17



#define PUMP_ML         10                     // ����������� ������ �� ������ ������

//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP




#endif