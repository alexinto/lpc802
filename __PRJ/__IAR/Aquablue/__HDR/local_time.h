
#ifndef LOCAL_TIME_H_
#define LOCAL_TIME_H_

#include <stdint.h>

#define HOURS_SET   0
#define MINUT_SET   1
#define SECONDS_SET 2

typedef struct {
    uint8_t second;
    uint8_t minuts;
    uint8_t hours;
    uint32_t time;
}my_time;

void set_time(my_time *time_value);
void set_time_local(my_time *time_value);
uint32_t get_time_local(uint8_t set);
uint32_t get_time(my_time *time_value,uint8_t set);

#endif  /* LOCAL_TIME_H_ */