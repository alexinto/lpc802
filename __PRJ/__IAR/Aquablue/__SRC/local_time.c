#include "local_time.h"
#include "System/sw_timer.h"

sw_timer__t timer_local;
my_time timers;
static sw_timer__t timer_local;
static void timer_loc_cb(struct sw_timer__t *timer, void *ext_data);

void set_time(my_time *time_value){
  time_value->time =(time_value->hours*3600)+(time_value->minuts*60)+time_value->second;
}
void set_time_local(my_time *time_value){
  timers=*time_value;
  timers.time =(timers.hours*3600)+(timers.minuts*60)+timers.second;
  sw_timer__start(&timer_local, 1000, timer_loc_cb, NULL);
}
static void timer_loc_cb(struct sw_timer__t *timer, void *ext_data) {
  timers.time++;
  if(timers.time==86400)
      timers.time=0;
  sw_timer__start(&timer_local, 1000, timer_loc_cb, NULL);
}

uint32_t get_time_local(uint8_t set){
  uint16_t res;
  timers.hours=timers.time/3600;
  timers.minuts=(timers.time%3600)/60;
  timers.second=timers.time%60;
  if(set==0)
    res=timers.hours;
  if(set==1)
    res=timers.minuts;
  if(set==2)
    res=timers.second;
  return res;
}
uint32_t get_time(my_time *time_value,uint8_t set){
  uint16_t res;
  time_value->hours=time_value->time/3600;
  time_value->minuts=(time_value->time%3600)/60;
  time_value->second=time_value->time%60;
  if(set==0)
    res=time_value->hours;
  if(set==1)
    res=time_value->minuts;
  if(set==2)
    res=time_value->second;
  return res;
}