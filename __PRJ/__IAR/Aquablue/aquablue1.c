#include <time.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "gpio.h"
#include "target.h"
#include "sw_timer.h"
#include "adc.h"
#include "supervisor.h"
#include "hw_uart.h"
#include "uart.h"
#include "pwm.h"
#include "gpio.h"
#include "ssd1306.h"
#include "i2c.h"
#include "fonts.h"
#include "local_time.h"
//#include "menu.h"



typedef enum {
  wait_press_buttons=0,
  process_button,
  bt_up_complete,
  bt_down_complete,
  bt_left_complete,
  bt_right_complete,
}button_set__e;

button_set__e button_state;   //if(button_state==bt_up_complete) ��������  button_count=0

typedef struct menu_list{
  uint8_t     *description;   //��������� �� ������ ������� ���� ��������
  struct      menu_list *next;    // ��������� �� ��������� ����
  struct      menu_list *prev;    // ��������� �� ���������� ����
 // uint8_t     active;  // ������� �� ������� ��� ����� ������  ���� 1 ���� 0
 // uint8_t     choise;  //������� ������ enter �� ��� ������ 
  my_time     events_start; // ����� ������ �������
  my_time     events_stop;  // ����� ����� ������� 
  uint8_t     *on_description;
  uint8_t     *off_description;
}menu_list;

menu_list main_menu[]={
  [0].description="���������:",[0].on_description="���",[0].off_description="����",[0].next=&main_menu[1],[0].prev=&main_menu[3],
  [1].description="������ ���������",[1].on_description="���",[1].off_description="����",[1].next=&main_menu[2],[1].prev=&main_menu[0],
  [2].description="������ ��2",[2].on_description="���",[2].off_description="����",[2].next=&main_menu[3],[2].prev=&main_menu[1],
  [3].description="��������� �������",[3].on_description="���",[3].off_description="����",[3].next=&main_menu[0],[3].prev=&main_menu[2]};


menu_list* curent_page;
extern my_time timers;
static uint8_t invert_cursor=0;
static uint8_t cursor=0;
static uint8_t next=0;
static uint8_t out_watch[5]={0};
uint8_t index_string_display;
uint8_t button_count; 
static uint8_t sleep_menu=0;
static sw_timer__t timer_display_update;
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
void bt_handler_left(int gpio, gpio__state_e pin_state);
void bt_handler_right(int gpio, gpio__state_e pin_state);
void bt_handler_up(int gpio, gpio__state_e pin_state);
void bt_handler_down(int gpio, gpio__state_e pin_state);
void main_cout(void);
void button_cout(void);




int main()
{   
  supervisor__init();
  sw_timer__init(NULL);
  ssd1306_init();
  ssd1306_clr();
  set_time(13,59,58);
  gpio__init(bt_left, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
  gpio__init(bt_right, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
  gpio__init(bt_up, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
  gpio__init(bt_down, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
  sw_timer__start(&timer_display_update, 1000, timer_cb, NULL);
  curent_page=&main_menu[0];
  curent_page->active=1;
  
  
  while(1){
    wdt__reset();
    sw_timer__cout();
    adc__cout();
    i2c__cout();
    main_cout();
    button_cout();
  }
}
uint8_t activ_cursor=0;
uint8_t set_next_cursor;
typedef  struct {
  uint8_t cursor_x;
  uint8_t cursor_y;
  uint8_t* cursor;
}cursor_type_t;
cursor_type_t cursor_data[5] = {[0].cursor_x = 0, [0].cursor_y = 2, [0].cursor = ">",
[1].cursor_x = 10,[1].cursor_y = 5, [1].cursor = "--",
[2].cursor_x = 14,[2].cursor_y = 5, [2].cursor = "--",
[3].cursor_x = 10,[3].cursor_y = 7, [3].cursor = "--",
[4].cursor_x = 14,[4].cursor_y = 7, [4].cursor = "--"};
uint8_t *empty_string="  ";
static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
  uint8_t time_hours[3];
  uint8_t time_minuts[2];
  uint8_t buff[20]={' '};
  
  if(sleep_menu==0){
    if(curent_page[cursor].active==1){
      switch(next){
      case 0:
        ssd1306_putc(curent_page[cursor].description, 3, 2,SMALL_SIZE);
        break;
      case 1:
        ssd1306_putc(curent_page[cursor].on_description, 3, 4,SMALL_SIZE);
        break;
      case 2:
        sprintf((char *)time_hours,"%02d:",curent_page[cursor].events_start.hours); // ����� ���������
        ssd1306_putc(time_hours, 10, 4,SMALL_SIZE);
        break;
      case 3:
        sprintf((char *)time_minuts,"%02d",curent_page[cursor].events_start.minuts); // ����� ����������
        ssd1306_putc(time_minuts, 14, 4,SMALL_SIZE);
        break;
      case 4:
        ssd1306_putc(curent_page[cursor].off_description, 3, 6,SMALL_SIZE);
        break;
      case 5:
        sprintf((char *)time_hours,"%02d:",curent_page[cursor].events_start.hours); // ����� ���������
        ssd1306_putc(time_hours, 10, 6,SMALL_SIZE);
        break;
      case 6:
        sprintf((char *)time_minuts,"%02d",curent_page[cursor].events_start.minuts); // ����� ����������
        ssd1306_putc(time_minuts, 14, 6,SMALL_SIZE);
        break;
      case 7:
        if(set_next_cursor!=activ_cursor){
          ssd1306_putc(empty_string, cursor_data[activ_cursor].cursor_x, cursor_data[activ_cursor].cursor_y,SMALL_SIZE);
          activ_cursor=set_next_cursor;
        }
        else
          ssd1306_putc(cursor_data[activ_cursor].cursor, cursor_data[activ_cursor].cursor_x, cursor_data[activ_cursor].cursor_y,SMALL_SIZE);
        break;
      default:
        next=0;
      }
      next+=1;
    }
    sprintf((char *)out_watch,"%02d:%02d",get_time(HOURS_SET),get_time(MINUT_SET));
    ssd1306_putc(out_watch, 13, 0,SMALL_SIZE);
    sw_timer__start(&timer_display_update, 50, timer_cb, NULL);
  }
  else{
    sw_timer__start(&timer_display_update, 3000, timer_cb, NULL);
    sprintf((char *)out_watch,"%02d:%02d",get_time(HOURS_SET),get_time(MINUT_SET));
    ssd1306_putc(out_watch, 3, 1,BIG_SIZE);
  }
}


void main_cout(void){
  
}
void button_cout(void){  
  uint8_t button[4];
  button[0]=gpio__get(bt_up);
  button[1]=gpio__get(bt_down);
  button[2]=gpio__get(bt_left);
  button[3]=gpio__get(bt_right);
  uint8_t i;    
  switch(button_state){    
  case wait_press_buttons:       
    for(i=0;i<4;i++)
      if(button[i]==GPIO__STATE_LOW){
        if(button_count==0)
          button_state=process_button;
        else
          break;
      }
    if(i>3)
      button_count=0;
    break;
  case process_button:
    for(i=0;i<4;i++)
      if(button[i]==GPIO__STATE_LOW)
        break;
    if(i>3)
      button_state=wait_press_buttons;
    else if(button_count++>100) 
      button_state=(button_set__e)(i+2); 
    break;
  case bt_up_complete:
    if(set_next_cursor>0)
      set_next_cursor-=1;
    button_state=wait_press_buttons;
    break;
  case bt_down_complete:
    if(set_next_cursor<4)
      set_next_cursor+=1;
    button_state=wait_press_buttons;
    break;
  case bt_left_complete:
    button_state=wait_press_buttons; 
    break;
  case bt_right_complete:
    button_state=wait_press_buttons; 
    break;
  }
}
