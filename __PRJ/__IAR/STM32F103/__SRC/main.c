#include <stddef.h>
#include "stm32f10x.h"
#include "mcu.h"
#include "gpio.h"

void button_cb(int gpio, gpio__state_e pin_state);

void main() {
    
    gpio__init(MCU__GPIO_P_A_5, GPIO__DIR_OUT, NULL);
    gpio__init(MCU__GPIO_P_B_1, GPIO__DIR_OUT, NULL);
    gpio__init(MCU__GPIO_P_C_13, GPIO__DIR_IN | GPIO__PULL_UP | 
               GPIO__INT_EDGE_RISING | GPIO__INT_EDGE_FALING, button_cb);
   
    RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
    
    TIM4->DIER |= TIM_DIER_UIE; 
	//��������
	TIM4->PSC = 8000;
	//�������� ������������
    TIM4->ARR = 1000;    
	//�������� �������
	TIM4->CR1 |= TIM_CR1_CEN;    
    
    NVIC_EnableIRQ(TIM4_IRQn);
    while(1) {
        
    }
}

void timer_4() {
    TIM4->SR &= ~TIM_SR_UIF;
    gpio__set(MCU__GPIO_P_A_5, GPIO__STATE_TOGGLE);
}


void button_cb(int gpio, gpio__state_e pin_state) {

    if (pin_state == GPIO__STATE_HIGH) {
        TIM4->ARR = TIM4->CNT;
        TIM4->CNT = 0;
        gpio__set(MCU__GPIO_P_A_5, GPIO__STATE_LOW);
        GPIOA->BRR |= (1 << 5);
    }
    else {
        TIM4->ARR = 40000;
        TIM4->CNT = 0;
        gpio__set(MCU__GPIO_P_A_5, GPIO__STATE_HIGH);
    }


}
