/***************************************************************************//**
 * @file target.h.
 * @brief  Конфигурация целевого приложения.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TARGET_H
#define TARGET_H

#include "mcu.h"

#define UART_SETTINGS (UART__BR_115200 | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FC_NONE)

#define USB_UART_COUNT          1
#define USB_UART_1 MCU__USB_COM_1




#define USER_BT MCU__GPIO_P_C_13
#define LED_BLUE MCU__GPIO_P_B_7
#define LED_GREEN MCU__GPIO_P_B_0




//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif