#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "gpio.h"
#include "target.h"
#include "System/sw_timer.h"
#include "adc.h"
#include "supervisor.h"
#include "i2c.h"
//#include "DRV/hw_uart.h"
//#include "DRV/uart.h"
#include "ssd1306.h"
#include "pwm.h"
#include "System/framework.h"

static uint16_t adc_buff[5];
static uint16_t adc_res;
static uint8_t uart_rx_buff[80];
static uint8_t uart_tx_buff[80];

static sw_timer__t timer;
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void bt_handler(int gpio, gpio__state_e pin_state);
static void adc_cb(adc__event_e event);
static void uart_cb(int uart_id, events__e event, uint8_t *buff, int len);
static void main__cout();


void main() {
    char buff[100];
    sprintf(buff, "123");
    supervisor__init();
    sw_timer__init(NULL);
//    adc__init(ADC_8 | ADC_7 | ADC_6);
//    adc__start(adc_buff, adc_cb);
    gpio__init(led_4, GPIO__DIR_OUT, NULL);
    gpio__set(led_4, GPIO__STATE_LOW);

    gpio__init(user_bt, GPIO__DIR_IN | GPIO__PULL_UP | GPIO__INT_EDGE_FALING | GPIO__INTR_0, bt_handler);
    sw_timer__start(&timer, 500, timer_cb, NULL);
//    hw_uart__init(UART_0, UART0_SETTINGS, NULL);
    sprintf((char*)uart_tx_buff, "AT\r\n");
//    hw_uart__tx(UART_0, uart_tx_buff, strlen((char*)uart_tx_buff), uart_cb);


    while(1) {
        framework__cout();
        wdt__reset();
//        supervisor__cout();
//        hw_uart__cout();
        adc__cout();
        main__cout();
   }
}

static void main__cout() {

}

static const char* const bt_commands[] = {"read",
                                          "write",};

static void uart_cb(int uart_id, events__e event, uint8_t *buff, int len) {
    __IO int x = sizeof(bt_commands) / sizeof(void*);
//    if (buff == uart_tx_buff)
//        hw_uart__rx(UART_0, uart_rx_buff, 20, uart_cb);
//    else
//        sw_timer__start(&timer, 1000, timer_cb, NULL);
}

void bt_handler(int gpio, gpio__state_e pin_state) {

}


static void adc_cb(adc__event_e event) {
    if ((event == ADC__EVENT_OK) && (adc_res))
        adc__start(adc_buff, adc_cb);
    else {
//        memset(adc_buff, 0x00, 10);
        adc_res = 0;
    }
}

static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, timer_cb, NULL);
    gpio__set(led_4, GPIO__STATE_TOGGLE);
//    hw_uart__tx(UART_0, uart_tx_buff, strlen((char*)uart_tx_buff), uart_cb);
}
