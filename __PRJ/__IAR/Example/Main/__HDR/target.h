#ifndef __TARGET_H
#define __TARGET_H
#include "mcu.h"

#define I2C_ADDR     0x98

#define led_1 MCU__GPIO_PIN_7
#define led_2 MCU__GPIO_PIN_12
#define led_3 MCU__GPIO_PIN_9
#define led_4 MCU__GPIO_PIN_17
#define led_5 MCU__GPIO_PIN_8

#define user_bt MCU__GPIO_PIN_8
#define isp_bt  MCU__GPIO_PIN_12

#define UART0_SETTINGS UART_BRD | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FRAME_3_5 | UART0_TX | UART0_RX
#define UART_BRD UART__BR_9600
#define UART0_RX RX__GPIO_PIN_10
#define UART0_TX TX__GPIO_PIN_16


//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP








#endif