/***************************************************************************//**
 * @file usb_example.c.
 * @brief  ������ ������ � USB ����������� ���-������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/supervisor.h"
#include "DRV/gpio.h"
#include "DRV/uart.h"

static struct {
    uint8_t uart_rx[100], uart_tx[100];
    sw_timer__t timer;
    uart__sub_t uart;
    int answer_err;
}usb_example__data;

static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void test_gpio_cb(u8 id, u8 gpio, gpio__state_e pin_state);
static void uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void uart_answer_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data);

void main() {
#ifdef _DEBUG_
    DBGMCU->CR = DBGMCU_CR_DBG_STOP;
    DBGMCU->APB1FZ = 0xFFFFFFFF;
    DBGMCU->APB2FZ = 0xFFFFFFFF;
#endif
    supervisor__init();
    sw_timer__init(NULL);
    uart__init();

    gpio__init(0, LED_BLUE, GPIO__DIR_OUT, NULL);
    gpio__init(0, LED_GREEN, GPIO__DIR_OUT, NULL);
    gpio__init(0, USER_BT, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | GPIO__INT_EDGE_FALING, test_gpio_cb);

    uart__open(&usb_example__data.uart, USB_UART_1, UART_SETTINGS, uart_event_hd, NULL);

    sw_timer__start(&usb_example__data.timer, 500, timer_cb, NULL);      // ��������� ������� �� 500��

    while(1) {
        framework__cout();

    }
}


static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, timer_cb, NULL);          // ������������� ������ �� �� �� ����� (-1)
    gpio__set(0, LED_BLUE, GPIO__STATE_TOGGLE);             // ������ �����������
}

static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
    switch(event) {
        case EVENT__OPEN:
            uart__rx(sub, UART__FRAME_3_5, usb_example__data.uart_rx, sizeof(usb_example__data.uart_rx), 0, uart_cb, NULL);   // ������ �� ����������� �����...
            break;
        case EVENT__CLOSE:
            uart__open(sub, sub->uart_id, UART_SETTINGS, uart_event_hd, NULL);  // ���������� �������� ������� "���������������" ����...
            break;
    default:
        break;
    }
}


static void uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    memcpy(usb_example__data.uart_tx, usb_example__data.uart_rx, len);
    uart__tx(&usb_example__data.uart, usb_example__data.uart_tx, len, uart_answer_cb, (void*)1);
    uart__rx(sub, UART__FRAME_3_5, usb_example__data.uart_rx,  sizeof(usb_example__data.uart_rx), 0, uart_cb, NULL);
}

static void uart_answer_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {

}


static void test_gpio_cb(u8 id, u8 gpio, gpio__state_e pin_state) { // ������� ������ USER_BT - �� ����������!!!!


}

i32 uart__cmd_handler(i32 uart_id, uart__cmd_e cmd) {
    return 0;
}