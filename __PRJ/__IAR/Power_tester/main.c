
#include <stddef.h>
#include "DRV/gpio.h"
#include "target.h"
#include "System/sw_timer.h"
//#include "adc.h"
#include "System/supervisor.h"
#include "DRV/i2c.h"
#include "DRV/hw_uart.h"
#include "DRV/uart.h"
#include "pwm.h"

typedef struct {
    u8 reg;
    u8 hi_byte;
    u8 lo_byte;
}ads1115_pkt_t;

typedef struct {
    uint8_t start        :1;
    uint8_t work         :1;
    uint8_t stop         :1;
    uint8_t current_set;
    u8 fun_percent, fun_current;
    uint16_t volts[3];
    uint16_t volt_12, volt_3_3, current;
    sw_timer__t fun_tmr;
}tester_t;

static tester_t tester_data;
static uint8_t i2c_state, i2c_idx;
static uint8_t i2c_buff[6];
static ads1115_pkt_t ads1115_cfg_tbl[] = {{0x01, 0x88, 0x83}, {0x01, 0xE5, 0x83}, {0x01, 0xF5, 0x83}};
static uint8_t uart_buff[UART_BUFF];
static sw_timer__t timer_i2c;

static void timer_i2c_cb(struct sw_timer__t *timer, void *ext_data);
static void i2c_start();
static void i2c__cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);
static void uart_cb(int uart_id, events__e event, uint8_t *buff, int len);
static void power_periph_start();
static void fun_tmr__cb(struct sw_timer__t *timer, void *ext_data);

void main() {
    gpio__init(0, PS_ON, GPIO__DIR_OUT, NULL);
    gpio__set(0, PS_ON, GPIO__STATE_LOW);
    gpio__init(0, FUN_PWM_PIN, GPIO__DIR_OUT, NULL);
    gpio__set(0, FUN_PWM_PIN, GPIO__STATE_HIGH);
    supervisor__init();
    sw_timer__init(NULL);
//    adc__init(ADC_VOLT);
    i2c__init(I2C_0, I2C_0_SETTINGS, NULL, i2c__cb);
//    gpio__init(user_bt, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
    hw_uart__init(UART_0, UART0_SETTINGS, NULL);
    hw_uart__rx(UART_0, uart_buff, UART_BUFF, uart_cb);

    tester_data.start = 1;

    while(1) {
        sw_timer__cout();
        hw_uart__cout();
        i2c__cout();
        wdt__reset();
//        supervisor__cout();
        power_periph_start();
   }
}


static void i2c_start() {
    i2c_state = i2c_idx = 0;
    sw_timer__start(&timer_i2c, 5, timer_i2c_cb, NULL);
}

static void power_periph_start() {
    if (tester_data.start) {
        if (!tester_data.work) {
        gpio__set(PS_ON, GPIO__STATE_HIGH);
        tester_data.work = 1;
        i2c_start();
        pwm__start(CH_0, PWM_PIN);
        sw_timer__start(&tester_data.fun_tmr, 0, fun_tmr__cb, NULL);
        }
        if (tester_data.volt_12 < 1600) // lower then 1V
            PWM0 = PWM_MAX;
        else if (PWM0 != PWM_MAX - tester_data.current_set * (PWM_MAX / 1000))
            PWM0 = PWM_MAX - tester_data.current_set * (PWM_MAX / 1000);
    }
    else if (tester_data.work) {
        tester_data.work = 0;
        gpio__set(FUN_PWM_PIN, GPIO__STATE_HIGH);
        sw_timer__stop(&timer_i2c);
        sw_timer__stop(&tester_data.fun_tmr);
        pwm__stop(CH_0);
        gpio__set(PS_ON, GPIO__STATE_LOW);
    }
}

static void i2c__cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
  static u8 data_addr;
    if (event != EVENT__OK) {
        i2c_state = i2c_idx = 0;
        i2c__deinit();
        i2c__init(I2C_0, I2C_0_SETTINGS, NULL, i2c__cb);
    }
    if ((!tester_data.work) || (len != 0))
        return;

    switch (i2c_state) {
        case 0:
            i2c__tx(I2C_0, 0x90, (uint8_t*)&ads1115_cfg_tbl[i2c_idx], 3, i2c__cb, NULL);
            break;
        case 1:
            i2c__tx(I2C_0, 0x90, (uint8_t*)&ads1115_cfg_tbl[i2c_idx], 1, i2c__cb, NULL);
            break;
        case 2:
            i2c__rx(I2C_0, 0x90, i2c_buff, 2, i2c__cb, NULL);
            break;
        case 3:
            if (((i2c_buff[0] | 0x80) != ads1115_cfg_tbl[i2c_idx].hi_byte) || (i2c_buff[1] != ads1115_cfg_tbl[i2c_idx].lo_byte)) {
                i2c_state = 0;
                return;
            }
            i2c__tx(I2C_0, 0x90, &data_addr, 1, i2c__cb, NULL);
            break;
        case 4:
            i2c__rx(I2C_0, 0x90, i2c_buff, 2, i2c__cb, NULL);
            break;
        case 5:
            i2c_buff[3] = i2c_buff[0];
            i2c_buff[2] = i2c_buff[1];
            tester_data.volts[i2c_idx] = *(uint16_t*)&i2c_buff[2];
            if (tester_data.volts[i2c_idx] > 32767)
                tester_data.volts[i2c_idx] = 0;
            if (++i2c_idx > 2) {
                i2c_idx = 0;
                tester_data.current = tester_data.volts[0] / 32;
                tester_data.volt_3_3 = tester_data.volts[1];
                tester_data.volt_12 = tester_data.volts[2];
            }
            i2c_state = 0;
            return;
    }
    i2c_state++;
}

static void timer_i2c_cb(struct sw_timer__t *timer, void *ext_data) {
    if (!tester_data.work)
        return;
    sw_timer__start(timer, -1, timer_i2c_cb, NULL);
    i2c__cb(I2C_0, EVENT__OK, 0x90, i2c_buff, 0, NULL);
}

static void uart_cb(int uart_id, events__e event, uint8_t *buff, int len) {
    uint8_t command = uart_buff[0], answer = 0x29;
    if (event != EVENT__OK) {
        hw_uart__deinit(UART_0);
        hw_uart__init(UART_0, UART0_SETTINGS, NULL);
        hw_uart__rx(UART_0, uart_buff, UART_BUFF, uart_cb);
        return;
    }
    switch(command) {
    case 0x20:
    case 0x21:
    case 0x22:
    case 0x23:
    case 0x24:
    case 0x29:
        hw_uart__rx(UART_0, uart_buff, UART_BUFF, uart_cb);
        return;
    case 0x70:
        tester_data.start = 1;
        answer = 0x20;
        tester_data.current_set = uart_buff[1];
        uart_buff[1] = tester_data.current_set;
        break;
    case 0x71:
        tester_data.start = 0;
        answer = 0x21;
        break;
    case 0x72:
        answer = 0x22;
        tester_data.current_set = uart_buff[1];
        break;
    case 0x73:
        if (tester_data.work)
            answer = 0x23;
        else
            answer = 0x21;
        tester_data.fun_percent = uart_buff[1] > 100 ? 100 : uart_buff[1];
        uart_buff[1] = tester_data.volt_12;
        uart_buff[2] = tester_data.volt_12 >> 8;
        uart_buff[3] = tester_data.current;
        uart_buff[4] = tester_data.current >> 8;
        uart_buff[5] = 0x19;
        uart_buff[6] = 0x00;
        uart_buff[7] = tester_data.fun_current;
        break;
    case 0x74:
        answer = 0x24;
        tester_data.fun_percent = uart_buff[1] > 100 ? 100 : uart_buff[1];
        break;
    }
    uart_buff[0] = answer;
    hw_uart__tx(UART_0, uart_buff, UART_BUFF, uart_cb);
}

static void fun_tmr__cb(struct sw_timer__t *timer, void *ext_data) {
    u8 timeout = 20 - timer->timeout_ms;
    if (ext_data) {
        if (timeout)
            gpio__set(0, FUN_PWM_PIN, GPIO__STATE_HIGH);
        ext_data = NULL;
    }
    else {
        timeout = tester_data.current > 160 ? 20 : tester_data.current / 8;
        if (tester_data.fun_percent > (timeout * 5))
            timeout = tester_data.fun_percent / 5;
        if (timeout)
            gpio__set(0, FUN_PWM_PIN, GPIO__STATE_LOW);
        tester_data.fun_current = timeout * 5;
        ext_data = (void*)1;
    }
    sw_timer__start(&tester_data.fun_tmr, timeout, fun_tmr__cb, ext_data);
}