/***************************************************************************//**
 * @file config.h.
 * @brief  Конфигурация целевого приложения.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CONFIG_H
#define CONFIG_H

// Конфигурация загрузчика
#define BOOT_ADDR  0x08000000                // Адрес reset вектора
#define BOOT_SIZE  0x2000                    // Размер загрузчика
#define APP_SIZE   0x10000                   // Размер приложения
#define APP_ADDR   (BOOT_ADDR + BOOT_SIZE)   // Адрес приложения
#define IMG_ADDR   0x08012000                // Адрес резервной прошивки (загрузчик + приложение)
#define HDR_OFFSET ( BOOT_SIZE + 0x800)      // адрес структуры хэдера загрузчика. Соответствует секции линкера ".boot_header" в main.icf



#endif
