/***************************************************************************//**
 * @file target.h.
 * @brief  Конфигурация целевого приложения.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TARGET_H
#define TARGET_H

#include "mcu.h"
#include "config.h"



// Конфигурация системы
#define WEAK_FUNC __weak          // Аттрибут "слабая" функция

// Конфигурация flash-памяти
#define FLASH_INT__COUNT  1
#define FLASH_INT__ID     0



#endif
