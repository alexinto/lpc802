/***************************************************************************//**
 * @file   main.c.
 * @brief  Модуль загрузчика STM32F4xx.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "mcu.h"
#include "System/supervisor.h"
#include "System/bootloader.h"
#include "DRV/flash.h"

#define RESET(a) ((void(*)())(*(uint32_t*)((uint32_t)(a) + 0x04)))()

#define BOOTLOADER__CHECK 0xDADA10AD

typedef struct {
    uint32_t command;
    uint32_t check_load;
}bootloader__boot_info_t;

__no_init bootloader__boot_info_t boot_info@".bootloader";


int main() {
    __disable_interrupt();
    supervisor__init();
    bootloader__init(FLASH_INT__ID, (u8*)BOOT_ADDR, HDR_OFFSET, flash__exec, NULL, NULL);
    // Проверяем целостность приложения и команду на обновление
    bootloader__hdr_t* boot_hdr = bootloader__get_hdr();
    if (bootloader__check(BOOTLOADER__FW_TYPE_APP) != EVENT__OK) {
        if (bootloader__check(BOOTLOADER__FW_TYPE_UPD) == EVENT__OK) {
            bootloader__app_upd();
            supervisor__apl_reset();
            while(1){}
        }
    }
    else if (boot_info.check_load == BOOTLOADER__CHECK) {
        switch(boot_info.command) {
            case BOOTLOADER_UPD_APL:
                if (bootloader__check(BOOTLOADER__FW_TYPE_UPD) == EVENT__OK) {
                    bootloader__app_upd();
                    boot_info.command = BOOTLOADER_CMD_NONE;
                    supervisor__apl_reset();
                    while(1){}
                }
                break;
            default:
                break;
        }
    }

    SCB->VTOR = APP_ADDR;
    __set_MSP(*(uint32_t*)APP_ADDR);
    RESET(APP_ADDR);
    return 0;
}
