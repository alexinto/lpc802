﻿/***************************************************************************//**
 * @file hw_pwm.c.
 * @brief Модуль управления частотой и скважностью PWM.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "mcu.h"
#include "System/fstandard.h"
#include "hw_pwm.h"
#include "DRV/gpio.h"

typedef struct {
    TIM_TypeDef* tim;
    u32 out_pin, out_ch, out_ch_en;
    u8 af_function, tim_en_pos;
    IRQn_Type irq;
}tim_descr_t;

typedef struct {
    hw_pwm__state_e state;
    u8 step;
    u32 freq, freq_comp;
    hw_pwm_cb_t cb;
    void* ext_data;
}hw_pwm__struct_t;

static const tim_descr_t hw_pwm_tbl[] = { TIM2, MCU__GPIO_P_A_1, 0x06 << TIM_CCMR1_OC2M_Pos, TIM_CCER_CC2E, 1, 0, TIM2_IRQn,                      //
                                          TIM3, MCU__GPIO_P_C_7, 0x06 << TIM_CCMR1_OC2M_Pos, TIM_CCER_CC2E, 2, 1, TIM3_IRQn,                      //
                                          TIM4, MCU__GPIO_P_B_6, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E, 2, 2, TIM4_IRQn,                      //+
                                          TIM5, MCU__GPIO_P_A_0, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E, 2, 3, TIM5_IRQn,                      //+
                                          TIM12, MCU__GPIO_P_H_6, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E, 9, 6, TIM8_BRK_TIM12_IRQn,           //+
                                          TIM13, MCU__GPIO_P_A_6, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E, 9, 7, TIM8_UP_TIM13_IRQn,            //+
                                          TIM14, MCU__GPIO_P_A_7, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E, 9, 8, TIM8_TRG_COM_TIM14_IRQn,       //+
                                          TIM9, MCU__GPIO_P_A_2, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E, 3, 16, TIM1_BRK_TIM9_IRQn,            //+
                                          TIM10, MCU__GPIO_P_B_8, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E, 3, 17, TIM1_BRK_TIM9_IRQn, // не опечатка - это системный таймер, он всегда включен!!!+
                                          TIM11, MCU__GPIO_P_B_9, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E, 3, 18, TIM1_TRG_COM_TIM11_IRQn};     //+

#define PWM_CNT (sizeof(hw_pwm_tbl) / sizeof(hw_pwm_tbl[0]))

static hw_pwm__struct_t hw_pwm__data[PWM_CNT];

void hw_pwm__ctrl(hw_pwm_id_e pwm, u32 freq, u8 percent) {
    TIM_TypeDef* pwm_ptr = hw_pwm_tbl[pwm].tim;
    tim_descr_t* pwm_descr_ptr = (tim_descr_t*)&hw_pwm_tbl[pwm];
    hw_pwm__struct_t* pwm_data_ptr = &hw_pwm__data[pwm];
    u32 timer_on = 0, temp_psc = 480000, temp_arr = 99;
    u32* rcc_apb = pwm_descr_ptr->tim_en_pos < 16 ? (u32*)&RCC->APB1ENR : (u32*)&RCC->APB2ENR;
    if (freq) {
        timer_on = TIM_CR1_CEN;
        gpio__af_init(0, pwm_descr_ptr->out_pin, GPIO__MODE_PP, pwm_descr_ptr->af_function);

        *rcc_apb |= 1 << pwm_descr_ptr->tim_en_pos;
        if (freq < 10) {
            temp_psc /= 10;
            temp_arr = 999;
        }
        pwm_ptr->PSC = temp_psc/freq;
        pwm_ptr->ARR = temp_arr;
        pwm_ptr->CCMR1 = pwm_descr_ptr->out_ch;
        pwm_ptr->CCER = pwm_descr_ptr->out_ch_en;
        pwm_ptr->BDTR = TIM_BDTR_MOE;
        temp_arr++;
        pwm_ptr->CCR1 = temp_arr * percent / 100;
    }
    else
        pwm_ptr->CCR1 = 0;
    pwm_ptr->DIER = 0;
    pwm_data_ptr->freq = pwm_data_ptr->freq_comp = freq;
    if (pwm_data_ptr->state != PWM_IDLE)
        pwm_data_ptr->state =  PWM_OK;

    pwm_ptr->CR1 = timer_on;
}

hw_pwm__state_e hw_pwm__set(hw_pwm_id_e pwm, u32 freq, u8 step, hw_pwm_cb_t cb, void* ext_data) {
    hw_pwm__state_e res = PWM_BUSY;
    hw_pwm__struct_t* pwm_descr_ptr = &hw_pwm__data[pwm];
    tim_descr_t* pwm_ptr = (tim_descr_t*)&hw_pwm_tbl[pwm];
    if ((pwm_descr_ptr->state == PWM_IDLE) && (pwm_ptr->tim->CR1)) {
        pwm_descr_ptr->freq = freq;
        pwm_descr_ptr->step = step;
        pwm_descr_ptr->cb = cb;
        pwm_descr_ptr->ext_data = ext_data;
        pwm_descr_ptr->state = PWM_WORK;
        pwm_ptr->tim->SR = 0;
        pwm_ptr->tim->DIER = 1;
        NVIC_SetPriority(pwm_ptr->irq, 1);
        NVIC_EnableIRQ(pwm_ptr->irq);
        res = PWM_OK;
    }
    return res;
}


void hw_pwm__cout() {
    for(u8 i = 0; i < PWM_CNT; i++) {
        hw_pwm__struct_t* pwm_descr_ptr = &hw_pwm__data[i];
        if (pwm_descr_ptr->state == PWM_OK) {
            pwm_descr_ptr->state = PWM_IDLE;
            if (pwm_descr_ptr->cb)
                pwm_descr_ptr->cb((hw_pwm_id_e)i, pwm_descr_ptr->freq, pwm_descr_ptr->ext_data);
        }
    }
}

void hw_pwm__IRQ_handler(u8 pwm) {
    TIM_TypeDef* pwm_ptr = hw_pwm_tbl[pwm].tim;
    hw_pwm__struct_t* pwm_data_ptr = &hw_pwm__data[pwm];
    pwm_ptr->SR = 0;
    u32 arr = pwm_ptr->ARR;
    u32 freq = 48000000 / (arr * pwm_ptr->PSC);
    pwm_data_ptr->freq_comp += pwm_data_ptr->step;
    arr = 48000000 / (pwm_data_ptr->freq_comp * arr);
    if (freq >= pwm_data_ptr->freq) {
        pwm_ptr->DIER = 0;
        pwm_data_ptr->state = PWM_OK;
    }
    else {
        pwm_ptr->PSC = arr;
    }
}
