/***************************************************************************//**
 * @file analizator.c.
 * @brief  �������� ������ �����������.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stddef.h>
#include "DRV/gpio.h"
#include "target.h"
#include "System/sw_timer.h"
#include "System/supervisor.h"
#include "DRV/uart.h"
#include "stdio.h"
#include "string.h"
#include "System/framework.h"
#include "DRV/KBD/key_code.h"

typedef enum {
    KEY__IDLE  = 0,
    KEY__TX_NULL,
    KEY__TX,
    KEY__ANSWER,
}key__state_e;

typedef struct {
    uart__sub_t sub;
    uint8_t tx_err, done;
    uint8_t buff[100];
}keybrd__struct_t;

static struct {
    uint8_t uart_rx[100], uart_tx[100];
    sw_timer__t timer;
    uart__sub_t uart;
    int answer_err;
    key__state_e state;
    keybrd__struct_t keyb[2];
}analizator;

uint8_t key_buff[8] = {0x00, 0x00, key_q, key_w, key_e, key_i, key_u, key_y},  key_buff2[8] = {0x00, 0x00, key_d, key_F8, key_F9, 0x00, 0x00, 0x00};
static uint8_t key_null[8];
static int key_state, key_tx;
static int counter;


static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void test_gpio_cb(u8 id, u8 gpio, gpio__state_e pin_state);
static void uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data);

void main() {
#ifdef _DEBUG_
    DBGMCU->CR = DBGMCU_CR_DBG_STOP;
    DBGMCU->APB1FZ = 0xFFFFFFFF;
    DBGMCU->APB2FZ = 0xFFFFFFFF;
#endif
    supervisor__init();
    sw_timer__init(NULL);
    uart__init();

    gpio__init(0, LED_BLUE, GPIO__DIR_OUT, NULL);
    gpio__init(0, LED_GREEN, GPIO__DIR_OUT, NULL);
    gpio__init(0, USER_BT, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | GPIO__INT_EDGE_FALING, test_gpio_cb);

    uart__open(&analizator.uart, USB_UART_1, UART_SETTINGS, uart_event_hd, NULL);
    uart__open(&analizator.keyb[0].sub, USB_UART_2, UART_SETTINGS, uart_event_hd, NULL);
    uart__open(&analizator.keyb[1].sub, USB_UART_3, UART_SETTINGS, uart_event_hd, NULL);

    sw_timer__start(&analizator.timer, 10, timer_cb, NULL);
    while(1) {
        framework__cout();

    }
}

static void uart_answer_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    if (ext_data)
        return;
    if (event != EVENT__OK)
        analizator.answer_err = 1;
    else {
        analizator.state = KEY__IDLE;
        analizator.keyb[0].done = analizator.keyb[1].done = 0;
        analizator.keyb[0].tx_err = analizator.keyb[1].tx_err = analizator.answer_err = 0;
    }
}


static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, timer_cb, NULL);
    if  ((!key_tx) && (memcmp(analizator.keyb[0].buff, key_null, sizeof(key_null)) ||  memcmp(analizator.keyb[1].buff, key_null, sizeof(key_null))))
        gpio__set(0, LED_BLUE, GPIO__STATE_HIGH);
    else
        gpio__set(0, LED_BLUE, GPIO__STATE_LOW);

    switch(analizator.state) {
        case KEY__TX_NULL:
            if ((analizator.keyb[0].tx_err) && (uart__tx(&analizator.keyb[0].sub, key_null, sizeof(key_null), uart_cb, NULL) == EVENT__OK))
                analizator.keyb[0].tx_err = 0;
            if ((analizator.keyb[1].tx_err) && (uart__tx(&analizator.keyb[1].sub, key_null, sizeof(key_null), uart_cb, NULL) == EVENT__OK))
                analizator.keyb[1].tx_err = 0;
            break;
        case KEY__TX:
            if ((analizator.keyb[0].tx_err) && (uart__tx(&analizator.keyb[0].sub, analizator.keyb[0].buff, sizeof(key_null), uart_cb, NULL) == EVENT__OK))
                analizator.keyb[0].tx_err = 0;
            if ((analizator.keyb[1].tx_err) && (uart__tx(&analizator.keyb[1].sub, analizator.keyb[0].buff, sizeof(key_null), uart_cb, NULL) == EVENT__OK))
                analizator.keyb[1].tx_err = 0;
            break;
        case KEY__ANSWER:
            if ((analizator.answer_err) && (uart__tx(&analizator.uart, analizator.uart_tx, sizeof(key_null) + 6, uart_answer_cb, NULL) == EVENT__OK))
                analizator.answer_err = 0;
            break;
        default:
            break;
    }

    if (counter)
        counter--;
    if (key_state) {
        if (!key_tx) {
            gpio__set(0, LED_GREEN, GPIO__STATE_HIGH);
            memcpy(analizator.keyb[0].buff, key_buff, sizeof(key_buff));
            memcpy(analizator.keyb[1].buff, key_buff2, sizeof(key_buff));
            analizator.state = KEY__TX_NULL;
            analizator.keyb[0].done = analizator.keyb[1].done = 0;
            analizator.keyb[0].tx_err = analizator.keyb[1].tx_err = analizator.answer_err = 0;
            if (uart__tx(&analizator.keyb[0].sub, key_null, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
                analizator.keyb[0].tx_err = 1;
            if (uart__tx(&analizator.keyb[1].sub, key_null, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
                analizator.keyb[1].tx_err = 1;
            key_tx = 1;
            counter = 50;
        }
    }
    else if (key_tx) {
        gpio__set(0, LED_GREEN, GPIO__STATE_LOW);
        memcpy(analizator.keyb[0].buff, analizator.uart_rx, sizeof(key_buff));
        analizator.keyb[1].buff[0] = analizator.uart_rx[0];
        memcpy(&analizator.keyb[1].buff[2], &analizator.uart_rx[8], 6);
        analizator.state = KEY__TX_NULL;
        analizator.keyb[0].done = analizator.keyb[1].done = 0;
        analizator.keyb[0].tx_err = analizator.keyb[1].tx_err = analizator.answer_err = 0;
        if (uart__tx(&analizator.keyb[0].sub, key_null, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
            analizator.keyb[0].tx_err = 1;
        if (uart__tx(&analizator.keyb[1].sub, key_null, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
            analizator.keyb[1].tx_err = 1;
        key_tx = 0;
        counter = 50;
    }
}

static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
    switch(event) {
        case EVENT__OPEN:
            if (sub == &analizator.uart)
                uart__rx(sub, UART__FRAME_COUNT, analizator.uart_rx, sizeof(key_null) + 6, 0, uart_cb, NULL);
            break;
        case EVENT__CLOSE:
            uart__open(sub, sub->uart_id, UART_SETTINGS, uart_event_hd, NULL);
            break;
    default:
        break;
    }
}


static void uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    if (sub == &analizator.uart) {
        if (len == sizeof(key_null) + 6) {
            memcpy(analizator.uart_tx, analizator.uart_rx, len);
            if ((!key_tx) && (analizator.state == KEY__IDLE)) {
                analizator.state = KEY__TX_NULL;
                analizator.keyb[0].done = analizator.keyb[1].done = 0;
                analizator.keyb[0].tx_err = analizator.keyb[1].tx_err = analizator.answer_err = 0;
                memcpy(analizator.keyb[0].buff, analizator.uart_rx, sizeof(key_buff));
                analizator.keyb[1].buff[0] = analizator.uart_rx[0];
                memcpy(&analizator.keyb[1].buff[2], &analizator.uart_rx[8], 6);
                if (uart__tx(&analizator.keyb[0].sub, key_null, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
                    analizator.keyb[0].tx_err = 1;
                if (uart__tx(&analizator.keyb[1].sub, key_null, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
                    analizator.keyb[1].tx_err = 1;
            }
            else {
                uart__tx(&analizator.uart, analizator.uart_tx, len, uart_answer_cb, (void*)1);
            }
        }
        uart__rx(sub, UART__FRAME_COUNT, analizator.uart_rx, sizeof(key_null) + 6, 0, uart_cb, NULL);
    }
    else if (sub == &analizator.keyb[0].sub) {
        if (event != EVENT__OK) {
            analizator.keyb[0].tx_err = 1;
        }
        else
            analizator.keyb[0].done = 1;
    }
    else if (sub == &analizator.keyb[1].sub) {
        if (event != EVENT__OK) {
            analizator.keyb[1].tx_err = 1;
        }
        else
            analizator.keyb[1].done = 1;
    }
    if ((analizator.keyb[0].done == 1) && (analizator.keyb[1].done == 1)) {
        analizator.keyb[0].done = analizator.keyb[1].done = 0;
        analizator.keyb[0].tx_err = analizator.keyb[1].tx_err = analizator.answer_err = 0;
        analizator.state++;
        switch(analizator.state) {
        case KEY__TX_NULL:
            if (uart__tx(&analizator.keyb[0].sub, key_null, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
                analizator.keyb[0].tx_err = 1;
            if (uart__tx(&analizator.keyb[1].sub, key_null, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
                analizator.keyb[1].tx_err = 1;
            break;
        case KEY__TX:
            if (uart__tx(&analizator.keyb[0].sub, analizator.keyb[0].buff, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
                analizator.keyb[0].tx_err = 1;
            if (uart__tx(&analizator.keyb[1].sub, analizator.keyb[1].buff, sizeof(key_null), uart_cb, NULL) != EVENT__OK)
                analizator.keyb[1].tx_err = 1;
            break;
        case KEY__ANSWER:
            if (uart__tx(&analizator.uart, analizator.uart_tx, sizeof(key_null) + 6, uart_answer_cb, NULL) != EVENT__OK)
                analizator.answer_err = 1;
            break;
        default:
            analizator.state = KEY__IDLE;
            break;
        }
    }
}

static void test_gpio_cb(u8 id, u8 gpio, gpio__state_e pin_state) {
    if ((pin_state == GPIO__STATE_HIGH) && (!counter)) {
        if (key_tx) {
            key_state = 0;
        }
        else
            key_state = 1;
    }
}