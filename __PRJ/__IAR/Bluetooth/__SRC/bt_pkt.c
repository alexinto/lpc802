/***************************************************************************//**
 * @file bt_pkt.c.
 * @brief ���� � ��������� ��������� ������ bluetooth.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "bt_pkt.h"
#include "crc.h"

typedef bt_events_e (*bt_cmd)(uint8_t* data);

static bt_events_e bt_end(uint8_t* data);
static bt_events_e bt_read(uint8_t* data);
static bt_events_e bt_write(uint8_t* data);
static bt_events_e bt_break(uint8_t* data);
static bt_events_e bt_continue(uint8_t* data);
static void bt_pkt__answer(bt_cmd_e cmd, uint8_t* data);
static void bt_pkt__transport_cb(bt_events_e event, uint8_t* buff, uint8_t len);

typedef enum {
    BT_STATE_BUSY      =  0,
    BT_STATE_READY     =  1,
}bt_state_e;

typedef struct {
    uint8_t* buffer;
    bt_state_e state;
    bt_cmd_e oper;
    uint8_t crc, last_crc;
    uint16_t len, param, len_pkt;
    bt_pkt__cb_t cb;
    uint8_t answer[3];
    uint8_t pkt[20];
}bt_pkt__t;

static bt_pkt__t bt_pkt_data;

static const bt_cmd bt_commands[] = {bt_end, bt_read, bt_write, bt_break, bt_continue};

bt_events_e bt_pkt__oper(uint8_t* data, bt_pkt__cb_t bt_cb) {
    bt_events_e res = BT_EVENT_FAIL_PKT;
    bt_pkt_data.buffer = data;
    bt_pkt_data.cb = bt_cb;
    if ((data[PKT_LEN] >= 2) && (crc__8_ccitt(&data[PKT_LEN], data[PKT_LEN]) == data[PKT_CRC])) {
        if ((res = bt_commands[(bt_cmd_e)data[PKT_CMD]](data)) != BT_EVENT_OK) {
            bt_pkt_data.oper = BT_CMD_END;
            bt_pkt__answer(BT_CMD_BREAK, data);
        }
    }
    return res;
}

static bt_events_e bt_end(uint8_t* data) {
    if (data[1] > 2) {
        if (bt_continue(data) != BT_EVENT_OK)
            return BT_EVENT_ERROR;
    }
    if (bt_pkt_data.crc)
        return BT_EVENT_CRC_ERR;
    if (bt_pkt_data.oper == BT_CMD_WRITE)
        bt_pkt__oper_transport(BT_CMD_END, NULL, NULL, NULL, 0, NULL);
    bt_pkt_data.oper = BT_CMD_END;
    bt_pkt__answer(BT_CMD_END, bt_pkt_data.answer);
    bt_pkt__transport_cb(BT_EVENT_OK, bt_pkt_data.answer, bt_pkt_data.answer[PKT_LEN] + 1);
    return BT_EVENT_OK;
}

static bt_events_e bt_read(uint8_t* data) {
    uint8_t len;
    bt_events_e res = BT_EVENT_OK;
    if (data == &bt_pkt_data.pkt[PKT_DATA]) {
        if (!bt_pkt_data.len)
            bt_pkt_data.len = bt_pkt_data.pkt[PKT_DATA] + bt_pkt_data.pkt[PKT_DATA + 1] * 256 + 1;

        if (bt_pkt_data.len > (bt_pkt_data.len_pkt + 17)) {
            len = 17;
            bt_pkt_data.buffer[2] = BT_CMD_CONTINUE;
            bt_pkt_data.crc = crc__8_ccitt_thread(&bt_pkt_data.pkt[PKT_DATA], len, bt_pkt_data.crc);
        }
        else {
            len = bt_pkt_data.len - bt_pkt_data.len_pkt;
            bt_pkt_data.oper = BT_CMD_END;
            bt_pkt_data.buffer[2] = BT_CMD_END;
            if (bt_pkt_data.crc = crc__8_ccitt_thread(&bt_pkt_data.pkt[PKT_DATA], len, bt_pkt_data.crc))
                res = BT_EVENT_CRC_ERR;
        }
        memcpy(&bt_pkt_data.buffer[PKT_DATA], &bt_pkt_data.pkt[PKT_DATA], len);
        bt_pkt_data.len_pkt += len;
        bt_pkt_data.buffer[1] = 2 + len;
        bt_pkt_data.buffer[0] = crc__8_ccitt(&bt_pkt_data.buffer[1], 2 + len);

    }
    else if (bt_pkt_data.buffer[PKT_CMD] == BT_CMD_READ) {
        bt_pkt_data.oper = BT_CMD_READ;
        bt_pkt_data.len = 0;
        bt_pkt_data.len_pkt = 0;
        bt_pkt_data.param = data[PKT_PARAM] + data[PKT_PARAM + 1] * 256;
        bt_pkt_data.crc = CRC__8_THREAD_CCITT;

        res = bt_pkt__oper_transport(BT_CMD_READ, bt_pkt_data.param, bt_pkt_data.len_pkt, &bt_pkt_data.pkt[PKT_DATA] , 17, bt_pkt__transport_cb);
        return res;
    }
    else
        res = bt_pkt__oper_transport(BT_CMD_READ, bt_pkt_data.param, bt_pkt_data.len_pkt, &bt_pkt_data.pkt[PKT_DATA] , 17, bt_pkt__transport_cb);

    return res;
}

static bt_events_e bt_write(uint8_t* data) {
    int len = data[PKT_LEN] - 2;
    if (data[PKT_CMD] == BT_CMD_WRITE) {
        bt_pkt_data.oper = BT_CMD_WRITE;
        bt_pkt_data.len = 0;
        bt_pkt_data.param = data[PKT_PARAM] + data[PKT_PARAM + 1] * 256;
        bt_pkt_data.crc = CRC__8_THREAD_CCITT;
    }
    if ((MAX_DATA_LEN - len) < bt_pkt_data.len)
        return BT_EVENT_ERROR;
    bt_pkt_data.crc = crc__8_ccitt_thread(&data[PKT_DATA], len, bt_pkt_data.crc);
    bt_pkt__answer(BT_CMD_CONTINUE, bt_pkt_data.answer);
    if (bt_pkt__oper_transport(BT_CMD_WRITE, bt_pkt_data.param, bt_pkt_data.len, &data[PKT_DATA] ,len, bt_pkt__transport_cb) != BT_EVENT_OK)
        return BT_EVENT_ERROR;
    bt_pkt_data.len += len;
    return BT_EVENT_OK;
}

static bt_events_e bt_break(uint8_t* data) {
    bt_pkt_data.oper = BT_CMD_END;
    return BT_EVENT_OK;
}

static bt_events_e bt_continue(uint8_t* data) {
    return (bt_pkt_data.oper == BT_CMD_END) ? BT_EVENT_ERROR : bt_commands[bt_pkt_data.oper](data);
}

static void bt_pkt__answer(bt_cmd_e cmd, uint8_t* data) {
    data[1] = 2;
    data[2] = cmd;
    data[0] = crc__8_ccitt(&data[PKT_LEN], 2);
}

uint16_t bt_pkt__data_build(uint16_t param, uint8_t* buff_target, uint8_t* buff_source, uint16_t len) {
    memcpy(buff_target + 4, buff_source, len);
	len += 4;
    *(uint16_t*)buff_target = len;
    *(uint16_t*)&buff_target[2] = param;
    buff_target[len] = crc__8_ccitt(buff_target, len);
	return len + 1;
}

static void bt_pkt__transport_cb(bt_events_e event, uint8_t* buff, uint8_t len) {
    if (event != BT_EVENT_OK)
        bt_pkt__answer(BT_CMD_BREAK, buff);
    else if (bt_pkt_data.oper == BT_CMD_READ) {
        bt_read(buff);
        buff = bt_pkt_data.buffer;
    }
    else
        buff = bt_pkt_data.answer;
    bt_pkt_data.cb(event, buff, buff[1] + 1);
}
