/***************************************************************************//**
 * @file hw_bt_pkt.c.
 * @brief ���� � ��������� ������������� ������ ��������� bluetooth.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "bt_pkt.h"
#include "bluetooth.h"
#include "hw_bt_pkt.h"
#include "flash.h"

#define FLASH_SIZE 256
__no_init static uint8_t flash_buffer[FLASH_SIZE];

struct {
    uint8_t flag_cb;
    bt_pkt__cb_t cb;
    uint8_t* buff;
    uint32_t len;
}hw_bt_pkt_data;

bt_events_e bt_pkt__oper_transport(bt_cmd_e cmd, uint16_t param, uint32_t addr_from, uint8_t* buff, uint32_t len, bt_pkt__cb_t cb) {
    bt_events_e res = BT_EVENT_OK;
    uint8_t temp;
    if (addr_from + len > FLASH_SIZE)
        return BT_EVENT_ERROR;
    hw_bt_pkt_data.cb = cb;
    hw_bt_pkt_data.buff = buff;
    hw_bt_pkt_data.len = len;
    hw_bt_pkt_data.flag_cb = 0;
    switch(cmd) {
    case BT_CMD_END:
        flash_write(flash_buffer, FLASH_SIZE);
        break;
    case BT_CMD_WRITE:
        memcpy(flash_buffer + addr_from, buff, len);
        hw_bt_pkt_data.cb(BT_EVENT_OK, hw_bt_pkt_data.buff, hw_bt_pkt_data.len);
        break;
    case BT_CMD_READ:
        if (param == 200) {
            temp = get_percent();
            bt_pkt__data_build(param, buff, &temp, 1);
        }
        else {
            flash_read(flash_buffer, FLASH_SIZE);
            memcpy(buff, flash_buffer + addr_from, len);
        }
        hw_bt_pkt_data.cb(BT_EVENT_OK, hw_bt_pkt_data.buff, hw_bt_pkt_data.len);
        break;
    default:
        res = BT_EVENT_NO_CMD;
        break;
    }
    return res;
}

void hw_bt_pkt__cout() {
    if(hw_bt_pkt_data.flag_cb) {
        hw_bt_pkt_data.flag_cb = 0;
        hw_bt_pkt_data.cb(BT_EVENT_OK, hw_bt_pkt_data.buff, hw_bt_pkt_data.len);
    }
}
