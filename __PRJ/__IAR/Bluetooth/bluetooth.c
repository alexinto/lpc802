#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "gpio.h"
#include "target.h"
#include "System/sw_timer.h"
#include "System/framework.h"
#include "adc.h"
#include "supervisor.h"
#include "LPC802/hw_uart.h"
//#include "DRV/uart.h"
#include "crc.h"
#include "bt_pkt.h"
#include "hw_bt_pkt.h"
#include "bluetooth.h"

typedef enum {
    NORMAL = 0,
    SUSPEND,
    SLEEP,
    WAKE,
    WAITING,
}fsm_state__e;

typedef enum {
    FSM_WAIT = 0,
    FSM_COMPLETE,
    FSM_ERROR
}fsm_sub_state__e;

typedef struct {
    uint8_t ready;
    fsm_state__e state;
    sw_timer__t tmr, led_tmr;
}fsm_pwr__t;

typedef struct {
    uint16_t adc, adc_mv;
    uint8_t percent;
    fsm_sub_state__e state;
}fsm_batt__t;

typedef struct{
    fsm_pwr__t pwr;
    fsm_batt__t batt;
}fsm_state__t;

typedef struct {
    uint8_t wake;
    uint8_t active;
    uint8_t uart_buff[20];
}bluetooth__data_t;

static bluetooth__data_t bt_data;
static fsm_state__t fsm = {.pwr.state = WAKE, .pwr.ready = 0, .batt.percent = 50, .batt.adc_mv = 3700};

static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void adc_cb(adc__event_e event);
static void uart_tx_cb(int uart_id, events__e event, uint8_t *buff, int len);
static void uart_rx_cb(int uart_id, events__e event, uint8_t *buff, int len);
static void bt_uart__reinit(void);
static void bluetooth_handler(bt_events_e event, uint8_t* buff, uint8_t len);
static void main__cout();
static void fsm_pwr();

void main() {
    supervisor__init();
    sw_timer__init(NULL);

    gpio__init(PWR_CNTRL, GPIO__DIR_OUT, NULL);
    gpio__set(PWR_CNTRL, GPIO__STATE_LOW);
    gpio__init(PWR_LED, GPIO__DIR_OUT, NULL);
    gpio__init(BT_EN, GPIO__DIR_OUT, NULL);
    gpio__init(BT_DROP, GPIO__DIR_OUT, NULL);
    gpio__init(BT_STATUS, GPIO__DIR_IN, NULL);
    gpio__set(PWR_LED, GPIO__STATE_LOW);
    gpio__set(BT_EN, GPIO__STATE_LOW);
    gpio__set(BT_DROP, GPIO__STATE_LOW);

    adc__init(ADC_8);

    bt_data.wake = 1;

    while(1) {
        //wdt__reset();
        framework__cout();
        hw_uart__cout();
        adc__cout();
        hw_bt_pkt__cout();
        main__cout();
    }
}

static void main__cout() {
    fsm_pwr();
}

static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    int timeout = -1;
    fsm.pwr.ready = 1;
    if (timer == &fsm.pwr.led_tmr)
        gpio__set(PWR_LED, GPIO__STATE_TOGGLE);
    else
        (bt_data.active) ? (bt_data.active = 0, timeout = SLEEP_TIMEOUT >> 2) : (bt_data.wake = 0);
    sw_timer__start(timer, timeout, timer_cb, NULL);
}

static void uart_rx_cb(int uart_id, events__e event, uint8_t *buff, int len) {
    bt_events_e res;
    bt_data.active = 1;
    fsm.pwr.led_tmr.timeout_ms = 500;

    if ((event != EVENT__OK) && (event != EVENT__TIMEOUT))
        bt_uart__reinit();
    else {
        res = bt_pkt__oper(bt_data.uart_buff, bluetooth_handler);
        if (res == BT_EVENT_FAIL_PKT)
            hw_uart__rx(UART_0, bt_data.uart_buff, 20, uart_rx_cb);
        else if (res != BT_EVENT_OK)
            hw_uart__tx(UART_0, bt_data.uart_buff, BT_LEN_PKT_ANSWER, uart_tx_cb);
    }
}

static void uart_tx_cb(int uart_id, events__e event, uint8_t *buff, int len) {
    if ((event != EVENT__OK) && (event != EVENT__TIMEOUT))
        bt_uart__reinit();
    else
        hw_uart__rx(UART_0, bt_data.uart_buff, 20, uart_rx_cb);
}

static void bluetooth_handler(bt_events_e event, uint8_t* buff, uint8_t len) {
    hw_uart__tx(UART_0, buff, len, uart_tx_cb);
}

static void bt_uart__reinit(void) {
    hw_uart__deinit(UART_0);
    hw_uart__init(UART_0, UART0_SETTINGS, NULL);
    hw_uart__rx(UART_0, bt_data.uart_buff, 20, uart_rx_cb);
}

//                                    4     8     12   16     20   24    28    32    36    40    44    48    52    56    60
const uint16_t percent_table[25] = {3400, 3650, 3697, 3717, 3738, 3748, 3768, 3778, 3789, 3799, 3809, 3819, 3830, 3840, 3870,
                                    3900, 3925, 3957, 3987, 4020, 4054, 4085, 4100, 4125, 4150};
//                                   64    68    72    76    80    84    88    92    96    100

uint8_t get_percent() {
    return fsm.batt.percent;
}

static void adc_cb(adc__event_e event) {
    static uint8_t adc_init_measure = 0;
    uint8_t temp;
    fsm.batt.state = FSM_COMPLETE;
    if (event != ADC__EVENT_OK) {
        adc__init(ADC_8);
        fsm.batt.adc_mv = 0xFFFF;
        fsm.batt.state = FSM_ERROR;
    }
    if (adc_init_measure++ > 5) {
        if (event == ADC__EVENT_OK) {
            fsm.batt.adc_mv = ((((uint32_t)3000 * fsm.batt.adc)) >> 11) + ADC_HYST; // ��������� �� ��������...
            for(temp = 0; (temp < 25) && (fsm.batt.adc_mv > percent_table[temp]); temp++);
            fsm.batt.percent =  temp << 2;
        }
        gpio__set(BT_EN, GPIO__STATE_HIGH);
    }
    else
        adc__start(&fsm.batt.adc, adc_cb);
}

static void fsm_pwr() {
    switch(fsm.pwr.state) {
    case NORMAL:
        if (!bt_data.wake)
            fsm.pwr.state = SUSPEND;
        break;
    case SUSPEND:
        sw_timer__stop(&fsm.pwr.tmr);
        sw_timer__stop(&fsm.pwr.led_tmr); //todo leds off
        hw_uart__deinit(UART_0);
        gpio__set(PWR_LED, GPIO__STATE_LOW);
        gpio__set(BT_EN, GPIO__STATE_LOW);
        gpio__set(PWR_CNTRL, GPIO__STATE_HIGH);
        fsm.pwr.state = SLEEP;
        break;
    case SLEEP:
            break;
    case WAKE:
    default:
        gpio__set(PWR_CNTRL, GPIO__STATE_LOW);
        gpio__set(PWR_LED, GPIO__STATE_HIGH);
        bt_uart__reinit();
        sw_timer__start(&fsm.pwr.led_tmr, 500, timer_cb, NULL);
        sw_timer__start(&fsm.pwr.tmr, SLEEP_TIMEOUT, timer_cb, NULL);
        fsm.pwr.state = WAITING;
        break;
    case WAITING:
        if (fsm.pwr.ready) {
            adc__start(&fsm.batt.adc, adc_cb);
            fsm.pwr.state = NORMAL;
        }
        break;
    }
}