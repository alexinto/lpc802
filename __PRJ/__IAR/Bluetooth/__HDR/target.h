#ifndef __TARGET_H
#define __TARGET_H
#include "mcu.h"

#define SLEEP_TIMEOUT 12000

#define ADC_HYST 20

#define PWR_LED     MCU__GPIO_PIN_0
#define PWR_CNTRL   MCU__GPIO_PIN_12
#define BT_EN       MCU__GPIO_PIN_14
#define BT_STATUS   MCU__GPIO_PIN_16
#define BT_DROP     MCU__GPIO_PIN_7

#define UART0_SETTINGS (UART_BRD | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FRAME_3_5 | UART0_TX | UART0_RX)
#define UART_BRD UART__BR_9600
#define UART0_RX RX__GPIO_PIN_13
#define UART0_TX TX__GPIO_PIN_17


//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif