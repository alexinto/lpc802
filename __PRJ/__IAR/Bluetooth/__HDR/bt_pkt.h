/***************************************************************************//**
 * @file bt_pkt.c.
 * @brief ���� � ��������� ��������� ������ bluetooth.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef _H_BT_PKT
#define _H_BT_PKT

#include <stdint.h>

#define BT_LEN_PKT_ANSWER  3

#define MAX_DATA_LEN       65535
#define PKT_MAX_DATA_LEN   17
#define PKT_CRC            0
#define PKT_LEN            1
#define PKT_CMD            2
#define PKT_DATA           3 
#define PKT_PARAM          5 

typedef enum {
    BT_EVENT_CRC_ERR    = -4,
    BT_EVENT_LEN_ERR    = -3,
    BT_EVENT_NO_CMD     = -2,
    BT_EVENT_ERROR      = -1,    
    BT_EVENT_OK         =  0,
    BT_EVENT_FAIL_PKT   =  1,
	BT_EVENT_OPER_END   =  2,
}bt_events_e;

typedef enum {
    BT_CMD_END      =  0,
    BT_CMD_READ     =  1, 
    BT_CMD_WRITE    =  2,
    BT_CMD_BREAK    =  3,
    BT_CMD_CONTINUE =  4,
}bt_cmd_e;

typedef void (*bt_pkt__cb_t)(bt_events_e event, uint8_t* buff, uint8_t len);

/***************************************************************************//**
 * @brief �������- ���������� ������� Bluetooth.
 * @param data - ��������� �� �������� ������. 
 * @param cb - ��������� �� ���������������� �������- ���������� ���������� ��������. 
 * @return BT_EVENT_OK - ��������� ������ �������.
 *         BT_EVENT_NO_CMD - ������� �� ����������.
 *         BT_EVENT_LEN_ERR - ������ ����� ������.
 *         BT_EVENT_ERROR - ������ ��������� ������.
 ******************************************************************************/
bt_events_e bt_pkt__oper(uint8_t* data, bt_pkt__cb_t bt_cb);

/***************************************************************************//**
 * @brief �������- ���������� ������ ������ bt_pkt.c.
 * @param cmd - ������� (BT_CMD_READ, BT_CMD_WRITE).
 * @param param - ����� ��������� ��� ������\������.
 * @param addr_from - ����� (� ������), �� �������� ��������� �������\ �������� ������.
 * @param buff - ��������� �� ����� (��� ������� ������ � ���� �������������� ������,
                 ��� ������� ������- ������ �� ������).
 * @param len - ����� ������.
 * @param cb - ��������� �� �������- ���������� ���������� �������� ��� ������ bt_pkt.c. 
 * @return BT_EVENT_OK - ��������� ������ �������.
 *         BT_EVENT_NO_CMD - ������� �� ����������.
 *         BT_EVENT_LEN_ERR - ������ ����� ������.
 *         BT_EVENT_ERROR - ������ ��������� ������.
 ******************************************************************************/
bt_events_e bt_pkt__oper_transport(bt_cmd_e cmd, uint16_t param, uint32_t addr_from, uint8_t* buff, uint32_t len, bt_pkt__cb_t cb);

/***************************************************************************//**
 * @brief ������� ��������������� ������ ������ ��� ������� Bluetooth. Buff_target ������ ���� �� 5+ ���� ������ ������ buff_source.
 * @param param - ����� ��������� ��� ������\������. 
 * @param buff_target - ��������� �� ������� �����, � ������� ������������ ����������������� ������. 
 * @param buff_source - ��������� �� ����� � ��������� �������. 
 * @param len - ����� ������ � ��������� ������� (sizeof(buff_target) >= sizeof(buff_source) + 5). 
 * @return ����� ������ buff_target..
 ******************************************************************************/
uint16_t bt_pkt__data_build(uint16_t param, uint8_t* buff_target, uint8_t* buff_source, uint16_t len);

/******************************************************************************
 *
 *
 ******************************************************************************/ 
bt_events_e bt_data_send(uint8_t* data);

bt_events_e bt_data_input(uint8_t* data);


bt_events_e bt_master__oper(bt_cmd_e cmd, uint16_t param, uint8_t* data, uint16_t len, bt_pkt__cb_t bt_cb);




















#endif
