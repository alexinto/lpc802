#include "target.h"
#include "gpio.h"
#include "supervisor.h"

static int timer_cnt;
static void cs_handler(int gpio, gpio__state_e pin_state);

void main() {
    supervisor__init();
//    gpio__init(USER_BUTTON, GPIO__DIR_IN | GPIO__PULL_UP | GPIO__INT_EDGE_RISING | GPIO__INTR_0, cs_handler);
    gpio__init(MCU__GPIO_PIN_12, GPIO__DIR_IN | GPIO__PULL_UP | GPIO__INT_EDGE_RISING | GPIO__INTR_1, cs_handler);
    gpio__init(TMR_O1, GPIO__DIR_OUT, NULL);
    gpio__set(TMR_O1, GPIO__STATE_HIGH);

    __enable_interrupt();

    while(1) {
    }
}


static void cs_handler(int gpio, gpio__state_e pin_state) {
    __disable_interrupt();
    gpio__set(TMR_O1, GPIO__STATE_LOW);
    for(int i = 3522; i; i--);  // 2047 mks
    gpio__set(TMR_O1, GPIO__STATE_HIGH);
    __enable_interrupt();
}




