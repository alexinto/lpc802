#ifndef __TARGET_H
#define __TARGET_H
#include "mcu.h"

#define SLEEP_TIMEOUT 12000

#define ADC_HYST 20

#define led_1 MCU__GPIO_PIN_7
#define led_2 MCU__GPIO_PIN_12
#define led_3 MCU__GPIO_PIN_9
#define led_4 MCU__GPIO_PIN_17
#define led_5 MCU__GPIO_PIN_8

#define USER_BUTTON MCU__GPIO_PIN_8
#define ISP_BUTTON  MCU__GPIO_PIN_12


#define PWR_CNTRL   MCU__GPIO_PIN_12
#define BT_EN       MCU__GPIO_PIN_14
#define BT_STATUS   MCU__GPIO_PIN_16
#define BT_DROP     MCU__GPIO_PIN_7

#define CS_IN       MCU__GPIO_PIN_15

#define TMR_O1      MCU__GPIO_PIN_17
#define INVERSE_PIN MCU__GPIO_PIN_11

//#define TMR_O1      MCU__GPIO_PIN_0


//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif