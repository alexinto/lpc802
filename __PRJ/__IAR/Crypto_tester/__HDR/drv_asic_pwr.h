﻿/***************************************************************************//**
 * @file drv_asic_pwr.h.
 * @brief Файл с функциями обработки команд модулей питания.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DRV_ASIC_PWR_H
#define DRV_ASIC_PWR_H

#include "System/events.h"

typedef enum {
    PWR_S9   = 0,
    PWR_NONE = 1,
}drv_asic_pwr__id_e;

typedef enum {
    PWR_ON_OFF         = 0, // Включение\отключение модуля
    PWR_CMD_NONE       = 1,
}drv_asic_pwr__cmd_e;

typedef void (*drv_asic_pwr__cmd_exec_cb)(int asic_id, events__e event, int param, void* ext_data);


events__e drv_asic_pwr__cmd_exec(drv_asic_pwr__id_e id, drv_asic_pwr__cmd_e cmd, int param, drv_asic_pwr__cmd_exec_cb cb, void* ext_data);


#endif
