/***************************************************************************//**
 * @file target.h.
 * @brief  Конфигурация целевого приложения.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TARGET_H
#define TARGET_H

#include "mcu.h"

#define SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_LOW_SPD | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB)
#define SPI__COUNT          2
#define TMC_5160_SPI    SPI_1

#define SPI_TERMO       SPI_2

#define I2C__COUNT          2
#define I2C_TERMO       I2C_1
#define I2C_ADDR         0xAA
#define I2C_PIC         I2C_2 // PB_10 = SCL, PB_11 = SDA

#define UART_BUFF_SIZE 200
#define HW_UART_COUNT 2
#define HW_UART_1 MCU__UART_1
#define HW_UART_1_TX MCU__GPIO_P_A_9
#define HW_UART_1_RX MCU__GPIO_P_A_10
#define HW_UART_2 MCU__UART_2
#define HW_UART_2_TX MCU__GPIO_P_A_2
#define HW_UART_2_RX MCU__GPIO_P_A_3
#define HW_UART_1_RTS -1
#define HW_UART_2_RTS -1
#define HW_UART_1_CTS -1
#define HW_UART_2_CTS -1

#define UART_SETTINGS (UART__BR_19200 | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FC_NONE)
#define USB_UART HW_UART_2
#define ASIC_UART HW_UART_1
#define UART_S9_SETTINGS (UART__BR_115200 | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FC_NONE)

#define USER_BT MCU__GPIO_P_C_13
#define LED_BLUE MCU__GPIO_P_B_7
#define LED_GREEN MCU__GPIO_P_B_0

#define BOARD_SELECTION_PIN MCU__GPIO_P_C_8
#define PROGRAM_VERSION (1)

//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif