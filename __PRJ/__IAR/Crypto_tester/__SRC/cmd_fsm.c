/***************************************************************************//**
 * @file cmd_fsm.c.
 * @brief ���� � ��������� ��������� ������ �����������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "string.h"
#include "target.h"
#include "cmd_fsm.h"
#include "drv_asic_pwr.h"
#include "drv_asic_check.h"

typedef cmd_fsm__error_e (*cmd_fsm__exec_t)(u8 id, u8 cmd, u16 param, void* ext_data);

typedef struct {
    u8 id_from, id_to;
    cmd_fsm__exec_t func;
}cmd_fsm__func_t;

typedef enum {
    BOARD_LEFT  = 0,
    BOARD_RIGHT = 1,
}cmd_fsm__board_ver_e;

static cmd_fsm__error_e event_to_fsm(events__e event);
static cmd_fsm__error_e cmd_fsm__kernel(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static cmd_fsm__error_e cmd_fsm__pwr_exec(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static void cmd_fsm__pwr_exec_cb(int id, events__e event, int param, void* ext_data);
static cmd_fsm__error_e cmd_fsm__asic_check_exec(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static void cmd_fsm__asic_check_exec_cb(int id, events__e event, int param, void* ext_data);

                                                //   ID to ID
static const cmd_fsm__func_t cmd_fsm__exec_tbl[] = {0x01, 0x0C, cmd_fsm__pwr_exec,                // 12 ������� �������
                                                    0x0D, 0x18, cmd_fsm__asic_check_exec,         // 12 ������� ������ �����
                                                    ID_KERNEL, ID_KERNEL, cmd_fsm__kernel,        // 0xFA �������� ����
                                                    0x00, 0x00, 0x00};

static struct {
    int msg_idx;
    cmd_fsm__state_e fsm_state;
    cmd_fsm__message_t msg[MSG_BUFF];
    cmd_fsm__ev_hd_t event_handler;
    void* ext_data;
}cmd_data;

/*******************************************************************************
 * ������� ������������� ������ CMD_FSM.
 ******************************************************************************/
void cmd_fsm__init(cmd_fsm__ev_hd_t event_handler) {
    cmd_data.event_handler = event_handler;
    gpio__init(BOARD_SELECTION_PIN, GPIO__DIR_IN, NULL);
}

/*******************************************************************************
 * ������� - ���������� ��������� ������ CMD_FSM.
 ******************************************************************************/
void cmd_fsm__cout(void) {
    cmd_fsm__message_t* cur_msg;
    switch(cmd_data.fsm_state) {
    case FSM_DEINIT:
        cmd_data.fsm_state = FSM_IDLE;
        break;
    case FSM_INIT:
        break;
    case FSM_IDLE:
        break;
    case FSM_WORK:
        cur_msg = &cmd_data.msg[cmd_data.msg_idx];
        if (cur_msg->id != 0) {
            cmd_fsm__func_t* msg_descr_idx = (cmd_fsm__func_t*)cmd_fsm__exec_tbl;
            while (msg_descr_idx->id_from != 0) {
                if ((cur_msg->id <= msg_descr_idx->id_to) && (cur_msg->id >= msg_descr_idx->id_from)) {
                    cur_msg->param = msg_descr_idx->func(cur_msg->id - msg_descr_idx->id_from, cur_msg->cmd, cur_msg->param, cur_msg);
                    break;
                }
                msg_descr_idx++;
            }
            if (msg_descr_idx->id_from == 0) {
                cur_msg->param = FSM_NOT_EXIST;
                cur_msg->cmd &= ~0x80;
            }
            cmd_data.msg_idx++;
        }
        else {
            for(cur_msg = cmd_data.msg; (!(cur_msg->cmd & 0x80)) && (cur_msg->id); cur_msg++);
            if (!cur_msg->id) {
                cmd_data.fsm_state = FSM_IDLE;
                cmd_data.event_handler(cmd_data.msg, cmd_data.ext_data);
            }
        }
    }
}

/*******************************************************************************
 * ������� - ��������� ������� �� ��������� ������ ������.
 ******************************************************************************/
cmd_fsm__error_e cmd_fsm__cmd_set(cmd_fsm__message_t* data, void* ext_data) {
    cmd_fsm__error_e res = FSM_BUSY;
    if (cmd_data.fsm_state == FSM_IDLE) {
        int idx  = -1;
        do {
            idx++;
            memcpy(&cmd_data.msg[idx], &data[idx], sizeof(data[0]));
        } while(data[idx].id != 0);
        cmd_data.ext_data = ext_data;
        cmd_data.msg_idx = 0;
        cmd_data.fsm_state = FSM_WORK;
        res = FSM_OK;
    }
    return res;
}


/*******************************************************************************
 * ������� ���������� ������ ������ DRV_KERNEL.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__kernel(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
//    cmd_fsm__message_t* cur_msg = ext_data;
    cmd_fsm__error_e res = FSM_PARAM_NA;
    switch(cmd) {
        case KERNEL_GET_VERSION:
            res = (cmd_fsm__error_e)PROGRAM_VERSION;
            break;
        case KERNEL_GET_BOARD_VER:
            res = (cmd_fsm__error_e)gpio__get(BOARD_SELECTION_PIN);
            break;
    }
    return res;
}

/*******************************************************************************
 * ������� ���������� ������ ������ �������.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__pwr_exec(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    events__e res = drv_asic_pwr__cmd_exec((drv_asic_pwr__id_e)dev_id, (drv_asic_pwr__cmd_e)cmd, param, cmd_fsm__pwr_exec_cb, ext_data);
    if (res == EVENT__OK)
        cur_msg->cmd |= 0x80;  // ���� ��������...
    return event_to_fsm(res);
}

/*******************************************************************************
 * �������, �� ���������� ���������� ������ ������ �������.
 ******************************************************************************/
static void cmd_fsm__pwr_exec_cb(int motor, events__e event, int param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    cur_msg->cmd &= ~0x80;
    cur_msg->param = (event != EVENT__OK) ? event_to_fsm(event) : param;
}

/*******************************************************************************
 * ������� ���������� ������ ������ �������.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__asic_check_exec(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    events__e res = drv_asic_check__cmd_exec((drv_asic_check__id_e)dev_id, (drv_asic_check__cmd_e)cmd, param, cmd_fsm__asic_check_exec_cb, ext_data);
    if (res == EVENT__OK)
        cur_msg->cmd |= 0x80;  // ���� ��������...
    return event_to_fsm(res);
}

/*******************************************************************************
 * �������, �� ���������� ���������� ������ ������ �������.
 ******************************************************************************/
static void cmd_fsm__asic_check_exec_cb(int asic_id, events__e event, int param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    cur_msg->cmd &= ~0x80;
    cur_msg->param = (event != EVENT__OK) ? event_to_fsm(event) : param;
}

static cmd_fsm__error_e event_to_fsm(events__e event) {
    cmd_fsm__error_e res = FSM_ERROR;
    switch(event) {
        case EVENT__TIMEOUT:
            res = FSM_DEV_TIMEOUT;
            break;
        case EVENT__PARAM_NA:
            res = FSM_PARAM_NA;
            break;
        case EVENT__BUSY:
            res = FSM_BUSY;
            break;
        case EVENT__OK:
            res = FSM_OK;
            break;
        case EVENT__NOT_EXIST:
            res = FSM_NOT_EXIST;
            break;
        case EVENT__NO_OPT:
            res = FSM_NO_OPTION;
            break;
    }
    return res;
}
