/***************************************************************************//**
 * @file drv_asic_check.c.
 * @brief ���� � ��������� ��������� ������ ������� �������� �����.
 * @author a.tushentsov.
 ******************************************************************************/
#include "gpio.h"
#include "System/sw_timer.h"
#include "DRV/uart.h"
#include "drv_asic_check.h"

#define CHECK_UART_BUFF_SIZE 2000

typedef struct {
    u8 len;
    u8 data;
}check_buff__t;
//                              len    1     2                                  len     1
static u8 check_tx_buff_s9[] = {0x07, 0x55, 0xAA, 0x52, 0x05, 0x00, 0x00, 0x0A, 0x00, 0x00};

typedef struct {
    uart__sub_t uart; // ������ ������. �� ������!!!
    u8 rx_buff[CHECK_UART_BUFF_SIZE];
    u8* tx_buff_def;
    union {
        u8* tx_buff;
        check_buff__t* cur_buff;
    };
    events__e event;
    u32 uart_settings;
    int param, gpio_reset;
    drv_asic_check__cmd_exec_cb cb;
    void* ext_data;
    sw_timer__t timer;
}drv_asic_check__struct_t;

static drv_asic_check__struct_t check_data[] = {{.uart_settings = UART_S9_SETTINGS, .tx_buff_def = check_tx_buff_s9, .gpio_reset = MCU__GPIO_P_C_1}};

static void check_uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data);
static void check_uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static u16 parse_rx_buff(drv_asic_check__struct_t* data, int len);
static void asic_check_tmr_cb(struct sw_timer__t *timer, void *ext_data);

events__e drv_asic_check__cmd_exec(drv_asic_check__id_e id, drv_asic_check__cmd_e cmd, int param, drv_asic_check__cmd_exec_cb cb, void* ext_data) {
    if (id >= CHECK_NONE)
        return EVENT__PARAM_NA;
    drv_asic_check__struct_t* data = &check_data[id];
    events__e res = EVENT__ERROR;
    data->tx_buff = data->tx_buff_def;
    data->param = param;
    data->cb = cb;
    data->ext_data = ext_data;
    switch(cmd) {
        case CHECK_SIMPLE:
            gpio__init(data->gpio_reset, GPIO__DIR_OUT, NULL);
            gpio__set(data->gpio_reset, GPIO__STATE_LOW);
            res = uart__open(&data->uart, ASIC_UART, data->uart_settings, check_uart_event_hd, NULL) == EVENT__OK ? EVENT__OK : EVENT__ERROR;
            break;
        default:
            res = EVENT__PARAM_NA;
    }
    return res;
}

static void check_uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
    drv_asic_check__struct_t* data = (drv_asic_check__struct_t*)sub;
    switch(event) {
    case EVENT__OPEN:
        gpio__set(data->gpio_reset, GPIO__STATE_HIGH);
        sw_timer__start(&data->timer, 100, asic_check_tmr_cb, data);
        break;
    default:
    case EVENT__CLOSE:
        data->cb(data - check_data, data->event, data->param, data->ext_data);
        break;
    }
}

static void asic_check_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    drv_asic_check__struct_t* data = (drv_asic_check__struct_t*)ext_data;
    if (uart__tx(&data->uart, &data->cur_buff->data, data->cur_buff->len, check_uart_cb, NULL) != EVENT__OK) {
        data->event = EVENT__ERROR;
        uart__close(&data->uart);
    }
}


static void check_uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    drv_asic_check__struct_t* data = (drv_asic_check__struct_t*)sub;
    data->tx_buff = data->tx_buff + data->cur_buff->len + 1;
    if (((buff == data->rx_buff) && (data->tx_buff != 0x00)) || (event != EVENT__OK)) {
        if (event == EVENT__OK) {
            if (buff == data->rx_buff)
                data->param = parse_rx_buff(data, len);
            data->event = EVENT__OK;
        }
        else
            data->event = EVENT__ERROR;
        gpio__set(data->gpio_reset, GPIO__STATE_LOW);
        uart__close(sub);
        return;
    }
    if (*data->tx_buff)
        uart__tx(sub, &data->cur_buff->data, data->cur_buff->len, check_uart_cb, NULL);
    else
        uart__rx(sub, UART__FRAME_3_5, data->rx_buff, CHECK_UART_BUFF_SIZE, 500, check_uart_cb, NULL);
}

static u16 parse_rx_buff(drv_asic_check__struct_t* data, int len) {
     u16 res = len / 9;

    return res;
}


