/***************************************************************************//**
 * @file crypto_tester.c.
 * @brief  �������� ������ �������.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "gpio.h"
#include "target.h"
#include "crc.h"
#include "System/sw_timer.h"
#include "supervisor.h"
#include "System/framework.h"
#include "i2c.h"
#include "DRV/uart.h"
#include "cmd_fsm.h"

struct uarts__struct_t;

typedef struct uarts__struct_t {
    struct uarts__struct_t* prev;
    struct uarts__struct_t* next;
    u8* data;
    int len;
} uarts__struct_t;

static struct {
    uarts__struct_t uart_list, uart_fsm_cb_sub, uart_tx_err_sub;
    u8 usb_rx_buff[UART_BUFF_SIZE], usb_tx_buff[UART_BUFF_SIZE], msg_error[5];
    u32 freq;
    sw_timer__t timer, motor_tmr;
    u8 button_on, button_on_isr;
    uart__sub_t usb;
}tester = {.uart_list.next = &tester.uart_list, .uart_list.prev = &tester.uart_list, .uart_fsm_cb_sub.data = tester.usb_tx_buff, .uart_tx_err_sub.data = tester.msg_error, .uart_tx_err_sub.len = 5};

static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void test_gpio_cb(int gpio, gpio__state_e pin_state);
static void uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void button__cout();
static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data);
static void cmd_fsm__event_hd(cmd_fsm__message_t* data, void* ext_data);


void main() {
#ifdef _DEBUG_
    DBGMCU->CR = DBGMCU_CR_DBG_STOP;
    DBGMCU->APB1FZ = 0xFFFFFFFF;
    DBGMCU->APB2FZ = 0xFFFFFFFF;
#endif
    supervisor__init();
    sw_timer__init(NULL);
    uart__init();
    supervisor__idle_sub_t main_idle_sub;
    supervisor__idle_lock(&main_idle_sub);

    i2c__init(I2C_PIC, 0, 0, NULL);

    cmd_fsm__init(cmd_fsm__event_hd);

    gpio__init(MCU__GPIO_P_C_0, GPIO__DIR_OUT, NULL);
    gpio__set(MCU__GPIO_P_C_0, GPIO__STATE_HIGH);
    gpio__init(MCU__GPIO_P_C_1, GPIO__DIR_OUT, NULL);
    gpio__set(MCU__GPIO_P_C_1, GPIO__STATE_LOW);

    gpio__init(LED_GREEN, GPIO__DIR_OUT, NULL);
    gpio__init(LED_BLUE, GPIO__DIR_OUT, NULL);
    gpio__init(USER_BT, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | GPIO__INT_EDGE_FALING, test_gpio_cb);

    uart__open(&tester.usb, USB_UART, UART_SETTINGS, uart_event_hd, NULL);

    sw_timer__start(&tester.timer, 500, timer_cb, NULL);

    while(1) {
        button__cout();
        sw_timer__cout();
        i2c__cout();
        cmd_fsm__cout();
    }
}

static void test_gpio_cb(int gpio, gpio__state_e pin_state) {
    tester.button_on_isr = (pin_state == GPIO__STATE_HIGH);
}



static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, timer_cb, NULL);
    gpio__set(LED_BLUE, GPIO__STATE_TOGGLE);
}


static void button__cout() {
    if (tester.button_on != tester.button_on_isr)
        sw_timer__start(&tester.timer, 200, timer_cb, NULL);
}


static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
    switch(event) {
    case EVENT__OPEN:
            uart__rx(&tester.usb, UART__FRAME_3_5, tester.usb_rx_buff, 100, 0, uart_cb, NULL);
        break;
    case EVENT__CLOSE:
        uart__open(sub, sub->uart_id, UART_SETTINGS, uart_event_hd, NULL);
        break;
    }
}

static void uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    cmd_fsm__message_t* msg = (cmd_fsm__message_t*)tester.usb_rx_buff;
    uart__rx(&tester.usb, UART__FRAME_3_5, tester.usb_rx_buff, UART_BUFF_SIZE, 0, uart_cb, NULL);
    if (buff != tester.usb_rx_buff) {
        uarts__struct_t* uart_sub = list__exclude(&tester.uart_list, LIST__HEAD);
        if (uart_sub)
            uart__tx(&tester.usb, uart_sub->data, uart_sub->len, uart_cb, NULL);
        return;
    }
    cmd_fsm__message_t* msg_error = (cmd_fsm__message_t*)tester.msg_error;
    msg_error->id = ID_KERNEL;
    msg_error->cmd = KERNEL_ANSWER;
    if (event != EVENT__OK)
        msg_error->param = FSM_ERROR;
    else if ((crc__8_ccitt(tester.usb_rx_buff, len)) || (len < 5))
        msg_error->param = FSM_CRC_ERROR;
    else {
        tester.usb_rx_buff[len - 1] = 0;
        msg_error->param = cmd_fsm__cmd_set(msg, NULL);
    }
    if (msg_error->param) {
        tester.msg_error[4] = crc__8_ccitt((u8*)&tester.msg_error, 4);
        if ((uart__tx(&tester.usb, tester.msg_error, 5, uart_cb, NULL) != EVENT__OK) && (tester.uart_tx_err_sub.next == NULL))
            list__include(&tester.uart_list, &tester.uart_tx_err_sub, LIST__TAIL);
    }
}

static void cmd_fsm__event_hd(cmd_fsm__message_t* data, void* ext_data) {
    cmd_fsm__message_t* msg = (cmd_fsm__message_t*)tester.usb_tx_buff;
    *(int*)ext_data = data->param;
    int len = 0;
    while(data[len].id != 0) {
        memcpy(&msg[len], &data[len], CMD_LEN);
        len++;
    }
    len *= CMD_LEN;
    tester.usb_tx_buff[len] = crc__8_ccitt((u8*)msg, len);
    if (uart__tx(&tester.usb, tester.usb_tx_buff, len + 1, uart_cb, NULL) != EVENT__OK) {
        tester.uart_fsm_cb_sub.len = len + 1;
        if (tester.uart_fsm_cb_sub.next == NULL)
            list__include(&tester.uart_list, &tester.uart_fsm_cb_sub, LIST__TAIL);
    }
}

