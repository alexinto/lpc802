/***************************************************************************//**
 * @file drv_asic_pwr.h.
 * @brief ���� � ��������� ��������� ������ ������� �������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "mcu.h"
#include "i2c.h"
#include "drv_asic_pwr.h"
#include "System/sw_timer.h"

typedef struct {
    u8 pic_addr;
    int param;
    drv_asic_pwr__cmd_exec_cb cb;
    events__e event;
    void* ext_data;
    const u8* pkt;
    u8* rec_ptr;
    sw_timer__t timer;
}drv_asic_pwr__struct_t;

static u8 rec_buff[10] = {0x01};
static const u8 pkt_s9_off_data[] = {0x01, 0x55, 0x01, 0xAA, 0x01, 0x04, 0x01, 0x07, 0x01, 0x00, 0x01, 0x0B, 0x00, 0x00};
static const u8 pkt_s9_on_data[] = {0x01, 0x55, 0x01, 0xAA, 0x01, 0x04, 0x01, 0x07, 0x01, 0x00, 0x01, 0x0B, 0xFF, 0xFF,
                                0x01, 0x55, 0x01, 0xAA, 0x01, 0x04, 0x01, 0x06, 0x01, 0x00, 0x01, 0x0A, 0xFF, 0xFF,
                                0x01, 0x55, 0x01, 0xAA, 0x01, 0x04, 0x01, 0x16, 0x01, 0x00, 0x01, 0x1A,
                                0x01, 0x55, 0x01, 0xAA, 0x01, 0x05, 0x01, 0x15, 0x01, 0x01, 0x01, 0x00, 0x01, 0x1B, 0xFF, 0xFF, 0x00, 0x00};

static drv_asic_pwr__struct_t pwr_data[] = {{.pic_addr = 0x20, .rec_ptr = rec_buff}};

static void i2c_tx_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);
static void i2c_tmr_cb(struct sw_timer__t *timer, void *ext_data);


events__e drv_asic_pwr__cmd_exec(drv_asic_pwr__id_e id, drv_asic_pwr__cmd_e cmd, int param, drv_asic_pwr__cmd_exec_cb cb, void* ext_data) {
    if (id >= PWR_NONE)
        return EVENT__PARAM_NA;
    drv_asic_pwr__struct_t* data = &pwr_data[id];
    events__e res = EVENT__ERROR;
    data->param = param;
    data->cb = cb;
    data->ext_data = ext_data;
    data->event = EVENT__OK;
    switch(cmd) {
        case PWR_ON_OFF:
            data->pkt = param ? pkt_s9_on_data : pkt_s9_off_data;
            res = i2c__tx(I2C_PIC, data->pic_addr, (u8*)data->pkt + 1, *data->pkt, i2c_tx_cb, data) == EVENT__OK ? EVENT__OK : EVENT__ERROR;
            break;
        default:
            res = EVENT__PARAM_NA;
    }
    return res;
}

static void i2c_tx_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
    static u16 err_cnt = 0;
    int timeout = 100;
    drv_asic_pwr__struct_t* data = (drv_asic_pwr__struct_t*)ext_data;
    if (event == EVENT__OK) {
        err_cnt = 0;
        timeout = 5;
        data->pkt += *data->pkt == 0xFF ? 1 : *data->pkt + 1;
    }
    else if (++err_cnt > 7)
        data->event = EVENT__ERROR;
    sw_timer__start(&data->timer, timeout, i2c_tmr_cb, ext_data);
}


static void i2c_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    drv_asic_pwr__struct_t* data = (drv_asic_pwr__struct_t*)ext_data;
    events__e res = data->event;
    if ((res == EVENT__OK) && (*data->pkt)) {
        if (*data->pkt != 0xFF)
            res = i2c__tx(I2C_PIC, data->pic_addr, (u8*)data->pkt + 1, *data->pkt, i2c_tx_cb, data) == EVENT__OK ? EVENT__CONTINUE : EVENT__ERROR;
        else
            res = i2c__rx(I2C_PIC, data->pic_addr, data->rec_ptr + 1, *data->rec_ptr, i2c_tx_cb, data) == EVENT__OK ? EVENT__CONTINUE : EVENT__ERROR;
    }
    if (res != EVENT__CONTINUE)
        data->cb(data - pwr_data, res, data->param, data->ext_data);
}


