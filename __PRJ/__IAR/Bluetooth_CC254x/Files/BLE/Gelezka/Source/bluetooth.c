/***************************************************************************//**
* @file bluetooth.c
* @brief Попытка переделать фреймворк от TI.
* @author a.tushentsov.
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "hal_types.h"
#include "hal_key.h"
#include "hal_timer.h"
#include "hal_drivers.h"
#include "hal_led.h"
#include "OSAL.h"
#include "OSAL_Tasks.h"
#include "OSAL_PwrMgr.h"
#include "osal_snv.h"
#include "OnBoard.h"
#include "simpleBLEPeripheral.h"

struct {
    buttons_t;
    union{
        struct{
            uint8 led_red  : 1;
            uint8 led_blue : 1;
        };
        uint8 leds;
    };
}bt_data;

static void answer_leds(uint8 led);

void main(void) {
    HAL_BOARD_INIT();
    InitBoard( OB_COLD );
    HalDriverInit();
    osal_snv_init();
    osal_init_system();
    HAL_ENABLE_INTERRUPTS();
//    InitBoard( OB_READY );
    osal_pwrmgr_device( PWRMGR_BATTERY );
//    adc = HalAdcRead (HAL_KEY_JOY_CHN, HAL_ADC_RESOLUTION_8);
    /* Start OSAL */
    osal_start_system(); // No Return from here
    return;
}

void OnBoard_KeyCallback ( uint8 keys, uint8 state ) {
     bt_data.buttons |= keys;
}

const char* const buttons_answer[] = {"Red: ", "Green: "};

static void answer_leds(uint8 led) {
    char buff[20];
    uint8 len, led_msk = 1 << led;
    len = sprintf(buff, "%s%s", buttons_answer[led], (bt_data.leds & led_msk) ? "On" : "Off");
    SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR4, len, buff);
}


void performPeriodicTask( void )
{
    uint8 button;
    for(button = 0; (!(bt_data.buttons & (1 << button))) && (button < 8); button++);
    if (bt_data.buttons) {
        bt_data.leds ^= (1 << button);
        bt_data.buttons &= ~(1 << button);
        answer_leds(button);
    }
    HalLedSet(bt_data.leds , HAL_LED_MODE_TOGGLE);
    HalLedSet(~bt_data.leds , HAL_LED_MODE_OFF);
}
/**************************************************************************************************
                                           CALL-BACKS
**************************************************************************************************/
const char* const bt_cmds_desc[] = {"red on", "green on", "red off", "green off", "red is", "green is", "cmd none"};

void WriteCB(uint8* buff, uint8 len) {
  uint8 buffer[20];
  uint8 length, cmd = 0;
  length = sprintf((char*)buffer, "Cmd not supported.");

  while(strcmp((char*)buff,bt_cmds_desc[cmd]) && (cmd < sizeof(bt_cmds_desc) / sizeof(char*))) cmd++;
  switch(cmd) {
  case 0:
  case 1:
      bt_data.leds |= (1 << cmd);
      answer_leds(cmd);
      break;
  case 3:
      cmd = 4;
  case 2:
      bt_data.leds &= ~(cmd >> 1);
      answer_leds(cmd >> 2);
      break;
  case 4:
  case 5:
      answer_leds(cmd&0x01);
      break;
  default:
      SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR4, length, buffer);
      break;
  }


}




/*************************************************************************************************
**************************************************************************************************/
