﻿/***************************************************************************//**
 * @file device__data.c.
 * @brief  Алгоритм работы анализатора.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "DRV/gpio.h"
#include "MODBUS/mb_rtu.h"
#include "fsm_mode.h"
#include "DRV/display.h"
#include "DRV/psu.h"
#include "DRV/dev_setts.h"
#include "DRV/sync.h"
#include "device.h"
#include "termo.h"
#include "DRV/menu_builder.h"

#ifndef DEVICE__LED_1WIRE
#define DEVICE__LED_1WIRE    200    // Поиск датчиков 1wire
#endif
#ifndef DEVICE__LED_MODBUS
#define DEVICE__LED_MODBUS   500    // Инициализация modbus устройств
#endif
#ifndef DEVICE__LED_COMPLETE
#define DEVICE__LED_COMPLETE 1000   // Переход в штатный режим работы
#endif

typedef enum {
    DEVICE__STATE_IDLE = 0,
    DEVICE__STATE_INIT,
    DEVICE__STATE_WORK,
}device__state_e;

static struct {
    device__state_e fsm;
    framework__sub_t sub;
    sw_timer__t led_tmr;
    u8 modbus_init;
    u8 termo__init;
}device__data;

static void device__cout();
static void modbus_init_cb(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data);
static void modbus_slave_cb(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void device__termo_cb(events__e event, float temp, void* ext_data);

events__e device__init() {
    framework__cout_subscribe(&device__data.sub, device__cout);
    gpio__init(0, LED_BLUE, GPIO__DIR_OUT, NULL);
    gpio__init(0, PWR_BUTTON_RST, GPIO__DIR_OUT, NULL);
    gpio__set(0, PWR_BUTTON_RST, GPIO__STATE_LOW);   // 380 В выкл
    gpio__init(0, PSU_UART__DIR_PIN, GPIO__DIR_OUT, NULL);
    gpio__set(0, PSU_UART__DIR_PIN, GPIO__STATE_LOW);
    gpio__init(0, SYNC_UART__DIR_PIN, GPIO__DIR_OUT, NULL);
    gpio__set(0, SYNC_UART__DIR_PIN, GPIO__STATE_LOW);
    sw_timer__start(&device__data.led_tmr, DEVICE__LED_1WIRE, timer_cb, NULL);
    dev_setts__init(DEV_SETTS__ID, 0, DEV_SETTS__ADDR, DEV_SETTS__SIZE, MENU_PARAM__NONE + 5, flash__exec);  // 0 - в проекте только одна флеш- флеш МК
    uart__init();
    device__data.fsm = DEVICE__STATE_IDLE;
    device__data.termo__init = 0;
    termo__init(device__termo_cb, NULL);  // Запускаем процесс поиска устройств 1wire

    return EVENT__OK;
}

static void device__cout() {
    switch(device__data.fsm) {
        case DEVICE__STATE_IDLE:  // Ищем устройства 1wire
            if (!device__data.termo__init)
                break;
            device__data.fsm = DEVICE__STATE_INIT;
            sw_timer__start(&device__data.led_tmr, DEVICE__LED_MODBUS, timer_cb, NULL);
            device__data.modbus_init = 0;
            mb_rtu__init(MODBUS_INT, 0x15, HW_UART_2, UART__BR_9600 | UART__PAR_NONE | UART__STOP_2 | UART__DATA_8 | UART__FC_485 | UART__PIN_INV, modbus_init_cb, (void*)0);
            break;
        case DEVICE__STATE_INIT: // Ждем инициализацию modbus
            if (!device__data.modbus_init)
                return;
            sw_timer__start(&device__data.led_tmr, DEVICE__LED_COMPLETE, timer_cb, NULL);
            psu__init(PSU_MAIN, MODBUS_INT, PSU__MB_ADDR);
            sync__init(0, MODBUS_SYNC, SYNC__MB_ADDR);
            display__init(DISPLAY__RENEW_TIME);    // Создание меню
            fsm_mode__init();
            device__data.fsm = DEVICE__STATE_WORK;
            break;
        case DEVICE__STATE_WORK:
            break;
        default:
            break;
    }
}

static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, timer_cb, NULL);
    gpio__set(0, LED_BLUE, GPIO__STATE_TOGGLE);
}

static void modbus_init_cb(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data) {
    mb_rtu__init(MODBUS_SYNC, SYNC__MB_ADDR, HW_UART_3, UART__BR_9600 | UART__PAR_NONE | UART__STOP_2 | UART__DATA_8 | UART__FC_485, modbus_slave_cb, (void*)0);
}

static void modbus_slave_cb(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data) {
    device__data.modbus_init = 1;
}

static void device__termo_cb(events__e event, float temp, void* ext_data) {
    device__data.termo__init = 1;
}

int uart__cmd_handler(int uart_id, uart__cmd_e cmd) {
    switch(cmd) {
    case UART__CMD_FC_DIR_TX:
        gpio__set(0, uart_id == HW_UART_2 ? PSU_UART__DIR_PIN : SYNC_UART__DIR_PIN, GPIO__STATE_HIGH);
        break;
    case UART__CMD_FC_DIR_RX:
        gpio__set(0, uart_id == HW_UART_2 ? PSU_UART__DIR_PIN : SYNC_UART__DIR_PIN, GPIO__STATE_LOW);
        break;
    }
    return 0;
}

