﻿#include "target.h"
#include "DRV/spi.h"
#include "mcu.h"

extern events__e hw_spi__func(hw_spi__cmd_e cmd, u8 spi_id, u8 cs_id, u32 sett_wr, u32 sett_rd, u8 *rx_buff, u32 rx_len, u8 *tx_buff, u32 tx_len, hw_spi__cb_t callback, void* ext_data);

const spi__cfg_tbl_t spi_cfg__tbl[] = {
    [SPI__DISPLAY] = {.spi_id = SPI_3, .cs_id = MCU__GPIO_NONE, .func = hw_spi__func},      // MCU__GPIO_P_C_11
};