﻿/***************************************************************************//**
 * @file fsm_mode.c.
 * @brief  Алгоритм работы автомата режимов.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "System/sw_timer.h"
#include "System/framework.h"
#include "DRV/gpio.h"
#include "fsm_mode.h"
#include "DRV/psu.h"
#include "DRV/sync.h"
#include "DRV/display.h"
#include "DRV/menu_builder.h"
#include "termo.h"

#ifndef FSM_MODE__PSU_TIMEOUT
#define FSM_MODE__PSU_TIMEOUT   100      // Интервал опроса, мс
#endif
#ifndef FSM__ERR_CNT_MAX
#define FSM__ERR_CNT_MAX        10       // Счетчик ошибок
#endif
#ifndef FSM_MODE__RST_TIMEOUT
#define FSM_MODE__RST_TIMEOUT   10000    // таймаут на управление ногой вкл/выкл 380 В
#endif
#ifndef FSM_MODE__BUTTON_FLT
#define FSM_MODE__BUTTON_FLT    4        // ФИльтр кнопок - от 1 до 8!!!
#endif

typedef enum {
    FSM__BUTTON_SW1 = 0,
    FSM__BUTTON_SW2 = 1,
    FSM__BUTTON_ON  = 2,
    FSM__BUTTON_OFF = 3,
    FSM__BUTTON_LOCK= 4,

    FSM__BUTTON_MAX,
}fsm__button_id_e;

typedef enum {
    FSM__CMD_ID_BUTTON = 0,
    FSM__CMD_ID_MODBUS = 1,
    FSM__CMD_ID_TMR    = 2,
    FSM__CMD_ID_MAX,
}fsm__cmd_id_e;

typedef enum {
    FSM_MODE__SCMD_END    = 0,
    FSM_MODE__SCMD_READ,
    FSM_MODE__SCMD_ON,
    FSM_MODE__SCMD_OFF,
    FSM_MODE__SCMD_V,
    FSM_MODE__SCMD_I,
    FSM_MODE__SCMD_OCP,
    FSM_MODE__SCMD_OVP,
    FSM_MODE__SCMD_SYNC,
    FSM_MODE__SCMD_NO_SYNC,
    FSM_MODE__ENC_UNLOCK,
    FSM_MODE__ENC_CHARGE,
    FSM_MODE__ENC_MANUAL,
    FSM_MODE__CHK_TEMP,
    FSM_MODE__CHRGE_CHK_U,
}fsm_mode__scmd_e;

typedef struct {
    uint8_t mask;
    uint8_t gpio;
}button__struct_t;

typedef struct {
    union {
        struct {
            uint8_t sw1  :1;
            uint8_t sw2  :1;
            uint8_t on   :1;
            uint8_t off  :1;
            uint8_t lock :1;
        };
        uint8_t mask;
    };
    fsm_mode__cmd_e cmd;
}fsm__cmd_tbl_t;

static const fsm_mode__e cmd2mode_tbl[] = {
    [FSM_MODE__CMD_NONE]   = FSM_MODE__OFF,
    [FSM_MODE__CMD_MANUAL] = FSM_MODE__MANUAL,
    [FSM_MODE__CMD_OCP]    = FSM_MODE__OCP,
    [FSM_MODE__CMD_CHARGE] = FSM_MODE__CHARGE,
    [FSM_MODE__CMD_ON]     = FSM_MODE__OFF,
    [FSM_MODE__CMD_OFF]    = FSM_MODE__OFF,
    [FSM_MODE__CMD_ERR]    = FSM_MODE__OFF,
    [FSM_MODE__CMD_LOCK]   = FSM_MODE__OFF,
};

static const fsm_state__e cmd2state_tbl[] = {
    [FSM_MODE__CMD_NONE]   = FSM_STATE__ERROR,
    [FSM_MODE__CMD_MANUAL] = FSM_STATE__ERROR,
    [FSM_MODE__CMD_OCP]    = FSM_STATE__ERROR,
    [FSM_MODE__CMD_CHARGE] = FSM_STATE__ERROR,
    [FSM_MODE__CMD_ON]     = FSM_STATE__ON,
    [FSM_MODE__CMD_OFF]    = FSM_STATE__OFF,
    [FSM_MODE__CMD_ERR]    = FSM_STATE__ERROR,
    [FSM_MODE__CMD_LOCK]   = FSM_STATE__LOCK,
};

// Таблица команд автомату состояний. Комбинации не указанные в таблице игнорируются.
static const fsm__cmd_tbl_t fsm__md_tbl[] = {{.sw1 = 1, .sw2 = 0, .on = 1, .off = 0, .lock = 0, .cmd = FSM_MODE__CMD_MANUAL},
                                             {.sw1 = 1, .sw2 = 1, .on = 1, .off = 0, .lock = 0, .cmd = FSM_MODE__CMD_OCP},
                                             {.sw1 = 0, .sw2 = 1, .on = 1, .off = 0, .lock = 0, .cmd = FSM_MODE__CMD_CHARGE}};

// Скрипты Автомата Состояний. Скрипт FSM_STATE__WAIT вызывается 1 раз при смене режима.
static const fsm_mode__scmd_e** s_mode_tbl[] = {
    [FSM_MODE__OFF] = (const fsm_mode__scmd_e*[]){
        [FSM_STATE__OFF] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_NO_SYNC, FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_END},
        [FSM_STATE__ON] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_NO_SYNC, FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_END},
        [FSM_STATE__WAIT] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_NO_SYNC, FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_END},
        [FSM_STATE__ERROR] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_NO_SYNC, FSM_MODE__SCMD_READ, FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_END},
        [FSM_STATE__LOCK] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_NO_SYNC, FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_END},
    },
    [FSM_MODE__MANUAL] = (const fsm_mode__scmd_e*[]){
        [FSM_STATE__OFF] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_READ, FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_SYNC, FSM_MODE__SCMD_V, FSM_MODE__SCMD_I, FSM_MODE__SCMD_END},
        [FSM_STATE__ON] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_READ, FSM_MODE__SCMD_SYNC, FSM_MODE__SCMD_OCP, FSM_MODE__SCMD_V, FSM_MODE__SCMD_ON, FSM_MODE__SCMD_END},
        [FSM_STATE__WAIT] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_SYNC, FSM_MODE__ENC_MANUAL, FSM_MODE__SCMD_END},
        [FSM_STATE__ERROR] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_READ, FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_SYNC, FSM_MODE__SCMD_V, FSM_MODE__SCMD_I, FSM_MODE__SCMD_END},
        [FSM_STATE__LOCK] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_NO_SYNC, FSM_MODE__SCMD_END},
    },
    [FSM_MODE__OCP] = (const fsm_mode__scmd_e*[]){
        [FSM_STATE__OFF] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_V, FSM_MODE__SCMD_I, FSM_MODE__SCMD_END},
        [FSM_STATE__ON] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_READ, FSM_MODE__SCMD_V, FSM_MODE__SCMD_I, FSM_MODE__SCMD_ON, FSM_MODE__SCMD_END},
        [FSM_STATE__WAIT] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_NO_SYNC, FSM_MODE__ENC_MANUAL, FSM_MODE__ENC_UNLOCK, FSM_MODE__SCMD_END},
        [FSM_STATE__ERROR] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_END},
        [FSM_STATE__LOCK] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_END},
    },
    [FSM_MODE__CHARGE] = (const fsm_mode__scmd_e*[]){
        [FSM_STATE__OFF] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_V, FSM_MODE__SCMD_I, FSM_MODE__CHK_TEMP, FSM_MODE__SCMD_END},
        [FSM_STATE__ON] = (fsm_mode__scmd_e[]){FSM_MODE__CHK_TEMP, FSM_MODE__SCMD_READ, FSM_MODE__CHRGE_CHK_U, FSM_MODE__SCMD_V, FSM_MODE__SCMD_I, FSM_MODE__SCMD_ON, FSM_MODE__SCMD_END},
        [FSM_STATE__WAIT] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_NO_SYNC, FSM_MODE__ENC_CHARGE, FSM_MODE__SCMD_END},
        [FSM_STATE__ERROR] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_OFF, FSM_MODE__CHK_TEMP, FSM_MODE__SCMD_END},
        [FSM_STATE__LOCK] = (fsm_mode__scmd_e[]){FSM_MODE__SCMD_OFF, FSM_MODE__SCMD_END},
    },
};

typedef union {
    struct {
        uint8_t otp      : 1;     // Превышение температуры.
        uint8_t chg      : 1;     // Завершение цикла заряда.
        uint8_t check_u  : 1;     // Проверка напряжения заряда.
        uint8_t unv      : 1;     // Отсутствует заряд АКБ.
        uint8_t reserved : 4;
    }bits;
    uint8_t data;
}fsm_mode__flags_u;

static struct {
    fsm_mode__flags_u flags;
    uint8_t button_dis;
    uint8_t lock_en;
    uint8_t err_cnt;
    events__e event;
    int filter_cnt;
    float voltage;
    fsm_mode__cmd_e sub_cmd, tmr_cmd, cmd_que[FSM__CMD_ID_MAX];
    fsm__cmd_tbl_t cur_cmd;
    button__struct_t button[FSM__BUTTON_MAX];
    fsm_mode__e mode;
    fsm_state__e state;
    sw_timer__t timer, psu_tmr, rst_tmr, charge_tmr;
    fsm_mode__cb_t cb;
    fsm_mode__scmd_e* script;
}fsm_data = {.button = {{0, SW1_BUTTON}, {0, SW2_BUTTON}, {0, PWR_ON_BUTTON}, {0, PWR_OFF_BUTTON}, {0, PWR_BUTTON_LOCK}}};

static framework__sub_t cout_sub;

static void fsm_timer_cb(struct sw_timer__t *timer, void *ext_data);
static void fsm_rst_cb(struct sw_timer__t *timer, void *ext_data);
static uint8_t pin_state_get(u8 mask);
static void fsm_mode__cout();
static events__e fsm_cmd__exec(fsm_mode__scmd_e script);
static void psu__rd_cb(int id, events__e event, void* ext_data);
static void psu__wr_cb(int id, events__e event, void* ext_data);
static void psu_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void charge_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static fsm_mode__cmd_e fsm_mode__cmd_check();
static void fsm_mode__cmd_clr(fsm_mode__cmd_e cmd);

events__e fsm_mode__init() {
    for(int i = 0; i < FSM__BUTTON_MAX; i++)
        gpio__init(0, fsm_data.button[i].gpio, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
    fsm_data.flags.data = 0;
    sw_timer__start(&fsm_data.timer, 10, fsm_timer_cb, NULL);
    gpio__set(0, PWR_BUTTON_RST, GPIO__STATE_LOW);   // 380 В выкл
    sw_timer__start(&fsm_data.rst_tmr, FSM_MODE__RST_TIMEOUT, fsm_rst_cb, NULL);
    int termo_calc_mode = menu_builder__param_get(MENU_PARAM__TERMO_MODE);
    termo__set_mode((termo__e)termo_calc_mode);
    return EVENT__OK;
}

fsm_mode__e fsm_mode__get() {
    return fsm_data.mode;
}

fsm_state__e fsm_mode__state_get() {
    fsm_state__e res = fsm_data.state;
    if (res == FSM_STATE__WAIT)
        res = cmd2state_tbl[fsm_data.sub_cmd];
    switch(res) {
        case FSM_STATE__OFF:
            if (fsm_data.flags.bits.chg)
                res = FSM_STATE__END;
            break;
        case FSM_STATE__ERROR:
            if (fsm_data.flags.bits.otp)
                res = FSM_STATE__OTP;
            else if (fsm_data.flags.bits.unv)
                res = FSM_STATE__UNV;
            break;
        default:
            break;
    }
    return res;
}

void fsm_mode__cmd(fsm_mode__cmd_e cmd) {
    fsm_data.cmd_que[FSM__CMD_ID_MODBUS] = cmd;
}

void fsm_mode__clr_err() {
    fsm_data.err_cnt = 0;
}

static void fsm_timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, fsm_timer_cb, NULL);
    int cur_filt = fsm_data.filter_cnt;
    uint8_t pin_state, temp;
    if (++fsm_data.filter_cnt > FSM_MODE__BUTTON_FLT)
        fsm_data.filter_cnt = 0;

    for(int i = 0; i < FSM__BUTTON_MAX; i++) {
        fsm_data.button[i].mask |= (gpio__get(0, fsm_data.button[i].gpio) << cur_filt);
        if (!fsm_data.filter_cnt) {
            pin_state = pin_state_get(fsm_data.button[i].mask);
            fsm_data.button[i].mask = 0;
            fsm_data.cur_cmd.mask &= ~(1 << i);
            fsm_data.cur_cmd.mask |= (pin_state << i);
        }
    }
// Определяем текущий режим работы и даем команду на смену (если нужно).
    if (!fsm_data.filter_cnt) {
        if (fsm_data.cur_cmd.cmd == FSM_MODE__CMD_NONE) {
            for(temp = 0; temp < (sizeof(fsm__md_tbl) / sizeof(fsm__md_tbl[0])); temp++) {
                if (fsm_data.cur_cmd.mask == fsm__md_tbl[temp].mask) {
                    if (fsm_data.mode != cmd2mode_tbl[fsm__md_tbl[temp].cmd])
                        fsm_data.cur_cmd.cmd = fsm__md_tbl[temp].cmd;
                    break;
                }
            }
        }
        if (fsm_data.cur_cmd.mask & (1 << FSM__BUTTON_LOCK)) {
            if (fsm_data.lock_en) {
                fsm_data.cmd_que[FSM__CMD_ID_TMR] = FSM_MODE__CMD_LOCK;
                fsm_data.err_cnt = 0;
            }
        }
        else if ((fsm_data.state == FSM_STATE__LOCK) && (++fsm_data.err_cnt > 100)) {
            fsm_data.sub_cmd = FSM_MODE__CMD_OFF;
            fsm_data.err_cnt = 0;
        }
        if (!fsm_data.button_dis) {
            if ((fsm_data.cur_cmd.mask & (1 << FSM__BUTTON_ON)) == 0)
                fsm_data.cmd_que[FSM__CMD_ID_BUTTON] = FSM_MODE__CMD_ON;
            if (fsm_data.cur_cmd.mask & (1 << FSM__BUTTON_OFF))
                fsm_data.cmd_que[FSM__CMD_ID_BUTTON] = FSM_MODE__CMD_OFF;
        }
    }
}

static uint8_t pin_state_get(u8 mask) {
    uint8_t one_cnt = 0;
    for(int i = 0; i < FSM_MODE__BUTTON_FLT; i++)
        if (mask & (1 << i))
            one_cnt++;
    return one_cnt > (FSM_MODE__BUTTON_FLT >> 1) ? 1 : 0;
}

static void fsm_mode__cmd_clr(fsm_mode__cmd_e cmd) {
    for(int i = 0; i < FSM__CMD_ID_MAX; i++)
        fsm_data.cmd_que[i] = FSM_MODE__CMD_NONE;
    fsm_data.cmd_que[FSM__CMD_ID_TMR] = FSM_MODE__CMD_NONE;
    fsm_data.sub_cmd = fsm_data.tmr_cmd = cmd;
    fsm_data.script = (fsm_mode__scmd_e*)s_mode_tbl[fsm_data.mode][FSM_STATE__WAIT];
    fsm_data.event = EVENT__OK;
    fsm_data.state = FSM_STATE__WAIT;
    fsm_data.flags.data = 0;
}


static fsm_mode__cmd_e fsm_mode__cmd_check() {
    if (fsm_data.sub_cmd == FSM_MODE__CMD_NONE) {
        for(int i = 0; i < FSM__CMD_ID_MAX; i++) {
            if (fsm_data.cmd_que[i] != FSM_MODE__CMD_NONE) {
                fsm_data.sub_cmd = fsm_data.cmd_que[i];
                fsm_data.cmd_que[i] = FSM_MODE__CMD_NONE;
                fsm_data.tmr_cmd = fsm_data.sub_cmd;
                sw_timer__stop(&fsm_data.psu_tmr);
                if (i != FSM__CMD_ID_TMR)
                    fsm_data.flags.data = 0;
                break;
            }
        }
    }
    return fsm_data.sub_cmd;
}

static void fsm_mode__cout() {
    fsm_mode__cmd_e sub_cmd = fsm_mode__cmd_check();
    switch(fsm_data.state) {
        case FSM_STATE__LOCK:
        if (sub_cmd != FSM_MODE__CMD_OFF)
            break;
        fsm_mode__cmd_clr(FSM_MODE__CMD_OFF);
        case FSM_STATE__ON:
            if (fsm_data.cur_cmd.cmd != FSM_MODE__CMD_NONE) {
                fsm_data.mode = cmd2mode_tbl[fsm_data.cur_cmd.cmd];
                fsm_data.cur_cmd.cmd = FSM_MODE__CMD_NONE;
                fsm_mode__cmd_clr(FSM_MODE__CMD_OFF);
            }
            else if (sub_cmd != FSM_MODE__CMD_NONE) {
                fsm_data.script = (fsm_mode__scmd_e*)s_mode_tbl[fsm_data.mode][cmd2state_tbl[sub_cmd]];
                fsm_data.event = EVENT__OK;
                fsm_data.state = FSM_STATE__WAIT;
            }
            break;
        case FSM_STATE__OFF:
            if (fsm_data.cur_cmd.cmd != FSM_MODE__CMD_NONE) {
                fsm_data.mode = cmd2mode_tbl[fsm_data.cur_cmd.cmd];
                fsm_data.cur_cmd.cmd = FSM_MODE__CMD_NONE;
                fsm_mode__cmd_clr(FSM_MODE__CMD_OFF);
            }
            else if (sub_cmd != FSM_MODE__CMD_NONE) {
                fsm_data.script = (fsm_mode__scmd_e*)s_mode_tbl[fsm_data.mode][cmd2state_tbl[sub_cmd]];
                fsm_data.event = EVENT__OK;
                fsm_data.state = FSM_STATE__WAIT;
                if (sub_cmd == FSM_MODE__CMD_ON) {
                    fsm_data.flags.data = 0;
                    sw_timer__start(&fsm_data.charge_tmr, menu_builder__param_get(MENU_PARAM__CHG_TIME_ERR) * 1000, charge_tmr_cb, NULL);
                }
            }
            break;
        case FSM_STATE__WAIT:
            if (fsm_data.event == EVENT__WAIT)
                break;
            if (fsm_data.event == EVENT__OK) {
                if (*fsm_data.script != FSM_MODE__SCMD_END) {
                    fsm_data.event = EVENT__WAIT;
                    if (fsm_cmd__exec(*fsm_data.script) != EVENT__OK)
                        fsm_data.event = EVENT__ERROR;
                    fsm_data.script++;
                    break;
                }
                if (fsm_data.err_cnt)
                    fsm_data.err_cnt--;
            }
            else if (++fsm_data.err_cnt > FSM__ERR_CNT_MAX) {
                fsm_data.err_cnt = 0;
                sub_cmd = fsm_data.tmr_cmd = FSM_MODE__CMD_ERR;
                display__set_err(1);
            }
            fsm_data.sub_cmd = FSM_MODE__CMD_NONE;
            fsm_data.state = cmd2state_tbl[sub_cmd];
            sw_timer__start(&fsm_data.psu_tmr, FSM_MODE__PSU_TIMEOUT, psu_tmr_cb, NULL);
            break;
        case FSM_STATE__ERROR:
        default:
            if (sub_cmd == FSM_MODE__CMD_ERR) {
                fsm_data.script = (fsm_mode__scmd_e*)s_mode_tbl[fsm_data.mode][FSM_STATE__ERROR];
                fsm_data.event = EVENT__OK;
                fsm_data.state = FSM_STATE__WAIT;
            }
            else if ((fsm_data.cur_cmd.cmd != FSM_MODE__CMD_NONE) || (sub_cmd != FSM_MODE__CMD_NONE)) {
                fsm_data.flags.data = 0;
                fsm_data.state = FSM_STATE__OFF;
                display__set_err(0);
            }
            break;
    }
}

static void psu_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    if (fsm_data.sub_cmd == FSM_MODE__CMD_NONE)
        fsm_data.sub_cmd = fsm_data.tmr_cmd;
}

static events__e fsm_cmd__exec(fsm_mode__scmd_e script) {
    events__e event = fsm_data.event = EVENT__OK;
    psu__state_e psu_state;
    float v, c, enc_v, enc_c;
    menu_bulder__params_e param;
    uint16_t data;
    switch(script) {
        case FSM_MODE__SCMD_READ:
            fsm_data.event = EVENT__WAIT;
            event = psu__read(PSU_MAIN, psu__rd_cb, NULL);
            break;
        case FSM_MODE__SCMD_V:
            enc_v = (float)encoder__get(ENCODER__VOLTAGE) / 10;
            v = psu__get_vref(PSU_MAIN);
            if ((v + 0.05 < enc_v) || (v - 0.05 > enc_v) || (psu__get_state(PSU_MAIN) == PSU__STATE_OFF)) {
                fsm_data.event = EVENT__WAIT;
                event = psu__set_v(PSU_MAIN, enc_v, psu__wr_cb, NULL);
            }
            break;
        case FSM_MODE__SCMD_I:
            enc_c = (float)encoder__get(ENCODER__CURRENT) / 10;
            c = psu__get_iref(PSU_MAIN);
            if ((c + 0.05 < enc_c) || (c - 0.05 > enc_c) || (psu__get_state(PSU_MAIN) == PSU__STATE_OFF)) {
                fsm_data.event = EVENT__WAIT;
                event = psu__set_i(PSU_MAIN, enc_c, psu__wr_cb, NULL);
            }
            break;
        case FSM_MODE__SCMD_ON:
            psu_state = psu__get_state(PSU_MAIN);
            if (psu_state != PSU__STATE_ON) {
                fsm_data.event = EVENT__WAIT;
                event = psu__set_state(PSU_MAIN, PSU__STATE_ON, psu__wr_cb, NULL);
            }
            break;
        case FSM_MODE__SCMD_OFF:
            display__set_v(0);
            display__set_c(0);
            display__set_out(0);
            fsm_data.event = EVENT__WAIT;
            event = psu__set_state(PSU_MAIN, PSU__STATE_OFF, psu__wr_cb, NULL);
            break;
        case FSM_MODE__SCMD_OCP:
            enc_c = (float)encoder__get(ENCODER__CURRENT) / 10;
            c = psu__get_ocp(PSU_MAIN);
            if ((c + 0.05 < enc_c) || (c - 0.05 > enc_c) || (!psu__get_ocp_state(PSU_MAIN).on)) {
                fsm_data.event = EVENT__WAIT;
                event = psu__set_ocp(PSU_MAIN, enc_c, psu__wr_cb, NULL);
            }
            else if (psu__get_ocp_state(PSU_MAIN).flag) {
                fsm_data.lock_en = 0;
                fsm_data.sub_cmd = fsm_data.tmr_cmd = FSM_MODE__CMD_ERR;
                gpio__set(0, PWR_BUTTON_RST, GPIO__STATE_LOW);   // 380 В выкл
                display__set_v(0);
                display__set_c(0);
                display__set_out(0);
                sw_timer__start(&fsm_data.rst_tmr, FSM_MODE__RST_TIMEOUT, fsm_rst_cb, NULL);
                event = EVENT__ERROR;
            }
            break;
        case FSM_MODE__SCMD_SYNC:
            data = menu_builder__param_get(MENU_PARAM__MB_MODE);
            if (data) {
                if (data == SYNC__MODE_SLAVE) {
                    fsm_data.button_dis = 1;
                    encoder__lock(ENCODER__VOLTAGE);
                    encoder__lock(ENCODER__CURRENT);
                }
                else {
                    fsm_data.button_dis = 0;
                    encoder__unlock(ENCODER__VOLTAGE);
                    encoder__unlock(ENCODER__CURRENT);
                }
                fsm_data.event = EVENT__WAIT;
                event = sync__start(0, (sync__mode_e)data,  psu__wr_cb, NULL);
                break;
            }                               // Без break!!!
        case FSM_MODE__SCMD_NO_SYNC:
            fsm_data.button_dis = 0;
            sync__stop(0);                 // Без break!!!
        case FSM_MODE__ENC_UNLOCK:
            encoder__unlock(ENCODER__VOLTAGE);
            encoder__unlock(ENCODER__CURRENT);
            break;
        case FSM_MODE__ENC_MANUAL:
            encoder__init(ENCODER__VOLTAGE, menu_builder__p_min_get(MENU_PARAM__VOLTAGE), menu_builder__p_max_get(MENU_PARAM__VOLTAGE));
            encoder__init(ENCODER__CURRENT, menu_builder__p_min_get(MENU_PARAM__CURRENT), menu_builder__p_max_get(MENU_PARAM__CURRENT));
            encoder__set(ENCODER__VOLTAGE, menu_builder__param_get(MENU_PARAM__VOLTAGE));
            encoder__set(ENCODER__CURRENT, menu_builder__param_get(MENU_PARAM__CURRENT));
            break;
        case FSM_MODE__ENC_CHARGE:
            encoder__lock(ENCODER__CURRENT);
            encoder__lock(ENCODER__VOLTAGE);
            param = menu_builder__param_get(MENU_PARAM__AKB_TYPE) ? MENU_PARAM__I_12120M : MENU_PARAM__I_14A510;
            encoder__init(ENCODER__CURRENT, menu_builder__p_min_get(param), menu_builder__p_max_get(param));
            encoder__set(ENCODER__CURRENT, menu_builder__param_get(param));
            v = 83.3 + (float)menu_builder__param_get(MENU_PARAM__T_COMP) / 10;
            v *= 10;
            encoder__init(ENCODER__VOLTAGE, (int)v, (int)v);
            encoder__set(ENCODER__VOLTAGE, (int)v);
            break;
        case FSM_MODE__CHK_TEMP:
            c = menu_builder__param_get(MENU_PARAM__T_MAX);
            if (c < termo__get()) {
                fsm_data.flags.bits.otp = 1;
                event = EVENT__ERROR;
            }
            else
                fsm_data.flags.bits.otp = 0;
            break;
        case FSM_MODE__CHRGE_CHK_U:
            if (fsm_data.flags.bits.check_u) {
                param = menu_builder__param_get(MENU_PARAM__AKB_TYPE) ? MENU_PARAM__I_END_12120M : MENU_PARAM__I_END_14A510;
                if (fsm_data.voltage < (56.4 + (float)menu_builder__param_get(MENU_PARAM__T_COMP) / 10)) {
                    fsm_data.flags.bits.unv = 1;
                    fsm_data.event = EVENT__ERROR;
                }
                else if (psu__get_i(PSU_MAIN) < ((float)menu_builder__param_get(param) / 10)) {
                    fsm_data.flags.bits.chg = 1;
                    fsm_data.cmd_que[FSM__CMD_ID_TMR] = FSM_MODE__CMD_OFF;
                }
            }
            break;
        case FSM_MODE__SCMD_END:
        default:
            break;
    }
    if (event != EVENT__OK)
        fsm_data.event = event;
    return event;
}

static void psu__rd_cb(int id, events__e event, void* ext_data) {
    fsm_data.event = event;
    if (event != EVENT__OK)
        return;
    float temp, tk;
    float voltage = psu__get_v(id);
    switch(fsm_data.mode) {
        case FSM_MODE__CHARGE:
            tk = (float)menu_builder__param_get(MENU_PARAM__T_COMP) / 10;
            voltage += tk;
            temp = termo__get();
            if (temp <= 0)
                voltage += 2.5;
            else if (temp < 16)
                voltage += 2.5 - 0.163 * temp;
            else if (temp >= 51)
                voltage -= 5;
            else if (temp >= 36)
                voltage += 11.55 - 0.33 * temp;
            break;
        default:
            break;
    }
    fsm_data.voltage = voltage;
    display__set_v(voltage);
    display__set_c(psu__get_i(id));
    display__set_out(psu__get_state(id) == PSU__STATE_ON ? 1 : 0);
}

static void psu__wr_cb(int id, events__e event, void* ext_data) {
    fsm_data.event = event;
}

static void fsm_rst_cb(struct sw_timer__t *timer, void *ext_data) {
    if (!ext_data) {
        gpio__set(0, PWR_BUTTON_RST, GPIO__STATE_HIGH);   // 380 В вкл
        sw_timer__start(&fsm_data.rst_tmr, FSM_MODE__RST_TIMEOUT, fsm_rst_cb, (void*)1);
    }
    else {
        fsm_data.lock_en = 1;
        framework__cout_subscribe(&cout_sub, fsm_mode__cout);
    }
}

static void charge_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    fsm_data.flags.bits.check_u = 1;
}