﻿/***************************************************************************//**
 * @file display.c.
 * @brief Модуль управления дисплеем 2004.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include <stdio.h>
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "DRV/display.h"
#include "DRV/lcd2004.h"
#include "DRV/menu_builder.h"
#include "encoder.h"
#include "DRV/max7219.h"
#include "fsm_mode.h"
#include "termo.h"

typedef enum {
    DISPLAY__MODE_MAIN = 0,
    DISPLAY__MODE_MENU,
}display__mode_e;

typedef struct {
    uint8_t clr;
    display__mode_e mode;
    int renew_ms;
    char buff[200];          // Буфер вывода на дисплей.
}display__struct_t;

typedef struct {
    uint8_t change;          // Признак выбора элемента. Кнопка выбора.
    int cur_value;
    menu__item__t* page;     // Текущая страница меню.
    menu__item__t* item;     // Текущий элемент меню. Установка курсора.
}display__menu_t;

typedef struct {
    float voltage, current;
    display__struct_t display;
    display__menu_t menu;
    sw_timer__t timer, enc_butt_tmr;
}display__data_t;

static display__data_t display__data;

static const char* display___akb_descr[] = {"14-A510", "12-120M"};
// Строковые значения режимов работы. Соответсвуют fsm_mode__e
static const char* display___mode_descr[] = {"OFF", "80V", "LIMIT", "CHARGE"};
static const char* display___state_descr[] = {"[OFF]", "[ON]", "[...]", "[ERR]", "[LOCK]", "[OTP]", "[END]", "[UnV]"};
static const char* display___mb_mode_descr[] = {"N", "S", "M"};


static void enc_butt_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void display___tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void encoder__cb(int enc, events__e event, int ticks, void* ext_data);
static void menu__buff_fill(char* buff);
static void main__buff_fill(char* buff);
static void encoder__menu(events__e event);
static void encoder__main(events__e event);
static void lcd2004__init_cb(int id, events__e event, void* ext_data);
static void lcd2004__cb(int id, events__e event, void* ext_data);
static void disp7__init_cb(int id, events__e event, void* ext_data);
static void disp7__cb(int id, events__e event, void* ext_data);

events__e display__init(int renew_ms) {
    gpio__init(0, PWR_LED, GPIO__DIR_OUT, NULL);
    gpio__init(0, OUT_LED, GPIO__DIR_OUT, NULL);
    gpio__init(0, ERR_LED, GPIO__DIR_OUT, NULL);
    gpio__set(0, PWR_LED, GPIO__STATE_HIGH);
    gpio__set(0, OUT_LED, GPIO__STATE_LOW);
    gpio__set(0, ERR_LED, GPIO__STATE_LOW);
    display__data.menu.item = display__data.menu.page = menu_builder__create();  // Создание меню
    encoder__init(ENCODER__MENU, 0, 0);
    encoder__init(ENCODER__VOLTAGE, 0, 0);
    encoder__init(ENCODER__CURRENT, 0, 0);
    encoder__set_mux(ENCODER__VOLTAGE, 1);
    encoder__set_mux(ENCODER__CURRENT, 1);
    encoder__start(ENCODER__MENU, 0, encoder__cb, NULL);
    encoder__start(ENCODER__VOLTAGE, 0, encoder__cb, NULL);
    encoder__start(ENCODER__CURRENT, 0, encoder__cb, NULL);
    encoder__set(ENCODER__VOLTAGE, 0);
    encoder__set(ENCODER__CURRENT, 0);

    display__data.display.renew_ms = renew_ms;
    lcd2004__init(0, I2C_DISPL, I2C_ADDR, lcd2004__init_cb, NULL);
    return EVENT__OK;
}

events__e display__set_v(float voltage) {
    display__data.voltage = voltage;
    return EVENT__OK;
}

events__e display__set_c(float current) {
    display__data.current = current;
    return EVENT__OK;
}

events__e display__set_err(uint8_t err) {
    gpio__set(0, ERR_LED, err ? GPIO__STATE_HIGH : GPIO__STATE_LOW);
    return EVENT__OK;
}

events__e display__set_out(uint8_t on) {
    gpio__set(0, OUT_LED, on ? GPIO__STATE_HIGH : GPIO__STATE_LOW);
    return EVENT__OK;
}

static void lcd2004__init_cb(int id, events__e event, void* ext_data) {
    max7219__init(DISP7TH, DEV_DISPLAY, disp7__init_cb, NULL);
}

static void disp7__init_cb(int id, events__e event, void* ext_data) {
    display__struct_t* p_displ = &display__data.display;
    sw_timer__start(&display__data.timer, p_displ->renew_ms, display___tmr_cb, NULL);
}


static void display___tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    display__struct_t* p_displ = &display__data.display;
    switch (p_displ->mode) {
        case DISPLAY__MODE_MAIN:
            main__buff_fill(p_displ->buff);
            break;
        case DISPLAY__MODE_MENU:
            menu__buff_fill(p_displ->buff);
            break;
    }
    if (p_displ->clr) {
        p_displ->clr = 0;
        lcd2004__clr(0, lcd2004__cb, (void*)1);
    }
    else
        lcd2004__set(0, p_displ->buff, strlen(p_displ->buff), lcd2004__cb, NULL);                      // выводим буфер на дисплей
}

static void menu__buff_fill(char* buff) {
    display__menu_t* p_menu = &display__data.menu;
    menu__item__t* cur_item = display__data.menu.page;
    char value[20]= {0};
    int cur_pos = 0;
    do {
        menu_builder__p2str(cur_item, value, 20);               // Если элемент не выбран- выводим значение параметра
        if (cur_item == p_menu->item) {
            if ((cur_item->data) && (p_menu->change)) {          // Если элемент выбран- выводим значение параметра с признаком выбора
                menu_builder__p2str(cur_item, &value[1], 18);
                sprintf(&value[strlen(value)], "]");
                value[0] = '[';
            }
            cur_pos += sprintf(&buff[cur_pos], "%c", '>');  // Установка курсора
        }
        else
            cur_pos += sprintf(&buff[cur_pos], "%c", ' ');                  // Остальные строки менюю...
        cur_pos += sprintf(&buff[cur_pos], "%s", cur_item->item_descr);
        if (cur_item->data) {
            for(int i = cur_pos - strlen(cur_item->item_descr) + 19 - strlen(value); cur_pos < i; cur_pos++) // Выводим значение в конец строк
                buff[cur_pos] = ' ';
        }
        cur_pos += sprintf(&buff[cur_pos], "%s\n", value);
        cur_item = cur_item->next;
    }while(cur_item);
}

static void main__buff_fill(char* buff) {
    float temperature;
    int cur_pos = 0;
    fsm_mode__e mode = fsm_mode__get();
    fsm_state__e state = fsm_mode__state_get();
    cur_pos += sprintf(&buff[cur_pos], "Voltage: % 9.1f V\n", display__data.voltage);       // Значение регистра modbus
    cur_pos += sprintf(&buff[cur_pos], "Current: % 9.1f A\n", display__data.current);       // Значение регистра modbus
    switch(mode) {
        case FSM_MODE__CHARGE:
            temperature = termo__get();
            cur_pos += sprintf(&buff[cur_pos], "AKB: % 8s % 6s\n", display___akb_descr[menu_builder__param_get(MENU_PARAM__AKB_TYPE)], display___state_descr[state]);     // Режим работы, состяние
            cur_pos += sprintf(&buff[cur_pos], "% 3.0f\n", temperature);     // Температура
            break;
        default:
            cur_pos += sprintf(&buff[cur_pos], "Mode: % 7s % 6s\n", display___mode_descr[mode], display___state_descr[state]);     // Режим работы, состяние
            cur_pos += sprintf(&buff[cur_pos], "% 3s\n", display___mb_mode_descr[menu_builder__param_get(MENU_PARAM__MB_MODE)]);   // Режим параллельной работы
            break;
    }
}


static void lcd2004__cb(int id, events__e event, void* ext_data) {
    display__struct_t* p_displ = &display__data.display;
    if (ext_data) {
        lcd2004__set(0, p_displ->buff, strlen(p_displ->buff), lcd2004__cb, NULL);                      // выводим буфер на дисплей
        return;
    }
    char buff[32];
    sprintf(buff, "% 5.1f% 5.1f", (float)encoder__get(ENCODER__VOLTAGE) / 10, (float)encoder__get(ENCODER__CURRENT) / 10);
    if (max7219__set(DISP7TH, buff, disp7__cb, NULL) != EVENT__OK)
        sw_timer__start(&display__data.timer, p_displ->renew_ms, display___tmr_cb, NULL);
}

static void disp7__cb(int id, events__e event, void* ext_data) {
    display__struct_t* p_displ = &display__data.display;
    sw_timer__start(&display__data.timer, p_displ->renew_ms, display___tmr_cb, NULL);
}

static void encoder__cb(int enc, events__e event, int ticks, void* ext_data) {
    display__struct_t* p_displ = &display__data.display;
    switch(enc) {
        case ENCODER__CURRENT:
        case ENCODER__VOLTAGE:
            if (event == EVENT__BUTTON_PRESS)
                encoder__set_mux(enc, 10);
            else if (event == EVENT__BUTTON_RELEASE)
                encoder__set_mux(enc, 1);
            fsm_mode__clr_err();      // Сбрасываем ошибки при активности енкодеров тока и напряжения.
            return;
        default:
            break;
    }
    switch (p_displ->mode) {
        case DISPLAY__MODE_MAIN:
            encoder__main(event);
            break;
        case DISPLAY__MODE_MENU:
            encoder__menu(event);
            break;
    }
}

static void encoder__main(events__e event) {
    switch(event) {
        case EVENT__BUTTON_PRESS:
            sw_timer__start(&display__data.enc_butt_tmr, 5000, enc_butt_tmr_cb, NULL);
        break;
        case EVENT__BUTTON_RELEASE:
            sw_timer__stop(&display__data.enc_butt_tmr);
        default:
            break;
    }
}

static void enc_butt_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    display__struct_t* p_displ = &display__data.display;
    switch (p_displ->mode) {
        case DISPLAY__MODE_MAIN:
            display__data.display.clr = 1;
            p_displ->mode = DISPLAY__MODE_MENU;
            break;
        case DISPLAY__MODE_MENU:
            display__data.display.clr = 1;
            p_displ->mode = DISPLAY__MODE_MAIN;
            break;
    }
}


static void encoder__menu(events__e event) {
    display__struct_t* p_displ = &display__data.display;
    display__menu_t* p_menu = &display__data.menu;
    menu_bulder__params_e cur_param;
    if (p_menu->change) {
        if (event != EVENT__BUTTON_PRESS) {
            menu_builder__param_set(menu_builder__item2id(p_menu->item),encoder__get(ENCODER__MENU));
            return;
        }
        else {
            menu_builder__param_save(menu_builder__item2id(p_menu->item));           // сохраняем параметр во флеш
        }
    }
    switch(event) {
        case EVENT__DOWN:
            if (p_menu->item->next)
                p_menu->item = p_menu->item->next;
            break;
        case EVENT__UP:
            if (p_menu->item->prev)
                p_menu->item = p_menu->item->prev;
            break;
        case EVENT__BUTTON_PRESS:
            if ((p_menu->item->data) && (!p_menu->item->read_only)) {
                cur_param = menu_builder__item2id(p_menu->item);
                encoder__init(ENCODER__MENU, menu_builder__p_min_get(cur_param), menu_builder__p_max_get(cur_param));
                p_menu->cur_value = menu_builder__param_get(cur_param);
                encoder__set(ENCODER__MENU, p_menu->cur_value);
                p_menu->change ^= 1;
            }
            else if (p_menu->item->child) {
                p_menu->item = p_menu->page = p_menu->item->child;
                p_displ->clr = 1;
            }
            else if (p_menu->item->parrent) {                  //выход из меню
                display__data.display.clr = 1;
                p_displ->mode = DISPLAY__MODE_MAIN;
            }
        break;
        default:
            break;
    }
}




