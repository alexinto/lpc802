/***************************************************************************//**
 * @file encoder.c.
 * @brief ������ ������ � ���������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "mcu.h"
#include "encoder.h"

typedef struct {
    u8 init;
    events__e event;
    void* ext_data;
    encoder__cb_t cb;
}encoder__struct_t;


static encoder__struct_t encoder__data[3] = {{.event = EVENT__WAIT}, {.event = EVENT__WAIT}, {.event = EVENT__WAIT}};


/*******************************************************************************
 * ������� ������������� ��������. ������� �������� ����.
 ******************************************************************************/
void encoder__init(int enc) {
    if (enc == enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    if (enc_ptr->init)
        return;
    NVIC_DisableIRQ(LPTIM1_IRQn);
    RCC->CSR |= RCC_CSR_LSION;
    while ((RCC->CSR & RCC_CSR_LSION) == 0) {}
    MODIFY_REG(RCC->DCKCFGR2, RCC_DCKCFGR2_LPTIM1SEL_Msk, RCC_DCKCFGR2_LPTIM1SEL_0); // HSI clock = RCC_DCKCFGR2_LPTIM1SEL_1
    RCC->APB1ENR |= RCC_APB1ENR_LPTIM1EN;
    gpio__af_init(MCU__GPIO_P_D_12, 0, 3);
    gpio__af_init(MCU__GPIO_P_E_1, 0, 3);
    LPTIM1->CR = 0;
    LPTIM1->IER = 3 << 5;
    LPTIM1->CFGR = 1 << 24 | 3 << 1 | 3 << 3;
    LPTIM1->ICR = 0x7F;
    NVIC_SetPriority(LPTIM1_IRQn, 0);
    NVIC_EnableIRQ(LPTIM1_IRQn);
    enc_ptr->init = 1;
    encoder__clr(enc);
}

/*******************************************************************************
 * ������� ������� �������� � ��������� �� ����� ������ �� �������� ����� �����.
 ******************************************************************************/
events__e encoder__start(int enc, int ticks, encoder__cb_t cb, void* ext_data) {
    if ((enc == enc_none) || (ticks < 0))               // ������������� ��������= ������������� ��������, �������� MOVE_TO_SW1.
        return EVENT__OK;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    events__e res = EVENT__OK;
    CRITICAL_SECTION_ON
    int cur_tick = (LPTIM1->CNT > 32767 ? 0 : LPTIM1->CNT);
    if ((ticks > (cur_tick - ENC_PPR / 20)) && (ticks < (cur_tick + ENC_PPR / 20)))
         res = EVENT__CONTINUE;
    else {
        LPTIM1->CMP = ticks;
        enc_ptr->event = EVENT__WAIT;
        LPTIM1->ICR = 0x7F;
    }
    CRITICAL_SECTION_OFF
    if (res == EVENT__OK) {
        enc_ptr->ext_data = ext_data;
        enc_ptr->cb = cb;
        LPTIM1->IER = 1;
    }
    return res;
}

/*******************************************************************************
 * ������� ��������� ������ �������� �������� ��������. ������� ���������� ����.
 ******************************************************************************/
void encoder__stop(int enc) {
    if (enc == enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    LPTIM1->IER = 0;
    LPTIM1->ICR = 0x7F;
    enc_ptr->event = EVENT__WAIT;
}

/*******************************************************************************
 * ������� ��������� �������� ��������.
 ******************************************************************************/
void encoder__clr(int enc) {
    if (enc == enc_none)
        return;
    LPTIM1->CR = 0;
    LPTIM1->CR;
    LPTIM1->CR = 1;
    LPTIM1->CR;
    LPTIM1->CR = 5;
    LPTIM1->ARR = 0xFFFF;
}

/*******************************************************************************
 * ������� ��������������� ��������. ������� ������������� ����.
 ******************************************************************************/
void encoder__deinit(int enc) {
    if (enc == enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    NVIC_DisableIRQ(LPTIM1_IRQn);
    LPTIM1->CR = 0;
    LPTIM1->ICR = 0x7F;
    enc_ptr->init = 0;
}

/*******************************************************************************
 * ������� ��������� �������� �������� ��������.
 ******************************************************************************/
int encoder__get(int enc) {
    int16_t cnt = LPTIM1->CNT;
    int res = cnt;
    if (cnt < 0)
        res = -1 & cnt;
    return enc == enc_none ? 0 : res;
}

/*******************************************************************************
 * �������- ���������� �������� ��������� ������ ��������.
 ******************************************************************************/
void encoder__cout() {
    for(u8 i =0; i < 3; i++) {
        encoder__struct_t* enc_ptr = &encoder__data[i];
        if (enc_ptr->event != EVENT__WAIT) {
            enc_ptr->event = EVENT__WAIT;
            int ticks = LPTIM1->CNT;
            if (enc_ptr->cb)
                enc_ptr->cb(i, ticks, enc_ptr->ext_data);
        }
    }
}

/*******************************************************************************
 * ���������� ISR ��������.
 ******************************************************************************/
void encoder__IRQ_handler(int enc) {
    LPTIM1->ICR = 0x7F;
//    LPTIM1->IER = 0;
    encoder__data[enc].event = EVENT__OK;
}