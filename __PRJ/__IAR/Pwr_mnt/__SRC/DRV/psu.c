﻿/***************************************************************************//**
 * @file psu.c.
 * @brief Модуль управления источником питания.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include <math.h>
#include "DRV/psu.h"
#include "MODBUS/mb_rtu.h"
#include "DRV/psu_reg.h"

#ifndef PSU__CNT
#define PSU__CNT 1
#endif

typedef enum {
    PSU__OP_NONE     = 0,
    PSU__OP_INIT,
    PSU__OP_WR_VREF,
    PSU__OP_WR_IREF,
    PSU__OP_WR_PWR_CTRL,
    PSU__OP_WR_OUT_CTRL,
    PSU__OP_RD_DO,
    PSU__OP_RD_DI_CC,
    PSU__OP_RD_DI_OCV,
    PSU__OP_RD_AI,
    PSU__OP_RD_AO,
    PSU__OP_WR_OCP,
    PSU__OP_WR_OVP,
    PSU__OP_WR_OCP_SET,
    PSU__OP_WR_OCP_CLR,
    PSU__OP_WR_OVP_CLR,
    PSU__OP_WR_OVP_SET,
}psu__oper_e;

typedef struct {
    uint8_t mb_id;
    uint8_t pwr_addr;
    uint16_t voltage;
    uint16_t current;
    psu__oper_e* oper;
    psu__cb_t cb;
    void* ext_data;
}psu__struct_t;

typedef enum {
    PSU__SCRIPT_NONE   = 0,
    PSU__SCRIPT_READ,
    PSU__SCRIPT_SET_V,
    PSU__SCRIPT_SET_I,
    PSU__SCRIPT_SET_OCP,
    PSU__SCRIPT_SET_OVP,
    PSU__SCRIPT_PWR_CTRL,
}psu__script_e;

static const psu__oper_e* psu_script_tbl[] = {
    [PSU__SCRIPT_NONE]     = (psu__oper_e[]){PSU__OP_NONE},
    [PSU__SCRIPT_READ]     = (psu__oper_e[]){PSU__OP_RD_DO, PSU__OP_RD_DI_CC, PSU__OP_RD_DI_OCV, PSU__OP_RD_AI, PSU__OP_RD_AO, PSU__OP_NONE},
    [PSU__SCRIPT_SET_V]    = (psu__oper_e[]){PSU__OP_WR_OVP_CLR, PSU__OP_WR_VREF, PSU__OP_NONE},
    [PSU__SCRIPT_SET_I]    = (psu__oper_e[]){PSU__OP_WR_OCP_CLR, PSU__OP_WR_IREF, PSU__OP_NONE},
    [PSU__SCRIPT_SET_OCP]  = (psu__oper_e[]){PSU__OP_WR_OCP_CLR, PSU__OP_WR_OCP, PSU__OP_WR_OCP_SET, PSU__OP_NONE},
    [PSU__SCRIPT_SET_OVP]  = (psu__oper_e[]){PSU__OP_WR_OVP_CLR, PSU__OP_WR_OVP, PSU__OP_WR_OVP_SET, PSU__OP_NONE},
    [PSU__SCRIPT_PWR_CTRL] = (psu__oper_e[]){PSU__OP_WR_OUT_CTRL, PSU__OP_WR_PWR_CTRL, PSU__OP_NONE},
};



static psu__struct_t psu__data[PSU__CNT];

static events__e psu__script_exec(psu__struct_t* d);
static void psu__mb_cb(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data);


events__e psu__init(int id, uint8_t mb_id, uint8_t pwr_addr) {
    psu__struct_t* d = &psu__data[id];
    if ((id >= PSU__CNT) || (d->oper))
        return EVENT__BUSY;
    d->mb_id = mb_id;
    d->pwr_addr = pwr_addr;
    return EVENT__OK;
}

float psu__get_vref(int id) {
    psu__struct_t* d = &psu__data[id];
    uint16_t data;
    if (id >= PSU__CNT)
        return 0;
    mb_reg__get(d->mb_id, MB_REG__TYPE_AO, PSU_REG__AO_VOLTAGE, &data);
    return (float)data / 10;
}

float psu__get_iref(int id) {
    psu__struct_t* d = &psu__data[id];
    uint16_t data;
    if (id >= PSU__CNT)
        return 0;
    mb_reg__get(d->mb_id, MB_REG__TYPE_AO, PSU_REG__AO_CURRENT, &data);
    return (float)data / 10;
}

float psu__get_v(int id) {
    psu__struct_t* d = &psu__data[id];
    uint16_t data, v10;
    if (id >= PSU__CNT)
        return 0;
    mb_reg__get(d->mb_id, MB_REG__TYPE_AI, PSU_REG__AI_VOLTAGE_10, &v10);
    mb_reg__get(d->mb_id, MB_REG__TYPE_AI, PSU_REG__AI_VOLTAGE, &data);
    return (float)data / pow(10, v10);
}

float psu__get_i(int id) {
    psu__struct_t* d = &psu__data[id];
    uint16_t data, c10;
    if (id >= PSU__CNT)
        return 0;
    mb_reg__get(d->mb_id, MB_REG__TYPE_AI, PSU_REG__AI_CURRENT_10, &c10);
    mb_reg__get(d->mb_id, MB_REG__TYPE_AI, PSU_REG__AI_CURRENT, &data);
    return (float)data / pow(10, c10);
}

float psu__get_ocp(int id) {
    psu__struct_t* d = &psu__data[id];
    uint16_t data;
    if (id >= PSU__CNT)
        return 0;
    mb_reg__get(d->mb_id, MB_REG__TYPE_AO, PSU_REG__AO_OCP, &data);
    return (float)data / 10;
}

psu__state_e psu__get_state(int id) {
    psu__struct_t* d = &psu__data[id];
    uint16_t data;
    if (id >= PSU__CNT)
        return PSU__STATE_OFF;
    mb_reg__get(d->mb_id, MB_REG__TYPE_DO, PSU_REG__DO_OUT_SW, &data);
    return data ? PSU__STATE_ON : PSU__STATE_OFF;
}

psu__ocp_v_t psu__get_ocp_state(int id) {
    psu__struct_t* d = &psu__data[id];
    psu__ocp_v_t state = {0};
    uint16_t data;
    if (id < PSU__CNT) {
        mb_reg__get(d->mb_id, MB_REG__TYPE_DO, PSU_REG__DO_OCP, &data);
        state.on = data;
        mb_reg__get(d->mb_id, MB_REG__TYPE_DI, PSU_REG__DI_OCP, &data);
        state.flag = data;
    }
    return state;
}

events__e psu__read(int id, psu__cb_t cb, void* ext_data) {
    psu__struct_t* d = &psu__data[id];
    if ((id >= PSU__CNT) || (d->oper))
        return EVENT__BUSY;
    d->oper = (psu__oper_e*)psu_script_tbl[PSU__SCRIPT_READ];
    d->cb = cb;
    d->ext_data = ext_data;
    return psu__script_exec(d);
}

events__e psu__set_v(int id, float voltage, psu__cb_t cb, void* ext_data) {
    psu__struct_t* d = &psu__data[id];
    if ((id >= PSU__CNT) || (d->oper))
        return EVENT__BUSY;
    d->oper = (psu__oper_e*)psu_script_tbl[PSU__SCRIPT_SET_V];
    d->cb = cb;
    d->ext_data = ext_data;
    mb_reg__set(d->mb_id, MB_REG__TYPE_AO, PSU_REG__AO_VOLTAGE, (int)(voltage * 10));
    return psu__script_exec(d);
}

events__e psu__set_i(int id, float current, psu__cb_t cb, void* ext_data) {
    psu__struct_t* d = &psu__data[id];
    if ((id >= PSU__CNT) || (d->oper))
        return EVENT__BUSY;
    d->oper = (psu__oper_e*)psu_script_tbl[PSU__SCRIPT_SET_I];
    d->cb = cb;
    d->ext_data = ext_data;
    mb_reg__set(d->mb_id, MB_REG__TYPE_AO, PSU_REG__AO_CURRENT, (int)(current * 10));
    return psu__script_exec(d);
}


events__e psu__set_ocp(int id, float current, psu__cb_t cb, void* ext_data) {
    psu__struct_t* d = &psu__data[id];
    if ((id >= PSU__CNT) || (d->oper))
        return EVENT__BUSY;
    d->oper = (psu__oper_e*)psu_script_tbl[PSU__SCRIPT_SET_OCP];
    d->cb = cb;
    d->ext_data = ext_data;
    mb_reg__set(d->mb_id, MB_REG__TYPE_AO, PSU_REG__AO_OCP, (int)(current * 10));
    return psu__script_exec(d);
}


events__e psu__set_state(int id, psu__state_e state, psu__cb_t cb, void* ext_data) {
    psu__struct_t* d = &psu__data[id];
    if ((id >= PSU__CNT) || (d->oper))
        return EVENT__BUSY;
    d->oper = (psu__oper_e*)psu_script_tbl[PSU__SCRIPT_PWR_CTRL];
    d->cb = cb;
    d->ext_data = ext_data;
    mb_reg__set(d->mb_id, MB_REG__TYPE_DO, PSU_REG__DO_OUT_SW, state);
    mb_reg__set(d->mb_id, MB_REG__TYPE_DO, PSU_REG__DO_PWRON, state);
    return psu__script_exec(d);
}


static void psu__mb_cb(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data) {
    psu__struct_t* d = (psu__struct_t*)ext_data;
    if ((event == EVENT__OK) && (d->oper)) {
        d->oper++;
        event = psu__script_exec(d);
    }
    if ((event != EVENT__OK) || (!d->oper)) {
        d->oper = NULL;
        if (d->cb)
            d->cb(d - psu__data, event, d->ext_data);
    }
}


static events__e psu__script_exec(psu__struct_t* d) {
    events__e event = EVENT__OK;
    switch(*d->oper) {
    case PSU__OP_WR_VREF:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_AO, PSU_REG__AO_VOLTAGE, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_IREF:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_AO, PSU_REG__AO_CURRENT, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_OUT_CTRL:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_DO, PSU_REG__DO_OUT_SW, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_PWR_CTRL:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_DO, PSU_REG__DO_PWRON, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_RD_DO:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_RD_DO, PSU_REG__DO_OUT_SW, PSU_REG__DO_MAX - PSU_REG__DO_OUT_SW, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_RD_DI_CC:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_RD_DI, PSU_REG__DI_CC, 2, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_RD_DI_OCV:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_RD_DI, PSU_REG__DI_OVP, PSU_REG__DI_MAX - PSU_REG__DI_OVP, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_RD_AI:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_RD_AI, PSU_REG__AI_VOLTAGE, PSU_REG__AI_MAX - PSU_REG__AI_VOLTAGE, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_RD_AO:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_RD_AO, PSU_REG__AO_ID, PSU_REG__AO_MAX - PSU_REG__AO_ID, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_OCP_CLR:
        mb_reg__set(d->mb_id, MB_REG__TYPE_DO, PSU_REG__DO_OCP, 0);
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_DO, PSU_REG__DO_OCP, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_OVP:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_AO, PSU_REG__AO_OVP, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_OCP:
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_AO, PSU_REG__AO_OCP, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_OCP_SET:
        mb_reg__set(d->mb_id, MB_REG__TYPE_DO, PSU_REG__DO_OCP, 1);
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_DO, PSU_REG__DO_OCP, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_OVP_CLR:
        mb_reg__set(d->mb_id, MB_REG__TYPE_DO, PSU_REG__DO_OVP, 0);
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_DO, PSU_REG__DO_OVP, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_WR_OVP_SET:
        mb_reg__set(d->mb_id, MB_REG__TYPE_DO, PSU_REG__DO_OVP, 1);
        event = mb_rtu__request(d->mb_id, d->pwr_addr, MB_RTU__OP_WR_DO, PSU_REG__DO_OVP, 1, psu__mb_cb, (void*)d);
        break;
    case PSU__OP_NONE:
    default:
        d->oper = NULL;
        break;
    }
    if (event != EVENT__OK)
        d->oper = NULL;
    return event;
}








