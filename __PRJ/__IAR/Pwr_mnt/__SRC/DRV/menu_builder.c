﻿/***************************************************************************//**
 * @file menu_builder.c.
 * @brief Модуль построения меню для 3х строчного дисплея.
 * @author a.tushentsov.
 ******************************************************************************/

#include "target.h"
#include <stdio.h>
#include "DRV/menu_builder.h"
#include "DRV/dev_setts.h"

typedef enum {
    MENU_BUILDER__TYPE_INT = 0,
    MENU_BUILDER__TYPE_FLOAT_10,
    MENU_BUILDER__TYPE_VER,
    MENU_BUILDER__TYPE_MB_MODE,
    MENU_BUILDER__TYPE_AKB,
    MENU_BUILDER__TYPE_TERMO,
}menu_builder__types_e;

typedef struct {
    int value;
    menu_builder__types_e type;
    int min;
    int max;
}menu_builder__struct_t;

static char* menu_builder__mode_tbl[]  = {"none", "slave", "master"};
static char* menu_builder__akb_tbl[]   = {"14-A510", "12-120M-B"};
static char* menu_builder__termo_tbl[] = {"MIN", "MAX", "AVG"};

// Начаьные значения параметров
static menu_builder__struct_t menu_builder__param_tbl[] = {
    [MENU_PARAM__VERSION]      = {0,     MENU_BUILDER__TYPE_VER,       0,   0},
    [MENU_PARAM__MB_MODE]      = {0,     MENU_BUILDER__TYPE_MB_MODE,   0,   2},
    [MENU_PARAM__MB_ADDR]      = {0x15,  MENU_BUILDER__TYPE_INT,       1,   240},
    [MENU_PARAM__VOLTAGE]      = {900,   MENU_BUILDER__TYPE_FLOAT_10,  750, 940},
    [MENU_PARAM__CURRENT]      = {100,   MENU_BUILDER__TYPE_FLOAT_10,  0,   1600},
    [MENU_PARAM__AKB_TYPE]     = {0,     MENU_BUILDER__TYPE_AKB,       0,   1},
    [MENU_PARAM__I_14A510]     = {250,   MENU_BUILDER__TYPE_FLOAT_10,  250, 400},
    [MENU_PARAM__I_12120M]     = {250,   MENU_BUILDER__TYPE_FLOAT_10,  250, 430},
    [MENU_PARAM__I_END_14A510] = {100,   MENU_BUILDER__TYPE_FLOAT_10,  1,   400},
    [MENU_PARAM__I_END_12120M] = {100,   MENU_BUILDER__TYPE_FLOAT_10,  1,   430},
    [MENU_PARAM__TERMO_MODE]   = {1,     MENU_BUILDER__TYPE_TERMO,     0,   2},
    [MENU_PARAM__T_MAX]        = {50,    MENU_BUILDER__TYPE_INT,       40,  60},
    [MENU_PARAM__T_COMP]       = {7,     MENU_BUILDER__TYPE_FLOAT_10,  -99, 99},
    [MENU_PARAM__CHG_TIME_ERR] = {10,    MENU_BUILDER__TYPE_INT,       1,   60},


};


static const menu__item__t menu_builder__menu[];
static const menu__item__t menu_builder__charge[];
static const menu__item__t menu_builder__akb[];


// Страница параметров
static const menu__item__t menu_builder__param[] = {{.item_descr = "MB Mode", .next = (menu__item__t*)&menu_builder__param[1],                                                       \
                                                          .data = &menu_builder__param_tbl[MENU_PARAM__MB_MODE]},
                                                         {.item_descr = "Modbas id", .next = (menu__item__t*)&menu_builder__param[2], .prev = (menu__item__t*)menu_builder__param,   \
                                                          .data = &menu_builder__param_tbl[MENU_PARAM__MB_ADDR], .read_only = 1},
                                                         {.item_descr = "Version", .next = (menu__item__t*)&menu_builder__param[3], .prev = (menu__item__t*)&menu_builder__param[1], \
                                                          .data = &menu_builder__param_tbl[MENU_PARAM__VERSION], .read_only = 1},
                                                         {.item_descr = "..", .prev = (menu__item__t*)&menu_builder__param[2], .child = (menu__item__t*)menu_builder__menu},
};


// Страница параметров для режима 80V
static const menu__item__t menu_builder__normal[] = {{.item_descr = "Voltage", .next = (menu__item__t*)&menu_builder__normal[1], .prev = (menu__item__t*)menu_builder__normal,        \
                                                           .data = &menu_builder__param_tbl[MENU_PARAM__VOLTAGE]},
                                                          {.item_descr = "Current", .next = (menu__item__t*)&menu_builder__normal[2], .prev = (menu__item__t*)menu_builder__normal,   \
                                                           .data = &menu_builder__param_tbl[MENU_PARAM__CURRENT]},
                                                          {.item_descr = "..", .prev = (menu__item__t*)&menu_builder__normal[1], .child = (menu__item__t*)menu_builder__menu},
};

// Страница параметров для 14-A510
static const menu__item__t menu_builder__14_a510[] = {{.item_descr = "Current", .next = (menu__item__t*)&menu_builder__14_a510[1], .prev = (menu__item__t*)menu_builder__14_a510,     \
                                                       .data = &menu_builder__param_tbl[MENU_PARAM__I_14A510]},
                                                      {.item_descr = "I-off", .next = (menu__item__t*)&menu_builder__14_a510[2], .prev = (menu__item__t*)menu_builder__14_a510,       \
                                                       .data = &menu_builder__param_tbl[MENU_PARAM__I_END_14A510]},
                                                      {.item_descr = "..", .prev = (menu__item__t*)&menu_builder__14_a510[1], .child = (menu__item__t*)menu_builder__akb},
};

// Страница параметров для 12-120M
static const menu__item__t menu_builder__12_120m[] = {{.item_descr = "Current", .next = (menu__item__t*)&menu_builder__12_120m[1], .prev = (menu__item__t*)menu_builder__12_120m,     \
                                                       .data = &menu_builder__param_tbl[MENU_PARAM__I_12120M]},
                                                      {.item_descr = "I-off", .next = (menu__item__t*)&menu_builder__12_120m[2], .prev = (menu__item__t*)menu_builder__12_120m,       \
                                                       .data = &menu_builder__param_tbl[MENU_PARAM__I_END_12120M]},
                                                      {.item_descr = "..", .prev = (menu__item__t*)&menu_builder__12_120m[1], .child = (menu__item__t*)menu_builder__akb},
};


// Страница настройки АКБ
static const menu__item__t menu_builder__akb[] = {{.item_descr = "AKB type", .next = (menu__item__t*)&menu_builder__akb[1], .prev = (menu__item__t*)menu_builder__akb,                \
                                                   .data = &menu_builder__param_tbl[MENU_PARAM__AKB_TYPE]},
                                                  {.item_descr = "14-A510", .next = (menu__item__t*)&menu_builder__akb[2], .prev = (menu__item__t*)menu_builder__akb, .child = (menu__item__t*)menu_builder__14_a510},
                                                  {.item_descr = "12-120M", .next = (menu__item__t*)&menu_builder__akb[3], .prev = (menu__item__t*)&menu_builder__akb[1], .child = (menu__item__t*)menu_builder__12_120m},
                                                  {.item_descr = "..", .prev = (menu__item__t*)&menu_builder__akb[2], .child = (menu__item__t*)menu_builder__charge},
};

// Страница настройки Термодатчиков
static const menu__item__t menu_builder__termo[] = {{.item_descr = "Mode", .next = (menu__item__t*)&menu_builder__termo[1], .prev = (menu__item__t*)menu_builder__termo,              \
                                                     .data = &menu_builder__param_tbl[MENU_PARAM__TERMO_MODE]},
                                                    {.item_descr = "Max", .next = (menu__item__t*)&menu_builder__termo[2], .prev = (menu__item__t*)menu_builder__termo,               \
                                                     .data = &menu_builder__param_tbl[MENU_PARAM__T_MAX]},
                                                    {.item_descr = "Comp", .next = (menu__item__t*)&menu_builder__termo[3], .prev = (menu__item__t*)&menu_builder__termo[1],          \
                                                     .data = &menu_builder__param_tbl[MENU_PARAM__T_COMP]},
                                                    {.item_descr = "..", .prev = (menu__item__t*)&menu_builder__termo[2], .child = (menu__item__t*)menu_builder__charge},
};

// Страница параметров для режима Charge
static const menu__item__t menu_builder__charge[] = {{.item_descr = "AKB", .next = (menu__item__t*)&menu_builder__charge[1], .prev = (menu__item__t*)menu_builder__charge, .child = (menu__item__t*)menu_builder__akb},
                                                     {.item_descr = "TERMO", .next = (menu__item__t*)&menu_builder__charge[2], .prev = (menu__item__t*)menu_builder__charge, .child = (menu__item__t*)menu_builder__termo},
                                                     {.item_descr = "Time UnV", .next = (menu__item__t*)&menu_builder__charge[3], .prev = (menu__item__t*)&menu_builder__charge[1],   \
                                                      .data = &menu_builder__param_tbl[MENU_PARAM__CHG_TIME_ERR]},
                                                     {.item_descr = "..", .prev = (menu__item__t*)&menu_builder__charge[2], .child = (menu__item__t*)menu_builder__menu},
};


// Главная страница меню
static const menu__item__t menu_builder__menu[] = {{.item_descr = "80V", .next = (menu__item__t*)&menu_builder__menu[1], .prev = (menu__item__t*)menu_builder__menu, .child = (menu__item__t*)menu_builder__normal},
                                            {.item_descr = "CHARGE", .next = (menu__item__t*)&menu_builder__menu[2], .prev = (menu__item__t*)menu_builder__menu, .child = (menu__item__t*)menu_builder__charge},
                                            {.item_descr = "PARAMETERS", .next = (menu__item__t*)&menu_builder__menu[3], .child = (menu__item__t*)menu_builder__param, .prev = (menu__item__t*)&menu_builder__menu[1],},
                                            {.item_descr = "..", .prev =  (menu__item__t*)&menu_builder__menu[2], .parrent = (menu__item__t*)menu_builder__menu}, // Выход из меню
};


static events__e menu_builder__param_load();

/*******************************************************************************
 * Создание меню.
 ******************************************************************************/
menu__item__t* menu_builder__create() {
    menu_builder__param_load();
    uint8_t ver[] = SW_VERSION;
    menu_builder__param_tbl[MENU_PARAM__VERSION].value = (ver[0] << 24) | (ver[1] << 16) | *(uint16_t*)&ver[2];
    return (menu__item__t*)menu_builder__menu;
}


int menu_builder__param_get(menu_bulder__params_e param) {
    if (param < MENU_PARAM__NONE)
        return menu_builder__param_tbl[param].value;
    return 0;
}

void menu_builder__param_set(menu_bulder__params_e param, int value) {
    if ((param < MENU_PARAM__NONE) && (value >= menu_builder__param_tbl[param].min) && (value <= menu_builder__param_tbl[param].max))
        menu_builder__param_tbl[param].value = value;
}

int menu_builder__p_min_get(menu_bulder__params_e param) {
    if (param < MENU_PARAM__NONE)
        return menu_builder__param_tbl[param].min;
    return 0;
}

int menu_builder__p_max_get(menu_bulder__params_e param) {
    if (param < MENU_PARAM__NONE)
        return menu_builder__param_tbl[param].max;
    return 0;
}

events__e menu_builder__p2str(menu__item__t* item, char* buff, int len) {
    menu_builder__struct_t* p_data = item->data;
    buff[0] = 0x00;
    if ((len < 9) || (!p_data)) {
        return EVENT__PARAM_NA;
    }
    switch(p_data->type) {
    case MENU_BUILDER__TYPE_INT:
        sprintf(buff, "%d", p_data->value);
        break;
    case MENU_BUILDER__TYPE_FLOAT_10:
        sprintf(buff, "% 3.1f", (float)p_data->value / 10);
        break;
    case MENU_BUILDER__TYPE_VER:
        sprintf(buff, "%d.%d.%d", p_data->value >> 24, (p_data->value >> 16 & 0xFF), (p_data->value & 0xFFFF));
        break;
    case MENU_BUILDER__TYPE_MB_MODE:
        sprintf(buff, "%s", menu_builder__mode_tbl[p_data->value]);
        break;
    case MENU_BUILDER__TYPE_AKB:
        sprintf(buff, "%s", menu_builder__akb_tbl[p_data->value]);
        break;
    case MENU_BUILDER__TYPE_TERMO:
        sprintf(buff, "%s", menu_builder__termo_tbl[p_data->value]);
        break;
    default:
        return EVENT__PARAM_NA;
    }
    return EVENT__OK;
}

menu_bulder__params_e menu_builder__item2id(menu__item__t* item) {
    menu_builder__struct_t* p_data = item->data;
    return (p_data) ? (menu_bulder__params_e)(p_data - menu_builder__param_tbl) : MENU_PARAM__NONE;
}


events__e menu_builder__param_save(menu_bulder__params_e param) {
    events__e res = EVENT__OK;
    if (param >= MENU_PARAM__NONE)
        return EVENT__PARAM_NA;
    res = dev_setts__set(0, param, (uint8_t*)&menu_builder__param_tbl[param].value, 4);
    return res;
}


static events__e menu_builder__param_load() {
    uint32_t value;
    for(menu_bulder__params_e param = MENU_PARAM__VERSION; param < MENU_PARAM__NONE; param++) {
        if (dev_setts__get(0, param, (uint8_t*)&value))
            menu_builder__param_tbl[param].value = value;
    }
    return EVENT__OK;
}

