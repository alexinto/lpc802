﻿/***************************************************************************//**
 * @file sync.c.
 * @brief Модуль управления источником питания.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include <math.h>
#include "System/sw_timer.h"
#include "DRV/sync.h"
#include "MODBUS/mb_rtu.h"
#include "DRV/sync_reg.h"
#include "encoder.h"
#include "fsm_mode.h"
#include "DRV/display.h"

#ifndef SYNC__CNT
#define SYNC__CNT 1
#endif

typedef enum {
    SYNC__OP_NONE     = 0,
    SYNC__OP_INIT,
    SYNC__OP_RD_AI,
    SYNC__OP_WR_ENC,
    SYNC__OP_WR_AI,
    SYNC__OP_WR_V,
    SYNC__OP_WR_I,
    SYNC__OP_WR_ON,
    SYNC__OP_WR_ERR,
    SYNC__OP_CHK_AI,
}sync__oper_e;

typedef struct {
    uint8_t mb_psu_id;
    uint8_t mb_id;
    uint8_t addr;
    uint16_t voltage;
    uint16_t current;
    sync__oper_e* oper;
    sync__cb_t cb;
    void* ext_data;
    sw_timer__t timer;
}sync__struct_t;

static const sync_reg__ax_e reg_cmp_tbl[] = {SYNC_REG__AX_VOLTAGE, SYNC_REG__AX_CURRENT, SYNC_REG__AX_START, SYNC_REG__AX_MAX};

static const sync__oper_e* sync_script_tbl[] = {
    [SYNC__MODE_NONE]      = (sync__oper_e[]){SYNC__OP_NONE},
    [SYNC__MODE_SLAVE]     = (sync__oper_e[]){SYNC__OP_WR_ENC, SYNC__OP_WR_AI, SYNC__OP_NONE},
    [SYNC__MODE_MASTER]    = (sync__oper_e[]){SYNC__OP_RD_AI, SYNC__OP_WR_V, SYNC__OP_WR_I, SYNC__OP_WR_ON, SYNC__OP_WR_ERR, SYNC__OP_CHK_AI, SYNC__OP_NONE},
};

static sync__struct_t sync__data[SYNC__CNT];

static events__e sync__script_exec(sync__struct_t* d);
static void sync__mb_cb(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data);
static void sync__tmr_cb(struct sw_timer__t *timer, void *ext_data);

events__e sync__init(int id, uint8_t mb_id, uint8_t sync_addr) {
    sync__struct_t* d = &sync__data[id];
    if ((id >= SYNC__CNT) || (d->oper))
        return EVENT__BUSY;
    d->mb_id = mb_id;
    d->addr = sync_addr;
    return EVENT__OK;
}

events__e sync__start(int id, sync__mode_e mode,  sync__cb_t cb, void* ext_data) {
    sync__struct_t* d = &sync__data[id];
    if ((id >= SYNC__CNT) || (mode == SYNC__MODE_NONE))
        return EVENT__PARAM_NA;
    if (d->oper)
        return EVENT__BUSY;
    d->oper = (sync__oper_e*)sync_script_tbl[mode];
    d->cb = cb;
    d->ext_data = ext_data;
    mb_rtu__cmd(d->mb_id, MB_RTU__CMD_START);
    return sync__script_exec(d);
}

events__e sync__stop(int id) {
    if (id >= SYNC__CNT)
        return EVENT__PARAM_NA;
    sync__struct_t* d = &sync__data[id];
    mb_rtu__cmd(d->mb_id, MB_RTU__CMD_STOP);
    mb_reg__set(d->mb_id, MB_REG__TYPE_AI, SYNC_REG__AX_VOLTAGE, encoder__get(ENCODER__VOLTAGE));
    mb_reg__set(d->mb_id, MB_REG__TYPE_AI, SYNC_REG__AX_CURRENT, encoder__get(ENCODER__CURRENT));
    mb_reg__set(d->mb_id, MB_REG__TYPE_AI, SYNC_REG__AX_START, fsm_mode__state_get() == FSM_STATE__ON ? 1 : 0);
    return EVENT__OK;
}

static void sync__mb_cb(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data) {
    sync__struct_t* d = (sync__struct_t*)ext_data;
    if ((event == EVENT__OK) && (d->oper)) {
        d->oper++;
        event = sync__script_exec(d);
    }
    if ((event != EVENT__OK) || (!d->oper)) {
        d->oper = NULL;
        if (d->cb)
            d->cb(d - sync__data, event, d->ext_data);
    }
}




static events__e sync__script_exec(sync__struct_t* d) {
    sync_reg__ax_e* p_reg = (sync_reg__ax_e*)reg_cmp_tbl;
    uint16_t data, data_ref;
    events__e event = EVENT__OK;
    switch(*d->oper) {
    case SYNC__OP_RD_AI:
        event = mb_rtu__request(d->mb_id, d->addr, MB_RTU__OP_RD_AI, SYNC_REG__AX_START, SYNC_REG__AX_MAX - SYNC_REG__AX_START, sync__mb_cb, (void*)d);
        break;
    case SYNC__OP_WR_ENC:
        mb_reg__get(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_VOLTAGE, &data);
        encoder__set(ENCODER__VOLTAGE, data);
        mb_reg__get(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_CURRENT, &data);
        encoder__set(ENCODER__CURRENT, data);
        mb_reg__get(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_START, &data);
        switch (fsm_mode__state_get()) {
        case FSM_STATE__ON:
            if (!data)
                fsm_mode__cmd(FSM_MODE__CMD_OFF);
            break;
        case FSM_STATE__END:
        case FSM_STATE__OFF:
            if (data)
                fsm_mode__cmd(FSM_MODE__CMD_ON);
            break;
        default:
            mb_reg__get(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_ERROR, &data_ref);
            if (data_ref) {
                mb_reg__set(d->mb_id, MB_REG__TYPE_AI, SYNC_REG__AX_ERROR, 0);
                fsm_mode__cmd(data ? FSM_MODE__CMD_ON : FSM_MODE__CMD_OFF);
            }
            else
                mb_reg__set(d->mb_id, MB_REG__TYPE_AI, SYNC_REG__AX_ERROR, 1);
            break;
        }
        sw_timer__start(&d->timer, 1, sync__tmr_cb,(void*)d);
        break;
    case SYNC__OP_WR_AI:
        mb_reg__set(d->mb_id, MB_REG__TYPE_AI, SYNC_REG__AX_VOLTAGE, encoder__get(ENCODER__VOLTAGE));
        mb_reg__set(d->mb_id, MB_REG__TYPE_AI, SYNC_REG__AX_CURRENT, encoder__get(ENCODER__CURRENT));
        mb_reg__set(d->mb_id, MB_REG__TYPE_AI, SYNC_REG__AX_START, fsm_mode__state_get() == FSM_STATE__ON ? 1 : 0);
        sw_timer__start(&d->timer, 1, sync__tmr_cb,(void*)d);
        break;
    case SYNC__OP_WR_V:
        mb_reg__set(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_VOLTAGE, encoder__get(ENCODER__VOLTAGE));
        event = mb_rtu__request(d->mb_id, d->addr, MB_RTU__OP_WR_AO, SYNC_REG__AX_VOLTAGE, 1, sync__mb_cb, (void*)d);
        break;
    case SYNC__OP_WR_I:
        mb_reg__set(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_CURRENT, encoder__get(ENCODER__CURRENT));
        event = mb_rtu__request(d->mb_id, d->addr, MB_RTU__OP_WR_AO, SYNC_REG__AX_CURRENT, 1, sync__mb_cb, (void*)d);
        break;
    case SYNC__OP_WR_ON:
        switch (fsm_mode__state_get()) {
        case FSM_STATE__ON:
            mb_reg__set(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_ERROR, 0);
            mb_reg__set(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_START, 1);
            break;
        case FSM_STATE__END:
        case FSM_STATE__OFF:
            mb_reg__set(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_ERROR, 0);
            mb_reg__set(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_START, 0);
            break;
        default:
            mb_reg__set(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_ERROR, 1);
            mb_reg__set(d->mb_id, MB_REG__TYPE_AO, SYNC_REG__AX_START, 0);
            break;
        }
        event = mb_rtu__request(d->mb_id, d->addr, MB_RTU__OP_WR_AO, SYNC_REG__AX_START, 1, sync__mb_cb, (void*)d);
        break;
    case SYNC__OP_WR_ERR:
        event = mb_rtu__request(d->mb_id, d->addr, MB_RTU__OP_WR_AO, SYNC_REG__AX_ERROR, 1, sync__mb_cb, (void*)d);
        break;
    case SYNC__OP_CHK_AI:
        while(*p_reg != SYNC_REG__AX_MAX) {
            mb_reg__get(d->mb_id, MB_REG__TYPE_AI, *p_reg, &data);
            mb_reg__get(d->mb_id, MB_REG__TYPE_AO, *p_reg, &data_ref);
            p_reg++;
            if (data == data_ref)
                continue;
            event = EVENT__ERROR;
            break;
        }
        if (event == EVENT__OK)
            sw_timer__start(&d->timer, 1, sync__tmr_cb,(void*)d);
        break;
    case SYNC__OP_NONE:
    default:
        d->oper = NULL;
        break;
    }
    if (event != EVENT__OK)
        d->oper = NULL;
    return event;
}


static void sync__tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    sync__struct_t* d = (sync__struct_t*)ext_data;
    events__e event = EVENT__OK;
    if (d->oper) {
        d->oper++;
        event = sync__script_exec(d);
    }
    if ((event != EVENT__OK) || (!d->oper)) {
        d->oper = NULL;
        if (d->cb)
            d->cb(d - sync__data, event, d->ext_data);
    }
}





