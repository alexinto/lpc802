﻿/***************************************************************************//**
* @file test_hw_spi.c.
* @brief Тест supervisor для stm32l4.
* @authors a.tushentsov.
******************************************************************************/
#include "target.h"
#include "System/framework.h"
#include "mcu.h"
#include "DRV/spi.h"
#include "DRV/gpio.h"

// Структура интерфейса SPI.
typedef struct {
    u32 bytes_count;                                    // Счетчик байтов.
    u8 init;                                            // Флаг инициализации интерфейса.
    u8 cs_id;
    events__e event;
    uint32_t settings;                                  // Текущие настройки.
    uint8_t *tx_buff;                                   // Указатель на буфер с пользовательскими данными для передачи.
    uint8_t *rx_buff;                                   // Указатель на буфер с пользовательскими данными для приема.
    u32 tx_len;                                         // Размер передающего буфера, байт.
    u32 rx_len;                                         // Размер приемного буфера, байт.
    hw_spi__cb_t cb;                                    // пользовательский коллбэк.
    void* ext_data;
} hw_spi__itf_t;


// Структура массивов адресов SPI.
const SPI_TypeDef* const spi__mass[] = {
    SPI1,
    SPI2,
    SPI3,
};

#define HW_SPI__COUNT (sizeof(spi__mass) / sizeof(spi__mass[0]))

static framework__sub_t cout_sub;
static u8 module__init;

// Макрос для получения указателя на структуру SPI по ID.
static hw_spi__itf_t itf[HW_SPI__COUNT];




static void hw_spi__itf_en(int spi_id);
static void hw_spi__itf_dis(int spi_id);
static events__e spi_init_regs(int spi_id);
static events__e hw_spi__init (int spi_id, uint32_t settings);
static events__e hw_spi__txrx(u8 spi_id);
static events__e hw_spi__break(int spi_id);
static void hw_spi__cout(void);


events__e hw_spi__func(hw_spi__cmd_e cmd, u8 spi_id, u8 cs_id, u32 sett_wr, u32 sett_rd, u8 *rx_buff, u32 rx_len, u8 *tx_buff, u32 tx_len, hw_spi__cb_t callback, void* ext_data) {
    events__e res = EVENT__PARAM_NA;
    if (spi_id >= HW_SPI__COUNT)
        return res;
    hw_spi__itf_t *spi = &itf[spi_id];
    if (spi->event != EVENT__OPEN)
        return EVENT__BUSY;
    spi->event = EVENT__LOCK;
    spi->bytes_count = 0;
    spi->cs_id = cs_id;
    spi->rx_buff = rx_buff;
    spi->tx_buff = tx_buff;
    spi->rx_len = rx_len;
    spi->tx_len = tx_len;
    spi->cb = callback;
    spi->ext_data = ext_data;
    if (cs_id != MCU__GPIO_NONE)
        gpio__set(0, cs_id, GPIO__STATE_LOW);
    switch(cmd) {
        case HW_SPI__CMD_INIT:
            if (!module__init) {
                module__init = 1;
                framework__cout_subscribe(&cout_sub, hw_spi__cout);
            }
            if (cs_id != MCU__GPIO_NONE)
                gpio__init(0, cs_id, GPIO__DIR_OUT, NULL);
            res = hw_spi__init(spi_id, sett_wr);
            break;
        case HW_SPI__CMD_TXRX:
            if (rx_buff != NULL || tx_buff != NULL) {
                res = hw_spi__txrx(spi_id);
            }
            break;
        case HW_SPI__CMD_BREAK:
            res = hw_spi__break(spi_id);
            break;
        case HW_SPI__CMD_DEINIT:
            break;
        default:
            break;
    }
    if ((res != EVENT__OK) || (callback == NULL)) {
        if (cs_id != MCU__GPIO_NONE)
            gpio__set(0, cs_id, GPIO__STATE_HIGH);
        spi->event = EVENT__OPEN;
    }
    return res;
}


static events__e spi_init_regs(int spi_id) {
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id];
    hw_spi__itf_t *spi = &itf[spi_id];
    SPI->CR1 &=~  SPI_CR1_BR;
    switch (spi->settings & SPI__BR_MASK) {
        case SPI__BR_LOW_SPD:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_1| SPI_CR1_BR_0;
        break;
        case SPI__BR_375_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_1;
        break;
        case SPI__BR_750_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_0;
        break;
        case SPI__BR_1500_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2;
        break;
        case SPI__BR_3_MBIT:
            SPI->CR1 |= SPI_CR1_BR_1| SPI_CR1_BR_0;
        break;
        case SPI__BR_6_MBIT:
            SPI->CR1 |= SPI_CR1_BR_1;
        break;
        case SPI__BR_12_MBIT:
            SPI->CR1 |= SPI_CR1_BR_0;
        break;
        case SPI__BR_24_MBIT:
        case SPI__BR_HIGH_SPD:
        break;
        default:
        return EVENT__PARAM_NA;
    }

//PHASE - POL
    SPI->CR1 &=~ (SPI_CR1_CPHA | SPI_CR1_CPOL);
    switch (spi->settings & SPI__CPHA_CPOL_MASK) {
        case SPI__CPHA_0_CPOL_0:
            break;
        case SPI__CPHA_0_CPOL_1:
            SPI->CR1 |= SPI_CR1_CPOL;
            break;
        case SPI__CPHA_1_CPOL_0:
            SPI->CR1 |= SPI_CR1_CPHA;
            break;
        case SPI__CPHA_1_CPOL_1:
            SPI->CR1 |= SPI_CR1_CPHA | SPI_CR1_CPOL;
            break;
        default:
            return EVENT__PARAM_NA;
    }
//FRAME
    switch (spi->settings & SPI__FRAME_MASK) {
        case SPI__DATA_FRAME_8_BIT:
            SPI->CR2 &=~ SPI_CR2_DS;
            break;
        case SPI__DATA_FRAME_16_BIT:
            SPI->CR2 |= SPI_CR2_DS;
            break;
        default:
            return EVENT__PARAM_NA;
    }
//LSB - MSB
    switch (spi->settings & SPI__LSBMSB_MASK) {
        case SPI__MSB:
            SPI->CR1 &=~ SPI_CR1_LSBFIRST;
            break;
        case SPI__LSB:
            SPI->CR1 |= SPI_CR1_LSBFIRST;
            break;
        default:
            return EVENT__PARAM_NA;
    }
//Software NSS + internal slave select
    SPI->CR1 |= SPI_CR1_SSM | SPI_CR1_SSI;
    SPI->CR2 |= SPI_CR2_NSSP | SPI_CR2_FRXTH;
// MOTOROLLA protocol
    SPI->CR2 &=~ SPI_CR2_FRF;

//MASTER - SLAVE
    switch (spi->settings & SPI__MODE_MASK) {
        case SPI__MODE_MASTER:
            SPI->CR1 |= SPI_CR1_MSTR;
            break;
        case SPI__MODE_SLAVE:       // slave не реализован
        default:
            return EVENT__PARAM_NA;
            break;
    }
    return EVENT__OK;
}

/*******************************************************************************
 *  Функция инициализации  SPIx.
 ******************************************************************************/
static events__e hw_spi__init (int spi_id, uint32_t settings) {
/* (+) 1. Включить порты на которых сидит SPI
    (+) 2. Включить тактирование SPI
    (+) 3. Установить скороть (прескейлер BR)
    (+) 4. Выбрать полярность и фазу (POL/PHA)
    (+) 5. Выбрать формат данных 8/16 бит
    (+) 6. Выбрать MSB/LSB
    (+) 7. Настроить NSS/CS (Регистры SSM/SSI)
    (+) 8. Настроить TI протокол (FRF бит)
    (+) 9. Настроить MASTER
    (+) 10. Включить SPI (SPE)
    (+) 11. Проверить бит MODF в SR регистре */
    hw_spi__itf_en(spi_id);
    hw_spi__itf_t *spi = &itf[spi_id];
    spi->settings = settings;
    spi->event = EVENT__OPEN;
    return EVENT__OK;
}

/***************************************************************************//**
 * Функция прекращения выполнения операции передачи данных указанным SPI.
 ******************************************************************************/
static events__e hw_spi__break(int spi_id) {
    CRITICAL_SECTION_ON
    switch(spi_id) {
    case SPI_1:
        RCC->APB2RSTR |= RCC_APB2RSTR_SPI1RST;
        RCC->APB2RSTR &= ~RCC_APB2RSTR_SPI1RST;
        break;
    case SPI_2:
        RCC->APB1RSTR |=  RCC_APB1RSTR_SPI2RST;
        RCC->APB1RSTR &=  ~RCC_APB1RSTR_SPI2RST;
        break;
    case SPI_3:
        RCC->APB1RSTR |=  RCC_APB1RSTR_SPI3RST;
        RCC->APB1RSTR &=  ~RCC_APB1RSTR_SPI3RST;
        break;
    }
    CRITICAL_SECTION_OFF
    return EVENT__OK;
}

/***************************************************************************//**
 * Функция деинициализации указанного SPI. Прекращает выполнение всех операций и отключает интерфейс.
 ******************************************************************************/
events__e hw_spi__deinit(int spi_id) {
    hw_spi__itf_t *spi = &itf[spi_id];
    hw_spi__break(spi_id);
    spi->init = 0;
    hw_spi__itf_dis(spi_id);
    return EVENT__OK;
}


/***************************************************************************//**
 * Инициализация обмена по SPI.
 ******************************************************************************/
static events__e hw_spi__txrx(u8 spi_id) {
    events__e res;
    /* Сначала настраиваем передачу -> потом прием.
    Последовательность передачи:
    (+) 1. Подали тактирование на Приемник (предварительно CS -> low (это должен сделать spi.c)), включили прерывания
    (+) 2. Загрузить DR данные в регистр
    (+) 3. В процессе прередачи флаг TXE будет выставлен
    (+) 4. Будет сгенерировано прерывание когда буфер на передачу освободится (Прерывание по TXEIE)

     Последовательность приема:
    (+) 1. Дожидаемся заполнения буфера RX(ждем флага RXNE)
    (+) 2. Ждем прерывания от RXNE
    (+) 3. Читаем регистр DR, тем самым очищаем флаг RXNE */
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id];
    hw_spi__itf_t *spi = &itf[spi_id];

    if ((res = spi_init_regs(spi_id)) != EVENT__OK)
        return res;

    SPI->DR;
    SPI->CR2 |= SPI_CR2_RXNEIE | SPI_CR2_ERRIE;
    *(uint8_t*)&SPI->DR = (spi->tx_buff) ? *spi->tx_buff : 0xFF;
    //Включаем SPI
    SPI->CR1 |= SPI_CR1_SPE;
    //Проверка на ошибку установки выбранного режима
    if ((SPI->SR & SPI_SR_MODF) != 0) {
        SPI->CR1 = 0;
        return EVENT__ERROR;
    }


    return res;
}

/***************************************************************************//**
 * Функция обработки текущего состояния драйвера SPI.
 ******************************************************************************/
static void hw_spi__cout(void) {
    hw_spi__itf_t *spi;
    events__e event;
    for(u8 i = 0; i < HW_SPI__COUNT; i++) {
        spi = &itf[i];
        if ((spi->event == EVENT__OPEN) || (spi->event == EVENT__LOCK))
            continue;
        event = spi->event;
        if (spi->cs_id != MCU__GPIO_NONE)
            gpio__set(0, spi->cs_id, GPIO__STATE_HIGH);
        spi->event = EVENT__OPEN;
        if (spi->cb)
            spi->cb(i, spi->cs_id, event, spi->rx_buff ? spi->rx_buff : spi->tx_buff, spi->bytes_count, spi->ext_data);
    }
}

/*******************************************************************************
 *  Функция-обработчик прерывай по SPI.
 ******************************************************************************/
void hw_spi__SPI_IRQHandler(int spi_id) {
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id];
    hw_spi__itf_t *spi = &itf[spi_id];
    int status_reg = SPI->SR;
    uint8_t data = SPI->DR;//*(uint8_t*)SPI->DR;
    // Проверка на ошибки
    if (status_reg & SPI_SR_OVR) {
            SPI->CR2 &= ~(SPI_CR2_RXNEIE | SPI_CR2_ERRIE);
//        hw_spi__break(spi_id);
            spi->event = EVENT__NO_MEM;
    }
    else {
        if ((spi->rx_buff) && (spi->bytes_count < spi->rx_len))
            spi->rx_buff[spi->bytes_count] = data;
        spi->bytes_count++;
        if ((spi->tx_buff) && (spi->bytes_count < spi->tx_len))
            data = spi->tx_buff[spi->bytes_count];
        else
            data = 0xFF;
        if ((spi->bytes_count < spi->rx_len) || (spi->bytes_count < spi->tx_len))
            *(uint8_t*)&SPI->DR = data;
        else {
            SPI->CR2 &= ~(SPI_CR2_RXNEIE | SPI_CR2_ERRIE);
            spi->event = EVENT__OK;
        }
     }
}

/*******************************************************************************
 *  Функция инициализации интерфейсов SPI.
 ******************************************************************************/
typedef struct {
    int gpio[3];
    uint32_t* spi_clock;
    int spi_clock_flag;
    int spi_irq;
    int spi_af_num;
}hw_spi_desc__t;


static const hw_spi_desc__t hw_spi_desc[] = {
//          CLK               MOSI            MISO
     { MCU__GPIO_P_B_3, MCU__GPIO_P_B_5, MCU__GPIO_P_B_4, (uint32_t*)&RCC->APB2ENR, RCC_APB2ENR_SPI1EN, SPI1_IRQn, 5 },
     { MCU__GPIO_NONE,  MCU__GPIO_NONE,  MCU__GPIO_NONE, (uint32_t*)&RCC->APB1ENR, RCC_APB1ENR_SPI2EN, SPI2_IRQn, 5 },
     { MCU__GPIO_P_C_10, MCU__GPIO_P_C_12, MCU__GPIO_P_C_12, (uint32_t*)&RCC->APB1ENR, RCC_APB1ENR_SPI3EN, SPI3_IRQn, 6 },
};

static void hw_spi__itf_en(int spi_id) {
    for(int spi_param_desc_cnt = 0; spi_param_desc_cnt < 3; spi_param_desc_cnt++)
        gpio__af_init(0, hw_spi_desc[spi_id].gpio[spi_param_desc_cnt], GPIO__MODE_PP, hw_spi_desc[spi_id].spi_af_num);
    *hw_spi_desc[spi_id].spi_clock |= hw_spi_desc[spi_id].spi_clock_flag;
    NVIC_SetPriority((IRQn_Type)hw_spi_desc[spi_id].spi_irq, 3);
    NVIC_EnableIRQ((IRQn_Type)hw_spi_desc[spi_id].spi_irq);
}
/*******************************************************************************
 *  Функция деинициализации интерфейсов SPI.
 ******************************************************************************/
static void hw_spi__itf_dis(int spi_id) {
    NVIC_DisableIRQ((IRQn_Type)hw_spi_desc[spi_id].spi_irq);
    *hw_spi_desc[spi_id].spi_clock &= ~hw_spi_desc[spi_id].spi_clock_flag;
    for(int spi_param_desc_cnt = 0; spi_param_desc_cnt < 3; spi_param_desc_cnt++)
        gpio__init(0, hw_spi_desc[spi_id].gpio[spi_param_desc_cnt],  GPIO__DIR_IN, NULL);
}
