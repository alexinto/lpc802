/***************************************************************************//**
 * @file sw_encoder.c.
 * @brief ������ ������ � ���������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "mcu.h"
#include "System/framework.h"
#include "encoder.h"

#ifndef SW_ENCODER___PIN_FILTER
#define SW_ENCODER___PIN_FILTER      4       // ������ �� ���� � �����(2..6)
#endif
#ifndef SW_ENCODER__FREQ
#define SW_ENCODER__FREQ             2000    // ������� �������������, ��
#endif

typedef struct {
    u8 button;
    u8 sw1;
    u8 sw2;
}encoder__hw_pin_e;

typedef struct {
    uint8_t init_pin   :1;
    uint8_t lock       :1;
    uint8_t reserved   :6;
}encoder__flags_t;

typedef struct {
    u8 cur_pin[3];
    u32 pin_cnt;
    u8 pin_mask;
    u8 button;
    u8 cur_front;
    int value, mux, min, max;
    events__e event;
    encoder__hw_pin_e hw_pins;
    void* ext_data;
    encoder__cb_t cb;
    encoder__flags_t flags;
}encoder__struct_t;

static framework__sub_t cout_sub;
static encoder__struct_t encoder__data[enc_none] = {{.event = EVENT__WAIT, .min = 0, .max = 2147483647, .hw_pins = {MCU__GPIO_P_E_6, MCU__GPIO_P_D_12, MCU__GPIO_P_E_1}},
                                                    {.event = EVENT__WAIT, .min = 0, .max = 2147483647, .hw_pins = {MCU__GPIO_P_E_7, MCU__GPIO_P_A_15, MCU__GPIO_P_B_3}},
                                                    {.event = EVENT__WAIT, .min = 0, .max = 2147483647, .hw_pins = {MCU__GPIO_P_E_8, MCU__GPIO_P_B_0, MCU__GPIO_P_B_1}},
};

static void encoder__cout();
static events__e encoder__check(encoder__struct_t* enc_ptr, uint8_t ch);
static uint8_t pin_state_get(u8 buff);

/*******************************************************************************
 * ������� ������������� ��������. ������� �������� ����.
 ******************************************************************************/
void encoder__init(int enc, int min, int max) {
    if (enc >= enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    enc_ptr->min = min;
    enc_ptr->max = max;
    encoder__set(enc, enc_ptr->value);
    if (!cout_sub.cout) {
        framework__cout_subscribe(&cout_sub, encoder__cout);
        NVIC_DisableIRQ(LPTIM1_IRQn);
        MODIFY_REG(RCC->DCKCFGR2, RCC_DCKCFGR2_LPTIM1SEL_Msk, RCC_DCKCFGR2_LPTIM1SEL_1); // HSI clock
        RCC->APB1ENR |= RCC_APB1ENR_LPTIM1EN;
        LPTIM1->IER = 2;
        LPTIM1->CR = 1;
        LPTIM1->ARR = 16000000 / SW_ENCODER__FREQ;
        LPTIM1->ICR = 0x7F;
        LPTIM1->CR = 1;
        LPTIM1->CR = 5;
        NVIC_SetPriority(LPTIM1_IRQn, 0);
        NVIC_EnableIRQ(LPTIM1_IRQn);
    }
    if (!enc_ptr->flags.init_pin) {
        enc_ptr->flags.init_pin = 1;
        gpio__init(0, enc_ptr->hw_pins.sw1, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
        gpio__init(0, enc_ptr->hw_pins.sw2, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
        gpio__init(0, enc_ptr->hw_pins.button, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
    }
}

events__e encoder__lock(int enc) {
    if (enc >= enc_none)
        return EVENT__PARAM_NA;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    enc_ptr->flags.lock = 1;
    return EVENT__OK;
}

events__e encoder__unlock(int enc) {
    if (enc >= enc_none)
        return EVENT__PARAM_NA;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    enc_ptr->flags.lock = 0;
    return EVENT__OK;
}


/*******************************************************************************
 * ������� ������� �������� � ��������� �� ����� ������ �� �������� ����� �����.
 ******************************************************************************/
events__e encoder__start(int enc, int ticks, encoder__cb_t cb, void* ext_data) {
    if (enc >= enc_none)
        return EVENT__PARAM_NA;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    CRITICAL_SECTION_ON
    enc_ptr->ext_data = ext_data;
    enc_ptr->cb = cb;
    enc_ptr->event = EVENT__WAIT;
    CRITICAL_SECTION_OFF
    return EVENT__OK;
}

/*******************************************************************************
 * ������� ��������� ������ �������� �������� ��������. ������� ���������� ����.
 ******************************************************************************/
void encoder__stop(int enc) {
    if (enc >= enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    enc_ptr->cb = NULL;
}

/*******************************************************************************
 * ������� ��������� �������� ��������.
 ******************************************************************************/
void encoder__set(int enc, int value) {
    if (enc >= enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    CRITICAL_SECTION_ON
    if (value > enc_ptr->max)
        enc_ptr->value = enc_ptr->max;
    else if (value < enc_ptr->min)
        enc_ptr->value = enc_ptr->min;
    else
        enc_ptr->value = value;
    enc_ptr->cur_pin[0] =enc_ptr->cur_pin[1] = enc_ptr->cur_pin[2] = 0;
    enc_ptr->pin_cnt = 0;
    CRITICAL_SECTION_OFF
}

/*******************************************************************************
 * ������� ��������������� ��������. ������� ������������� ����.
 ******************************************************************************/
void encoder__deinit(int enc) {
    if (enc >= enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    CRITICAL_SECTION_ON
    gpio__init(0, enc_ptr->hw_pins.sw1, GPIO__DIR_IN, NULL);
    gpio__init(0, enc_ptr->hw_pins.sw2, GPIO__DIR_IN, NULL);
    enc_ptr->cb = NULL;
    enc_ptr->flags.init_pin = 0;
    CRITICAL_SECTION_OFF
}

/*******************************************************************************
 * ������� ��������� �������� �������� ��������.
 ******************************************************************************/
int encoder__get(int enc) {
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    return enc_ptr->value;
}

/*******************************************************************************
 * ������� ��������� ��������� �������� ��������.
 ******************************************************************************/
void encoder__set_mux(int enc, int mux) {
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    enc_ptr->mux = mux;
}


/*******************************************************************************
 * �������- ���������� �������� ��������� ������ ��������.
 ******************************************************************************/
static void encoder__cout() {
    events__e res;
    for(u8 i =0; i < enc_none; i++) {
        encoder__struct_t* enc_ptr = &encoder__data[i];
        if (enc_ptr->event != EVENT__WAIT) {
            res = enc_ptr->event;
            enc_ptr->event = EVENT__WAIT;
            if (enc_ptr->cb)
                enc_ptr->cb(i, res, enc_ptr->value, enc_ptr->ext_data);
        }
    }
}


static events__e encoder__check(encoder__struct_t* enc_ptr, uint8_t ch) {
    events__e res = EVENT__ERROR;
    if (enc_ptr->cb == NULL)
        return res;
    uint8_t code = ((enc_ptr->pin_mask >> 1) & 0x3) | ((enc_ptr->pin_mask >> 3) & 0xC);
    switch (code) {
    case 0x6:
    case 0x9:
        enc_ptr->value += enc_ptr->mux ? enc_ptr->mux : 1;
        res = EVENT__UP;
        break;
    case 0x5:
    case 0xA:
        enc_ptr->value -= enc_ptr->mux ? enc_ptr->mux : 1;
        res = EVENT__DOWN;
        break;
    default:
        enc_ptr->pin_mask = 0;
        break;
    }
    if (enc_ptr->value > enc_ptr->max)
        enc_ptr->value = enc_ptr->max;
    else if (enc_ptr->value < enc_ptr->min)
        enc_ptr->value = enc_ptr->min;
    return res;
}


/*******************************************************************************
 * ���������� ISR ��������.
 ******************************************************************************/
void encoder__IRQ_handler(int enc) {
    events__e event;
    uint8_t cur_state[2];
    LPTIM1->ICR = 0x7F;
    encoder__struct_t* enc_ptr;
    for (enc = 0; enc < enc_none; enc++) {
        enc_ptr = &encoder__data[enc];
        if (enc_ptr->flags.lock)
            continue;
        enc_ptr->cur_pin[0] |= gpio__get(0, enc_ptr->hw_pins.sw1) << enc_ptr->pin_cnt;
        enc_ptr->cur_pin[1] |= gpio__get(0, enc_ptr->hw_pins.sw2) << enc_ptr->pin_cnt;
        enc_ptr->cur_pin[2] |= gpio__get(0, enc_ptr->hw_pins.button) << enc_ptr->pin_cnt;
        if (++enc_ptr->pin_cnt >= SW_ENCODER___PIN_FILTER) {
            cur_state[0] = pin_state_get(enc_ptr->cur_pin[0]);
            cur_state[1] = pin_state_get(enc_ptr->cur_pin[1]);
            if (((enc_ptr->pin_mask >> (7 - enc_ptr->cur_front)) & 0x01) != cur_state[0]) {
                enc_ptr->pin_mask |= cur_state[1] << (3 - enc_ptr->cur_front);
                if (cur_state[0]) {
                    enc_ptr->pin_mask |= 1 << (7 - enc_ptr->cur_front);
                }
                else {
                    enc_ptr->pin_mask &= ~(1 << (7 - enc_ptr->cur_front));
                }

                if (++enc_ptr->cur_front > 2) {
                    event = encoder__check(enc_ptr, 0);
                    enc_ptr->cur_front = enc_ptr->pin_mask = 0;
                    if ((event != EVENT__ERROR) && (enc_ptr->event == EVENT__WAIT))
                        enc_ptr->event = event;
                }
                else {
                    if (cur_state[0])
                        enc_ptr->pin_mask |= 1 << (7 - enc_ptr->cur_front);
                    else
                        enc_ptr->pin_mask &= ~(1 << (7 - enc_ptr->cur_front));
                }

            }
            cur_state[0] = pin_state_get(enc_ptr->cur_pin[2]);        // ������
            if ((enc_ptr->button != cur_state[0]) && (enc_ptr->event == EVENT__WAIT)) {
                enc_ptr->event = cur_state[0] ? EVENT__BUTTON_RELEASE : EVENT__BUTTON_PRESS;
            }
            enc_ptr->button = cur_state[0];
            enc_ptr->cur_pin[0] = enc_ptr->cur_pin[1] = enc_ptr->cur_pin[2] = 0;
            enc_ptr->pin_cnt = 0;
        }
    }
}

static uint8_t pin_state_get(u8 buff) {
    uint8_t one_cnt = 0;
    for(int i = 0; i < SW_ENCODER___PIN_FILTER; i++)
        if (buff & (1 << i))
            one_cnt++;
    return one_cnt > (SW_ENCODER___PIN_FILTER >> 1) ? 1 : 0;
}



