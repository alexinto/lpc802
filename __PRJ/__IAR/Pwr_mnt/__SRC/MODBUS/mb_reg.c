/***************************************************************************//**
 * @file mb_reg.c.
 * @brief ���� � �������� ��������� Modbus.
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "MODBUS/mb_reg.h"
#include "DRV/psu_reg.h"
#include "DRV/sync_reg.h"

#define MB_REG__END_ADDR 0xFFFF

// ������� ��������� ����� �������
static mb_reg__struct_t* mb_reg__psu_tbl[] = {
    [MB_REG__TYPE_DO] = (mb_reg__struct_t[]){
        {PSU_REG__DO_OUT_SW, 0},
        {PSU_REG__DO_LOCK, 0},
        {PSU_REG__DO_OVP, 0},
        {PSU_REG__DO_OCP, 0},
        {PSU_REG__DO_PWRON, 0},

        {MB_REG__END_ADDR, 0}
    },
    [MB_REG__TYPE_DI] = (mb_reg__struct_t[]){
        {PSU_REG__DI_CC, 0},
        {PSU_REG__DI_CV, 0},
        {PSU_REG__DI_OVP, 0},
        {PSU_REG__DI_OCP, 0},
        {PSU_REG__DI_OTP, 0},

        {MB_REG__END_ADDR, 0}
    },
    [MB_REG__TYPE_AI] = (mb_reg__struct_t[]){
        {PSU_REG__AI_VOLTAGE, 0},
        {PSU_REG__AI_CURRENT, 0},
        {PSU_REG__AI_RATED_VOLTAGE, 0},
        {PSU_REG__AI_RATED_CURRENT, 0},
        {PSU_REG__AI_VOLTAGE_10, 0},
        {PSU_REG__AI_CURRENT_10, 0},

        {MB_REG__END_ADDR, 0}
    },
    [MB_REG__TYPE_AO] = (mb_reg__struct_t[]){
        {PSU_REG__AO_ID, 0},
        {PSU_REG__AO_VOLTAGE, 0},
        {PSU_REG__AO_CURRENT,    0},
        {PSU_REG__AO_BAUDRATE, 0},
        {PSU_REG__AO_OVP, 0},
        {PSU_REG__AO_OCP, 0},

        {MB_REG__END_ADDR, 0}
    },
};

// ������� ��������� �������������
static mb_reg__struct_t* mb_reg__sync_tbl[] = {
    [MB_REG__TYPE_DO] = (mb_reg__struct_t[]){
        {MB_REG__END_ADDR, 0}
    },
    [MB_REG__TYPE_DI] = (mb_reg__struct_t[]){
        {MB_REG__END_ADDR, 0}
    },
    [MB_REG__TYPE_AI] = (mb_reg__struct_t[]){
        {SYNC_REG__AX_START, 0},
        {SYNC_REG__AX_ERROR, 0},
        {SYNC_REG__AX_VOLTAGE, 0},
        {SYNC_REG__AX_CURRENT, 0},
        {SYNC_REG__AX_MODE, 0},

        {MB_REG__END_ADDR, 0}
    },
    [MB_REG__TYPE_AO] = (mb_reg__struct_t[]){
        {SYNC_REG__AX_START, 0},
        {SYNC_REG__AX_ERROR, 0},
        {SYNC_REG__AX_VOLTAGE, 0},
        {SYNC_REG__AX_CURRENT, 0},
        {SYNC_REG__AX_MODE, 0},

        {MB_REG__END_ADDR, 0}
    },
};



events__e mb_reg__get(u8 id, mb_reg__type_e type, uint16_t addr, uint16_t* data) {
    mb_reg__struct_t* p_reg = (id == MODBUS_INT) ? mb_reg__psu_tbl[type] : mb_reg__sync_tbl[type];
    while ((p_reg) && (p_reg->addr != MB_REG__END_ADDR)) {
        if (p_reg->addr == addr) {
            *data = p_reg->data;
            break;
        }
        p_reg++;
    }
    return EVENT__OK;
}


events__e mb_reg__set(u8 id, mb_reg__type_e type, uint16_t addr, uint16_t data) {
    mb_reg__struct_t* p_reg = (id == MODBUS_INT) ? mb_reg__psu_tbl[type] : mb_reg__sync_tbl[type];
    while ((p_reg) && (p_reg->addr != MB_REG__END_ADDR)) {
        if (p_reg->addr == addr) {
            p_reg->data = data;
            break;
        }
        p_reg++;
    }
    return EVENT__OK;
}