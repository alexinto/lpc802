/***************************************************************************//**
 * @file sync_reg.h.
 * @brief ���� � ������� ��������� Modbus RTU ���������� PwrMnt. ������������� ������� ��������� � mb_reg.c
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef SYNC_REG_H_
#define SYNC_REG_H_


typedef enum {
    SYNC_REG__AX_START        = 0x01,        //
    SYNC_REG__AX_ERROR        = 0x02,        //
    SYNC_REG__AX_VOLTAGE      = 0x03,        //
    SYNC_REG__AX_CURRENT      = 0x04,        //
    SYNC_REG__AX_MODE         = 0x05,        //

    SYNC_REG__AX_MAX,                        //
}sync_reg__ax_e;



#endif /* PSU_REG_H_ */
