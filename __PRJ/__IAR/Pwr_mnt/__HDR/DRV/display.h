﻿/***************************************************************************//**
 * @file display.c.
 * @brief Модуль управления дисплеем 2004.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DISPLAY_H_
#define DISPLAY_H_
#include "System/events.h"


events__e display__init(int renew_ms);

events__e display__set_v(float voltage);

events__e display__set_c(float voltage);

events__e display__set_err(uint8_t err);

events__e display__set_out(uint8_t on);


#endif