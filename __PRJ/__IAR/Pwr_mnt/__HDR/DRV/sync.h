﻿/***************************************************************************//**
 * @file sync.h.
 * @brief Модуль управления источником питания.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef SYNC_H_
#define SYNC_H_
#include "System/events.h"

typedef enum {
    SYNC__MODE_NONE   = 0,
    SYNC__MODE_SLAVE  = 1,
    SYNC__MODE_MASTER = 2,
}sync__mode_e;


typedef enum {
    SYNC__STATE_OFF  = 0,
    SYNC__STATE_ON   = 1,
}sync__state_e;

typedef void (*sync__cb_t)(int id, events__e event, void* ext_data);

events__e sync__init(int id, uint8_t mb_id, uint8_t sync_addr);

events__e sync__start(int id, sync__mode_e mode,  sync__cb_t cb, void* ext_data);

events__e sync__stop(int id);







#endif