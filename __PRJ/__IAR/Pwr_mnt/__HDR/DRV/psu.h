﻿/***************************************************************************//**
 * @file psu.c.
 * @brief Модуль управления источником питания.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef PSU_H_
#define PSU_H_
#include "System/events.h"

typedef enum {
    PSU__STATE_OFF  = 0,
    PSU__STATE_ON   = 1,
}psu__state_e;

typedef struct {
    uint8_t on;
    uint8_t flag;
}psu__ocp_v_t;

typedef void (*psu__cb_t)(int id, events__e event, void* ext_data);

events__e psu__init(int id, uint8_t mb_id, uint8_t pwr_addr);

float psu__get_vref(int id);
float psu__get_iref(int id);
float psu__get_v(int id);
float psu__get_i(int id);
float psu__get_ocp(int id);
psu__state_e psu__get_state(int id);
psu__ocp_v_t psu__get_ocp_state(int id);

events__e psu__read(int id, psu__cb_t cb, void* ext_data);                 // Чтение всех регистров
events__e psu__set_v(int id, float voltage, psu__cb_t cb, void* ext_data);
events__e psu__set_i(int id, float current, psu__cb_t cb, void* ext_data);
events__e psu__set_ocp(int id, float current, psu__cb_t cb, void* ext_data);
events__e psu__set_state(int id, psu__state_e state, psu__cb_t cb, void* ext_data);









#endif