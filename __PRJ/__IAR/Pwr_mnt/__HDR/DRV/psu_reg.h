/***************************************************************************//**
 * @file psu_reg.h.
 * @brief ���� � ������� ��������� Modbus RTU ����� �������. ������������� ������� ��������� � psu_reg.c
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef PSU_REG_H_
#define PSU_REG_H_

typedef enum {
    PSU_REG__DO_OUT_SW        = 0x85,        //
    PSU_REG__DO_LOCK          = 0x86,        //
    PSU_REG__DO_OVP           = 0x88,        //
    PSU_REG__DO_OCP           = 0x89,        //
    PSU_REG__DO_PWRON         = 0x8E,        //

    PSU_REG__DO_MAX,                         //
}psu_reg__do_e;

typedef enum {
    PSU_REG__DI_CC            = 0x74,        //
    PSU_REG__DI_CV            = 0x75,        //
    PSU_REG__DI_OVP           = 0x78,        //
    PSU_REG__DI_OCP           = 0x79,        //
    PSU_REG__DI_OTP           = 0x7A,        //

    PSU_REG__DI_MAX,                         //
}psu_reg__di_e;

typedef enum {
    PSU_REG__AO_ID            = 0x94,        //
    PSU_REG__AO_VOLTAGE       = 0x95,        //
    PSU_REG__AO_CURRENT       = 0x96,        //
    PSU_REG__AO_BAUDRATE      = 0x9C,        //
    PSU_REG__AO_OVP           = 0x9D,        //
    PSU_REG__AO_OCP           = 0x9E,        //

    PSU_REG__AO_MAX,                         //
}psu_reg__ao_e;

typedef enum {
    PSU_REG__AI_VOLTAGE       = 0x64,        //
    PSU_REG__AI_CURRENT       = 0x65,        //
    PSU_REG__AI_RATED_VOLTAGE = 0x67,        //
    PSU_REG__AI_RATED_CURRENT = 0x68,        //
    PSU_REG__AI_VOLTAGE_10    = 0x6A,        //
    PSU_REG__AI_CURRENT_10    = 0x6B,        //

    PSU_REG__AI_MAX,                         //
}psu_reg__ai_e;


#endif /* PSU_REG_H_ */
