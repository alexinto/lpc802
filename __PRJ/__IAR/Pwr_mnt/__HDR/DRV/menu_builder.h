﻿/***************************************************************************//**
 * @file menu_builder.h.
 * @brief Модуль построения меню.
 * @author a.tushentsov.
 ******************************************************************************/

#ifndef MENU_BUILDER_H_
#define MENU_BUILDER_H_

#include "System/fstandard.h"
#include "menu_tree.h"

typedef enum {
    MENU_PARAM__VERSION = 0,
    MENU_PARAM__MB_MODE,
    MENU_PARAM__MB_ADDR,
    MENU_PARAM__VOLTAGE,     // mV
    MENU_PARAM__CURRENT,     // mA
    MENU_PARAM__AKB_TYPE,
    MENU_PARAM__I_14A510,
    MENU_PARAM__I_12120M,
    MENU_PARAM__I_END_14A510,
    MENU_PARAM__I_END_12120M,
    MENU_PARAM__TERMO_MODE,
    MENU_PARAM__T_MAX,
    MENU_PARAM__T_COMP,
    MENU_PARAM__CHG_TIME_ERR,

    MENU_PARAM__NONE
}menu_bulder__params_e;

// Возращает указатель на первую страницу меню.
menu__item__t* menu_builder__create();

int menu_builder__param_get(menu_bulder__params_e param);
void menu_builder__param_set(menu_bulder__params_e param, int value);

int menu_builder__p_min_get(menu_bulder__params_e param);
int menu_builder__p_max_get(menu_bulder__params_e param);

char* menu_builder__descr_get(menu_bulder__params_e param);

menu_bulder__params_e menu_builder__item2id(menu__item__t* item);

events__e menu_builder__p2str(menu__item__t* item, char* buff, int len);

events__e menu_builder__param_save(menu_bulder__params_e param);

#endif