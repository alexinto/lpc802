﻿/***************************************************************************//**
 * @file fsm_mode.h.
 * @brief Модуль автомата режимов.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef FSM_MODE_H_
#define FSM_MODE_H_
#include "System/events.h"

typedef enum {
    FSM_MODE__OFF      = 0,
    FSM_MODE__MANUAL   = 1,
    FSM_MODE__OCP      = 2,
    FSM_MODE__CHARGE   = 3,
}fsm_mode__e;

typedef enum {
    FSM_STATE__OFF      = 0,
    FSM_STATE__ON       = 1,
    FSM_STATE__WAIT     = 2,
    FSM_STATE__ERROR    = 3,
    FSM_STATE__LOCK     = 4,
    FSM_STATE__OTP      = 5,
    FSM_STATE__END      = 6,
    FSM_STATE__UNV      = 7,
}fsm_state__e;

typedef enum {
    FSM_MODE__CMD_NONE     = 0,
    FSM_MODE__CMD_MANUAL   = 1,
    FSM_MODE__CMD_OCP      = 2,
    FSM_MODE__CMD_CHARGE   = 3,
    FSM_MODE__CMD_ON       = 4,
    FSM_MODE__CMD_OFF      = 5,
    FSM_MODE__CMD_ERR      = 6,
    FSM_MODE__CMD_LOCK     = 7,
}fsm_mode__cmd_e;


typedef void (*fsm_mode__cb_t)(events__e event, fsm_mode__e mode);

events__e fsm_mode__init();

fsm_mode__e fsm_mode__get();

fsm_state__e fsm_mode__state_get();

void fsm_mode__clr_err();   // Функция очистки ошибок

void fsm_mode__cmd(fsm_mode__cmd_e cmd);







#endif