﻿/***************************************************************************//**
 * @file target.h.
 * @brief  Конфигурация целевого приложения.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TARGET_H
#define TARGET_H

#include "mcu.h"
#include "encoder.h"

#define SW_VERSION {0x04,0x00,0x01,0x00}    // Нули не удалять!

// Настроечные таймауты и прочие изменяемые параметры
#define TERMO__TIMEOUT          5000        // Интервал опроса всех термодатчиков, мс.
#define FSM_MODE__PSU_TIMEOUT   100         // Интервал опроса PSU, мс
#define FSM__ERR_CNT_MAX        10          // Счетчик ошибок- по достижении значения выполняется процедура режима "Alarm"
#define FSM_MODE__RST_TIMEOUT   10000       // таймаут на управление ногой вкл/выкл 380 В
#define FSM_MODE__BUTTON_FLT    4           // ФИльтр кнопок (1..8). Влияет на ложные срабатывания кнопок.
#define SW_ENCODER___PIN_FILTER 4           // Фильтр на пины енкодера в битах(2..6). Определяются дребезгом контактов.
#define SW_ENCODER__FREQ        2000        // Частота дискретизации, Гц. Повышение влияет на общую производительность системы!

// Светодиодная индикация, интервалы переключения светодиода в мс.
#define DEVICE__LED_1WIRE       200         // Режим поиска датчиков 1wire
#define DEVICE__LED_MODBUS      500         // Инициализация modbus устройств
#define DEVICE__LED_COMPLETE    1000        // Загрузка завершена. Штатный режим работы.

//Конфигурация Модуля настроек
#define DEV_SETTS__ID   0                   // В проекте только одна флеш- флеш МК
#define DEV_SETTS__ADDR 0x08100000          // Начальный адрес флеш
#define DEV_SETTS__SIZE 0x8000              // Размер области памяти для настроек, кратен размеру сектора флеш!


//Конфигурация MODBUS
#define MB_RTU__COUNT 2
#define MODBUS_INT  0
#define MODBUS_SYNC 1
#define MB_RTU__TIMEOUT 50                // таймаут ожидания ответа, мс

// Конфигурация UART
#define HW_UART_COUNT 3
#define HW_UART_1 MCU__UART_1
#define HW_UART_1_TX MCU__GPIO_P_B_14
#define HW_UART_1_RX MCU__GPIO_P_B_15
#define HW_UART_2 MCU__UART_2
#define HW_UART_2_TX MCU__GPIO_P_A_2
#define HW_UART_2_RX MCU__GPIO_P_A_3
#define PSU_UART__DIR_PIN MCU__GPIO_P_A_1
#define HW_UART_3 MCU__UART_3
#define HW_UART_3_TX MCU__GPIO_P_D_8
#define HW_UART_3_RX MCU__GPIO_P_D_9
#define SYNC_UART__DIR_PIN MCU__GPIO_P_D_10

// Конфигурация устройств SPI
#define SPI__DEV_COUNT 1                    // Одно устройство- дисплей
#define SPI__DISPLAY   0                    // Идентификатор дисплея

// Кнфигурация 1Wire
#define ONEWIRE__CNT 1                      // Количество интерфейсов 1wire
#define ONEWIRE_1    0                      // Один интерфейс на HW_UART_1

// Конфигурация DS18b20
#define DS18B20__CNT 3                      // Количество термодатчиков
#define DS18B20_1 0                         //
#define DS18B20_2 1                         //
#define DS18B20_3 2                         //

// Конфигурация енкодеров
#define ENCODER__MENU      enc0             // Енкодер навигации в меню
#define ENCODER__VOLTAGE   enc1             // Енкодер напряжения
#define ENCODER__CURRENT   enc2             // Енкодер тока

// Конфигурация дисплеев
#define DISPLAY__RENEW_TIME 50              // время обновление дисплеев, мс.
#define DEV_DISPLAY         SPI__DISPLAY    // Номер устройства SPI для дисплея
#define MAX7219__COUNT      1               // Пока только один семисегментый дисплей
#define DISP7TH             0               // Пока только один семисегментый дисплей
#define I2C__COUNT          3               // кол-во интерфейсов I2C в проекте
#define I2C_DISPL       I2C_2               // Интерфейс lcd дисплея
#define I2C_ADDR         0x3F               // Адрес lcd дисплея

// Конфигурация автомата режимов работы
#define PWR_ON_BUTTON    MCU__GPIO_P_A_8    // Кнопка включения выхода БП
#define PWR_OFF_BUTTON   MCU__GPIO_P_A_9    // Кнопка выключения выхода БП
#define SW1_BUTTON       MCU__GPIO_P_A_10   // 3х позиционный переключатель
#define SW2_BUTTON       MCU__GPIO_P_A_11   // 3х позиционный переключатель
#define PWR_BUTTON_LOCK  MCU__GPIO_P_F_7    // реле блокировки БП
#define PWR_BUTTON_RST   MCU__GPIO_P_G_1    // реле подачи 380В на БП
#define PWR_LED          MCU__GPIO_P_A_7    // Светодиод питания МК
#define OUT_LED          MCU__GPIO_P_A_6    // Светодиод состояния выхода БП
#define ERR_LED          MCU__GPIO_P_A_5    // Светодиод ошибка OCP/интерфейсов
#define USER_BT          MCU__GPIO_P_C_13   // Синяя кнопка на отладке
#define LED_BLUE         MCU__GPIO_P_B_7    // Синий светодиод на отладке

// Конфигурация блока питания
#define PSU__MB_ADDR 0x1                    // Модбас адрес БП
#define PSU_MAIN 0                          // Пока только 1 БП...

// Конфигурация параллельной работы
#define SYNC__MB_ADDR 0x15                  // Модбас адрес устройства

//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif