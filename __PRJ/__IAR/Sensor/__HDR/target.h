#ifndef TARGET_H
#define TARGET_H

#include "mcu.h"

#define UART_BUFF      20
#define I2C_ADDR     0x98

#define user_bt MCU__GPIO_PIN_0
#define PS_ON MCU__GPIO_PIN_7

#define LED_GREEN MCU__GPIO_PIN_17
#define SENSOR_DISABLE MCU__GPIO_PIN_12

#define PWM_PIN MCU__GPIO_PIN_1
#define ADC_VOLT ADC_8
#define UART_BRD UART__BR_9600
#define UART0_RX RX__GPIO_PIN_0
#define UART0_TX TX__GPIO_PIN_14
#define UART0_SETTINGS UART_BRD | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FRAME_3_5 | UART0_TX | UART0_RX

#define I2C_0_SETTINGS SDA__GPIO_PIN_10 | SCL__GPIO_PIN_16
//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif