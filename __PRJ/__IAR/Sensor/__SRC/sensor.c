/***************************************************************************//**
 * @file sensor.c.
 * @brief  �������� ������ �������.
 * @author a.tushentsov.
 ******************************************************************************/

#include <stddef.h>
#include "DRV/gpio.h"
#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "System/supervisor.h"
#include "DRV/i2c.h"
#include "DRV/hw_uart.h"
#include "DRV/uart.h"
#include "System/crc.h"

#define R12 12000   // � �������
#define SUPPLY  0
#define U_PWR   1
#define U_SENS  2
#define U_OUT   3
#define ADC_BUSY 0x80

typedef struct {
    uint8_t start        :1;
    uint8_t work         :1;
    uint8_t stop         :1;
    uint8_t current_set;
    uint16_t voltage[4];
}tester_t;

static const uint8_t ads1115_voltages[] = {0x01, 0xC5, 0x83, 0x01, 0xD5, 0x83, 0x01, 0xE5, 0x83, 0x01, 0xF5, 0x83};

static struct {
    uint8_t data_reg;
    uint8_t* voltage[4];
    union {
        struct {
            uint8_t state;
            uint8_t substate;
        };
        uint16_t states;
    };
}ads1115 = {0x00, (uint8_t*)ads1115_voltages, (uint8_t*)&ads1115_voltages[3], (uint8_t*)&ads1115_voltages[6], (uint8_t*)&ads1115_voltages[9]};

static tester_t tester_data;
static uint8_t i2c_buff[6];
static uint8_t uart_buff[UART_BUFF];
static sw_timer__t timer_i2c;

static void timer_i2c_cb(struct sw_timer__t *timer, void *ext_data);
static void i2c_start();
static void i2c__cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);
static void uart_cb(int uart_id, events__e event, uint8_t *buff, int len);
static void power_periph_start();

 void main() {
    supervisor__init();
    sw_timer__init(NULL);

    gpio__init(0, SENSOR_DISABLE, GPIO__DIR_OUT, NULL);
    gpio__set(0, SENSOR_DISABLE, GPIO__STATE_HIGH);
    gpio__init(0, LED_GREEN, GPIO__DIR_OUT, NULL);
    gpio__set(0, LED_GREEN, GPIO__STATE_LOW);

    i2c__init(I2C_0, I2C_0_SETTINGS, NULL, i2c__cb);
    hw_uart__init(UART_0, UART0_SETTINGS, NULL);
    hw_uart__rx(UART_0, uart_buff, UART_BUFF, uart_cb);
    i2c_start();

    tester_data.start = 0;

    while(1) {
        sw_timer__cout();
        hw_uart__cout();
        i2c__cout();
        framework__cout();
        wdt__reset();

        power_periph_start();
   }
}


static void i2c_start() {
    ads1115.states = 0;
    i2c__tx(I2C_0, 0x90, *ads1115.voltage, 3, i2c__cb, NULL);
    sw_timer__start(&timer_i2c, 100, timer_i2c_cb, NULL);
}

static void power_periph_start() {
    if (tester_data.start) {
        if (!tester_data.work) {
            gpio__set(SENSOR_DISABLE, GPIO__STATE_LOW);
            gpio__set(LED_GREEN, GPIO__STATE_HIGH);
            tester_data.work = 1;
            i2c_start();
        }
    }
    else if (tester_data.work) {
        tester_data.work = 0;
        gpio__set(LED_GREEN, GPIO__STATE_LOW);
        gpio__set(SENSOR_DISABLE, GPIO__STATE_HIGH);
    }
}

static void i2c__cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
    if (event != EVENT__OK) {// || (!tester_data.work)) {
        i2c__deinit();
        i2c__init(I2C_0, I2C_0_SETTINGS, NULL, i2c__cb);
        return;
    }
    for(int x = 0; x < 1000; x++);
    switch (ads1115.substate) {
    case 0:
    case 2:
        i2c__rx(I2C_0, 0x90, i2c_buff, 2, i2c__cb, NULL);
        break;
    case 1:
        if (((i2c_buff[0] | ADC_BUSY)  == ads1115.voltage[ads1115.state][1]) && (i2c_buff[1] == ads1115.voltage[ads1115.state][2]))
            i2c__tx(I2C_0, 0x90, &ads1115.data_reg, 1, i2c__cb, NULL);
        else
            i2c__tx(I2C_0, 0x90, ads1115.voltage[ads1115.state], 3, NULL, NULL);
        break;
    case 3:
        i2c_buff[3] = i2c_buff[0];
        i2c_buff[2] = i2c_buff[1];
        if ((tester_data.voltage[ads1115.state] = *(int16_t*)&i2c_buff[2]) > 32767)
            tester_data.voltage[ads1115.state] = 0;
        tester_data.voltage[ads1115.state] = (uint32_t)(20480 * tester_data.voltage[ads1115.state]) / 32767;
        tester_data.voltage[ads1115.state] /= 10;
        if (++ads1115.state > 3) {
            ads1115.states = 0;
        }
        i2c__tx(I2C_0, 0x90, ads1115.voltage[ads1115.state], 3, NULL, NULL);
        break;
   default:
        break;
    }
    ads1115.substate++;
}

static void timer_i2c_cb(struct sw_timer__t *timer, void *ext_data) {
//    if (!tester_data.work)
//        return;
    sw_timer__start(timer, -1, timer_i2c_cb, NULL);
    ads1115.substate = 0;
    i2c__tx(I2C_0, 0x90, *ads1115.voltage, 1, i2c__cb, NULL);
}


static void uart_cb(int uart_id, events__e event, uint8_t *buff, int len) {
    uint8_t command = uart_buff[0], answer = 0x29;
//    hw_uart__tx(UART_0, uart_buff, UART_BUFF, uart_cb);

    if ((event != EVENT__OK) || (command < 0x70) || (command > 0x73)) {
        hw_uart__deinit(UART_0);
        hw_uart__init(UART_0, UART0_SETTINGS, NULL);
        hw_uart__rx(UART_0, uart_buff, UART_BUFF, uart_cb);
        return;
    }
    switch(command) {
    case 0x70: // ������ �������
        tester_data.start = 1;
        answer = 0x20;
        tester_data.current_set = uart_buff[1];
        uart_buff[1] = tester_data.current_set;
        break;
    case 0x72: // ���������� �������� ����
        answer = 0x22;
        tester_data.current_set = uart_buff[1];
        uart_buff[1] = tester_data.current_set;
        break;
    case 0x71:  // ��������� ������
        tester_data.start = 0;
        answer = 0x21;
    case 0x73:  // ��������� ���������
        if (command == 0x73)
            answer = 0x23;
        uart_buff[1] = 0x00;
        u32 u_pwr = tester_data.voltage[U_PWR] << 1, u_sens = tester_data.voltage[U_SENS];
        u32 u_r12 = (u_pwr - u_sens);

        *(uint16_t*)&uart_buff[2] = tester_data.voltage[SUPPLY] << 1;
        *(uint16_t*)&uart_buff[4] = tester_data.voltage[U_OUT];
        *(uint16_t*)&uart_buff[6] = (u_pwr < u_sens ? 0 : u_r12) * u_sens / R12;
        *(uint16_t*)&uart_buff[8] = 0x00;
        break;
    }
    uart_buff[0] = answer;
    uart_buff[8] = crc__8_ccitt(uart_buff, 8);
    hw_uart__tx(UART_0, uart_buff, UART_BUFF, uart_cb);
}
