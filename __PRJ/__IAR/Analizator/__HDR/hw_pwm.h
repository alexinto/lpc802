/***************************************************************************//**
 * @file hw_pwm.h.
 * @brief �������� ������� ������ hw_pwm.c
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef HW_PWM_H_
#define HW_PWM_H_

#include "standard.h"
#include "System/events.h"

typedef enum {
    PWM_0  = 0, // MCU__GPIO_P_A_1  ENC+    TIM2
    PWM_1  = 1, // MCU__GPIO_P_C_7  ENC+    TIM3
    PWM_2  = 2, // MCU__GPIO_P_B_6  ENC+    TIM4
    PWM_3  = 3, // MCU__GPIO_P_A_0  ENC+    TIM5
    PWM_4  = 4, // MCU__GPIO_P_H_6          TIM12
    PWM_5  = 5, // MCU__GPIO_P_A_6          TIM13
    PWM_6  = 6, // MCU__GPIO_P_A_7          TIM14
    PWM_7  = 7, // MCU__GPIO_P_A_2          TIM9
    PWM_8  = 8, // MCU__GPIO_P_B_8          TIM10
    PWM_9  = 9, // MCU__GPIO_P_B_9          TIM11
    PWM_NONE = 10,
}hw_pwm_id_e;

typedef void (*hw_pwm_cb_t)(hw_pwm_id_e pwm, u32 pulse_cnt, void* ext_data);

/***************************************************************************//**
 * @brief �������- ������� ������ PWM. ���������� ��������� c ������������������� ����������
 *        pwm-������ ������������ 0 ��������. ��������������� ��������� � ����� �������
 *        � 0 ������� pwm ������ ������������ ���������� percent = 0.
 * @param pwm - ����� ������ PWM.
 * @param freq - ������� ������ PWM (�� 0 �� 60���).
 * @param percent - ���������� ������� (�� 0 �� 100%).
 * @param inversion - ������������� ������ (1)
 * @return void.
 ******************************************************************************/
void hw_pwm__ctrl(hw_pwm_id_e pwm, u32 freq, u16 percent, u8 inversion);

/***************************************************************************//**
 * @brief �������- ������ �������� ��������� ������������ ������ PWM.
 * @param pwm - ����� ������ PWM.
 * @param freq - ������� ������ PWM (�� 0 �� 60���).
 * @param width - ������������ �������� � ���.
 * @param cb - ������� �� ����� ������������ ��������� ���-�� ���������. ����� ���� NULL.
 * @param ext_data - ���������������� ������.
 * @return PWM_OK - ������ �������.
 *         PWM_BUSY - ������.
 ******************************************************************************/
events__e hw_pwm__set(hw_pwm_id_e pwm, u32 freq, u16 width, hw_pwm_cb_t cb, void* ext_data);

events__e hw_pwm__get(hw_pwm_id_e pwm);

/***************************************************************************//**
 * @brief ������� -���������� ��������� ������ hw_pwm.c
 * @param void.
 * @return void.
 ******************************************************************************/
void hw_pwm__cout();
#endif
