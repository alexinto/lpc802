/***************************************************************************//**
 * @file drv_peletier.c.
 * @brief ���� � ��������� ��������� ������ ������ �������.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DRV_FUN_H
#define DRV_FUN_H

#include "System/events.h"

typedef enum {
    FUN_0  = 0,
    FUN_1  = 1,
    FUN_NONE = 2,
}drv_fun__id_e;

typedef enum {
    FUN_SET_SPEED      = 0, // �������� 0-100%
    FUN_SET_FREQ       = 1, // �������
    FUN_ON_OFF         = 2, // ���������\���������� ������
    FUN_CMD_NONE       = 3,
}drv_fun__cmd_e;

events__e drv_fun__init();

events__e drv_fun__cmd_exec(drv_fun__id_e id, drv_fun__cmd_e cmd, int param);


#endif
