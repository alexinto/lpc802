/***************************************************************************//**
 * @file target.h.
 * @brief  Конфигурация целевого приложения.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TARGET_H
#define TARGET_H

#include "mcu.h"

#define SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_LOW_SPD | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB)
#define SPI__COUNT          2
#define TMC_5160_SPI    SPI_1
#define MOTOR_X         motor0
#define MOTOR_Y         motor1
#define MOTOR_Z         motor2
#define MOTOR_M1        motor3
#define MOTOR_M2        motor4
#define MOTOR_P         motor5
#define MOTOR_DOOR1     motor6
#define MOTOR_DOOR2     motor7

#define PWR_SERV_MG90 MCU__GPIO_P_G_10
#define PWR_SERV_RDS3235 MCU__GPIO_P_G_11

#define SPI_TERMO       SPI_2
#define SPI_SENSOR_1        0
#define SPI_SENSOR_2        1

#define I2C__COUNT          2
#define I2C_TERMO       I2C_2
#define I2C_ADDR         0xAA

#define UART_BUFF_SIZE 200
#define HW_UART_COUNT 2
#define HW_UART_1 MCU__UART_1
#define HW_UART_1_TX MCU__GPIO_P_K_0
#define HW_UART_1_RX MCU__GPIO_P_K_1
#define HW_UART_2 MCU__UART_2
#define HW_UART_2_TX MCU__GPIO_P_D_5
#define HW_UART_2_RX MCU__GPIO_P_D_6
#define UART_SETTINGS (UART__BR_115200 | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FC_NONE)
#define ONEWIRE_1_UART HW_UART_2
#define ONEWIRE_2_UART HW_UART_2

#define USB_UART_COUNT          1
#define USB_UART_1 MCU__USB_COM_1

#define USER_BT MCU__GPIO_P_C_13
#define LED_BLUE MCU__GPIO_P_B_7
#define LED_GREEN MCU__GPIO_P_B_0

#define BOARD_SELECTION_PIN MCU__GPIO_P_C_8
#define PROGRAM_VERSION (3)

//#define WDT_ON
//#define WDT_AUTO_RESET_SLEEP


#endif