/***************************************************************************//**
 * @file drv_peletier.c.
 * @brief ���� � ��������� ��������� ������ ������ �������.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DRV_PELETIER_H
#define DRV_PELETIER_H

#include "System/events.h"

typedef enum {
    PELETIER_0  = 0,
    PELETIER_1  = 1,
    PELETIER_NONE = 2,
}drv_peletier_id_e;

typedef enum {
    PELETIER_CMD_SET_TEMP   = 0, // ����������� � ������� 0.1 � + 1000 (0 = -100�, 800 = -20�, 1000 = 0�, 1510 = 51�, 1125 = 12.5�)
    PELETIER_CMD_GET_TEMP   = 1,
    PELETIER_CMD_SET_POWER  = 2, // �������� ��� ��������� �������� (+13� = 150, 11� = 100)
    PELETIER_CMD_SET_SPEED  = 3, // �������� ��� �������� ���������� � ��������� �������� (������ 10)
    PELETIER_CMD_SET_KP     = 4, // ����������� ���������������� ������������ ���- ����������
    PELETIER_CMD_OFF        = 5,
    PELETIER_CMD_NONE       = 6,
}drv_peletier_cmd_e;

typedef void (*drv_peletier_cb_t)(drv_peletier_id_e id, events__e event, int param, void* ext_data);

events__e drv_peletier__cmd_exec(drv_peletier_id_e id, drv_peletier_cmd_e cmd, int param, drv_peletier_cb_t cb, void* ext_data);


#endif
