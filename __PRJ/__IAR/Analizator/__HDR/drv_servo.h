/***************************************************************************//**
 * @file drv_servo.h.
 * @brief ������ ���������� ���������������.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DRV_SERVO_H_
#define DRV_SERVO_H_

#include "System/events.h"

typedef enum {
    SERVO_0    = 0,     //
    SERVO_1    = 1,     //
    SERVO_2    = 2,     //
    SERVO_3    = 3,     //
    SERVO_4    = 4,     //
    SERVO_5    = 5,     //
    SERVO_6    = 6,     //
    SERVO_7    = 7,     //
    SERVO_8    = 8,     //
    SERVO_9    = 9,     //
    SERVO_10   = 10,    //
    SERVO_11   = 11,    //
    SERVO_NONE = 12,    //
}drv_servo_id_e;

typedef enum {
    DRV_SERVO__MOVE              = 0, // ������������� �� ������� X �������� (0-360)
    DRV_SERVO__SET_TIMEOUT       = 1, // ������ �� ������� ��������
    DRV_SERVO__SET_MIN_SPEED     = 2, // ������ ��������� �������� �����������
    DRV_SERVO__SET_MAX_SPEED     = 3, // ������ ������������ ��������
    DRV_SERVO__SET_ACC           = 4, // ������ ���������
    DRV_SERVO__MG90_ON           = 5, // �������� ������ MG90
    DRV_SERVO__RDS3235_ON        = 6, // �������� ������ RDS3235
    DRV_SERVO__MG90_OFF          = 7, // ��������� ������ MG90
    DRV_SERVO__RDS3235_OFF       = 8, // ��������� ������ RDS3235
    DRV_SERVO__NONE              = 9, // ��� ���������
}drv_servo__cmd_e;

typedef void (*drv_servo__cb_t)(int motor, events__e event, int param, void* ext_data);

events__e drv_servo__init();

/***************************************************************************//**
 * @brief ������� ���������� ������ ��������������.
 * @param motor - ����� ������.
 * @param cmd - ������� ������.
 * @param param - �������� �������.
 * @param cb - ������� ��������.
 * @param ext_data - ���������������� ������.
 * @return EVENT_OK.
 ******************************************************************************/
events__e drv_servo__cmd(int motor, drv_servo__cmd_e cmd, int param, drv_servo__cb_t cb, void* ext_data);

void drv_servo__cout();

#endif