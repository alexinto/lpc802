/***************************************************************************//**
 * @file drv_lamp.c.
 * @brief ���� � ��������� ��������� ������ ������ �������.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DRV_LAMP_H
#define DRV_LAMP_H

#include "System/events.h"

typedef enum {
    LAMP_0   = 0,
    LAMP_NONE = 1,
}drv_lamp__id_e;

typedef enum {
    LAMP_ON_OFF         = 0, // ���������\���������� ������
    LAMP_CMD_NONE       = 1,
}drv_lamp__cmd_e;


events__e drv_lamp__cmd_exec(drv_lamp__id_e id, drv_lamp__cmd_e cmd, int param);


#endif
