/***************************************************************************//**
 * @file cmd_fsm.h.
 * @brief �������� �������� � ������� ��������- ����������� ������ �����������.
 * @authors a.tushentsov
 ******************************************************************************/
#ifndef CMD_FSM_H_
#define CMD_FSM_H_

#include "standard.h"
#include "System/events.h"

#define CMD_LEN   4
#define MSG_BUFF  100

typedef enum {
    ID_KERNEL = 250,
}cmd_fsm__id_e;

typedef enum {
    KERNEL_GET_VERSION   = 0,
    KERNEL_GET_BOARD_VER = 1,
    KERNEL_ANSWER        = 2,
}cmd_fsm__kernel_cmd_e;


typedef enum {
    FSM_OK               = 0,
    FSM_CRC_ERROR        = 65535,  // ������ ������.
    FSM_BUSY             = 65534,  // ���������� ������.
    FSM_NOT_EXIST        = 65533,  // ���������� �� ���������� (������ ������������)
    FSM_NOT_DEV          = 65532,  // ���������� ���������� (�� �������� �� ����).
    FSM_FROZEN           = 65531,  // ���������� ������� (��������� � �������, ������� �� ����� � �.�.)
    FSM_PARAM_NA         = 65530,  // �������� ��������/�������.
    FSM_DEV_TIMEOUT      = 65529,  // ���������� �� ��������� ������� �� ���������� �����
    FSM_ERROR            = 65528,  // ����������� ������ ����������
    FSM_NO_OPTION        = 65527,  // �������� �� ��������� ��� ��������� �� ��������� (��� ������������ ������������)
}cmd_fsm__error_e;

typedef enum {
    FSM_DEINIT   =  0,
    FSM_INIT     =  1,
    FSM_IDLE     =  2,
    FSM_WORK     =  3,
}cmd_fsm__state_e;


typedef struct {
    u8 id;
    u8 cmd;
    u16 param;
}cmd_fsm__message_t;

typedef void (*cmd_fsm__ev_hd_t)(cmd_fsm__message_t* data, void* ext_data);

/***************************************************************************//**
 * @brief ������� �������������� ����������� ������.
 * @param event_handler - ����� ������� ����������� ���������� ������.
 * @return void.
 ******************************************************************************/
void cmd_fsm__init(cmd_fsm__ev_hd_t event_handler);

/***************************************************************************//**
 * @brief ������� ������� ������ � ��������� �� ����������.
 * @param data - ��������� �� ����� ������. ����������� 0x00.
 * @param ext_data - ���������������� ������.
 * @return void.
 ******************************************************************************/
cmd_fsm__error_e cmd_fsm__cmd_set(cmd_fsm__message_t* data, void* ext_data);


void cmd_fsm__cout(void);
#endif