/***************************************************************************//**
 * @file drv_servo.c.
 * @brief  ������� ��������������.
 * @author a.tushentsov.
 ******************************************************************************/

#include "System/sw_timer.h"
#include "hw_pwm.h"
#include "drv_servo.h"

#define SERVO_FREQ 200 // ������� � ��


struct drv_servo__list_t;

typedef struct drv_servo__list_t {
    struct drv_servo__list_t* prev;   // ��������� �� ���������� ��������� ������.
    struct drv_servo__list_t* next;   // ��������� �� ��������� ��������� ������.
} drv_servo__list_t;


typedef struct {
    drv_servo__list_t sub;
    drv_servo__cmd_e cmd;
    u16 param, servo_us[2];
    events__e state;
    hw_pwm_id_e pwm[2];
    int timeout, min_val, max_val, degree, en_pin;
    sw_timer__t tmr;
    void* ext_data;
    drv_servo__cb_t cb;
}drv_servo__struct_t;

static drv_servo__struct_t drv_servo__data[] = {{.state = EVENT__WAIT, .pwm[0] = PWM_0, .pwm[1] = PWM_3,    .en_pin = MCU__GPIO_P_E_8,  .timeout = 400, .min_val = 544, .max_val = 2400, .degree = 180},
                                                {.state = EVENT__WAIT, .pwm[0] = PWM_0, .pwm[1] = PWM_NONE, .en_pin = MCU__GPIO_P_E_10, .timeout = 400, .min_val = 544, .max_val = 2400, .degree = 180},
                                                {.state = EVENT__WAIT, .pwm[0] = PWM_0, .pwm[1] = PWM_NONE, .en_pin = MCU__GPIO_P_E_11, .timeout = 400, .min_val = 544, .max_val = 2400, .degree = 180},
                                                {.state = EVENT__WAIT, .pwm[0] = PWM_0, .pwm[1] = PWM_NONE, .en_pin = MCU__GPIO_P_E_12, .timeout = 400, .min_val = 544, .max_val = 2400, .degree = 180},
                                                {.state = EVENT__WAIT, .pwm[0] = PWM_0, .pwm[1] = PWM_NONE, .en_pin = MCU__GPIO_P_E_13, .timeout = 400, .min_val = 544, .max_val = 2400, .degree = 180},
                                                {.state = EVENT__WAIT, .pwm[0] = PWM_0, .pwm[1] = PWM_NONE, .en_pin = MCU__GPIO_P_E_14, .timeout = 400, .min_val = 544, .max_val = 2400, .degree = 180},
                                                {.state = EVENT__WAIT, .pwm[0] = PWM_NONE, .en_pin = MCU__GPIO_P_E_14}, {.state = EVENT__WAIT, .pwm[0] = PWM_NONE, .en_pin = MCU__GPIO_P_E_14}, {.state = EVENT__WAIT, .pwm[0] = PWM_NONE, .en_pin = MCU__GPIO_P_E_14}, {.state = EVENT__WAIT, .pwm[0] = PWM_NONE, .en_pin = MCU__GPIO_P_E_14}, {.state = EVENT__WAIT, .pwm[0] = PWM_NONE, .en_pin = MCU__GPIO_P_E_14}, {.state = EVENT__WAIT, .pwm[0] = PWM_NONE, .en_pin = MCU__GPIO_P_E_14}};

static drv_servo__list_t drv_servo__cmd_list = {.next = &drv_servo__cmd_list, .prev = &drv_servo__cmd_list};

static void drv_servo__tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void drv_servo__pwm_cb(hw_pwm_id_e pwm, u32 freq, void* ext_data);

events__e drv_servo__init() {
    gpio__init(PWR_SERV_MG90, GPIO__DIR_OUT, NULL);
    gpio__init(PWR_SERV_RDS3235, GPIO__DIR_OUT, NULL);
    gpio__set(PWR_SERV_MG90, GPIO__STATE_LOW);
    gpio__set(PWR_SERV_RDS3235, GPIO__STATE_LOW);
    for(int  i = 0; i < SERVO_NONE; i++) {
        gpio__init(drv_servo__data[i].en_pin, GPIO__DIR_OUT, NULL);
        gpio__set(drv_servo__data[i].en_pin, GPIO__STATE_LOW);
    }
    return EVENT__OK;
}

events__e drv_servo__cmd(int motor, drv_servo__cmd_e cmd, int param, drv_servo__cb_t cb, void* ext_data) {
    drv_servo__struct_t* servo_ptr = &drv_servo__data[motor];
    if (servo_ptr->state != EVENT__WAIT)
        return EVENT__BUSY;
    if (servo_ptr->pwm[0] >= PWM_NONE)
        return EVENT__NOT_EXIST;
    if ((param < 0) || (param > 60000) || (cmd >= DRV_SERVO__NONE))
        return EVENT__PARAM_NA;
    if (cmd == DRV_SERVO__MOVE) {
        if (param > servo_ptr->degree)
            return EVENT__PARAM_NA;
        servo_ptr->servo_us[0] = (servo_ptr->max_val - servo_ptr->min_val) * param / servo_ptr->degree;
        servo_ptr->servo_us[0] += servo_ptr->min_val;
        servo_ptr->servo_us[1] = servo_ptr->min_val + servo_ptr->max_val - servo_ptr->servo_us[0];
    }
    servo_ptr->param = param;
    servo_ptr->cmd = cmd;
    servo_ptr->cb = cb;
    servo_ptr->ext_data = ext_data;
    servo_ptr->state = EVENT__OK;
    if (!servo_ptr->sub.next)
        list__include(&drv_servo__cmd_list, &servo_ptr->sub, LIST__TAIL);
    return EVENT__OK;
}


void drv_servo__cout() {
    drv_servo__struct_t* servo_ptr;
    if ((servo_ptr = list__foreach(&drv_servo__cmd_list, NULL, LIST__HEAD)) != NULL) {
        gpio__state_e pwr_pin_ctrl = GPIO__STATE_LOW;
        switch(servo_ptr->cmd) {
            case DRV_SERVO__MOVE:
                if (hw_pwm__get(servo_ptr->pwm[0]) == EVENT__OK) {
                    gpio__init(servo_ptr->en_pin, GPIO__DIR_IN, NULL);
                    for(int i = 0; (servo_ptr->pwm[i] != PWM_NONE) && (i < 2); i++)
                        hw_pwm__set(servo_ptr->pwm[i], SERVO_FREQ, servo_ptr->servo_us[i], drv_servo__pwm_cb, servo_ptr);
                }
                return;
            case DRV_SERVO__SET_TIMEOUT:
                servo_ptr->timeout = servo_ptr->param;
                break;
            case DRV_SERVO__MG90_ON:
                pwr_pin_ctrl = GPIO__STATE_HIGH;
            case DRV_SERVO__MG90_OFF:
                gpio__set(PWR_SERV_MG90, pwr_pin_ctrl);
                break;
            case DRV_SERVO__RDS3235_ON:
                pwr_pin_ctrl = GPIO__STATE_HIGH;
            case DRV_SERVO__RDS3235_OFF:
                gpio__set(PWR_SERV_RDS3235, pwr_pin_ctrl);
                break;
            default:
                servo_ptr->state = EVENT__PARAM_NA;
                break;
        }
        drv_servo__pwm_cb(servo_ptr->pwm[0], 0, servo_ptr);
    }
}

static void drv_servo__pwm_cb(hw_pwm_id_e pwm, u32 cnt, void* ext_data) {
    drv_servo__struct_t* servo_ptr = ext_data;
    gpio__init(servo_ptr->en_pin, GPIO__DIR_OUT, NULL);
    if (servo_ptr->sub.next)
        list__exclude(&servo_ptr->sub, LIST__THIS);
    sw_timer__start(&servo_ptr->tmr, cnt ? servo_ptr->timeout : 0, drv_servo__tmr_cb, servo_ptr);
}


static void drv_servo__tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    drv_servo__struct_t* servo_ptr = ext_data;
    events__e res = servo_ptr->state;
    servo_ptr->state = EVENT__WAIT;
    servo_ptr->cb((servo_ptr-drv_servo__data) / sizeof(drv_servo__data[0]), res, servo_ptr->param, servo_ptr->ext_data);
}