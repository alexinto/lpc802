/***************************************************************************//**
 * @file drv_peletier.c.
 * @brief ���� � ��������� ��������� ������ ������ �������.
 * @author a.tushentsov.
 ******************************************************************************/
#include <math.h>
#include "System/sw_timer.h"
#include "hw_pwm.h"
#include "ads1115.h"
#include "hw_pwm.h"
#include "drv_peletier.h"

#define NO_TEMP 10000
#define PELETIER_FREQ 24000
#define PELETIER_INTERVAL 2500


typedef enum {
    PELETIER_IDLE    = 0,
    PELETIER_WORK    = 1,
    PELETIER_ERROR   = 2,
}peletier__state_e;

typedef enum {
    PELETIER_INIT     = 0,
    PELETIER_PREHEAT  = 1,
    PELETIER_HEAT     = 2,
    PELETIER_PRECOOL  = 3,
    PELETIER_COOL     = 4,
    PELETIER_WAIT     = 5,
}peletier__mode_e;

typedef struct {
    int prev_err;
    float kp, ki, kd, it;
}peletier__pid_t;

typedef struct {
    u8 adc_ch;
    int cur_percent, min_power, power;
    peletier__mode_e mode;
    hw_pwm_id_e pwm;
    peletier__state_e modul_state;
    drv_peletier_cmd_e cur_cmd;
    int cur_temp, set_temp, speed, mode_pin;
    peletier__pid_t pid;
    sw_timer__t speed_tmr;
    drv_peletier_cb_t cb;
    void* ext_data;
}peletier__struct_t;

static peletier__struct_t peletier_data[] = {{.cur_cmd = PELETIER_CMD_NONE, .cur_temp = NO_TEMP, .adc_ch = 2, .mode_pin = MCU__GPIO_P_D_8, .pwm = PWM_8, .speed = 20, .min_power = 160, .pid.kp = 0.5, .pid.ki = 0, .pid.kd = 0,},
                                             {.cur_cmd = PELETIER_CMD_NONE, .cur_temp = NO_TEMP, .adc_ch = 3, .mode_pin = MCU__GPIO_P_C_9, .pwm = PWM_9, .speed = 20, .min_power = 160, .pid.kp = 0.5, .pid.ki = 0, .pid.kd = 0,}};

static int temperature_r_to_temp (float volt);
static void drv_peletier__adc_cb(u8 id, u8 ch, events__e event, float adc, void* ext_data);
static void drv_peletier__tmr_cb(struct sw_timer__t *timer, void *ext_data);
static int peletier__pid(u16 cur_temp, u16 target_temp, peletier__pid_t* pid);

events__e drv_peletier__cmd_exec(drv_peletier_id_e id, drv_peletier_cmd_e cmd, int param, drv_peletier_cb_t cb, void* ext_data) {
    peletier__struct_t* data = &peletier_data[id];
    events__e res = (events__e)param;
    if (data->cur_cmd < PELETIER_CMD_NONE)
        return EVENT__BUSY;
    if ((cmd >= PELETIER_CMD_NONE) || (param > 2000))
        return EVENT__PARAM_NA;
    data->cur_cmd = cmd;
    data->cb = cb;
    data->ext_data = ext_data;
    switch(cmd) {
        case PELETIER_CMD_SET_POWER:
            if ((param > 400) || (param < 10))
                res = EVENT__PARAM_NA;
            else
                data->min_power = param;
            break;
        case PELETIER_CMD_SET_SPEED:
            if ((param > 50) || (param < 1))
                res = EVENT__PARAM_NA;
            else
                data->speed = param;
            break;
        case PELETIER_CMD_SET_KP:
            if ((param > 100) || (param < 1))
                res = EVENT__PARAM_NA;
            else
                data->pid.kp = (float)param / 10;
            break;
        case PELETIER_CMD_SET_TEMP:
            data->set_temp = (param - 1000) * 10;
            data->power = data->set_temp * data->set_temp / data->min_power / 1000;
            data->cur_percent = data->pid.prev_err = 0;
            data->pid.it = 0;
            data->modul_state = PELETIER_WORK;
            data->mode = PELETIER_INIT;
            gpio__init(data->mode_pin, GPIO__DIR_OUT, NULL);
            gpio__set(data->mode_pin, GPIO__STATE_HIGH);
            sw_timer__start(&data->speed_tmr, 100, drv_peletier__tmr_cb, data);
        case PELETIER_CMD_GET_TEMP:
            ads1115__get(0, data->adc_ch, drv_peletier__adc_cb, data);
            return EVENT__OK;
        case PELETIER_CMD_OFF:
            data->modul_state = PELETIER_IDLE;
            hw_pwm__ctrl(data->pwm, 0, 0, 0);
            sw_timer__stop(&data->speed_tmr);
            break;
    }
    data->cur_cmd = PELETIER_CMD_NONE;
    data->cb(id, EVENT__OK, 0, data->ext_data);
    return res;
}

static void drv_peletier__adc_cb(u8 id, u8 ch, events__e event, float adc, void* ext_data) {
    peletier__struct_t* data = ext_data;
    data->cur_temp = temperature_r_to_temp(adc);
    if (data->cur_cmd != PELETIER_CMD_NONE) {
        data->cur_cmd = PELETIER_CMD_NONE;
        data->cb((drv_peletier_id_e)(data - peletier_data), event, data->cur_temp / 10 + 1000, data->ext_data);
    }
}

static void drv_peletier__tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    static int power_fix_prev, fix_cnt;
    peletier__struct_t* data = ext_data;
    int power_fix = peletier__pid(data->cur_temp, data->set_temp, &data->pid), timeout = -1;
    switch(data->mode) {
        case PELETIER_INIT:
            data->cur_percent++;
            if (data->cur_percent >= power_fix) {
                data->cur_percent = data->power;
                timeout = PELETIER_INTERVAL;
                data->mode = PELETIER_PREHEAT;
            }
            break;
        case PELETIER_PREHEAT:
            if (power_fix < (-5 * data->pid.kp)) {
                data->mode = PELETIER_HEAT;
                timeout = 100;
            }
            else if (power_fix >= power_fix_prev) {
                if (fix_cnt++ >= 1) {
                    fix_cnt  = 0;
                    if (data->power < 1000)
                        data->power += power_fix / data->speed + 1;
                    data->cur_percent = data->power;
                }
            }
            else if (fix_cnt > 0)
                fix_cnt--;
            break;
        case PELETIER_HEAT:
            data->cur_percent = data->power + power_fix;
            break;
    }
    sw_timer__start(&data->speed_tmr, timeout, drv_peletier__tmr_cb, data);

    if (data->cur_percent < 0)
        data->cur_percent = 0;
    else if (data->cur_percent > 1000)
        data->cur_percent = 1000;

    hw_pwm__ctrl(data->pwm, PELETIER_FREQ, data->cur_percent, 0);
    power_fix_prev = peletier__pid(data->cur_temp, data->set_temp, &data->pid);
}



#define TEMP_R0 (100.00)
#define TEMP_A (3.9083E-3)       // A coef. of positive reverse transfer function
#define TEMP_B (-5.775E-7) 	     // B coef. of positive reverse transfer function
#define TEMP_D1 (255.819)        // D1 coef. of positive reverse transfer function
#define TEMP_D2 (9.14550)        // D2 coef. of positive reverse transfer function
#define TEMP_D3 (-2.92363)       // D3 coef. of positive reverse transfer function
#define TEMP_D4 (1.79090)        // D4 coef. of positive reverse transfer function

static int temperature_r_to_temp (float volt) {
  int res  = 0;
  float r = volt;
  float t, X;
  if (r < TEMP_R0) {
	  X = r / TEMP_R0 - 1;
	  t = TEMP_D1*X + TEMP_D2*pow(X,2) + TEMP_D3*pow(X,3) + TEMP_D4*pow(X,4);
  }
  else {
	  X = 1 - r / TEMP_R0;
	  X = pow(TEMP_A, 2) - 4 * TEMP_B * X;
	  X = sqrt(X) - TEMP_A;
	  t = X / (2 * TEMP_B);
  }
  res = (int)(t * 100);
  if (res < -10000)
      res = -10000;
  return res > 10000 ? 10000 : res ;
}


static int peletier__pid(u16 cur_temp, u16 target_temp, peletier__pid_t* pid) {
    int cur_err = target_temp - cur_temp;
    pid->it += pid->ki * cur_err;
    float res = pid->kp * cur_err + pid->it + pid->kd * (cur_err - pid->prev_err);
    pid->prev_err = target_temp - cur_temp;
    return (int)res;
}