/***************************************************************************//**
 * @file drv_fun.c.
 * @brief ���� � ��������� ���������� �������������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "mcu.h"
#include "drv_lamp.h"


typedef struct {
    int en_pin;
}drv_lamp__struct_t;

static drv_lamp__struct_t lamp_data[] = {{.en_pin = MCU__GPIO_P_B_15}};

events__e drv_lamp__cmd_exec(drv_lamp__id_e id, drv_lamp__cmd_e cmd, int param) {
    drv_lamp__struct_t* data = &lamp_data[id];
    if ((cmd >= LAMP_CMD_NONE) || (param > 60000))
        return EVENT__PARAM_NA;
    gpio__init(data->en_pin, GPIO__DIR_OUT, NULL);
    switch(cmd) {
        case LAMP_ON_OFF:
            gpio__set(data->en_pin, param ? GPIO__STATE_HIGH : GPIO__STATE_LOW);
            break;
    }
    return EVENT__OK;
}

