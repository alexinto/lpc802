﻿/***************************************************************************//**
 * @file drv_motor.c.
 * @brief  Драйвер шагового двигателя.
 * @author a.tushentsov.
 ******************************************************************************/

#include "target.h"
#include "System/list.h"
#include "System/sw_timer.h"
#include "encoder.h"
#include "drv_motor.h"

#define TMC_READ  0
#define TMC_WRITE 1
#define MOTOR_DRV_CMD_TIMEOUT 20000
#define USER_PARAM 0x0FFFFFFF

struct drv_motor__list_t;

typedef struct drv_motor__list_t {
    struct drv_motor__list_t* prev;   // Указатель на прерыдущий экземпляр мотора.
    struct drv_motor__list_t* next;   // Указатель на следующий экземпляр мотора.
} drv_motor__list_t;

typedef struct {
    u8 rw;
    tmc_5160__param_e param_id;
    int param_val;
}drv_motor__tmc_t;

typedef struct {
    drv_motor__list_t sub;
    drv_motor__cmd_e user_cmd;
    events__e state;
    tmc_5160__id_e tmc_id;
    encoder_id_e enc;
    int sw1_pin, motor_stop_pin, metric, param_val, cur_pos, option;
    sw_timer__t motor_tmr;
    void* ext_data;
    drv_motor__tmc_t* cur_cmd;
    drv_motor__cb_t cb;
}drv_motor__struct_t;

static drv_motor__struct_t drv_motor__data[] = {{.state = EVENT__WAIT, .tmc_id = TMC_5160_0,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_G_0, .motor_stop_pin = MCU__GPIO_P_F_4, .option = MCU__GPIO_P_G_8, .metric = 1250 / 5},     // motor0 12.5mm
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_1,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_G_1, .motor_stop_pin = MCU__GPIO_P_F_4, .option = MCU__GPIO_P_G_8, .metric = 1250 / 5},     // motor1
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_2,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_G_2, .motor_stop_pin = MCU__GPIO_P_F_5, .option = MCU__GPIO_P_G_8, .metric = 1250 / 5},     // motor2
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_3,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_G_4, .motor_stop_pin = MCU__GPIO_P_F_4, .option = MCU__GPIO_NONE, .metric = 1250 / 5},     // motor3
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_4,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_G_5, .motor_stop_pin = MCU__GPIO_P_F_4, .option = MCU__GPIO_NONE, .metric = 1250 / 5},     // motor4
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_5,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_G_3, .motor_stop_pin = MCU__GPIO_P_F_5, .option = MCU__GPIO_P_G_8, .metric = 1250 / 5},     // motor5
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_6,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_G_6, .motor_stop_pin = MCU__GPIO_P_F_5, .option = MCU__GPIO_NONE, .metric = 1250 / 5},     // motor6
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_7,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_G_7, .motor_stop_pin = MCU__GPIO_P_F_5, .option = MCU__GPIO_NONE, .metric = 1250 / 5},     // motor7
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_NONE,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_C_13, .motor_stop_pin = MCU__GPIO_P_C_13, .option = MCU__GPIO_NONE, .metric = 1250 / 5},     // motor8
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_NONE,  .enc = enc_none, .sw1_pin = MCU__GPIO_P_C_13, .motor_stop_pin = MCU__GPIO_P_C_13, .option = MCU__GPIO_NONE, .metric = 1250 / 5},     // motor9
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_NONE, .enc = enc_none, .sw1_pin = MCU__GPIO_P_C_13, .motor_stop_pin = MCU__GPIO_P_C_13, .option = MCU__GPIO_NONE, .metric = 1250 / 5},     // motor10
                                                {.state = EVENT__WAIT, .tmc_id = TMC_5160_NONE, .enc = enc_none, .sw1_pin = MCU__GPIO_P_C_13, .motor_stop_pin = MCU__GPIO_P_C_13, .option = MCU__GPIO_NONE, .metric = 1250 / 5}};    // motor11
//                                                           0                         1
static const drv_motor__tmc_t drv_motor__tmc_cmd_tbl[] = {TMC_READ, TMC_CUR_POS, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            2                                3                              4                       5                         6                            7                       8                            9                              10                              11                              12                     13                         14                         15                           16                      17                                18
                                                          TMC_WRITE, TMC_SET_HOME_SPEED, USER_PARAM, TMC_WRITE, TMC_DEST_POS, -25000, TMC_READ, TMC_POS_REACHED, USER_PARAM, TMC_WRITE, TMC_TOFF, 0, TMC_WRITE, TMC_CUR_POS, 0, TMC_WRITE, TMC_DEST_POS, 0, TMC_WRITE, TMC_TOFF, 3, TMC_WRITE, TMC_DEST_POS, 50, TMC_READ, TMC_POS_REACHED, USER_PARAM, TMC_WRITE, TMC_DEST_POS, -50, TMC_READ, TMC_POS_REACHED, USER_PARAM, TMC_WRITE, TMC_TOFF, 0, TMC_WRITE, TMC_CUR_POS, 0, TMC_WRITE, TMC_DEST_POS, 0, TMC_WRITE, TMC_TOFF, 3, TMC_WRITE, TMC_RESTORE_SPEED, USER_PARAM, TMC_READ, TMC_PARAM_NONE, 1,
//                                                            19                                 20                                      21                                22
                                                          TMC_WRITE, TMC_DEST_POS, USER_PARAM, TMC_READ, TMC_POS_REACHED, USER_PARAM, TMC_READ, TMC_CUR_POS, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            23                           24                           25
                                                          TMC_WRITE, TMC_MIN_SPEED, USER_PARAM, TMC_WRITE, TMC_STOP_SPEED, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            26                          27
                                                          TMC_WRITE, TMC_MAX_SPEED, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            28                    29                    30
                                                          TMC_WRITE, TMC_ACC, USER_PARAM, TMC_WRITE, TMC_DEC, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            31                        32
                                                          TMC_WRITE, TMC_I_HOLD, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            33                        34
                                                          TMC_WRITE, TMC_I_RUN, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            35                               36
                                                          TMC_WRITE, TMC_STEALTH_SPEED, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            37                            38
                                                          TMC_WRITE, TMC_INITIALIZE, 0, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
//                                                            39                                 40                                      41                               42
                                                          TMC_WRITE, TMC_DEST_POS, USER_PARAM, TMC_READ, TMC_POS_REACHED, USER_PARAM, TMC_READ, TMC_CUR_POS, USER_PARAM, TMC_READ, TMC_PARAM_NONE, USER_PARAM,
};

static const drv_motor__tmc_t* const drv_motor__scripts[] = {drv_motor__tmc_cmd_tbl, &drv_motor__tmc_cmd_tbl[2], &drv_motor__tmc_cmd_tbl[19], &drv_motor__tmc_cmd_tbl[23], &drv_motor__tmc_cmd_tbl[26], &drv_motor__tmc_cmd_tbl[28],
                                                             &drv_motor__tmc_cmd_tbl[31], &drv_motor__tmc_cmd_tbl[33], &drv_motor__tmc_cmd_tbl[35], &drv_motor__tmc_cmd_tbl[37], &drv_motor__tmc_cmd_tbl[39]};

static drv_motor__list_t drv_motor__cmd_list = {.next = &drv_motor__cmd_list, .prev = &drv_motor__cmd_list};

static void drv_motor__timer_cb(struct sw_timer__t *timer, void *ext_data);
static void tmc_cb(int tmc_id, events__e event, tmc_5160__param_e param_id, int param, void* ext_data);

/*******************************************************************************
 * Функция инициализации модуля DRV_MOTOR.
 ******************************************************************************/
events__e drv_motor__init(int motor, drv_motor__cb_t cb, void* ext_data) {
    drv_motor__struct_t* motor_ptr = &drv_motor__data[motor];
    if (motor_ptr->state != EVENT__WAIT)
        return EVENT__BUSY;
    gpio__init(motor_ptr->sw1_pin, GPIO__DIR_IN, NULL);
    gpio__init(motor_ptr->option, GPIO__DIR_IN, NULL);
    encoder__init(motor_ptr->enc, 0, 0);
    motor_ptr->cb = cb;
    motor_ptr->ext_data = ext_data;
    motor_ptr->state = EVENT__LOCK;
    motor_ptr->cur_cmd = (drv_motor__tmc_t*)drv_motor__scripts[DRV_MOTOR__INIT];
    if (!motor_ptr->sub.next)
        list__include(&drv_motor__cmd_list, &motor_ptr->sub, LIST__TAIL);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция перемещения двигателя к заданной точке.
 ******************************************************************************/
events__e drv_motor__cmd(int motor, drv_motor__cmd_e cmd, int param, drv_motor__cb_t cb, void* ext_data) {
    drv_motor__struct_t* motor_ptr = &drv_motor__data[motor];
    if (motor_ptr->state != EVENT__WAIT)
        return EVENT__BUSY;
    if ((cmd >= DRV_MOTOR__NONE) || (motor_ptr->tmc_id == TMC_5160_NONE))
        return EVENT__NOT_EXIST;
    if ((param < 0) || (param > 60000))
        return EVENT__PARAM_NA;
    motor_ptr->cb = cb;
    motor_ptr->ext_data = ext_data;
    motor_ptr->state = EVENT__LOCK;
    motor_ptr->cur_cmd = (drv_motor__tmc_t*)drv_motor__scripts[cmd];
    motor_ptr->user_cmd = cmd;
    motor_ptr->param_val = param; // 0.05 mm;
    if (!motor_ptr->sub.next)
        list__include(&drv_motor__cmd_list, &motor_ptr->sub, LIST__TAIL);
    sw_timer__start(&motor_ptr->motor_tmr, MOTOR_DRV_CMD_TIMEOUT, drv_motor__timer_cb, motor_ptr);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция обработки текущего состояния модуля DRV_MOTOR.
 ******************************************************************************/
void drv_motor__cout(void) {
    drv_motor__struct_t* motor_cmd_ptr;
    events__e res = EVENT__OK;
    if ((motor_cmd_ptr = list__foreach(&drv_motor__cmd_list, NULL, LIST__HEAD)) != NULL) {
        if (motor_cmd_ptr->cur_cmd->param_val != USER_PARAM)
            motor_cmd_ptr->param_val = motor_cmd_ptr->cur_cmd->param_val;
        if ((motor_cmd_ptr->cur_cmd->param_id == TMC_POS_REACHED) && ((gpio__get(motor_cmd_ptr->sw1_pin) == GPIO__STATE_HIGH) && (motor_cmd_ptr->param_val <= motor_cmd_ptr->cur_pos))) {
            list__exclude(&motor_cmd_ptr->sub, LIST__THIS);
            tmc_cb(motor_cmd_ptr->tmc_id, EVENT__OK, TMC_POS_REACHED, 1, motor_cmd_ptr);
        }
        else {
            res = motor_cmd_ptr->cur_cmd->rw ? tmc_5160__set_param(motor_cmd_ptr->tmc_id, motor_cmd_ptr->cur_cmd->param_id, motor_cmd_ptr->cur_cmd->param_id != TMC_TOFF ? motor_cmd_ptr->param_val * TMC_PPR / motor_cmd_ptr->metric :  motor_cmd_ptr->param_val, tmc_cb, motor_cmd_ptr) : tmc_5160__get_param(motor_cmd_ptr->tmc_id, motor_cmd_ptr->cur_cmd->param_id, tmc_cb, motor_cmd_ptr);
            if (res == EVENT__OK)
                list__exclude(&motor_cmd_ptr->sub, LIST__THIS);
        }
    }
    for(int i = 0; i < motor_none; i++) {
        drv_motor__struct_t* motor_ptr = &drv_motor__data[i];
        if (motor_ptr->state != EVENT__WAIT) {
            if (motor_ptr->state != EVENT__LOCK) {
                events__e res = ((motor_ptr->user_cmd == DRV_MOTOR__MOVE_OPT) && gpio__get(motor_ptr->option)) ? EVENT__NO_OPT : motor_ptr->state;
                if (motor_ptr->sub.next)
                    list__exclude(&motor_ptr->sub, LIST__THIS);
                int cur_pos_cb = motor_ptr->cur_pos, enc_cur_pos = encoder__get(motor_ptr->enc);
                if (enc_cur_pos >= 0)
                    cur_pos_cb = (enc_cur_pos * motor_ptr->metric) / ENC_PPR;
                sw_timer__stop(&motor_ptr->motor_tmr);
                motor_ptr->state = EVENT__WAIT;
                motor_ptr->cb(i, res, cur_pos_cb, motor_ptr->ext_data);
            }
        }
    }
}

/*******************************************************************************
 * Коллбэк операции из drv_motor__tmc_cmd_tbl.
 ******************************************************************************/
static void tmc_cb(int tmc_id, events__e event, tmc_5160__param_e param_id, int param, void* ext_data) {
    drv_motor__struct_t* motor_ptr = ext_data;
    if (param_id == TMC_CUR_POS)
        motor_ptr->cur_pos = (param * motor_ptr->metric) / TMC_PPR;
    if ((param_id != TMC_POS_REACHED) || (param))
        motor_ptr->cur_cmd++;
    if ((motor_ptr->cur_cmd->param_id < TMC_PARAM_NONE) && (event == EVENT__OK) && (!motor_ptr->sub.next)) {
        list__include(&drv_motor__cmd_list, &motor_ptr->sub, LIST__TAIL);
    }
    else {
        if ((motor_ptr->cur_cmd->param_id == TMC_PARAM_NONE) && (motor_ptr->cur_cmd->param_val))
            encoder__set(motor_ptr->enc, 0);
        motor_ptr->state = event;
        return;
    }
}

static void drv_motor__timer_cb(struct sw_timer__t *timer, void *ext_data) {
    drv_motor__struct_t* motor_ptr = ext_data;
    motor_ptr->state = EVENT__TIMEOUT;
}
