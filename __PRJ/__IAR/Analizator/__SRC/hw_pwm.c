/***************************************************************************//**
 * @file hw_pwm.c.
 * @brief ������ ���������� �������� � ����������� PWM.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "mcu.h"
#include "standard.h"
#include "hw_pwm.h"
#include "gpio.h"

#define PWM_PULSE_CNT 10  // ������� 2 !!!

typedef struct {
    TIM_TypeDef* tim;
    u32 out_pin, out_ch, out_ch_en;
    u8 af_function, tim_en_pos, ccr_ch;
    IRQn_Type irq;
}tim_descr_t;

typedef struct {
    events__e state;
    u32 pulse_cnt;
    hw_pwm_cb_t cb;
    void* ext_data;
}hw_pwm__struct_t;

static const tim_descr_t hw_pwm_tbl[] = { TIM2, MCU__GPIO_P_A_1, 0x06 << TIM_CCMR1_OC2M_Pos, TIM_CCER_CC2E_Pos, 1, 0, 1, TIM2_IRQn,                      //
                                          TIM3, MCU__GPIO_P_C_7, 0x06 << TIM_CCMR1_OC2M_Pos, TIM_CCER_CC2E_Pos, 2, 1, 1, TIM3_IRQn,                      //
                                          TIM4, MCU__GPIO_P_B_6, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E_Pos, 2, 2, 0, TIM4_IRQn,                      //+
                                          TIM5, MCU__GPIO_P_A_0, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E_Pos, 2, 3, 0, TIM5_IRQn,                      //+
                                          TIM12, MCU__GPIO_P_H_6, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E_Pos, 9, 6, 0, TIM8_BRK_TIM12_IRQn,           //+
                                          TIM13, MCU__GPIO_P_A_6, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E_Pos, 9, 7, 0, TIM8_UP_TIM13_IRQn,            //+
                                          TIM14, MCU__GPIO_P_A_7, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E_Pos, 9, 8, 0, TIM8_TRG_COM_TIM14_IRQn,       //+
                                          TIM9, MCU__GPIO_P_A_2, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E_Pos, 3, 16, 0, TIM1_BRK_TIM9_IRQn,            //+
                                          TIM10, MCU__GPIO_P_B_8, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E_Pos, 3, 17, 0, TIM1_BRK_TIM9_IRQn, // �� �������� - ��� ��������� ������, �� ������ �������!!!+
                                          TIM11, MCU__GPIO_P_B_9, 0x06 << TIM_CCMR1_OC1M_Pos, TIM_CCER_CC1E_Pos, 3, 18, 0, TIM1_TRG_COM_TIM11_IRQn};     //+

#define PWM_CNT (sizeof(hw_pwm_tbl) / sizeof(hw_pwm_tbl[0]))

static hw_pwm__struct_t hw_pwm__data[PWM_CNT];

/*******************************************************************************
 * ������� ������� pwm-������. ���������� �������� ��������� ������.
 ******************************************************************************/
void hw_pwm__ctrl(hw_pwm_id_e pwm, u32 freq, u16 percent, u8 inversion) {
    TIM_TypeDef* pwm_ptr = hw_pwm_tbl[pwm].tim;
    tim_descr_t* pwm_descr_ptr = (tim_descr_t*)&hw_pwm_tbl[pwm];
    hw_pwm__struct_t* pwm_data_ptr = &hw_pwm__data[pwm];
    u32* ccr_reg = (u32*)(&pwm_ptr->CCR1 + pwm_descr_ptr->ccr_ch);
    u32 timer_on = 0, temp_psc = 48000, temp_arr = 999;
    u32* rcc_apb = pwm_descr_ptr->tim_en_pos < 16 ? (u32*)&RCC->APB1ENR : (u32*)&RCC->APB2ENR;
    pwm_ptr->DIER = 0;
    if (freq) {
        timer_on = TIM_CR1_CEN;
        gpio__af_init(pwm_descr_ptr->out_pin, 0, pwm_descr_ptr->af_function);
        *rcc_apb |= 1 << pwm_descr_ptr->tim_en_pos;
        freq = temp_psc/freq;
        pwm_ptr->PSC = freq > 1 ? freq - 1 : freq;
        pwm_ptr->ARR = temp_arr;
        pwm_ptr->CCMR1 = pwm_descr_ptr->out_ch;
        pwm_ptr->CCER = (inversion ? 3 : 1) << pwm_descr_ptr->out_ch_en;
        pwm_ptr->BDTR = TIM_BDTR_MOE;
        temp_arr++;
        *ccr_reg = temp_arr * percent / 1000;
    }
    else
        *ccr_reg = 0;
    pwm_data_ptr->state = EVENT__WAIT;
    pwm_ptr->CR1 = timer_on;
}

/*******************************************************************************
 * ������� ��������� pwm-������ �������� ������������ ������� (���).
 ******************************************************************************/
events__e hw_pwm__set(hw_pwm_id_e pwm, u32 freq, u16 width, hw_pwm_cb_t cb, void* ext_data) {
    hw_pwm__struct_t* pwm_data_ptr = &hw_pwm__data[pwm];
    events__e res = EVENT__BUSY;
    if (pwm_data_ptr->state == EVENT__WAIT) {
        u32 timer_on = 0, temp_arr = 1000000 / freq;
        TIM_TypeDef* pwm_ptr = hw_pwm_tbl[pwm].tim;
        tim_descr_t* pwm_descr_ptr = (tim_descr_t*)&hw_pwm_tbl[pwm];
        u32* ccr_reg = (u32*)(&pwm_ptr->CCR1 + pwm_descr_ptr->ccr_ch);
        pwm_ptr->DIER = 0;
        pwm_data_ptr->cb = cb;
        pwm_data_ptr->ext_data = ext_data;
        pwm_data_ptr->pulse_cnt = 0;
        res = EVENT__OK;
        if (freq) {
            u32* rcc_apb = pwm_descr_ptr->tim_en_pos < 16 ? (u32*)&RCC->APB1ENR : (u32*)&RCC->APB2ENR;
            timer_on = TIM_CR1_CEN;
            gpio__af_init(pwm_descr_ptr->out_pin, 0, pwm_descr_ptr->af_function);
            *rcc_apb |= 1 << pwm_descr_ptr->tim_en_pos;
            pwm_ptr->PSC = 48;
            pwm_ptr->ARR = temp_arr;
            pwm_ptr->CCMR1 = pwm_descr_ptr->out_ch;
            pwm_ptr->CCER = 1 << pwm_descr_ptr->out_ch_en;
            *ccr_reg = width;
            pwm_data_ptr->state = EVENT__LOCK;
            pwm_ptr->SR = 0;
            pwm_ptr->DIER = 1;
            NVIC_SetPriority(pwm_descr_ptr->irq, 1);
            NVIC_EnableIRQ(pwm_descr_ptr->irq);
        }
        else {
            *ccr_reg = 0;
            pwm_data_ptr->state = EVENT__OK;
        }
        pwm_ptr->CR1 = timer_on;
    }
    return res;
}

events__e hw_pwm__get(hw_pwm_id_e pwm) {
    return hw_pwm__data[pwm].state == EVENT__WAIT ? EVENT__OK : EVENT__BUSY;
}


/*******************************************************************************
 * ������� ��������� �������� ��������� ������ PWM.
 ******************************************************************************/
void hw_pwm__cout() {
    for(u8 i = 0; i < PWM_CNT; i++) {
        hw_pwm__struct_t* pwm_descr_ptr = &hw_pwm__data[i];
        if ((pwm_descr_ptr->state != EVENT__WAIT) && (pwm_descr_ptr->state != EVENT__LOCK)) {
            pwm_descr_ptr->state = EVENT__WAIT;
            if (pwm_descr_ptr->cb)
                pwm_descr_ptr->cb((hw_pwm_id_e)i, pwm_descr_ptr->pulse_cnt, pwm_descr_ptr->ext_data);
        }
    }
}

/*******************************************************************************
 * ������� ��������� ISR ������ PWM.
 ******************************************************************************/
void hw_pwm__IRQ_handler(u8 pwm) {
    TIM_TypeDef* pwm_ptr = hw_pwm_tbl[pwm].tim;
    hw_pwm__struct_t* pwm_data_ptr = &hw_pwm__data[pwm];
    u32* ccr_reg = (u32*)(&pwm_ptr->CCR1 + hw_pwm_tbl[pwm].ccr_ch);
    pwm_ptr->SR = 0;
    switch(++pwm_data_ptr->pulse_cnt) {
    case PWM_PULSE_CNT - 1:
        pwm_ptr->DIER = 0x3;
        break;
    case PWM_PULSE_CNT:
        *ccr_reg = 0;
        break;
    case PWM_PULSE_CNT + 1:
        pwm_ptr->CR1 = pwm_ptr->DIER = 0;
        pwm_data_ptr->state = EVENT__OK;
        break;
    }
}
