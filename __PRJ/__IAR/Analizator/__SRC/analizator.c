/***************************************************************************//**
 * @file analizator.c.
 * @brief  �������� ������ �����������.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stddef.h>
#include "gpio.h"
#include "target.h"
#include "crc.h"
#include "System/sw_timer.h"
#include "System/framework.h"
#include "supervisor.h"
#include "i2c.h"
#include "spi.h"
#include "dac.h"
#include "DRV/uart.h"
#include "stdio.h"
#include "string.h"
#include "hw_pwm.h"
#include "drv_motor.h"
#include "drv_servo.h"
#include "cmd_fsm.h"
#include "drv_led.h"
#include "ads1115.h"
#include "drv_fun.h"

struct uarts__struct_t;

typedef struct uarts__struct_t {
    struct uarts__struct_t* prev;
    struct uarts__struct_t* next;
    u8* data;
    int len;
} uarts__struct_t;

static struct {
    uarts__struct_t uart_list, uart_fsm_cb_sub, uart_tx_err_sub;
    u8 test_buff[100], usb_rx_buff[UART_BUFF_SIZE], usb_tx_buff[UART_BUFF_SIZE], msg_error[5];
    u32 freq;
    sw_timer__t timer, motor_tmr;
    u8 button_on, button_on_isr;
    uart__sub_t uart, usb;
}analizator = {.uart_list.next = &analizator.uart_list, .uart_list.prev = &analizator.uart_list, .uart_fsm_cb_sub.data = analizator.usb_tx_buff, .uart_tx_err_sub.data = analizator.msg_error, .uart_tx_err_sub.len = 5};

static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void test_gpio_cb(int gpio, gpio__state_e pin_state);
static void uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void button__cout();
static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data);
static void cmd_fsm__event_hd(cmd_fsm__message_t* data, void* ext_data);

static void motor_init_cb(int motor, events__e event, int param, void* ext_data);

static const drv_motor_id_e motors_id[] = {MOTOR_X, MOTOR_Y, MOTOR_Z, MOTOR_M1, MOTOR_M2, MOTOR_P, MOTOR_DOOR1, MOTOR_DOOR2, motor_none};

#include "ds18b20.h"

void main() {
#ifdef _DEBUG_
    DBGMCU->CR = DBGMCU_CR_DBG_STOP;
    DBGMCU->APB1FZ = 0xFFFFFFFF;
    DBGMCU->APB2FZ = 0xFFFFFFFF;
#endif
    supervisor__init();
    sw_timer__init(NULL);
    uart__init();
    spi__init(TMC_5160_SPI, TMC_5160_SPI_SETTINGS);

    cmd_fsm__init(cmd_fsm__event_hd);
    drv_led__init(DRV_LED_0);
    drv_motor__init(*motors_id, motor_init_cb, (void*)motors_id);
    drv_servo__init();
    drv_fun__init();
//    ds18b20__get(DS18B20_1, NULL, NULL);

//    spi__init(SPI_TERMO, SPI_SETTINGS);

    gpio__init(LED_GREEN, GPIO__DIR_OUT, NULL);
    gpio__init(LED_BLUE, GPIO__DIR_OUT, NULL);
    gpio__init(USER_BT, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | GPIO__INT_EDGE_FALING, test_gpio_cb);

    uart__open(&analizator.usb, USB_UART_1, UART_SETTINGS, uart_event_hd, NULL);

    ads1115__init(0, I2C_TERMO, (0x90 >> 1), VOLT_0_256, SPS_128);

    sw_timer__start(&analizator.timer, 500, timer_cb, NULL);

    while(1) {
        button__cout();
        framework__cout();
        hw_pwm__cout();
        drv_servo__cout();
        drv_motor__cout();
        cmd_fsm__cout();
    }
}

static void test_gpio_cb(int gpio, gpio__state_e pin_state) {
    analizator.button_on_isr = (pin_state == GPIO__STATE_HIGH);
}



static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    sw_timer__start(timer, -1, timer_cb, NULL);
    gpio__set(LED_BLUE, GPIO__STATE_TOGGLE);
//    i2c__tx(I2C_TERMO, I2C_ADDR, analizator.spi_1_buff, 2, i2c_termo_cb);
}


static void button__cout() {
    if (analizator.button_on != analizator.button_on_isr)
        sw_timer__start(&analizator.timer, 200, timer_cb, NULL);
}


static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
    switch(event) {
    case EVENT__OPEN:
            uart__rx(&analizator.usb, UART__FRAME_3_5, analizator.usb_rx_buff, 100, 0, uart_cb, NULL);
        break;
    case EVENT__CLOSE:
        uart__open(sub, sub->uart_id, UART_SETTINGS, uart_event_hd, NULL);
        break;
    }
}

static void uart_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    cmd_fsm__message_t* msg = (cmd_fsm__message_t*)analizator.usb_rx_buff;
    uart__rx(&analizator.usb, UART__FRAME_3_5, analizator.usb_rx_buff, UART_BUFF_SIZE, 0, uart_cb, NULL);
    if (buff != analizator.usb_rx_buff) {
        uarts__struct_t* uart_sub = list__exclude(&analizator.uart_list, LIST__HEAD);
        if (uart_sub)
            uart__tx(&analizator.usb, uart_sub->data, uart_sub->len, uart_cb, NULL);
        return;
    }
    cmd_fsm__message_t* msg_error = (cmd_fsm__message_t*)analizator.msg_error;
    msg_error->id = ID_KERNEL;
    msg_error->cmd = KERNEL_ANSWER;
    if (event != EVENT__OK)
        msg_error->param = FSM_ERROR;
    else if ((crc__8_ccitt(analizator.usb_rx_buff, len)) || (len < 5))
        msg_error->param = FSM_CRC_ERROR;
    else {
        analizator.usb_rx_buff[len - 1] = 0;
        msg_error->param = cmd_fsm__cmd_set(msg, NULL);
    }
    if (msg_error->param) {
        analizator.msg_error[4] = crc__8_ccitt((u8*)&analizator.msg_error, 4);
        if (uart__tx(&analizator.usb, analizator.msg_error, 5, uart_cb, NULL) != EVENT__OK)
            if (analizator.uart_tx_err_sub.next == NULL)
                list__include(&analizator.uart_list, &analizator.uart_tx_err_sub, LIST__TAIL);
    }
}

static void motor_init_cb(int motor, events__e event, int param, void* ext_data) {
    drv_motor_id_e* motor_id = ext_data;
    motor_id++;
    if (*motor_id < motor_none)
        drv_motor__init(*motor_id, motor_init_cb, (void*)motor_id);
    else
        gpio__init(USER_BT, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING | GPIO__INT_EDGE_FALING, test_gpio_cb);
}

static void cmd_fsm__event_hd(cmd_fsm__message_t* data, void* ext_data) {
    cmd_fsm__message_t* msg = (cmd_fsm__message_t*)analizator.usb_tx_buff;
    *(int*)ext_data = data->param;
    int len = 0;
    while(data[len].id != 0) {
        memcpy(&msg[len], &data[len], CMD_LEN);
        len++;
    }
    len *= CMD_LEN;
    analizator.usb_tx_buff[len] = crc__8_ccitt((u8*)msg, len);
    if (uart__tx(&analizator.usb, analizator.usb_tx_buff, len + 1, uart_cb, NULL) != EVENT__OK) {
        analizator.uart_fsm_cb_sub.len = len + 1;
        if (analizator.uart_fsm_cb_sub.next == NULL)
            list__include(&analizator.uart_list, &analizator.uart_fsm_cb_sub, LIST__TAIL);
    }
}