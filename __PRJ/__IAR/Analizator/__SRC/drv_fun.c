/***************************************************************************//**
 * @file drv_fun.c.
 * @brief ���� � ��������� ���������� �������������.
 * @author a.tushentsov.
 ******************************************************************************/
#include <math.h>
#include "System/sw_timer.h"
#include "hw_pwm.h"
#include "hw_pwm.h"
#include "drv_fun.h"

typedef enum {
    FUN_IDLE = 0,
    FUN_WORK = 1,
}drv_fun__state_e;

typedef struct {
    drv_fun__state_e state;
    hw_pwm_id_e pwm;
    u16 freq, percent;
}drv_fun__struct_t;

static drv_fun__struct_t fun_data[] = {{.pwm = PWM_6, .freq = 100, .percent = 100}, {.pwm = PWM_5, .freq = 100, .percent = 100}};

events__e drv_fun__init() {
    for(int i = 0; i < FUN_NONE; i++)
        hw_pwm__ctrl(fun_data[i].pwm, 100, 0, 1);
    return EVENT__OK;
}

events__e drv_fun__cmd_exec(drv_fun__id_e id, drv_fun__cmd_e cmd, int param) {
    drv_fun__struct_t* data = &fun_data[id];
    if ((cmd >= FUN_CMD_NONE) || (param > 24000))
        return EVENT__PARAM_NA;
    u16 freq = data->freq;
    switch(cmd) {
        case FUN_SET_SPEED:
            if (param > 100)
                return EVENT__PARAM_NA;
            data->percent = param / 2 + 50;
            break;
        case FUN_SET_FREQ:
            freq = data->freq = param;
            break;
        case FUN_ON_OFF:
            data->state = param ? FUN_WORK : FUN_IDLE;
            break;
    }
    hw_pwm__ctrl(data->pwm, data->state ? freq : 0, data->percent * 10, 1);
    return EVENT__OK;
}

