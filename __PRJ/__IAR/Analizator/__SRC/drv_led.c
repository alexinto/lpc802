/***************************************************************************//**
 * @file drv_led.c.
 * @brief  ������� ������ �����������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "mcu.h"
#include "hw_pwm.h"
#include "drv_led.h"

typedef struct {
    int ch_en[8];
    hw_pwm_id_e pwm_id;
}drv_led__cfg_t;

typedef struct {
    u8 percent, leds;
    u32 freq;
    drv_led__cfg_t* cfg;
}drv_led__struct_t;

static const drv_led__cfg_t drv_led__cfg_tbl[] = {{.ch_en[0] = MCU__GPIO_P_D_13, .ch_en[1] = MCU__GPIO_P_D_14, .ch_en[2] = MCU__GPIO_P_D_15, .ch_en[3] = MCU__GPIO_P_E_0, .ch_en[4] = MCU__GPIO_P_E_2, .ch_en[5] = MCU__GPIO_P_E_3, .ch_en[6] = MCU__GPIO_P_E_4, .ch_en[7] = MCU__GPIO_P_E_5, .pwm_id = PWM_7},};

static drv_led__struct_t led_data[] = {{.cfg = (drv_led__cfg_t*)drv_led__cfg_tbl, .freq = 1000, .percent = 50},};


events__e drv_led__init(drv_led__id_e led_id) {
    for(int i = 0; i < 8; i++) {
        gpio__init(led_data[led_id].cfg->ch_en[i], GPIO__DIR_OUT, NULL);
        gpio__set(led_data[led_id].cfg->ch_en[i], GPIO__STATE_LOW);
    }
    return EVENT__OK;
}


events__e drv_led__cmd_exec(drv_led__id_e led_id, drv_led__cmd_e cmd, int param) {
    if (led_id >= DRV_LED_NONE)
        return EVENT__NOT_EXIST;
    if (cmd >= DRV_LED_CMD_NONE)
        return EVENT__PARAM_NA;
    drv_led__struct_t* data = &led_data[led_id];
    switch(cmd) {
        case DRV_LED_SET:
            for(int i = 0; i < 8; i++)
                gpio__set(led_data[led_id].cfg->ch_en[i], (param & (1 << i)) ? GPIO__STATE_HIGH : GPIO__STATE_LOW);
            data->leds = param;
            if (!data->leds)
                hw_pwm__ctrl(data->cfg->pwm_id, 0, 0, 0);
            break;
        case DRV_LED_SET_BRIGHT:
            data->percent = param;
            if (data->percent > 100)
                data->percent = 100;
            break;
        case DRV_LED_SET_FREQ:
            data->freq = param;
            break;
    }
    if (data->leds)
        hw_pwm__ctrl(data->cfg->pwm_id, data->freq, data->percent * 10, 0);
    return data->freq && data->percent ? (events__e)data->leds : EVENT__OK;
}


