/***************************************************************************//**
 * @file cmd_fsm.c.
 * @brief ���� � ��������� ��������� ������ �����������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "string.h"
#include "target.h"
#include "cmd_fsm.h"
#include "drv_motor.h"
#include "drv_led.h"
#include "drv_servo.h"
#include "drv_peletier.h"
#include "drv_fun.h"
#include "drv_lamp.h"

typedef cmd_fsm__error_e (*cmd_fsm__exec_t)(u8 id, u8 cmd, u16 param, void* ext_data);

typedef struct {
    u8 id_from, id_to;
    cmd_fsm__exec_t func;
}cmd_fsm__func_t;

typedef enum {
    BOARD_LEFT  = 0,
    BOARD_RIGHT = 1,
}cmd_fsm__board_ver_e;

static cmd_fsm__error_e cmd_fsm__motor_exec(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static cmd_fsm__error_e cmd_fsm__peletier(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static cmd_fsm__error_e cmd_fsm__led_exec(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static cmd_fsm__error_e cmd_fsm__servo(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static cmd_fsm__error_e cmd_fsm__fun(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static cmd_fsm__error_e cmd_fsm__lamp(u8 dev_id, u8 cmd, u16 param, void* ext_data);
static cmd_fsm__error_e cmd_fsm__kernel(u8 dev_id, u8 cmd, u16 param, void* ext_data);

                                                //   ID to ID
static const cmd_fsm__func_t cmd_fsm__exec_tbl[] = {0x01, 0x0C, cmd_fsm__motor_exec,                // 12 �������
                                                    0x0D, 0x0E, cmd_fsm__peletier,                  // 2 ������ �������
                                                    0x0F, 0x14, cmd_fsm__led_exec,                  // 6 ������������ �������
                                                    0x15, 0x20, cmd_fsm__servo,                     // 12 ���������������
                                                    0x21, 0x22, cmd_fsm__fun,                       // 2 �����������
                                                    0x30, 0x30, cmd_fsm__lamp,                      // 1 �����
                                                    ID_KERNEL, ID_KERNEL, cmd_fsm__kernel,          // 0xFA �������� ����
                                                    0x00, 0x00, 0x00};

static struct {
    int msg_idx;
    cmd_fsm__state_e fsm_state;
    cmd_fsm__message_t msg[MSG_BUFF];
    cmd_fsm__ev_hd_t event_handler;
    void* ext_data;
}cmd_data;

static void cmd_fsm__motor_cb(int motor, events__e event, int param, void* ext_data);
static void cmd_fsm__servo_cb(int motor, events__e event, int param, void* ext_data);
static void cmd_fsm__peletier_cb(drv_peletier_id_e id, events__e event, int param, void* ext_data);
static cmd_fsm__error_e event_to_fsm(events__e event);

/*******************************************************************************
 * ������� ������������� ������ CMD_FSM.
 ******************************************************************************/
void cmd_fsm__init(cmd_fsm__ev_hd_t event_handler) {
    cmd_data.event_handler = event_handler;
    gpio__init(BOARD_SELECTION_PIN, GPIO__DIR_IN, NULL);
}

/*******************************************************************************
 * ������� - ���������� ��������� ������ CMD_FSM.
 ******************************************************************************/
void cmd_fsm__cout(void) {
    cmd_fsm__message_t* cur_msg;
    switch(cmd_data.fsm_state) {
    case FSM_DEINIT:
        cmd_data.fsm_state = FSM_IDLE;
        break;
    case FSM_INIT:
        break;
    case FSM_IDLE:
        break;
    case FSM_WORK:
        cur_msg = &cmd_data.msg[cmd_data.msg_idx];
        if (cur_msg->id != 0) {
            cmd_fsm__func_t* msg_descr_idx = (cmd_fsm__func_t*)cmd_fsm__exec_tbl;
            while (msg_descr_idx->id_from != 0) {
                if ((cur_msg->id <= msg_descr_idx->id_to) && (cur_msg->id >= msg_descr_idx->id_from)) {
                    cur_msg->param = msg_descr_idx->func(cur_msg->id - msg_descr_idx->id_from, cur_msg->cmd, cur_msg->param, cur_msg);
                    break;
                }
                msg_descr_idx++;
            }
            if (msg_descr_idx->id_from == 0) {
                cur_msg->param = FSM_NOT_EXIST;
                cur_msg->cmd &= ~0x80;
            }
            cmd_data.msg_idx++;
        }
        else {
            for(cur_msg = cmd_data.msg; (!(cur_msg->cmd & 0x80)) && (cur_msg->id); cur_msg++);
            if (!cur_msg->id) {
                cmd_data.fsm_state = FSM_IDLE;
                cmd_data.event_handler(cmd_data.msg, cmd_data.ext_data);
            }
        }
    }
}

/*******************************************************************************
 * ������� - ��������� ������� �� ��������� ������ ������.
 ******************************************************************************/
cmd_fsm__error_e cmd_fsm__cmd_set(cmd_fsm__message_t* data, void* ext_data) {
    cmd_fsm__error_e res = FSM_BUSY;
    if (cmd_data.fsm_state == FSM_IDLE) {
        int idx  = -1;
        do {
            idx++;
            memcpy(&cmd_data.msg[idx], &data[idx], sizeof(data[0]));
        } while(data[idx].id != 0);
        cmd_data.ext_data = ext_data;
        cmd_data.msg_idx = 0;
        cmd_data.fsm_state = FSM_WORK;
        res = FSM_OK;
    }
    return res;
}

/*******************************************************************************
 * ������� ���������� ������ ������ DRV_MOTOR.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__motor_exec(u8 id, u8 cmd, u16 param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    events__e res = drv_motor__cmd(id, (drv_motor__cmd_e)cmd, param, cmd_fsm__motor_cb, ext_data);
    if (res == EVENT__OK)
        cur_msg->cmd |= 0x80;  // ���� ��������...
    return event_to_fsm(res);
}

/*******************************************************************************
 * �������, �� ���������� ���������� ������ ������ DRV_MOTOR.
 ******************************************************************************/
static void cmd_fsm__motor_cb(int motor, events__e event, int param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    cur_msg->cmd &= ~0x80;
    cur_msg->param = (event != EVENT__OK) ? event_to_fsm(event) : param;
}

/*******************************************************************************
 * ������� ���������� ������ ������ DRV_PELETIER.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__peletier(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    cur_msg->cmd |= 0x80;  // ���� ��������...
    events__e res = drv_peletier__cmd_exec((drv_peletier_id_e)dev_id, (drv_peletier_cmd_e)cmd, param, cmd_fsm__peletier_cb, ext_data);
    if (res < EVENT__OK) {
        cur_msg->cmd &= ~0x80;
        return event_to_fsm(res);
    }
    return (cmd_fsm__error_e)res;
}

static void cmd_fsm__peletier_cb(drv_peletier_id_e id, events__e event, int param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    cur_msg->cmd &= ~0x80;
    cur_msg->param = (event != EVENT__OK) ? event_to_fsm(event) : param;
}

/*******************************************************************************
 * ������� ���������� ������ ������ DRV_LED.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__led_exec(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
    int res = drv_led__cmd_exec((drv_led__id_e)dev_id, (drv_led__cmd_e)cmd, param);
    return res < 0 ? event_to_fsm((events__e)res) : (cmd_fsm__error_e)res;
}

/*******************************************************************************
 * ������� ���������� ������ ������ ���������������.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__servo(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    events__e res = drv_servo__cmd(dev_id, (drv_servo__cmd_e)cmd, param, cmd_fsm__servo_cb, ext_data);
    if (res == EVENT__OK)
        cur_msg->cmd |= 0x80;  // ���� ��������...
    return event_to_fsm(res);
}

/*******************************************************************************
 * �������, �� ���������� ���������� ������ ������ DRV_SERVO.
 ******************************************************************************/
static void cmd_fsm__servo_cb(int motor, events__e event, int param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    cur_msg->cmd &= ~0x80;
    cur_msg->param = (event != EVENT__OK) ? event_to_fsm(event) : param;
}

/*******************************************************************************
 * ������� ���������� ������ ������ ������������.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__fun(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    cur_msg->cmd &= ~0x80;
    events__e res = drv_fun__cmd_exec((drv_fun__id_e)dev_id, (drv_fun__cmd_e)cmd, param);
    return res != EVENT__OK ? event_to_fsm(res) : (cmd_fsm__error_e)param;
}

static cmd_fsm__error_e cmd_fsm__lamp(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
    cmd_fsm__message_t* cur_msg = ext_data;
    cur_msg->cmd &= ~0x80;
    events__e res = drv_lamp__cmd_exec((drv_lamp__id_e)dev_id, (drv_lamp__cmd_e)cmd, param);
    return res != EVENT__OK ? event_to_fsm(res) : (cmd_fsm__error_e)param;
}


/*******************************************************************************
 * ������� ���������� ������ ������ DRV_KERNEL.
 ******************************************************************************/
static cmd_fsm__error_e cmd_fsm__kernel(u8 dev_id, u8 cmd, u16 param, void* ext_data) {
//    cmd_fsm__message_t* cur_msg = ext_data;
    cmd_fsm__error_e res = FSM_PARAM_NA;
    switch(cmd) {
        case KERNEL_GET_VERSION:
            res = (cmd_fsm__error_e)PROGRAM_VERSION;
            break;
        case KERNEL_GET_BOARD_VER:
            res = (cmd_fsm__error_e)gpio__get(BOARD_SELECTION_PIN);
            break;
    }
    return res;
}

static cmd_fsm__error_e event_to_fsm(events__e event) {
    cmd_fsm__error_e res = FSM_ERROR;
    switch(event) {
        case EVENT__TIMEOUT:
            res = FSM_DEV_TIMEOUT;
            break;
        case EVENT__PARAM_NA:
            res = FSM_PARAM_NA;
            break;
        case EVENT__BUSY:
            res = FSM_BUSY;
            break;
        case EVENT__OK:
            res = FSM_OK;
            break;
        case EVENT__NOT_EXIST:
            res = FSM_NOT_EXIST;
            break;
        case EVENT__NO_OPT:
            res = FSM_NO_OPTION;
            break;
    }
    return res;
}
