/***************************************************************************//**
 * @file menu_tree.c.
 * @brief  ���������� ������ � ������� ����.
 * @author a.tushentsov.
 ******************************************************************************/

#include "menu_tree.h"

/*******************************************************************************
 * �������- ���������� ������ ������ ����.
 ******************************************************************************/
menu__item__t* menu__cmd_exec(menu__cmd__e cmd, menu__item__t* menu_item, menu__item__t* new_item) {
    menu__item__t* res = new_item;
    switch(cmd) {
        case MENU__CMD_ADD_CHILD:
            new_item->parrent = menu_item;
            new_item->child = menu_item->child;
            if (menu_item->child)
                menu_item->child->parrent = new_item;
            menu_item->child = new_item;
            break;
        case MENU__CMD_DEL_CHILD: // todo
            break;
        case MENU__CMD_ADD_ITEM:
            new_item->prev = menu_item;
            new_item->next = menu_item->next;
            new_item->parrent = menu_item->parrent;
            new_item->next = menu_item->next;
            if (menu_item->next)
                menu_item->next->prev = new_item;
            menu_item->next = new_item;
            break;
        case MENU__CMD_DEL_ITEM:   // todo
            break;
    }
    return res;
}
