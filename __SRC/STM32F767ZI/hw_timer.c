﻿/***************************************************************************//**
 * @file hw_timer.c.
 * @brief Молуль аппаратного таймера.
 * @authors a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "target.h"
#include "System/framework.h"
#include "System/hw_timer.h"


// Внутренняя структура модуля аппаратного таймера.
typedef struct {
    sw_timer__sys_time_t cur_time;           // Текущее время.
    sw_timer__sys_time_t time_alrm;          // Время сработки.
    hw_timer__isr_handler_t isr_handler;     // Указатель на обработчик, вызывемый в прерывании аппаратного таймера.
    sw_timer__event_handler_t event_handler; // Указатель на обработчик программных таймеров.
    int init;                                // Флаг того, что проинициализирован модуль таймера.
} hw_timer__t;

static hw_timer__t hw_timer;

/*******************************************************************************
 * Функция инициализации аппаратного таймера, используемого для реализации программных таймеров.
 ******************************************************************************/
events__e hw_timer__init(hw_timer__isr_handler_t isr_handler, sw_timer__event_handler_t event_handler) {
    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
    TIM1->PSC = 11719;
    TIM1->ARR = 4095;
    TIM1->CCR1 = 0;
    TIM1->CR1 = 17;
    TIM1->SR = 0;
    TIM1->DIER = 1;
    NVIC_SetPriority(TIM1_CC_IRQn, 0);
    NVIC_SetPriority(TIM1_UP_TIM10_IRQn, 0);
    NVIC_EnableIRQ(TIM1_CC_IRQn);
    NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
    hw_timer.isr_handler = isr_handler;
    hw_timer.event_handler = event_handler;
    hw_timer.cur_time.timestamp = 0;
    hw_timer.cur_time.ms = 0;
    hw_timer.time_alrm.ms = -1;
    hw_timer.init = 1;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция внешнего вызова для обработки текущего состояния модуля hw_timer.
 ******************************************************************************/
void hw_timer__cout(void) {

}

/*******************************************************************************
 * Функция получения системного времени.
 ******************************************************************************/
void hw_timer__sys_time_get(sw_timer__sys_time_t* time) {
    int time_ss;
    sw_timer__sys_time_t res;
    CRITICAL_SECTION_ON
        time_ss = TIM1->CNT;
        time_ss = 4095 - time_ss;
        time_ss = time_ss >> 2;
        hw_timer.cur_time.ms = time_ss - (time_ss / 42);
        res = hw_timer.cur_time;
        if (TIM1->SR & 1) {
            res.timestamp++;
            res.ms = 0;
        }
    if (time)
        *time = res;
    CRITICAL_SECTION_OFF
}

/*******************************************************************************
 * Функция вызовет срабатывание прерывания таймера в заданное время.
 ******************************************************************************/
void hw_timer__start(sw_timer__sys_time_t time) {
    int time_ss, time_ss_cur;
    if (time.ms < 0) {  // Остановка.
        TIM1->DIER = 1;
        return;
    }
    hw_timer.time_alrm = time;
    CRITICAL_SECTION_ON
    time_ss = (hw_timer.time_alrm.ms) << 2;
    time_ss = time_ss + (time_ss / 41);
    time_ss = 4095 - time_ss;
    time_ss_cur = TIM1->CNT;
    if (hw_timer.time_alrm.timestamp <= hw_timer.cur_time.timestamp) {
        if (((time_ss_cur - time_ss) < 4) || (hw_timer.time_alrm.timestamp < hw_timer.cur_time.timestamp)) {
            if ((time_ss = time_ss_cur - 4) < 0) {
                hw_timer.time_alrm.timestamp++;
                time_ss += 4096;
                hw_timer.time_alrm.ms = 0;
            }
        }
        TIM1->CCR1 = time_ss;
        TIM1->DIER = 3;
    }
    CRITICAL_SECTION_OFF
}

/*******************************************************************************
 * Функция-обработчик прерывания аппаратного таймера.
 ******************************************************************************/
void hw_timer__RTC_IRQHandler(void) {
    hw_timer.isr_handler();
}

/*******************************************************************************
 * Функция-обработчик прерывания аппаратного таймера по переполнению. Вызывается
 * каждые 0.5 секунды.
 ******************************************************************************/
void hw_timer__RTC_IRQ_OVF_Handler(void) {
    if (!hw_timer.init)
        return;
    hw_timer.cur_time.timestamp++;
    hw_timer__sys_time_get(NULL);   // Обновит текуще время в переменной hw_timer.cur_time.

    if (hw_timer.time_alrm.ms < 0) {
        TIM1->DIER = 1;
        return;
    }
    if (hw_timer.time_alrm.timestamp < hw_timer.cur_time.timestamp)
        hw_timer__RTC_IRQHandler();
    else if (hw_timer.time_alrm.timestamp == hw_timer.cur_time.timestamp) {
        if (hw_timer.time_alrm.ms > hw_timer.cur_time.ms)
            hw_timer__start(hw_timer.time_alrm);
        else
            hw_timer__RTC_IRQHandler();
    }
}

