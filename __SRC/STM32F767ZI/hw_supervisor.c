﻿/***************************************************************************//**
 * @file hw_supervisor.c.
 * @brief Модуль управления частотой и режимами энергосбережения контроллера.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "mcu.h"
#include "System/fstandard.h"
#include "System/hw_supervisor.h"
#include "System/supervisor.h"

#define SYS_CLOCK_LOW_SLEEP_0_HZ    4200000     // Системная частота при тактировании от MSI (в Hz)
#define SYS_CLOCK_LOW_SLEEP_1_HZ    131000      // Системная частота при сне (в Hz)
#define SYS_CLOCK_HIGH_SLEEP_0_HZ   48000000    // Системная частота при тактировании от HSI (в Hz)

#define WDT__IWD_DELAY_MAX                      0xFFF
#define WDT__IWDG_KEY_RELOAD                    0xAAAA      /*!< IWDG Reload Counter Enable   */
#define WDT__IWDG_KEY_ENABLE                    0x0000CCCC  /*!< IWDG Peripheral Enable       */
#define WDT__IWDG_KEY_WRITE_ACCESS_ENABLE       0x00005555  /*!< IWDG KR Write Access Enable  */
#define WDT__IWDG_KEY_WRITE_ACCESS_DISABLE      0x00000000  /*!< IWDG KR Write Access Disable */

// Внутрення структура модуля.
typedef struct {
    supervisor__wdt_e cur_state;                                // Текущее состояние модуля.
    uint8_t count_lock;                                         // Количество программных модулей, подписавшихся на отключенный watchdog
    uint32_t cpu_clk;                                           // Текущая частота CPU.
    uint32_t periph_clk;                                        // Текущая частота периферии.
} supervisor__struct_t;

static supervisor__struct_t supervisor;

typedef enum {
    LVL_2_0V = 0,
    LVL_2_2V,
    LVL_2_4V,
    LVL_2_5V,
    LVL_2_6V,
    LVL_2_8V,
    LVL_2_9V,
} supervisor__pvd_e;

typedef enum {
    LVL_1_8V,
    LVL_1_5V,
    LVL_1_2V,
} supervisor__pvr_e;

typedef enum {
    LVL_1_7 = 0,
    LVL_2_0,
    LVL_2_2,
    LVL_2_5,
    LVL_2_8,
} supervisor__bor_e;

/* static void hw_supervisor__set_voltage_regulator_range(supervisor__pvr_e range); */
#ifdef WE_NEED_PVD
static void hw_supervisor__set_pvd_threshold(supervisor__pvd_e threshold);
#endif
static void hw_supervisor__set_bor_lvl(supervisor__bor_e borlvl);

/*******************************************************************************
 * Функция настройки параметров запуска супервизора (PDV, BOR и др.).
 ******************************************************************************/
void hw_supervisor__init(void) {
    hw_supervisor__set_bor_lvl(LVL_2_8);
#ifdef WE_NEED_PVD
    hw_supervisor__set_pvd_threshold(LVL_2_9V);
#endif
}

/*******************************************************************************
 * Функция перезагрузки приложения.
 ******************************************************************************/
void hw_supervisor__apl_reset(void) {
    SCB->AIRCR = 0x05FA0000 | SCB_AIRCR_SYSRESETREQ_Msk;
    while(1);
}

/*******************************************************************************
 * Функция, возвращающая причину перезагрузки приложения.
 ******************************************************************************/
static const supervisor__reset_reason_e hw_reset_reasons[] = {SUPERVISOR__RESET_UNKNOWN, SUPERVISOR__RESET_FIREWALL, SUPERVISOR__RESET_OPT_BYTE, SUPERVISOR__RESET_PIN,
                                                              SUPERVISOR__RESET_BOR, SUPERVISOR__RESET_SW, SUPERVISOR__RESET_WDT, SUPERVISOR__RESET_WDT, SUPERVISOR__RESET_IN_SLEEP};

supervisor__reset_reason_e hw_supervisor__reset_reason_get(void) {
    uint8_t res = 0, reason = RCC->CSR >> 24;
    RCC->CSR |= RCC_CSR_RMVF;
    for(;(res < 8) && (reason >> res); res++);
    return hw_reset_reasons[res];
}

/*******************************************************************************
 * Функция работы с WDT.
 ******************************************************************************/
supervisor__wdt_e hw_supervisor__wdt_cmd(supervisor__wdt_e cmd) {
    supervisor__wdt_e res = SUPERVISOR__WDT_ERROR;
    switch (cmd) {
        case SUPERVISOR__WDT_ON_1S:
        case SUPERVISOR__WDT_ON_2S:
        case SUPERVISOR__WDT_ON_3S:
        case SUPERVISOR__WDT_ON_4S:
            if (!supervisor.count_lock) {
                IWDG->KR = WDT__IWDG_KEY_ENABLE;                // Подрубаем IWDG
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // Разрешаем запись в PR и RLR регистры
                for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
                IWDG->PR = 3;
                IWDG->RLR = cmd * 1000;
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;
                supervisor.cur_state = cmd;
            }
            else
                supervisor.count_lock--;
        case SUPERVISOR__WDT_RESET:
            IWDG->KR = WDT__IWDG_KEY_RELOAD;  // Перезагружаем счетчик
        case SUPERVISOR__WDT_GET_STATE:
            res = supervisor.cur_state;
            break;
        case SUPERVISOR__WDT_OFF:
            if (supervisor.count_lock < 0xFF)
                supervisor.count_lock++;
            IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // Разрешаем запись в PR и RLR регистры
            for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
            IWDG->PR = 7;
            IWDG->RLR = 0xFFF;
            IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;
            supervisor.cur_state = cmd;
        default:
            // нет такой команды.
            break;
    }
    return res;
}

/*******************************************************************************
 * Функция перехода контроллера в SLEEP.
 * Была выявлена проблема при отладке во время выхода из пониженного потребления по прерыванию(свяливался в hard fault).
 * Отключение и включение irq + добавление NOP-ов помогает.
 ******************************************************************************/
void hw_supervisor__idle_on(void) {

    MODIFY_REG(RCC->APB1ENR, 0, RCC_APB1ENR_PWREN);     //включение тактирования PWR системы, без нее не уйдем в стоп!
    supervisor.cpu_clk = 0;
    MODIFY_REG(SCB->SCR, 0, SCB_SCR_SLEEPDEEP_Msk ); // low-power mode = stop mode
    __disable_irq();
    __WFI();
    __enable_irq();
}

/*******************************************************************************
  Функция выхода контроллера из SLEEP.
 ******************************************************************************/
void hw_supervisor__idle_off(void) {
    hw_supervisor__clk_set(SUPERVISOR__CLK_HIGH, 0);
}

/*******************************************************************************
 * Функция получения текущей частоты тактирования процессора.
 ******************************************************************************/
uint32_t hw_supervisor__cpu_clk_get(void) {
    return supervisor.cpu_clk;
}

/*******************************************************************************
 * Функция получения текущей частоты тактирования периферии.
 ******************************************************************************/
uint32_t hw_supervisor__periph_clk_get(void) {
    return supervisor.periph_clk;
}



/*******************************************************************************
 * Функция установки частоты тактирования МК.
 ******************************************************************************/
supervisor__event_e hw_supervisor__clk_set(supervisor__clk_e clk, int sleep) {
    switch (clk) {
        case SUPERVISOR__CLK_LOW: {
            // Тактирование от MSI, SYSCLK = HCLK = 4.2 MHz, APB1,APB2 = 1. Вся периферия 4.2 MHz
            break;
        }
        case SUPERVISOR__CLK_HIGH: {
            // Тактирование от HSI, SYSCLK = HCLK = 48 MHz.
            /* Enable Prefetch Buffer */
//            FLASH->ACR |= FLASH_ACR_PRFTEN;
            /* Flash 1 wait state */
            MODIFY_REG(FLASH->ACR, FLASH_ACR_LATENCY, 2);
            while ((FLASH->ACR &  FLASH_ACR_LATENCY) == 0) {;}

            MODIFY_REG(RCC->APB1ENR, 0, RCC_APB1ENR_PWREN);     //включение тактирования PWR системы, без нее не уйдем в стоп!
            PWR->CR1 = PWR_CR1_VOS_0 | PWR_CR1_LPDS | PWR_CR1_DBP;  // Stop 2 mode cannot be entered when LPR bit is set. Stop 1 is entered instead.
            //Запуск HSI
            MODIFY_REG(RCC->CR, 0, RCC_CR_HSION);
            //Ждем стабилизации HSI
            while ((RCC->CR & RCC_CR_HSIRDY) == 0) {;}
             /*Изменение параметров делителей всего, что зависит от системной частоты
              необходимо производить при отключенном PLL cтр. 144 в REF.MANUAL */
             //Отключаем PLL
            MODIFY_REG(RCC->CR, RCC_CR_PLLON, 0);
            // Делитель системной частоты (HCLK = SYSCLK)
            //Делитель шины APB1 (PCLK2 = HCLK)
            //Делитель шины APB2 (PCLK1 = HCLK)
            //настройки  - выход из СТОП с HSI генератором.
            MODIFY_REG(RCC->CFGR, 0, RCC_CFGR_HPRE_DIV1 | RCC_CFGR_PPRE1_DIV1 | RCC_CFGR_PPRE2_DIV1);
            //Задаем нстройки делителя и умножителя
            RCC->PLLCFGR = RCC_PLLCFGR_PLLSRC_HSI | RCC_PLLCFGR_PLLR_1 | (0x60 << RCC_PLLCFGR_PLLN_Pos) | ( 0x10 << RCC_PLLCFGR_PLLM_Pos) | RCC_PLLCFGR_PLLQ_1;  //todo USB тут включать?
            //Включаем PLL
            MODIFY_REG(RCC->CR,0 , RCC_CR_PLLON);
            //Ждем стабилизации PLL
            while((RCC->CR & RCC_CR_PLLRDY) == 0) {;}
           //Выбираем PLL для тактирования SysClk
            MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, RCC_CFGR_SW_PLL);
            //Ждем стабилизации PLL
            while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL) {;}
            supervisor.cpu_clk = SYS_CLOCK_HIGH_SLEEP_0_HZ;
            supervisor.periph_clk = SYS_CLOCK_HIGH_SLEEP_0_HZ;

            break;
        }
        default:
            return SUPERVISOR__EVENT_ERROR;
    }
    return SUPERVISOR__EVENT_OK;
}

/*******************************************************************************
 * Функция установки PVR на нужный уровень.
 ******************************************************************************/
/*
static void hw_supervisor__set_voltage_regulator_range(supervisor__pvr_e range) {
    RCC->CR &=~  RCC_CR_HSEON;
    //Включаем тактирование PWR
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    //Ждем установки VOSF в регистре PWR_CSR в ноль
    while(READ_BIT(PWR->CSR, PWR_CSR_VOSF) == 1) {;}
    switch (range) {
        case LVL_1_8V: // 1.8V MAX  RANGE 1
            PWR->CR |= PWR_CR_VOS_0;
            PWR->CR &=~ PWR_CR_VOS_1;
            break;
        case LVL_1_5V:  // 1.5V medium RANGE 2
            PWR->CR |= PWR_CR_VOS_1;
            PWR->CR &=~ PWR_CR_VOS_0;
            break;
        case LVL_1_2V:   // 1.2V LOW RANGE 3
            PWR->CR |= PWR_CR_VOS_0;
            PWR->CR |= PWR_CR_VOS_1;
            break;
        default:   //Оставить без изменений, сохранив прерыдущий RANGE
            PWR->CR &=~ PWR_CR_VOS_0;
            PWR->CR &=~ PWR_CR_VOS_1;
            break;
    }
    //Ждем установки VOSF в регистре PWR_CSR в ноль
    while(READ_BIT(PWR->CSR, PWR_CSR_VOSF) == 1) {;}
    //Подробнее смотри на 104 стр. в RM
}
*/


#ifdef WE_NEED_PVD
/*******************************************************************************
 * Функция установки PVD на нужный уровень.
 ******************************************************************************/
static void hw_supervisor__set_pvd_threshold(supervisor__pvd_e threshold) {
    //Включаем тактирование PWR
    MODIFY_REG(RCC->APB1ENR1, 0, RCC_APB1ENR1_PWREN);
//    //Включаем PVD
    MODIFY_REG(PWR->CR2, 0, PWR_CR2_PVDE);
//    //Выставляем пороговый уровень напряжения
     MODIFY_REG(PWR->CR2, PWR_CR2_PLS_Msk, threshold << PWR_CR2_PLS_Pos);
}
#endif
/*******************************************************************************
 * Функция установки BOR на нужный уровень.
 ******************************************************************************/
static void hw_supervisor__set_bor_lvl(supervisor__bor_e borlvl) {
    WRITE_REG(FLASH->KEYR, 0x45670123);
    WRITE_REG(FLASH->KEYR, 0xCDEF89AB);
    WRITE_REG(FLASH->OPTKEYR, 0x08192A3B);
    WRITE_REG(FLASH->OPTKEYR, 0x4C5D6E7F);
    for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 10000000); x++);
    MODIFY_REG(FLASH->OPTCR, FLASH_OPTCR_BOR_LEV_Msk | FLASH_OPTCR_IWDG_STOP | FLASH_OPTCR_nDBANK, borlvl << FLASH_OPTCR_BOR_LEV_Pos);
    FLASH->CR = (uint32_t)(0x01 << 31);
}

