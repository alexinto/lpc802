﻿/***************************************************************************//**
 * @file hw_uart_usb.c.
 * @brief  Драйвер USB CDC для STM32L476VG.
 * @author a.tushentsov.
 ******************************************************************************/
#include "string.h"
#include "target.h"
#include "DRV/hw_uart_usb.h"
#include "usbd_desc.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "System/supervisor.h"
#include "System/sw_timer.h"
#include "DRV/gpio.h"

USBD_HandleTypeDef hUsbDeviceFS;
extern USBD_DescriptorsTypeDef FS_Desc;
#define HW_UART_USB__FIFO_SIZE 64

#define USB_CRITICAL_SECTION_ON                             \
{                                                           \
    NVIC_DisableIRQ(OTG_FS_IRQn);

#define USB_CRITICAL_SECTION_OFF                            \
    NVIC_EnableIRQ(OTG_FS_IRQn);                            \
}
typedef enum {
    USB_IDLE    = 0,
    USB_INIT    = 1,
    USB_WORK    = 2,
    USB_DEINIT  = 3,
}usb_fsm_state__e;

// Структура интерфейса UART.
typedef struct {
    uart__hw_event_handler_t hw_event_handler;          // Указатель на функцию-обработчик ошибок/событий UART.
    hw_uart__cb_t tx_isr_cb;                            // Указатель на пользовательский коллбэк по передаче (из прерывания).
    uint8_t *tx_buff;                                   // Указатель пользовательский буфер для передачи данных.
    int tx_len;                                         // Размер пользовательского буфера для приема данных (по заполнению вызовется коллбэк).
    hw_uart__cb_t rx_isr_cb;                         // Указатель на пользовательский коллбэк по приему заданного количества байт (из прерывания).
    int rx_len;                                         // Размер пользовательского буфера для приема данных (по заполнению вызовется коллбэк). Оптимально 64 байта.
                                                        // Либо должен быть кратен 64 байтам.
    int tx_bytes_count, rx_bytes_count, save_bytes, offset_bytes;     // Счетчики переданных\принятых байтов.
    events__e tx_event, rx_event;
    u8 rx_buff[HW_UART_USB__FIFO_SIZE];                     // Буфер для приема данных.
    sw_timer__t timer;
} hw_uart_usb__itf_t;

typedef struct {
    hw_uart_usb__itf_t uart[USB_UART_COUNT];
    u8 init, isr_flag;
    usb_fsm_state__e fsm_state;
    supervisor__idle_sub_t  idle_sub;
    uart__hw_event_handler_t hw_event_handler;
    sw_timer__t connect_tmr;
}hw_uart_usb_itf_t;


static hw_uart_usb_itf_t hw_usb; // .state = USB_IDLE;

static events__e hw_uart_usb__hw_init();
static void hw_uart_usb__tx_timer_cb(struct sw_timer__t *timer, void *ext_data);
static void connect_timer_cb(struct sw_timer__t *timer, void *ext_data);
static void hw_uart_usb__cmd_exec(int uart_id, uart__cmd_e cmd);

events__e hw_uart_usb__init(int uart_id, uint32_t settings, uart__hw_event_handler_t hw_event_handler) {
    hw_usb.init |= 1 << (uart_id - USB_UART_1);
    hw_uart_usb__itf_t* uart = &hw_usb.uart[uart_id - USB_UART_1];
    USB_CRITICAL_SECTION_ON
    uart->rx_bytes_count = 0;
    uart->tx_event = uart->rx_event = EVENT__CONTINUE;
    USB_CRITICAL_SECTION_OFF
    if (hw_usb.fsm_state == USB_WORK)
        hw_event_handler(uart_id, EVENT__OPEN, NULL);
    else {
        hw_usb.fsm_state = USB_IDLE;
        hw_usb.isr_flag = 0;
        sw_timer__start(&hw_usb.connect_tmr, 100, connect_timer_cb, NULL);
    }
    hw_usb.hw_event_handler = hw_event_handler;
    return EVENT__OK;
}

static events__e hw_uart_usb__hw_init() {
    u32 temp;
    NVIC_DisableIRQ(OTG_FS_IRQn);
    hw_uart_usb__cmd_exec(USB_UART_1, UART__CMD_PWR_ON_232);

    supervisor__idle_lock(&hw_usb.idle_sub);


    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);

    temp = GPIOA->OSPEEDR;
    temp &= ~((GPIO_OSPEEDR_OSPEEDR0 << (11 * 2u)) | (GPIO_OSPEEDR_OSPEEDR0 << (12 * 2u)));
    temp |= (3 << (11 * 2u)) | (3 << (12 * 2u));
    GPIOA->OSPEEDR = temp;
    temp = GPIOA->OTYPER;
    temp &= ~((GPIO_OTYPER_OT0 << 11) | (GPIO_OTYPER_OT0 << 12));
    temp |= (((1 & 0x02) >> 4u) << 11) | (((1 & 0x02) >> 4u) << 12);
    GPIOA->OTYPER = temp;
    temp = GPIOA->PUPDR;
    temp &= ~((GPIO_PUPDR_PUPDR0 << (11 * 2u)) | (GPIO_PUPDR_PUPDR0 << (12 * 2u)));
    GPIOA->PUPDR = temp;

    temp = GPIOA->AFR[11 >> 3u];
    temp &= ~(0xFu << ((11 & 0x07u) * 4u));
    temp |= (0x0A << ((11 & 0x07u) * 4u));
    GPIOA->AFR[11 >> 3u] = temp;
    temp = GPIOA->AFR[12 >> 3u];
    temp &= ~(0xFu << ((12 & 0x07u) * 4u));
    temp |= (0x0A << ((12 & 0x07u) * 4u));
    GPIOA->AFR[12 >> 3u] = temp;
    temp = GPIOA->MODER;
    temp &= ~((GPIO_MODER_MODER0 << (11 * 2u)) | (GPIO_MODER_MODER0 << (12 * 2u)));
    temp |= (0x02 << (11 * 2u)) | (0x02 << (12 * 2u));
    GPIOA->MODER = temp;
    MODIFY_REG(RCC->DCKCFGR2, RCC_DCKCFGR2_CK48MSEL_Msk, 0);
    SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_OTGFSEN);
//    SET_BIT(PWR->CR2, PWR_CR2_USV);

    USB_OTG_FS->GUSBCFG |= USB_OTG_GUSBCFG_PHYSEL;
    // Reset USB
    USB_OTG_FS->GRSTCTL |= USB_OTG_GRSTCTL_CSRST;
    temp = 0;
    while (((USB_OTG_FS->GRSTCTL & USB_OTG_GRSTCTL_CSRST) == USB_OTG_GRSTCTL_CSRST) && (temp < 200000))
        temp++;
    /* Init Device Library, add supported class and start the library. */
    hUsbDeviceFS.pDesc = &FS_Desc;
    hUsbDeviceFS.dev_state = USBD_STATE_DEFAULT;
    hUsbDeviceFS.id = DEVICE_FS;
    USBD_LL_Init(&hUsbDeviceFS);
    hUsbDeviceFS.pClass = &USBD_CDC;
    hUsbDeviceFS.pUserData = &USBD_Interface_fops_FS;
    if (USBD_LL_Start(&hUsbDeviceFS) != USBD_OK)
        return EVENT__ERROR;
    USB_OTG_FS->GAHBCFG |= USB_OTG_GAHBCFG_GINT;
    NVIC_SetPriority(OTG_FS_IRQn, 3);
    NVIC_EnableIRQ(OTG_FS_IRQn);
    return EVENT__OK;
}

events__e hw_uart_usb__deinit(int uart_id) {
    if (uart_id != -1) {
        hw_usb.init &= ~(1 << (uart_id - USB_UART_1));
        hw_usb.hw_event_handler(uart_id, EVENT__CLOSE, NULL);
        return EVENT__OK;
    }
    NVIC_DisableIRQ(OTG_FS_IRQn);
    hw_uart_usb__cmd_exec(USB_UART_1, UART__CMD_PWR_OFF);
    /* Set Default State */
    hUsbDeviceFS.dev_state = USBD_STATE_DEFAULT;
    /* Free Class Resources */
    hUsbDeviceFS.pClass->DeInit(&hUsbDeviceFS, (uint8_t)hUsbDeviceFS.dev_config);
    USB_OTG_FS->GRSTCTL |= USB_OTG_GRSTCTL_CSRST;
    /* Stop the low level driver  */
    USBD_LL_Stop(&hUsbDeviceFS);
    /* Initialize low level driver */
    USBD_LL_DeInit(&hUsbDeviceFS);
    for (int i = 0; i < USB_UART_COUNT; i++)
        hw_usb.uart[i].rx_isr_cb = 0;
    supervisor__idle_unlock(&hw_usb.idle_sub);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция отправки пользовательских данных в указанный UART.
 ******************************************************************************/
events__e hw_uart_usb__tx(int uart_id, uint8_t *buff, int len, hw_uart__cb_t tx_isr_cb) {
    uint8_t result = USBD_BUSY, port = uart_id - USB_UART_1;
    hw_uart_usb__itf_t* uart = &hw_usb.uart[port];
    result = CDC_Transmit_FS((uint8_t*)buff, len, port);
    switch(result) {
        case USBD_OK:
            uart->tx_buff = buff;
            uart->tx_len = len;
            uart->tx_isr_cb = tx_isr_cb;
            uart->tx_bytes_count = 0;
            uart->tx_event = EVENT__LOCK;
            sw_timer__start(&uart->timer, 100, hw_uart_usb__tx_timer_cb, (void*)uart_id);
            return EVENT__OK;
        case USBD_BUSY:
//            uart->tx_event = EVENT__OK;
            return EVENT__BUSY;
        default:
            break;
    }
    return EVENT__ERROR;
}

static void hw_uart_usb__tx_timer_cb(struct sw_timer__t *timer, void *ext_data) {
    uint8_t port = (int)ext_data - USB_UART_1;
    hw_uart_usb__itf_t* uart = &hw_usb.uart[port];
    USB_CRITICAL_SECTION_ON
    if (uart->tx_event == EVENT__LOCK)
        uart->tx_event = EVENT__OK;
    USB_CRITICAL_SECTION_OFF
}

/*******************************************************************************
 * Функция чтения данных из указанного порта.
 ******************************************************************************/
events__e hw_uart_usb__rx(int uart_id, hw_uart__cb_t rx_isr_cb) {
    int port = uart_id - USB_UART_1;
    hw_usb.uart[port].rx_isr_cb = rx_isr_cb;
    return EVENT__OK;
}

void hw_uart_usb_rx_cb(int port, uint8_t* buff, uint32_t len) {
    hw_uart_usb__itf_t* uart = &hw_usb.uart[port];
    USB_CRITICAL_SECTION_ON
    if (uart->rx_isr_cb) {
        memcpy(uart->rx_buff, buff, len);
        uart->rx_bytes_count = len;
        uart->rx_event = EVENT__OK;
    }
    else
        cdc_rx(port);
    USB_CRITICAL_SECTION_OFF
}

void hw_uart_usb_tx_cb(uint8_t port) {
    hw_uart_usb__itf_t* uart = &hw_usb.uart[port];
    USB_CRITICAL_SECTION_ON
    if (uart->tx_event == EVENT__LOCK)
        uart->tx_event = EVENT__OK;
    USB_CRITICAL_SECTION_OFF
}

void hw_uart_usb__rx_flush(int uart_id) {
    int port = uart_id - USB_UART_1;
    hw_usb.uart[port].rx_isr_cb = NULL;
    hw_usb.uart[port].rx_bytes_count = hw_usb.uart[port].save_bytes = 0;
}

void hw_uart_usb__event_handler(int uart_id, events__e event) {

}

void hw_uart_usb__cout(void) {
    switch(hw_usb.fsm_state) {
        case USB_IDLE:
            if (hw_usb.isr_flag) {
                hw_usb.fsm_state = USB_INIT;
                hw_uart_usb__hw_init();
                hw_usb.isr_flag = 0;
                sw_timer__start(&hw_usb.connect_tmr, 1000, connect_timer_cb, NULL);
            }
            break;
        case USB_INIT:
            if (!hw_usb.isr_flag)
                break;
            if (hUsbDeviceFS.dev_state == USBD_STATE_CONFIGURED) {
                hw_usb.fsm_state = USB_WORK;
                for(int i = 0; i < USB_UART_COUNT; i++)
                    if (hw_usb.init & (1 << i))
                        hw_usb.hw_event_handler(USB_UART_1 + i, EVENT__OPEN, NULL);
            }
            else {
                hw_uart_usb__deinit(-1);
                hw_usb.isr_flag = 0;
                sw_timer__start(&hw_usb.connect_tmr, 1000, connect_timer_cb, NULL);
                hw_usb.fsm_state = USB_IDLE;
            }
            break;
        case USB_WORK:
        if (hUsbDeviceFS.dev_state != USBD_STATE_CONFIGURED) { //todo ?
                hw_uart_usb__deinit(-1);
                hw_usb.isr_flag = 0;
                sw_timer__start(&hw_usb.connect_tmr, 1000, connect_timer_cb, NULL);
                hw_usb.fsm_state = USB_DEINIT;
                return;
            }
            for(int i = 0; i < USB_UART_COUNT; i++) {
                hw_uart_usb__itf_t* uart = &hw_usb.uart[i];
                events__e event = uart->tx_event;
                int bytes_cnt;
                int uart_id = i + USB_UART_1;
                u8* cur_buff = uart->rx_buff;
                if (!(hw_usb.init & (1 << i)))
                    continue;
                if ((event != EVENT__CONTINUE) && (event != EVENT__LOCK) && (uart->tx_isr_cb)) {
                    sw_timer__stop(&uart->timer);
                    uart->tx_event = EVENT__CONTINUE;
                    uart->tx_bytes_count = uart->tx_len;           // to_do ax_srtream падает !
                    uart->tx_isr_cb(uart_id, event, uart->tx_buff, uart->tx_bytes_count);
                }
                if (((event = uart->rx_event) != EVENT__CONTINUE) && (uart->rx_isr_cb)) {
                    uart->rx_event = EVENT__CONTINUE;
                    if (uart->save_bytes)
                        bytes_cnt = uart->save_bytes;
                    else {
                        uart->offset_bytes = 0;
                        bytes_cnt = uart->rx_bytes_count;
                        uart->rx_bytes_count = 0;
                    }
                    cur_buff += uart->offset_bytes;
                    uart->save_bytes = uart->rx_isr_cb(uart_id, event, cur_buff, bytes_cnt);
                    uart->offset_bytes += bytes_cnt - uart->save_bytes;
                    if (uart->save_bytes)
                        uart->rx_event = EVENT__OK;
                    else
                        cdc_rx(i);
                }
            }
            break;
        case USB_DEINIT:
            if (!hw_usb.isr_flag)
                break;
            hw_usb.isr_flag = 0;
            hw_usb.fsm_state = USB_IDLE;
            for(int i = 0; i < USB_UART_COUNT; i++)
                if (hw_usb.init & (1 << i))
                    hw_uart_usb__deinit(i + USB_UART_1);
            break;
    }
}

extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
void hw_uart_usb__IRQ(void) {
    HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
}

/*******************************************************************************
 * Функция выполнения hw-команд интерфейса
 ******************************************************************************/
static void hw_uart_usb__cmd_exec(int uart_id, uart__cmd_e cmd) {
    uart__cmd_handler(uart_id, cmd);
}


static void connect_timer_cb(struct sw_timer__t *timer, void *ext_data) {
    hw_usb.isr_flag = 1;
}