﻿/***************************************************************************//**
 * @file hw_uart.h
 * @brief Низкоуровневый драйвер аппаратного UART.
 * @author a.tushentsov
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "System/supervisor.h"
#include "System/fstandard.h"
#include "mcu.h"
#include "DRV/hw_uart.h"
#include "DRV/uart.h"
#include "System/sw_timer.h"
#include "System/list.h"

#ifndef HW_UART__FIFO_SIZE        // не более 512!
    #define HW_UART__FIFO_SIZE 64
#endif

#define UART_CRITICAL_SECTION_ON(uart_id)                                        \
{                                                                                   \
    NVIC_DisableIRQ((IRQn_Type)hw_uart_descr[uart_id & UART__ID_MSK].uart_irq);

#define UART_CRITICAL_SECTION_OFF(uart_id)                                       \
    NVIC_EnableIRQ((IRQn_Type)hw_uart_descr[uart_id & UART__ID_MSK].uart_irq);   \
}

// Битовое поле с флагами причины прерывания.
 typedef struct {
    u32 PE      :1;
    u32 FE      :1;
    u32 NE      :1;
    u32 ORE     :1;
    u32 IDLE    :1;
    u32 RXNE    :1;
    u32 TC      :1;
    u32 TXE     :1;
    u32 LBDF    :1;
    u32 CTSIF   :1;
    u32 CTS     :1;
    u32 RTOF    :1;
    u32 EOBF    :1;
    u32         :1;
    u32 ABRE    :1;
    u32 ABRF    :1;
    u32 BUSY    :1;
    u32 CMF     :1;
    u32 SBKF    :1;
    u32 RWU     :1;
    u32 WUF     :1;
    u32 TEACK   :1;
    u32 REACK   :1;
 } hw_uart__isr_t;

 // Объединение для регистра причины прерывания.
 typedef union {
     u32 isr_u32;
     hw_uart__isr_t isr;
 } hw_uart__isr_u;

// Структура интерфейса UART.
typedef struct {
    u32 settings;
    events__e tx_event, rx_event;
    uart__hw_event_handler_t hw_event_handler;                    // Указатель на функцию-обработчик ошибок/событий UART.
    u8 *tx_buff;                                        // Указатель на буфер с пользовательскими данными для передачи.
    hw_uart__cb_t tx_isr_cb;                            // Указатель на пользовательский коллбэк по передаче (из прерывания).
    int tx_len;                                         // Размер буфера с пользовательскими данными для передачи.
    int tx_bytes_count, rx_bytes_count, save_bytes, offset_bytes;     // Счетчики переданных\принятых байтов.
    u8 rx_buff[HW_UART__FIFO_SIZE];                     // Буфер для приема данных.
    u8 rx_offset, init;
    u8 parity, wordlenth9;
    hw_uart__cb_t rx_isr_cb;                         // Указатель на пользовательский коллбэк по приему заданного количества байт (из прерывания).
    supervisor__idle_sub_t  idle_sub;
    sw_timer__t timer;
} hw_uart__itf_t;

//
typedef struct {
    int gpio[2];
    u32* uart_clock;
    int uart_clock_flag;
    int uart_irq;
    USART_TypeDef * uart_cmsis;
    u8 af_nb;
} hw_uart_descr__t;

static const hw_uart_descr__t hw_uart_descr[] = {
#ifdef HW_UART_1_TX
    { { HW_UART_1_TX, HW_UART_1_RX }, (u32*)&RCC->APB2ENR, RCC_APB2ENR_USART1EN, USART1_IRQn, USART1, 0x04 },
#endif
#ifdef HW_UART_2_TX
    { { HW_UART_2_TX, HW_UART_2_RX }, (u32*)&RCC->APB1ENR, RCC_APB1ENR_USART2EN, USART2_IRQn, USART2, 0x07 },
#endif
#ifdef HW_UART_3_TX
    { { HW_UART_3_TX, HW_UART_3_RX }, (u32*)&RCC->APB1ENR, RCC_APB1ENR_USART3EN, USART3_IRQn, USART3, 0x07 },
#endif
#ifdef HW_UART_4_TX
    { { HW_UART_4_TX, HW_UART_4_RX }, (u32*)&RCC->APB1ENR, RCC_APB1ENR_UART4EN, UART4_IRQn, UART4, 0x08 },
#endif
#ifdef HW_UART_5_TX
    { { HW_UART_5_TX, HW_UART_5_RX }, (u32*)&RCC->APB1ENR, RCC_APB1ENR_UART5EN, UART5_IRQn, UART5, 0x08 },
#endif
#ifdef HW_UART_6_TX
    { { HW_UART_6_TX, HW_UART_6_RX }, (u32*)&RCC->APB1ENR2, RCC_APB1ENR2_LPUART1EN, LPUART1_IRQn, LPUART1, 0x08 },
#endif
};

// Таблицы настроек скорости при тактировании UART частотой разных частот + значения тайм-аута на отправку последнего байта.
static const u32 hw_uart__16mhz_br_table[11] = {
    0x3412,    //2400
    0x1A05,    //4800
    0x0D02,    //9600
    0x0681,    //19200
    0x0340,    //38400
    0x0225,    //57600
    0x0112,    //115200
    0x0085,    //230400
    0x0042,    //460800
    0x0021,    //921600
};

static hw_uart__itf_t hw_uart[HW_UART_COUNT];

void hw_uart__USART_IRQHandler(int uart_id);
static void hw_uart__itf_en(int uart_id);
static void hw_uart__itf_dis(int uart_id);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);

__weak int uart__cmd_handler(int uart_id, uart__cmd_e cmd) {return 0;}

/*******************************************************************************
 * Функция инициализации указанного UART-а необходимыми настройками.
 ******************************************************************************/
events__e hw_uart__init(int uart_id, u32 settings, uart__hw_event_handler_t hw_event_handler) {
    USART_TypeDef* UART = hw_uart_descr[uart_id].uart_cmsis;
    hw_uart__itf_t* uart = &hw_uart[uart_id];
    events__e res = EVENT__OK;
    uart->settings = settings;
    uart->hw_event_handler = hw_event_handler;
    if ((uart->settings & UART__FC_MASK) == UART__FC_485)
        uart__cmd_handler(uart_id, UART__CMD_FC_DIR_RX);
    supervisor__idle_lock(&uart->idle_sub);
    UART_CRITICAL_SECTION_ON(uart_id)
// Начало инициализации
        hw_uart__itf_en(uart_id);
        MODIFY_REG(RCC->DCKCFGR2, 0xFFFF, 0xAAAA);            // UARTs clk = 16Mhz
        //Включаем UARTx (x=1/2/3/4/5)
        WRITE_REG(UART->CR1, USART_CR1_UESM | USART_CR1_OVER8 | USART_CR1_TE);    // Большая точность получается на шине.
        WRITE_REG(UART->ICR, 0x1B5F | USART_ICR_CMCF | USART_ICR_WUCF);
        if (settings & UART__PIN_INV) {                        // Инвертируем RX
            SET_BIT(UART->CR2, USART_CR2_TXINV | USART_CR2_RXINV);
        }
//Настройка Стоп бита
        switch (settings & UART__STOP_MASK) {
            case UART__STOP_0_5:
                MODIFY_REG(UART->CR2, USART_CR2_STOP, USART_CR2_STOP_0);
                break;
            case UART__STOP_1:
                CLEAR_BIT(UART->CR2, USART_CR2_STOP);
                break;
            case UART__STOP_1_5:
                SET_BIT(UART->CR2, USART_CR2_STOP);
                break;
            case UART__STOP_2:
                MODIFY_REG(UART->CR2, USART_CR2_STOP, USART_CR2_STOP_1);
                break;
            default:
                res = EVENT__PARAM_NA;
        }
        //Настройка объема передаваемых данных
        switch (settings & UART__DATA_MASK) {
            case UART__DATA_7:
                CLEAR_BIT(UART->CR1, USART_CR1_M);
                uart->wordlenth9 = 0;
                break;
            case UART__DATA_8:
                CLEAR_BIT(UART->CR1, USART_CR1_M);
                if (READ_BIT(settings, UART__PAR_MASK) != UART__PAR_NONE) {
                    uart->wordlenth9 = 1;
                    SET_BIT(UART->CR1, USART_CR1_M);
                }
                else
                    uart->wordlenth9 = 0;
                break;
            case UART__DATA_9:
                SET_BIT(UART->CR1, USART_CR1_M);
                uart->wordlenth9 = 1;
                break;
            default:
                res = EVENT__PARAM_NA;
        }
// Настройка четности
        switch (READ_BIT(settings, UART__PAR_MASK)) {
            case UART__PAR_ODD:
                uart->parity = 1;
                SET_BIT(UART->CR1, USART_CR1_PCE | USART_CR1_PS);
                break;
            case UART__PAR_EVEN:
                uart->parity = 1;
                MODIFY_REG(UART->CR1, USART_CR1_PS, USART_CR1_PCE);
                break;
            default:
                uart->parity = 0;
                CLEAR_BIT(UART->CR1, USART_CR1_PCE);
                break;
        }

        //Проверка на допустимые форматы!
        int M = READ_BIT(UART->CR1, USART_CR1_M);     // data length
        int PCE = READ_BIT(UART->CR1, USART_CR1_PCE);    //ParityControl
        int UserDataLenth = READ_BIT(settings, UART__DATA_MASK);

        if (M == 0) {
            if (PCE == 0) {
                if (UserDataLenth != UART__DATA_8)
                    res = EVENT__PARAM_NA;
            }
            else if (PCE != 0) {
                if ((UserDataLenth != UART__DATA_7) & (!uart->wordlenth9))
                    res = EVENT__PARAM_NA;
            }
        }
        else if (M != 0) {
            if (PCE == 0) {
                if (UserDataLenth != UART__DATA_9)
                    res = EVENT__PARAM_NA;
            }
            else if (PCE != 0) {
                if (UserDataLenth != UART__DATA_8)
                    res = EVENT__PARAM_NA;
            }
        }
        else
            res = EVENT__PARAM_NA;
        SET_BIT(UART->CR1, USART_CR1_UE);    // Большая точность получается на шине.
        //Настройки частоты
        UART->BRR = hw_uart__16mhz_br_table[(settings & UART__BR_MASK) - 4];
        uart->tx_event = uart->rx_event = EVENT__CONTINUE;
        uart->rx_bytes_count = uart->save_bytes = 0;
        uart->rx_isr_cb = NULL;
        uart->init = 1;
        NVIC_SetPriority((IRQn_Type)hw_uart_descr[uart_id & UART__ID_MSK].uart_irq, 2);
    UART_CRITICAL_SECTION_OFF(uart_id)
    if (res == EVENT__OK) {
        int timeout = uart__cmd_handler(uart_id, (uart->settings & UART__FC_MASK) == UART__FC_485 ? UART__CMD_PWR_ON_485 : UART__CMD_PWR_ON_232);
        sw_timer__start(&uart->timer, timeout, timer_cb, (void*)uart_id);
    }
    else
        hw_uart__deinit(uart_id);

    return EVENT__OK;
}

/*******************************************************************************
 * Функция запуска приема данных из указанного порта во встроенный кольцевой буфер.
 ******************************************************************************/
events__e hw_uart__rx(int uart_id, hw_uart__cb_t rx_isr_cb) {
    USART_TypeDef* UART = hw_uart_descr[uart_id].uart_cmsis;
    hw_uart__itf_t* uart = &hw_uart[uart_id];
    UART_CRITICAL_SECTION_ON(uart_id)
        uart->rx_isr_cb = rx_isr_cb;
        //Подключаем прерывания о готовности принятых данных (RXNE)
        SET_BIT(UART->CR1, USART_CR1_RXNEIE | USART_CR1_PEIE);
        if (((uart->settings & UART__FC_MASK) != UART__FC_485) || ((UART->CR1 & USART_CR1_TCIE) != USART_CR1_TCIE)) {
            //Настройка прерывай на ошибки
            SET_BIT(UART->CR3, USART_CR3_EIE);    //Включаем прерывания на FRAGMING/OVERFLOW/NOISE
            // Включаем recieve
            SET_BIT(UART->CR1, USART_CR1_RE);
        }
    UART_CRITICAL_SECTION_OFF(uart_id)
    return EVENT__OK;
}

/*******************************************************************************
 * Функция отправки пользовательских данных в указанный UART.
 ******************************************************************************/
events__e hw_uart__tx(int uart_id, u8* buff, int len, hw_uart__cb_t tx_isr_cb) {
    USART_TypeDef* UART = hw_uart_descr[uart_id].uart_cmsis;
    hw_uart__itf_t* uart = &hw_uart[uart_id];
    UART_CRITICAL_SECTION_ON(uart_id)
        SET_BIT(UART->CR1, USART_CR1_TCIE);    //Разрешаем прерывания по TC
        if ((uart->settings & UART__FC_MASK) == UART__FC_485) {
            uart__cmd_handler(uart_id, UART__CMD_FC_DIR_TX);
            UART->CR3 &= ~ USART_CR3_EIE;    //Выключаем прерывания на FRAGMING/OVERFLOW/NOISE
            CLEAR_BIT(UART->CR1, USART_CR1_RE);
            uart->rx_bytes_count = uart->save_bytes = 0;
        }
        uart->tx_buff = buff;
        uart->tx_len = len;
        uart->tx_isr_cb = tx_isr_cb;
        uart->tx_bytes_count = 0;
        // Передача
        SET_BIT(UART->ICR, USART_ICR_TCCF);
        int temp = uart->tx_buff[uart->tx_bytes_count];

        if (uart->wordlenth9 == 1)
            temp = (uart->tx_buff[uart->tx_bytes_count] & 0x01FF);
        else
            temp = uart->tx_buff[uart->tx_bytes_count];

        WRITE_REG(UART->TDR, temp);
    UART_CRITICAL_SECTION_OFF(uart_id)
    return EVENT__OK;
}

/*******************************************************************************
 * Функция деинициализации указанного UART-а. Прекращает выполнение UART-м всех операций и отключает его.
 ******************************************************************************/
events__e hw_uart__deinit(int uart_id) {
    hw_uart_descr__t* UART = (hw_uart_descr__t*)&hw_uart_descr[uart_id];
    NVIC_DisableIRQ((IRQn_Type)UART->uart_irq);
    hw_uart__itf_t* uart = &hw_uart[uart_id];
    //Отключаем выбранный USART
    uart__cmd_handler(uart_id, UART__CMD_PWR_OFF);
    UART->uart_cmsis->CR1 = 0;
    hw_uart__itf_dis(uart_id);
    uart->init = 0;
    supervisor__idle_unlock(&uart->idle_sub);
    uart->hw_event_handler(uart_id, EVENT__CLOSE, NULL);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция очистки встроенного приемного буфера.
 ******************************************************************************/
void hw_uart__rx_flush(int uart_id) {
    hw_uart__itf_t* uart = &hw_uart[uart_id];
    uart->rx_bytes_count = uart->save_bytes = 0;
    uart->rx_isr_cb = NULL;
}

/*******************************************************************************
 * Функция внешнего вызова для обработки текущего состояния драйвера UART.
 ******************************************************************************/
void hw_uart__cout(void) {
    for(int i = 0; i < HW_UART_COUNT; i++) {
        hw_uart__itf_t* uart = &hw_uart[i];
        if (!uart->init)
            continue;
        events__e event = uart->tx_event;
        int bytes_cnt;
        int uart_id = i + HW_UART_1;
        u8* cur_buff = uart->rx_buff + uart->rx_offset;
        if ((event != EVENT__CONTINUE)&& (uart->tx_isr_cb)) {
            uart->tx_event = EVENT__CONTINUE;
            uart->tx_isr_cb(uart_id, event, uart->tx_buff, uart->tx_bytes_count);
        }
        if (((event = uart->rx_event) != EVENT__CONTINUE) && (uart->rx_isr_cb)) {
            uart->rx_event = EVENT__CONTINUE;
            if (uart->save_bytes) {
                bytes_cnt = uart->save_bytes;
                cur_buff = uart->rx_offset ? cur_buff - uart->rx_offset : cur_buff + HW_UART__FIFO_SIZE / 2;
                cur_buff += uart->offset_bytes;
            }
            else {
                uart->offset_bytes = 0;
                UART_CRITICAL_SECTION_ON(uart_id)
                    bytes_cnt = uart->rx_bytes_count;
                    uart->rx_bytes_count = 0;
                    uart->rx_offset ^= HW_UART__FIFO_SIZE / 2;
                UART_CRITICAL_SECTION_OFF(uart_id)
            }
            uart->save_bytes = uart->rx_isr_cb(uart_id, event, cur_buff, bytes_cnt);
            UART_CRITICAL_SECTION_ON(uart_id)
            if (((uart->save_bytes != 0) || (uart->rx_bytes_count)) && (uart->rx_event == EVENT__CONTINUE))
                uart->rx_event = EVENT__OK;
            UART_CRITICAL_SECTION_OFF(uart_id)
            uart->offset_bytes += bytes_cnt - uart->save_bytes;
        }
    }
}

/*******************************************************************************
 * Обобщенная функция-обработчик прерывания по приему и ошибкам.
 ******************************************************************************/
void hw_uart__USART_IRQHandler(int uart_id) {
    USART_TypeDef* UART = hw_uart_descr[uart_id].uart_cmsis;
    hw_uart__itf_t* uart = &hw_uart[uart_id];
    hw_uart__isr_u status_reg;
    status_reg.isr_u32 = UART->ISR;

    //Проверяем на наличие ошибок. Игнорируем noise error.
    if (status_reg.isr.NE)
        UART->ICR = USART_ICR_NCF;
    else if (status_reg.isr.PE | status_reg.isr.FE | status_reg.isr.ORE) {
        UART->ICR = 0x1FFFFF;
        if (UART->CR1 & USART_CR1_RE) {
            UART->CR1 &= ~(USART_CR1_RE | USART_CR1_PEIE | USART_CR1_RXNEIE);
            UART->CR3 &= ~ USART_CR3_EIE;    //Выключаем прерывания на FRAGMING/OVERFLOW/NOISE
//            uart->rx_bytes_count = uart->save_bytes = 0;
            uart->rx_event = EVENT__ERROR;
        }
    }
    //RECIEVE
    if ((UART->CR1 & USART_CR1_RXNEIE) && (status_reg.isr.RXNE)) {
        //Так как флаг очищается при первом его считывании, его необходимо запомнить в буфер
        volatile int buffer = UART->RDR;
        u8 temp[2], buff_len = 1;
        temp[0] = buffer;
        temp[1] = buffer >> 8;

        if (uart->parity == 1)
            temp[0] &= 0x7F;
        else if (uart->wordlenth9 == 1) {
            buff_len = 2;
            temp[1] &= 0x01;
        }
        //Если сам не сбросился -> сбрасываем чтобы не было ошибки overrun
        UART->RQR |= USART_RQR_RXFRQ;
        if ((uart->rx_bytes_count + buff_len) < (HW_UART__FIFO_SIZE / 2)) {
            if (uart->rx_bytes_count < 0)
                uart->rx_bytes_count = 0;
            memcpy(uart->rx_buff + uart->rx_offset + uart->rx_bytes_count, &temp, buff_len);
            uart->rx_bytes_count += buff_len;
            uart->rx_event = EVENT__OK;
        }
        else {
            UART->CR1 &= ~(USART_CR1_RE | USART_CR1_PEIE | USART_CR1_RXNEIE);
            UART->CR3 &= ~ USART_CR3_EIE;    //Выключаем прерывания на FRAGMING/OVERFLOW/NOISE
            uart->rx_event = EVENT__NO_MEM;
        }
    }
    if (READ_BIT(UART->CR1, USART_CR1_TCIE) && (status_reg.isr.TC)) {
        uart->tx_bytes_count++;
        int temp = (uint16_t) uart->tx_buff[uart->tx_bytes_count];
        if (uart->wordlenth9 == 1) {
            temp = ((uint16_t) uart->tx_buff[uart->tx_bytes_count] & 0x01FF);
            if (uart->parity == 1) {
                ;
            }
        }
        if (uart->tx_bytes_count < uart->tx_len)
            UART->TDR = temp;
        else {
            //Передан последний  байт
            //Отключаем прерывания окончания передачи и даем колбэк
            UART->CR1 &= ~ USART_CR1_TCIE;
            uart->tx_event = EVENT__OK;
            if ((uart->settings & UART__FC_MASK) == UART__FC_485) {
                UART->ICR = 0x1FFFFF;
                (volatile void*)UART->RDR; (volatile void*)UART->RDR;
                UART->CR3 |= USART_CR3_EIE;    //Включаем прерывания на FRAGMING/OVERFLOW/NOISE
                SET_BIT(UART->CR1, USART_CR1_RE);
                uart__cmd_handler(uart_id, UART__CMD_FC_DIR_RX);
            }
        }
    }
}

/*******************************************************************************
 * Функция инициализации GPIO связанного с UART.
 ******************************************************************************/
static void hw_uart__itf_en(int uart_id) {
    hw_uart__itf_t* uart = &hw_uart[uart_id];
    // Начинаем STM32 UART инициализацию
    // Включаем тактирование для заданного порта и юарта
    gpio__mode_e pin_mode = uart->settings & UART__PIN_MODE ? GPIO__MODE_OD: GPIO__MODE_PP;
    for (int x = 0; x < 2; x++)
        gpio__af_init(0, hw_uart_descr[uart_id].gpio[x], pin_mode, hw_uart_descr[uart_id].af_nb);
    *hw_uart_descr[uart_id].uart_clock |= hw_uart_descr[uart_id].uart_clock_flag;
}

/*******************************************************************************
 * Функция деинициализации GPIO связанного с UART.
 ******************************************************************************/
static void hw_uart__itf_dis(int uart_id) {
    *hw_uart_descr[uart_id].uart_clock &= ~hw_uart_descr[uart_id].uart_clock_flag;
    for (int x = 0; x < 2; x++)
        gpio__init(0, hw_uart_descr[uart_id].gpio[x], GPIO__DIR_IN, NULL);
}

static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    int uart_id  = (int)ext_data;
    hw_uart__itf_t* uart = &hw_uart[uart_id];
    uart->hw_event_handler(uart_id, EVENT__OPEN, NULL);
}