/***************************************************************************//**
 * @file hw_dac.c.
 * @brief  ������� ������ � ������� ��� �� STM32F676ZI.
 * @author a.tushentsov.
 ******************************************************************************/
#include "mcu.h"
#include "dac.h"

static const struct {
    u32 ctrl, *step_256;
}data[] = {DAC_CR_EN1, (u32*)&DAC->DHR8R1,   // PA4
           DAC_CR_EN2, (u32*)&DAC->DHR8R2};  // PA5

void dac__set(dac__ch_e ch, int voltage) {
    u32 dac_ctrl = data[ch].ctrl, dac_volt = (voltage << 8) / REF_VOLT;
    if (!(RCC->APB1ENR & RCC_APB1ENR_DACEN))
        RCC->APB1ENR |=  RCC_APB1ENR_DACEN;
    if (voltage >= 0) {
        *data[ch].step_256 = dac_volt > 255 ? 255 : dac_volt;
        DAC->CR |= dac_ctrl;
    }
    else
        DAC->CR &= ~dac_ctrl;
}
