/***************************************************************************//**
* @file test_hw_spi.c.
* @brief ���� supervisor ��� stm32l4.
* @authors a.tushentsov.
******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "target.h"
#include "System/fstandard.h"
#include "mcu.h"
#include "DRV/hw_spi.h"

static void hw_spi__itf_en(int spi_id);
static void hw_spi__itf_dis(int spi_id);
static events__e spi_init_regs(int spi_id);
static void hw_spi__break(int spi_id);

// ��������� ���������� SPI.
typedef struct {
    uint32_t settings;                                  // ������� ���������.
    uint8_t *tx_buff;                                   // ��������� �� ����� � ����������������� ������� ��� ��������.
    uint8_t *rx_buff;                                   // ��������� �� ����� � ����������������� ������� ��� ������.
    int len;                                            // ������ ������ � ����������������� ������� ��� �������� � ������.
    hw_spi__cb_t rxtx_isr_cb;                           // ��������� �� ���������������� ������� (�� ����������).
    int bytes_count;                                    // ������� ������.
    int init;                                           // ���� ������������� ����������.
} hw_spi__itf_t;

// ������ ��� ��������� ��������� �� ��������� SPI �� ID.

static hw_spi__itf_t itf[SPI__COUNT];

// ��������� �������� ������� SPI.
 const SPI_TypeDef* const spi__mass[] = {
    SPI1,
    SPI2,
    SPI3,
};

static events__e spi_init_regs(int spi_id) {
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id];
    hw_spi__itf_t *spi = &itf[spi_id];
    SPI->CR1 &=~  SPI_CR1_BR;
    switch (spi->settings & SPI__BR_MASK) {
        case SPI__BR_LOW_SPD:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_1| SPI_CR1_BR_0;
        break;
        case SPI__BR_375_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_1;
        break;
        case SPI__BR_750_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_0;
        break;
        case SPI__BR_1500_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2;
        break;
        case SPI__BR_3_MBIT:
            SPI->CR1 |= SPI_CR1_BR_1| SPI_CR1_BR_0;
        break;
        case SPI__BR_6_MBIT:
            SPI->CR1 |= SPI_CR1_BR_1;
        break;
        case SPI__BR_12_MBIT:
            SPI->CR1 |= SPI_CR1_BR_0;
        break;
        case SPI__BR_24_MBIT:
        case SPI__BR_HIGH_SPD:
        break;
        default:
        return EVENT__PARAM_NA;
    }

//PHASE - POL
    SPI->CR1 &=~ (SPI_CR1_CPHA | SPI_CR1_CPOL);
    switch (spi->settings & SPI__CPHA_CPOL_MASK) {
        case SPI__CPHA_0_CPOL_0:
            break;
        case SPI__CPHA_0_CPOL_1:
            SPI->CR1 |= SPI_CR1_CPOL;
            break;
        case SPI__CPHA_1_CPOL_0:
            SPI->CR1 |= SPI_CR1_CPHA;
            break;
        case SPI__CPHA_1_CPOL_1:
            SPI->CR1 |= SPI_CR1_CPHA | SPI_CR1_CPOL;
            break;
        default:
            return EVENT__PARAM_NA;
    }
//FRAME
    switch (spi->settings & SPI__FRAME_MASK) {
        case SPI__DATA_FRAME_8_BIT:
            SPI->CR2 &=~ SPI_CR2_DS;
            break;
        case SPI__DATA_FRAME_16_BIT:
            SPI->CR2 |= SPI_CR2_DS;
            break;
        default:
            return EVENT__PARAM_NA;
    }
//LSB - MSB
    switch (spi->settings & SPI__LSBMSB_MASK) {
        case SPI__MSB:
            SPI->CR1 &=~ SPI_CR1_LSBFIRST;
            break;
        case SPI__LSB:
            SPI->CR1 |= SPI_CR1_LSBFIRST;
            break;
        default:
            return EVENT__PARAM_NA;
    }
//Software NSS + internal slave select
    SPI->CR1 |= SPI_CR1_SSM | SPI_CR1_SSI;
    SPI->CR2 |= SPI_CR2_NSSP | SPI_CR2_FRXTH;
// MOTOROLLA protocol
    SPI->CR2 &=~ SPI_CR2_FRF;

//MASTER - SLAVE
    switch (spi->settings & SPI__MODE_MASK) {
        case SPI__MODE_MASTER:
            SPI->CR1 |= SPI_CR1_MSTR;
            break;
        case SPI__MODE_SLAVE:       // slave �� ����������
        default:
            return EVENT__PARAM_NA;
            break;
    }
    return EVENT__OK;
}

/*******************************************************************************
 *  ������� �������������  SPIx.
 ******************************************************************************/
events__e hw_spi__init (int spi_id, uint32_t settings) {
/* (+) 1. �������� ����� �� ������� ����� SPI
    (+) 2. �������� ������������ SPI
    (+) 3. ���������� ������� (���������� BR)
    (+) 4. ������� ���������� � ���� (POL/PHA)
    (+) 5. ������� ������ ������ 8/16 ���
    (+) 6. ������� MSB/LSB
    (+) 7. ��������� NSS/CS (�������� SSM/SSI)
    (+) 8. ��������� TI �������� (FRF ���)
    (+) 9. ��������� MASTER
    (+) 10. �������� SPI (SPE)
    (+) 11. ��������� ��� MODF � SR �������� */
    hw_spi__itf_en(spi_id);
    hw_spi__itf_t *spi = &itf[spi_id];
    spi->settings = settings;
    spi->init = 1;
    return EVENT__OK;
}

/***************************************************************************//**
 * ������� ����������� ���������� �������� �������� ������ ��������� SPI.
 ******************************************************************************/
static void hw_spi__break(int spi_id) {
    CRITICAL_SECTION_ON
    switch(spi_id) {
    case SPI_1:
        RCC->APB2RSTR |= RCC_APB2RSTR_SPI1RST;
        RCC->APB2RSTR &= ~RCC_APB2RSTR_SPI1RST;
        break;
    case SPI_2:
        RCC->APB1RSTR |=  RCC_APB1RSTR_SPI2RST;
        RCC->APB1RSTR &=  ~RCC_APB1RSTR_SPI2RST;
        break;
    case SPI_3:
        RCC->APB1RSTR |=  RCC_APB1RSTR_SPI3RST;
        RCC->APB1RSTR &=  ~RCC_APB1RSTR_SPI3RST;
        break;
    }
    CRITICAL_SECTION_OFF
}

/***************************************************************************//**
 * ������� ��������������� ���������� SPI. ���������� ���������� ���� �������� � ��������� ���������.
 ******************************************************************************/
events__e hw_spi__deinit(int spi_id) {
    hw_spi__itf_t *spi = &itf[spi_id];
    hw_spi__break(spi_id);
    spi->init = 0;
    hw_spi__itf_dis(spi_id);
    return EVENT__OK;
}


/***************************************************************************//**
 * ������������� ������ �� SPI.
 ******************************************************************************/
events__e hw_spi__txrx (int spi_id, uint8_t *rx_buff, uint8_t *tx_buff, uint32_t len, hw_spi__cb_t callback){
    events__e res;
    /* ������� ����������� �������� -> ����� �����.
    ������������������ ��������:
    (+) 1. ������ ������������ �� �������� (�������������� CS -> low (��� ������ ������� spi.c)), �������� ����������
    (+) 2. ��������� DR ������ � �������
    (+) 3. � �������� ��������� ���� TXE ����� ���������
    (+) 4. ����� ������������� ���������� ����� ����� �� �������� ����������� (���������� �� TXEIE)

     ������������������ ������:
    (+) 1. ���������� ���������� ������ RX(���� ����� RXNE)
    (+) 2. ���� ���������� �� RXNE
    (+) 3. ������ ������� DR, ��� ����� ������� ���� RXNE */
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id];
    hw_spi__itf_t *spi = &itf[spi_id];
    if ((len == 0) || (rx_buff == NULL && tx_buff == NULL))
        return EVENT__PARAM_NA;

    if ((res = spi_init_regs(spi_id)) != EVENT__OK)
        return res;
    spi->len = len;
    spi->rxtx_isr_cb = callback;
    spi->bytes_count = 0;
    spi->tx_buff = tx_buff;
    spi->rx_buff = rx_buff;

    SPI->DR;
    SPI->CR2 |= SPI_CR2_RXNEIE | SPI_CR2_ERRIE;
    *(uint8_t*)&SPI->DR = (tx_buff) ? *tx_buff : 0xFF;
    //�������� SPI
    SPI->CR1 |= SPI_CR1_SPE;
    //�������� �� ������ ��������� ���������� ������
    if ((SPI->SR & SPI_SR_MODF) != 0) {
        SPI->CR1 = 0;
        return EVENT__ERROR;
    }

    return EVENT__OK;
}

/***************************************************************************//**
 * ������� ��������� �������� ��������� �������� SPI.
 ******************************************************************************/
void hw_spi__cout(void) {
}

/*******************************************************************************
 *  �������-���������� �������� �� SPI.
 ******************************************************************************/
void hw_spi__SPI_IRQHandler(int spi_id) {
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id];
    hw_spi__itf_t *spi = &itf[spi_id];
    events__e code = EVENT__OK;
    int status_reg = SPI->SR;
    uint8_t data = SPI->DR;//*(uint8_t*)SPI->DR;
    // �������� �� ������
    if (status_reg & SPI_SR_OVR)
        code = EVENT__NO_MEM;
    else if (spi->rx_buff)
        spi->rx_buff[spi->bytes_count] = data;
    spi->bytes_count++;
    if ((spi->bytes_count < spi->len) && (code == EVENT__OK))
        *(uint8_t*)&SPI->DR = spi->tx_buff ? spi->tx_buff[spi->bytes_count] : 0xFF;
    else {
        SPI->CR2 &= ~(SPI_CR2_RXNEIE | SPI_CR2_ERRIE);
//        hw_spi__break(spi_id);
        if (spi->rxtx_isr_cb)
            spi->rxtx_isr_cb(spi_id, code, spi->rx_buff ? spi->rx_buff : spi->tx_buff, spi->bytes_count);
    }
}

/*******************************************************************************
 *  ������� ������������� ����������� SPI.
 ******************************************************************************/
typedef struct {
    int gpio[3];
    uint32_t* spi_clock;
    int spi_clock_flag;
    int spi_irq;
}hw_spi_desc__t;
//                                 0                 1               2                 3                 4                  5                6                7                  8                9            10              11
const int spi_1_cs_tbl[] = {MCU__GPIO_P_F_10, MCU__GPIO_P_F_11, MCU__GPIO_P_F_12, MCU__GPIO_P_F_9, MCU__GPIO_P_F_8, MCU__GPIO_P_F_13, MCU__GPIO_P_F_14, MCU__GPIO_P_F_15, MCU__GPIO_NONE, MCU__GPIO_NONE, MCU__GPIO_NONE, MCU__GPIO_NONE, MCU__GPIO_NONE};
const int spi_2_cs_tbl[] = {MCU__GPIO_P_G_10, MCU__GPIO_P_G_11, MCU__GPIO_NONE};
const int spi_3_cs_tbl[] = {MCU__GPIO_P_B_2, MCU__GPIO_NONE};

static const hw_spi_desc__t hw_spi_desc[] = {
     { MCU__GPIO_P_B_3, MCU__GPIO_P_B_5, MCU__GPIO_P_B_4, (uint32_t*)&RCC->APB2ENR, RCC_APB2ENR_SPI1EN, SPI1_IRQn },
     { MCU__GPIO_P_B_13, MCU__GPIO_P_B_15, MCU__GPIO_P_B_14, (uint32_t*)&RCC->APB1ENR, RCC_APB1ENR_SPI2EN, SPI2_IRQn },
//    { { HW_SPI_3_CLK, HW_SPI_3_MOSI, HW_SPI_3_MISO }, (uint32_t*)&RCC->APB1ENR1, RCC_APB1ENR1_SPI3EN, SPI3_IRQn },
};

static void hw_spi__itf_en(int spi_id) {
    for(int spi_param_desc_cnt = 0; spi_param_desc_cnt < 3; spi_param_desc_cnt++)
        gpio__af_init(0, hw_spi_desc[spi_id].gpio[spi_param_desc_cnt], 0, 0x5);
    *hw_spi_desc[spi_id].spi_clock |= hw_spi_desc[spi_id].spi_clock_flag;
    NVIC_SetPriority((IRQn_Type)hw_spi_desc[spi_id].spi_irq, 3);
    NVIC_EnableIRQ((IRQn_Type)hw_spi_desc[spi_id].spi_irq);
}
/*******************************************************************************
 *  ������� ��������������� ����������� SPI.
 ******************************************************************************/
static void hw_spi__itf_dis(int spi_id) {
    NVIC_DisableIRQ((IRQn_Type)hw_spi_desc[spi_id].spi_irq);
    *hw_spi_desc[spi_id].spi_clock &= ~hw_spi_desc[spi_id].spi_clock_flag;
    for(int spi_param_desc_cnt = 0; spi_param_desc_cnt < 3; spi_param_desc_cnt++)
        gpio__init(0, hw_spi_desc[spi_id].gpio[spi_param_desc_cnt],  GPIO__DIR_IN, NULL);
}
