﻿/***************************************************************************//**
 * @file gpio.c.
 * @brief Модуль управления портами ввода-вывода контроллера stm32l15xRBx.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "System/fstandard.h"
#include "DRV/gpio.h"
#include "mcu.h"

// Структура массивов адресов портов GPIO.
 const GPIO_TypeDef* const gpio__mass[] = {
    GPIOA,
    GPIOB,
    GPIOC,
    GPIOD,
    GPIOE,
    GPIOF,
    GPIOG,
    GPIOH,
    GPIOI,
    GPIOJ,
    GPIOK,
};

// Структура модуля GPIO.
typedef struct {
    gpio__isr_cb_t isr_cb[16];
} gpio__t;

static gpio__t gpio_modul;

/*******************************************************************************
 * Прототипы функций
 ******************************************************************************/
static void NVIC_Control(int gpio);

/*******************************************************************************
 * Функция инициализации вывода порта.
 ******************************************************************************/
events__e gpio__init(uint8_t id, uint8_t gpio, int pin_settings, gpio__isr_cb_t isr_cb) {
    int pin = (gpio & 0x0F);
    int port = gpio >> 4;
    int low_bit = (1<<(pin*2));
    int high_bit = (1<<((pin*2)+1));

    if ((((pin_settings & GPIO__INT_MASK) == GPIO__INT_EDGE_RISING || (pin_settings & GPIO__INT_MASK) == GPIO__INT_EDGE_FALING) && isr_cb == NULL) || (gpio == MCU__GPIO_NONE))
        return EVENT__ERROR;

    // Выключаем прерывание пина на случай повторного конфигурирования
    if ((SYSCFG->EXTICR[(pin/4)] & (0xF << (pin%4)*4)) == (port << (pin%4)*4))
        MODIFY_REG(EXTI->IMR, (1 << pin), 0);

    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // Вычисляем адресс порта в памяти.

    // Запуск тактирования порта (А,B, C,D).
    MODIFY_REG(RCC->AHB1ENR, 0, (1 << port));  // (1/0)=>(enabled/disabled)
    // Настраиваем направление (вход/выход).
    if ((pin_settings & GPIO__DIR_MASK) == GPIO__DIR_OUT) { //01 - output
        port_struct->OTYPER &= (~(1 << pin));
        MODIFY_REG(port_struct->MODER, high_bit, low_bit);
    }
    else {   //00 - input (reset state)
        port_struct->OTYPER |= (1 << pin);
        MODIFY_REG(port_struct->MODER, high_bit | low_bit, 0);
    }

    // Настраиваем подтяжку.
    if ((pin_settings & GPIO__PULL_MASK) == GPIO__PULL_UP) {  //01 - pull-up
        MODIFY_REG(port_struct->PUPDR, high_bit, low_bit);
    }
    else if ((pin_settings & GPIO__PULL_MASK) == GPIO__PULL_DOWN) { //10 pull-down
        MODIFY_REG(port_struct->PUPDR, low_bit, high_bit);
    }
    else {
        MODIFY_REG(port_struct->PUPDR, (low_bit | high_bit), 0);
    }

    // Настраиваем фронт прерывания и активируем его.
    if (pin_settings & GPIO__INT_MASK) {
        //Включаем тактирование SYSCFGEN
        RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
        // Настраиваем на прерывания КОНКРЕТНЫЙ пин
        SYSCFG->EXTICR[(pin/4)] &=~ (0xF << (pin%4)*4);
        SYSCFG->EXTICR[(pin/4)] |= (port << (pin%4)*4);
        MODIFY_REG(port_struct->MODER, high_bit | low_bit, 0);   //  configure IO as input
        if (pin_settings & GPIO__INT_EDGE_FALING)
            SET_BIT(EXTI->FTSR, (1 << pin));               //   Rising trigger disabled
        else
            CLEAR_BIT(EXTI->FTSR, (1 << pin));
        if (pin_settings & GPIO__INT_EDGE_RISING)
            SET_BIT(EXTI->RTSR, (1 << pin));               //   Rising trigger disabled
        else
            CLEAR_BIT(EXTI->RTSR, (1 << pin));
        MODIFY_REG(EXTI->IMR, 0, (1 << pin));                    //  Interrupt mask on line x
        //Настраиваем и разрешаем контроллер прерываний
        NVIC_Control(gpio);
    }
    if (isr_cb != NULL)
        gpio_modul.isr_cb[pin] = isr_cb;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция инициализации вывода порта альтернативной функцией. Используется
 * только внутренними модулями для инициализации интерфейсов.
 ******************************************************************************/
events__e gpio__af_init(uint8_t id, uint8_t gpio, gpio__mode_e gpio_mode, int af_number) {
    int pin = (gpio & 0x0F);
    int port = gpio >> 4;
    int pin_pos = pin << 1;
    if (gpio == MCU__GPIO_NONE)
        return EVENT__ERROR;
    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // Вычисляем адресс порта в памяти.
//    MODIFY_REG(EXTI->IMR, (1 << pin), 0);

    MODIFY_REG(RCC->AHB1ENR, 0, (1 << port));  // (1/0)=>(enabled/disabled)
    //настраиваем порт на very high speed
    port_struct->OSPEEDR |= (0x03 << pin_pos);
    //Подтяжка pull-up
    MODIFY_REG(port_struct->PUPDR, 0x03 << pin_pos, gpio_mode << pin_pos);
    //Тип выхода open-drain
    MODIFY_REG(port_struct->OTYPER, 1 << pin, gpio_mode << pin);
    //Настройка альтернативной функции
    MODIFY_REG(port_struct->AFR[pin / 8], 0xf << (4 * (pin % 8)), af_number << (4 * (pin % 8)));

    MODIFY_REG(port_struct->MODER, 0x03 << pin_pos, 0x02 << pin_pos); // Перевести в последнюю очередь, иначе дернет ногой!

    return EVENT__OK;
}

/*******************************************************************************
 * Функция работы с NVIC контроллёром. Может разрешать прерывания,
   а так же устанавливает приоритеты
 ******************************************************************************/
static void NVIC_Control(int gpio) {
    int pin = (gpio & 0x0F);
    if (pin == 0) {
        NVIC_SetPriority(EXTI0_IRQn, 1);
        NVIC_EnableIRQ(EXTI0_IRQn);
    }
    else if (pin == 1) {
        NVIC_SetPriority(EXTI1_IRQn, 1);
        NVIC_EnableIRQ(EXTI1_IRQn);
    }
    else if (pin == 2) {
        NVIC_SetPriority(EXTI2_IRQn, 1);
        NVIC_EnableIRQ(EXTI2_IRQn);
    }
    else if (pin == 3) {
        NVIC_SetPriority(EXTI3_IRQn, 1);
        NVIC_EnableIRQ(EXTI3_IRQn);
    }
    else if (pin == 4) {
        NVIC_SetPriority(EXTI4_IRQn, 1);
        NVIC_EnableIRQ(EXTI4_IRQn);
    }
    else if (((pin >= 5))&&(pin <= 9)){
        NVIC_SetPriority(EXTI9_5_IRQn, 1);
        NVIC_EnableIRQ(EXTI9_5_IRQn);
    }
    else if (((pin >= 10))&&(pin <= 15)){
        NVIC_SetPriority(EXTI15_10_IRQn, 1);
        NVIC_EnableIRQ(EXTI15_10_IRQn);
    }
}

/*******************************************************************************
 * Функция установки логического уровня вывода порта.
 ******************************************************************************/
events__e gpio__set(uint8_t id, uint8_t gpio, gpio__state_e pin_state) {
    int pin_mask = 1<<(gpio & 0x0F);
    int port = gpio >> 4;
    if (gpio == MCU__GPIO_NONE)
        return EVENT__OK;

    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // Вычисляем адресс порта в памяти
    switch (pin_state) {
        case GPIO__STATE_HIGH:
            WRITE_REG(port_struct->BSRR, pin_mask);
            break;
        case GPIO__STATE_LOW:
            WRITE_REG(port_struct->BSRR, pin_mask << 16);
            break;
        case GPIO__STATE_TOGGLE:
            if ((port_struct->ODR & pin_mask) !=  pin_mask)   // если бит 0, то выставляем, иначе- сбрасываем
                WRITE_REG(port_struct->BSRR, pin_mask);
            else
                WRITE_REG(port_struct->BSRR, pin_mask << 16);
            break;
        default:
            break;
    }
    return EVENT__OK;
}

/*******************************************************************************
 * Функция считывания логического уровня указанного вывода порта.
 ******************************************************************************/
gpio__state_e gpio__get(uint8_t id, uint8_t gpio) {
    int pin_mask = 1 << (gpio & 0x0F);
    int port = gpio >> 4;
    if (gpio == MCU__GPIO_NONE)
        return GPIO__STATE_LOW;

    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];
    if (READ_BIT(port_struct->IDR, pin_mask) != 0)
        return GPIO__STATE_HIGH;
    else
        return GPIO__STATE_LOW;
}

/*******************************************************************************
 * Обработчик прерываний для всех портов
 ******************************************************************************/
void gpio__EXTI_IRQHandler(int gpio__exti_port, int gpio__exti_pin) {
    int gpio = (gpio__exti_port << 4) | gpio__exti_pin;
    if (gpio_modul.isr_cb[gpio__exti_pin] != NULL)
        gpio_modul.isr_cb[gpio__exti_pin](0, gpio, gpio__get(0, gpio));
}
