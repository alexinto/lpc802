﻿/***************************************************************************//**
 * @file gpio.c.
 * @brief Модуль управления портами ввода-вывода контроллера stm32l15xRBx.
 * @author a.tushentsov.
 ******************************************************************************/
#ifdef STM32F10X_HD

#include "System/framework.h"
#include "DRV/gpio.h"
#include "mcu.h"

// Структура массивов адресов портов GPIO.
 const GPIO_TypeDef* const gpio__mass[] = {
    GPIOA,
    GPIOB,
    GPIOC,
    GPIOD,
    GPIOE,
    GPIOF,
    GPIOG,
};

// Структура модуля GPIO.
typedef struct {
    gpio__isr_cb_t isr_cb[16];
} gpio__t;

static gpio__t gpio_modul;

/*******************************************************************************
 * Прототипы функций
 ******************************************************************************/
static void NVIC_Control(i32 gpio);

/*******************************************************************************
 * Функция инициализации вывода порта.
 ******************************************************************************/
events__e gpio__init(u8 id, u8 gpio, i32 pin_settings, gpio__isr_cb_t isr_cb) {
    u8 pin = (gpio & 0x0F);
    u8 port = gpio >> 4;
    if ((((pin_settings & GPIO__INT_MASK) == GPIO__INT_EDGE_RISING || (pin_settings & GPIO__INT_MASK) == GPIO__INT_EDGE_FALING) && isr_cb == NULL) || (gpio == MCU__GPIO_NONE))
        return EVENT__ERROR;
    // Выключаем прерывание пина на случай повторного конфигурирования
    if ((AFIO->EXTICR[(pin/4)] & (0xF << (pin%4)*4)) == (port << (pin%4)*4))
        MODIFY_REG(EXTI->IMR, (1 << pin), 0);

    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // Вычисляем адресс порта в памяти.
    u8 cfg_pos;
    __IO u32* cnf_mode;
    if (pin > 7) {
        cfg_pos = pin - 8;
        cnf_mode = &port_struct->CRH;
    }
    else {
        cfg_pos = pin;
        cnf_mode = &port_struct->CRL;
    }
    cfg_pos *= 4;
    // Запуск тактирования порта (А,B, C,D).
    MODIFY_REG(RCC->APB2ENR, 0, (4 << port));  // (1/0)=>(enabled/disabled)
    // Настраиваем направление (вход/выход).
    if ((pin_settings & GPIO__DIR_MASK) == GPIO__DIR_OUT) { //01 - output
        *cnf_mode = (*cnf_mode & (~(0xF << cfg_pos))) | (0x3 << cfg_pos);
    }
    else {   //00 - input (reset state) and AF function
        *cnf_mode = (*cnf_mode & (~(0xF << cfg_pos))) | (0x8 << cfg_pos);
    }
    // Настраиваем подтяжку.
    if ((pin_settings & GPIO__PULL_MASK) == GPIO__PULL_UP) {  //01 - pull-up
        WRITE_REG(port_struct->BSRR, 1 << pin);
    }
    else { //10 pull-down
        WRITE_REG(port_struct->BSRR, 0x10000 << pin);
    }

    // Настраиваем фронт прерывания и активируем его.
    if (pin_settings & GPIO__INT_MASK) {
        //Включаем тактирование AFIOEN
        RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
        // Настраиваем на прерывания КОНКРЕТНЫЙ пин
        AFIO->EXTICR[(pin/4)] &=~ (0xF << (pin%4)*4);
        AFIO->EXTICR[(pin/4)] |= (port << (pin%4)*4);
        if (pin_settings & GPIO__INT_EDGE_FALING)
            SET_BIT(EXTI->FTSR, (1 << pin));               //   Rising trigger disabled
        else
            CLEAR_BIT(EXTI->FTSR, (1 << pin));
        if (pin_settings & GPIO__INT_EDGE_RISING)
            SET_BIT(EXTI->RTSR, (1 << pin));               //   Rising trigger disabled
        else
            CLEAR_BIT(EXTI->RTSR, (1 << pin));
        MODIFY_REG(EXTI->IMR, 0, (1 << pin));                    //  Interrupt mask on line x
        //Настраиваем и разрешаем контроллер прерываний
        NVIC_Control(gpio);
    }
    if (isr_cb != NULL)
        gpio_modul.isr_cb[pin] = isr_cb;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция инициализации вывода порта альтернативной функцией. Используется
 * только внутренними модулями для инициализации интерфейсов.
 ******************************************************************************/
events__e gpio__af_init(u8 id, u8 gpio, gpio__mode_e gpio_mode, i32 af_number) {
    if (gpio == MCU__GPIO_NONE)
        return EVENT__ERROR;
    i32 pin = (gpio & 0x0F);
    i32 port = gpio >> 4;
    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // Вычисляем адресс порта в памяти.
    u8 cfg_pos;
    __IO u32* cnf_mode;
    if (pin > 7) {
        cfg_pos = pin - 8;
        cnf_mode = &port_struct->CRH;
    }
    else {
        cfg_pos = pin;
        cnf_mode = &port_struct->CRL;
    }
    cfg_pos *= 4;
    // Запуск тактирования порта (А,B, C,D).
    MODIFY_REG(RCC->APB2ENR, 0, (4 << port));  // (1/0)=>(enabled/disabled)
    //Включаем тактирование AFIOEN
    RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
    if (gpio == MCU__GPIO_P_A_15)
        AFIO->MAPR |= AFIO_MAPR_SWJ_CFG_JTAGDISABLE; // только SWD без JTAG

    switch(gpio_mode) {
        case GPIO__MODE_PP:
            *cnf_mode = (*cnf_mode & (~(0xF << cfg_pos))) | (0xB << cfg_pos);
            break;
        case GPIO__MODE_OD:
            *cnf_mode = (*cnf_mode & (~(0xF << cfg_pos))) | (0xF << cfg_pos);
            break;
        case GPIO__MODE_ANALOG:
            *cnf_mode = *cnf_mode & (~(0xF << cfg_pos));
            break;
    }
    switch(af_number) {
        case 1:
            if (gpio == MCU__GPIO_P_C_6)               // для TIM3
                 AFIO->MAPR |= AFIO_MAPR_TIM3_REMAP_FULLREMAP;
            break;
        default:
            break;
    }
    
//    MODIFY_REG(EXTI->IMR, (1 << pin), 0);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция работы с NVIC контроллёром. Может разрешать прерывания,
   а так же устанавливает приоритеты
 ******************************************************************************/
static void NVIC_Control(i32 gpio) {
    i32 pin = (gpio & 0x0F);
    if (pin == 0) {
        NVIC_SetPriority(EXTI0_IRQn, 1);
        NVIC_EnableIRQ(EXTI0_IRQn);
    }
    else if (pin == 1) {
        NVIC_SetPriority(EXTI1_IRQn, 1);
        NVIC_EnableIRQ(EXTI1_IRQn);
    }
    else if (pin == 2) {
        NVIC_SetPriority(EXTI2_IRQn, 1);
        NVIC_EnableIRQ(EXTI2_IRQn);
    }
    else if (pin == 3) {
        NVIC_SetPriority(EXTI3_IRQn, 1);
        NVIC_EnableIRQ(EXTI3_IRQn);
    }
    else if (pin == 4) {
        NVIC_SetPriority(EXTI4_IRQn, 1);
        NVIC_EnableIRQ(EXTI4_IRQn);
    }
    else if (((pin >= 5))&&(pin <= 9)){
        NVIC_SetPriority(EXTI9_5_IRQn, 1);
        NVIC_EnableIRQ(EXTI9_5_IRQn);
    }
    else if (((pin >= 10))&&(pin <= 15)){
        NVIC_SetPriority(EXTI15_10_IRQn, 1);
        NVIC_EnableIRQ(EXTI15_10_IRQn);
    }
}

/*******************************************************************************
 * Функция установки логического уровня вывода порта.
 ******************************************************************************/
events__e gpio__set(u8 id, u8 gpio, gpio__state_e pin_state) {
    i32 pin_mask = 1<<(gpio & 0x0F);
    i32 port = gpio >> 4;
    if (gpio == MCU__GPIO_NONE)
        return EVENT__PARAM_NA;

    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // Вычисляем адресс порта в памяти
    switch (pin_state) {
        case GPIO__STATE_HIGH:
            WRITE_REG(port_struct->BSRR, pin_mask);
            break;
        case GPIO__STATE_LOW:
            WRITE_REG(port_struct->BSRR, pin_mask << 16);
            break;
        case GPIO__STATE_TOGGLE:
            if ((port_struct->ODR & pin_mask) !=  pin_mask)   // если бит 0, то выставляем, иначе- сбрасываем
                WRITE_REG(port_struct->BSRR, pin_mask);
            else
                WRITE_REG(port_struct->BSRR, pin_mask << 16);
            break;
        default:
            break;
    }
    return EVENT__OK;
}

/*******************************************************************************
 * Функция считывания логического уровня указанного вывода порта.
 ******************************************************************************/
gpio__state_e gpio__get(u8 id, u8 gpio) {
    i32 pin_mask = 1 << (gpio & 0x0F);
    i32 port = gpio >> 4;
    if (gpio == MCU__GPIO_NONE)
        return GPIO__STATE_LOW;

    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];
    if (READ_BIT(port_struct->IDR, pin_mask) != 0)
        return GPIO__STATE_HIGH;
    else
        return GPIO__STATE_LOW;
}

/*******************************************************************************
 * Обработчик прерываний для всех портов
 ******************************************************************************/
void gpio__EXTI_IRQHandler(i32 gpio__exti_port, i32 gpio__exti_pin) {
    i32 gpio = (gpio__exti_port << 4) | gpio__exti_pin;
    if (gpio_modul.isr_cb[gpio__exti_pin] != NULL)
        gpio_modul.isr_cb[gpio__exti_pin](0, gpio, gpio__get(0, gpio));
}

#endif /* STM32F10X_HD */