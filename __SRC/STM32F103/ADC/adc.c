﻿/***************************************************************************//**
 * @file adc.c.
 * @brief  Драйвер микросхемы ADC.
 * @author a.tushentsov.
 ******************************************************************************/
#ifdef STM32F10X_HD

#include "target.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "DRV/gpio.h"
#include "mcu.h"
#include "ADC/adc.h"

#ifndef ADC__CNT
#define ADC__CNT 1
#endif

#define ADC__TIMEOUT   500        // Таймаут ожидания преобразования, мс.

typedef enum {
    ADC__SAMPLE_1_5    = 0,    // 1.5 cycles
    ADC__SAMPLE_7_5    = 1,    // 7.5 cycles
    ADC__SAMPLE_13_5   = 2,    // 13.5 cycles
    ADC__SAMPLE_28_5   = 3,    // 28.5 cycles
    ADC__SAMPLE_41_5   = 4,    // 41.5 cycles
    ADC__SAMPLE_55_5   = 5,    // 55.5 cycles
    ADC__SAMPLE_71_5   = 6,    // 71.5 cycles
    ADC__SAMPLE_239_5  = 7,    // 239.5 cycles
}adc__sample_e;

typedef enum {
    ADC__STATE_IDLE = 0,
    ADC__STATE_WORK = 1,
}adc__state_e;

// Таблица соответствия вывода каналу ADC         0                1               2                3                4               5                 6                7
static const mcu__gpio_e adc__pin_tbl[] = {MCU__GPIO_P_A_0, MCU__GPIO_P_A_1, MCU__GPIO_P_A_2, MCU__GPIO_P_A_3, MCU__GPIO_P_A_4, MCU__GPIO_P_A_5, MCU__GPIO_P_A_6, MCU__GPIO_P_A_7,
                                           MCU__GPIO_P_B_0, MCU__GPIO_P_B_1, MCU__GPIO_P_C_0, MCU__GPIO_P_C_1, MCU__GPIO_P_C_2, MCU__GPIO_P_C_3};

#define ADC__CH_CNT (sizeof(adc__pin_tbl) / sizeof(adc__pin_tbl[0]))

static const ADC_TypeDef* adc__tbl[] = {ADC1, ADC2};

#if ADC__CNT > 2
#error "Must be define ADC__CNT  1 or 2 in target.h!"
#endif

typedef struct {
    sw_timer__t timer;
    adc__cb_t cb;
    void* ext_data;
    float vref[ADC__CH_CNT];
    adc__state_e state;
    u8 init;
    u8 ch_id;
}adc_data_t;

static framework__sub_t cout_sub;
static adc_data_t adc_data[ADC__CNT];

static events__e adc__hw_init(u8 id);
static void adc__cout();
static void adc__timeout_cb(struct sw_timer__t *timer, void *ext_data);

/*******************************************************************************
 * Функция инициализации ADC. Конфигурирование параметров.
 ******************************************************************************/
events__e adc__init(u8 id, u8 ch_id, float vref) {
    if (id >= ADC__CNT)
        return EVENT__PARAM_NA;
    adc_data_t* d = &adc_data[id];
    if (!d->init) {
        d->init = 1;
        framework__cout_subscribe(&cout_sub, adc__cout);
        adc__hw_init(id);
    }
    d->vref[ch_id] = vref;
    gpio__af_init(0, adc__pin_tbl[ch_id], GPIO__MODE_ANALOG, 0);


    return EVENT__OK;
}

/*******************************************************************************
 * Функция считывания данных ADC. Считывание 1го канала.
 ******************************************************************************/
events__e adc__get(u8 id, u8 ch_id, adc__cb_t cb, void* ext_data) {
    if ((id >= ADC__CNT) || (cb == NULL))
        return EVENT__PARAM_NA;
    adc_data_t* d = &adc_data[id];
    ADC_TypeDef* p_reg = (ADC_TypeDef*)adc__tbl[id];
    if (d->state != ADC__STATE_IDLE)
        return EVENT__BUSY;
    d->state = ADC__STATE_WORK;
    d->cb = cb;
    d->ext_data = ext_data;
    d->ch_id = ch_id;
    MODIFY_REG(p_reg->SQR3, ADC_SQR3_SQ1, ch_id);
    p_reg->SR = 0xFFFF;
    SET_BIT(p_reg->CR2, ADC_CR2_SWSTART);
    sw_timer__start(&d->timer, ADC__TIMEOUT, adc__timeout_cb, (void*)d);
    return EVENT__OK;
}


/*******************************************************************************
 * Функция инициализации аппаратного уровня.
 ******************************************************************************/
static events__e adc__hw_init(u8 id) {
    ADC_TypeDef* p_reg = (ADC_TypeDef*)adc__tbl[id];
    if (!id)
        RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
    else
        RCC->APB2ENR |= RCC_APB2ENR_ADC2EN;
    SET_BIT(RCC->CFGR, RCC_CFGR_ADCPRE_DIV8);
    // Запсукаем однократные выборки сигнала.
    p_reg->CR2 = ADC_CR2_ADON | ADC_CR2_EXTTRIG | ADC_CR2_EXTSEL_0 | ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_2 | ADC_CR2_TSVREFE;
    p_reg->SQR1 = 0;                   // По каждому каналу запускаем однократное преобразование
    p_reg->SMPR2 = ADC__SAMPLE_239_5;  // Выставляем семплирование по 0-му элементу очереди. Так как используем только его.
    return EVENT__OK;
}

/*******************************************************************************
 * Коллбэк по таймауту ожидания преобразования.
 ******************************************************************************/
static void adc__timeout_cb(struct sw_timer__t *timer, void *ext_data) {
    adc_data_t* d = (adc_data_t*)ext_data;
    u8 id = d - adc_data;
    ADC_TypeDef* p_reg = (ADC_TypeDef*)adc__tbl[id];
    if (!p_reg->SR & ADC_SR_EOC) {                            // Проверяем бит завершения преобразования
        d->state = ADC__STATE_IDLE;
        if (d->cb)
            d->cb(id, d->ch_id, EVENT__TIMEOUT, 0, d->ext_data);
    }
}

/*******************************************************************************
 * Функция- обработчик состояния модуля.
 ******************************************************************************/
static void adc__cout() {
    adc_data_t* d;
    ADC_TypeDef* p_reg;
    float value;
    for(int i = 0; i < ADC__CNT; i++) {
        d = &adc_data[i];
        p_reg = (ADC_TypeDef*)adc__tbl[i];
        switch(d->state) {
            case ADC__STATE_IDLE:
                break;
            case ADC__STATE_WORK:
                if (!p_reg->SR & ADC_SR_EOC)
                    break;
                value = d->vref[d->ch_id] * (p_reg->DR & ADC_DR_DATA) / 4096; // Разрядность ADC - 12 бит
                sw_timer__stop(&d->timer);
                d->state = ADC__STATE_IDLE;
                if (d->cb)
                    d->cb(i, d->ch_id, EVENT__OK, value, d->ext_data);
                break;
        }
    }
}



#endif /* STM32F10X_HD */