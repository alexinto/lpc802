﻿/***************************************************************************//**
 * @file hw_spi.c.
 * @brief Модуль работы с внутренней FLASH (реализация для stm32F103).
 * @author a.hamidullin.
 ******************************************************************************/
#include "target.h"
#include "System/framework.h"
#include "System/hw_supervisor.h"
#include "DRV/spi.h"
#include "DRV/gpio.h"
#include "DRV/hw_spi.h"
#include "mcu.h"

#define HW_SPI__TRANSACTION_SIZE    10
#define SPI_DUMMY                   0xFF                              // Передается в полудуплексном режиме (только прием).

// Перечень состояний модуля
typedef enum {
    HW_SPI__STATE_IDLE     = 0,
    HW_SPI__STATE_INIT     = 1,       // Инициализация модуля
    HW_SPI__STATE_TXRX     = 2,       // Передача и прием данных
    HW_SPI__STATE_BREAK    = 3,       // Отмена текущей операции
    HW_SPI__STATE_DEINIT   = 4,       // Передача и прием данных
    HW_SPI__STATE_ERR      = 5,
}hw_spi__state_e;


// Структура, описывающая модуль работы со встроенной FLASH.
typedef struct {
    hw_spi__state_e state; // Текущее состояние.
	u8 spi_id;
	u8 cs_id;
	u8* tx_buff;
	u32 tx_len;
	u8* rx_buff;
	u32 rx_len;
	u32 len;
    u32 sett;
    u8 wait_flag;       // Флаг процесса приема-передачи в текущий момент времени
    hw_spi__cb_t cb;    // Указатель на коллбэк для текущей операции (NULL - нет коллбэка).
	void* ext_data;
} hw_spi__struct_t;

static framework__sub_t cout_sub;

static hw_spi__struct_t hw_spi__data[HW_SPI__COUNT];

static const SPI_TypeDef* hw_spi__reg_tbl[] = {SPI1, SPI2, SPI3};


static events__e hw_spi__break(int spi_id);
static events__e hw_spi__init(u8 spi_id, u8 cs_id, u32 sett_wr, u32 sett_rd);
static events__e hw_spi__cs_low(u8 spi_id, u8 cs_id);
static events__e hw_spi__cs_high(u8 spi_id, u8 cs_id);
static events__e hw_spi__op_txrx(u8 spi_id, hw_spi__cb_t callback);
static events__e hw_spi__txrx(u8 spi_id);
static events__e hw_spi__deinit(u8 spi_id);
static void hw_spi__cout(void);
static uint32_t hw_spi__flp2(uint32_t x);
static uint8_t hw_spi__get_exponent(uint8_t val);

/*******************************************************************************
 * Функция для реализации аппаратного SPI. Соответсвует прототипу hw_spi__func_t.
 ******************************************************************************/
events__e hw_spi__func(hw_spi__cmd_e cmd, u8 spi_id, u8 cs_id, u32 sett_wr, u32 sett_rd, u8 *rx_buff, u32 rx_len, u8 *tx_buff, u32 tx_len, hw_spi__cb_t callback, void* ext_data)
{
	if (spi_id >= HW_SPI__COUNT)
		return EVENT__PARAM_NA;

	if (cout_sub.cout == NULL) {
		framework__cout_subscribe(&cout_sub, hw_spi__cout);
		for(u8 i=0; i< HW_SPI__COUNT; i++)
			hw_spi__data[i].state = HW_SPI__STATE_IDLE;
	}

	hw_spi__struct_t* d = &hw_spi__data[spi_id];

	if(d->state == HW_SPI__STATE_ERR)
		return EVENT__ERROR;
	if(d->state != HW_SPI__STATE_IDLE)
		return EVENT__BUSY;

	d->cb = callback;
	d->ext_data = ext_data;
	d->cs_id = cs_id;
	d->spi_id = spi_id;
	d->rx_buff = rx_buff;
	d->tx_buff = tx_buff;
	d->rx_len = rx_len;
	d->tx_len = tx_len;
	d->len = 0;
    d->sett = sett_wr ? sett_wr : sett_rd;

	switch (cmd) {
		case HW_SPI__CMD_INIT:
            d->state = HW_SPI__STATE_INIT;
            return hw_spi__init(spi_id, cs_id, sett_wr, sett_rd);
		case HW_SPI__CMD_TXRX:
            d->state = HW_SPI__STATE_TXRX;
			return hw_spi__op_txrx(spi_id, callback);
		case HW_SPI__CMD_BREAK:
            d->state = HW_SPI__STATE_BREAK;
			return hw_spi__break(spi_id);
		case HW_SPI__CMD_DEINIT:
			return hw_spi__deinit(spi_id);
		default:
			return EVENT__ERROR;
	}
}


/*******************************************************************************
 * Функция выключения аппаратного SPI. Сбрасывает значения регистров. Инициализирует все выводы SPI как вход.
 ******************************************************************************/
static events__e hw_spi__deinit(u8 spi_id)
{
	SPI_TypeDef* SPI = 0;
	switch(spi_id) {
	case SPI_1:
		SPI = SPI1;
		break;
	default:
		return EVENT__PARAM_NA;
	}

	CLEAR_BIT(SPI->CR1, SPI_CR1_SPE);
    hw_spi__data[spi_id].state = HW_SPI__STATE_IDLE;
	return EVENT__OK;
}

/*******************************************************************************
 * Функция инициализации аппаратного SPI. Производит инициализацию как самого SPI, так и задействованных GPIO.
 ******************************************************************************/
static events__e hw_spi__init(u8 spi_id, u8 cs_id, u32 sett_wr, u32 sett_rd)
{

/*
	SPI_1:
		MCU__GPIO_P_A_5 - SCK
        MCU__GPIO_P_A_6 - MISO
        MCU__GPIO_P_A_7 - MOSI
	SPI_2:
		не поддерживается
	SPI_3:
		не поддерживается

	(-) 1. Включить порты на которых сидит SPI
    (-) 2. Включить тактирование SPI
    (-) 3. Установить скороть (прескейлер BR)
    (-) 4. Выбрать полярность и фазу (POL/PHA)
    (-) 5. Выбрать формат данных 8/16 бит
    (-) 6. Выбрать MSB/LSB
    (-) 7. Настроить NSS/CS (Регистры SSM/SSI)
    (-) 8. Настроить TI протокол (FRF бит)
    (-) 9. Настроить MASTER
    (-) 10. Включить SPI (SPE)
    (-) 11. Проверить бит MODF в SR регистре

*/

    hw_spi__struct_t* d = &hw_spi__data[spi_id];
    d->wait_flag = 0;
  	SPI_TypeDef * SPI = 0;

	switch(spi_id) {
	case SPI_1:
	  	SPI = SPI1;
		gpio__init(0, cs_id, GPIO__DIR_OUT | GPIO__PULL_UP, NULL);
		hw_spi__cs_high(spi_id, cs_id);

		gpio__af_init(0, MCU__GPIO_P_A_5, GPIO__MODE_PP, NULL);
        gpio__init(0, MCU__GPIO_P_A_6, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
		gpio__af_init(0, MCU__GPIO_P_A_7, GPIO__MODE_PP, NULL);
		break;
	default:
		return EVENT__PARAM_NA;
	}

  	CLEAR_BIT(SPI->CR1, SPI_CR1_SPE);

	switch(spi_id) {
	case SPI_1:
		SET_BIT(RCC->APB2ENR, RCC_APB2ENR_SPI1EN);
		break;
	case SPI_2:
		SET_BIT(RCC->APB1ENR, RCC_APB1ENR_SPI2EN);
	case SPI_3:
		SET_BIT(RCC->APB1ENR, RCC_APB1ENR_SPI3EN);
	default:
		return EVENT__ERROR;
	}

	CLEAR_BIT(SPI1->CR1, SPI_CR1_BR_0
						| SPI_CR1_BR_1
						| SPI_CR1_BR_2
						| SPI_CR1_CPHA
						| SPI_CR1_CPOL
						| SPI_CR1_DFF
						| SPI_CR1_LSBFIRST
						| SPI_CR1_SSM
						| SPI_CR1_SSI
			  );

	u32 freq_max = hw_spi__flp2(hw_supervisor__periph_clk_get() / 1000000);
	u8 freq = (sett_wr & SPI__BR_MASK) >> SPI__BR_POS;
	if (freq > freq_max) {
		freq = freq_max;
	}

	freq = hw_spi__flp2(freq);
	u8 prescaler = hw_spi__get_exponent(freq_max / freq);

	if (prescaler > 7) {
		prescaler = 7;
	}

	if (prescaler & 0x01) {
		SET_BIT(SPI->CR1, SPI_CR1_BR_0);
	}
	if (prescaler & 0x02) {
		SET_BIT(SPI->CR1, SPI_CR1_BR_1);
	}
	if (prescaler & 0x04) {
		SET_BIT(SPI->CR1, SPI_CR1_BR_2);
	}

	u32 cpha = (sett_wr & SPI__CPHA_1_CPOL_0) >> SPI__CPHA_CPOL_POS;
	if (cpha) {
		SET_BIT(SPI->CR1, SPI_CR1_CPHA);
	} else {
		CLEAR_BIT(SPI->CR1, SPI_CR1_CPHA);
	}

	u32 cpol = (sett_wr & SPI__CPHA_0_CPOL_1) >> SPI__CPHA_CPOL_POS;
	if (cpol) {
		SET_BIT(SPI->CR1, SPI_CR1_CPOL);
	} else {
		CLEAR_BIT(SPI->CR1, SPI_CR1_CPOL);
	}

	u32 bit16 = (sett_wr & SPI__DATA_FRAME_16_BIT) >> SPI__FRAME_POS;
	if (bit16) {
		SET_BIT(SPI->CR1, SPI_CR1_DFF);
	} else {
		CLEAR_BIT(SPI->CR1, SPI_CR1_DFF);
	}

	u32 msb = (sett_wr & SPI__MSB) >> SPI__LSBMSB_POS;
	if (msb) {
		CLEAR_BIT(SPI->CR1, SPI_CR1_LSBFIRST);
	} else {
		SET_BIT(SPI->CR1, SPI_CR1_LSBFIRST);
	}

	SET_BIT(SPI->CR1, SPI_CR1_SSM);  // software CS
	SET_BIT(SPI->CR1, SPI_CR1_SSI);

	u32 master = (sett_wr & SPI__MODE_MASTER) >> SPI__MODE_POS;
	if (master) {
		SET_BIT(SPI->CR1, SPI_CR1_MSTR);
	} else {
		CLEAR_BIT(SPI->CR1, SPI_CR1_MSTR);
	}

	SET_BIT(SPI->CR1, SPI_CR1_SPE);

	if (READ_BIT(SPI->SR, SPI_SR_MODF)) {
		return EVENT__ERROR;
	}

    hw_spi__data[spi_id].state = HW_SPI__STATE_IDLE;
	return EVENT__OK;
}

/*******************************************************************************
 * Функция приемо-передачи данных по шине SPI.
 ******************************************************************************/
static events__e hw_spi__op_txrx(u8 spi_id, hw_spi__cb_t callback)
{
    hw_spi__struct_t* d = &hw_spi__data[spi_id];
    d->wait_flag = 1;
    u8 cur_transaction_cnt = 0;
	hw_spi__cs_low(d->spi_id, d->cs_id);

	if (callback != NULL) {
		d->cb = callback;
		d->wait_flag = 1;
		supervisor__idle_lock(d->idle_sub);
		return EVENT__OK;
	}


    while((d->len < d->tx_len) || (d->len < d->rx_len)) {
        hw_spi__txrx(spi_id);
        cur_transaction_cnt++;
        if (cur_transaction_cnt >= HW_SPI__TRANSACTION_SIZE) {
            return EVENT__OK;
        }
    }

	hw_spi__cs_high(d->spi_id, d->cs_id);

	d->wait_flag = 0;
    d->state = HW_SPI__STATE_IDLE;
	return EVENT__OK;
}

/*******************************************************************************
 * Низкоуровневая функция приемо-передачи данных по шине SPI.
 ******************************************************************************/
static events__e hw_spi__txrx(u8 spi_id)
{
    hw_spi__struct_t* d = &hw_spi__data[spi_id];
    SPI_TypeDef* SPI = (SPI_TypeDef*)hw_spi__reg_tbl[d->spi_id];

	while(!READ_BIT(SPI->SR, SPI_SR_TXE));
	if (d->tx_buff && (d->len < d->tx_len))
		WRITE_REG(SPI->DR, d->tx_buff[d->len]);
	else
		WRITE_REG(SPI->DR, SPI_DUMMY);

	while(!READ_BIT(SPI->SR, SPI_SR_RXNE));
	if (d->rx_buff && (d->len < d->rx_len))
		d->rx_buff[d->len] = READ_REG(SPI->DR);
	else
		READ_REG(SPI->DR);

	while(READ_BIT(SPI->SR, SPI_SR_BSY));
    d->len++;

	return EVENT__OK;
}


/*******************************************************************************
 * Фукнция немедленного прерывания приемо-передачи данных по SPI.
 ******************************************************************************/
static events__e hw_spi__break(int spi_id) {
    CRITICAL_SECTION_ON
    switch(spi_id) {
    case SPI_1:
		SET_BIT(RCC->APB2RSTR, RCC_APB2RSTR_SPI1RST);
		CLEAR_BIT(RCC->APB2RSTR, RCC_APB2RSTR_SPI1RST);
        return EVENT__OK;
    case SPI_2:
		SET_BIT(RCC->APB1RSTR, RCC_APB1RSTR_SPI2RST);
		CLEAR_BIT(RCC->APB1RSTR, RCC_APB1RSTR_SPI2RST);
        return EVENT__OK;
    case SPI_3:
		SET_BIT(RCC->APB1RSTR, RCC_APB1RSTR_SPI3RST);
		CLEAR_BIT(RCC->APB1RSTR, RCC_APB1RSTR_SPI3RST);
        return EVENT__OK;
    }
    CRITICAL_SECTION_OFF
	return EVENT__ERROR;
}

/*******************************************************************************
 * Функция выбора ведомого устройства SPI.
 ******************************************************************************/
static events__e hw_spi__cs_low(u8 spi_id, u8 cs_id)
{
	switch(spi_id) {
    case SPI_1:
		gpio__set(NULL, cs_id, GPIO__STATE_LOW);
		return EVENT__OK;
	default:
	  	return EVENT__PARAM_NA;
	}
}

/*******************************************************************************
 * Функция снятия выбора ведомого устройства SPI.
 ******************************************************************************/
static events__e hw_spi__cs_high(u8 spi_id, u8 cs_id)
{
	switch(spi_id) {
    case SPI_1:
		gpio__set(NULL, cs_id, GPIO__STATE_HIGH);
		return EVENT__OK;
	default:
	  	return EVENT__PARAM_NA;
	}
}


/*******************************************************************************
 * Служебная функция для пересчета требуемой частоты шины SPI в состояние регистров
 ******************************************************************************/
static uint32_t hw_spi__flp2(uint32_t x)
{
    x = x | (x >> 1);
    x = x | (x >> 2);
    x = x | (x >> 4);
    x = x | (x >> 8);
    x = x | (x >> 16);
    return x - (x >> 1);
}

/*******************************************************************************
 * Служебная функция для пересчета требуемой частоты шины SPI в состояние регистров
 ******************************************************************************/
static uint8_t hw_spi__get_exponent(uint8_t val)
{
  static const uint8_t byte[256] =
  {
    [1]   = 0,
    [2]   = 1,
    [4]   = 2,
    [8]   = 3,
    [16]  = 4,
    [32]  = 5,
    [64]  = 6,
    [128] = 7,
  };

  return byte[val & 0xFF];
}


/*******************************************************************************
 * Функция обработки текущего состояния модуля работы с FLASH.
 ******************************************************************************/
static void hw_spi__cout(void) {
	hw_spi__struct_t* d;
	events__e res = EVENT__ERROR;

	for(u8 i=0; i< HW_SPI__COUNT; i++) {
		d = &hw_spi__data[i];

        switch (d->state) {
            case HW_SPI__STATE_INIT:
                hw_spi__init(d->spi_id, d->cs_id, d->sett, d->sett);
                d->state = HW_SPI__STATE_IDLE;
                break;
            case HW_SPI__STATE_TXRX:
                hw_spi__op_txrx(d->spi_id, NULL);
                if (d->wait_flag == 0) {
                    d->state = HW_SPI__STATE_IDLE;
                }
                break;
            case HW_SPI__STATE_BREAK:
                hw_spi__break(d->spi_id);
                d->state = HW_SPI__STATE_IDLE;
                break;
            case HW_SPI__STATE_DEINIT:
                hw_spi__deinit(d->spi_id);
                d->state = HW_SPI__STATE_IDLE;
                break;
            case HW_SPI__STATE_IDLE:
                d->state = HW_SPI__STATE_IDLE;
                break;
            default:
                d->state = HW_SPI__STATE_ERR;
                break;
        }
        if (d->cb){
            if (d->wait_flag == 0) {
                d->cb(d->spi_id, d->cs_id, res, d->rx_buff, d->rx_len, d->ext_data);
            }
        }
    }

}
