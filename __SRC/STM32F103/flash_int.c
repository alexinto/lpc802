﻿/***************************************************************************//**
 * @file flash_int.c.
 * @brief Модуль работы с внутренней FLASH (реализация для stm32F103).
 * @author a.hamidullin.
 ******************************************************************************/
#include "target.h"
#include "System/framework.h"
#include "System/supervisor.h"
#include "flash_int.h"
#include "mcu.h"

#ifndef FLASH_INT__COUNT
#define FLASH_INT__COUNT 1
#endif

/*******************************************************************************
 * Описание встроенной FLASH памяти МК.
 ******************************************************************************/
#define FLASH_INT__INFO_MEM_BEGIN             ((uint32_t)0x1FFFF000)
#define FLASH_INT__INFO_MEM_END               ((uint32_t)0x1FFFF7FF)
#define FLASH_INT__OPTION_BYTES_MEM_BEGIN     ((uint32_t)0x1FFFF800)
#define FLASH_INT__OPTION_BYTES_MEM_END       ((uint32_t)0x1FFFF80F)
#define FLASH_INT__MAIN_MEM_BEGIN             ((uint32_t)0x08000000)
#define FLASH_INT__MAIN_MEM_END               ((uint32_t)0x0803FFFF)


#define FLASH_INT__PAGE_SIZE           (2048U)                       // FLASH Page Size in bytes

// Типы выполняемых операций.
typedef enum {
    FLASH_INT__OPER_NA,             // Нет операции.
    FLASH_INT__OPER_R,              // Чтение.
    FLASH_INT__OPER_W,              // Запись.
    FLASH_INT__OPER_CP,             // Очистка страницы.
    FLASH_INT__OPER_CS,             // Очистка сектора.
    FLASH_INT__OPER_CA,             // Очистка всей памяти.
} flash_int__operation_e;

// Структура, описывающая модуль работы со встроенной FLASH.
typedef struct {
    flash__cb_t cb;                                                 // Указатель на коллбэк для текущей операции (NULL - нет коллбэка).
    flash_int__operation_e cur_oper;                                // Текущая операция.
    supervisor__idle_sub_t idle_sub;                                // Идентификатор подписчика на блокировку ухода в сон (нужен при коллбэке).
    framework__sub_t cout_sub;
    uint8_t *data;                                                  // Указатель на пользовательские данные.
    void *ext_data;                                                 // Указатель на пользовательские данные.
    uint32_t cur_clk;                                               // Текущая частота тактирования FLASH.
#if __DATA_MODEL__ == __DATA_MODEL_SMALL__
    uint32_t addr;                                                  // Адрес, по которому осуществляется операция.
#else
    uint32_t addr;                                                  // Адрес, по которому осуществляется операция.
#endif
    uint32_t len;                                                   // Размер пользовательских данных.
    u8 init;                                                        // Флаг инициализации модуля работы с FLASH (эмулятора).
} flash_int__struct_t;

static flash_int__struct_t flash;

static events__e flash_int__op_page_erase(void);
static events__e flash_int__op_write(void);
static events__e flash_int__op_erase(void);
static events__e flash_int__op_read(void);

static int get_addr_begin(int addr);
static events__e flash__unlock(void);
static events__e flash_lock(void);
static void flash_int__cout(void);

static events__e flash_int__read(uint32_t addr, uint8_t *buff, uint32_t len, flash__cb_t read_cb, void *ext_data);
static events__e flash_int__erase(uint32_t addr, uint32_t len, flash__cb_t erase_cb, void *ext_data);
static events__e flash_int__write(uint32_t addr, uint8_t *data, uint32_t len, flash__cb_t write_cb, void *ext_data);


/*******************************************************************************
 * Функция инициализации драйвера встроенной флеш-памяти.
 * Функционал дублируется функцией flash_int__exec с операцией FLASH__OP_INIT.
 ******************************************************************************/
events__e flash_int__init(i32 dev_id, flash__type_e type, u32 settings, flash__cb_t cb)
{
	framework__cout_subscribe(&flash.cout_sub, flash_int__cout);
	return cb ? EVENT__PARAM_NA : EVENT__OK;
}

/*******************************************************************************
 * Функция запуска операций со встроенной флеш-памятью.
 ******************************************************************************/
events__e flash_int__exec(uint8_t dev_id, flash__oper_e oper, uint32_t addr, uint8_t *buff, uint32_t len, flash__cb_t cb, void* ext_data)
{
	switch (oper) {
		case FLASH__OP_INIT:
        	framework__cout_subscribe(&flash.cout_sub, flash_int__cout);
            return cb ? EVENT__PARAM_NA : EVENT__OK;
		case FLASH__OP_ERASE:
			return flash_int__erase(addr, len, cb, ext_data);
		case FLASH__OP_READ:
			return flash_int__read(addr, buff, len, cb, ext_data);
		case FLASH__OP_WRITE:
			return flash_int__write(addr, buff, len, cb, ext_data);
		default:
			return EVENT__ERROR;
	}
}


/*******************************************************************************
 * Функция стирания страницы.
 ******************************************************************************/
static uint8_t flash_ready(void)
{
	return !READ_BIT(FLASH->SR, FLASH_SR_BSY);
}

static events__e flash_int__erase(uint32_t addr, uint32_t len, flash__cb_t erase_cb, void *ext_data) {
	if (!(addr >= FLASH_INT__MAIN_MEM_BEGIN && addr <= FLASH_INT__MAIN_MEM_END))
		return EVENT__PARAM_NA;
	addr = get_addr_begin(addr); //Вычисление начала страницы
	flash.cur_oper = FLASH_INT__OPER_CP;
	flash.addr = addr;
	flash.len = len;
	if (erase_cb != NULL) {     // Если указатель, то произведем операцию в коллауте.
		flash.cb = erase_cb;
		flash.ext_data = ext_data;
		supervisor__idle_lock(&flash.idle_sub);
		return EVENT__OK;
	}
	events__e res = flash_int__op_erase();
	return res;
}

/*******************************************************************************
 * Функция записи данных во FLASH.
 ******************************************************************************/
static events__e flash_int__write(uint32_t addr, uint8_t *data, uint32_t len, flash__cb_t write_cb, void *ext_data) {
	if ( !(addr >= FLASH_INT__INFO_MEM_BEGIN && addr <= FLASH_INT__INFO_MEM_END) &&
		!(addr >= FLASH_INT__OPTION_BYTES_MEM_BEGIN && addr <= FLASH_INT__OPTION_BYTES_MEM_END) &&
		!(addr >= FLASH_INT__MAIN_MEM_BEGIN && addr <= FLASH_INT__MAIN_MEM_END)
	)
		return EVENT__PARAM_NA;

	if ( !(addr + len >= FLASH_INT__INFO_MEM_BEGIN && addr + len <= FLASH_INT__INFO_MEM_END) &&
		!(addr + len >= FLASH_INT__OPTION_BYTES_MEM_BEGIN && addr + len <= FLASH_INT__OPTION_BYTES_MEM_END) &&
		!(addr + len >= FLASH_INT__MAIN_MEM_BEGIN && addr + len <= FLASH_INT__MAIN_MEM_END)
	)
		return EVENT__PARAM_NA;

	if (data == NULL || len == 0)
		return EVENT__PARAM_NA;

	flash.cur_oper = FLASH_INT__OPER_W;
	flash.addr = addr;
	flash.data = data;
	flash.len = len;
	if (write_cb != NULL) {     // Если указатель, то произведем операцию в коллауте.
		flash.cb = write_cb;
		flash.ext_data = ext_data;
		supervisor__idle_lock(&flash.idle_sub);
		return EVENT__OK;
	}
	events__e res = flash_int__op_write();

	return res;
}

/*******************************************************************************
 * Функция чтения данных из FLASH в указанный буфер.
 ******************************************************************************/
static events__e flash_int__read(uint32_t addr, uint8_t *buff, uint32_t len, flash__cb_t read_cb, void *ext_data) {
	if ( !(addr >= FLASH_INT__INFO_MEM_BEGIN && addr <= FLASH_INT__INFO_MEM_END) &&
		!(addr >= FLASH_INT__OPTION_BYTES_MEM_BEGIN && addr <= FLASH_INT__OPTION_BYTES_MEM_END) &&
		!(addr >= FLASH_INT__MAIN_MEM_BEGIN && addr <= FLASH_INT__MAIN_MEM_END)
	)
		return EVENT__PARAM_NA;

	if ( !(addr + len >= FLASH_INT__INFO_MEM_BEGIN && addr + len <= FLASH_INT__INFO_MEM_END) &&
		!(addr + len >= FLASH_INT__OPTION_BYTES_MEM_BEGIN && addr + len <= FLASH_INT__OPTION_BYTES_MEM_END) &&
		!(addr + len >= FLASH_INT__MAIN_MEM_BEGIN && addr + len <= FLASH_INT__MAIN_MEM_END)
	)
		return EVENT__PARAM_NA;

	if (buff == NULL || len == 0)
		return EVENT__PARAM_NA;

	flash.cur_oper = FLASH_INT__OPER_R;
	flash.addr = addr;
	flash.data = buff;
	flash.len = len;
	if (read_cb != NULL) {     // Если указатель, то произведем операцию в коллауте.
		flash.cb = read_cb;
		flash.ext_data = ext_data;
		supervisor__idle_lock(&flash.idle_sub);
		return EVENT__OK;
	}
	events__e res = flash_int__op_read();

	return res;
}

/*******************************************************************************
 * Функция обработки текущего состояния модуля работы с FLASH.
 ******************************************************************************/
static void flash_int__cout(void) {
	events__e res;
	supervisor__idle_unlock(&flash.idle_sub);
    flash__oper_e oper;

	switch (flash.cur_oper) {
		case FLASH_INT__OPER_R: {
			res = flash_int__op_read();
            oper = FLASH__OP_READ;
			break;
		}
		case FLASH_INT__OPER_W: {
			res = flash_int__op_write();
            oper = FLASH__OP_WRITE;
			break;
		}
		case FLASH_INT__OPER_CP: {
			res = flash_int__op_erase();
            oper = FLASH__OP_ERASE;
			break;
		}
		default:
            return;
	}
    if (flash.cb)
        flash.cb(0, oper, res, flash.addr, flash.data, flash.len, flash.ext_data); // 0- только одна флеш доступна
}


/*******************************************************************************
 * Функция осуществления операции стирания страницы.
 ******************************************************************************/

static events__e flash_int__op_page_erase(void) {
	events__e res = EVENT__OK;
	CRITICAL_SECTION_ON

	res = flash__unlock();
	if (res != EVENT__OK)
		return EVENT__ERROR;

	SET_BIT(FLASH->CR, FLASH_CR_PER);
	WRITE_REG(FLASH->AR, flash.addr);
	SET_BIT(FLASH->CR, FLASH_CR_STRT);

	while(!flash_ready());

	CLEAR_BIT(FLASH->CR, FLASH_CR_PER);

	res = flash_lock();
	if (res != EVENT__OK)
		return EVENT__ERROR;

	flash.cur_oper = FLASH_INT__OPER_NA;
	CRITICAL_SECTION_OFF
	return res;
}


/*******************************************************************************
 * Функция осуществления операции стирания определенного количества байт.
 ******************************************************************************/

static events__e flash_int__op_erase(void) {
	events__e res = EVENT__OK;

	if (flash.len % FLASH_INT__PAGE_SIZE) {
		return EVENT__ERROR;
	}

	uint8_t cnt = flash.len / FLASH_INT__PAGE_SIZE;

	for (uint8_t i=0; i<cnt; i++) {
		res = flash_int__op_page_erase();
		if (res != EVENT__OK) {
			return res;
		}
	  	flash.addr += FLASH_INT__PAGE_SIZE;
	}

	return res;
}

/*******************************************************************************
 * Функция осуществления операции записи.
 ******************************************************************************/
static events__e flash_int__op_write(void) {
    events__e res;

    uint16_t* srcw = (uint16_t*)flash.data;
    uint16_t* dstw = (uint16_t*)flash.addr;
    uint32_t lenw = flash.len / 2;

	if (flash.len % 2) {
		return EVENT__ERROR;
	}

    CRITICAL_SECTION_ON

    res = flash__unlock();
    if (res != EVENT__OK)
		return EVENT__ERROR;

    SET_BIT(FLASH->CR, FLASH_CR_PG);

    while (lenw)
    {
		*dstw = *srcw;
		while (!flash_ready());
		if (*dstw != *srcw)
			return EVENT__ERROR;
		dstw++;
		srcw++;
		lenw--;
	}

	CLEAR_BIT(FLASH->CR, FLASH_CR_PG);

	res = flash_lock();
	if (res != EVENT__OK)
		return EVENT__ERROR;

	flash.cur_oper = FLASH_INT__OPER_NA;
	CRITICAL_SECTION_OFF
	return EVENT__OK;
}

/*******************************************************************************
 * Функция осуществления операции чтения.
 ******************************************************************************/
static events__e flash_int__op_read(void) {
	CRITICAL_SECTION_ON

	memcpy(flash.data, (uint32_t*) flash.addr, flash.len);
	flash.cur_oper = FLASH_INT__OPER_NA;
	CRITICAL_SECTION_OFF
	return EVENT__OK;
}

static events__e flash__unlock(void) {

	if (READ_BIT(FLASH->CR, FLASH_CR_LOCK))
	{
		WRITE_REG(FLASH->KEYR, FLASH_KEY1);
		WRITE_REG(FLASH->KEYR, FLASH_KEY2);
	}

	if (READ_BIT(FLASH->CR, FLASH_CR_LOCK))
	{
		return EVENT__ERROR;
	}
	return EVENT__OK;
}

static events__e flash_lock(void) {

	SET_BIT(FLASH->CR, FLASH_CR_LOCK);

	if (!READ_BIT(FLASH->CR, FLASH_CR_LOCK))
	{
		return EVENT__ERROR;
	}

	return EVENT__OK;
}

static int get_addr_begin(int addr) {
	addr -= FLASH_INT__MAIN_MEM_BEGIN;
	int page_nuber = addr/FLASH_INT__PAGE_SIZE;
	int begin_number = (FLASH_INT__MAIN_MEM_BEGIN + (FLASH_INT__PAGE_SIZE ) * page_nuber);
	return begin_number;
}
