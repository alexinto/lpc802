﻿/***************************************************************************//**
 * @file sw_timer.c.
 * @brief Модуль программных таймеров.
 * @author a.tushentsov
 ******************************************************************************/
#include <stddef.h>
#include <string.h>
#include "target.h"
#include "System/list.h"
#include "System/sw_timer.h"
#include "System/hw_timer.h"
#include "System/supervisor.h"
#include "System/framework.h"

#define SW_TIMER__TIME_PLUS(struct_addr,field_name, time, ms)       \
        (struct_addr)->field_name = sw_timer__time_plus((time), (ms))

// Структура модуля программных таймеров.
typedef struct
{
    sw_timer__head_t work_list;              // Список запущенных и ждущих запуска таймеров
    sw_timer__head_t cb_list;                // Список таймеров, ждущих cb в cout
    u8 init;                            // Флаг инициализации модуля.
    sw_timer__event_handler_t event_handler; // Указатель на пользовательскую функцию-обработчик событий/ошибок модуля программных таймеров.
    supervisor__idle_sub_t  idle_sub;        // Идентификатор подписчика на блокировку ухода в сон для модуля программных тацмеров.
    framework__sub_t cout_sub;
} sw_timer__struct_t;

static sw_timer__struct_t sw_timer__modul;

void sw_timer__hw_timer_isr_handler();
static void sw_timer__cout();

/*******************************************************************************
 * Функция инициализации модуля программных таймеров.
 * Настраивает аппаратный таймер, от которого работают все программные таймеры.
 ******************************************************************************/
events__e sw_timer__init(sw_timer__event_handler_t event_handler) {
    if (sw_timer__modul.init == 1)
        return EVENT__OK;
    sw_timer__modul.init = 1;
    sw_timer__modul.event_handler = event_handler;
    list__init(&sw_timer__modul.work_list);
    list__init(&sw_timer__modul.cb_list);
    framework__cout_subscribe(&sw_timer__modul.cout_sub, sw_timer__cout);
    return hw_timer__init(sw_timer__hw_timer_isr_handler, event_handler);
}

/***************************************************************************//**
 * Функция запуска программного таймера.
 ******************************************************************************/
events__e sw_timer__start(sw_timer__t *timer, i32 timeout_ms, sw_timer__callback_t cb, void *ext_data) {
    if (timer == NULL || timeout_ms < -1)
        return EVENT__PARAM_NA;
    sw_timer__t* before = NULL;
    i32 is_first = 1;
    sw_timer__stop(timer);
    timer->cb = cb;
    timer->ext_data = ext_data;
    sw_timer__sys_time_t cur_time, alarm_time, time;
    if (cb == NULL) {                                  // если таймер синхронный
        hw_timer__sys_time_get(&alarm_time);
        alarm_time = sw_timer__time_plus(alarm_time, timeout_ms);
        do {
            hw_timer__sys_time_get(&cur_time);
        } while(sw_timer__time_compare(cur_time, alarm_time) < 0);
        return EVENT__OK;
    }
    switch(timeout_ms) {
        case -1:
            SW_TIMER__TIME_PLUS(timer, alarm, timer->alarm, (u32)timer->timeout_ms);
            break;
        default:
            hw_timer__sys_time_get(&time);
            SW_TIMER__TIME_PLUS(timer, alarm, time, (u32)timeout_ms);
            timer->timeout_ms = timeout_ms;
            break;
    }
    CRITICAL_SECTION_ON
    while( (before = list__foreach(&sw_timer__modul.work_list, before, LIST__HEAD)) != NULL) {
        i32 res = sw_timer__time_compare(before->alarm, timer->alarm);
        switch(res) {
            case -1: // мы точно позже
                is_first = 0;
                continue;
            case 0:  // будильник уже установлени
                if(timeout_ms != -1)
                    is_first = 0;
        }
        break;          // ищем дальше
    }
    if(is_first) {
        list__head_include(&sw_timer__modul.work_list, timer);
        hw_timer__start(timer->alarm);
    }
    else {
        if(before == NULL)
            list__tail_include(&sw_timer__modul.work_list, timer);
        else
            list__tail_include(before, timer);
    }
    timer->head = &sw_timer__modul.work_list;
    CRITICAL_SECTION_OFF
    return EVENT__OK;
}

/***************************************************************************//**
 * Функция остановки программного таймера
 ******************************************************************************/
events__e sw_timer__stop(sw_timer__t *stop_timer) {
    sw_timer__sys_time_t time = {.ms = -1};
    CRITICAL_SECTION_ON
    if(stop_timer->head == &sw_timer__modul.work_list) {
        if(sw_timer__modul.work_list.next == stop_timer) { // мы первые
            if((void*)stop_timer->next != (void*)&sw_timer__modul.work_list)
                hw_timer__start( ((sw_timer__t*)stop_timer->next)->alarm);
            else
                hw_timer__start(time);
        }
        list__exclude_me(stop_timer);
    }
    else if(stop_timer->head == &sw_timer__modul.cb_list)
         list__exclude_me(stop_timer);
    stop_timer->head = NULL;
    CRITICAL_SECTION_OFF
    return EVENT__OK;
}

/*******************************************************************************
 * Обработчик очереди программных таймеров
 ******************************************************************************/
void sw_timer__hw_timer_isr_handler() {
    sw_timer__sys_time_t alarm;
    hw_timer__sys_time_get(&alarm);
// Обеспечение непрерываемости sw_timer__hw_timer_isr_handler перенесена на уровень hw_timer
    sw_timer__t* timer;
    while((timer = (sw_timer__t*)sw_timer__modul.work_list.next) != (sw_timer__t*)&sw_timer__modul.work_list) {
        i32 res = sw_timer__time_compare(alarm, timer->alarm);
        if(res >= 0) { // мы раньше или сейчас    ==> сработка
            list__exclude_me(timer);
            list__tail_include(&sw_timer__modul.cb_list, timer);
            timer->head = &sw_timer__modul.cb_list;
            timer->alarm = alarm;
            continue;
        }
        alarm = timer->alarm;
        break;
    }
    if(list__is_empty(&sw_timer__modul.work_list))
        alarm.ms = -1;
    hw_timer__start(alarm);
    if(!list__is_empty(&sw_timer__modul.cb_list))
        supervisor__idle_lock(&sw_timer__modul.idle_sub);
}

/***************************************************************************//**
 * Функция внешнего вызова для обработки текущего состояния модуля программных таймеров.
 ******************************************************************************/
static void sw_timer__cout() {
    hw_timer__cout();
    supervisor__idle_unlock(&sw_timer__modul.idle_sub);
    if(list__is_empty(&sw_timer__modul.cb_list))
      return;
    sw_timer__t* timer;
    while(1) {
        CRITICAL_SECTION_ON
        timer = (sw_timer__t*)list__exclude(&sw_timer__modul.cb_list, LIST__HEAD);
        if(timer != NULL)
            timer->head = NULL;
        CRITICAL_SECTION_OFF
        if(timer == NULL)
             break;
        timer->cb(timer, timer->ext_data);
    }
}

/*******************************************************************************
 * Функция получения системного времени.
 ******************************************************************************/
void sw_timer__sys_time_get(sw_timer__sys_time_t* time) {
    hw_timer__sys_time_get(time);
}

/*******************************************************************************
 * Функция суммирования времени
 ******************************************************************************/
sw_timer__sys_time_t sw_timer__time_plus(sw_timer__sys_time_t time, u32 ms) {
    ms += time.ms;
    time.timestamp += ms / 1000;
    time.ms = ms % 1000;
    return time;
}

/*******************************************************************************
 * Функция сравнения времени
 ******************************************************************************/
i32 sw_timer__time_compare(sw_timer__sys_time_t time1, sw_timer__sys_time_t time2) {
    if(time1.timestamp < time2.timestamp) return -1;
    if(time1.timestamp > time2.timestamp) return 1;
    if(time1.ms < time2.ms) return -1;
    if(time1.ms > time2.ms) return 1;
    return 0;
}
