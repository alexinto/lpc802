/*******************************************************************************
 * @file bootloader.c
 * @brief Модуль обновления прошивки.
 * @author a.tushentsov
 ******************************************************************************/
#include "target.h"
#include "System/framework.h"
#include "System/crc.h"
#include "System/bootloader.h"

#ifndef BOOTLOADER__BUFF_SIZE
#define BOOTLOADER__BUFF_SIZE 128     // Размер буфера для выполнения операций проверки целостности приложения.
#endif

typedef enum {
    BOOTLOADER__FSM_IDLE        = 0,  // Свободное состояние. Автомат готов к приему команд.
    BOOTLOADER__FSM_HDR_RD      = 1,  // Чтение хэдера основного приложения.
    BOOTLOADER__FSM_UPD_HDR_RD  = 2,  // Чтение хэдера резервного приложения.
    BOOTLOADER__FSM_CHECK_APP   = 3,  // Проверка целостности основного приложения.
    BOOTLOADER__FSM_CHECK_UPD   = 4,  // Проверка целостности резервного приложения.
}bootloader__fsm_e;

typedef struct {
    flash__exec_t flash_func;            // Функция работы с флеш-памятью.
    bootloader__cb_t cb;                 // Пользовательский коллбэк.
    u8* fw_addr;                         // Начальный адрес размещения основной прошивки.
    void* ext_data;                      // Указатель на пользовательские данные.
    u32 hdr_offset;                      // Смещение хэдера относительно адреса прошивки, байт.
    u8 flash_id[2];                      // Массив идетификаторов флеш-памяти. 0- основное приложение, 1- резервное.
    u8 init;                             // Флаг инициализации. Выставляется при успешном чтении хэдеров.
}bootloader__info_t;

static u8 boot_buff[BOOTLOADER__BUFF_SIZE]; // Буфер для работы с флеш-памятью.
static bootloader__hdr_t hdr;               // Хэдер основного приложения.
static bootloader__info_t boot_data;        // Данные модуля.
static framework__sub_t sub;                // Подписчик на выполнение cout.
static u8 module_init;                      // Флаг инициализации модуля (выставляется 1 раз при запуске).

static void bootloader__cout();

/*******************************************************************************
 * Функция инициализации.
 ******************************************************************************/
events__e bootloader__init(u8 flash_id, u8* fw_addr, u32 hdr_offset, flash__exec_t flash_func, bootloader__cb_t cb, void* ext_data) {
    if ((cb) || (flash_func == NULL))                      // todo Добавить коллбэк
        return EVENT__PARAM_NA;
    bootloader__info_t* d = &boot_data;
    if (!module_init) {
        module_init = 1;
        framework__cout_subscribe(&sub, bootloader__cout);
    }
    d->flash_id[0] = d->flash_id[1] = flash_id;
    d->fw_addr = fw_addr;
    d->hdr_offset = hdr_offset;
    d->ext_data = ext_data;
    d->cb = cb;
    d->flash_func = flash_func;
    events__e res = d->flash_func(flash_id, FLASH__OP_READ, (i32)(fw_addr + hdr_offset), (u8*)&hdr, sizeof(bootloader__hdr_t), NULL, NULL);
    if (res == EVENT__OK) {
        if ((crc__32b((u8*)&hdr, sizeof(bootloader__hdr_t)))  && (crc__32b((u8*)&hdr, sizeof(bootloader__hdr_t) - 4) != hdr.crc))   // 4- размер crc32b
            res = EVENT__CRC_ERROR;
        else
            d->init = 1;
    }
    return res;
}

/*******************************************************************************
 * Функция назначения идентификатора флеш-памяти для размещения резервного приложения.
 ******************************************************************************/
events__e bootloader__add_flash_upd(u8 flash_id) {
    bootloader__info_t* d = &boot_data;
    d->flash_id[1] = flash_id;     // 1 - Идентицикатор флеш для резервного образа
    return EVENT__OK;
}

/*******************************************************************************
 * Функция получения текущего загруженного приложения.
 ******************************************************************************/
bootloader__cmd_e bootloader__cmd_get() {
    return BOOTLOADER_CMD_NONE;
}

/*******************************************************************************
 * Функция проверки crc32.
 ******************************************************************************/
events__e bootloader__check(bootloader__fw_type_e type) {
    bootloader__info_t* d = &boot_data;
    if (!d->init)
        return EVENT__DEINIT;
    bootloader__hdr_t upd_hdr;
    u8 flash_id = d->flash_id[0];
    u32 app_addr = (u32)(d->fw_addr + hdr.boot_size);
    u32 max_addr = (u32)(d->fw_addr + hdr.fw_size);
    u32 crc_check = CRC__32b_THREAD_START_CONST;
    u32 crc32;
    events__e res = EVENT__OK;
    switch(type) {
        case BOOTLOADER__FW_TYPE_UPD:
            flash_id = d->flash_id[1];
            res = d->flash_func(flash_id, FLASH__OP_READ, hdr.upd_addr + d->hdr_offset, (u8*)&upd_hdr, sizeof(bootloader__hdr_t), NULL, NULL);
            if ((res == EVENT__OK) && ((crc__32b((u8*)&upd_hdr, sizeof(bootloader__hdr_t)))  && (crc__32b((u8*)&upd_hdr, sizeof(bootloader__hdr_t) - 4) != hdr.crc)))   // 4- размер crc32b
                res = EVENT__CRC_ERROR;
            app_addr = hdr.upd_addr + upd_hdr.boot_size;
            max_addr = (i32)(hdr.upd_addr + upd_hdr.fw_size);
            break;
        case BOOTLOADER__FW_TYPE_APP:
        default:
            break;
    }
    if (max_addr < 4)
        res = EVENT__ERROR;
    max_addr -= 4;
    i32 cur_len = (max_addr - app_addr)  < BOOTLOADER__BUFF_SIZE ? (max_addr - app_addr) : BOOTLOADER__BUFF_SIZE;
    while((app_addr < max_addr) && (res == EVENT__OK)) {
        if ((app_addr + cur_len) > max_addr)
            cur_len = max_addr - app_addr;
        res = d->flash_func(flash_id, FLASH__OP_READ, app_addr, boot_buff, cur_len, NULL, NULL);
        app_addr += cur_len;
        crc_check = crc__32b_thread(boot_buff, cur_len, crc_check);
    }
    crc32 = crc_check;
    if (res == EVENT__OK) {
        res = d->flash_func(flash_id, FLASH__OP_READ, app_addr, boot_buff, 4, NULL, NULL);  // размер crc__32b
        crc_check = crc__32b_thread(boot_buff, 4, crc_check);
    }
    if ((res == EVENT__OK) && ((crc_check) && (crc32 != *(u32*)boot_buff)))
        return EVENT__CRC_ERROR;
    return res;
}

/*******************************************************************************
 * Функция возвращает указатель на хэдер бутлоадера.
 ******************************************************************************/
bootloader__hdr_t* bootloader__get_hdr() {
    bootloader__info_t* d = &boot_data;
    if (!d->init)
        return NULL;
    return &hdr;
}

/*******************************************************************************
 * Обработчки внутреннего состояния модуля.
 ******************************************************************************/
events__e bootloader__app_upd() {
    bootloader__info_t* d = &boot_data;
    if (!d->init)
        return EVENT__DEINIT;
    bootloader__hdr_t upd_hdr;
    u32 app_addr = (u32)(d->fw_addr + hdr.boot_size);
    u32 upd_addr = hdr.upd_addr + hdr.boot_size;
    events__e res = d->flash_func(d->flash_id[1], FLASH__OP_READ, hdr.upd_addr + d->hdr_offset, (u8*)&upd_hdr, sizeof(bootloader__hdr_t), NULL, NULL);
    int app_len = upd_hdr.fw_size - upd_hdr.boot_size;
    if ((res != EVENT__OK) || (app_len > (hdr.fw_max - hdr.boot_size)))
        return EVENT__ERROR;
    if (res == EVENT__OK)
        res = d->flash_func(d->flash_id[0], FLASH__OP_ERASE, app_addr, NULL, hdr.fw_max - hdr.boot_size, NULL, NULL);
    if (res != EVENT__OK)
        return res;
    i32 cur_len = BOOTLOADER__BUFF_SIZE;
    do {
        if (cur_len > app_len)
            cur_len = app_len;
        res = d->flash_func(d->flash_id[1], FLASH__OP_READ, upd_addr, boot_buff, cur_len, NULL, NULL);
        if (res == EVENT__OK)
            res = d->flash_func(d->flash_id[0], FLASH__OP_WRITE, app_addr, boot_buff, cur_len, NULL, NULL);
        app_addr += cur_len;
        upd_addr += cur_len;
        app_len -= cur_len;
    }while(app_len && (res == EVENT__OK));
    return res;
}


/*******************************************************************************
 * Обработчки внутреннего состояния модуля.
 ******************************************************************************/
static void bootloader__cout() {

}