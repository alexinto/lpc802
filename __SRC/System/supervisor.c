﻿/***************************************************************************//**
 * @file supervisor.c.
 * @brief Драйвер SUPERVISOR.
 * @author a.tushentsov.
 ******************************************************************************/

#include "System/fstandard.h"
#include <stddef.h>
#include "target.h"
#include "System/framework.h"
#include "System/fstandard.h"
#include "System/supervisor.h"
#include "System/hw_supervisor.h"
#include "System/sw_timer.h"


#define SUPERVISOR__IDLE_COUNTER_MAX    2    // Лимит счетчика прокрутки суперцикла перед тем, как уйти в сон (нужно чтобы прокручивать все программные модули, прежде чем точно уходить в сон).

typedef enum {
    SUPERVISOR__MODE_IDLE,
    SUPERVISOR__MODE_ACTIVE,
} supervisor__mode_e;

// Внутрення структура модуля.
typedef struct {

#ifdef SUPERVISOR__EVT_HANDLER_USE
    supervisor__event_handler_t event_handler;                  // Обработчик событий (только при SUPERVISOR__EVT_HANDLER_USE)
#endif
    u8 init;                                                    // Состояние инициализации модуля.
    supervisor__mode_e mode;                                    // Режим работы CPU.
    //списки подписки на idle
    supervisor__idle_sub_t idle_list;                           // Указатель на список подписчиков, блокирующих переход в режим idle.
    //списки подписки на clk
    list__item_t any_clk_list;                                  // Указатель на список подписчиков, которые могут работать на любой частоте.
    list__item_t low_clk_list;                                  // Указатель на список подписчиков, которым необходимо работать на низкой частоте.
    list__item_t high_clk_list;                                 // Указатель на список подписчиков, которым необходимо работать на высокой частоте.

    i32 idle_counter;                                           // Счетчик прокрутки суперцикла перед тем, как уйти в сон.

} supervisor__struct_t;

static supervisor__struct_t supervisor;
static framework__sub_t cout_sub;

static void supervisor__cout(void);

/*******************************************************************************
 *  Функция инициализации модуля супервизора.
 ******************************************************************************/
supervisor__event_e supervisor__init(void) {
    hw_supervisor__init();
    hw_supervisor__clk_set(SUPERVISOR__CLK_HIGH, 0);
    supervisor.mode = SUPERVISOR__MODE_ACTIVE;
    list__init(&supervisor.idle_list);
    list__init(&supervisor.any_clk_list);
    list__init(&supervisor.low_clk_list);
    list__init(&supervisor.high_clk_list);
    if (!supervisor.init) {
        supervisor.init = 1;
        framework__cout_subscribe(&cout_sub, supervisor__cout);
    }
    return SUPERVISOR__EVENT_OK;
}

#ifdef SUPERVISOR__ON
/*******************************************************************************
 * Функция подписки на активный режим энергосбережения.
 * До тех пор, пока есть хотя бы один подписчик, контроллер не уйдет в режим повышенного энергосбережения.
 * Повторный вызов функции с тем же идентификатором подписчика не приводит к созданию нового подписчика.
 ******************************************************************************/
supervisor__event_e supervisor__idle_lock(supervisor__idle_sub_t *idle_sub) {
    if (idle_sub == NULL)
        return SUPERVISOR__EVENT_ERROR;
    if (idle_sub->head == &supervisor.idle_list) {
        return SUPERVISOR__EVENT_OK;
    }
    CRITICAL_SECTION_ON
        list__head_include(&supervisor.idle_list, idle_sub);
    CRITICAL_SECTION_OFF
    idle_sub->head = &supervisor.idle_list;

    return SUPERVISOR__EVENT_OK;
}

/*******************************************************************************
 * Функция отписки от активного режима энергосбережения.
 ******************************************************************************/
supervisor__event_e supervisor__idle_unlock(supervisor__idle_sub_t *idle_sub) {
    if (idle_sub == NULL)
        return SUPERVISOR__EVENT_ERROR;
    supervisor__event_e res = SUPERVISOR__EVENT_OK;
    CRITICAL_SECTION_ON
        if(idle_sub->head == &supervisor.idle_list)
        {
            list__exclude_me(idle_sub);
            idle_sub->head = idle_sub;
        }
    CRITICAL_SECTION_OFF

    return res;
}

/*******************************************************************************
 * Функция обработки подписчиков на IDLE.
 * Учтены особенности следующих контроллеров: STM32 и MSP430.
 * Цикл ухода в сон для STM32 обязателен, т.к. микроконтроллер может просыпаться в результате действия внутренних сервисных функций (например при отладке).
 * Для STM32 используется ньюанс- команда __WFI() будет пропущена, если выставлены флаги обработки прерываний (даже при выключенных прерываниях).
 * Для этого уход в режим сна осуществляется при установленном приоритете обработки прерываний = max(0x80).
 * Установка 0 приоритета осуществляется только после выхода из режима сна. Цикл обработки cout несколько раз создан,
 * т.к. существует некоторый интервал между последней отписавшейся от режима activ и первой подписавшейся на него функциями.
 * Для MSP430 существуют дополнительные проверки в прерываниях листов подписчиков и если подписчик появился во время ухода в сон,
 * то активируется таймер, который разбудит MSP дополнительно.
 * Если обобщить, то STM при отключенных прерываниях проверяет список подписчиков на пустоту и засыпает (при пустом списке),
 * а при возникновении прерывания выходит из сна, разрешает прерывание и попадает в обработчик ISR.
 * MSP430 засыпать с отключенными прерываниями не умеет. Поэтому она перед уходом в сон разрешит прерывание в hw_supervisor__idle_on()
 * и даст команду на уход в сон.
 ******************************************************************************/
static void supervisor__cout(void) {
    if (!supervisor.init) {
        return;
    }
    while(1) {
        CRITICAL_SECTION_ON
            if (list__is_empty(&supervisor.idle_list)) {
                if (supervisor.idle_counter >= SUPERVISOR__IDLE_COUNTER_MAX) {
                    supervisor.mode = SUPERVISOR__MODE_IDLE;
#ifdef WDT_AUTO_RESET_SLEEP
                    wdt__reset();
#endif
                    hw_supervisor__idle_on();   // Уход в сон. Тут выполнение программы остановится до прерывания.
                 }
                else
                    supervisor.idle_counter++;
            }
            else {
                if (supervisor.mode == SUPERVISOR__MODE_IDLE) {
                    hw_supervisor__idle_off();
#ifdef SUPERVISOR__EVT_HANDLER_USE
                    if (supervisor.event_handler) supervisor.event_handler(SUPERVISOR__EVENT_WAKE);
#endif
                }
                supervisor.idle_counter = 0;
            }
        CRITICAL_SECTION_OFF

        if ((supervisor.idle_counter < SUPERVISOR__IDLE_COUNTER_MAX) || (supervisor.mode == SUPERVISOR__MODE_ACTIVE))
            break;
    }
    supervisor.mode = SUPERVISOR__MODE_ACTIVE;
}
#else
    static void supervisor__cout(void) {}
#endif


/*******************************************************************************
 * Функция перезагрузки приложения.
 ******************************************************************************/
void supervisor__apl_reset(void) {
    hw_supervisor__apl_reset();
}


/*******************************************************************************
 * Функция получения текущей частоты тактирования процессора.
 ******************************************************************************/
u32 supervisor__cpu_clk_get(void) {
    return hw_supervisor__cpu_clk_get();
}

/*******************************************************************************
 * Функция получения текущей частоты тактирования периферии.
 ******************************************************************************/
u32 supervisor__periph_clk_get(void) {
    return hw_supervisor__periph_clk_get();
}





