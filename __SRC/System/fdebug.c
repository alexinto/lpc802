﻿/***************************************************************************//**
 * @file fdebug.c
 * @brief Модуль для вывода отладочной информации.
 * @author a.tushentsov
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "System/fdebug.h"
#include "System/framework.h"
#include "DRV/uart.h"

static struct {
    u8 init;
    u8 module_init;
    events__e state;
    fdebug__handler_t handler;
    framework__sub_t cout_sub;
    fdebug__struct_t settings;
}data = {.state = EVENT__CONTINUE, .settings= {1, FDEBUG__UART, 0xC0A8011E, 0xDEBA, 0}};

#ifdef FDEBUG__ON
#ifndef FDEBUG__ITF_ID
    #error "Must be define DEBUG__ITF_ID in target.h!!!"
#endif
#ifndef FDEBUG__BUFF_SIZE
    #define FDEBUG__BUFF_SIZE 255
#endif

char fdebug__buff[FDEBUG__BUFF_SIZE];
static uart__sub_t fdebug__uart_sub;

static void fdebug__uart_send(char* buffer);
static void fdebug__eth_send(char* buffer);
static void fdebug__uart_hd(struct uart__sub_t* sub, events__e event, void* ext_data);
#endif
static void fdebug__cout();

/*******************************************************************************
 * Функция инициализации.
 ******************************************************************************/
void fdebug__init(fdebug__struct_t* settings, fdebug__handler_t handler) {
    data.handler = handler;
    if (!data.init) {
        framework__cout_subscribe(&data.cout_sub, fdebug__cout);
        data.init = 1;
    }
#ifdef FDEBUG__ON
    if (settings)
        memcpy(&data.settings, settings, sizeof(fdebug__struct_t));
    if (!data.settings.status) {
        fdebug__uart_hd(&fdebug__uart_sub, EVENT__CLOSE, NULL);
        return;
    }
    switch(data.settings.itf) {
        case FDEBUG__CONSOLE:
#ifdef FDEBUG__CONSOLE_ID
            uart__open(&fdebug__uart_sub, FDEBUG__CONSOLE_ID, data.settings.settings, fdebug__uart_hd, NULL);
            break;
#endif
        case FDEBUG__UART:
            uart__open(&fdebug__uart_sub, FDEBUG__ITF_ID, data.settings.settings, fdebug__uart_hd, NULL);
            break;
        case FDEBUG__ETH:
            data.module_init = 1;
            fdebug__uart_hd(&fdebug__uart_sub, EVENT__CLOSE, NULL); // todo Добавить Ethernet!!!
            break;
    }
#else
    data.state = EVENT__INIT;
#endif
}

/*******************************************************************************
 * Функция деинициализации.
 ******************************************************************************/
void fdebug__deinit() {
#ifdef FDEBUG__ON
    switch(data.settings.itf) {
        case FDEBUG__UART:
            uart__close(&fdebug__uart_sub);
            break;
        case FDEBUG__ETH:
            data.module_init = 0;
            break;
    }
#else
    data.state = EVENT__DEINIT;
#endif
}

static void fdebug__cout() {
#ifndef FDEBUG__ON
    events__e cur_state;
    if (data.state != EVENT__CONTINUE) {
        cur_state = data.state;
        data.state = EVENT__CONTINUE;
        if (data.handler)
            data.handler(cur_state, &data.settings, NULL, 0);
    }
#endif
}

#ifdef FDEBUG__ON
/*******************************************************************************
 * Функция проверки уровня дебага.
 ******************************************************************************/
i32 fdebug__check(u8 level) {
    if (!data.module_init)
        return 0;
    return (data.settings.status & (1 << level)) ? 1 : 0;
}

/*******************************************************************************
 * Функция отправки отладочной информации.
 ******************************************************************************/
void fdebug__print() {
    switch(data.settings.itf) {
        case FDEBUG__CONSOLE:
        case FDEBUG__UART:
            fdebug__uart_send(fdebug__buff);
            break;
        case FDEBUG__ETH:
            fdebug__eth_send(fdebug__buff);
            break;
    }
}

/*******************************************************************************
 * Функция отправки отладочной информации по интерфейсу UART
 ******************************************************************************/
static void fdebug__uart_send(char* buffer) {
    uart__tx(&fdebug__uart_sub, (u8*)buffer, strlen(buffer) + 1, NULL, NULL);
}

/*******************************************************************************
 * Функция отправки отладочной информации по интерфейсу Ethernet
 ******************************************************************************/
static void fdebug__eth_send(char* buffer) {
    if (data.handler)
        data.handler(EVENT__EXECUTE, &data.settings, buffer, strlen(buffer) + 1);
}

/*******************************************************************************
 * Обработчик событий UART-а.
 ******************************************************************************/
static void fdebug__uart_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
    switch(event) {
        case EVENT__OPEN:
            data.module_init = 1;
            if (data.handler)
                data.handler(EVENT__INIT, &data.settings, NULL, 0);
            break;
        case EVENT__CLOSE:
            data.module_init = 0;
            if (data.handler)
                data.handler(EVENT__DEINIT, &data.settings, NULL, 0);
            break;
        default:
            break;
    }
}

#endif
