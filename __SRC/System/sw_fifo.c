﻿/*******************************************************************************
 * @file sw_fifo.c
 * @brief Утилита для работы с FIFO буфером.
 * @author a.tushentsov
 ******************************************************************************/
#include "System/fstandard.h"
#include <string.h>
#include "target.h"
#include "System/sw_fifo.h"
#include "System/resource.h"

#define SW_FIFO__RESERVE 1        // 1 байт для определения переполнения буфера
static u32 sw_fifo__offset;

#if (!defined CRITICAL_SECTION_ON) || (!defined CRITICAL_SECTION_OFF)
    #error "Must be define CRITICAL_SECTION_ON and CRITICAL_SECTION_OFF in target.h!"
#endif

/*******************************************************************************
 * Функция установки смещения на буфер.
 ******************************************************************************/
void sw_fifo__offset_set(u32 offset) {
    sw_fifo__offset = offset;
}

/*******************************************************************************
 * Функция инициализации FIFO буфера.
 ******************************************************************************/
i32 sw_fifo__init(fifo__struct_t *fifo, u8* buff, i32 size) {
    if ((buff) && (size > SW_FIFO__RESERVE)) {
        while(resource__lock(fifo) != EVENT__OK){}  // Ждем захвата ресурса
        CRITICAL_SECTION_ON                         // Атомарность при инициализации
        fifo->tx = fifo->rx = fifo->buff = buff;
        fifo->size = size;
        CRITICAL_SECTION_OFF
        resource__unlock(fifo);
        return size - SW_FIFO__RESERVE;
    }
    return 0;
}

/*******************************************************************************
 * Функция очистки FIFO буфера.
 ******************************************************************************/
i32 sw_fifo__flush(fifo__struct_t *fifo) {
    if (fifo == NULL)
        return 0;
    while(resource__lock(fifo) != EVENT__OK){}      // Ждем захвата ресурса
    CRITICAL_SECTION_ON                             // Атомарность при очистке буфера
    fifo->tx = fifo->rx = fifo->buff;
    CRITICAL_SECTION_OFF
    resource__unlock(fifo);
    return fifo->size - SW_FIFO__RESERVE;
}
/*******************************************************************************
 * Размер свободного места
 ******************************************************************************/
i32 sw_fifo__free(fifo__struct_t *fifo) {
    if (fifo == NULL)
        return 0;
    return fifo->size - SW_FIFO__RESERVE - sw_fifo__count(fifo);
}
/*******************************************************************************
 * Размер полезных данных
 ******************************************************************************/
i32 sw_fifo__count(fifo__struct_t *fifo) {
    if (fifo == NULL)
        return 0;
    u8* rx_buff = fifo->rx;      // Обеспечивается сохранение указателей при условном переходе
    u8* tx_buff = fifo->tx;
    return rx_buff > tx_buff ? (tx_buff + fifo->size - rx_buff) : (tx_buff - rx_buff);
}

/*******************************************************************************
 * Функция добавления данных в FIFO.
 ******************************************************************************/
i32 sw_fifo__put(fifo__struct_t *fifo, u8 *data, i32 len) {
    if ((fifo == NULL) || (data == NULL) || (len == 0))
        return 0;
    u8* rx_buff = fifo->rx;    // Обеспечивается сохранение указателей при условном переходе
    u8* tx_buff = fifo->tx;
    i32 fifo_count = rx_buff > tx_buff ? (tx_buff + fifo->size - rx_buff) : (tx_buff - rx_buff);   // Макрос для получения полезных данных в fifo.
    if (len > (fifo->size - fifo_count - SW_FIFO__RESERVE)) // Количество свободных байт данных в буфере.
        return 0;
    i32 chain_len;
    if (rx_buff > tx_buff)
        chain_len = rx_buff - tx_buff;
    else
        chain_len = fifo->buff + fifo->size - tx_buff;
    if (chain_len >= len) {
        memcpy(tx_buff + sw_fifo__offset, data, len);
        tx_buff += len;
    }
    else {
        memcpy(tx_buff + sw_fifo__offset, data, chain_len);
        memcpy(fifo->buff + sw_fifo__offset, &data[chain_len], len - chain_len);
        tx_buff = &fifo->buff[len - chain_len];
    }
    if (tx_buff >= fifo->buff + fifo->size)
        tx_buff = fifo->buff;
    fifo->tx = tx_buff;
    return len;
}

/*******************************************************************************
 * Функция чтения данных из FIFO.
 ******************************************************************************/
i32 sw_fifo__get(fifo__struct_t *fifo, u8 *data, i32 len) {
    if ((fifo == NULL) || (data == NULL) || (len == 0))
        return 0;
    u8* rx_buff = fifo->rx;     // Обеспечивается сохранение указателей при условном переходе
    u8* tx_buff = fifo->tx;
    i32 fifo_count = rx_buff > tx_buff ? (tx_buff + fifo->size - rx_buff) : (tx_buff - rx_buff);   // Макрос для получения полезных данных в fifo.
    if (len > fifo_count)            // выдадим столько сколько есть
        len = fifo_count;
    i32 chain_len;
    if (len) {
        if (rx_buff <= tx_buff) {
            chain_len = tx_buff - rx_buff;
            if (data)
                memcpy(data, rx_buff + sw_fifo__offset, len);
            rx_buff += len;
            if (rx_buff >= fifo->buff + fifo->size)
                rx_buff = fifo->buff;
        }
        else {
            chain_len = fifo->buff + fifo->size - rx_buff;
            if (len > chain_len) {
                if (data) {
                    memcpy(data, rx_buff + sw_fifo__offset, chain_len);
                    memcpy(&data[chain_len], fifo->buff + sw_fifo__offset, len - chain_len);
                }
                rx_buff = fifo->buff + len - chain_len;
            }
            else {
                if (data)
                    memcpy(data, rx_buff + sw_fifo__offset, len);
                rx_buff += len;
            }
        }
    }
    if (rx_buff >= fifo->buff + fifo->size)
        rx_buff = fifo->buff;
    fifo->rx = rx_buff;
    return len;
}
