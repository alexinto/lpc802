﻿/***************************************************************************//**
 * @file framework.c.
 * @brief Модуль подписки на framework__cout().
 * @author a.tushentsov.
 ******************************************************************************/
#include "System/list.h"
#include "System/framework.h"


// Структура заголовка списка подписчиков на вызов cout().
typedef struct {
    framework__sub_t* prev;
    framework__sub_t* next;
} framework__head_t;

static framework__head_t framework__list = {.prev = (framework__sub_t*)&framework__list,
                                            .next = (framework__sub_t*)&framework__list};

/*******************************************************************************
 * Функция подключения модулей к системному cout.
 ******************************************************************************/
void framework__cout_subscribe(framework__sub_t* item, framework__cout_t cout) {
    if (item->next == NULL) {
        item->cout = cout;
        list__tail_include(&framework__list, item);
    }
};

/*******************************************************************************
 * Функция обработки текущего состояния модуля frameworkа (должна вызываться в суперцикле).
 ******************************************************************************/
void framework__cout(void) {
    framework__sub_t* before = NULL;
    while( (before = list__foreach(&framework__list, before, LIST__HEAD)) != NULL)
        before->cout();
}
