/***************************************************************************//**
 * @file hw_i2c.�
 * @brief ������� I2C (��������� ��������� ����� ��� stm32l1).
 * @authors a.tushentsov
 ******************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include "standard.h"
#include "stm32l1xx.h"
#include "target.h"
#include "mcu.h"
#include "hw_i2c.h"
#include "i2c.h"
#include "supervisor.h"
#ifdef BB_I2C__1
    #include "bb_i2c.h"
#endif

#ifndef HW_I2C__ARB_LOST_DELAY
    #define HW_I2C__ARB_LOST_DELAY 2     //����-��� ������ ��������� ��� ������������ �����- ������������������ 1.. 255. 1= 1 ����������� cout().
#endif

#if (HW_I2C__ARB_LOST_DELAY == 0) || (HW_I2C__ARB_LOST_DELAY == 255)
    #error "HW_I2C__ARB_LOST_DELAY must be > 0 and < 255"
#endif

#define I2C_MODE_READ	                1
#define I2C_MODE_WRITE	                0
#define I2C_ADDRESS_7BITS(addr, mode)	((addr << 1) | mode)
#define I2C_ADDRESS_10BITS(addr, mode)	(((addr << 1) | mode) & 0xFF)
#define I2C_HEADER_10BITS(addr)	        (0xF1 | (addr & 0x03))

typedef struct {
     uint8_t SB: 1;
     uint8_t ADDR: 1;
     uint8_t BTF: 1;
     uint8_t ADD10: 1;
     uint8_t STOPF: 1;
     uint8_t RESEVED: 1;
     uint8_t RxNE: 1;
     uint8_t TxE: 1;
     uint8_t BERR: 1;
     uint8_t ARLO: 1;
     uint8_t AF: 1;
     uint8_t OVR: 1;
     uint8_t PECERR: 1;
     uint8_t RESEVED1: 1;
     uint8_t TIMEOUT: 1;
     uint8_t SMBALERT: 1;
 } hw_i2c__sr1_t;

typedef struct {
     uint8_t MSL: 1;
     uint8_t BUSY: 1;
     uint8_t TRA: 1;
     uint8_t RESEVED2: 1;
     uint8_t GENCALL: 1;
     uint8_t SMBDEFAULT: 1;
     uint8_t SMBHOST: 1;
     uint8_t DUALF: 1;
     uint8_t PEC: 1;
 } hw_i2c__sr2_t;

 typedef union {
     uint16_t sr2_u16;
     hw_i2c__sr2_t sr2;
 } hw_i2c__sr2_u;

  typedef union {
     uint16_t sr1_u16;
     hw_i2c__sr1_t sr1;
 } hw_i2c__sr1_u;

// ���� ��������.
typedef enum {
    HW_I2C__OPER_NONE,
    HW_I2C__OPER_TX,
    HW_I2C__OPER_RX,
} hw_i2c__oper_e;

// ��������� ���������� I2C.
typedef struct {
    int own_addr;                                       // ����������� �����.
    int addr;                                           // ����� ���������.
    uint8_t *buff;                                      // ��������� �� ����� � ����������������� �������.
    int len;                                            // ������ ������ � ����������������� �������.
    hw_i2c__cb_t isr_cb;                                // ��������� �� ���������������� ������� (�� ����������).
    int bytes_count;                                    // ������� ������.
    int i2c_delay;                                      // �������� ��� ����� ������������������ (����������� ������� 100���).
    I2C_TypeDef* regs;
    hw_i2c__oper_e cur_oper;                            // ������� ����������� ��������.
    uint16_t settings;                                  // ������� ���������.
} hw_i2c__itf_t;



// ����������� ���������� ������������ ���������� I2C � �������.
#if defined(I2C_1) && defined(I2C_2)
    #define HW_I2C__COUNT  2
#elif !defined(I2C_1) && !defined(I2C_2)
    #define HW_I2C__COUNT  0
#else
    #define HW_I2C__COUNT  1
#endif

// ������ ��� ��������� ��������� �� ��������� ����������� I2C �� ID.
#if HW_I2C__COUNT == 2
    #define HW_I2C__PTR_FROM_ID_GET(i2c_id)   (i2c_id) == I2C_2 ? &hw_i2c.itf[1] : &hw_i2c.itf[0]
#else
    #define HW_I2C__PTR_FROM_ID_GET(i2c_id)   &hw_i2c.itf[0]
#endif

// ������ ��� ��������� ID ����������� I2C �� ��������� �� ���������.
#if HW_I2C__COUNT == 2
    #define HW_I2C__ID_FROM_PTR_GET(i2c_ptr)   (i2c_ptr) == &hw_i2c.itf[0] ? I2C_1 : I2C_2
#elif defined (I2C_B0)
    #define HW_I2C__ID_FROM_PTR_GET(i2c_ptr)   I2C_1
#else
    #define HW_I2C__ID_FROM_PTR_GET(i2c_ptr)   I2C_2
#endif

#if HW_I2C__COUNT != 0

// ��������� �������� I2C.
typedef struct {
    hw_i2c__itf_t itf[HW_I2C__COUNT];
} hw_i2c__t;

static hw_i2c__t hw_i2c = {
#if HW_I2C__COUNT == 2
    .itf[0].regs = I2C1,
    .itf[1].regs = I2C2,
#elif defined (I2C_B0)
    .itf[0].regs = I2C1,
#else
    .itf[0].regs = I2C2,
#endif
};

static events__e hw_i2c__itf1_gpio_en(void);
static void hw_i2c__itf1_gpio_dis(void);
static events__e hw_i2c__itf2_gpio_en(void);
static void hw_i2c__itf2_gpio_dis(void);

static events__e hw_i2c__init_continue(hw_i2c__itf_t *i2c);
static void hw_i2c__deinit_short(hw_i2c__itf_t *i2c);
void hw_i2c__I2C_EV_IRQHandler(int i2c_id);
void hw_i2c__I2C_ER_IRQHandler(int i2c_id);

/*******************************************************************************
 * ������� ������������� ����������� I2C �� ������ � ���������� �����������, � ��� �� �������� �������-����������� �������.
 *        Ex: while(hw_i2c__init(I2C_2, I2C__FREQ_100KHZ | I2C__ADDR_7BIT, 0, NULL) != 0){};
 *        �������� ������� ������������� �� ������� ������������ ���� � ������ ������������� I2C.
 ******************************************************************************/
events__e hw_i2c__init(int i2c_id, uint16_t settings, int own_addr, hw_i2c__cb_t event_handler) {
    hw_i2c__deinit(i2c_id);
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    i2c->settings = settings;
    i2c->own_addr = own_addr;
    i2c->cur_oper = HW_I2C__OPER_NONE;
// �������������� ����� I2C � ��������� ���������� ����������
    switch(i2c_id) {
        case I2C_1:
            if (hw_i2c__itf1_gpio_en() != EVENT__OK)
                return EVENT__ERROR;
            NVIC_EnableIRQ(I2C1_EV_IRQn);
            NVIC_EnableIRQ(I2C1_ER_IRQn);
            break;
        case I2C_2:
            if (hw_i2c__itf2_gpio_en() != EVENT__OK)
                return EVENT__ERROR;
            NVIC_EnableIRQ(I2C2_EV_IRQn);
            NVIC_EnableIRQ(I2C2_ER_IRQn);
            break;
        default:
            return EVENT__PARAM_NA;
    }
    return hw_i2c__init_continue(i2c);
}

/*******************************************************************************
 * ������� todo
 ******************************************************************************/
static events__e hw_i2c__init_continue(hw_i2c__itf_t *i2c) {
    I2C_TypeDef *I2C = i2c->regs;
    hw_i2c__deinit_short(i2c);
//��������� I2C �� ������ ������
    I2C->CR1 &=~ I2C_CR1_PE;
//SoftWare Restart
    I2C->CR1 |= I2C_CR1_SWRST;
    I2C->CR1 &=~ I2C_CR1_SWRST;
//��������� Peripheral clock frequency (2..50���)
    I2C->CR2 &=~ I2C_CR2_FREQ; //���������� ������� �������
    I2C->CCR &=~ I2C_CCR_DUTY; //�������� ��� 400��� �� ����������� 2, ������ � ���.
    I2C->CR2 |= I2C_CR2_FREQ_5;
    I2C->CCR &=~ I2C_CCR_FS;
    I2C->CCR |= (I2C_CCR_CCR & ((uint16_t)0x00A0));
    I2C->TRISE = ((uint16_t)0x0021);

    I2C->OAR1 &=~ I2C_OAR1_ADDMODE;
    I2C->OAR1 |= 0x4000 | ((i2c->own_addr<<1) & (I2C_OAR1_ADD1_7));
    //�������� ���������
    I2C->CR1 |= I2C_CR1_PE;
    I2C->CR1 &= ~I2C_CR1_PE;

    I2C->CR1 &= ~(I2C_CR1_PE | I2C_CR1_NOSTRETCH);
    I2C->CR1 |= I2C_CR1_PE;
    I2C->CR1 |= I2C_CR1_ACK;
    /*I2C � stm32l1 ���������� ��������� � slave mode,
    ��� �������� � master mode ���������� �����
    ������������� ����� ��������� ��������� start condition */

    I2C->CR1 |= I2C_CR1_SMBUS;      // ��� ��������� SCL � ������� 25 �� ����� ������ ERROR.

    if ((I2C->SR2 & I2C_SR2_BUSY) == I2C_SR2_BUSY)
        return EVENT__ERROR;
    i2c->i2c_delay = 0xff;
    return EVENT__OK;
}

/*******************************************************************************
 * ������� ����������� ����������� ���������� I2C �������� � ������������ ��������.
 ******************************************************************************/
events__e hw_i2c__deinit(int i2c_id) {
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    hw_i2c__deinit_short(i2c);
    switch(i2c_id) {
        case I2C_1:
            hw_i2c__itf1_gpio_dis();
            NVIC_DisableIRQ(I2C1_EV_IRQn);
            NVIC_DisableIRQ(I2C1_ER_IRQn);
            break;
       case I2C_2:
            hw_i2c__itf2_gpio_dis();
            NVIC_DisableIRQ(I2C2_EV_IRQn);
            NVIC_DisableIRQ(I2C2_ER_IRQn);
            break;
       default:
            return  EVENT__PARAM_NA;
    }
    return EVENT__OK;
}

/*******************************************************************************
 * ������� ����������� ����������� ���������� I2C �������� � ������������ ��������.
 ******************************************************************************/
static void hw_i2c__deinit_short(hw_i2c__itf_t *i2c) {
    I2C_TypeDef *I2C = i2c->regs;
    I2C->CR1 = I2C_CR1_SWRST;
    i2c->i2c_delay = 0xff;
}

/*******************************************************************************
 * ������� ������ ������ �� ����������� I2C.
 ******************************************************************************/
events__e hw_i2c__rx(int i2c_id, int addr, uint8_t *buff, int len, hw_i2c__cb_t isr_cb) {
    if (buff == NULL || len == 0)
        return EVENT__PARAM_NA;
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);

    if (i2c->cur_oper != HW_I2C__OPER_NONE)
        return EVENT__BUSY;
    i2c->cur_oper = HW_I2C__OPER_RX;
    hw_i2c__init_continue(i2c);

    i2c->buff = buff;
    i2c->len = len;
    i2c->isr_cb = isr_cb;
    i2c->addr = addr;
    i2c->i2c_delay = 0x00;
    I2C_TypeDef *I2C = i2c->regs;
    // ������������ ������� START
    I2C->CR1 |= I2C_CR1_ACK;
    I2C->CR1 &=~ I2C_CR1_POS;
    //��������� ����������
    I2C->CR2 |= I2C_CR2_ITEVTEN | I2C_CR2_ITERREN | I2C_CR2_ITBUFEN;
    I2C->CR1 |= I2C_CR1_START;

    return EVENT__OK;
}

/*******************************************************************************
 * ������� ������ ������ �� ����������� I2C.
 ******************************************************************************/
events__e hw_i2c__tx(int i2c_id, int addr, uint8_t *buff, int len, hw_i2c__cb_t isr_cb) {
    if (buff == NULL || len == 0)
        return EVENT__PARAM_NA;
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);

    if (i2c->cur_oper != HW_I2C__OPER_NONE)
        return EVENT__BUSY;
    i2c->cur_oper = HW_I2C__OPER_TX;
    hw_i2c__init_continue(i2c);

    i2c->buff = buff;
    i2c->len = len;
    i2c->isr_cb = isr_cb;
    i2c->addr = addr;
    i2c->i2c_delay = 0x00;
    I2C_TypeDef *I2C = i2c->regs;
    // ������������ ������� START
    I2C->CR1 |= I2C_CR1_ACK;
    I2C->CR1 &=~ I2C_CR1_POS;
    //��������� ����������
    I2C->CR2 |= I2C_CR2_ITEVTEN | I2C_CR2_ITERREN | I2C_CR2_ITBUFEN;
    I2C->CR1 |= I2C_CR1_START;
    return EVENT__OK;
}

/*******************************************************************************
 * ������� �������� ������ ��� ��������� �������� ��������� �������� I2C.
 ******************************************************************************/
void hw_i2c__cout(void) {
    for(int i2c_id = 0; i2c_id < HW_I2C__COUNT; i2c_id++) {
        hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
        if (i2c->cur_oper != HW_I2C__OPER_NONE) {
            I2C_TypeDef *I2C = i2c->regs;
            if ((i2c->i2c_delay == HW_I2C__ARB_LOST_DELAY) && (!(I2C->SR2 & I2C_SR2_MSL))) {
                i2c->cur_oper = HW_I2C__OPER_NONE;
                i2c->isr_cb(i2c_id, EVENT__ERROR, i2c->addr, i2c->buff, i2c->bytes_count);
            }
            CRITICAL_SECTION_ON
            (i2c->i2c_delay >= HW_I2C__ARB_LOST_DELAY) ? (i2c->i2c_delay = 0xff):(i2c->i2c_delay++);
            CRITICAL_SECTION_OFF
        }
    }
}

/*******************************************************************************
 * * �������-���������� ���������� �� ������ � �������� I2C.
 ******************************************************************************/
void hw_i2c__I2C_EV_IRQHandler(int i2c_id) {
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    I2C_TypeDef *I2C = i2c->regs;

    hw_i2c__sr1_u status_reg_1;
    hw_i2c__sr2_u status_reg_2; //� �������� �������, ��� ��� ������� ������ � ������ �������� ����������� ��������� ��������������� SR1 � SR2.

    status_reg_1.sr1_u16 = I2C->SR1;
    status_reg_2.sr2_u16 = I2C->SR2;

    if (status_reg_2.sr2.MSL == 0) {
        i2c->cur_oper = HW_I2C__OPER_NONE;
        i2c->isr_cb(i2c_id, EVENT__ERROR, i2c->addr, i2c->buff, i2c->bytes_count);

        return;
    }

    if (status_reg_1.sr1.SB == 1) {
        //���� �������� 10 bit �����
        if ((I2C->OAR1 & I2C_OAR1_ADDMODE) == I2C_OAR1_ADDMODE)
            I2C->DR = I2C_HEADER_10BITS(i2c->addr);
          //���� ������ 7 bit
        else
            (i2c->cur_oper == HW_I2C__OPER_TX)?(I2C->DR = I2C_ADDRESS_7BITS(i2c->addr, I2C_MODE_WRITE)):(I2C->DR = I2C_ADDRESS_7BITS(i2c->addr, I2C_MODE_READ));
        return;
    }
    //���� �������� ������� 10 bit ������
    else if (status_reg_1.sr1.ADD10 == 1) { //todo test
        (i2c->cur_oper == HW_I2C__OPER_TX)?(I2C->DR = I2C_ADDRESS_10BITS(i2c->addr, I2C_MODE_WRITE)):(I2C->DR = I2C_ADDRESS_10BITS(i2c->addr, I2C_MODE_READ));
    }
    else if (status_reg_1.sr1.ADDR == 1) {
        i2c->bytes_count = 0;
        if (i2c->cur_oper == HW_I2C__OPER_TX) {
            i2c->bytes_count++;
            if (i2c->bytes_count <= i2c->len)
                I2C->DR = i2c->buff[i2c->bytes_count - 1];
        }
        else {
            //RX = 2
            if (i2c->len == 2) {
                I2C->CR1 &=~ I2C_CR1_ACK;
                I2C->CR1 |= I2C_CR1_POS;
            }
            //RX = 1
            else if (i2c->len == 1) {
                I2C->CR1 &=~ I2C_CR1_ACK;
                I2C->CR1 |= I2C_CR1_STOP;
            }
            //RX > 2
            else {
                I2C->CR1|= I2C_CR1_ACK;
            }
        }
    return;
    }

    else if (status_reg_1.sr1.TxE == 1){
        i2c->bytes_count++;
        if (i2c->bytes_count <= i2c->len)
            I2C->DR = i2c->buff[i2c->bytes_count - 1];
        //���� ��������� ��������� �������
        else if ((status_reg_1.sr1.BTF == 1)){
                i2c->bytes_count--;
                i2c->cur_oper = HW_I2C__OPER_NONE;
//                I2C->CR2 &=~ (I2C_CR2_ITEVTEN | I2C_CR2_ITERREN | I2C_CR2_ITBUFEN);
                I2C->CR1 |= I2C_CR1_STOP;
                i2c->isr_cb(i2c_id, EVENT__OK, i2c->addr, i2c->buff, i2c->bytes_count);
        }
            else
                I2C->CR2 &= ~I2C_CR2_ITBUFEN;
        return;
    }
    else if (status_reg_1.sr1.RxNE == 1) {
        //���� �������� >2 ������
        i2c->bytes_count++;
        if (i2c->len <= i2c->bytes_count) {
            i2c->buff[i2c->bytes_count - 1] = I2C->DR;
            i2c->cur_oper = HW_I2C__OPER_NONE;
//            I2C->CR2 &=~ (I2C_CR2_ITEVTEN | I2C_CR2_ITERREN | I2C_CR2_ITBUFEN);
            I2C->CR1 |= I2C_CR1_STOP;
            i2c->isr_cb(i2c_id, EVENT__OK, i2c->addr, i2c->buff, i2c->bytes_count);
            return;
        }
        else if (i2c->len > i2c->bytes_count) {
                i2c->buff[i2c->bytes_count - 1] = I2C->DR;
                if (i2c->len == (i2c->bytes_count + 1)) {
                    I2C->CR1 &=~ I2C_CR1_ACK;
                }
        }
        return;
    }
    else if (status_reg_1.sr1.STOPF == 1) {
        I2C->CR1 |= I2C_CR1_STOP;
    }
    else {
//        I2C->CR2 &=~ (I2C_CR2_ITEVTEN | I2C_CR2_ITERREN | I2C_CR2_ITBUFEN);
        i2c->cur_oper = HW_I2C__OPER_NONE;
        i2c->isr_cb(i2c_id, EVENT__ERROR, i2c->addr, i2c->buff, i2c->bytes_count);
    }
}

/*******************************************************************************
 * �������-���������� ���������� �� �������.
 ******************************************************************************/
void hw_i2c__I2C_ER_IRQHandler(int i2c_id) {
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    I2C_TypeDef *I2C = i2c->regs;

    I2C->CR1 &=~ I2C_CR1_ACK;
    I2C->CR1 |= I2C_CR1_STOP;
    i2c->i2c_delay = 0xff;
    hw_i2c__sr1_u status_reg_1;
    status_reg_1.sr1_u16 = I2C->SR1;
    events__e event = EVENT__ERROR;
    if (status_reg_1.sr1.TIMEOUT == 1) {
        I2C->SR1 &=~ I2C_SR1_TIMEOUT;
    }
    if (status_reg_1.sr1.AF == 1) {
        event = EVENT__ERROR;
        I2C->SR1 &=~ I2C_SR1_AF;
    }
    if (status_reg_1.sr1.ARLO == 1) {
         event = EVENT__ERROR;
         I2C->SR1 &=~ I2C_SR1_ARLO;
    }
    if (status_reg_1.sr1.OVR == 1) {
         event = EVENT__ERROR;
         I2C->SR1 &=~ I2C_SR1_OVR;
    }
    if ((status_reg_1.sr1.PECERR == 1) || (status_reg_1.sr1.BERR == 1)) {
         I2C->SR1 &=~ (I2C_SR1_PECERR | I2C_SR1_BERR);
    }
    i2c->cur_oper = HW_I2C__OPER_NONE;
//    I2C->CR2 &=~ (I2C_CR2_ITEVTEN | I2C_CR2_ITERREN | I2C_CR2_ITBUFEN);
    i2c->isr_cb(i2c_id, event, i2c->addr, i2c->buff, i2c->bytes_count);
}

/*******************************************************************************
 *  ������� ������������� ������� I2C_1.
 ******************************************************************************/
static events__e hw_i2c__itf1_gpio_en(void) {
    /**I2C1 GPIO Configuration
    PB6     ------> I2C1_SCL
    PB7     ------> I2C1_SDA
    */
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    I2C2->CR1 &=~ I2C_CR1_PE;
    gpio__init(MCU__GPIO_P_B_6, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
    gpio__init(MCU__GPIO_P_B_7, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
    if (!(READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_6) && READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_7)))
        return EVENT__ERROR;
    GPIOB->MODER &=~ GPIO_MODER_MODER6_0;
    GPIOB->MODER |= GPIO_MODER_MODER6_1;

    GPIOB->MODER &=~ GPIO_MODER_MODER7_0;
    GPIOB->MODER |= GPIO_MODER_MODER7_1;
    GPIOB->OTYPER |= GPIO_OTYPER_OT_7;
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR7_0;

    GPIOB->OTYPER |= GPIO_OTYPER_OT_6;
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR6_0;
    GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR6 | GPIO_OSPEEDER_OSPEEDR7);
    GPIOB->AFR[0] &=~ (GPIO_AFRL_AFRL6 | GPIO_AFRL_AFRL7);
    GPIOB->AFR[0] |= 0x44000000;
    RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;

    return EVENT__OK;
}

/*******************************************************************************
 *  ������� ��-������������� ������� I2C_1.
 ******************************************************************************/
static void hw_i2c__itf1_gpio_dis(void) {
    GPIOB->MODER &=~ (GPIO_MODER_MODER6 | GPIO_MODER_MODER7);
    GPIOB->OTYPER &=~ (GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_7);
    GPIOB->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR6 | GPIO_OSPEEDER_OSPEEDR7);
    GPIOB->PUPDR &=~ (GPIO_PUPDR_PUPDR6 | GPIO_PUPDR_PUPDR7);
    GPIOB->AFR[0] &=~ (GPIO_AFRL_AFRL6 | GPIO_AFRL_AFRL7);
    RCC->APB2ENR &=~ RCC_APB1ENR_I2C1EN;
}

/*******************************************************************************
 *  ������� ������������� ������� I2C_2.
 ******************************************************************************/
static events__e hw_i2c__itf2_gpio_en(void) {
    /**I2C2 GPIO Configuration
    PB10     ------> I2C2_SCL
    PB11     ------> I2C2_SDA
    */
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    RCC->APB1ENR |= RCC_APB1ENR_I2C2EN;
    I2C2->CR1 &=~ I2C_CR1_PE;
    gpio__init(MCU__GPIO_P_B_10, GPIO__DIR_IN | GPIO__PULL_UP, 0);
    gpio__init(MCU__GPIO_P_B_11, GPIO__DIR_IN | GPIO__PULL_UP, 0);
    if (!(READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_11) && READ_BIT(GPIOB->IDR, GPIO_IDR_IDR_10)))
        return EVENT__ERROR;

    GPIOB->MODER &=~ (GPIO_MODER_MODER10_0);
    GPIOB->MODER |= GPIO_MODER_MODER10_1;

    GPIOB->MODER &=~ (GPIO_MODER_MODER11_0);
    GPIOB->MODER |= GPIO_MODER_MODER11_1;
    GPIOB->OTYPER |= GPIO_OTYPER_OT_11;
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR11_0;

    GPIOB->OTYPER |= GPIO_OTYPER_OT_10;
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR10_0;
    GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR10 | GPIO_OSPEEDER_OSPEEDR11);
    GPIOB->AFR[1] &=~ (GPIO_AFRH_AFRH10 | GPIO_AFRH_AFRH11);
    GPIOB->AFR[1] |= 0x00004400;
    RCC->APB1ENR |= RCC_APB1ENR_I2C2EN;

    return EVENT__OK;
}

/*******************************************************************************
 *  ������� ��-������������� ������� I2C_2.
 ******************************************************************************/
static void hw_i2c__itf2_gpio_dis(void) {
    GPIOB->MODER &=~ (GPIO_MODER_MODER10 | GPIO_MODER_MODER11);
    GPIOB->OTYPER &=~ (GPIO_OTYPER_OT_10 | GPIO_OTYPER_OT_11);
    GPIOB->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR10 | GPIO_OSPEEDER_OSPEEDR11);
    GPIOB->PUPDR &=~ (GPIO_PUPDR_PUPDR10 | GPIO_PUPDR_PUPDR11);
    GPIOB->AFR[1] &=~ (GPIO_AFRH_AFRH10 | GPIO_AFRH_AFRH11);
    RCC->APB1ENR &=~ RCC_APB1ENR_I2C2EN;
}

#endif