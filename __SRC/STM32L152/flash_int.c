/***************************************************************************//**
 * @file flash_int.c.
 * @brief ������ ������ � ���������� FLASH (���������� ��� stm32L152).
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "standard.h"
#define FLASH_INT__MDL
    #include "target.h"
#undef FLASH_INT__MDL
#include "flash_int.h"
#include "flash.h"
#include "resource.h"
#include "supervisor.h"
#include "wdt.h"
#include "mcu.h"
#include "stm32l1xx.h"


#define FLASH_INT__ID 1
#ifndef FLASH_INT__ID
    #error "You must declare FLASH_INT__ID in target.h for use flash_int modul!"
#endif

/*******************************************************************************
 * �������� FLASH ������ ��.
 ******************************************************************************/
    #define MCU__FLASH_INT_INFO_MEM_BEGIN     ((uint32_t)0x1FF00000)
    #define MCU__FLASH_INT_INFO_MEM_END       ((uint32_t)0x1FF800FF)
    #define MCU__FLASH_INT_EEPROM_MEM_BEGIN   ((uint32_t)0x08080000)
    #define MCU__FLASH_INT_EEPROM_MEM_END     ((uint32_t)0x08080FFF)
    #define MCU__FLASH_INT_MAIN_MEM_BEGIN     ((uint32_t)0x08000000)
    #define MCU__FLASH_INT_MAIN_MEM_END       ((uint32_t)0x0801FFFF)


#define FLASH_PDKEY1               ((uint32_t)0x04152637U) /*!< Flash power down key1 */
#define FLASH_PDKEY2               ((uint32_t)0xFAFBFCFDU) /*!< Flash power down key2: used with FLASH_PDKEY1
                                                              to unlock the RUN_PD bit in FLASH_ACR */

#define FLASH_PEKEY1               ((uint32_t)0x89ABCDEFU) /*!< Flash program erase key1 */
#define FLASH_PEKEY2               ((uint32_t)0x02030405U) /*!< Flash program erase key: used with FLASH_PEKEY2
                                                               to unlock the write access to the FLASH_PECR register and
                                                               data EEPROM */

#define FLASH_PRGKEY1              ((uint32_t)0x8C9DAEBFU) /*!< Flash program memory key1 */
#define FLASH_PRGKEY2              ((uint32_t)0x13141516U) /*!< Flash program memory key2: used with FLASH_PRGKEY2
                                                               to unlock the program memory */

#define FLASH_OPTKEY1              ((uint32_t)0xFBEAD9C8U) /*!< Flash option key1 */
#define FLASH_OPTKEY2              ((uint32_t)0x24252627U) /*!< Flash option key2: used with FLASH_OPTKEY1 to
                                                              unlock the write access to the option byte block */


#define FLASH_PAGE_SIZE           (256U)  /*!< FLASH Page Size in bytes */

const flash__sectors_set_t flash_int__memory_map[] = { \
    { 0x08000000, 32,   16,  256 },
    {          0,  0,    0,    0 },
};


// ���� ����������� ��������.
typedef enum {
    FLASH_INT__OPER_NA,             // ��� ��������.
    FLASH_INT__OPER_R,              // ������.
    FLASH_INT__OPER_W,              // ������.
    FLASH_INT__OPER_CP,             // ������� ��������.
    FLASH_INT__OPER_CS,             // ������� �������.
    FLASH_INT__OPER_CA,             // ������� ���� ������.
} flash_int__operation_t;

// ���������, ����������� ������ ������ � FLASH.
typedef struct {
    int init;                                                       // ���� ������������� ������ ������ � FLASH (���������).
    uint32_t cur_clk;                                               // ������� ������� ������������ FLASH.

    flash__event_cb cb;                                         // ��������� �� ������� ��� ������� �������� (NULL - ��� ��������).
    void *ext_data;                                                 // ��������� �� ���������������� ������.
    flash_int__operation_t cur_oper;                                // ������� ��������.
#if __DATA_MODEL__ == __DATA_MODEL_SMALL__
    uint32_t addr;                                                  // �����, �� �������� �������������� ��������.
#else
    uint32_t addr;                                                  // �����, �� �������� �������������� ��������.
#endif
    uint8_t *data;                                                  // ��������� �� ���������������� ������.
    uint32_t len;                                                   // ������ ���������������� ������.
    supervisor__idle_sub_t idle_sub;                                // ������������� ���������� �� ���������� ����� � ��� (����� ��� ��������).
    resource__t resource;                                           // FLASH �������� ��������.
} flash_int__struct_t;

static flash_int__struct_t flash;

static ax_event__e flash_int__op_page_erase(void);
static ax_event__e flash_int__op_write(void);
static ax_event__e flash_int__op_read(void);

extern void flash_int__isr_handler(void);
static int get_addr_begin(int addr);
static ax_event__e flash__unlock(void);
static ax_event__e flash_lock(void);

/*******************************************************************************
 * ������� �������� ��������.
 ******************************************************************************/
ax_event__e flash_int__page_erase(uint32_t addr, flash__event_cb erase_cb, void *ext_data) {
    if (!(addr >= MCU__FLASH_INT_MAIN_MEM_BEGIN && addr <= MCU__FLASH_INT_MAIN_MEM_END))
        return AX_EVENT__PARAMS_NA;
    addr = get_addr_begin(addr); //���������� ������ ��������
    if (resource__lock(&flash.resource) != RESOURCE__OK)
        return AX_EVENT__BUSY;
    flash.cur_oper = FLASH_INT__OPER_CP;
    flash.addr = addr;
    flash.len = 256;
    if (erase_cb != NULL) {     // ���� ���������, �� ���������� �������� � ��������.
        flash.cb = erase_cb;
        flash.ext_data = ext_data;
        supervisor__idle_lock(&flash.idle_sub);
        return AX_EVENT__OK;
    }
    ax_event__e res = flash_int__op_page_erase();
    resource__unlock(&flash.resource);
    return res;
}

/*******************************************************************************
 * ������� �������� �������.
 ******************************************************************************/
ax_event__e flash_int__sector_erase(uint32_t addr, flash__event_cb erase_cb, void *ext_data) {
    // � stm32l1 �������� ��� �������� ������ �������� (��������).
    return AX_EVENT__PARAMS_NA;
}

/*******************************************************************************
 * ������� �������� ���� ������.
 ******************************************************************************/
ax_event__e flash_int__erase_all(flash__event_cb erase_cb, void *ext_data) {
    // todo ���� �� �����������.
    return AX_EVENT__PARAMS_NA;
}

/*******************************************************************************
 * ������� ������ ������ �� FLASH.
 ******************************************************************************/
ax_event__e flash_int__write(uint32_t addr, uint8_t *data, uint32_t len, flash__event_cb write_cb, void *ext_data) {
    if ( !(addr >= MCU__FLASH_INT_INFO_MEM_BEGIN && addr <= MCU__FLASH_INT_INFO_MEM_END) && !(addr >= MCU__FLASH_INT_MAIN_MEM_BEGIN && addr <= MCU__FLASH_INT_MAIN_MEM_END))
        return AX_EVENT__PARAMS_NA;
    if ( !(addr + len >= MCU__FLASH_INT_INFO_MEM_BEGIN && addr + len <= MCU__FLASH_INT_INFO_MEM_END) && !(addr + len >= MCU__FLASH_INT_MAIN_MEM_BEGIN && addr + len <= MCU__FLASH_INT_MAIN_MEM_END))
        return AX_EVENT__PARAMS_NA;
    if (data == NULL || len == 0)
        return AX_EVENT__PARAMS_NA;

    if (resource__lock(&flash.resource) != RESOURCE__OK)
        return AX_EVENT__BUSY;
    flash.cur_oper = FLASH_INT__OPER_W;
    flash.addr = addr;
    flash.data = data;
    flash.len = len;
    if (write_cb != NULL) {     // ���� ���������, �� ���������� �������� � ��������.
        flash.cb = write_cb;
        flash.ext_data = ext_data;
        supervisor__idle_lock(&flash.idle_sub);
        return AX_EVENT__OK;
    }
    ax_event__e res = flash_int__op_write();
    resource__unlock(&flash.resource);
    return res;
}

/*******************************************************************************
 * ������� ������ ������ �� FLASH � ��������� �����.
 ******************************************************************************/
ax_event__e flash_int__read(uint32_t addr, uint8_t *buff, uint32_t len, flash__event_cb read_cb, void *ext_data) {
    if ( !(addr >= MCU__FLASH_INT_INFO_MEM_BEGIN && addr <= MCU__FLASH_INT_INFO_MEM_END) && !(addr >= MCU__FLASH_INT_MAIN_MEM_BEGIN && addr <= MCU__FLASH_INT_MAIN_MEM_END))
        return AX_EVENT__PARAMS_NA;
    if ( !(addr + len >= MCU__FLASH_INT_INFO_MEM_BEGIN && addr + len <= MCU__FLASH_INT_INFO_MEM_END) && !(addr + len >= MCU__FLASH_INT_MAIN_MEM_BEGIN && addr + len <= MCU__FLASH_INT_MAIN_MEM_END))
        return AX_EVENT__PARAMS_NA;
    if (buff == NULL || len == 0)
        return AX_EVENT__PARAMS_NA;

    if (resource__lock(&flash.resource) != RESOURCE__OK)
        return AX_EVENT__BUSY;
    flash.cur_oper = FLASH_INT__OPER_R;
    flash.addr = addr;
    flash.data = buff;
    flash.len = len;
    if (read_cb != NULL) {     // ���� ���������, �� ���������� �������� � ��������.
        flash.cb = read_cb;
        flash.ext_data = ext_data;
        supervisor__idle_lock(&flash.idle_sub);
        return AX_EVENT__OK;
    }
    ax_event__e res = flash_int__op_read();
    resource__unlock(&flash.resource);
    return res;
}

/*******************************************************************************
 * ������� ��������� �������� ��������� ������ ������ � FLASH.
 ******************************************************************************/
void flash_int__cout(void) {
    ax_event__e res;
    supervisor__idle_unlock(&flash.idle_sub);

    switch (flash.cur_oper) {
        case FLASH_INT__OPER_R: {
            res = flash_int__op_read();
            break;
        }
        case FLASH_INT__OPER_W: {
            res = flash_int__op_write();
            break;
        }
        case FLASH_INT__OPER_CP: {
            res = flash_int__op_page_erase();
            break;
        }
        default: return;
    }
    resource__cb_start(&flash.resource);
    flash.cb(FLASH_INT__ID, res, flash.addr, flash.data, flash.len, flash.ext_data);
    resource__cb_stop(&flash.resource);
}

/*******************************************************************************
 * ������� ��������� ��������� � ��������� ����� ������ FLASH.
 ******************************************************************************/
const flash__sectors_set_t *flash_int__memory_map_get(void) {
    return flash_int__memory_map;
}

/*******************************************************************************
 * ������� ������������� �������� �������� ��������.
 ******************************************************************************/
static ax_event__e flash_int__op_page_erase(void) {
    ax_event__e res = AX_EVENT__OK;
    CRITICAL_SECTION_ON
    wdt__cmd(WDT__OFF);
    res = flash__unlock();
    /* Set the ERASE bit */
    SET_BIT(FLASH->PECR, FLASH_PECR_ERASE);
    /* Set PROG bit */
    SET_BIT(FLASH->PECR, FLASH_PECR_PROG);
    *(__IO uint32_t *)(uint32_t)(flash.addr) =  0x00000000;
    while ((FLASH->SR & FLASH_SR_EOP) == 0) {;}
    CLEAR_BIT(FLASH->PECR, FLASH_PECR_ERASE);
    CLEAR_BIT(FLASH->PECR, FLASH_PECR_PROG);
    SET_BIT(FLASH->SR, FLASH_SR_EOP);
    res = flash_lock();
    flash.cur_oper = FLASH_INT__OPER_NA;
    wdt__cmd(WDT__ON);
    CRITICAL_SECTION_OFF
    return res;
}

/*******************************************************************************
 * ������� ������������� �������� ������.
 ******************************************************************************/
static ax_event__e flash_int__op_write(void) {
        ax_event__e res;
        CRITICAL_SECTION_ON
        wdt__cmd(WDT__OFF);
        res = flash__unlock();
        memcpy((uint8_t*) flash.addr, flash.data, flash.len);
        while ((FLASH->SR & FLASH_SR_EOP) == 0) {;}
        flash.cur_oper = FLASH_INT__OPER_NA;
        res = flash_lock();
        wdt__cmd(WDT__ON);
        CRITICAL_SECTION_OFF
        return res;
}

/*******************************************************************************
 * ������� ������������� �������� ������.
 ******************************************************************************/
static ax_event__e flash_int__op_read(void) {
    CRITICAL_SECTION_ON
    memcpy(flash.data, (uint32_t*) flash.addr, flash.len);  // ����������� ������.
    flash.cur_oper = FLASH_INT__OPER_NA;
    CRITICAL_SECTION_OFF
    return AX_EVENT__OK;
}

static ax_event__e flash__unlock(void) {
   if (READ_BIT(FLASH->PECR, FLASH_PECR_PRGLOCK))
  {
    /* Unlocking FLASH_PECR register access*/
    if(READ_BIT(FLASH->PECR, FLASH_PECR_PELOCK))
    {
      WRITE_REG(FLASH->PEKEYR, FLASH_PEKEY1);
      WRITE_REG(FLASH->PEKEYR, FLASH_PEKEY2);
    }

    /* Unlocking the program memory access */
    WRITE_REG(FLASH->PRGKEYR, FLASH_PRGKEY1);
    WRITE_REG(FLASH->PRGKEYR, FLASH_PRGKEY2);
  }
  else
  {
    return AX_EVENT__ERROR;
  }

  return AX_EVENT__OK;
}

static ax_event__e flash_lock(void) {
  SET_BIT(FLASH->PECR, FLASH_PECR_PRGLOCK);
  return AX_EVENT__OK;
}

static int get_addr_begin(int addr) {
  addr -= MCU__FLASH_INT_MAIN_MEM_BEGIN;
  int page_nuber = addr/FLASH_PAGE_SIZE;
  int begin_number = (MCU__FLASH_INT_MAIN_MEM_BEGIN + (FLASH_PAGE_SIZE ) * page_nuber);
  return begin_number;
}

