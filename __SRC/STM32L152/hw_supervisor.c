/***************************************************************************//**
 * @file supervisor.c.
 * @brief ������ ���������� �������� � �������� ���������������� �����������.
 * @author s.korotkov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "mcu.h"
#include "standard.h"
#include "hw_supervisor.h"
#include "supervisor.h"

#define SYS_CLOCK_LOW_SLEEP_0_HZ    4200000     // ��������� ������� ��� ������������ �� MSI (� Hz)
#define SYS_CLOCK_LOW_SLEEP_1_HZ    131000      // ��������� ������� ��� ��� (� Hz)
#define SYS_CLOCK_HIGH_SLEEP_0_HZ   32000000    // ��������� ������� ��� ������������ �� HSI (� Hz)

#define WDT__IWD_DELAY_MAX                      0xFFF
#define WDT__IWDG_KEY_RELOAD                    0xAAAA      /*!< IWDG Reload Counter Enable   */
#define WDT__IWDG_KEY_ENABLE                    0x0000CCCC  /*!< IWDG Peripheral Enable       */
#define WDT__IWDG_KEY_WRITE_ACCESS_ENABLE       0x00005555  /*!< IWDG KR Write Access Enable  */
#define WDT__IWDG_KEY_WRITE_ACCESS_DISABLE      0x00000000  /*!< IWDG KR Write Access Disable */

// ��������� ��������� ������.
typedef struct {
    uint32_t cpu_clk;                                           // ������� ������� CPU.
    uint32_t periph_clk;                                        // ������� ������� ���������.
    supervisor__wdt_e cur_state;                                // ������� ��������� ������.
    uint8_t count_lock;                                         // ���������� ����������� �������, ������������� �� ����������� watchdog
} supervisor__struct_t;

static supervisor__struct_t supervisor;

typedef enum {
    LVL_1_9V,
    LVL_2_1V,
    LVL_2_3V,
    LVL_2_5V,
    LVL_2_7V,
    LVL_2_9V,
    LVL_3_1V,
} supervisor__pvd_e;

typedef enum {
    LVL_1_8V,
    LVL_1_5V,
    LVL_1_2V,
} supervisor__pvr_e;

typedef enum {
    LVL_1_75,
    LVL_2_05,
    LVL_2_4,
    LVL_2_6,
    LVL_2_9,
} supervisor__bor_e;

/* static void hw_supervisor__set_voltage_regulator_range(supervisor__pvr_e range); */
static void hw_supervisor__set_pvd_threshold(supervisor__pvd_e threshold);
static void hw_supervisor__set_bor_lvl(supervisor__bor_e borlvl);

/*******************************************************************************
 * ������� ��������� ���������� ������� ����������� (PDV, BOR � ��.).
 ******************************************************************************/
void hw_supervisor__init(void) {
    hw_supervisor__set_bor_lvl(LVL_2_4);
    hw_supervisor__set_pvd_threshold(LVL_3_1V);
}

/*******************************************************************************
 * ������� ������������ ����������.
 ******************************************************************************/
void hw_supervisor__apl_reset(void) {
    SCB->AIRCR = 0x05FA0000 | SCB_AIRCR_SYSRESETREQ_Msk;
}

/*******************************************************************************
 * ������� �������� ����������� � SLEEP.
 * ���� �������� �������� ��� ������� �� ����� ������ �� ����������� ����������� �� ����������(���������� � hard fault).
 * ���������� � ��������� irq + ���������� NOP-�� ��������.
 ******************************************************************************/
void hw_supervisor__idle_on(void) {

    MODIFY_REG(RCC->APB1ENR, 0, RCC_APB1ENR_PWREN);     //��������� ������������ PWR �������, ��� ��� �� ����� � ����!
    supervisor.cpu_clk = 0;
    MODIFY_REG(SCB->SCR, 0, SCB_SCR_SLEEPDEEP_Msk ); // low-power mode = stop mode
    MODIFY_REG(PWR->CR, 0, PWR_CR_LPSDSR);
    __disable_irq();
    FLASH->ACR |= FLASH_ACR_SLEEP_PD;  //���� ����� �������� flash � �����
    hw_supervisor__clk_set(SUPERVISOR__CLK_LOW, 0); // 0= 4Mhz, 1= 131kHz
    __WFI();
   // PWR->CR &=~ PWR_CR_LPSDSR; //������� ���������� �� �����- ��������� � run ���� ����� ������ �� ����� ���������.

    __enable_irq();
}

/*******************************************************************************
  ������� ������ ����������� �� SLEEP.
 ******************************************************************************/
void hw_supervisor__idle_off(void) {
    hw_supervisor__clk_set(SUPERVISOR__CLK_HIGH, 0);
}

/*******************************************************************************
 * ������� ��������� ������� ������� ������������ ����������.
 ******************************************************************************/
uint32_t hw_supervisor__cpu_clk_get(void) {
    return supervisor.cpu_clk;
}

/*******************************************************************************
 * ������� ��������� ������� ������� ������������ ���������.
 ******************************************************************************/
uint32_t hw_supervisor__periph_clk_get(void) {
    return supervisor.periph_clk;
}

/*******************************************************************************
 * ������� ��������� ������� ������������ ��.
 ******************************************************************************/
supervisor__event_e hw_supervisor__clk_set(supervisor__clk_e clk, int sleep) {
    switch (clk) {
        case SUPERVISOR__CLK_LOW: {
            // ������������ �� MSI, SYSCLK = HCLK = 4.2 MHz, APB1,APB2 = 1. ��� ��������� 4.2 MHz

            /*������������������ ���������� HSI: ���� ����� � �����
            1) ��c�������� � ��������� MSI
            2) ��������� PLL
            3) ��������� HSI
            4) ������ �������� ������������ sysclck � HSI �� MSI
            */
            //����������� MSI
            if (sleep == 0) {
                 MODIFY_REG(RCC->ICSCR, RCC_ICSCR_MSIRANGE, RCC_ICSCR_MSIRANGE_6);// 6 = 4 MHz
                 supervisor.cpu_clk = SYS_CLOCK_LOW_SLEEP_0_HZ;
                 supervisor.periph_clk = SYS_CLOCK_LOW_SLEEP_0_HZ;
            }
            else if (sleep == 1) {
                MODIFY_REG(RCC->ICSCR, RCC_ICSCR_MSIRANGE, RCC_ICSCR_MSIRANGE_1); // 1 = 131 kHz
                supervisor.cpu_clk = SYS_CLOCK_LOW_SLEEP_1_HZ;
                supervisor.periph_clk = SYS_CLOCK_LOW_SLEEP_1_HZ;
            }
            else
                return SUPERVISOR__EVENT_ERROR;
            // �������� �����
            FLASH->ACR |= FLASH_ACR_PRFTEN;
            //������� MSI
            MODIFY_REG(RCC->CR, 0, RCC_CR_MSION);
            //���� ������������ MSI
            while ((RCC->CR & RCC_CR_MSIRDY) != RCC_CR_MSIRDY) {;}
            //������ HSI ��������� MSI ��� ������������ SysClck
            MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, RCC_CFGR_SW_MSI);
            //���� ������������ PLL ��� MSI
            while ((RCC->CFGR & RCC_CFGR_SW) != RCC_CFGR_SWS_MSI) {;}

//            hw_supervisor__set_voltage_regulator_range(LVL_1_2V);

            break;
        }
        case SUPERVISOR__CLK_HIGH: {
            // ������������ �� PLL+HSI, SYSCLK = HCLK = 32 MHz, APB1,APB2 = 1. ��� ��������� 32 MHz
            /*������������������ ��������� ��������� ������� sysclk
             ��� ������������ �� HSI:
            1) ������ �������� ������������ sysclck � HSI �� MSI
            2) ��������� PLL
            3) ���������� ��������� PLL ��� ����� ��������� ������� (����������� �� HSI) */
            //RCC->CR &=~  RCC_CR_HSEON;
            //�������� HSE �.�. �� �������������
            //RCC->APB1ENR |= RCC_APB1ENR_PWREN;
            //��������� ������ ��������
//            hw_supervisor__set_voltage_regulator_range(LVL_1_8V);  //����������� ��� ���� TO DO
            //�������� ������������� �� ���.60 �� REF.MANUAL
           /* Enable 64-bit access */
            MODIFY_REG( FLASH->ACR , 0, FLASH_ACR_ACC64);
            while ((FLASH->ACR &  FLASH_ACR_ACC64) == 0) {;}
            /* Enable Prefetch Buffer */
            FLASH->ACR |= FLASH_ACR_PRFTEN;
            /* Flash 1 wait state */
            MODIFY_REG( FLASH->ACR , 0, FLASH_ACR_LATENCY); //����������� ��� ���� TO DO
            while ((FLASH->ACR &  FLASH_ACR_LATENCY) == 0) {;}
            //������ HSI
            MODIFY_REG(RCC->CR, 0, RCC_CR_HSION);
            //���� ������������ HSI
            while ((RCC->CR & RCC_CR_HSIRDY) == 0) {;}
             /*��������� ���������� ��������� �����, ��� ������� �� ��������� �������
              ���������� ����������� ��� ����������� PLL c��. 144 � REF.MANUAL */
             //��������� PLL
            MODIFY_REG(RCC->CR, RCC_CR_PLLON, 0);
            // �������� ��������� ������� (HCLK = SYSCLK)
            MODIFY_REG(RCC->CFGR, RCC_CFGR_HPRE, RCC_CFGR_HPRE_DIV1);
            //�������� ���� APB1 (PCLK2 = HCLK)
            MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE2,RCC_CFGR_PPRE2_DIV1);
            //�������� ���� APB2 (PCLK1 = HCLK)
            MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE1,RCC_CFGR_PPRE1_DIV1);
            //������� ��������� PLL
            //MODIFY_REG(RCC->CFGR, RCC_CFGR_PLLSRC | RCC_CFGR_PLLMUL | RCC_CFGR_PLLDIV, 0);
            //������ �������� �������� � ���������� //32mhz
            MODIFY_REG(RCC->CFGR, RCC_CFGR_PLLSRC | RCC_CFGR_PLLMUL | RCC_CFGR_PLLDIV,RCC_CFGR_PLLSRC_HSI | RCC_CFGR_PLLMUL4 |RCC_CFGR_PLLDIV2);
            //�������� PLL
            MODIFY_REG(RCC->CR,0 , RCC_CR_PLLON);
            //���� ������������ PLL
            while((RCC->CR & RCC_CR_PLLRDY) == 1) {;}
           //�������� PLL ��� ������������ SysClk
            MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, RCC_CFGR_SW_PLL);
            //���� ������������ PLL
            while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL) {;}
            supervisor.cpu_clk = SYS_CLOCK_HIGH_SLEEP_0_HZ;
            supervisor.periph_clk = SYS_CLOCK_HIGH_SLEEP_0_HZ;

            break;
        }
        default:
            return SUPERVISOR__EVENT_ERROR;
    }
    return SUPERVISOR__EVENT_OK;
}

/*******************************************************************************
 * ������� ��������� PVR �� ������ �������.
 ******************************************************************************/
/*
static void hw_supervisor__set_voltage_regulator_range(supervisor__pvr_e range) {
    RCC->CR &=~  RCC_CR_HSEON;
    //�������� ������������ PWR
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    //���� ��������� VOSF � �������� PWR_CSR � ����
    while(READ_BIT(PWR->CSR, PWR_CSR_VOSF) == 1) {;}
    switch (range) {
        case LVL_1_8V: // 1.8V MAX  RANGE 1
            PWR->CR |= PWR_CR_VOS_0;
            PWR->CR &=~ PWR_CR_VOS_1;
            break;
        case LVL_1_5V:  // 1.5V medium RANGE 2
            PWR->CR |= PWR_CR_VOS_1;
            PWR->CR &=~ PWR_CR_VOS_0;
            break;
        case LVL_1_2V:   // 1.2V LOW RANGE 3
            PWR->CR |= PWR_CR_VOS_0;
            PWR->CR |= PWR_CR_VOS_1;
            break;
        default:   //�������� ��� ���������, �������� ���������� RANGE
            PWR->CR &=~ PWR_CR_VOS_0;
            PWR->CR &=~ PWR_CR_VOS_1;
            break;
    }
    //���� ��������� VOSF � �������� PWR_CSR � ����
    while(READ_BIT(PWR->CSR, PWR_CSR_VOSF) == 1) {;}
    //��������� ������ �� 104 ���. � RM
}
*/

/*******************************************************************************
 * ������� ��������� PVD �� ������ �������.
 ******************************************************************************/
static void hw_supervisor__set_pvd_threshold(supervisor__pvd_e threshold) {
    //�������� ������������ PWR
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    //�������� PVD
    MODIFY_REG(PWR->CR,PWR_CR_PVDE, PWR_CR_PVDE);
    //���������� ��������� ������� ����������
    switch (threshold) {
    case LVL_1_9V: // 1.9V
        MODIFY_REG(PWR->CR, PWR_CR_PLS_LEV7, PWR_CR_PLS_LEV0);
        break;
    case LVL_2_1V:  // 2.1V
       MODIFY_REG(PWR->CR, PWR_CR_PLS_LEV7, PWR_CR_PLS_LEV1);
        break;
    case LVL_2_3V:   // 2.3V
        MODIFY_REG(PWR->CR, PWR_CR_PLS_LEV7, PWR_CR_PLS_LEV2);
        break;
    case LVL_2_5V: // 2.5V
        MODIFY_REG(PWR->CR, PWR_CR_PLS_LEV7, PWR_CR_PLS_LEV3);
        break;
    case LVL_2_7V:  // 2.7V
        MODIFY_REG(PWR->CR, PWR_CR_PLS_LEV7, PWR_CR_PLS_LEV4);
        break;
    case LVL_2_9V:   // 2.9V
        MODIFY_REG(PWR->CR, PWR_CR_PLS_LEV7, PWR_CR_PLS_LEV5);
        break;
    case LVL_3_1V:   // 3.1V
        MODIFY_REG(PWR->CR, PWR_CR_PLS_LEV7, PWR_CR_PLS_LEV6);
        break;
    default:   // 1.9V
        MODIFY_REG(PWR->CR,PWR_CR_PVDE, 0);
        MODIFY_REG(PWR->CR, PWR_CR_PLS_LEV7, PWR_CR_PLS_LEV0);
        break;
       // ���������� ���� ��� ���� �������� PVD �
       //  PWR->CSR   PWR_CSR_PVDO  0 - ���� ������, 1 - ����
    }
}

/*******************************************************************************
 * ������� ��������� BOR �� ������ �������.
 ******************************************************************************/
static void hw_supervisor__set_bor_lvl(supervisor__bor_e borlvl) {
   uint32_t tmp = 0U, tmp1 = 0U, tmp2 = 0U;
    if(READ_BIT(FLASH->PECR, FLASH_PECR_OPTLOCK)) {
    /* Unlocking FLASH_PECR register access*/
        if(SET_BIT(FLASH->PECR, FLASH_PECR_PELOCK)) {
        /* Unlocking FLASH_PECR register access*/
        WRITE_REG(FLASH->PEKEYR, 0x89ABCDEF);
        WRITE_REG(FLASH->PEKEYR, 0x02030405);
        }
        /* Unlocking the option bytes block access */
        WRITE_REG(FLASH->OPTKEYR, 0xFBEAD9C8);
        WRITE_REG(FLASH->OPTKEYR, 0x24252627);
        /* Get the User Option byte register */
        tmp1 = OB->USER & ((~FLASH_OBR_BOR_LEV) >> 16U);

        // ���������� ��������� ������� ����������
        switch (borlvl) {
            case LVL_1_75: // 1.69-1.8V
                tmp2 = ((uint8_t)0x08U);
                break;
            case LVL_2_05:  // 1.94-2.1V
                tmp2 = ((uint8_t)0x09U);
                break;
            case LVL_2_4:   // 2.3-2.49V
                tmp2 = ((uint8_t)0x0AU);
                break;
            case LVL_2_6: // 2.54-2.74V
               tmp2 = ((uint8_t)0x0BU);
                break;
            case LVL_2_9:  // 2.77-3.0V
                tmp2 = ((uint8_t)0x0CU);
                break;
            default:   // 1.69-1.8V
                tmp2 = ((uint8_t)0x00U);
                break;
        }
        uint32_t mama = ((FLASH->OBR & FLASH_OBR_BOR_LEV) >> 16);
        if (((FLASH->OBR & FLASH_OBR_BOR_LEV) >> 16) != tmp2) {
            /* Calculate the option byte to write - [0xFFU | nUSER | 0x00U | USER]*/
            tmp = (uint32_t)~((tmp2 | tmp1)) << 16U;
            tmp |= (tmp2 | tmp1);
            /* Write the BOR Option Byte */
            CRITICAL_SECTION_ON
                OB->USER = tmp;
            CRITICAL_SECTION_OFF
            SET_BIT(FLASH->PECR, FLASH_PECR_OPTLOCK);
            SET_BIT(FLASH->PECR, FLASH_PECR_OBL_LAUNCH);
            SET_BIT(FLASH->SR, FLASH_SR_OPTVERR);
        }
    }
}

/*******************************************************************************
 * ������� ������ � WDT.
 ******************************************************************************/
supervisor__wdt_e hw_supervisor__wdt_cmd(supervisor__wdt_e cmd) {
    supervisor__wdt_e res = SUPERVISOR__WDT_ERROR;
    switch (cmd) {
        case SUPERVISOR__WDT_ON_1S:
        case SUPERVISOR__WDT_ON_2S:
        case SUPERVISOR__WDT_ON_3S:
        case SUPERVISOR__WDT_ON_4S:
            if (!supervisor.count_lock) {
                IWDG->KR = WDT__IWDG_KEY_ENABLE;                // ��������� IWDG
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // ��������� ������ � PR � RLR ��������
                for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
                IWDG->PR = 3;
                IWDG->RLR = cmd * 1000;
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;
                supervisor.cur_state = cmd;
            }
            else
                supervisor.count_lock--;
        case SUPERVISOR__WDT_RESET:
            IWDG->KR = WDT__IWDG_KEY_RELOAD;  // ������������� �������
        case SUPERVISOR__WDT_GET_STATE:
            res = supervisor.cur_state;
            break;
        case SUPERVISOR__WDT_OFF:
            if (supervisor.count_lock < 0xFF)
                supervisor.count_lock++;
            IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // ��������� ������ � PR � RLR ��������
            for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
            IWDG->PR = 7;
            IWDG->RLR = 0xFFF;
            IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;
            supervisor.cur_state = cmd;
        default:
            // ��� ����� �������.
            break;
    }
    return res;
}