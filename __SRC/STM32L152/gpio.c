/***************************************************************************//**
 * @file gpio.c.
 * @brief ������ ���������� ������� �����-������ ����������� stm32l15xRBx.
 * @author s.korotkov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "standard.h"
#include "gpio.h"
#include "mcu.h"

// ��������� �������� ������� ������ GPIO.
 const GPIO_TypeDef* const gpio__mass[] = {
    GPIOA,
    GPIOB,
    GPIOC,
    GPIOD,
    GPIOE,
};

// ��������� ������ GPIO.
typedef struct {
    gpio__isr_cb_t isr_cb[16];
} gpio__t;

static gpio__t gpio_modul;

/*******************************************************************************
 * ��������� �������
 ******************************************************************************/
static void NVIC_Control(int gpio);

/*******************************************************************************
 * ������� ������������� ������ �����.
 ******************************************************************************/
gpio__event_e gpio__init(int gpio, int pin_settings, gpio__isr_cb_t isr_cb) {
    int pin = (gpio & 0x0F);
    int port = gpio >> 4;
    int low_bit = (1<<(pin*2));
    int high_bit = (1<<((pin*2)+1));

    // ��������� ���������� ���� �� ������ ���������� ����������������
    if ((SYSCFG->EXTICR[(pin/4)] & (0xF << (pin%4)*4)) == (port << (pin%4)*4))
        MODIFY_REG(EXTI->IMR, (1 << pin), 0);

    if (((pin_settings & GPIO__INT_MASK) == GPIO__INT_EDGE_RISING || (pin_settings & GPIO__INT_MASK) == GPIO__INT_EDGE_FALING) && isr_cb == NULL)
        return GPIO__EVENT_ERROR;

    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // ��������� ������ ����� � ������.

    // ������ ������������ ����� (�,B, C,D).
#if defined(STM32F40_41xxx) || defined(STM32F407xx)
    MODIFY_REG(RCC->AHB1ENR, 0, (1 << port));  // (1/0)=>(enabled/disabled)
#elif defined(STM32L476xx) || defined(STM32L4R7xx)
    MODIFY_REG(RCC->AHB2ENR, 0, (1 << port));  // (1/0)=>(enabled/disabled)
#endif
#if defined(STM32L1XX_MD) || defined(STM32L1XX_MDP)
    MODIFY_REG(RCC->AHBENR, 0, (1 << port));    // (1/0)=>(enabled/disabled)
#endif /* STM32L1XX_MD */

    // ����������� ����������� (����/�����).
    if ((pin_settings & GPIO__DIR_MASK) == GPIO__DIR_OUT) { //01 - output
        port_struct->OTYPER &= (~(1 << pin));
        MODIFY_REG(port_struct->MODER, high_bit, low_bit);
    }
    else {   //00 - input (reset state)
        port_struct->OTYPER |= (1 << pin);
        MODIFY_REG(port_struct->MODER, high_bit | low_bit, 0);
    }

    // ����������� ��������.
    if ((pin_settings & GPIO__PULL_MASK) == GPIO__PULL_UP) {  //01 - pull-up
        MODIFY_REG(port_struct->PUPDR, high_bit, low_bit);
    }
    else if ((pin_settings & GPIO__PULL_MASK) == GPIO__PULL_DOWN) { //10 pull-down
        MODIFY_REG(port_struct->PUPDR, low_bit, high_bit);
    }
    else {
        MODIFY_REG(port_struct->PUPDR, (low_bit | high_bit), 0);
    }

    // ����������� ����� ���������� � ���������� ���.
    if ((pin_settings & GPIO__INT_MASK) == GPIO__INT_EDGE_FALING) {
        //�������� ������������ SYSCFGEN
        RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
        // ����������� �� ���������� ���������� ���
        SYSCFG->EXTICR[(pin/4)] &=~ (0xF << (pin%4)*4);
        SYSCFG->EXTICR[(pin/4)] |= (port << (pin%4)*4);
        MODIFY_REG(port_struct->MODER, high_bit | low_bit, 0);   //  configure IO as input
        MODIFY_REG(EXTI->FTSR, 0, (1 << pin));                   //   Falling trigger enabled
        MODIFY_REG(EXTI->RTSR, (1 << pin), 0);                   //   Rising trigger disabled
        MODIFY_REG(EXTI->IMR, 0, (1 << pin));                    //  Interrupt mask on line x
        //����������� � ��������� ���������� ����������
        NVIC_Control(gpio);
    }
    else if ((pin_settings & GPIO__INT_MASK) == GPIO__INT_EDGE_RISING) {
        RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
        SYSCFG->EXTICR[(pin/4)] &=~ (0xF << (pin%4)*4);
        SYSCFG->EXTICR[(pin/4)] |= (port << (pin%4)*4);

        MODIFY_REG(port_struct->MODER, high_bit | low_bit, 0);   //  configure IO as input
        MODIFY_REG(EXTI->RTSR, 0, (1 << pin));                   //  Rising trigger enabled
        MODIFY_REG(EXTI->FTSR, (1 << pin), 0);                   //  Falling trigger disabled
        MODIFY_REG(EXTI->IMR, 0, (1 << pin));                    //  Interrupt mask on line x
        // ����������� � ��������� ���������� ����������
        NVIC_Control(gpio);
    }
    if (isr_cb != NULL)
        gpio_modul.isr_cb[pin] = isr_cb;
    return GPIO__EVENT_OK;
}

/*******************************************************************************
 * ������� ������������� ������ ����� �������������� ��������. ������������
 * ������ ����������� �������� ��� ������������� �����������.
 ******************************************************************************/
gpio__event_e gpio__af_init(int gpio, int gpio_mode, int af_number) {
    int pin = (gpio & 0x0F);
    int port = gpio >> 4;
    int pin_pos = pin << 1;
    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // ��������� ������ ����� � ������.
//    MODIFY_REG(EXTI->IMR, (1 << pin), 0);

#if defined(STM32F40_41xxx)
    MODIFY_REG(RCC->AHB1ENR, 0, (1 << port));  // (1/0)=>(enabled/disabled)
#elif defined(STM32L476xx) || defined(STM32L4R7xx)
    MODIFY_REG(RCC->AHB2ENR, 0, (1 << port));  // (1/0)=>(enabled/disabled)
#elif defined(STM32L1XX_MD) || defined(STM32L1XX_MDP)
    MODIFY_REG(RCC->AHBENR, 0, (1 << port));    // (1/0)=>(enabled/disabled)
#endif /* STM32L1XX_MD */
    //����������� ���� �� very high speed
    port_struct->OSPEEDR |= (0x03 << pin_pos);
    //�������� pull-up
    MODIFY_REG(port_struct->PUPDR, 0x03 << pin_pos, gpio_mode << pin_pos);
    //��� ������ open-drain
    MODIFY_REG(port_struct->OTYPER, 1 << pin, gpio_mode << pin);
    //��������� �������������� �������
    MODIFY_REG(port_struct->AFR[pin / 8], 0xf << (4 * (pin % 8)), af_number << (4 * (pin % 8)));
    // ���������, ����� ������ ����� � ������������� AF!
    MODIFY_REG(port_struct->MODER, 0x03 << pin_pos, 0x02 << pin_pos);
    return GPIO__EVENT_OK;
}

/*******************************************************************************
 * ������� ������ � NVIC �����������. ����� ��������� ����������,
   � ��� �� ������������� ����������
 ******************************************************************************/
static void NVIC_Control(int gpio) {
    int pin = (gpio & 0x0F);
    if (pin == 0) {
        NVIC_SetPriority(EXTI0_IRQn, 1);
        NVIC_EnableIRQ(EXTI0_IRQn);
    }
    else if (pin == 1) {
        NVIC_SetPriority(EXTI1_IRQn, 1);
        NVIC_EnableIRQ(EXTI1_IRQn);
    }
    else if (pin == 2) {
        NVIC_SetPriority(EXTI2_IRQn, 1);
        NVIC_EnableIRQ(EXTI2_IRQn);
    }
    else if (pin == 3) {
        NVIC_SetPriority(EXTI3_IRQn, 1);
        NVIC_EnableIRQ(EXTI3_IRQn);
    }
    else if (pin == 4) {
        NVIC_SetPriority(EXTI4_IRQn, 1);
        NVIC_EnableIRQ(EXTI4_IRQn);
    }
    else if (((pin >= 5))&&(pin <= 9)){
        NVIC_SetPriority(EXTI9_5_IRQn, 1);
        NVIC_EnableIRQ(EXTI9_5_IRQn);
    }
    else if (((pin >= 10))&&(pin <= 15)){
        NVIC_SetPriority(EXTI15_10_IRQn, 1);
        NVIC_EnableIRQ(EXTI15_10_IRQn);
    }
}

/*******************************************************************************
 * ������� ��������� ����������� ������ ������ �����.
 ******************************************************************************/
void gpio__set(int gpio, gpio__state_e pin_state) {
    int pin_mask = 1<<(gpio & 0x0F);
    int port = gpio >> 4;
    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];  // ��������� ������ ����� � ������

    switch (pin_state) {
        case GPIO__STATE_HIGH:
#if defined(STM32F40_41xxx) || defined(STM32F407xx) || defined(STM32L476xx) || defined(STM32L4R7xx)
            WRITE_REG(port_struct->BSRR, pin_mask);
#endif /* STM32F40_41xxx */
#if defined(STM32L1XX_MD) || defined(STM32L1XX_MDP)
            WRITE_REG(port_struct->BSRRL, pin_mask);
#endif /* STM32L1XX_MD */
            break;
        case GPIO__STATE_LOW:
#if defined(STM32F40_41xxx) || defined(STM32F407xx) || defined(STM32L476xx) || defined(STM32L4R7xx)
            WRITE_REG(port_struct->BSRR, pin_mask << 16);
#endif /* STM32F40_41xxx */
#if defined(STM32L1XX_MD) || defined(STM32L1XX_MDP)
            WRITE_REG(port_struct->BSRRH, pin_mask);
#endif /* STM32F40_41xxx */
            break;
        case GPIO__STATE_TOGGLE:
            if ((port_struct->ODR & pin_mask) !=  pin_mask)   // ���� ��� 0, �� ����������, �����- ����������
                #if defined(STM32F40_41xxx) || defined(STM32F407xx) || defined(STM32L476xx) || defined(STM32L4R7xx)
                WRITE_REG(port_struct->BSRR, pin_mask);
                #endif /* STM32F40_41xxx */
                #if defined(STM32L1XX_MD) || defined(STM32L1XX_MDP)
                WRITE_REG(port_struct->BSRRL, pin_mask);
                #endif /* STM32L1XX_MD */
            else
                #if defined(STM32F40_41xxx) || defined(STM32F407xx) || defined(STM32L476xx)  || defined(STM32L4R7xx)
                WRITE_REG(port_struct->BSRR, pin_mask << 16);
                #endif /* STM32F40_41xxx */
                #if defined(STM32L1XX_MD) || defined(STM32L1XX_MDP)
                WRITE_REG(port_struct->BSRRH, pin_mask);
                #endif /* STM32L1XX_MD */
            break;
        default:
            break;
    }
}

/*******************************************************************************
 * ������� ���������� ����������� ������ ���������� ������ �����.
 ******************************************************************************/
gpio__state_e gpio__get(int gpio) {
    int pin_mask = 1 << (gpio & 0x0F);
    int port = gpio >> 4;
    GPIO_TypeDef *port_struct = (GPIO_TypeDef*) gpio__mass[port];
    if (READ_BIT(port_struct->IDR, pin_mask) != 0)
        return GPIO__STATE_HIGH;
    else
        return GPIO__STATE_LOW;
}

/*******************************************************************************
 * ���������� ���������� ��� ���� ������
 ******************************************************************************/
void gpio__EXTI_IRQHandler(int gpio__exti_port, int gpio__exti_pin) {
    int gpio = (gpio__exti_port << 4) | gpio__exti_pin;
    if (gpio_modul.isr_cb[gpio__exti_pin] != NULL)
        gpio_modul.isr_cb[gpio__exti_pin](gpio, gpio__get(gpio));
}
