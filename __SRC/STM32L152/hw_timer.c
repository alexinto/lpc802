/***************************************************************************//**
 * @file hw_timer.c.
 * @brief ������ ����������� �������.
 * @authors a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "standard.h"
#include "target.h"
#include "stm32l1xx.h"
#include "hw_timer.h"

// ���������� ��������� ������ ����������� �������.
typedef struct {
    sw_timer__sys_time_t cur_time;           // ������� �����.
    sw_timer__sys_time_t time_alrm;          // ����� ��������.
    hw_timer__isr_handler_t isr_handler;     // ��������� �� ����������, ��������� � ���������� ����������� �������.
    sw_timer__event_handler_t event_handler; // ��������� �� ���������� ����������� ��������.
    int init;                                // ���� ����, ��� ������������������ ������ �������.
    int isr_ovf_flag;                        // ���� ����, ��� �� ������� ���� ������� ��������� ���������� ������������ 1 �������.
} hw_timer__t;

static hw_timer__t hw_timer;

/*******************************************************************************
 * ������� ������������� ����������� �������, ������������� ��� ���������� ����������� ��������.
 ******************************************************************************/
sw_timer__event_e hw_timer__init(hw_timer__isr_handler_t isr_handler, sw_timer__event_handler_t event_handler) {
    MODIFY_REG(RCC->APB1ENR, 0, RCC_APB1ENR_PWREN);
    // ������ �������������, ������� �� ���������� (��� ���� ������)
    MODIFY_REG(PWR->CR, 0, PWR_CR_DBP);                     // The DBP bit must be set to enable RTC registers write access.
    RTC->WPR = 0xCA;                                        // The following steps are required to unlock the write protection on
    RTC->WPR = 0x53;                                        // all the RTC registers except for RTC_ISR[13:8], RTC_TAFCR, and RTC_BKPxR.
    MODIFY_REG(RTC->CR, RTC_CR_WUTE | RTC_CR_WUTIE, 0);
    while ((RTC->ISR & RTC_ISR_WUTWF) != RTC_ISR_WUTWF) {;} // ��������� WUTWF ���� ������
    //�������� LSE
    MODIFY_REG(RCC->CSR, 0, RCC_CSR_LSEON | RCC_CSR_LSECSSON |  RCC_CSR_RTCEN);
    //���� ������������ LSE
    while ((RCC->CSR & RCC_CSR_LSERDY) == 0) {;}
    // ������������� ������������ ������ RTC
    MODIFY_REG(RCC->CSR, RCC_CSR_RTCSEL, RCC_CSR_RTCSEL_0);
    // �������� ������ RTC
    MODIFY_REG(RCC->CSR, 0, RCC_CSR_RTCEN);
    MODIFY_REG(RTC->CR, RTC_CR_ALRAE | RTC_CR_ALRAIE | RTC_CR_WUTE | RTC_CR_ALRBE | RTC_CR_ALRBIE, 0);
    while ((RTC->ISR & RTC_ISR_ALRAWF) != RTC_ISR_ALRAWF) {;}
    MODIFY_REG(RTC->ISR, 0, RTC_ISR_INIT);
    while ((RTC->ISR & RTC_ISR_INITF) != RTC_ISR_INITF) {;}
    RTC->PRER = 0x00070fff;
    MODIFY_REG(RTC->ISR, RTC_ISR_INIT | RTC_ISR_WUTF, 0);
    RTC->ALRMAR = 0xffffffff;
    RTC->ALRMASSR = 0x0c000000;         // �������� 13 AN3371
    //����������� 2� ������ �� �������� ������ 0.5 ���.
    RTC->WUTR = 0x1fff;
    hw_timer.isr_ovf_flag = 1;
    // ��������� ������ �� ��������������������� ������ - SHADOW ��������.
    RTC->CR = RTC_CR_BYPSHAD;
    MODIFY_REG(RTC->CR, RTC_CR_WUCKSEL, RTC_CR_WUCKSEL_0 | RTC_CR_WUCKSEL_1);
    // ������������� ����� EXTI 17 ��� ������ ���������� RTC_Alarm
    MODIFY_REG(EXTI->RTSR, 0, 1<<17 | 1<<20);
    MODIFY_REG(EXTI->FTSR, 1<<17 | 1<<20, 0);
    MODIFY_REG(EXTI->IMR, 0, 1<<17 | 1<<20);
    // �������� 2� ������ (������������)
    MODIFY_REG(RTC->CR, 0, RTC_CR_WUTE | RTC_CR_WUTIE);
    NVIC_EnableIRQ(RTC_Alarm_IRQn);
    NVIC_EnableIRQ(RTC_WKUP_IRQn);
    hw_timer.isr_handler = isr_handler;
    hw_timer.event_handler = event_handler;
    hw_timer.cur_time.timestamp = 0;
    hw_timer.cur_time.ms = 0;
    hw_timer.time_alrm.ms = -1;
    hw_timer.init = 1;

    return SW_TIMER__EVENT_OK;
}

/*******************************************************************************
 * ������� �������� ������ ��� ��������� �������� ��������� ������ hw_timer.
 ******************************************************************************/
void hw_timer__cout(void) {

}

/*******************************************************************************
 * ������� ��������� ���������� �������.
 ******************************************************************************/
sw_timer__sys_time_t hw_timer__sys_time_get(void) {
    int time_ss, time_ss_1;
    sw_timer__sys_time_t res;
    CRITICAL_SECTION_ON
        do {
            time_ss = (RTC->SSR&0xfff);
            time_ss_1 = (RTC->SSR&0xfff);
        } while(time_ss != time_ss_1);

        time_ss = 4095 - time_ss;
        time_ss = time_ss >> 2;
        hw_timer.cur_time.ms = time_ss - (time_ss / 42);
        res = hw_timer.cur_time;
        if ((time_ss_1 >= 4095) && (!hw_timer.isr_ovf_flag)) {  // �� �����: ���� time_ss_1 � 4095, �� ����� ��� ��������� ���� �������. � ����� � ���. ������� �� isr_ovf_flag.
            res.timestamp++;
            res.ms = 0;
        }
    CRITICAL_SECTION_OFF
    return  res;
}

/*******************************************************************************
 * ������� ������� ������������ ���������� ������� � �������� �����.
 ******************************************************************************/
void hw_timer__start(sw_timer__sys_time_t time) {
    int time_ss, time_ss_cur, time_ss_cur_1;
    if (time.ms < 0) {  // ���������.
        MODIFY_REG(RTC->CR, RTC_CR_ALRAE | RTC_CR_ALRAIE, 0);
        return;
    }
    hw_timer.time_alrm = time;
    CRITICAL_SECTION_ON
            time_ss = (hw_timer.time_alrm.ms) << 2;
            time_ss = time_ss + (time_ss / 41);
            time_ss = 4095 - time_ss;
            do {
                time_ss_cur = RTC->SSR&0xfff;
                time_ss_cur_1 = RTC->SSR&0xfff;
            }while(time_ss_cur != time_ss_cur_1);

            if (hw_timer.time_alrm.timestamp <= hw_timer.cur_time.timestamp) {
                MODIFY_REG(RTC->CR, RTC_CR_ALRAE, 0);
                if (((time_ss_cur - time_ss) < 4) || (hw_timer.time_alrm.timestamp < hw_timer.cur_time.timestamp)) {
                    if ((time_ss = time_ss_cur - 4) < 0) {
                        hw_timer.time_alrm.timestamp++;
                        time_ss += 4096;
                        hw_timer.time_alrm.ms = 0;
                    }
                }
                RTC->ALRMASSR = time_ss + 0x0c000000;  // �������� 13 AN3371
                MODIFY_REG(RTC->CR, 0, RTC_CR_ALRAE | RTC_CR_ALRAIE);
            }
    CRITICAL_SECTION_OFF
}

/*******************************************************************************
 * �������-���������� ���������� ����������� �������.
 ******************************************************************************/
void hw_timer__RTC_IRQHandler(void) {
    hw_timer.isr_handler();
}

/*******************************************************************************
 * �������-���������� ���������� ����������� ������� �� ������������. ����������
 * ������ 0.5 �������.
 ******************************************************************************/
void hw_timer__RTC_IRQ_OVF_Handler(void) {
    if (!hw_timer.init)
        return;
    if (hw_timer.isr_ovf_flag) {
        hw_timer.isr_ovf_flag = 0;
        return;
    }
    hw_timer.cur_time.timestamp++;
    hw_timer.isr_ovf_flag = 1;

    hw_timer__sys_time_get();   // ������� ������ ����� � ���������� hw_timer.cur_time.

    if (hw_timer.time_alrm.ms < 0) {
        MODIFY_REG(RTC->CR, RTC_CR_ALRAE | RTC_CR_ALRAIE, 0);
        return;
    }
    if (hw_timer.time_alrm.timestamp < hw_timer.cur_time.timestamp)
        hw_timer__RTC_IRQHandler();
    else if (hw_timer.time_alrm.timestamp == hw_timer.cur_time.timestamp) {
        if (hw_timer.time_alrm.ms <= hw_timer.cur_time.ms)
            hw_timer__RTC_IRQHandler();
        else
            hw_timer__start(hw_timer.time_alrm);
    }
}


