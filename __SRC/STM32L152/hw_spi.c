/***************************************************************************//**
* @file test_hw_spi.c.
* @brief ���� supervisor ��� stm32l1.
* @authors s.korotkov.
******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "standard.h"
#include "target.h"
#include "device.h"
#include "hw_spi.h"
#include "stm32l1xx.h"
#ifdef BB_SPI__1
    #include "bb_spi.h"
#endif

static void hw_spi__itf1_gpio_en(void);
static void hw_spi__itf1_gpio_dis(void);
static void hw_spi__itf2_gpio_en(void);
static void hw_spi__itf2_gpio_dis(void);
void hw_spi__SPI_IRQHandler(int spi_id);

// ��������� ���������� SPI.
typedef struct {
    uint32_t settings;                                  // ������� ���������.
    uint8_t *tx_buff;                                   // ��������� �� ����� � ����������������� ������� ��� ��������.
    uint8_t *rx_buff;                                   // ��������� ���������������� ����� ��� ������ ������.
    int len;                                            // ������ ������ � ����������������� ������� ��� �������� � ������.
    int start_deinit;                                   // ���� ��������� �������������� spi
    int start_break;                                    // ���� ��������� ������������ spi
    hw_spi__cb_t rxtx_isr_cb;                           // ��������� �� ���������������� ������� �� ������ ��������� ���������� ���� (�� ����������).
    int tx_bytes_count;                                 // ������� ���������� ������.
    int rx_bytes_count;                                 // ���������� �������� ����.
    int busy_txrx;                                      // ���� ��������� ��������
    int init;                                           // ���� ������������� ����������.
} hw_spi__itf_t;

// ����������� ���������� ������������ SPI-�� � �������.
#if defined (SPI_1) && defined (SPI_2)
    #define HW_SPI__COUNT  2
#elif !defined (SPI_1) && !defined (SPI_2)
    #define HW_SPI__COUNT  0
#else
    #define HW_SPI__COUNT  1
#endif


// ������ ��� ��������� ��������� �� ��������� SPI �� ID.
#define HW_SPI__PTR_FROM_ID_GET(spi_id)   &hw_spi.itf[spi_id - 1]

typedef struct {
    hw_spi__itf_t itf[HW_SPI__COUNT];
} hw_spi__struct;

static hw_spi__struct hw_spi;

// ��������� �������� ������� SPI.
 const SPI_TypeDef* const spi__mass[] = {
    SPI1,
    SPI2,
};

/*******************************************************************************
 *  ������� �������������  SPIx.
 ******************************************************************************/
spi__event_e hw_spi__init (int spi_id, uint32_t settings, cs_handler_t cs_hdl) {
    if (spi_id > MCU__SPI_MAX) {
        #ifdef BB_SPI__1
            return bb_spi__init (spi_id, settings);
        #else
            return SPI__EVENT_PARAM_NA;
        #endif
    }
    // todo Luchnikov: �������� ��� ������ � cs_hdl � ����� �� �� ������.
    if ((settings & SPI__MODE_MASK) == SPI__MODE_SLAVE)
        return SPI__EVENT_PARAM_NA; // todo ���� SLAVE �� ����������.

/* (+) 1. �������� ����� �� ������� ����� SPI
    (+) 2. �������� ������������ SPI
    (+) 3. ���������� ������� (���������� BR)
    (+) 4. ������� ���������� � ���� (POL/PHA)
    (+) 5. ������� ������ ������ 8/16 ���
    (+) 6. ������� MSB/LSB
    (+) 7. ��������� NSS/CS (�������� SSM/SSI)
    (+) 8. ��������� TI �������� (FRF ���)
    (+) 9. ��������� MASTER
    (+) 10. �������� SPI (SPE)
    (+) 11. ��������� ��� MODF � SR �������� */

    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);
    if (spi->settings == settings)
        return SPI__EVENT_OK;

    spi->settings = 0; // ���� ����� ����� ������, �� ������� ��������� ������ ����������� - ����� ����� ������ ��������� �������.
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id - 1];

    switch(spi_id) {
        case SPI_1:
            hw_spi__itf1_gpio_en();
            NVIC_EnableIRQ(SPI1_IRQn);
            break;
        case SPI_2:
            hw_spi__itf2_gpio_en();
            NVIC_EnableIRQ(SPI2_IRQn);
            break;
        default:
            return SPI__EVENT_PARAM_NA;
    }
//todo baud rate
    SPI->CR1 &=~  SPI_CR1_BR;
    SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_1| SPI_CR1_BR_0;

//PHASE - POL
    switch (settings & SPI__CPHA_CPOL_MASK) {
        case SPI__CPHA_0_CPOL_0:
            SPI->CR1 &=~ SPI_CR1_CPHA | SPI_CR1_CPOL;
            break;
        case SPI__CPHA_0_CPOL_1:
            SPI->CR1 &=~ SPI_CR1_CPHA;
            SPI->CR1 |= SPI_CR1_CPOL;
            break;
        case SPI__CPHA_1_CPOL_0:
            SPI->CR1 |= SPI_CR1_CPHA;
            SPI->CR1 &=~ SPI_CR1_CPOL;
            break;
        case SPI__CPHA_1_CPOL_1:
            SPI->CR1 |= SPI_CR1_CPHA | SPI_CR1_CPOL;
            break;
        default:
            return SPI__EVENT_PARAM_NA;
    }
//FRAME
    switch (settings & SPI__FRAME_MASK) {
        case SPI__DATA_FRAME_8_BIT:
            SPI->CR1 &=~ SPI_CR1_DFF;
            break;
        case SPI__DATA_FRAME_16_BIT:
            SPI->CR1 |= SPI_CR1_DFF;
            break;
        default:
            return SPI__EVENT_PARAM_NA;
    }
//LSB - MSB
    //todo ����� ���������� enum!!!!!!!!!!!!!!!!!!!!!!!!!!!
    switch (settings & SPI__LSBMSB_MASK) {
        case SPI__MSB:
             SPI->CR1 &=~ SPI_CR1_LSBFIRST;
            break;
        case SPI__LSB:
            SPI->CR1 |= SPI_CR1_LSBFIRST;

            break;
        default:
            return SPI__EVENT_PARAM_NA;
    }
//Software NSS + internal slave select
    SPI->CR1 |= SPI_CR1_SSM | SPI_CR1_SSI;

// MOTOROLLA protocol
    SPI->CR2 &=~ SPI_CR2_FRF;

//MASTER - SLAVE
    switch (settings & SPI__MODE_MASK) {
        case SPI__MODE_SLAVE:
            SPI->CR1 &=~ SPI_CR1_MSTR;
            break;
        case SPI__MODE_MASTER:
            SPI->CR1 |= SPI_CR1_MSTR;
            break;
        default:
            return SPI__EVENT_PARAM_NA;
            break;
    }

        if ((SPI->CR1 & SPI_CR1_SPE) == 0) {   //�������� SPI
        SPI->CR1 |= SPI_CR1_SPE;
}
//�������� �� ������ ��������� ���������� ������
    if ((SPI->SR & SPI_SR_MODF) != 0)
        return SPI__EVENT_ERROR;

    spi->settings = settings;
    spi->init = 1;
    return SPI__EVENT_OK;
}



/***************************************************************************//**
 * ������� ��������������� ���������� SPI. ���������� ���������� ���� �������� � ��������� ���������.
 ******************************************************************************/
spi__event_e hw_spi__deinit(int spi_id) {
    if (spi_id > MCU__SPI_MAX) {
        #ifdef BB_SPI__1
            return bb_spi__deinit (spi_id);
        #else
            return SPI__EVENT_PARAM_NA;
        #endif
    }
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id - 1];
    if (spi->start_deinit)
        return SPI__EVENT_BUSY;

    if (spi->busy_txrx == 0 && (SPI->SR & SPI_SR_BSY) == 0) {
        hw_spi__txrx_break(spi_id); // ������� ���, ����� ��� ������� �� spi->busy_txrx.
        SPI->CR1 &=~ SPI_CR1_SPE;
        switch(spi_id) {
            case SPI_1:
                hw_spi__itf1_gpio_dis();
                NVIC_DisableIRQ(SPI1_IRQn);
                break;
            case SPI_2:
                hw_spi__itf2_gpio_dis();
                NVIC_DisableIRQ(SPI2_IRQn);
                break;
        }
        spi->init = 0;
    }
    else {
        hw_spi__txrx_break(spi_id);
        spi->start_deinit = 1;
    }
    return SPI__EVENT_OK;
}


/***************************************************************************//**
 * ������������� ������ �� SPI.
 ******************************************************************************/
spi__event_e hw_spi__txrx (int spi_id, int cs_id, uint8_t *rx_buff, uint8_t *tx_buff, uint32_t len, int cb_from_isr, hw_spi__cb_t callback){
    if (spi_id > MCU__SPI_MAX) {
        #ifdef BB_SPI__1
            return bb_spi__txrx (spi_id, cs_id, rx_buff, tx_buff, len, callback);
        #else
            return SPI__EVENT_PARAM_NA;
        #endif
    }
    /* ������� ����������� �������� -> ����� �����.

    ������������������ ��������:
    (+) 1. ������ ������������ �� �������� (�������������� CS -> low (��� ������ ������� spi.c)), �������� ����������
    (+) 2. ��������� DR ������ � �������
    (+) 3. � �������� ��������� ���� TXE ����� ���������
    (+) 4. ����� ������������� ���������� ����� ����� �� �������� ����������� (���������� �� TXEIE)

     ������������������ ������:
    (+) 1. ���������� ����������� ������ RX(���� ����� RXNE)
    (+) 2. ���� ���������� �� RXNE
    (+) 3. ������ ������� DR, ��� ����� ������� ���� RXNE */

    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id - 1];
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);

    if ((spi->init == 0) || (len == 0) || (rx_buff == NULL && tx_buff == NULL) || (callback == NULL))
        return SPI__EVENT_PARAM_NA;

    if ( (spi->busy_txrx == 1) || ((SPI->SR & SPI_SR_BSY) == 1))
        return SPI__EVENT_BUSY;
    spi->busy_txrx = 1;

    spi->tx_buff = tx_buff;
    spi->rx_buff = rx_buff;
    spi->len = len;
    spi->rxtx_isr_cb = callback;
    spi->start_break = 0;
    spi->start_deinit = 0;

    spi->tx_bytes_count = 0;
    spi->rx_bytes_count = 0;

    if (spi->tx_buff == NULL) {
        //������ ����� (�������� 0�FF)
        SPI->DR = (uint8_t)0xFF;
    }
    else {
        SPI->DR = (uint8_t)*spi->tx_buff;
    }
    SPI->CR2 |= SPI_CR2_TXEIE | SPI_CR2_RXNEIE | SPI_CR2_ERRIE;
    return SPI__EVENT_OK;
}


/***************************************************************************//**
 * ������� ����������� ���������� �������� �������� ������ ��������� SPI.
 ******************************************************************************/
spi__event_e hw_spi__txrx_break(int spi_id) {
    if (spi_id > MCU__SPI_MAX) {
        #ifdef BB_SPI__1
            return bb_spi__txrx_break(spi_id);
        #else
            return SPI__EVENT_PARAM_NA;
        #endif
    }
    spi__event_e ret = SPI__EVENT_OK;
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);
    CRITICAL_SECTION_ON
        if (spi->start_break)
            ret = SPI__EVENT_BUSY;
        else if (spi->busy_txrx == 0) {
            // ������ �������. ������ ����������.
        }
        else {
            spi->busy_txrx = 0;
            spi->start_break = 1;
        }
    CRITICAL_SECTION_OFF
    return ret;
}

/***************************************************************************//**
 * ������� ��������� �������� ��������� �������� SPI.
 ******************************************************************************/
void hw_spi__cout(void) {
#ifdef BB_SPI__1
    bb_spi__cout();
#endif
    __NOP();
}

/*******************************************************************************
 *  �������-���������� �������� �� SPI.
 ******************************************************************************/
void hw_spi__SPI_IRQHandler(int spi_id) {
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[spi_id - 1];
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);
    spi__event_e code = SPI__EVENT_OK;
    int status_reg = SPI->SR;

    // �������� �� ������
    if (status_reg & SPI_SR_OVR) {
        code = SPI__EVENT_OVERRUN;
        uint8_t temp = SPI->SR;
         temp = SPI->DR;
        SPI->CR2 &= ~(SPI_CR2_RXNEIE | SPI_CR2_TXEIE | SPI_CR2_ERRIE);
        if (spi->busy_txrx) {
            spi->busy_txrx = 0;
            spi->rxtx_isr_cb(spi_id, 0, code, spi->rx_buff, spi->rx_bytes_count);
        }
    }
    else  if((status_reg & SPI_SR_RXNE) && (SPI->CR2 & SPI_CR2_RXNEIE)) { //�����
        // if ����� -> �� ������ � ����� ����������������
        if (spi->busy_txrx && spi->rx_buff != NULL)
            spi->rx_buff[spi->rx_bytes_count] = (uint8_t)SPI->DR;
        else {
            // todo ���������, �� ����� �� ���������� ������� ��� ��� �����������.
            uint8_t temp = READ_REG(SPI->DR);
        }
        spi->rx_bytes_count++;
        if (spi->rx_bytes_count >= spi->len) {
            SPI->CR2 &= ~(SPI_CR2_RXNEIE | SPI_CR2_ERRIE);
            //todo ��������� ��� ���� cs?
            if (spi->busy_txrx) {
                spi->busy_txrx = 0;
//                gpio__set(TARGET__FLASH_CS, GPIO__STATE_HIGH);
                spi->rxtx_isr_cb(spi_id, 0, code, spi->rx_buff, spi->rx_bytes_count);
            }
        }
    }
    else if ((status_reg & SPI_SR_TXE) && (SPI->CR2 & SPI_CR2_TXEIE)) {  //��������
        //�������� �� ��������� spi �������� RM ���.786
        if ((spi->start_break == 1) || (spi->start_deinit == 1)) {
            SPI->CR2 &=~ SPI_CR2_TXEIE;
            return; // ������ ��� ���������� � ���������� �� ������ (��� �� - ���������� ��������).
        }
        spi->tx_bytes_count++;
        if (spi->tx_bytes_count >= spi->len) {
            SPI->CR2 &=~ SPI_CR2_TXEIE;
        }
        else {
            if (spi->tx_buff == NULL)
                SPI->DR = 0xFF;
            else
                SPI->DR = spi->tx_buff[spi->tx_bytes_count];
        }
    }
    if ((spi->start_break == 1) && ((SPI->SR & SPI_SR_BSY) == 0)) {
        spi->start_break = 0;
        spi->start_deinit = 0;
        SPI->CR1 &=~ SPI_CR1_SPE;
        if (spi->start_deinit == 1) {
            switch(spi_id) {
                case SPI_1:
                    hw_spi__itf1_gpio_dis();
                    NVIC_DisableIRQ(SPI1_IRQn);
                    break;
                case SPI_2:
                    hw_spi__itf2_gpio_dis();
                    NVIC_DisableIRQ(SPI2_IRQn);
                    break;
            }
            spi->init = 0;
        }
    }
    if (SPI->SR & SPI_SR_BSY) {
       __NOP(); //todo back to cb
    }
}



/*******************************************************************************
 *  ������� ������������� ������� SPI_1.
 ******************************************************************************/
static void hw_spi__itf1_gpio_en(void) {

    /**SPI1 GPIO Configuration
    PA5     ------> SPI1_SCK
    PA6     ------> SPI1_MISO
    PA12     ------> SPI1_MOSI
    */
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
    GPIOA->MODER &=~ (GPIO_MODER_MODER5_0 | GPIO_MODER_MODER6_0 | GPIO_MODER_MODER12_0);
    GPIOA->MODER |= (GPIO_MODER_MODER5_1 | GPIO_MODER_MODER6_1 | GPIO_MODER_MODER12_1);
    GPIOA->OTYPER &=~ (GPIO_OTYPER_OT_5 | GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_12);
    GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR5 | GPIO_OSPEEDER_OSPEEDR6 | GPIO_OSPEEDER_OSPEEDR12);
    GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR5 | GPIO_PUPDR_PUPDR6 | GPIO_PUPDR_PUPDR12);
    GPIOA->PUPDR |= GPIO_PUPDR_PUPDR6_0; // ����� �� ��������� �������� !
    GPIOA->AFR[0] &=~ (GPIO_AFRL_AFRL5 | GPIO_AFRL_AFRL6);
    GPIOA->AFR[1] &=~ GPIO_AFRH_AFRH12;
    GPIOA->AFR[0] |= 0x05500000;
    GPIOA->AFR[1] |= 0x00050000;
}

/*******************************************************************************
 *  ������� ��-������������� ������� SPI_1.
 ******************************************************************************/
static void hw_spi__itf1_gpio_dis(void) {

    GPIOA->MODER |= (GPIO_MODER_MODER5 | GPIO_MODER_MODER6 | GPIO_MODER_MODER12);
    GPIOA->OTYPER &=~ (GPIO_OTYPER_OT_5 | GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_12);
    GPIOA->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR5 | GPIO_OSPEEDER_OSPEEDR6 | GPIO_OSPEEDER_OSPEEDR12);
    GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR5 | GPIO_PUPDR_PUPDR6 | GPIO_PUPDR_PUPDR12);
    GPIOA->AFR[0] &=~ (GPIO_AFRL_AFRL5 | GPIO_AFRL_AFRL6);
    GPIOA->AFR[1] &=~ GPIO_AFRH_AFRH12;
    RCC->APB2ENR &=~ RCC_APB2ENR_SPI1EN;
}

/*******************************************************************************
 *  ������� ������������� ������� SPI_2.
 ******************************************************************************/
static void hw_spi__itf2_gpio_en(void) {

    /**SPI1 GPIO Configuration
    PB13     ------> SPI2_SCK
    PB14     ------> SPI2_MISO
    PB15     ------> SPI2_MOSI
    */
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
    GPIOB->MODER &=~ (GPIO_MODER_MODER13_0 | GPIO_MODER_MODER14_0 | GPIO_MODER_MODER15_0);
    GPIOB->MODER |= (GPIO_MODER_MODER13_1 | GPIO_MODER_MODER14_1 | GPIO_MODER_MODER15_1);
    GPIOB->OTYPER &=~ (GPIO_OTYPER_OT_13 | GPIO_OTYPER_OT_14 | GPIO_OTYPER_OT_15);
    GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR13 | GPIO_OSPEEDER_OSPEEDR14 | GPIO_OSPEEDER_OSPEEDR15);
    GPIOB->PUPDR &=~ (GPIO_PUPDR_PUPDR13 | GPIO_PUPDR_PUPDR14 | GPIO_PUPDR_PUPDR15);
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR14_0; // ����� �� ��������� �������� !
    GPIOB->AFR[1] &=~ (GPIO_AFRH_AFRH13 | GPIO_AFRH_AFRH14 | GPIO_AFRH_AFRH15);
    GPIOB->AFR[1] |= 0x55500000;

}

/*******************************************************************************
 *  ������� ��-������������� ������� SPI_1.
 ******************************************************************************/
static void hw_spi__itf2_gpio_dis(void) {

    GPIOB->MODER |= (GPIO_MODER_MODER13 | GPIO_MODER_MODER14 | GPIO_MODER_MODER15);
    GPIOB->OTYPER &=~ (GPIO_OTYPER_OT_13 | GPIO_OTYPER_OT_14 | GPIO_OTYPER_OT_15);
    GPIOB->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR13 | GPIO_OSPEEDER_OSPEEDR14 | GPIO_OSPEEDER_OSPEEDR15);
    GPIOB->PUPDR &=~ (GPIO_PUPDR_PUPDR13 | GPIO_PUPDR_PUPDR14 | GPIO_PUPDR_PUPDR15);
    GPIOB->AFR[1] &=~ (GPIO_AFRH_AFRH13 | GPIO_AFRH_AFRH14 | GPIO_AFRH_AFRH15);
    RCC->APB1ENR &=~ RCC_APB1ENR_SPI2EN;
}
