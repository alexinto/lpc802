#include "target.h"
#include "stddef.h"
#include "ssd1306.h"
#include "i2c.h"
#include "fonts.h"

#ifndef SSD1306_I2C
#error "Must be define SSD1306_I2C in target.h !!!"
#endif
#ifndef SSD1306_SDA
#error "Must be define SSD1306_SDA in target.h !!!"
#endif
#ifndef SSD1306_SCL
#error "Must be define SSD1306_SCL in target.h !!!"
#endif


/* Private SSD1306 structure */
typedef struct {
    uint8_t big_font : 1;
    ssd1306_oper_e Oper;
    uint8_t cmd_buff[2], buffer[129], big_buff[129];
    uint8_t byte_cnt, buff_size, coor_x, coor_y;
} ssd1306_t;

/* Private variable */
static ssd1306_t ssd1306 = {.cmd_buff[0] = 0x00,
                            .buffer[0] = 0x40,
                            .big_buff[0] = 0x40,
                            .big_font = 0};

const uint8_t ssd1306_init_buff[] = {0xAE, 0x20, 0x10, 0xB0, 0xC8, 0x00, 0x10, 0x40, 0x81, 0xFF, 0xA1, 0xA6, 0xA8, 0x3F, 0xA4, 0xD3, 0x00, 0xD5, 0xF0, 0xD9, 0x22, 0xDA, 0x12, 0xDB, 0x20, 0x8D, 0x14, 0xAF};
static void ssd1306__cb(int i2c_id, i2c__event_e event, int addr, uint8_t *buff, int len);
static uint8_t stretch_byte(uint8_t byte);

void ssd1306_init(void) {
    ssd1306.Oper = NO_INIT;
    /* Init I2C */
    i2c__init(SSD1306_I2C, SSD1306_SDA | SSD1306_SCL, NULL, NULL);
    ssd1306.byte_cnt = 0;
    ssd1306.cmd_buff[1] = *ssd1306_init_buff;
    i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.cmd_buff, 2, ssd1306__cb);
}

static void ssd1306__cb(int i2c_id, i2c__event_e event, int addr, uint8_t *buffer, int len) {
    if (event != I2C__EVENT_OK) {
        ssd1306_init();
        return;
    }
    switch(ssd1306.Oper) {
    case NO_INIT:
        if (ssd1306.cmd_buff[1] != 0xAF){
            ssd1306.byte_cnt++;
            ssd1306.cmd_buff[1] = *(ssd1306_init_buff + ssd1306.byte_cnt);
            i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.cmd_buff, 2, ssd1306__cb);
        }
        else {
            ssd1306.Oper = IDLE;
            ssd1306_clr();
        }
        break;
    case CLEAR:
        if (ssd1306.byte_cnt++ > 7) {
            ssd1306.Oper = IDLE;
            ssd1306_putc("", 0, 0, SMALL_SIZE);
        }
        else
            i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.buffer, 129, ssd1306__cb);
        break;
    case SET_CURSOR:
        ssd1306.cmd_buff[1] = 0x00 + (ssd1306.coor_x & 0x0f);
        ssd1306.Oper = SET_CURSOR_X;
        i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.cmd_buff, 2, ssd1306__cb);
        break;
    case SET_CURSOR_X:
        ssd1306.byte_cnt = ssd1306.coor_x >> 4;
        ssd1306.cmd_buff[1] = 0x10 + ssd1306.byte_cnt;
        if (ssd1306.big_font)
           ssd1306.Oper = WRITE_BIG;
        else
            ssd1306.Oper = WRITE_STR;
        i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.cmd_buff, 2, ssd1306__cb);
        break;
    case WRITE_BIG:
        ssd1306.Oper = WRITE_STR;
        if (ssd1306.buff_size)
            i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.big_buff, ssd1306.buff_size + 2, ssd1306__cb);
        else
            ssd1306.Oper = IDLE;
        break;
    case WRITE_STR:
        if (ssd1306.big_font) {
            ssd1306.Oper = SET_CURSOR;
            ssd1306.big_font = 0;
            ssd1306.cmd_buff[1] = 0xB0 + ssd1306.coor_y;
            i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.cmd_buff, 2, ssd1306__cb);
            return;
        }
        ssd1306.Oper = WRITE;
        if (ssd1306.buff_size)
            i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.buffer, ssd1306.buff_size + 2, ssd1306__cb);
        else
            ssd1306.Oper = IDLE;
        break;
    default:
        ssd1306.Oper = IDLE;
    case IDLE:
        break;
    }
}

ssd1306_event_e ssd1306_clr(void) {
    if (ssd1306.Oper != IDLE)
        return BUSY;
    ssd1306.Oper = CLEAR;
    ssd1306.byte_cnt = 0;
    for (int x = 1; x <= 128; x++)
        ssd1306.buffer[x] = 0x00;
    i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.buffer, 129, ssd1306__cb);
    return OK;
}

static uint8_t stretch_byte(uint8_t byte) {
    uint8_t res = 0;
    for (int x  = 0; x < 4; x++)
        if (byte & (1 << x))
            res |= 0x03 << (x << 1);
    return res;
}

ssd1306_event_e ssd1306_putc(uint8_t* string, uint8_t xc, uint8_t yc, font_size_e size) {
    uint8_t* buff, ch, cnt, cnt_big, ch_str;
    if (ssd1306.Oper != IDLE)
        return BUSY;
    cnt = 0;
    ssd1306.big_font = size;
    for (int y = 0; (y < (18 >> size)) && ((ch = *(string + y)) >= 0x20); y++) {
        buff = (uint8_t*)font_8x4.data + ((ch - 0x20) * 5);
        for (int x = 0; x < 5; x++) {
            cnt_big = ((cnt + x) << size) + 1;
            if (size) {
                ch_str = stretch_byte(*(buff + x) >> 4);
                ssd1306.big_buff[cnt_big] = ch_str;
                ssd1306.big_buff[cnt_big + size] = ch_str;
                ch_str = stretch_byte(*(buff + x));
                ssd1306.buffer[cnt_big + size] = ch_str;
            }
            else
                ch_str = *(buff + x);
            ssd1306.buffer[cnt_big] = ch_str;
        }
        ssd1306.buffer[cnt_big + 1 + size] = 0x00;
        ssd1306.buffer[cnt_big + 2 + size] = 0x00;
        ssd1306.big_buff[cnt_big + 1 + size] = 0x00;
        ssd1306.big_buff[cnt_big + 2 + size] = 0x00;
        cnt += 7;
    }
    ssd1306.buff_size = cnt << size;
    ssd1306.Oper = SET_CURSOR;
    yc &= 0x07;
    yc <<= size;
    xc <<= size;
    ssd1306.coor_x = xc * 6;
    ssd1306.coor_y = yc;
    ssd1306.cmd_buff[1] = 0xB0 + yc + size;
    i2c__tx(SSD1306_I2C, SSD1306_I2C_ADDR, ssd1306.cmd_buff, 2, ssd1306__cb);
    return OK;
}

