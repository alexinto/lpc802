﻿/***************************************************************************//**
 * @file mb_rtu.c.
 * @brief Функции работы с пакетами Modbus RTU.
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include <string.h>
#include "System/fstandard.h"
#include "System/sw_timer.h"
#include "System/crc.h"
#include "MODBUS/mb_rtu.h"


#ifndef MB_RTU__TIMEOUT
#define MB_RTU__TIMEOUT 1000             // таймаут ожидания ответа
#endif

#ifndef MB_RTU__COUNT
#define MB_RTU__COUNT 1
#endif

#ifndef MB_RTU__BUFF_SIZE
#define MB_RTU__BUFF_SIZE 253
#endif

#pragma pack(push, 1)

typedef struct {
    uint8_t addr;
    uint8_t func;
    uint8_t reg_hi;
    uint8_t reg_lo;
    union {
        uint8_t reg_cnt[2];
        uint16_t reg_data;
    };
    uint16_t crc;
}mb_rtu__req_t;

typedef struct {
    uint8_t addr;
    uint8_t func;
    union {
        uint8_t len;
        uint8_t exception;
    };
    uint8_t buff[MB_RTU__BUFF_SIZE];
}mb_rtu__resp_t;

#pragma pack(pop)

typedef struct {
    uint8_t init;
    uint8_t addr;
    mb_rtu__oper_e oper;
    mb_rtu__req_t request;
    mb_rtu__resp_t response;
    sw_timer__t tmr;
    uart__sub_t uart;
    mb_rtu__cb_t cb;
    void* ext_data;
}mb_rtu__struct_t;


static mb_rtu__struct_t mb_rtu__data[MB_RTU__COUNT];


static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data);
static void uart_rx_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void uart_tx_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void uart_answer_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data);
static void mb_rtu__tmr_cb(struct sw_timer__t *timer, void *ext_data);
static events__e mb_rtu__resp_parse(mb_rtu__struct_t* d);
static events__e mb_rtu__req_parse(mb_rtu__struct_t* d);

events__e mb_rtu__init(u8 id, u8 addr, int uart_id, uint32_t uart_settings, mb_rtu__cb_t cb, void* ext_data) {
    if (id >= MB_RTU__COUNT)
        return EVENT__PARAM_NA;
    mb_rtu__struct_t* d = &mb_rtu__data[id];
    d->oper = MB_RTU__OP_NONE;
    d->addr = addr;
    d->cb = cb;
    d->ext_data = ext_data;
    return uart__open(&d->uart, uart_id, uart_settings, uart_event_hd, d);
}

events__e mb_rtu__cmd(u8 id, mb_rtu__cmd_e cmd) {
    if (id >= MB_RTU__COUNT)
        return EVENT__PARAM_NA;
    mb_rtu__struct_t* d = &mb_rtu__data[id];
    d->init = (cmd == MB_RTU__CMD_START) ? 1 : 0;
    return EVENT__OK;
}


events__e mb_rtu__deinit(u8 id) {
    if (id >= MB_RTU__COUNT)
        return EVENT__PARAM_NA;
    mb_rtu__struct_t* d = &mb_rtu__data[id];
    return uart__close(&d->uart);
}

events__e mb_rtu__request(u8 id, uint8_t addr, mb_rtu__oper_e oper, u16 reg, u16 reg_cnt, mb_rtu__cb_t cb, void* ext_data) {
    if (id >= MB_RTU__COUNT)
        return EVENT__PARAM_NA;
    mb_rtu__struct_t* d = &mb_rtu__data[id];
    if (d->oper != MB_RTU__OP_NONE)
        return EVENT__BUSY;
    d->oper = oper;
    d->cb = cb;
    d->ext_data = ext_data;
    d->request.addr = addr;
    d->request.func = oper;
    d->request.reg_hi = reg >> 8;
    d->request.reg_lo = reg;
    d->request.reg_cnt[0] = reg_cnt >> 8;
    d->request.reg_cnt[1] = reg_cnt;
    uint16_t reg_data;
    if (oper == MB_RTU__OP_WR_AO) {
         mb_reg__get(d - mb_rtu__data, MB_REG__TYPE_AO, reg, &reg_data);
        d->request.reg_data = (reg_data >> 8) | (reg_data << 8);
    }
    else if (oper == MB_RTU__OP_WR_DO) {
        mb_reg__get(d - mb_rtu__data, MB_REG__TYPE_DO, reg, &reg_data);
        d->request.reg_data = reg_data ? 0x00FF : 0;
    }
    d->request.crc = crc__16((uint8_t*)&d->request, sizeof(d->request) - 2);     // 2 - размер crc16
    sw_timer__start(&d->tmr, MB_RTU__TIMEOUT, mb_rtu__tmr_cb, d);
    uart__tx(&d->uart, (uint8_t*)&d->request, sizeof(d->request), uart_tx_cb, d);
    return EVENT__OK;
}


static void uart_event_hd(struct uart__sub_t* sub, events__e event, void* ext_data) {
    mb_rtu__struct_t* d = (mb_rtu__struct_t*)ext_data;
    switch(event) {
    case EVENT__OPEN:
        d->init = 1;
        event = uart__rx(sub, UART__FRAME_3_5, (uint8_t*)&d->response, sizeof(d->response), 0, uart_rx_cb, d);
        break;
    case EVENT__CLOSE:
        d->init = 0;
    default:
        break;
    }
    if (d->cb)
        d->cb(d - mb_rtu__data, event, MB_RTU__EX_NONE, d->ext_data);
}

static void uart_rx_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    mb_rtu__struct_t* d = (mb_rtu__struct_t*)ext_data;
    mb_rtu__ex_e exception = MB_RTU__EX_NONE;
    if ((event == EVENT__OK) && (!crc__16(buff, len)) && (d->init)) {
        if (d->oper != MB_RTU__OP_NONE) {                  // Пришел ответ
            if (d->response.addr == d->request.addr) {     // Нам пришел то хоть?
                if (d->response.func & 0x80) {             // Пришел exception ?
                    exception = (d->response.exception < MB_RTU__EX_UNCKNOWN) ? (mb_rtu__ex_e)d->response.exception : MB_RTU__EX_UNCKNOWN;
                    event = EVENT__ERROR;
                }
                else
                    event = mb_rtu__resp_parse(d);
                sw_timer__stop(&d->tmr);
                d->oper = MB_RTU__OP_NONE;
                if (d->cb)
                    d->cb(d - mb_rtu__data, event, exception, d->ext_data);
            }
        }
        else if ((!d->response.addr) || (d->response.addr == d->addr)) { // Возможно нам запрос пришел
            event  = mb_rtu__req_parse(d);
            if (d->response.addr) {
                if (event == EVENT__OK)     // Если отправляем ответ
                    len  = d->response.len + sizeof(mb_rtu__resp_t) - MB_RTU__BUFF_SIZE + 2;
                else if (event != EVENT__EXECUTE)
                    len = 5;       // размер пакета Exception
               if (uart__tx(sub, buff, len, uart_answer_cb, d) == EVENT__OK) // todo Доработать!!! Костыль эхо-тест
                   return;
            }
        }
    }
    uart__rx(sub, UART__FRAME_3_5, (uint8_t*)&d->response, sizeof(d->response), 0, uart_rx_cb, d);
}

static void uart_answer_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    mb_rtu__struct_t* d = (mb_rtu__struct_t*)ext_data;
    uart__rx(sub, UART__FRAME_3_5, (uint8_t*)&d->response, sizeof(d->response), 0, uart_rx_cb, ext_data);
}

static void uart_tx_cb(uart__sub_t* sub, events__e event, u8* buff, int len, void* ext_data) {
    mb_rtu__struct_t* d = (mb_rtu__struct_t*)ext_data;
    if (event != EVENT__OK) {
        sw_timer__stop(&d->tmr);
        d->oper = MB_RTU__OP_NONE;
        if (d->cb)
            d->cb(d - mb_rtu__data, event, MB_RTU__EX_NONE, d->ext_data);
    }
}

static void mb_rtu__tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    mb_rtu__struct_t* d = (mb_rtu__struct_t*)ext_data;
    d->oper = MB_RTU__OP_NONE;
    if (d->cb)
        d->cb(d - mb_rtu__data, EVENT__TIMEOUT, MB_RTU__EX_NONE, d->ext_data);
}


static events__e mb_rtu__resp_parse(mb_rtu__struct_t* d) {
    mb_rtu__req_t* req = &d->request;
    mb_rtu__resp_t* resp = &d->response;
    uint8_t* buff = resp->buff;
    if (req->func != resp->func)
        return EVENT__ERROR;
    events__e res = EVENT__OK;
    uint16_t reg_addr = ((uint16_t)req->reg_hi << 8) | req->reg_lo;
    uint16_t addr_end = reg_addr + ((req->reg_cnt[0] << 8) | req->reg_cnt[1]);
    mb_reg__type_e a_type = MB_REG__TYPE_AO;
    mb_reg__type_e d_type = MB_REG__TYPE_DO;
    switch(resp->func) {
    case MB_RTU__OP_RD_DI:
        d_type = MB_REG__TYPE_DI;
    case MB_RTU__OP_RD_DO:
        if ((addr_end - reg_addr) > (resp->len * 8)) {
            res = EVENT__ERROR;
            break;
        }
        for(int i = 0; reg_addr < addr_end; i++) {
            if (i > 7) {
                i = 0;
                buff++;
            }
            res = mb_reg__set(d - mb_rtu__data, d_type, reg_addr, (*buff >> i) & 0x01);
            reg_addr++;
        }
        break;
    case MB_RTU__OP_WR_DO:
        break;
    case MB_RTU__OP_RD_AI:
        a_type = MB_REG__TYPE_AI;
    case MB_RTU__OP_RD_AO:
        for(int i = 0; (i < resp->len) && (res == EVENT__OK); i += 2, reg_addr++) {       // 2 - размер регистра, байт
            res = mb_reg__set(d - mb_rtu__data, a_type, reg_addr, ((u16)resp->buff[i] << 8) | resp->buff[i + 1]);
        }
        break;
    case MB_RTU__OP_WR_AO:
        break;
    default:
        res = EVENT__ERROR;
        break;
    }
    return res;
}

static events__e mb_rtu__req_parse(mb_rtu__struct_t* d) {
    events__e res = EVENT__OK;
    mb_rtu__req_t* req = (mb_rtu__req_t*)&d->response;
    mb_rtu__resp_t* resp = &d->response;
    uint8_t* buff = resp->buff;
    uint16_t reg_addr = ((uint16_t)req->reg_hi << 8) | req->reg_lo;
    uint16_t reg_cnt = ((uint16_t)req->reg_cnt[0] << 8) | req->reg_cnt[1];
    uint16_t addr_end = reg_addr + reg_cnt;
    uint16_t data, crc;
    mb_rtu__ex_e exception = MB_RTU__EX_ADDR;
    mb_reg__type_e a_type = MB_REG__TYPE_AO;
    mb_reg__type_e d_type = MB_REG__TYPE_DO;
    switch(req->func) {
    case MB_RTU__OP_RD_DI:
        d_type = MB_REG__TYPE_DI;
    case MB_RTU__OP_RD_DO:
        if (reg_cnt >= (MB_RTU__BUFF_SIZE * 8)) {
            res = EVENT__ERROR;
            break;
        }
        *buff = 0;
        for(int i = 0; ((reg_addr < addr_end) && (res == EVENT__OK)); i++) {
            if (i > 7) {
                buff++;
                *buff = i = 0;
            }
            res = mb_reg__get(d - mb_rtu__data, d_type, reg_addr, &data);
            *buff |= data << i;
            reg_addr++;
        }
        resp->len = buff - resp->buff + 1;
        break;
    case MB_RTU__OP_WR_DO:
       if ((res = mb_reg__set(d - mb_rtu__data, MB_REG__TYPE_DO, reg_addr, req->reg_data ? 1 : 0)) == EVENT__OK)
            res = EVENT__EXECUTE;
        break;
    case MB_RTU__OP_RD_AI:
        a_type = MB_REG__TYPE_AI;
    case MB_RTU__OP_RD_AO:
        if (reg_cnt > (MB_RTU__BUFF_SIZE / 2)) {
            res = EVENT__ERROR;
            break;
        }
        for(int i = 0; ((reg_addr < addr_end) && (res == EVENT__OK)); i += 2, reg_addr++) {       // 2 - размер регистра, байт
            res = mb_reg__get(d - mb_rtu__data, a_type, reg_addr, &data);
            resp->buff[i] = data >> 8;
            resp->buff[i + 1] = data;
        }
        resp->len = reg_cnt * 2;
        break;
    case MB_RTU__OP_WR_AO:
        if ((res = mb_reg__set(d - mb_rtu__data, MB_REG__TYPE_AO, reg_addr, (req->reg_data << 8) | (req->reg_data >> 8))) == EVENT__OK)
            res = EVENT__EXECUTE;
        break;
    default:
        res = EVENT__ERROR;
        exception = MB_RTU__EX_FUNC;
        break;
    }
    if (res == EVENT__OK) {
        crc = crc__16((uint8_t*)resp, 3 + resp->len);
        resp->buff[resp->len] = crc;
        resp->buff[resp->len + 1] = crc >> 8;
    }
    else if (res != EVENT__EXECUTE) {
        resp->func |= 0x80;
        resp->exception = exception;
        crc = crc__16((uint8_t*)resp, sizeof(mb_rtu__resp_t) - sizeof(resp->buff));
        resp->buff[0] = crc;
        resp->buff[1] = crc >> 8;
    }
    return res;
}








