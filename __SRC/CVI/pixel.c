/***************************************************************************//**
 * @file pixel.c.
 * @brief Модуль захвата видео с экрана.
 * @author a.tushentsov
 ******************************************************************************/
#include <windows.h>
#include <ansi_c.h>
#include <userint.h>
#include "target.h"
#include "CVI/pixel.h"
#include "System/list.h"

typedef struct {
	list__item_t item;
	int handle;
	char* buff;
	HDC screen;
	HDC mirror;
	BITMAPINFO descr;
	HBITMAP space;
	int x, y;
}pixel__image_struct_t;

static int pixel__handle;
static list__item_t pixel__list = {&pixel__list, &pixel__list};

static pixel__image_struct_t* pixel__find(int handle);

int pixel__create(int handle, int x, int y, int width, int height) {
	pixel__image_struct_t* old_item = pixel__find(handle), *new_item = NULL;
	if (!old_item) {
		if ((new_item = malloc(sizeof(pixel__image_struct_t))) == NULL)
			return 0;
		CRITICAL_SECTION_ON
		new_item->handle = ++pixel__handle;
		CRITICAL_SECTION_OFF
		old_item = new_item;
	}
	old_item->screen = GetDC(NULL);          //1. Создаешь/получаешь контекст экрана (GetDC)
	old_item->mirror = CreateCompatibleDC(old_item->screen);  //2. Создаешь совместимый контекст в памяти (CreateCompatibleDC)
	old_item->descr.bmiHeader.biSize = sizeof(old_item->descr.bmiHeader);
	old_item->descr.bmiHeader.biPlanes = 1;
	old_item->descr.bmiHeader.biBitCount = 32;
	old_item->descr.bmiHeader.biCompression = BI_RGB;
	old_item->descr.bmiHeader.biSizeImage = 0; ////--------
	old_item->descr.bmiHeader.biClrUsed = 0;
	old_item->descr.bmiHeader.biClrImportant = 0;
	old_item->descr.bmiHeader.biWidth = width;
	old_item->descr.bmiHeader.biHeight = height;
	old_item->x = x;
	old_item->y = y;
	if(old_item->space)
		DeleteObject(old_item->space);
	old_item->space = CreateDIBSection(old_item->screen, &old_item->descr, DIB_RGB_COLORS, (VOID**)&old_item->buff, NULL, 0);
	SelectObject(old_item->mirror, old_item->space);   //4. Выбираешь битмап в контекст в памяти (SelectObject)
	if (new_item)
		list__include(&pixel__list, new_item, LIST__TAIL);
	return old_item->handle;
}

events__e pixel__scan(int handle) {
	pixel__image_struct_t* cur_item = pixel__find(handle);
	if (!cur_item)
		return EVENT__ERROR;
	BitBlt(cur_item->mirror, 0, 0, cur_item->descr.bmiHeader.biWidth, cur_item->descr.bmiHeader.biHeight, cur_item->screen, cur_item->x, cur_item->y, SRCCOPY | CAPTUREBLT);

	return EVENT__OK;
}

events__e pixel__draw(int handle, int x, int y) {
	pixel__image_struct_t* cur_item = pixel__find(handle);
	if (!cur_item)
		return EVENT__ERROR;
	HWND hwnd;
//	hwnd = GetDesktopWindow ();
//	BOOL res = SetWindowPos(hwnd,hwnd,0,0,100,100, 0);
	hwnd = WindowFromDC(cur_item->screen);
//	LockWindowUpdate (0);

	BitBlt(cur_item->screen, x, y, cur_item->descr.bmiHeader.biWidth, cur_item->descr.bmiHeader.biHeight, cur_item->mirror, 0, 0, SRCCOPY | CAPTUREBLT);
//	LockWindowUpdate (hwnd);
	return EVENT__OK;
}

static pixel__image_struct_t* pixel__find(int handle) {
	pixel__image_struct_t* cur_item = list__foreach(&pixel__list, NULL, LIST__HEAD);
	while(cur_item) {
		if (cur_item->handle == handle)
			break;
		cur_item = list__foreach(&pixel__list, cur_item, LIST__HEAD);
	}
	return cur_item;
}

events__e pixel__save(int handle, char* filename) {
	pixel__image_struct_t* cur_item = pixel__find(handle);
	if (!cur_item)
		return EVENT__ERROR;
	BITMAP bmpScreen;
	BITMAPFILEHEADER   bmfHeader;
	char* lpbitmap = NULL;
	HANDLE hDIB = NULL;
	GetObject(cur_item->space, sizeof(BITMAP), &bmpScreen);
	int dwBmpSize = ((bmpScreen.bmWidth * cur_item->descr.bmiHeader.biBitCount + 31) / 32) * 4 * bmpScreen.bmHeight;
	hDIB = malloc(dwBmpSize);
	lpbitmap = (char*)GlobalLock(hDIB);
	GetDIBits(cur_item->screen, cur_item->space, 0, bmpScreen.bmHeight, lpbitmap, &cur_item->descr, DIB_RGB_COLORS);
	bmfHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	int dwSizeofDIB = dwBmpSize + bmfHeader.bfOffBits;
	bmfHeader.bfSize = dwSizeofDIB;
	bmfHeader.bfType = 0x4D42; // BM.
	FILE* f_out = fopen (filename, "wb");
	char* buff = (char*)&bmfHeader;
	for(int i = 0; i < sizeof(bmfHeader); i++)
		putc(buff[i], f_out);
	buff = (char*)&cur_item->descr;
	for(int i = 0; i < sizeof(BITMAPINFOHEADER); i++)
		putc(buff[i], f_out);
	for(int i = 0; i < dwBmpSize; i++)
		putc(lpbitmap[i], f_out);
	fflush(f_out);
	fclose(f_out);
	return EVENT__OK;
}


int pixel__load(char* filename) {
	pixel__image_struct_t* new_item;
	FILE* f_in = fopen (filename, "rb");
	if (f_in == NULL)
		return 0;
	if ((new_item = malloc(sizeof(pixel__image_struct_t))) == NULL)
		return 0;
	CRITICAL_SECTION_ON
	new_item->handle = ++pixel__handle;
	CRITICAL_SECTION_OFF
	BITMAPFILEHEADER   bmfHeader;
	char* lpbitmap = NULL;
	char* buff = (char*)&bmfHeader;
	for(int i = 0; i < sizeof(bmfHeader); i++)
		buff[i] = (char)getc(f_in);
	buff = (char*)&new_item->descr;
	for(int i = 0; i < sizeof(BITMAPINFOHEADER); i++)
		buff[i] = (char)getc(f_in);
	int dwBmpSize = bmfHeader.bfSize - bmfHeader.bfOffBits;
	lpbitmap = malloc(dwBmpSize);
	for(int i = 0; i < dwBmpSize; i++)
		lpbitmap[i] = (char)getc(f_in);
	fclose(f_in);
//	new_item->screen = GetDC(NULL);          //1. Создаешь/получаешь контекст экрана (GetDC)

	HRGN region;
	region  = CreateRectRgn(0, 0, 100, 100);
	new_item->screen = GetDCEx(NULL, region, DCX_LOCKWINDOWUPDATE);          //1. Создаешь/получаешь контекст экрана (GetDC)
/*
	HWND hwnd;
	hwnd = WindowFromDC(new_item->screen);
	ValidateRgn(hwnd, region);
	SelectClipRgn(new_item->screen, region);
*/
	new_item->mirror = CreateCompatibleDC(new_item->screen);  //2. Создаешь совместимый контекст в памяти (CreateCompatibleDC)
	new_item->space = CreateDIBSection(new_item->screen, &new_item->descr, DIB_RGB_COLORS, (VOID**)&new_item->buff, NULL, 0);
	SelectObject(new_item->mirror, new_item->space);   //4. Выбираешь битмап в контекст в памяти (SelectObject)
	memcpy(new_item->buff, lpbitmap, dwBmpSize);
	free(lpbitmap);
	list__include(&pixel__list, new_item, LIST__TAIL);
	return new_item->handle;
}

int pixel__cmp(pixel__t* pix1, pixel__t* pix2, uint8_t acc, uint8_t gyst) {
	int c1[] = {pix1->r, pix1->b, pix1->b};
	int c2[] = {pix2->r, pix2->b, pix2->b};
	int res = 0;
	for(; res < 3; res++) {
		c1[res] >>= acc;
		c2[res] >>= acc;
		if ((c1[res] < (c2[res] - gyst)) || (c1[res] > (c2[res] + gyst)))
			break;
	}
	return res < 3 ? 1 : 0;
}

pixel__t pixel__get_color(int handle, uint32_t index) {
	pixel__t pix_null = {0,0,0,0}, *res = &pix_null;
	pixel__image_struct_t* cur_item = pixel__find(handle);
	if (cur_item)
		res = &((pixel__t*)cur_item->buff)[index];
	return *res;
}

