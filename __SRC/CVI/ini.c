#include <windows.h>
#include <ansi_c.h>
#include <userint.h>
#include <inifile.h>
#include "target.h"
#include "System/framework.h"
#include "ini.h"
//************************************************************************
ini__deskriptor_t* ini__create(int root, char* file_or_subkey)
{
    ini__deskriptor_t* d = malloc(sizeof(ini__deskriptor_t));
    if(d == NULL)
        return NULL;
    if((d->ini = Ini_New (1)) == 0)
    {
        free(d);
        return NULL;
    }
    d->root = root;
    strcpy(d->file_or_subkey, file_or_subkey);
    int res;
    CVI__BREAK_ERR_OFF
    if(d->root == 0)
        res = Ini_ReadFromFile(d->ini, d->file_or_subkey);
    else
        res = Ini_ReadFromRegistry(d->ini, d->root, d->file_or_subkey);
    CVI__BREAK_ERR_ON
    return d;
}
//************************************************************************
void ini__close(ini__deskriptor_t* d)
{
    if(d)
    {
        Ini_Dispose (d->ini);
        free(d);
    }
}
//************************************************************************
ini__event_code_e ini__save(ini__deskriptor_t* d)
{
    if(d == NULL) return INI__EVENT_NULL;
    ini__event_code_e ret;
    CVI__BREAK_ERR_OFF
    if(d->root == 0)
        ret = Ini_WriteToFile(d->ini, d->file_or_subkey) == 0? INI__EVENT_OK: INI__EVENT_WRITE;
    else
        ret = Ini_WriteToRegistry(d->ini, d->root, d->file_or_subkey) == 0? INI__EVENT_OK: INI__EVENT_WRITE;
    CVI__BREAK_ERR_ON
    return ret;
}

char* ini__key_get(ini__deskriptor_t* d, char* section, char* key, int key_num) {
	char* name;
	if (Ini_NthItemName(d->ini, section, key_num, &name) <= 0)
		return NULL;
	strcpy(key, name);
    return key;
}


//************************************************************************
char* ini__string_get(ini__deskriptor_t* d, char* section, char* key, char* string, int str_len)
{
	char* name;
    if(d)
    {
		if (key == NULL) {
			if (Ini_NthItemName(d->ini, section, str_len, &name) <= 0)
				return NULL;
			strcpy(string, name);
		}
		else if(Ini_GetStringIntoBuffer (d->ini, section, key, string, str_len) <= 0)
            return NULL;
    }
    return string;
}
//************************************************************************
void ini__string_put(ini__deskriptor_t* d, char* section, char* key, char* string)
{
    if(d)
        Ini_PutString (d->ini, section, key, string);
}
//************************************************************************
int* ini__int_get(ini__deskriptor_t* d, char* section, char* key, int* value)
{
    if(d)
        if (Ini_GetInt(d->ini, section, key, value) <= 0)
			return NULL;
    return value;
}
//************************************************************************
void ini__int_put(ini__deskriptor_t* d, char* section, char* key, int value)
{
    if(d)
        Ini_PutInt(d->ini, section, key, value);
}
//************************************************************************
float ini__float_get(ini__deskriptor_t* d, char* section, char* key, float value)
{
    double f = value;
    if(d)
        Ini_GetDouble(d->ini, section, key, &f);
    return (float)f;
}
//************************************************************************
void ini__float_put(ini__deskriptor_t* d, char* section, char* key, float value)
{
    if(d)
        Ini_PutInt(d->ini, section, key, (double)value);
}
//************************************************************************
void ini__panel_put(ini__deskriptor_t* d, char* section, int panel)
{
    int value;
    GetPanelAttribute (panel, ATTR_WIDTH, &value);
    Ini_PutInt(d->ini, section, "panel_width", value);
    GetPanelAttribute (panel, ATTR_HEIGHT, &value);
    Ini_PutInt(d->ini, section, "panel_height", value);
    GetPanelAttribute (panel, ATTR_LEFT, &value);
    Ini_PutInt(d->ini, section, "panel_left", value);
    GetPanelAttribute (panel, ATTR_TOP, &value);
    Ini_PutInt(d->ini, section, "panel_top", value);

    int ctrl;
    for(GetPanelAttribute (panel, ATTR_PANEL_FIRST_CTRL, &ctrl); ctrl;GetCtrlAttribute(panel, ctrl, ATTR_NEXT_CTRL, &ctrl))
    {
        GetCtrlAttribute(panel, ctrl, ATTR_CTRL_STYLE, &value);
        switch(value)
        {
            case CTRL_TABLE_LS:
            case CTRL_TABLE:
            {
                int cnt = 0;
                GetNumTableColumns(panel, ctrl, &cnt);
                for(int i = 1; i <= cnt; i++)
                {
                    char key[128];
                    sprintf(key,"table.%02d.%02d.width", ctrl,i);
                    GetTableColumnAttribute(panel, ctrl, i, ATTR_COLUMN_WIDTH, &value);
                    Ini_PutInt(d->ini, section, key, value);
                }
                break;
            }
        }
    }
}
//************************************************************************
void ini__panel_get(ini__deskriptor_t* d, char* section, int panel)
{
    int value;
    GetPanelAttribute(panel, ATTR_SIZABLE, &value);
    if(value)
    {
        GetPanelAttribute (panel, ATTR_WIDTH, &value);
        Ini_GetInt(d->ini, section, "panel_width", &value);
        SetPanelAttribute (panel, ATTR_WIDTH, value);

        GetPanelAttribute (panel, ATTR_HEIGHT, &value);
        Ini_GetInt(d->ini, section, "panel_height", &value);
        SetPanelAttribute (panel, ATTR_HEIGHT, value);
    }
    GetPanelAttribute (panel, ATTR_LEFT, &value);
    Ini_GetInt(d->ini, section, "panel_left", &value);
    SetPanelAttribute (panel, ATTR_LEFT, value);

    GetPanelAttribute (panel, ATTR_TOP, &value);
    Ini_GetInt(d->ini, section, "panel_top", &value);
    SetPanelAttribute (panel, ATTR_TOP, value);

    int ctrl;
    for(GetPanelAttribute (panel, ATTR_PANEL_FIRST_CTRL, &ctrl); ctrl;GetCtrlAttribute(panel, ctrl, ATTR_NEXT_CTRL, &ctrl))
    {
        GetCtrlAttribute(panel, ctrl, ATTR_CTRL_STYLE, &value);
        switch(value)
        {
            case CTRL_TABLE_LS:
            case CTRL_TABLE:
            {
                GetCtrlAttribute(panel, ctrl, ATTR_ENABLE_COLUMN_SIZING, &value);
                int cnt = 0;
                GetNumTableColumns(panel, ctrl, &cnt);
                for(int i = 1; value && i <= cnt; i++)
                {
                    char key[128];
                    sprintf(key,"table.%02d.%02d.width", ctrl,i);
                    GetTableColumnAttribute(panel, ctrl, i, ATTR_COLUMN_WIDTH, &value);
                    Ini_GetInt(d->ini, section, key, &value);
                    SetTableColumnAttribute(panel, ctrl, i, ATTR_COLUMN_WIDTH, value);
                }
                break;
            }
        }
    }
}

void ini__remove_section(ini__deskriptor_t* d, char* section) {
    if ((d) && (Ini_SectionExists(d->ini, section)))
        Ini_RemoveSection(d->ini, section);
}

