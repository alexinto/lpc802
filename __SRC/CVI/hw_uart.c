/***************************************************************************//**
 * @file hw_uart.c.
 * @brief  Драйвер uart для CVI.
 * @author a.khamidullin.
 ******************************************************************************/
#include <windows.h>
#include <ansi_c.h>
#include <rs232.h>
#include <string.h>
#include "target.h"
#include "DRV/hw_uart.h"
#include "System/framework.h"
#include "System/sw_timer.h"

// #define DEBUG_HW_UART 1

#define HW_UART_USB__FIFO_SIZE 2048

typedef enum {
    COM_PORT_IDLE    = 0,
    COM_PORT_INIT    = 1,
    COM_PORT_WORK    = 2,
    COM_PORT_DEINIT  = 3,
}usb_fsm_state__e;

// Структура интерфейса UART.
typedef struct {
	u8 init;
	int port_id;
    uart__hw_event_handler_t hw_event_handler;          // Указатель на функцию-обработчик ошибок/событий UART.
    hw_uart__cb_t tx_isr_cb;                            // Указатель на пользовательский коллбэк по передаче (из прерывания).
    uint8_t *tx_buff;                                   // Указатель пользовательский буфер для передачи данных.
    int tx_len;                                         // Размер пользовательского буфера для приема данных (по заполнению вызовется коллбэк).
    hw_uart__cb_t rx_isr_cb;                            // Указатель на пользовательский коллбэк по приему заданного количества байт (из прерывания).
    int rx_len;                                         // Размер пользовательского буфера для приема данных (по заполнению вызовется коллбэк). Оптимально 64 байта.
                                                        // Либо должен быть кратен 64 байтам.
    int rx_bytes_count, save_bytes, offset_bytes;       // Счетчики переданных\принятых байтов.
    events__e tx_event, rx_event;
    u8 rx_buff[HW_UART_USB__FIFO_SIZE];                     // Буфер для приема данных.
    usb_fsm_state__e fsm_state;                         // Автомат состояния интерфейса
} hw_uart__itf_t;


static hw_uart__itf_t uart__data[HW_UART_COUNT];
static framework__sub_t cout_sub;

static int CVICALLBACK com_port__rx_task(void *functionId);

events__e hw_uart__init(int uart_id, uint32_t settings, uart__hw_event_handler_t hw_event_handler) {

    hw_uart__itf_t* uart = &uart__data[uart_id];
	framework__cout_subscribe(&cout_sub, hw_uart__cout);

	int baudrate = UART__BR_115200;
	switch ((settings & UART__BR_MASK) >> UART__BR_SHIFT) {
		case UART__BR_9600: baudrate = 9600; break;
		case UART__BR_19200: baudrate = 19200; break;
		case UART__BR_38400: baudrate = 38400; break;
		case UART__BR_57600: baudrate = 57600; break;
		case UART__BR_115200: baudrate = 115200; break;
		default: baudrate = UART__BR_115200; break;
	}

	int parity = 0;
	switch ((settings & UART__PAR_MASK) >> UART__PAR_SHIFT) {
		case UART__PAR_NONE: parity = 0; break;
		case UART__PAR_ODD: parity = 1; break;
		case UART__PAR_EVEN: parity = 2; break;
		default: parity = 0; break;
	}

	int stop_bits = 1;
	switch ((settings & UART__STOP_MASK) >> UART__STOP_SHIFT) {
		case UART__STOP_1: stop_bits = 1; break;
		case UART__STOP_2: stop_bits = 2; break;
		default: stop_bits = 1; break;
	}

	int bits = 8;
	switch ((settings & UART__DATA_MASK) >> UART__DATA_SHIFT) {
		case UART__DATA_7: bits = 7; break;
		case UART__DATA_8: bits = 8; break;
		default: bits = 8; break;
	}


	char com_str[10];
	sprintf(com_str, "COM%d", uart_id);

	DisableBreakOnLibraryErrors (); // Отключаем вывод ошибок CVI  в отладке...

	int res = OpenComConfig(uart_id, com_str, baudrate, parity, bits, stop_bits, 1024, 512);

	EnableBreakOnLibraryErrors ();

	if (res != 0) {
		return EVENT__ERROR;
	}

	SetComTime(uart_id, 1);

    CRITICAL_SECTION_ON
    uart->rx_bytes_count = uart->save_bytes = 0;
    uart->tx_event = uart->rx_event = EVENT__CONTINUE;
	uart->port_id = uart_id;
	uart->hw_event_handler = hw_event_handler;
	uart->fsm_state = COM_PORT_INIT;
	uart->init = 1;
    CRITICAL_SECTION_OFF

	CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, com_port__rx_task, uart, NULL);

    return EVENT__OK;
}

static int CVICALLBACK com_port__rx_task(void *data) {
	hw_uart__itf_t* uart = (hw_uart__itf_t*)data;
	int res;

	while(uart->init) {
		Sleep(1);

		res = 0;
		if (uart->rx_event == EVENT__CONTINUE) {
			res = ComRd(uart->port_id, (char*)uart->rx_buff, sizeof(uart->rx_buff));
		}

		if (res < 0) {
			printf("COM port read error\r\n");
			break;
		} else if (res > 0 ) {
		    CRITICAL_SECTION_ON
			if (uart->rx_event == EVENT__CONTINUE) {
				uart->rx_bytes_count = res;
				uart->rx_event = EVENT__OK;
			}
		    CRITICAL_SECTION_OFF
#ifdef DEBUG_HW_UART
			printf("COM received %d bytes\r\n", res);
#endif
		}

	}
	uart->port_id = -1;
	return 0;
}

events__e hw_uart__deinit(int uart_id) {
    hw_uart__itf_t* uart = &uart__data[uart_id];
	uart->fsm_state = COM_PORT_DEINIT;
	uart->init = 0;

	int res = CloseCom(uart_id);
	if (res != 0) {
		printf("ERROR: Unable to close COM port\r\n");
		return EVENT__ERROR;
	}
	else {
		return EVENT__OK;
	}
}

/*******************************************************************************
 * Функция отправки пользовательских данных в указанный UART.
 ******************************************************************************/
events__e hw_uart__tx(int uart_id, uint8_t *buff, int len, hw_uart__cb_t tx_isr_cb) {

	hw_uart__itf_t* uart = &uart__data[uart_id];
    uart->tx_buff = buff;
	uart->tx_len = len;
	uart->tx_isr_cb = tx_isr_cb;
	uart->tx_event = EVENT__LOCK;

	int res = ComWrt(uart_id, (char*)uart->tx_buff, uart->tx_len);
#ifdef DEBUG_HW_UART
	printf("COM written %d bytes\r\n", res);
#endif
	if (res < 0) {
		return EVENT__ERROR;
	} else {
		uart->tx_event = EVENT__OK;
		return EVENT__OK;
	}
}

/*******************************************************************************
 * Функция чтения данных из указанного порта.
 ******************************************************************************/
events__e hw_uart__rx(int uart_id, hw_uart__cb_t rx_isr_cb) {

	hw_uart__itf_t* uart = &uart__data[uart_id];
	uart->rx_isr_cb = rx_isr_cb;

	int res = 0;
	if (res < 0) {
		return EVENT__ERROR;
	} else {
		return EVENT__OK;
	}

}


/*******************************************************************************
 * Функция очистки приемного буфера
 ******************************************************************************/
void hw_uart__rx_flush(int uart_id) {
	hw_uart__itf_t* uart = &uart__data[uart_id];

	FlushInQ (uart_id);
    CRITICAL_SECTION_ON
	uart->save_bytes = uart->rx_bytes_count = 0;
	uart->rx_isr_cb = NULL;
	uart->rx_event = EVENT__CONTINUE;
    CRITICAL_SECTION_OFF

}


void hw_uart__cout(void) {
	hw_uart__itf_t* uart;
    int bytes_cnt;
    for(int i = 0; i < HW_UART_COUNT; i++) {
		uart = &uart__data[i];
	    switch(uart->fsm_state) {
	        case COM_PORT_IDLE:
	            break;
	        case COM_PORT_INIT:
                uart->fsm_state = COM_PORT_WORK;
				if (uart->hw_event_handler)
	            	uart->hw_event_handler(i, EVENT__OPEN, NULL);
	            break;
	        case COM_PORT_WORK:
                events__e event = uart->tx_event;
                if ((event != EVENT__CONTINUE) && (event != EVENT__LOCK) && (uart->tx_isr_cb)) {
                    uart->tx_event = EVENT__CONTINUE;
                    uart->tx_isr_cb(uart->port_id, event, uart->tx_buff, uart->tx_len);
                }
                if (((event = uart->rx_event) != EVENT__CONTINUE) && (uart->rx_isr_cb)) {
                    if (uart->save_bytes)
                        bytes_cnt = uart->save_bytes;
                    else {
                        uart->offset_bytes = 0;
                        bytes_cnt = uart->rx_bytes_count;
                        uart->rx_bytes_count = 0;
                    }
                    uart->save_bytes = uart->rx_isr_cb(uart->port_id, event, uart->rx_buff + uart->offset_bytes, bytes_cnt);
                    uart->offset_bytes += bytes_cnt - uart->save_bytes;
                    if (!uart->save_bytes)
                        uart->rx_event = EVENT__CONTINUE;   // Разрешаем вычитывать буфер RX
                }
	            break;
//	        case COM_PORT_DEINIT:
			default:
				if (uart->port_id != -1)    // Ждем остановки потока...
					break;

	            hw_uart__deinit(i);
	            uart->fsm_state = COM_PORT_IDLE;
				if (uart->hw_event_handler)
	            	uart->hw_event_handler(i, EVENT__CLOSE, NULL);
	            break;
	    }
	}
}


