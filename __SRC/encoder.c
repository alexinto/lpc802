/***************************************************************************//**
 * @file encoder.c.
 * @brief ������ ������ � ���������.
 * @author a.tushentsov.
 ******************************************************************************/
#include "mcu.h"
#include "encoder.h"
#include "System/framework.h"

typedef struct {
    u8 init;
    events__e event;
    void* ext_data;
    encoder__cb_t cb;
}encoder__struct_t;

static framework__sub_t cout__sub;

static encoder__struct_t encoder__data[3] = {{.event = EVENT__WAIT}, {.event = EVENT__WAIT}, {.event = EVENT__WAIT}};

static void encoder__cout();

/*******************************************************************************
 * ������� ������������� ��������. ������� �������� ����.
 ******************************************************************************/
void encoder__init(int enc, int min, int max) {
    if (enc == enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    if (enc_ptr->init)
        return;
    framework__cout_subscribe(&cout__sub, encoder__cout);
    NVIC_DisableIRQ(LPTIM1_IRQn);
    MODIFY_REG(RCC->DCKCFGR2, RCC_DCKCFGR2_LPTIM1SEL_Msk, RCC_DCKCFGR2_LPTIM1SEL_1); // HSI clock
    RCC->APB1ENR |= RCC_APB1ENR_LPTIM1EN;
    gpio__af_init(MCU__GPIO_P_D_12, 0, 3);
    gpio__af_init(MCU__GPIO_P_E_1, 0, 3);
    LPTIM1->IER = 0;
    LPTIM1->CFGR = 1 << 24;
    LPTIM1->CR = 1;
    LPTIM1->ARR = 0xFFFF;
    LPTIM1->ICR = 0x7F;
    LPTIM1->CR = 5;
    NVIC_SetPriority(LPTIM1_IRQn, 0);
    NVIC_EnableIRQ(LPTIM1_IRQn);
    enc_ptr->init = 1;
}

/*******************************************************************************
 * ������� ������� �������� � ��������� �� ����� ������ �� �������� ����� �����.
 ******************************************************************************/
events__e encoder__start(int enc, int ticks, encoder__cb_t cb, void* ext_data) {
    if ((enc == enc_none) || (ticks < 0))               // ������������� ��������= ������������� ��������, �������� MOVE_TO_SW1.
        return EVENT__OK;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    events__e res = EVENT__OK;
    CRITICAL_SECTION_ON
    int cur_tick = (LPTIM1->CNT > 32767 ? 0 : LPTIM1->CNT);
    if ((ticks > (cur_tick - ENC_PPR / 20)) && (ticks < (cur_tick + ENC_PPR / 20)))
         res = EVENT__CONTINUE;
    else {
        LPTIM1->CMP = ticks;
        enc_ptr->event = EVENT__WAIT;
        LPTIM1->ICR = 0x7F;
    }
    CRITICAL_SECTION_OFF
    if (res == EVENT__OK) {
        enc_ptr->ext_data = ext_data;
        enc_ptr->cb = cb;
        LPTIM1->IER = 1;
    }
    return res;
}

/*******************************************************************************
 * ������� ��������� ������ �������� �������� ��������. ������� ���������� ����.
 ******************************************************************************/
void encoder__stop(int enc) {
    if (enc == enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    LPTIM1->IER = 0;
    LPTIM1->ICR = 0x7F;
    enc_ptr->event = EVENT__WAIT;
}

/*******************************************************************************
 * ������� ��������� �������� ��������.
 ******************************************************************************/
void encoder__set(int enc, int value) {
    if (enc == enc_none)
        return;
    LPTIM1->CR = 0;
    LPTIM1->CR;
    LPTIM1->CR = 1;
    LPTIM1->CR;
    LPTIM1->CR = 5;
}

/*******************************************************************************
 * ������� ��������������� ��������. ������� ������������� ����.
 ******************************************************************************/
void encoder__deinit(int enc) {
    if (enc == enc_none)
        return;
    encoder__struct_t* enc_ptr = &encoder__data[enc];
    NVIC_DisableIRQ(LPTIM1_IRQn);
    LPTIM1->CR = 0;
    LPTIM1->ICR = 0x7F;
    enc_ptr->init = 0;
}

/*******************************************************************************
 * ������� ��������� �������� �������� ��������.
 ******************************************************************************/
int encoder__get(int enc) {
    return enc == enc_none ? -1 : (LPTIM1->CNT > 60000 ? 0 : LPTIM1->CNT);
}

/*******************************************************************************
 * �������- ���������� �������� ��������� ������ ��������.
 ******************************************************************************/
static void encoder__cout() {
    for(u8 i =0; i < 3; i++) {
        encoder__struct_t* enc_ptr = &encoder__data[i];
        if (enc_ptr->event != EVENT__WAIT) {
            enc_ptr->event = EVENT__WAIT;
            int ticks = LPTIM1->CNT;
            enc_ptr->cb(i, EVENT__OK, ticks, enc_ptr->ext_data);
        }
    }
}

/*******************************************************************************
 * ���������� ISR ��������.
 ******************************************************************************/
void encoder__IRQ_handler(int enc) {
    LPTIM1->ICR = 0x7F;
    LPTIM1->IER = 0;
    encoder__data[enc].event = EVENT__OK;
}