/***************************************************************************//**
 * @file hw_uart_usb.c.
 * @brief  ������� USB CDC ��� STM32L476VG.
 * @author a.tushentsov.
 ******************************************************************************/
#include "string.h"
#include "device.h"
#include "target.h"
#include "hw_uart_usb.h"
#include "usbd_desc.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "supervisor.h"

USBD_HandleTypeDef hUsbDeviceFS;
extern USBD_DescriptorsTypeDef FS_Desc;

#define CRITICAL_SECTION_USB_ON                             \
{                                                           \
    uint32_t critical_section_state = USB_OTG_FS->GINTMSK;  \
    USB_OTG_FS->GINTMSK = 0;

#define CRITICAL_SECTION_USB_OFF                            \
    USB_OTG_FS->GINTMSK = critical_section_state;           \
}

// ��������� ���������� UART.
typedef struct {
    uart__err_handler_t err_handler;                    // ��������� �� �������-���������� ������/������� UART.
    hw_uart__cb_t tx_isr_cb;                            // ��������� �� ���������������� ������� �� �������� (�� ����������).
    uint8_t *tx_buff;                                   // ��������� ���������������� ����� ��� �������� ������.
    int tx_len;                                         // ������ ����������������� ������ ��� ������ ������ (�� ���������� ��������� �������).

    hw_uart__rx_cb_t rx_isr_cb;                         // ��������� �� ���������������� ������� �� ������ ��������� ���������� ���� (�� ����������).
    int rx_len;                                         // ������ ����������������� ������ ��� ������ ������ (�� ���������� ��������� �������). ���������� 64 �����.
                                                        // ���� ������ ���� ������ 64 ������.
    int rx_bytes_count;                                 // ���������� �������� ����.
} hw_uart_usb__itf_t;

typedef struct {
    hw_uart_usb__itf_t uart[USB_UART_COUNT];
    uint8_t init;
    supervisor__idle_sub_t  idle_sub;
}hw_uart_usb_itf_t;

static hw_uart_usb_itf_t hw_usb;

uart__event_e hw_uart_usb__init(void) {
    uint32_t temp;
    if (hw_usb.init)
        return UART__EVENT_OK;
    for (int i = 0; i < USB_UART_COUNT; i++) {
        hw_usb.uart[i].rx_isr_cb = 0;
    }
    memset(USB_OTG_FS, 0x00, sizeof(USB_OTG_GlobalTypeDef));
    CRITICAL_SECTION_ON
    SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOAEN);
    temp = GPIOA->OSPEEDR;
    temp &= ~((GPIO_OSPEEDR_OSPEED0 << (11 * 2u)) | (GPIO_OSPEEDR_OSPEED0 << (12 * 2u)));
    temp |= (3 << (11 * 2u)) | (3 << (12 * 2u));
    GPIOA->OSPEEDR = temp;
    temp = GPIOA->OTYPER;
    temp &= ~((GPIO_OTYPER_OT0 << 11) | (GPIO_OTYPER_OT0 << 12));
    temp |= (((1 & 0x02) >> 4u) << 11) | (((1 & 0x02) >> 4u) << 12);
    GPIOA->OTYPER = temp;
    temp = GPIOA->PUPDR;
    temp &= ~((GPIO_PUPDR_PUPD0 << (11 * 2u)) | (GPIO_PUPDR_PUPD0 << (12 * 2u)));
    GPIOA->PUPDR = temp;
    temp = GPIOA->AFR[11 >> 3u];
    temp &= ~(0xFu << ((11 & 0x07u) * 4u));
    temp |= (0x0A << ((11 & 0x07u) * 4u));
    GPIOA->AFR[11 >> 3u] = temp;
    temp = GPIOA->AFR[12 >> 3u];
    temp &= ~(0xFu << ((12 & 0x07u) * 4u));
    temp |= (0x0A << ((12 & 0x07u) * 4u));
    GPIOA->AFR[12 >> 3u] = temp;
    temp = GPIOA->MODER;
    temp &= ~((GPIO_MODER_MODE0 << (11 * 2u)) | (GPIO_MODER_MODE0 << (12 * 2u)));
    temp |= (0x02 << (11 * 2u)) | (0x02 << (12 * 2u));
    GPIOA->MODER = temp;
    MODIFY_REG(RCC->CCIPR, RCC_CCIPR_CLK48SEL, 0x08000000);
    SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_OTGFSEN);
    SET_BIT(PWR->CR2, PWR_CR2_USV);
    CRITICAL_SECTION_OFF

    USB_OTG_FS->GUSBCFG |= USB_OTG_GUSBCFG_PHYSEL;
    // Reset USB
    USB_OTG_FS->GRSTCTL |= USB_OTG_GRSTCTL_CSRST;
    temp = 0;
    while (((USB_OTG_FS->GRSTCTL & USB_OTG_GRSTCTL_CSRST) == USB_OTG_GRSTCTL_CSRST) && (temp < 200000))
        temp++;

    /* Init Device Library, add supported class and start the library. */
    hUsbDeviceFS.pDesc = &FS_Desc;
    hUsbDeviceFS.dev_state = USBD_STATE_DEFAULT;
    hUsbDeviceFS.id = DEVICE_FS;
    USBD_LL_Init(&hUsbDeviceFS);
    hUsbDeviceFS.pClass = &USBD_CDC;
    hUsbDeviceFS.pUserData = &USBD_Interface_fops_FS;
    if (USBD_LL_Start(&hUsbDeviceFS) != USBD_OK) {
        USB_Error_Handler();
        return UART__EVENT_ERROR;
    }
    NVIC_SetPriority(OTG_FS_IRQn, 0);
    NVIC_EnableIRQ(OTG_FS_IRQn);
    CRITICAL_SECTION_ON
    USB_OTG_FS->GAHBCFG |= USB_OTG_GAHBCFG_GINT;
    CRITICAL_SECTION_OFF
    hw_usb.init = 1;
    supervisor__idle_lock(&hw_usb.idle_sub);
    return UART__EVENT_OK;
}

uart__event_e hw_uart_usb__deinit(void) {
    /* Set Default State */
    hUsbDeviceFS.dev_state = USBD_STATE_DEFAULT;
    /* Free Class Resources */
    hUsbDeviceFS.pClass->DeInit(&hUsbDeviceFS, (uint8_t)hUsbDeviceFS.dev_config);
    /* Stop the low level driver  */
    USBD_LL_Stop(&hUsbDeviceFS);
    /* Initialize low level driver */
    USBD_LL_DeInit(&hUsbDeviceFS);
    hw_usb.init = 0;
    supervisor__idle_unlock(&hw_usb.idle_sub);
    return UART__EVENT_OK;
}

/*******************************************************************************
 * ������� �������� ���������������� ������ � ��������� UART.
 ******************************************************************************/
uart__event_e hw_uart_usb__tx(int uart_id, uint8_t *buff, int len, hw_uart__cb_t tx_isr_cb) {
    uint8_t result, port = uart_id - USB_UART_1;

    hw_usb.uart[port].tx_buff = buff;
    hw_usb.uart[port].tx_len = len;
    hw_usb.uart[port].tx_isr_cb = tx_isr_cb;
    result = CDC_Transmit_FS((uint8_t*)buff, len, port);
    switch(result) {
    case USBD_OK:
        return UART__EVENT_OK;
    case USBD_BUSY:
        return UART__EVENT_BUSY;
    default:
        return UART__EVENT_ERROR;
    }
}

/*******************************************************************************
 * ������� ������ ������ �� ���������� �����.
 ******************************************************************************/
uart__event_e hw_uart_usb__rx(int uart_id, hw_uart__rx_cb_t rx_isr_cb) {
    uart__event_e res = UART__EVENT_OK;
    uint8_t port = uart_id - USB_UART_1;

    if (hw_usb.uart[port].rx_isr_cb)
        res = UART__EVENT_BUSY;
    else
        hw_usb.uart[port].rx_isr_cb = rx_isr_cb;

    return res;
}

void hw_uart_usb_rx_cb(uint8_t* buff, uint32_t len, uint8_t port) {
    hw_uart__rx_cb_t cb = hw_usb.uart[port].rx_isr_cb;
    port += USB_UART_1;
    if (cb)
        cb(port, buff, len);
}

void hw_uart_usb_tx_cb(uint8_t port) {
    if (hw_usb.uart[port].tx_isr_cb)
        hw_usb.uart[port].tx_isr_cb(port + USB_UART_1, UART__EVENT_OK, hw_usb.uart[port].tx_buff, hw_usb.uart[port].tx_len);
}

extern PCD_HandleTypeDef hpcd_USB_OTG_FS;

void hw_uart_usb__IRQ(void) {
  HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
}

void USB_Error_Handler(void) {
    while(1);
}
