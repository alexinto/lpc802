/***************************************************************************//**
 * @file flash_int.c.
 * @brief ������ ������ � ���������� FLASH (���������� ��� STM32L476). 
 *        ��� ������ ������ ���� ��������� �� 8 ���� (64 ���). 
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "standard.h"
#include "target.h"
#include "flash_int.h"
#include "flash.h"
#include "resource.h"
#include "supervisor.h"
#include "wdt.h"
#include "mcu.h"

#define wdt__cmd(x) __no_operation()

#ifndef FLASH_INT__ID
    #define FLASH_INT__ID 0
#endif

/*******************************************************************************
 * �������� FLASH ������ ��.
 ******************************************************************************/
    #define MCU__FLASH_INT_MAIN_MEM_BEGIN     ((uint32_t)0x08000000)
    #define MCU__FLASH_INT_MAIN_MEM_END       ((uint32_t)0x080FFFFF)


#define FLASH_PDKEY1               ((uint32_t)0x04152637U) /*!< Flash power down key1 */
#define FLASH_PDKEY2               ((uint32_t)0xFAFBFCFDU) /*!< Flash power down key2: used with FLASH_PDKEY1
                                                              to unlock the RUN_PD bit in FLASH_ACR */

#define FLASH_PEKEY1               ((uint32_t)0x45670123U) /*!< Flash program erase key1 */
#define FLASH_PEKEY2               ((uint32_t)0xCDEF89ABU)  /*!< The following values must be written consecutively to unlock the FLASH_CR
                                                                 register allowing flash programming/erasing operations */

#define FLASH_OPTKEY1              ((uint32_t)0x08192A3BU) /*!< Flash option key1 */
#define FLASH_OPTKEY2              ((uint32_t)0x4C5D6E7FU) /*!< The following values must be written consecutively to unlock the FLASH_OPTR
                                                                register allowing option byte programming/erasing operations. */


#define FLASH_PAGE_SIZE           (2048U)  /*!< FLASH Page Size in bytes */


// ���� ����������� ��������.
typedef enum {
    FLASH_INT__OPER_NA,             // ��� ��������.
    FLASH_INT__OPER_R,              // ������.
    FLASH_INT__OPER_W,              // ������.
    FLASH_INT__OPER_CP,             // ������� ��������.
    FLASH_INT__OPER_CS,             // ������� �������.
    FLASH_INT__OPER_CA,             // ������� ���� ������.
} flash_int__operation_t;

// ���������, ����������� ������ ������ � FLASH.
typedef struct {
    flash__hw_cb cb;                                                // ��������� �� ������� ��� ������� �������� (NULL - ��� ��������).
    flash__op_e cur_oper;                                // ������� ��������.
    uint32_t addr;                                                  // �����, �� �������� �������������� ��������.
    uint8_t *data;                                                  // ��������� �� ���������������� ������.
    uint32_t len;                                                   // ������ ���������������� ������.
    resource__t resource;                                           // FLASH �������� ��������.
    flash__t* flash_not_use;
} flash_int__struct_t;

static flash_int__struct_t flash;

static ax_event__e flash__unlock(void);
static ax_event__e flash_lock(void);
static ax_event__e flash_int__op_switch(void);

static ax_event__e flash_int__op_page_erase(void);
static ax_event__e flash_int__op_read(void);
static ax_event__e flash_int__op_write(void);

static int get_page(int addr);

/*******************************************************************************
 * ������� �������� ��������.
 ******************************************************************************/

ax_event__e flash_int__execute(flash__t* flash_not_use, flash__op_e oper, uint32_t addr, uint8_t *buff, uint32_t len, flash__hw_cb cb) {
    ax_event__e res = AX_EVENT__OK;
    if (!(addr >= MCU__FLASH_INT_MAIN_MEM_BEGIN && addr <= MCU__FLASH_INT_MAIN_MEM_END)) 
        return AX_EVENT__PARAMS_NA;
    if (((addr - MCU__FLASH_INT_MAIN_MEM_BEGIN) % 8) || (len % 8)) 
        return AX_EVENT__PARAMS_NA;
    if (resource__lock(&flash.resource) != RESOURCE__OK)
        return AX_EVENT__BUSY;
    flash.flash_not_use = flash_not_use;
    flash.cur_oper = oper;
    if (flash.cb = cb) {
        flash.addr = addr;
        flash.data = buff;
        flash.len = len;
    }
    else {
        res = flash_int__op_switch();                
        resource__unlock(&flash.resource);
    }
    return res;
}

/*******************************************************************************
 * ������� ������ ����������� ������� ��������.
 ******************************************************************************/
static ax_event__e flash_int__op_switch(void) {
    ax_event__e res =  AX_EVENT__PARAMS_NA;
    switch (flash.cur_oper) {
        case FLASH__OP_READ:
            res = flash_int__op_read();
            break;
        case FLASH__OP_WRITE:
            res = flash_int__op_write();
            break;
        case FLASH__OP_ERASE_PAGE:
            flash.len = FLASH_PAGE_SIZE;
            res = flash_int__op_page_erase();
            break;
        default:
            break;
    }
    flash.cur_oper = FLASH__OP_NONE;
    return res;
}

/*******************************************************************************
 * ������� ��������� �������� ��������� ������ ������ � FLASH.
 ******************************************************************************/
void flash_int__cout(flash__t* flashka) {
    ax_event__e res;
    if (flash.cur_oper == FLASH__OP_NONE)
        return;
    res = flash_int__op_switch();
    resource__cb_start(&flash.resource);
    flash.cb(flash.flash_not_use, res, flash.addr, flash.data, flash.len);
    resource__cb_stop(&flash.resource);
}

/*******************************************************************************
 * ������� ������������� �������� �������� ��������.
 ******************************************************************************/
static ax_event__e flash_int__op_page_erase(void) {
    ax_event__e res = AX_EVENT__OK;
    int page;
    CRITICAL_SECTION_ON
    wdt__cmd(WDT__OFF);
    if (FLASH->SR & FLASH_SR_BSY)
        return AX_EVENT__BUSY;
    res = flash__unlock();
    page = get_page(flash.addr); //���������� ������ ��������
    if(page < 256)
        CLEAR_BIT(FLASH->CR, FLASH_CR_BKER);
    else
        SET_BIT(FLASH->CR, FLASH_CR_BKER);
    MODIFY_REG(FLASH->CR, FLASH_CR_PNB, ((page & 0xFFU) << FLASH_CR_PNB_Pos));
    SET_BIT(FLASH->CR, FLASH_CR_PER);
    SET_BIT(FLASH->CR, FLASH_CR_STRT);
    for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 10000000); x++);
    res = flash_lock();
    wdt__cmd(WDT__ON);
    CRITICAL_SECTION_OFF
    return res;
}

/*******************************************************************************
 * ������� ������������� �������� ������.
 ******************************************************************************/
static ax_event__e flash_int__op_write(void) {
    ax_event__e res;
    uint8_t *addr = (uint8_t*)flash.addr, *data = (uint8_t*)flash.data;
    CRITICAL_SECTION_ON
    res = flash__unlock();
    SET_BIT(FLASH->CR, FLASH_CR_PG);
    memcpy(addr, data, flash.len);
    for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 10000000); x++);
    res = flash_lock();
    CRITICAL_SECTION_OFF
    return res;
}

/*******************************************************************************
 * ������� ������������� �������� ������.
 ******************************************************************************/
static ax_event__e flash_int__op_read(void) {
    memcpy(flash.data, (uint32_t*) flash.addr, flash.len);  // ����������� ������.
    return AX_EVENT__OK;
}

ax_event__e flash__unlock(void) {
    if ((READ_BIT(FLASH->CR, FLASH_CR_LOCK)) || (READ_BIT(FLASH->CR, FLASH_CR_OPTLOCK))) {
        CRITICAL_SECTION_ON
        /* Unlocking FLASH_CR register access*/
        WRITE_REG(FLASH->KEYR, FLASH_PEKEY1);
        WRITE_REG(FLASH->KEYR, FLASH_PEKEY2);
        WRITE_REG(FLASH->OPTKEYR, FLASH_OPTKEY1);
        WRITE_REG(FLASH->OPTKEYR, FLASH_OPTKEY2);
        CRITICAL_SECTION_OFF
    }
    return AX_EVENT__OK;
}

ax_event__e flash_lock(void) {
  FLASH->CR = FLASH_CR_LOCK | FLASH_CR_OPTLOCK;
  return AX_EVENT__OK;
}

static int get_page(int addr) {
  addr -= MCU__FLASH_INT_MAIN_MEM_BEGIN;
  int page = (addr/FLASH_PAGE_SIZE);
  return page;
}

