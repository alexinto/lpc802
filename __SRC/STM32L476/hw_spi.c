/***************************************************************************//**
* @file test_hw_spi.c.
* @brief ���� supervisor ��� stm32l4.
* @authors a.tushentsov.
******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "standard.h"
#include "target.h"
#include "device.h"
#include "hw_spi.h"
#include "stm32l4xx.h"
#ifdef BB_SPI__1
    #include "bb_spi.h"
#endif

static void hw_spi__itf_en(int spi_id);
static void hw_spi__itf_dis(int spi_id);
void hw_spi__SPI_IRQHandler(int spi_id);
spi__event_e spi_init_regs(int spi_id);

// ��������� ���������� SPI.
typedef struct {
    uint32_t settings;                                  // ������� ���������.
    uint8_t *buff;                                      // ��������� �� ����� � ����������������� ������� ��� ��������.
    int len;                                            // ������ ������ � ����������������� ������� ��� �������� � ������.
    hw_spi__cb_t rxtx_isr_cb;                           // ��������� �� ���������������� ������� (�� ����������).
    int bytes_count;                                    // ������� ������.
    int init;                                           // ���� ������������� ����������.
} hw_spi__itf_t;

// ������ ��� ��������� ��������� �� ��������� SPI �� ID.
#define HW_SPI_ID (spi_id - HW_SPI_1)
#define HW_SPI__PTR_FROM_ID_GET(spi_id)   &hw_spi.itf[HW_SPI_ID]
#define HW_SPI_ID_NUM (spi_id - SPI_1)

typedef struct {
    hw_spi__itf_t itf[HW_SPI__COUNT];
} hw_spi__struct;

static hw_spi__struct hw_spi;

// ��������� �������� ������� SPI.
 const SPI_TypeDef* const spi__mass[] = {
    SPI1,
    SPI2,
    SPI3,
};

spi__event_e spi_init_regs(int spi_id) {
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[HW_SPI_ID_NUM];
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);

//todo baud rate
    SPI->CR1 &=~  SPI_CR1_BR;
    switch (spi->settings & SPI__BR_MASK) {
        case SPI__BR_LOW_SPD:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_1| SPI_CR1_BR_0;
        break;
        case SPI__BR_375_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_1;
        break;
        case SPI__BR_750_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_0;
        break;
        case SPI__BR_1500_KBIT:
            SPI->CR1 |= SPI_CR1_BR_2;
        break;
        case SPI__BR_3_MBIT:
            SPI->CR1 |= SPI_CR1_BR_1| SPI_CR1_BR_0;
        break;
        case SPI__BR_6_MBIT:
            SPI->CR1 |= SPI_CR1_BR_1;
        break;
        case SPI__BR_12_MBIT:
            SPI->CR1 |= SPI_CR1_BR_0;
        break;
        case SPI__BR_24_MBIT:
        case SPI__BR_HIGH_SPD:
        break;
        default:
        return SPI__EVENT_PARAM_NA;
    }

//PHASE - POL
    SPI->CR1 &=~ (SPI_CR1_CPHA | SPI_CR1_CPOL);
    switch (spi->settings & SPI__CPHA_CPOL_MASK) {
        case SPI__CPHA_0_CPOL_0:
            break;
        case SPI__CPHA_0_CPOL_1:
            SPI->CR1 |= SPI_CR1_CPOL;
            break;
        case SPI__CPHA_1_CPOL_0:
            SPI->CR1 |= SPI_CR1_CPHA;
            break;
        case SPI__CPHA_1_CPOL_1:
            SPI->CR1 |= SPI_CR1_CPHA | SPI_CR1_CPOL;
            break;
        default:
            return SPI__EVENT_PARAM_NA;
    }
//FRAME
    switch (spi->settings & SPI__FRAME_MASK) {
        case SPI__DATA_FRAME_8_BIT:
            SPI->CR2 &=~ SPI_CR2_DS;
            break;
        case SPI__DATA_FRAME_16_BIT:
            SPI->CR2 |= SPI_CR2_DS;
            break;
        default:
            return SPI__EVENT_PARAM_NA;
    }
//LSB - MSB
    switch (spi->settings & SPI__LSBMSB_MASK) {
        case SPI__MSB:
            SPI->CR1 &=~ SPI_CR1_LSBFIRST;
            break;
        case SPI__LSB:
            SPI->CR1 |= SPI_CR1_LSBFIRST;
            break;
        default:
            return SPI__EVENT_PARAM_NA;
    }
//Software NSS + internal slave select
    SPI->CR1 |= SPI_CR1_SSM | SPI_CR1_SSI;
    SPI->CR2 |= SPI_CR2_NSSP | SPI_CR2_FRXTH;
// MOTOROLLA protocol
    SPI->CR2 &=~ SPI_CR2_FRF;

//MASTER - SLAVE
    switch (spi->settings & SPI__MODE_MASK) {
        case SPI__MODE_MASTER:
            SPI->CR1 |= SPI_CR1_MSTR;
            break;
        case SPI__MODE_SLAVE:       // slave �� ����������
        default:
            return SPI__EVENT_PARAM_NA;
            break;
    }
    return SPI__EVENT_OK;
}

/*******************************************************************************
 *  ������� �������������  SPIx.
 ******************************************************************************/
spi__event_e hw_spi__init (int spi_id, uint32_t settings, cs_handler_t cs_hdl) {
    if (spi_id > MCU__SPI_MAX) {
        #ifdef BB_SPI__1
            return bb_spi__init (spi_id, settings);
        #else
            return SPI__EVENT_PARAM_NA;
        #endif
    }

/* (+) 1. �������� ����� �� ������� ����� SPI
    (+) 2. �������� ������������ SPI
    (+) 3. ���������� ������� (���������� BR)
    (+) 4. ������� ���������� � ���� (POL/PHA)
    (+) 5. ������� ������ ������ 8/16 ���
    (+) 6. ������� MSB/LSB
    (+) 7. ��������� NSS/CS (�������� SSM/SSI)
    (+) 8. ��������� TI �������� (FRF ���)
    (+) 9. ��������� MASTER
    (+) 10. �������� SPI (SPE)
    (+) 11. ��������� ��� MODF � SR �������� */
    hw_spi__itf_en(spi_id);
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);
    spi->settings = settings;
    spi->init = 1;
    return SPI__EVENT_OK;
}



/***************************************************************************//**
 * ������� ��������������� ���������� SPI. ���������� ���������� ���� �������� � ��������� ���������.
 ******************************************************************************/
spi__event_e hw_spi__deinit(int spi_id) {
    if (spi_id > MCU__SPI_MAX) {
        #ifdef BB_SPI__1
            return bb_spi__deinit (spi_id);
        #else
            return SPI__EVENT_PARAM_NA;
        #endif
    }
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);
    hw_spi__txrx_break(spi_id); // ������� ���, ����� ��� ������� �� spi->busy_txrx.
    spi->init = 0;
    hw_spi__itf_dis(spi_id);
    return SPI__EVENT_OK;
}


/***************************************************************************//**
 * ������������� ������ �� SPI.
 ******************************************************************************/
spi__event_e hw_spi__txrx (int spi_id, int cs_id, uint8_t *rx_buff, uint8_t *tx_buff, uint32_t len, int cb_from_isr, hw_spi__cb_t callback){
    spi__event_e res;
    if (spi_id > MCU__SPI_MAX) {
        #ifdef BB_SPI__1
            return bb_spi__txrx (spi_id, cs_id, rx_buff, tx_buff, len, callback);
        #else
            return SPI__EVENT_PARAM_NA;
        #endif
    }
    /* ������� ����������� �������� -> ����� �����.

    ������������������ ��������:
    (+) 1. ������ ������������ �� �������� (�������������� CS -> low (��� ������ ������� spi.c)), �������� ����������
    (+) 2. ��������� DR ������ � �������
    (+) 3. � �������� ��������� ���� TXE ����� ���������
    (+) 4. ����� ������������� ���������� ����� ����� �� �������� ����������� (���������� �� TXEIE)

     ������������������ ������:
    (+) 1. ���������� ���������� ������ RX(���� ����� RXNE)
    (+) 2. ���� ���������� �� RXNE
    (+) 3. ������ ������� DR, ��� ����� ������� ���� RXNE */

    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[HW_SPI_ID_NUM];
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);

    if ((len == 0) || (rx_buff == NULL && tx_buff == NULL))
        return SPI__EVENT_PARAM_NA;

    if ((res = spi_init_regs(spi_id)) != SPI__EVENT_OK)
        return res;
    spi->len = len;
    spi->rxtx_isr_cb = callback;
    spi->bytes_count = 0;

    if (tx_buff == NULL) {         //������ �����
        spi->buff = rx_buff;
//        SPI->CR1 |= SPI_CR1_RXONLY;
        SPI->DR;
        SPI->CR1 &= ~SPI_CR1_RXONLY;
        SPI->CR2 |= SPI_CR2_RXNEIE | SPI_CR2_ERRIE;
        *(uint8_t*)&SPI->DR = 0xFF;
    }
    else {
        spi->buff = tx_buff;
        SPI->CR1 &= ~SPI_CR1_RXONLY;
        *(uint8_t*)&SPI->DR = *spi->buff;
        SPI->CR2 |= SPI_CR2_TXEIE;
    }
    //�������� SPI
    SPI->CR1 |= SPI_CR1_SPE;
    //�������� �� ������ ��������� ���������� ������
    if ((SPI->SR & SPI_SR_MODF) != 0)
        return SPI__EVENT_ERROR;

    return SPI__EVENT_OK;
}


/***************************************************************************//**
 * ������� ����������� ���������� �������� �������� ������ ��������� SPI.
 ******************************************************************************/
spi__event_e hw_spi__txrx_break(int spi_id) {
    if (spi_id > MCU__SPI_MAX) {
        #ifdef BB_SPI__1
            return bb_spi__txrx_break(spi_id);
        #else
            return SPI__EVENT_PARAM_NA;
        #endif
    }
    CRITICAL_SECTION_ON
    switch(spi_id) {
    case SPI_1:
        RCC->APB2RSTR |= RCC_APB2RSTR_SPI1RST;
        RCC->APB2RSTR &= ~RCC_APB2RSTR_SPI1RST;
        break;
    case SPI_2:
        RCC->APB1RSTR1 |=  RCC_APB1RSTR1_SPI2RST;
        RCC->APB1RSTR1 &=  ~RCC_APB1RSTR1_SPI2RST;
        break;
    case SPI_3:
        RCC->APB1RSTR1 |=  RCC_APB1RSTR1_SPI3RST;
        RCC->APB1RSTR1 &=  ~RCC_APB1RSTR1_SPI3RST;
        break;
    }
    CRITICAL_SECTION_OFF
    return SPI__EVENT_OK;
}

/***************************************************************************//**
 * ������� ��������� �������� ��������� �������� SPI.
 ******************************************************************************/
void hw_spi__cout(void) {
#ifdef BB_SPI__1
    bb_spi__cout();
#endif
    __NOP();

}

/*******************************************************************************
 *  �������-���������� �������� �� SPI.
 ******************************************************************************/
void hw_spi__SPI_IRQHandler(int spi_id) {
    SPI_TypeDef *SPI = (SPI_TypeDef*) spi__mass[HW_SPI_ID_NUM];
    hw_spi__itf_t *spi = HW_SPI__PTR_FROM_ID_GET(spi_id);
    spi__event_e code = SPI__EVENT_OK;
    int status_reg = SPI->SR;
    uint8_t data = SPI->DR;//*(uint8_t*)SPI->DR;
    // �������� �� ������
    if ((status_reg & SPI_SR_OVR) && (SPI->CR2 & SPI_CR2_ERRIE)) {
        code = SPI__EVENT_OVERRUN;
        SPI->CR2 &= ~(SPI_CR2_RXNEIE | SPI_CR2_TXEIE | SPI_CR2_ERRIE);
        if (spi->rxtx_isr_cb) {
            hw_spi__txrx_break(spi_id);
            spi->rxtx_isr_cb(spi_id, 0, code, spi->buff, spi->bytes_count);
        }
    }
    else if ((status_reg & SPI_SR_TXE) && (SPI->CR2 & SPI_CR2_TXEIE)) {  //��������
        spi->bytes_count++;
        if (spi->bytes_count >= spi->len) {
//            SPI->DR;
//            MODIFY_REG(SPI->CR2, SPI_CR2_TXEIE, SPI_CR2_RXNEIE);
            for(int x = 0; (x < 100000) && (SPI->SR & SPI_SR_BSY); x++);
            hw_spi__txrx_break(spi_id);
            spi->rxtx_isr_cb(spi_id, 0, code, spi->buff, spi->bytes_count);
        }
        else
            *(uint8_t*)&SPI->DR = spi->buff[spi->bytes_count];
    }
    else if((status_reg & SPI_SR_RXNE) && (SPI->CR2 & SPI_CR2_RXNEIE)) { //�����
//        if (SPI->CR2 & SPI_CR2_ERRIE) {
        spi->buff[spi->bytes_count] = data;
        spi->bytes_count++;
        if (spi->bytes_count < spi->len)
            *(uint8_t*)&SPI->DR = 0xFF;
        else {
            hw_spi__txrx_break(spi_id);
            spi->rxtx_isr_cb(spi_id, 0, code, spi->buff, spi->bytes_count);
        }
    }
}

/*******************************************************************************
 *  ������� ������������� ����������� SPI.
 ******************************************************************************/
typedef struct {
    int gpio[3];
    uint32_t* spi_clock;
    int spi_clock_flag;
    int spi_irq;
}hw_spi_desc__t;

static const hw_spi_desc__t hw_spi_desc[] = {
#ifdef HW_SPI_1_CLK
    HW_SPI_1_CLK, HW_SPI_1_MOSI, HW_SPI_1_MISO, (uint32_t*)&RCC->APB2ENR, RCC_APB2ENR_SPI1EN, SPI1_IRQn,
#endif
#ifdef HW_SPI_2_CLK
    HW_SPI_2_CLK, HW_SPI_2_MOSI, HW_SPI_2_MISO, (uint32_t*)&RCC->APB1ENR1, RCC_APB1ENR1_SPI2EN, SPI2_IRQn,
#endif
#ifdef HW_SPI_3_CLK
    HW_SPI_3_CLK, HW_SPI_3_MOSI, HW_SPI_3_MISO, (uint32_t*)&RCC->APB1ENR1, RCC_APB1ENR1_SPI3EN, SPI3_IRQn,
#endif
};

static void hw_spi__itf_en(int spi_id) {
    for(int spi_param_desc_cnt = 0; spi_param_desc_cnt < 3; spi_param_desc_cnt++)
        gpio__af_init(hw_spi_desc[HW_SPI_ID].gpio[spi_param_desc_cnt], 0, 0x5);
    *hw_spi_desc[HW_SPI_ID].spi_clock |= hw_spi_desc[HW_SPI_ID].spi_clock_flag;
    NVIC_EnableIRQ((IRQn_Type)hw_spi_desc[HW_SPI_ID].spi_irq);
}
/*******************************************************************************
 *  ������� ��������������� ����������� SPI.
 ******************************************************************************/
static void hw_spi__itf_dis(int spi_id) {
    NVIC_DisableIRQ((IRQn_Type)hw_spi_desc[HW_SPI_ID].spi_irq);
    *hw_spi_desc[HW_SPI_ID].spi_clock &= ~hw_spi_desc[HW_SPI_ID].spi_clock_flag;
    for(int spi_param_desc_cnt = 0; spi_param_desc_cnt < 3; spi_param_desc_cnt++)
        gpio__init(hw_spi_desc[HW_SPI_ID].gpio[spi_param_desc_cnt],  GPIO__DIR_IN, NULL);
}
