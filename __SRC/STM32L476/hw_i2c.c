/***************************************************************************//**
 * @file hw_i2c.�
 * @brief ������� I2C (��������� ��������� ����� ��� stm32l1).
 * @authors s.korotkov
 ******************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include "standard.h"
#include "target.h"
#include "mcu.h"
#include "hw_i2c.h"
#include "i2c.h"
#include "supervisor.h"

#ifndef HW_I2C__ARB_LOST_DELAY
    #define HW_I2C__ARB_LOST_DELAY 5000     //����-��� ������ ��������� ��� ������������ �����- ������������������ 1.. 255. 1= 1 ����������� cout().
#endif

#if (HW_I2C__ARB_LOST_DELAY == 0) || (HW_I2C__ARB_LOST_DELAY == 255)
    #error "HW_I2C__ARB_LOST_DELAY must be > 0 and < 255"
#endif

#define I2C_MODE_READ	                1
#define I2C_MODE_WRITE	                0
#define I2C_ADDRESS_7BITS(addr, mode)	((addr << 1) | mode)
#define I2C_ADDRESS_10BITS(addr, mode)	(((addr << 1) | mode) & 0xFF)
#define I2C_HEADER_10BITS(addr)	        (0xF1 | (addr & 0x03))
#define HW_I2C_ID (i2c_id - HW_I2C_1)

typedef struct {
     uint8_t SB: 1;
     uint8_t ADDR: 1;
     uint8_t BTF: 1;
     uint8_t ADD10: 1;
     uint8_t STOPF: 1;
     uint8_t RESEVED: 1;
     uint8_t RxNE: 1;
     uint8_t TxE: 1;
     uint8_t BERR: 1;
     uint8_t ARLO: 1;
     uint8_t AF: 1;
     uint8_t OVR: 1;
     uint8_t PECERR: 1;
     uint8_t RESEVED1: 1;
     uint8_t TIMEOUT: 1;
     uint8_t SMBALERT: 1;
 } hw_i2c__sr1_t;

typedef struct {
     uint8_t MSL: 1;
     uint8_t BUSY: 1;
     uint8_t TRA: 1;
     uint8_t RESEVED2: 1;
     uint8_t GENCALL: 1;
     uint8_t SMBDEFAULT: 1;
     uint8_t SMBHOST: 1;
     uint8_t DUALF: 1;
     uint8_t PEC: 1;
 } hw_i2c__sr2_t;

 typedef union {
     uint16_t sr2_u16;
     hw_i2c__sr2_t sr2;
 } hw_i2c__sr2_u;

  typedef union {
     uint16_t sr1_u16;
     hw_i2c__sr1_t sr1;
 } hw_i2c__sr1_u;

// ���� ��������.
typedef enum {
    HW_I2C__OPER_NONE,
    HW_I2C__OPER_TX,
    HW_I2C__OPER_RX,
} hw_i2c__oper_e;

// ��������� ���������� I2C.
typedef struct {
    int own_addr;                                       // ����������� �����.
    int addr;                                           // ����� ���������.
    uint8_t *buff;                                      // ��������� �� ����� � ����������������� �������.
    int len;                                            // ������ ������ � ����������������� �������.
    hw_i2c__cb_t isr_cb;                                // ��������� �� ���������������� ������� (�� ����������).
    int bytes_count;                                    // ������� ������.
    int i2c_delay;                                      // �������� ��� ����� ������������������ (����������� ������� 100���).
    hw_i2c__oper_e cur_oper;                            // ������� ����������� ��������.
    uint16_t settings;                                  // ������� ���������.
} hw_i2c__itf_t;

// ������ ��� ��������� ��������� �� ��������� ����������� I2C �� ID.
#define HW_I2C__PTR_FROM_ID_GET(i2c_id)   (&hw_i2c.itf[i2c_id - HW_I2C_1])

// ������ ��� ��������� ID ����������� I2C �� ��������� �� ���������.
#define HW_I2C__ID_FROM_PTR_GET(i2c_ptr)   (i2c_ptr + HW_I2C_1)

#if HW_I2C__COUNT != 0

// ��������� �������� I2C.
typedef struct {
    hw_i2c__itf_t itf[HW_I2C__COUNT];
} hw_i2c__t;

static const I2C_TypeDef* const i2c_regs[] = {I2C1, I2C2, I2C3};

static hw_i2c__t hw_i2c;

static i2c__event_e hw_i2c__itf_en(int i2c_id);
static i2c__event_e hw_i2c__itf_dis(int i2c_id);

static i2c__event_e hw_i2c__init_continue(int i2c_id);
static void hw_i2c__deinit_short(int i2c_id);
void hw_i2c__I2C_EV_IRQHandler(int i2c_id);
void hw_i2c__I2C_ER_IRQHandler(int i2c_id);

/*******************************************************************************
 * ������� ������������� ����������� I2C �� ������ � ���������� �����������, � ��� �� �������� �������-����������� �������.
 *        Ex: while(hw_i2c__init(I2C_2, I2C__FREQ_100KHZ | I2C__ADDR_7BIT, 0, NULL) != 0){};
 *        �������� ������� ������������� �� ������� ������������ ���� � ������ ������������� I2C.
 ******************************************************************************/
i2c__event_e hw_i2c__init(int i2c_id, uint16_t settings, int own_addr, hw_i2c__cb_t event_handler) {
    if (settings == 0)
        return I2C__EVENT_PARAM_NA;
    hw_i2c__deinit(i2c_id);
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    i2c->settings = settings;
    i2c->own_addr = own_addr;
    i2c->cur_oper = HW_I2C__OPER_NONE;
    // �������������� ����� I2C � ��������� ���������� ����������
    hw_i2c__itf_en(i2c_id);
    return hw_i2c__init_continue(i2c_id);
}

/*******************************************************************************
 * ������� todo
 ******************************************************************************/
static i2c__event_e hw_i2c__init_continue(int i2c_id) {
    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[HW_I2C_ID];
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    hw_i2c__deinit_short(i2c_id);
//��������� I2C �� ������ ������
    switch (I2C__MAX_FREQ_MASK & i2c->settings) {
    case I2C__FREQ_100KHZ:
        I2C->TIMINGR = (uint32_t)(0x0 << I2C_TIMINGR_PRESC_Pos) | (0x0A << I2C_TIMINGR_SCLDEL_Pos) | (0x0A << I2C_TIMINGR_SDADEL_Pos) | (70 << I2C_TIMINGR_SCLH_Pos) | (70 << I2C_TIMINGR_SCLL_Pos);
        break;
    case I2C__FREQ_400KHZ:
        I2C->TIMINGR = (uint32_t)(0x0 << I2C_TIMINGR_PRESC_Pos) | (0x02 << I2C_TIMINGR_SCLDEL_Pos) | (0x02 << I2C_TIMINGR_SDADEL_Pos) | (20 << I2C_TIMINGR_SCLH_Pos) | (20 << I2C_TIMINGR_SCLL_Pos);
        break;
    default:
        return I2C__EVENT_PARAM_NA;
    }
    switch (i2c->settings & I2C__ADDR_MASK) {
        case I2C__ADDR_7BIT:
            if (i2c->own_addr > 127)
                return I2C__EVENT_PARAM_NA;
            I2C->CR2 = 0;
            break;
        case I2C__ADDR_10BIT:
            if (i2c->own_addr > 1023)
                return I2C__EVENT_PARAM_NA;
            I2C->CR2 = I2C_CR2_ADD10;
            break;
        default:
            return I2C__EVENT_PARAM_NA;
    }
//�������� ����������������� �������, ����������� �� ������ STOP.
    I2C->CR1 = I2C_CR1_GCEN | I2C_CR1_WUPEN; // I2C_CR1_PE
    /*I2C � stm32l1 ���������� ��������� � slave mode,
    ��� �������� � master mode ���������� �����
    ������������� ����� ��������� ��������� start condition */
  // ��� ��������� SCL � ������� 25 �� ����� ������ ERROR.
    I2C->TIMEOUTR = I2C_TIMEOUTR_TIMOUTEN | 98;

    if ((I2C->ISR & I2C_ISR_BUSY) == I2C_ISR_BUSY)
        return I2C__EVENT_ERROR;
    i2c->i2c_delay = 10000;
    i2c->bytes_count = 0;
    return I2C__EVENT_OK;
}

/*******************************************************************************
 * ������� ����������� ����������� ���������� I2C �������� � ������������ ��������.
 ******************************************************************************/
i2c__event_e hw_i2c__deinit(int i2c_id) {
    hw_i2c__deinit_short(i2c_id);
    hw_i2c__itf_dis(i2c_id);
    return I2C__EVENT_OK;
}

/*******************************************************************************
 * ������� ����������� ����������� ���������� I2C �������� � ������������ ��������.
 ******************************************************************************/
static void hw_i2c__deinit_short(int i2c_id) {
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[HW_I2C_ID];
    I2C->CR1 = 0; I2C->CR1;
    i2c->i2c_delay = 10000;
}

/*******************************************************************************
 * ������� ������ ������ �� ����������� I2C.
 ******************************************************************************/
i2c__event_e hw_i2c__rx(int i2c_id, int addr, uint8_t *buff, int len, hw_i2c__cb_t isr_cb) {
    if (buff == NULL || len == 0)
        return I2C__EVENT_PARAM_NA;
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);

    if (i2c->cur_oper != HW_I2C__OPER_NONE)
        return I2C__EVENT_BUSY;
    i2c->cur_oper = HW_I2C__OPER_RX;
    hw_i2c__init_continue(i2c_id);

    i2c->buff = buff;
    i2c->len = len;
    i2c->isr_cb = isr_cb;
    i2c->addr = addr;
    i2c->i2c_delay = 0x00;

    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[HW_I2C_ID];
    //�������� I2C, ��������� ����������
    I2C->CR1 |= (len <= 1) ? (I2C_CR1_PE | I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_ERRIE) : (I2C_CR1_PE | I2C_CR1_RXIE | I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_ERRIE);
    // ������������ ������� START
    if (len < 255)
        I2C->CR2 |= I2C_CR2_RD_WRN | I2C_CR2_START | (addr << 1) | (len << I2C_CR2_NBYTES_Pos) | I2C_CR2_AUTOEND;
    else
        I2C->CR2 |= I2C_CR2_RD_WRN | I2C_CR2_START | (addr << 1) | (0xFF << I2C_CR2_NBYTES_Pos);

    return I2C__EVENT_OK;
}

/*******************************************************************************
 * ������� ������ ������ �� ����������� I2C.
 ******************************************************************************/
i2c__event_e hw_i2c__tx(int i2c_id, int addr, uint8_t *buff, int len, hw_i2c__cb_t isr_cb) {
    if (buff == NULL || len == 0)
        return I2C__EVENT_PARAM_NA;
    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);

    if (i2c->cur_oper != HW_I2C__OPER_NONE)
        return I2C__EVENT_BUSY;
    i2c->cur_oper = HW_I2C__OPER_TX;
    hw_i2c__init_continue(i2c_id);

    i2c->buff = buff;
    i2c->len = len;
    i2c->isr_cb = isr_cb;
    i2c->addr = addr;
    i2c->i2c_delay = 0x00;

    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[HW_I2C_ID];
    //��������� ����������
    I2C->CR1 |= I2C_CR1_PE | I2C_CR1_TXIE | I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_ERRIE;
    // ������������ ������� START
    I2C->CR2 |= I2C_CR2_START | (addr << 1) | (0xFF << I2C_CR2_NBYTES_Pos);
    return I2C__EVENT_OK;
}

/*******************************************************************************
 * ������� �������� ������ ��� ��������� �������� ��������� �������� I2C.
 ******************************************************************************/
void hw_i2c__cout(void) {
    for(int i2c_id = 1; i2c_id <= HW_I2C__COUNT; i2c_id++) {
        hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
        if (i2c->cur_oper != HW_I2C__OPER_NONE) {
            if (i2c->i2c_delay == HW_I2C__ARB_LOST_DELAY) {
                i2c->cur_oper = HW_I2C__OPER_NONE;
                i2c->isr_cb(i2c_id, I2C__EVENT_ERROR, i2c->addr, i2c->buff, i2c->bytes_count);
            }
            CRITICAL_SECTION_ON
            (i2c->i2c_delay >= HW_I2C__ARB_LOST_DELAY) ? (i2c->i2c_delay = 10000):(i2c->i2c_delay++);
            CRITICAL_SECTION_OFF
        }
    }
}

/*******************************************************************************
 * * �������-���������� ���������� �� ������ � �������� I2C.
 ******************************************************************************/
void hw_i2c__I2C_EV_IRQHandler(int i2c_id) {
#ifdef I2C_DEBUG_PIN
    gpio__set(I2C_DEBUG_PIN, GPIO__STATE_HIGH);
#endif

    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[HW_I2C_ID];
    i2c__event_e event = I2C__EVENT_OK;
    uint32_t flags = I2C->ISR;
    if (I2C->ISR & (I2C_ISR_STOPF)) {
        flags = I2C->ISR & (~I2C_ISR_STOPF);
        if (flags & (~0xF)) {
            if (I2C->ISR & I2C_ISR_NACKF)
                event = I2C__EVENT_AF_ERROR;
            else
                event = I2C__EVENT_ERROR;
        }
        if (i2c->cur_oper == HW_I2C__OPER_RX)
            i2c->buff[i2c->bytes_count] = I2C->RXDR;
        I2C->CR1 = 0;
        i2c->cur_oper = HW_I2C__OPER_NONE;
        i2c->isr_cb(i2c_id, event, i2c->addr, i2c->buff, i2c->bytes_count);
    }
    else if  (I2C->ISR & (I2C_ISR_NACKF)) {
        I2C->CR1 &= ~(I2C_CR1_RXIE | I2C_CR1_TXIE | I2C_CR1_NACKIE);
        I2C->CR2 |= I2C_CR2_STOP;
    }
    else if ((I2C->ISR & I2C_ISR_TXE) && (I2C->CR1 & I2C_CR1_TXIE)){
        i2c->bytes_count++;
        if (i2c->bytes_count <= i2c->len)
            I2C->TXDR = i2c->buff[i2c->bytes_count - 1];
        //���� ��������� ��������� �������
        else {
                i2c->bytes_count--;
                I2C->CR1 &= ~(I2C_CR1_RXIE | I2C_CR1_TXIE | I2C_CR1_NACKIE);
                I2C->CR2 |= I2C_CR2_STOP;
        }
    }
    else if ((I2C->ISR & I2C_ISR_RXNE) && (I2C->CR1 & I2C_CR1_RXIE)) {
        //��������� STOP �� ������������� �����.
        if (i2c->len <= (i2c->bytes_count + 2)) {
            I2C->CR1 &= ~(I2C_CR1_RXIE | I2C_CR1_TXIE | I2C_CR1_NACKIE);
            I2C->CR2 |= I2C_CR2_STOP;
        }
        i2c->buff[i2c->bytes_count] = I2C->RXDR;
        i2c->bytes_count++;
    }
    else {
        I2C->CR1 = 0;
        i2c->cur_oper = HW_I2C__OPER_NONE;
        i2c->isr_cb(i2c_id, I2C__EVENT_ERROR, i2c->addr, i2c->buff, i2c->bytes_count);
    }
#ifdef I2C_DEBUG_PIN
    gpio__set(I2C_DEBUG_PIN, GPIO__STATE_LOW);
#endif

}

/*******************************************************************************
 * �������-���������� ���������� �� �������.
 ******************************************************************************/
void hw_i2c__I2C_ER_IRQHandler(int i2c_id) {
#ifdef I2C_DEBUG_PIN
    gpio__set(I2C_DEBUG_PIN, GPIO__STATE_HIGH);
#endif

    hw_i2c__itf_t *i2c = HW_I2C__PTR_FROM_ID_GET(i2c_id);
    I2C_TypeDef *I2C = (I2C_TypeDef*)i2c_regs[HW_I2C_ID];
    i2c->i2c_delay = 10000;
    i2c__event_e event = I2C__EVENT_ERROR;
    if (I2C->ISR & I2C_ISR_TIMEOUT)
       event = I2C__EVENT_BUSY;
    else if (I2C->ISR & I2C_ISR_ARLO)
        event = I2C__EVENT_ARLO_ERROR;
    I2C->CR1 = 0;
    i2c->cur_oper = HW_I2C__OPER_NONE;
    i2c->isr_cb(i2c_id, event, i2c->addr, i2c->buff, i2c->bytes_count);
#ifdef I2C_DEBUG_PIN
    gpio__set(I2C_DEBUG_PIN, GPIO__STATE_LOW);
#endif
}

/*******************************************************************************
 *  ������� ������������� ���������� I2C.
 ******************************************************************************/
typedef struct {
    int gpio[2];
    int i2c_clk_msk;
    int i2c_clk_source;
    int i2c_clock_en;
    int i2c_irq[2];
}hw_i2c_desc__t;

static const hw_i2c_desc__t hw_i2c_desc[] = {
#ifdef HW_I2C_1_SCL
    HW_I2C_1_SCL, HW_I2C_1_SDA, RCC_CCIPR_I2C1SEL_Msk, RCC_CCIPR_I2C1SEL_1, RCC_APB1ENR1_I2C1EN, I2C1_EV_IRQn, I2C1_ER_IRQn,
#endif
#ifdef HW_I2C_2_SCL
    HW_I2C_2_SCL, HW_I2C_2_SDA, RCC_CCIPR_I2C2SEL_Msk, RCC_CCIPR_I2C2SEL_1, RCC_APB1ENR1_I2C2EN, I2C2_EV_IRQn, I2C2_ER_IRQn,
#endif
#ifdef HW_I2C_3_SCL
    HW_I2C_3_SCL, HW_I2C_3_SDA, RCC_CCIPR_I2C3SEL_Msk, RCC_CCIPR_I2C3SEL_1, RCC_APB1ENR1_I2C3EN, I2C3_EV_IRQn, I2C3_ER_IRQn,
#endif
};

static i2c__event_e hw_i2c__itf_en(int i2c_id) {
    MODIFY_REG(RCC->CCIPR, hw_i2c_desc[HW_I2C_ID].i2c_clk_msk, hw_i2c_desc[HW_I2C_ID].i2c_clk_source);   // I2C_CLK = 16MHz
    RCC->APB1ENR1 |= hw_i2c_desc[HW_I2C_ID].i2c_clock_en;
    for(int x = 0; x < 2; x++) {
        gpio__af_init(hw_i2c_desc[HW_I2C_ID].gpio[x], 1, 0x4);
        NVIC_EnableIRQ((IRQn_Type)hw_i2c_desc[HW_I2C_ID].i2c_irq[x]);
    }
    return I2C__EVENT_OK;
}

/*******************************************************************************
 *  ������� ��-������������� ���������� I2C.
 ******************************************************************************/
static i2c__event_e hw_i2c__itf_dis(int i2c_id) {
    RCC->APB1ENR1 &= ~hw_i2c_desc[HW_I2C_ID].i2c_clock_en;
    for(int x = 0; x < 2; x++) {
        NVIC_DisableIRQ((IRQn_Type)hw_i2c_desc[HW_I2C_ID].i2c_irq[x]);
        gpio__init(hw_i2c_desc[HW_I2C_ID].gpio[x], GPIO__DIR_IN, NULL);
    }
    return I2C__EVENT_OK;
}

#endif