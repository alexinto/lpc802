/***************************************************************************//**
 * @file hw_uart.h
 * @brief �������������� ������� ����������� UART
 *        ��� ���������� ������������ ������� ��������, � ������ �������������,
 *        �������������� ���������� UART ����� ��������� �� ����� ����������
 *        � ������� �������� #define UART_Ax_USE_BLOCKED.
 * @author p.luchnikov
 ******************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include "standard.h"
#include "target.h"
#include "mcu.h"
#include "hw_uart.h"
#include "uart.h"
#include "supervisor.h"
#include "sw_timer.h"
#ifdef BB_UART__1
    #include "bb_uart.h"
#endif
//#include "device.h"

 typedef struct {
    uint32_t	PE	:1;
    uint32_t	FE	:1;
    uint32_t	NE	:1;
    uint32_t	ORE	:1;
    uint32_t	IDLE	:1;
    uint32_t	RXNE	:1;
    uint32_t	TC	:1;
    uint32_t	TXE	:1;
    uint32_t	LBDF	:1;
    uint32_t	CTSIF	:1;
    uint32_t	CTS	:1;
    uint32_t	RTOF	:1;
    uint32_t	EOBF	:1;
    uint32_t    	   	:1;
    uint32_t	ABRE	:1;
    uint32_t	ABRF	:1;
    uint32_t	BUSY	:1;
    uint32_t	CMF	:1;
    uint32_t	SBKF	:1;
    uint32_t	RWU	:1;
    uint32_t	WUF	:1;
    uint32_t	TEACK	:1;
    uint32_t	REACK	:1;
 } hw_uart__isr_t;

 typedef union {
     uint32_t isr_u32;
     hw_uart__isr_t isr;
 } hw_uart__isr_u;

// ��������� �������� ������� ������ GPIO.
const USART_TypeDef * const usart__mass[] = {
    USART1,
    USART2,
    USART3,
    UART4,
    UART5,
    LPUART1,
};

// ��������� ���������� UART.
typedef struct {
    uint32_t settings;                                  // ������� ���������.
    uart__err_handler_t err_handler;                    // ��������� �� �������-���������� ������/������� UART.
    uint8_t *tx_buff;                                   // ��������� �� ����� � ����������������� ������� ��� ��������.
    hw_uart__cb_t tx_isr_cb;                            // ��������� �� ���������������� ������� �� �������� (�� ����������).
    int tx_len;                                         // ������ ������ � ����������������� ������� ��� ��������.
    int tx_bytes_count;                                 // ������� ���������� ������.

    uint8_t *rx_buff;                                   // ��������� ���������������� ����� ��� ������ ������.
    hw_uart__cb_t rx_isr_cb;                            // ��������� �� ���������������� ������� �� ������ ��������� ���������� ���� (�� ����������).
    int rx_len;                                         // ������ ����������������� ������ ��� ������ ������ (�� ���������� ��������� �������).
    int rx_bytes_count;                                 // ���������� �������� ����.
    int block_tx;
    int32_t tx_delay_ticks;                             // ����-��� � ����� �� �������� ���������� �����.
    struct ctrl_t {                                     // ������� ����������� ���������.
        volatile uint8_t tx_start: 1;                   // ����, ������������� � ���, ��� ������ ���� ��������� �������� ������.
        volatile uint8_t rx_start: 1;                   // ����, ������������� � ���, ��� ������ ���� ��������� ������ ������.
    }ctrl;
    sw_timer__sys_time_t send_time;                     // ��������� ����� ������ �������� ���������� �����. ������� ��� ��������� ��������, ����� ����� �� ������������� (�� ������������ ���� TXIFG).
                                                        // �������� ��� ��� �������� ������ � ����� �� ��������.
    uint8_t parity;
    uint8_t wordlenth9;
} hw_uart__itf_t;


// ������ ��� ��������� ��������� �� ��������� UART �� ID.
    #define HW_UART__PTR_FROM_ID_GET(uart_id)   &hw_uart.itf[uart_id - 1]
//������� ��� ��������� ID ��������� �� ��������� �� ���������
#define HW_UART__ID_FROM_PTR_GET(uart_ptr)  (&hw_uart.itf[uart_ptr] - &hw_uart.itf[0])

#if (HW_UART__COUNT > _UART__DESCRIPTOR_COUNT_)
    #error "_UART__DESCRIPTOR_COUNT_ must be more than HW_UART__COUNT"
#endif

#if HW_UART__COUNT != 0
// ������� �������� �������� ��� ������������ UART �������� ������ ������ + �������� ����-���� �� �������� ���������� �����.
static const  uint32_t hw_uart__16mhz_br_table[11] = {
    0x3412,    //2400
    0x1A05,    //4800
    0x0D02,    //9600
    0x0681,    //19200
    0x0340,    //38400
    0x0225,    //57600
    0x0112,    //115200
    0x0085,    //230400
    0x0042,    //460800
    0x0021,    //921600
};

 // ��������� �������� UART.
    typedef struct {
        hw_uart__itf_t itf[HW_UART__COUNT];
    } hw_uart__t;

    static hw_uart__t hw_uart;

    void  hw_uart__USART_IRQHandler(int uart_id);

    extern void hw_uart__tx1_vector_handler(void);
    #ifdef UART_A0
        extern void hw_uart__tx0_vector_handler(void);
        extern void hw_uart__rx0_vector_handler(void);
    #endif
    #ifdef UART_A1
        extern void hw_uart__tx1_vector_handler(void);
        extern void hw_uart__rx1_vector_handler(void);
    #endif

static void uart__1_gpio_en(void);
static void uart__2_gpio_en(void);
static void uart__3_gpio_en(void);
static void uart__4_gpio_en(void);
static void uart__5_gpio_en(void);
static void uart__6_gpio_en(void);

static void uart__1_gpio_dis(void);
static void uart__2_gpio_dis(void);
static void uart__3_gpio_dis(void);
static void uart__4_gpio_dis(void);
static void uart__5_gpio_dis(void);
static void uart__6_gpio_dis(void);
#endif


uart__event_e hw_uart__init(int uart_id, uint32_t settings, uart__err_handler_t err_handler) {

    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__init(uart_id, settings, fc_cmd_handler, err_handler);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT  != 0
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    uart->settings = 0; // ���� ����� ����� ������, �� ������� ��������� ������ ����������� - ����� ����� ������ ��������� �������.

// ������ �������������
    USART_TypeDef  *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    switch(uart_id) {
    case 1:
        uart__1_gpio_en();
        NVIC_EnableIRQ(USART1_IRQn);
    break;
    case 2:
        uart__2_gpio_en();
        NVIC_EnableIRQ(USART2_IRQn);
    break;
    case 3:
        uart__3_gpio_en();
        NVIC_EnableIRQ(USART3_IRQn);
    break;
    case 4:
        uart__4_gpio_en();
        NVIC_EnableIRQ(UART4_IRQn);
    break;
    case 5:
        uart__5_gpio_en();
        NVIC_EnableIRQ(UART5_IRQn);
    break;
    case 6:
        uart__6_gpio_en();
        NVIC_EnableIRQ(LPUART1_IRQn);
    break;
}
    MODIFY_REG(RCC->CCIPR, 0xFFF, 0xAAA);
    //�������� UARTx (x=1/2/3/4/5)
    UART->CR1 = USART_CR1_UE | USART_CR1_UESM | USART_CR1_OVER8;  // ������� �������� ���������� �� ����.
    UART->ICR = 0x1B5F | USART_ICR_CMCF | USART_ICR_WUCF;
//��������� ���� ����
    switch (settings & UART__STOP_MASK) {
        case UART__STOP_0_5:
            UART->CR2 &=~ USART_CR2_STOP;
            UART->CR2 |= USART_CR2_STOP_0;
            break;
        case UART__STOP_1:
            UART->CR2 &=~ USART_CR2_STOP;
            break;
        case UART__STOP_1_5:
            UART->CR2 |=  USART_CR2_STOP;
            break;
        case UART__STOP_2:
            UART->CR2 &=~ USART_CR2_STOP;
            UART->CR2 |= USART_CR2_STOP_1;
            break;
        default:
            return UART__EVENT_PARAM_NA;
    }
 //��������� ������ ������������ ������
    switch (settings & UART__DATA_MASK) {
        case UART__DATA_7: UART->CR1 &=~ USART_CR1_M; uart->wordlenth9 = 0; break;
        case UART__DATA_8: UART->CR1 &=~ USART_CR1_M;
        if ((settings & UART__PAR_MASK) != UART__PAR_NONE) {
        uart->wordlenth9 = 1;
        UART->CR1 |= USART_CR1_M;
        }
        else
        uart->wordlenth9 = 0;
        break;
        case UART__DATA_9: UART->CR1 |= USART_CR1_M; uart->wordlenth9 = 1; break;
        default: return UART__EVENT_PARAM_NA;
    }
// ��������� ��������
    switch (settings & UART__PAR_MASK) {
        case UART__PAR_ODD:
            uart->parity = 1;
            UART->CR1 |= USART_CR1_PCE;
            UART->CR1 |= USART_CR1_PS;
            break;
        case UART__PAR_EVEN:
            uart->parity = 1;
            UART->CR1 |= USART_CR1_PCE;
            UART->CR1 &=~ USART_CR1_PS;
            break;
        default:
            uart->parity = 0;
            UART->CR1 &=~ USART_CR1_PCE;
            break;
    }

  //�������� �� ���������� �������!
    int M = (UART->CR1 & USART_CR1_M);     // data length
    int PCE = (UART->CR1 & USART_CR1_PCE); //ParityControl
    int UserDataLenth = (settings & UART__DATA_MASK);

    if (M == 0) {
        if (PCE == 0) {
            if(UserDataLenth != UART__DATA_8)
                return UART__EVENT_PARAM_NA;
        }
        else if (PCE !=0 ) {
            if((UserDataLenth != UART__DATA_7) & (!uart->wordlenth9))
                return UART__EVENT_PARAM_NA;
        }
    }
    else if (M != 0) {
         if (PCE == 0) {
            if(UserDataLenth != UART__DATA_9)
                return UART__EVENT_PARAM_NA;
        }
        else if (PCE !=0 ) {
           if(UserDataLenth != UART__DATA_8)
                return UART__EVENT_PARAM_NA;
        }
    }
    else
        return UART__EVENT_PARAM_NA;
 //��������� �������
    UART->BRR = hw_uart__16mhz_br_table[(settings & UART__BR_MASK) - 4];
    uart->settings = settings;
    uart->err_handler = err_handler;

    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}


/*******************************************************************************
 * ������� ��������������� ���������� UART-�. ���������� ���������� UART-� ���� �������� � ��������� ���.
 ******************************************************************************/
uart__event_e hw_uart__deinit(int uart_id) {
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__deinit(uart_id);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    USART_TypeDef  *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    hw_uart__tx_break(uart_id, NULL, NULL);
    hw_uart__rx_break(uart_id, NULL, NULL);

    //��������� ��������� USART
    UART->CR1 &=~ (USART_CR1_UE | USART_CR1_RE | USART_CR1_TE);

    switch(uart_id) {
        case 1:
            NVIC_DisableIRQ(USART1_IRQn);
            uart__1_gpio_dis();
        break;
        case 2:
            NVIC_DisableIRQ(USART2_IRQn);
            uart__2_gpio_dis();
        break;
        case 3:
            NVIC_DisableIRQ(USART3_IRQn);
            uart__3_gpio_dis();
        break;
        case 4:
            NVIC_DisableIRQ(UART4_IRQn);
            uart__4_gpio_dis();
        break;
        case 5:
            NVIC_DisableIRQ(UART5_IRQn);
            uart__5_gpio_dis();
        break;
        case 6:
            NVIC_DisableIRQ(LPUART1_IRQn);
            uart__6_gpio_dis();
        break;
    }
    uart->settings = 0;
    uart->err_handler = NULL;
    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� �������� ���������������� ������ � ��������� UART.
 ******************************************************************************/
uart__event_e hw_uart__tx(int uart_id, uint8_t *buff, int len, hw_uart__cb_t tx_isr_cb) {
    if ((buff == NULL) || (len == 0))
        return UART__EVENT_PARAM_NA;
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__tx(uart_id, buff, len, tx_isr_cb);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);

    USART_TypeDef  *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    CRITICAL_SECTION_ON
    uart->ctrl.tx_start = 1;
    uart->tx_buff = buff;
    uart->tx_len = len;
    uart->tx_isr_cb = tx_isr_cb;
    uart->tx_bytes_count = 0;

    // ��������
    UART->ICR |= USART_ICR_TCCF;
    int temp = uart->tx_buff[uart->tx_bytes_count];

    if (uart->wordlenth9 == 1) {
        temp = (uart->tx_buff[uart->tx_bytes_count] & 0x01FF);
    }
    else {
        temp = uart->tx_buff[uart->tx_bytes_count];
    }
    UART->TDR = temp;
    UART->CR1 |= (USART_CR1_TE | USART_CR1_TXEIE | USART_CR1_TCIE); //��������� ���������� �� TXE � TC
    CRITICAL_SECTION_OFF

    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� ������ ������ �� ���������� �����.
 ******************************************************************************/
uart__event_e hw_uart__rx(int uart_id, uint8_t *buff, int len, hw_uart__cb_t rx_isr_cb) {
    if (buff == NULL || len == 0 || rx_isr_cb == NULL)
        return UART__EVENT_PARAM_NA;
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__rx(uart_id, buff, len, rx_isr_cb);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    USART_TypeDef *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    CRITICAL_SECTION_ON
    uart->ctrl.rx_start = 1;
    uart->rx_buff = buff;
    uart->rx_len = len;
    uart->rx_bytes_count = 0;
    uart->rx_isr_cb = rx_isr_cb;
    //���������� ���������� � ���������� �������� ������ (RXNE)
//    UART->ICR |= USART_ISR_RXNE;

    UART->CR1 |= USART_CR1_RXNEIE;
    //��������� �������� �� ������
    UART->CR1 |= USART_CR1_PEIE;  //�������� ���������� �� Parity erorr
    UART->CR3 |= USART_CR3_EIE;   //�������� ���������� �� FRAGMING/OVERFLOW/NOISE
    // �������� recieve
    UART->CR1 |= USART_CR1_RE;
    CRITICAL_SECTION_OFF
    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� ����������� ���������� �������� �������� ������ ��������� UART-��.
 ******************************************************************************/
uart__event_e hw_uart__tx_break(int uart_id, uint8_t **buff, int *len) {
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__tx_break(uart_id, buff, len);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    USART_TypeDef *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    //��������� �����������
    UART->CR1 &=~ (USART_CR1_TE | USART_CR1_TCIE | USART_CR1_TXEIE);
    uart->ctrl.tx_start = 0;

    if (buff != NULL)
        *buff = uart->tx_buff;
    if (len != NULL)
        *len = uart->tx_bytes_count;
    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� ����������� ���������� �������� ������ ������ ��������� UART-��.
 ******************************************************************************/
uart__event_e hw_uart__rx_break(int uart_id, uint8_t **buff, int *len) {
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__rx_break(uart_id, buff, len);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    USART_TypeDef *UART = (USART_TypeDef *) usart__mass[uart_id];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    UART->CR1 &=~ (USART_CR1_RE | USART_CR1_RXNEIE | USART_CR1_PEIE);
    UART->CR3 &=~ USART_CR3_EIE;
    uart->ctrl.rx_start = 0;
    if (buff != NULL)
        *buff = uart->rx_buff;
    if (len != NULL)
        *len = uart->rx_bytes_count;
    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� �������� ������ ��� ��������� �������� ��������� �������� UART.
 ******************************************************************************/
void hw_uart__cout(void) {
}

/*******************************************************************************
 * ���������� �������-���������� ���������� �� ������ � �������.
 ******************************************************************************/
void hw_uart__USART_IRQHandler(int uart_id) {
#if HW_UART__COUNT != 0
    USART_TypeDef *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    uart__event_e event = UART__EVENT_FRAMING_ERR;

   hw_uart__isr_u status_reg;
   status_reg.isr_u32 = UART->ISR;

   __no_operation();

   //��������� �� ������� ������
   if (status_reg.isr.PE |status_reg.isr.FE | status_reg.isr.NE | status_reg.isr.ORE) {
       // �������� ��������.
       if (status_reg.isr.PE){
           event= UART__EVENT_PARITY_ERR;
       }
       // �������� �����. ���� ��� ������ ����, ������ ����� ������ ��������� TX/RX
       else if (status_reg.isr.FE) {
           event = UART__EVENT_FRAMING_ERR;
       }
       // �������� ������������.
       else if (status_reg.isr.ORE) {
           event = UART__EVENT_OVERFLOW;
       }
       // �������� �� ����.
       else if (status_reg.isr.NE) {
           event = UART__EVENT_NOISE;
       }

       UART->CR1 &=~ (USART_CR1_RE | USART_CR1_PEIE | USART_CR1_RXNEIE);  //��������� ���������� �� Parity erorr
       UART->CR3 &=~ USART_CR3_EIE;   //��������� ���������� �� FRAGMING/OVERFLOW/NOISE
       UART->ICR = 0x1FFFFF;
       if (uart->ctrl.rx_start) {
           uart->ctrl.rx_start = 0;
           uart->rx_isr_cb(uart_id, event, uart->rx_buff, uart->rx_bytes_count);
       }
       if (uart->ctrl.tx_start) {
           uart->ctrl.tx_start = 0;
           uart->tx_isr_cb(uart_id, event, uart->tx_buff, uart->tx_bytes_count);
       }
       return;
   }

   //RECIEVE
    if ((UART->CR1 & USART_CR1_RXNEIE) && (status_reg.isr.RXNE)) {
        //��� ��� ���� ��������� ��� ������ ��� ����������, ��� ���������� ��������� � �����
        volatile int buffer = UART->RDR;
        int temp = buffer;

        if (uart->wordlenth9 == 1) {
            if (uart->parity == 1) {
                temp = (buffer & 0x00FF);
            }
            else {
                temp = (buffer & 0x01FF);
            }
        }
        else {
            if (uart->parity == 1) {
                temp = (buffer & 0x007F);
            }
            else {
                temp = (buffer & 0x00FF);
            }
        }


        //���� ��� �� ��������� -> ���������� ����� �� ���� ������ overrun
        UART->RQR |= USART_RQR_RXFRQ;
        if (uart->ctrl.rx_start) {
            //���� ������ ��� ������ �� ��������
            uart->rx_buff[uart->rx_bytes_count] = temp;
            uart->rx_bytes_count++;
            if (uart->rx_bytes_count >= uart->rx_len) {
                uart->ctrl.rx_start = 0;
                //                        UART->CR1 &=~ (USART_CR1_RE | USART_CR1_PEIE | USART_CR1_RXNEIE);  //�������� ���������� �� Parity erorr
                UART->CR3 &=~ USART_CR3_EIE;   //�������� ���������� �� FRAGMING/OVERFLOW/NOISE
                uart->rx_isr_cb(uart_id, UART__EVENT_OK, uart->rx_buff, uart->rx_bytes_count);
                if (!uart->ctrl.rx_start)
                    UART->CR1 &=~ (USART_CR1_RE | USART_CR1_RXNEIE | USART_CR1_PEIE);

            }
        }
        //        return;
    }
   if ((UART->CR1 & USART_CR1_TE) && ((status_reg.isr.TXE) || (status_reg.isr.TC))) {
        if (READ_BIT(UART->ISR, USART_ISR_TXE) && READ_BIT(UART->CR1, USART_CR1_TXEIE)) { //���� ����� �� ������
            UART->RQR |= USART_RQR_TXFRQ;

            if (uart->ctrl.tx_start) {
                uart->tx_bytes_count++;

                int temp = (uint16_t)uart->tx_buff[uart->tx_bytes_count];
                if (uart->wordlenth9 == 1) {
                    temp = ((uint16_t)uart->tx_buff[uart->tx_bytes_count] & 0x01FF);
                    if (uart->parity == 1)
                       ;;
                }

                if (uart->tx_bytes_count < uart->tx_len -1) {
                    // �������� len -1 ����
                   UART->TDR = temp;
                }
                else {
                    //�������� ���������  ����
                     UART->CR1 &=~ (USART_CR1_TXEIE); //�������� TXE
                     UART->CR1 |= (USART_CR1_TCIE); //��������� ���������� �� TC
                     if (uart->tx_len != 1) {
                     UART->TDR = temp;
                     }
                     return;
                }
            }
            else {
                UART->CR1 &=~ (USART_CR1_TCIE | USART_CR1_TXEIE | USART_CR1_TE);
            }
        }
        //��������� ���������� ��������� �������� � ���� ������
        else if (READ_BIT(UART->ISR, USART_ISR_TC) && READ_BIT(UART->CR1, USART_CR1_TCIE)) {

                if (!uart->ctrl.tx_start)
                    return;
                uart->ctrl.tx_start = 0;
                    uart->tx_bytes_count++;
                    uart->tx_isr_cb(uart_id, UART__EVENT_OK, uart->tx_buff, uart->tx_bytes_count);
                    if (!uart->ctrl.tx_start) {
                        UART->CR1 &=~ (USART_CR1_TCIE | USART_CR1_TE);
                        UART->ICR |= USART_ICR_TCCF;
                    }
        }
    }
#endif
}


/*******************************************************************************
 * ������� ������������� GPIO ���������� � UART_1.
 ******************************************************************************/
#if HW_UART__COUNT != 0
static void uart__1_gpio_en(void) {
    // �������� STM32 UART �������������
    // �������� ������������ ��� ��������� ����� � �����
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    /*PA9     ------> USART1_TX
    PA10     ------> USART1_RX */
    //����������� �� �������������� ������� ���� PA9
    GPIOA->MODER &=~ GPIO_MODER_MODER9_0;
    GPIOA->MODER |= GPIO_MODER_MODER9_1;
    //����������� ���� PA9 �� very high speed
    GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR9_1 | GPIO_OSPEEDER_OSPEEDR9_0);
    //�������� pull-up
    GPIOA->PUPDR |= GPIO_PUPDR_PUPDR9_0;
    GPIOA->PUPDR &=~ GPIO_PUPDR_PUPDR9_1;
    //��� ������ push-pull
    GPIOA->OTYPER &=~ GPIO_OTYPER_OT_9;
    //��������� �������������� �������
    if ((GPIOA->AFR[1] & GPIO_AFRH_AFSEL9) != (0x07 << GPIO_AFRH_AFSEL9_Pos)) {
        GPIOA->AFR[1] &=~ GPIO_AFRH_AFSEL9;
        GPIOA->AFR[1] |=  0x07 << GPIO_AFRH_AFSEL9_Pos;
    }
    //����������� ���� PA10
    GPIOA->MODER &=~ GPIO_MODER_MODER10_0;
    GPIOA->MODER |= GPIO_MODER_MODER10_1;
    GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR10_1 | GPIO_OSPEEDER_OSPEEDR10_0);
    GPIOA->PUPDR |= GPIO_PUPDR_PUPDR10_0;
    GPIOA->PUPDR &=~ GPIO_PUPDR_PUPDR10_1;
    GPIOA->OTYPER &=~ GPIO_OTYPER_OT_10;
    if ((GPIOA->AFR[1] & GPIO_AFRH_AFSEL10) != (0x07 << GPIO_AFRH_AFSEL10_Pos)) {
        GPIOA->AFR[1] &=~ GPIO_AFRH_AFSEL10;
        GPIOA->AFR[1] |=  0x07 << GPIO_AFRH_AFSEL10_Pos;
    }
    RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
//    USART1->ICR = 0xFFFFFFFF;
}

/*******************************************************************************
 * ������� ������������� GPIO ���������� � UART_2.
 ******************************************************************************/

static void uart__2_gpio_en(void) {
    // �������� STM32 UART �������������
    // �������� ������������ ��� ��������� ����� � �����
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    /*PA2     ------> USART2_TX
    PA3     ------> USART2_RX */
    //����������� �� �������������� ������� ���� PA2
    GPIOA->MODER &=~ GPIO_MODER_MODER2_0;
    GPIOA->MODER |= GPIO_MODER_MODER2_1;
    //����������� ���� PA9 �� very high speed
    GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR2_0);
    //�������� pull-up
    GPIOA->PUPDR |= GPIO_PUPDR_PUPDR2_0;
    GPIOA->PUPDR &=~ GPIO_PUPDR_PUPDR2_1;
    //��� ������ push-pull
    GPIOA->OTYPER &=~ GPIO_OTYPER_OT_2;
    //��������� �������������� �������
    if ((GPIOA->AFR[0] & GPIO_AFRL_AFRL2) != 0x00000700 ) {
        GPIOA->AFR[0] &=~ GPIO_AFRL_AFRL2;
        GPIOA->AFR[0] |=  ((uint32_t)0x00000700);
    }
    //����������� ���� PA3
    GPIOA->MODER &=~ GPIO_MODER_MODER3_0;
    GPIOA->MODER |= GPIO_MODER_MODER3_1;
    GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR3_1 | GPIO_OSPEEDER_OSPEEDR3_0);
    GPIOA->PUPDR |= GPIO_PUPDR_PUPDR3_0;
    GPIOA->PUPDR &=~ GPIO_PUPDR_PUPDR3_1;
    GPIOA->OTYPER &=~ GPIO_OTYPER_OT_3;
    if ((GPIOA->AFR[0] & GPIO_AFRL_AFRL3) != 0x00007000 ) {
        GPIOA->AFR[0] &=~ GPIO_AFRL_AFRL3;
        GPIOA->AFR[0] |=  ((uint32_t)0x00007000);
    }
    RCC->APB1ENR1 |= RCC_APB1ENR1_USART2EN;
}

/*******************************************************************************
 * ������� ������������� GPIO ���������� � UART_3.
 ******************************************************************************/
static void uart__3_gpio_en(void) {
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
    /*PB10     ------> USART1_TX
    PB11     ------> USART1_RX */
    //����������� �� �������������� ������� ���� PB10
    GPIOB->MODER &=~ GPIO_MODER_MODER10_0;
    GPIOB->MODER |= GPIO_MODER_MODER10_1;
    //����������� ���� PB10 �� very high speed
    GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR10_1 | GPIO_OSPEEDER_OSPEEDR10_0);
    //�������� pull-up
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR10_0;
    GPIOB->PUPDR &=~ GPIO_PUPDR_PUPDR10_1;
    //��� ������ push-pull
    GPIOB->OTYPER &=~ GPIO_OTYPER_OT_10;
    //��������� �������������� �������
    if ((GPIOB->AFR[1] & GPIO_AFRH_AFSEL10) != (0x07 << GPIO_AFRH_AFSEL10_Pos)) {
        GPIOB->AFR[1] &=~ GPIO_AFRH_AFSEL10;
        GPIOB->AFR[1] |=  0x07 << GPIO_AFRH_AFSEL10_Pos;
    }
    //����������� ���� PB11
    GPIOB->MODER &=~ GPIO_MODER_MODER11_0;
    GPIOB->MODER |= GPIO_MODER_MODER11_1;
    GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR11_1 | GPIO_OSPEEDER_OSPEEDR11_0);
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR11_0;
    GPIOB->PUPDR &=~ GPIO_PUPDR_PUPDR11_1;
    GPIOB->OTYPER &=~ GPIO_OTYPER_OT_11;
    if ((GPIOB->AFR[1] & GPIO_AFRH_AFSEL11) != (0x07 << GPIO_AFRH_AFSEL11_Pos)) {
        GPIOB->AFR[1] &=~ GPIO_AFRH_AFSEL11;
        GPIOB->AFR[1] |=  0x07 << GPIO_AFRH_AFSEL11_Pos;
    }
    RCC->APB1ENR1 |= RCC_APB1ENR1_USART3EN;
}
/*******************************************************************************
 * ������� ������������� GPIO ���������� � UART_4.
 ******************************************************************************/
static void uart__4_gpio_en(void) {
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    /*PA00     ------> USART4_TX
    PA01     ------> USART4_RX */
    //����������� �� �������������� ������� ���� PB00
    GPIOA->MODER &=~ GPIO_MODER_MODER0_0;
    GPIOA->MODER |= GPIO_MODER_MODER0_1;
    //����������� ���� PB00 �� very high speed
    GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR0_1 | GPIO_OSPEEDER_OSPEEDR0_0);
    //�������� pull-up
    GPIOA->PUPDR |= GPIO_PUPDR_PUPDR0_0;
    GPIOA->PUPDR &=~ GPIO_PUPDR_PUPDR0_1;
    //��� ������ push-pull
    GPIOA->OTYPER &=~ GPIO_OTYPER_OT_0;
    //��������� �������������� �������
    if ((GPIOA->AFR[0] & GPIO_AFRL_AFSEL0) != (0x07 << GPIO_AFRL_AFSEL0_Pos)) {
        GPIOA->AFR[0] &=~ GPIO_AFRL_AFSEL0;
        GPIOA->AFR[0] |=  0x07 << GPIO_AFRL_AFSEL0_Pos;
    }
    //����������� ���� PB01
    GPIOA->MODER &=~ GPIO_MODER_MODER1_0;
    GPIOA->MODER |= GPIO_MODER_MODER1_1;
    GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR1_1 | GPIO_OSPEEDER_OSPEEDR1_0);
    GPIOA->PUPDR |= GPIO_PUPDR_PUPDR1_0;
    GPIOA->PUPDR &=~ GPIO_PUPDR_PUPDR1_1;
    GPIOA->OTYPER &=~ GPIO_OTYPER_OT_1;
    if ((GPIOA->AFR[0] & GPIO_AFRL_AFSEL1) != (0x07 << GPIO_AFRL_AFSEL1_Pos)) {
        GPIOA->AFR[0] &=~ GPIO_AFRL_AFSEL1;
        GPIOA->AFR[0] |=  0x07 << GPIO_AFRL_AFSEL1_Pos;
    }
    RCC->APB1ENR1 |= RCC_APB1ENR1_UART4EN;
}
/*******************************************************************************
 * ������� ������������� GPIO ���������� � UART_5.
 ******************************************************************************/
static void uart__5_gpio_en(void) {
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN | RCC_AHB2ENR_GPIODEN;
    /*PC12     ------> USART5_TX
      PD2   ------> USART5_RX */
    //����������� �� �������������� ������� ���� PC12
    GPIOC->MODER &=~ GPIO_MODER_MODER12_0;
    GPIOC->MODER |= GPIO_MODER_MODER12_1;
    //����������� ���� PC12 �� very high speed
    GPIOC->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR12_1 | GPIO_OSPEEDER_OSPEEDR12_0);
    //�������� pull-up
    GPIOC->PUPDR |= GPIO_PUPDR_PUPDR12_0;
    GPIOC->PUPDR &=~ GPIO_PUPDR_PUPDR12_1;
    //��� ������ push-pull
    GPIOC->OTYPER &=~ GPIO_OTYPER_OT_12;
    //��������� �������������� �������
    if ((GPIOC->AFR[1] & GPIO_AFRH_AFSEL12) != (0x07 << GPIO_AFRH_AFSEL12_Pos)) {
        GPIOC->AFR[1] &=~ GPIO_AFRH_AFSEL12;
        GPIOC->AFR[1] |=  0x07 << GPIO_AFRH_AFSEL12_Pos;
    }
    //����������� ���� PD2
    GPIOD->MODER &=~ GPIO_MODER_MODER2_0;
    GPIOD->MODER |= GPIO_MODER_MODER2_1;
    GPIOD->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR2_0);
    GPIOD->PUPDR |= GPIO_PUPDR_PUPDR2_0;
    GPIOD->PUPDR &=~ GPIO_PUPDR_PUPDR2_1;
    GPIOD->OTYPER &=~ GPIO_OTYPER_OT_2;
    if ((GPIOD->AFR[0] & GPIO_AFRL_AFSEL2) != (0x07 << GPIO_AFRL_AFSEL2_Pos)) {
        GPIOD->AFR[0] &=~ GPIO_AFRL_AFSEL2;
        GPIOD->AFR[0] |=  0x07 << GPIO_AFRL_AFSEL2_Pos;
    }
    RCC->APB1ENR1 |= RCC_APB1ENR1_UART5EN;
}
/*******************************************************************************
 * ������� ������������� GPIO ���������� � UART_6.
 ******************************************************************************/
static void uart__6_gpio_en(void) {
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;
    /*PC01     ------> USART6_TX
      PC00   ------> USART6_RX */
    //����������� �� �������������� ������� ���� PC1
    GPIOC->MODER &=~ GPIO_MODER_MODER1_0;
    GPIOC->MODER |= GPIO_MODER_MODER1_1;
    //����������� ���� PC1 �� very high speed
    GPIOC->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR1_1 | GPIO_OSPEEDER_OSPEEDR1_0);
    //�������� pull-up
    GPIOC->PUPDR |= GPIO_PUPDR_PUPDR1_0;
    GPIOC->PUPDR &=~ GPIO_PUPDR_PUPDR1_1;
    //��� ������ push-pull
    GPIOC->OTYPER &=~ GPIO_OTYPER_OT_1;
    //��������� �������������� �������
    if ((GPIOC->AFR[0] & GPIO_AFRL_AFSEL1) != (0x07 << GPIO_AFRL_AFSEL1_Pos)) {
        GPIOC->AFR[0] &=~ GPIO_AFRL_AFSEL1;
        GPIOC->AFR[0] |=  0x07 << GPIO_AFRL_AFSEL1_Pos;
    }
    //����������� ���� PC0
    GPIOC->MODER &=~ GPIO_MODER_MODER0_0;
    GPIOC->MODER |= GPIO_MODER_MODER0_1;
    GPIOC->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR0_1 | GPIO_OSPEEDER_OSPEEDR0_0);
    GPIOC->PUPDR |= GPIO_PUPDR_PUPDR0_0;
    GPIOC->PUPDR &=~ GPIO_PUPDR_PUPDR0_1;
    GPIOC->OTYPER &=~ GPIO_OTYPER_OT_0;
    if ((GPIOC->AFR[0] & GPIO_AFRL_AFSEL0) != (0x07 << GPIO_AFRL_AFSEL0_Pos)) {
        GPIOC->AFR[0] &=~ GPIO_AFRL_AFSEL0;
        GPIOC->AFR[0] |=  0x07 << GPIO_AFRL_AFSEL0_Pos;
    }
    RCC->APB1ENR2 |= RCC_APB1ENR2_LPUART1EN;
}



/*******************************************************************************
 * ������� ���������������  UART_1.
 ******************************************************************************/
static void uart__1_gpio_dis(void) {
//    // �������� STM32 UART �������������
//    // �������� ������������ ��� ��������� ����� � �����
    RCC->APB2ENR &=~ RCC_APB2ENR_USART1EN;
//   /*PA9     ------> USART1_TX
//    PA10     ------> USART1_RX*/
//    //������ ���
    GPIOA->MODER |= (GPIO_MODER_MODER9_1 | GPIO_MODER_MODER9_0);
//    //����������� ���� PA9 �� very high speed
    GPIOA->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR9_1 | GPIO_OSPEEDER_OSPEEDR9_0);
//    //�������� pull-up
    GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR9_1 | GPIO_PUPDR_PUPDR9_0);
//    //��������� �������������� �������
    GPIOA->AFR[1] &=~ GPIO_AFRH_AFSEL9;
//    //����������� ���� PA10
    GPIOA->MODER |= (GPIO_MODER_MODER10_0 | GPIO_MODER_MODER10_1);
    GPIOA->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR10_1 | GPIO_OSPEEDER_OSPEEDR10_0);
    GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR10_0 | GPIO_PUPDR_PUPDR10_1);
    GPIOA->AFR[1] &=~ GPIO_AFRH_AFSEL10;
}

/*******************************************************************************
 * ������� ���������������  UART_2.
 ******************************************************************************/
static void uart__2_gpio_dis(void) {
    // �������� STM32 UART �������������
    // �������� ������������ ��� ��������� ����� � �����
    RCC->APB1ENR1 &=~ RCC_APB1ENR1_USART2EN;
    /*PA2     ------> USART2_TX
    PA3     ------> USART2_RX */
    //����������� �� �������������� ������� ���� PA2
    GPIOA->MODER |= (GPIO_MODER_MODER2_0 | GPIO_MODER_MODER2_1);
    GPIOA->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR2_0);
    GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR2_1 | GPIO_PUPDR_PUPDR2_0);
    GPIOA->AFR[0] &=~ GPIO_AFRL_AFSEL2;
    //����������� ���� PA3
    GPIOA->MODER |= (GPIO_MODER_MODER3_1 | GPIO_MODER_MODER3_0);
    GPIOA->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR3_1 | GPIO_OSPEEDER_OSPEEDR3_0);
    GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR3_0 | GPIO_PUPDR_PUPDR3_1);
    GPIOA->AFR[0] &=~ GPIO_AFRL_AFSEL3;
}

/*******************************************************************************
 * ������� ���������������  UART_3.
 ******************************************************************************/
static void uart__3_gpio_dis(void) {
//    /*PC04     ------> USART3_TX
//    PC05     ------> USART3_RX  */
    RCC->APB1ENR1 &=~ RCC_APB1ENR1_USART3EN;
    //����������� �� �������������� ������� ���� PC4
    GPIOC->MODER |= (GPIO_MODER_MODER4_0 | GPIO_MODER_MODER4_1);
    GPIOC->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR4_1 | GPIO_OSPEEDER_OSPEEDR4_0);
    GPIOC->PUPDR &=~ (GPIO_PUPDR_PUPDR4_1 | GPIO_PUPDR_PUPDR4_0);
    GPIOC->AFR[0] &=~ GPIO_AFRL_AFSEL4;
    //����������� ���� PC5
    GPIOC->MODER |= (GPIO_MODER_MODER5_1 | GPIO_MODER_MODER5_0);
    GPIOC->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR5_1 | GPIO_OSPEEDER_OSPEEDR5_0);
    GPIOC->PUPDR &=~ (GPIO_PUPDR_PUPDR5_0 | GPIO_PUPDR_PUPDR5_1);
    GPIOC->AFR[0] &=~ GPIO_AFRL_AFSEL5;
}
/*******************************************************************************
 * ������� ���������������  UART_4.
 ******************************************************************************/
static void uart__4_gpio_dis(void) {
//    /*PA00     ------> USART6_TX
//    PA01     ------> USART6_RX  */
    RCC->APB1ENR1 &=~ RCC_APB1ENR1_UART4EN;
    //����������� �� �������������� ������� ���� PC4
    GPIOA->MODER |= (GPIO_MODER_MODER0_0 | GPIO_MODER_MODER0_1);
    GPIOA->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR0_1 | GPIO_OSPEEDER_OSPEEDR0_0);
    GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR0_1 | GPIO_PUPDR_PUPDR0_0);
    GPIOA->AFR[0] &=~ GPIO_AFRL_AFSEL0;
    //����������� ���� PC5
    GPIOA->MODER |= (GPIO_MODER_MODER1_1 | GPIO_MODER_MODER1_0);
    GPIOA->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR1_1 | GPIO_OSPEEDER_OSPEEDR1_0);
    GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR1_0 | GPIO_PUPDR_PUPDR1_1);
    GPIOA->AFR[0] &=~ GPIO_AFRL_AFSEL1;
}
/*******************************************************************************
 * ������� ���������������  UART_5.
 ******************************************************************************/
static void uart__5_gpio_dis(void) {
//    /*PC12     ------> UART5_TX
//     PD2    ------> UART5_RX  */
    RCC->APB1ENR1 &=~ RCC_APB1ENR1_UART5EN;
    //����������� �� �������������� ������� ���� PC12
    GPIOC->MODER |= (GPIO_MODER_MODER12_0 | GPIO_MODER_MODER12_1);
    GPIOC->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR12_1 | GPIO_OSPEEDER_OSPEEDR12_0);
    GPIOC->PUPDR &=~ (GPIO_PUPDR_PUPDR12_1 | GPIO_PUPDR_PUPDR12_0);
    GPIOC->AFR[1] &=~ GPIO_AFRH_AFSEL12;
    //����������� ���� PD2
    GPIOD->MODER |= (GPIO_MODER_MODER2_1 | GPIO_MODER_MODER2_0);
    GPIOD->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR2_0);
    GPIOD->PUPDR &=~ (GPIO_PUPDR_PUPDR2_0 | GPIO_PUPDR_PUPDR2_1);
    GPIOD->AFR[0] &=~ GPIO_AFRL_AFSEL2;
}
/*******************************************************************************
 * ������� ���������������  UART_6 (LPUART1).
 ******************************************************************************/
static void uart__6_gpio_dis(void) {
//    /*PC01     ------> USART6_TX
//    PC00     ------> USART6_RX  */
    RCC->APB1ENR2 &= ~RCC_APB1ENR2_LPUART1EN;
    //����������� �� �������������� ������� ���� PC12
    GPIOC->MODER |= (GPIO_MODER_MODER1_0 | GPIO_MODER_MODER1_1);
    GPIOC->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR1_1 | GPIO_OSPEEDER_OSPEEDR1_0);
    GPIOC->PUPDR &=~ (GPIO_PUPDR_PUPDR1_1 | GPIO_PUPDR_PUPDR1_0);
    GPIOC->AFR[0] &=~ GPIO_AFRL_AFSEL1;
    //����������� ���� PD2
    GPIOC->MODER |= (GPIO_MODER_MODER0_1 | GPIO_MODER_MODER0_0);
    GPIOC->OSPEEDR &=~ (GPIO_OSPEEDER_OSPEEDR0_1 | GPIO_OSPEEDER_OSPEEDR0_0);
    GPIOC->PUPDR &=~ (GPIO_PUPDR_PUPDR0_0 | GPIO_PUPDR_PUPDR0_1);
    GPIOC->AFR[0] &=~ GPIO_AFRL_AFSEL0;
}

#endif

