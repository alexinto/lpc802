/***************************************************************************//**
 * @file interrupts.c.
 * @brief �������� ������������ ���������� �� (���������� ��� stm32l1).
 * @authors p.luchnikov
 ******************************************************************************/
#include <stdint.h>
#include "standard.h"
#include "device.h"
#include "target.h"
#include "mcu.h"
#include "supervisor.h"

extern void hw_uart__tx1_vector_handler(void);
extern void hw_uart__USART_IRQHandler(int uart_id);

extern void hw_spi__SPI_IRQHandler(int spi_id);

extern void hw_i2c__I2C_EV_IRQHandler(int i2c_id);
extern void hw_i2c__I2C_ER_IRQHandler(int i2c_id);

extern void hw_timer__RTC_IRQHandler(void);
extern void hw_timer__RTC_IRQ_OVF_Handler(void);

extern void hw_timer__TIM9_IRQHandler(void);

extern void wdt__lsi_freq_calc_handler(void);

extern void gpio__EXTI_IRQHandler(int gpio__exti_port, int gpio__exti_pin);

extern void hw_uart_usb__IRQ(void);

static void interrupts__EXTI_IRQ_AF(int gpio__exti_pin);

/*******************************************************************************
 * �������-���������� ���������� WatchDog.
 ******************************************************************************/
void WWDG_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� Power Voltage Detector.
 ******************************************************************************/
void PVD_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� Tamper and Time Stamp.
 ******************************************************************************/
void TAMPER_STAMP_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� �� RTC Wakeup.
 ******************************************************************************/
void RTC_WKUP_IRQHandler(void) {
    SET_BIT(EXTI->PR1, 1<<20);
    CLEAR_BIT(RTC->ISR, RTC_ISR_WUTF);
    hw_timer__RTC_IRQ_OVF_Handler();
}

/*******************************************************************************
 * �������-���������� ���������� FLASH.
 ******************************************************************************/
void FLASH_IRQHandler(void) {
	FLASH->SR &=~ FLASH_SR_EOP;
}

/*******************************************************************************
 * �������-���������� ���������� RCC.
 ******************************************************************************/
void RCC_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������- �������� ��������� ��������������� ������ �����.
 ******************************************************************************/
static void interrupts__EXTI_IRQ_AF(int interrupts__exti_pin) {
    EXTI->PR1 |= (1 << interrupts__exti_pin);
    int port = ((SYSCFG->EXTICR[interrupts__exti_pin/4] >> ((interrupts__exti_pin%4) * 4))) & 0x0f;
    uint32_t gpio_conf = ((((GPIO_TypeDef *) (GPIOA_BASE + (0x400*port))) -> MODER) >> (interrupts__exti_pin*2)) & 0x03;
    if (gpio_conf < 2)
        gpio__EXTI_IRQHandler(port, interrupts__exti_pin);
    else {
        // todo �������������� �������/������ ���� �� �����������
        // __NOP();
    }
}

/*******************************************************************************
 * �������-���������� ���������� EXTI Line 0.
 ******************************************************************************/
void EXTI0_IRQHandler(void) {
    interrupts__EXTI_IRQ_AF(0);
}


/*******************************************************************************
 * �������-���������� ���������� EXTI Line 1.
 ******************************************************************************/
void EXTI1_IRQHandler(void) {
    interrupts__EXTI_IRQ_AF(1);
}

/*******************************************************************************
 * �������-���������� ���������� EXTI Line 2.
 ******************************************************************************/
void EXTI2_IRQHandler(void) {
	interrupts__EXTI_IRQ_AF(2);
}

/*******************************************************************************
 * �������-���������� ���������� EXTI Line 3.
 ******************************************************************************/
void EXTI3_IRQHandler(void) {
	interrupts__EXTI_IRQ_AF(3);
}

/*******************************************************************************
 * �������-���������� ���������� EXTI Line 4.
 ******************************************************************************/
void EXTI4_IRQHandler(void) {
    interrupts__EXTI_IRQ_AF(4);
}

/*******************************************************************************
 * �������-���������� ���������� EXTI Line 9..5.
 ******************************************************************************/
void EXTI9_5_IRQHandler(void) {
    if (EXTI->PR1 & (1 << 5)) interrupts__EXTI_IRQ_AF(5);
    if (EXTI->PR1 & (1 << 6)) interrupts__EXTI_IRQ_AF(6);
    if (EXTI->PR1 & (1 << 7)) interrupts__EXTI_IRQ_AF(7);
    if (EXTI->PR1 & (1 << 8)) interrupts__EXTI_IRQ_AF(8);
    if (EXTI->PR1 & (1 << 9)) interrupts__EXTI_IRQ_AF(9);
}

/*******************************************************************************
 * �������-���������� ���������� EXTI Line 15..10.
 ******************************************************************************/
void EXTI15_10_IRQHandler(void) {
    if (EXTI->PR1 & (1 << 10)) interrupts__EXTI_IRQ_AF(10);
    if (EXTI->PR1 & (1 << 11)) interrupts__EXTI_IRQ_AF(11);
    if (EXTI->PR1 & (1 << 12)) interrupts__EXTI_IRQ_AF(12);
    if (EXTI->PR1 & (1 << 13)) interrupts__EXTI_IRQ_AF(13);
    if (EXTI->PR1 & (1 << 14)) interrupts__EXTI_IRQ_AF(14);
    if (EXTI->PR1 & (1 << 15)) interrupts__EXTI_IRQ_AF(15);
}

/*******************************************************************************
 * �������-���������� ���������� ADC1.
 ******************************************************************************/
void ADC1_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� DAC.
 ******************************************************************************/
void DAC_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� COMP through EXTI Line.
 ******************************************************************************/
void COMP_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� TIM9.
 ******************************************************************************/
void TIM9_IRQHandler(void) {
    while(1);
}

/*******************************************************************************
 * �������-���������� ���������� TIM10.
 ******************************************************************************/
void TIM10_IRQHandler(void) {
#ifndef WDT__NOT_USED
    wdt__lsi_freq_calc_handler();
#endif
}

/*******************************************************************************
 * �������-���������� ���������� TIM11.
 ******************************************************************************/
void TIM11_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� TIM2.
 ******************************************************************************/
void TIM2_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� TIM3.
 ******************************************************************************/
void TIM3_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� TIM4.
 ******************************************************************************/
void TIM4_IRQHandler(void) {
	while(1);
}

/*******************************************************************************
 * �������-���������� ���������� I2C1 Event.
 ******************************************************************************/
void I2C1_EV_IRQHandler(void) {
#if defined (I2C__DESCRIPTOR_COUNT) && I2C__DESCRIPTOR_COUNT != 0 && !defined (I2C1_USE_BLOCKED)
    hw_i2c__I2C_EV_IRQHandler(I2C_1);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� I2C1 Error.
 ******************************************************************************/
void I2C1_ER_IRQHandler(void) {
#if defined (I2C__DESCRIPTOR_COUNT) && I2C__DESCRIPTOR_COUNT != 0 && !defined (I2C1_USE_BLOCKED)
	hw_i2c__I2C_ER_IRQHandler(I2C_1);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� I2C2 Event.
 ******************************************************************************/
void I2C2_EV_IRQHandler(void) {
#if defined (I2C__DESCRIPTOR_COUNT) && I2C__DESCRIPTOR_COUNT != 0 && !defined (I2C2_USE_BLOCKED)
#ifdef HW_I2C__DEBUG_PIN1
    gpio__set(HW_I2C__DEBUG_PIN1, GPIO__STATE_HIGH);
#endif

    hw_i2c__I2C_EV_IRQHandler(I2C_2);

#ifdef HW_I2C__DEBUG_PIN1
    gpio__set(HW_I2C__DEBUG_PIN1, GPIO__STATE_LOW);
#endif
#endif
}

/*******************************************************************************
 * �������-���������� ���������� I2C2 Error.
 ******************************************************************************/
void I2C2_ER_IRQHandler(void) {
#if defined (I2C__DESCRIPTOR_COUNT) && I2C__DESCRIPTOR_COUNT != 0 && !defined (I2C2_USE_BLOCKED)
	hw_i2c__I2C_ER_IRQHandler(I2C_2);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� I2C3 Event.
 ******************************************************************************/
void I2C3_EV_IRQHandler(void) {
#if defined (I2C__DESCRIPTOR_COUNT) && I2C__DESCRIPTOR_COUNT != 0 && !defined (I2C3_USE_BLOCKED)
#ifdef HW_I2C__DEBUG_PIN1
    gpio__set(HW_I2C__DEBUG_PIN1, GPIO__STATE_HIGH);
#endif

    hw_i2c__I2C_EV_IRQHandler(I2C_3);

#ifdef HW_I2C__DEBUG_PIN1
    gpio__set(HW_I2C__DEBUG_PIN1, GPIO__STATE_LOW);
#endif
#endif
}

/*******************************************************************************
 * �������-���������� ���������� I2C3 Error.
 ******************************************************************************/
void I2C3_ER_IRQHandler(void) {
#if defined (I2C__DESCRIPTOR_COUNT) && I2C__DESCRIPTOR_COUNT != 0 && !defined (I2C3_USE_BLOCKED)
	hw_i2c__I2C_ER_IRQHandler(I2C_3);
#endif
}


/*******************************************************************************
 * �������-���������� ���������� SPI1.
 ******************************************************************************/
void  SPI1_IRQHandler(void) {
#if defined (_SPI__DESCRIPTOR_COUNT_) && _SPI__DESCRIPTOR_COUNT_ != 0 && !defined (SPI1_USE_BLOCKED)
    hw_spi__SPI_IRQHandler(SPI_1);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� SPI2.
 ******************************************************************************/
void SPI2_IRQHandler(void) {
#if defined (_SPI__DESCRIPTOR_COUNT_) && _SPI__DESCRIPTOR_COUNT_ != 0 && !defined (SPI2_USE_BLOCKED)
    hw_spi__SPI_IRQHandler(SPI_2);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� USART1.
 ******************************************************************************/
void USART1_IRQHandler(void) {
#if defined (_UART__DESCRIPTOR_COUNT_) && _UART__DESCRIPTOR_COUNT_ != 0 && !defined (UART_1_USE_BLOCKED)
    hw_uart__USART_IRQHandler(UART_1);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� USART2.
 ******************************************************************************/
void USART2_IRQHandler(void) {
#if defined (_UART__DESCRIPTOR_COUNT_) && _UART__DESCRIPTOR_COUNT_ != 0 && !defined (UART_2_USE_BLOCKED)
    hw_uart__USART_IRQHandler(UART_2);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� USART3.
 ******************************************************************************/
void USART3_IRQHandler(void) {
#if defined (_UART__DESCRIPTOR_COUNT_) && _UART__DESCRIPTOR_COUNT_ != 0 && !defined (UART_3_USE_BLOCKED)
    hw_uart__USART_IRQHandler(UART_3);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� UART4.
 ******************************************************************************/
void UART4_IRQHandler(void) {
#if defined (_UART__DESCRIPTOR_COUNT_) && _UART__DESCRIPTOR_COUNT_ != 0 && !defined (UART_4_USE_BLOCKED)
    hw_uart__USART_IRQHandler(UART_4);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� UART5.
 ******************************************************************************/
void UART5_IRQHandler(void) {
#if defined (_UART__DESCRIPTOR_COUNT_) && _UART__DESCRIPTOR_COUNT_ != 0 && !defined (UART_5_USE_BLOCKED)
    hw_uart__USART_IRQHandler(UART_5);
#endif
}

/*******************************************************************************
 * �������-���������� ���������� RTC Alarm through EXTI Line.
 ******************************************************************************/
void RTC_Alarm_IRQHandler(void) {
    SET_BIT(EXTI->PR1, 1<<18);
    CLEAR_BIT(RTC->ISR, RTC_ISR_ALRAF);
    hw_timer__RTC_IRQHandler();
}

/*******************************************************************************
 * �������-���������� ���������� TIM6.
 ******************************************************************************/
#ifndef INTERRUPTS__TIM6_IRQ_NOT_USE
void TIM6_IRQHandler(void) {
	while(1);
}
#endif

/*******************************************************************************
 * �������-���������� ���������� TIM7.
 ******************************************************************************/
void TIM7_IRQHandler(void) {
	while(1);
}

void HardFault_Handler(void) {
    while(1);
}

void LPTIM1_IRQHandler(void) {
    while(1);
}

void LPTIM2_IRQHandler(void) {
    while(1);
}

void OTG_FS_IRQHandler(void) {
#ifdef USB_UART_1
    hw_uart_usb__IRQ();
#endif
}
