/***************************************************************************//**
 * @file hw_uart.h
 * @brief �������������� ������� ����������� UART
 *        ��� ���������� ������������ ������� ��������, � ������ �������������,
 *        �������������� ���������� UART ����� ��������� �� ����� ����������
 *        � ������� �������� #define UART_Ax_USE_BLOCKED.
 * @author p.luchnikov
 ******************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include "standard.h"
#include "target.h"
#include "mcu.h"
#include "hw_uart.h"
#include "uart.h"
#include "supervisor.h"
#include "sw_timer.h"
#ifdef BB_UART__1
    #include "bb_uart.h"
#endif
//#include "device.h"

 typedef struct {
    uint32_t	PE	:1;
    uint32_t	FE	:1;
    uint32_t	NE	:1;
    uint32_t	ORE	:1;
    uint32_t	IDLE	:1;
    uint32_t	RXNE	:1;
    uint32_t	TC	:1;
    uint32_t	TXE	:1;
    uint32_t	LBDF	:1;
    uint32_t	CTSIF	:1;
    uint32_t	CTS	:1;
    uint32_t	RTOF	:1;
    uint32_t	EOBF	:1;
    uint32_t    	   	:1;
    uint32_t	ABRE	:1;
    uint32_t	ABRF	:1;
    uint32_t	BUSY	:1;
    uint32_t	CMF	:1;
    uint32_t	SBKF	:1;
    uint32_t	RWU	:1;
    uint32_t	WUF	:1;
    uint32_t	TEACK	:1;
    uint32_t	REACK	:1;
 } hw_uart__isr_t;

 typedef union {
     uint32_t isr_u32;
     hw_uart__isr_t isr;
 } hw_uart__isr_u;

// ��������� �������� ������� ������ GPIO.
const USART_TypeDef * const usart__mass[] = {
    USART1,
    USART2,
    USART3,
    UART4,
    UART5,
    LPUART1,
};

// ��������� ���������� UART.
typedef struct {
    uint32_t settings;                                  // ������� ���������.
    uart__err_handler_t err_handler;                    // ��������� �� �������-���������� ������/������� UART.
    uint8_t *tx_buff;                                   // ��������� �� ����� � ����������������� ������� ��� ��������.
    hw_uart__cb_t tx_isr_cb;                            // ��������� �� ���������������� ������� �� �������� (�� ����������).
    int tx_len;                                         // ������ ������ � ����������������� ������� ��� ��������.
    int tx_bytes_count;                                 // ������� ���������� ������.

    uint8_t *rx_buff;                                   // ��������� ���������������� ����� ��� ������ ������.
    hw_uart__rx_cb_t rx_isr_cb;                            // ��������� �� ���������������� ������� �� ������ ��������� ���������� ���� (�� ����������).
    int rx_len;                                         // ������ ����������������� ������ ��� ������ ������ (�� ���������� ��������� �������).
    int rx_bytes_count;                                 // ���������� �������� ����.
    int block_tx;
    int32_t tx_delay_ticks;                             // ����-��� � ����� �� �������� ���������� �����.
    struct ctrl_t {                                     // ������� ����������� ���������.
        volatile uint8_t tx_start: 1;                   // ����, ������������� � ���, ��� ������ ���� ��������� �������� ������.
        volatile uint8_t rx_start: 1;                   // ����, ������������� � ���, ��� ������ ���� ��������� ������ ������.
    }ctrl;
    sw_timer__sys_time_t send_time;                     // ��������� ����� ������ �������� ���������� �����. ������� ��� ��������� ��������, ����� ����� �� ������������� (�� ������������ ���� TXIFG).
                                                        // �������� ��� ��� �������� ������ � ����� �� ��������.
    uint8_t parity;
    uint8_t wordlenth9;
} hw_uart__itf_t;


#define HW_UART_ID (uart_id - HW_UART_1)
// ������ ��� ��������� ��������� �� ��������� UART �� ID.
#define HW_UART__PTR_FROM_ID_GET(uart_id)   &hw_uart.itf[HW_UART_ID]
//������� ��� ��������� ID ��������� �� ��������� �� ���������
//#define HW_UART__ID_FROM_PTR_GET(uart_ptr)  (&hw_uart.itf[uart_ptr] - &hw_uart.itf[0])

#if (HW_UART__COUNT > _UART__DESCRIPTOR_COUNT_)
    #error "_UART__DESCRIPTOR_COUNT_ must be more than HW_UART__COUNT"
#endif

#if HW_UART__COUNT != 0
// ������� �������� �������� ��� ������������ UART �������� ������ ������ + �������� ����-���� �� �������� ���������� �����.
static const  uint32_t hw_uart__16mhz_br_table[11] = {
    0x3412,    //2400
    0x1A05,    //4800
    0x0D02,    //9600
    0x0681,    //19200
    0x0340,    //38400
    0x0225,    //57600
    0x0112,    //115200
    0x0085,    //230400
    0x0042,    //460800
    0x0021,    //921600
};

 // ��������� �������� UART.
    typedef struct {
        hw_uart__itf_t itf[HW_UART__COUNT];
    } hw_uart__t;

    static hw_uart__t hw_uart;

    void  hw_uart__USART_IRQHandler(int uart_id);

    extern void hw_uart__tx1_vector_handler(void);
    #ifdef UART_A0
        extern void hw_uart__tx0_vector_handler(void);
        extern void hw_uart__rx0_vector_handler(void);
    #endif
    #ifdef UART_A1
        extern void hw_uart__tx1_vector_handler(void);
        extern void hw_uart__rx1_vector_handler(void);
    #endif

static void uart__itf_en(int uart_id);
static void uart__itf_dis(int uart_id);

#endif


uart__event_e hw_uart__init(int uart_id, uint32_t settings, uart__err_handler_t err_handler) {

    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__init(uart_id, settings, fc_cmd_handler, err_handler);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT  != 0
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    uart->settings = 0; // ���� ����� ����� ������, �� ������� ��������� ������ ����������� - ����� ����� ������ ��������� �������.

// ������ �������������
    USART_TypeDef  *UART = (USART_TypeDef *) usart__mass[HW_UART_ID];
    uart__itf_en(uart_id);

    MODIFY_REG(RCC->CCIPR, 0xFFF, 0xAAA);            // UARTs clk = 16Mhz
    //�������� UARTx (x=1/2/3/4/5)
    UART->CR1 = USART_CR1_UE | USART_CR1_UESM | USART_CR1_OVER8;  // ������� �������� ���������� �� ����.
    UART->ICR = 0x1B5F | USART_ICR_CMCF | USART_ICR_WUCF;
//��������� ���� ����
    switch (settings & UART__STOP_MASK) {
        case UART__STOP_0_5:
            UART->CR2 &=~ USART_CR2_STOP;
            UART->CR2 |= USART_CR2_STOP_0;
            break;
        case UART__STOP_1:
            UART->CR2 &=~ USART_CR2_STOP;
            break;
        case UART__STOP_1_5:
            UART->CR2 |=  USART_CR2_STOP;
            break;
        case UART__STOP_2:
            UART->CR2 &=~ USART_CR2_STOP;
            UART->CR2 |= USART_CR2_STOP_1;
            break;
        default:
            return UART__EVENT_PARAM_NA;
    }
 //��������� ������ ������������ ������
    switch (settings & UART__DATA_MASK) {
        case UART__DATA_7: UART->CR1 &=~ USART_CR1_M; uart->wordlenth9 = 0; break;
        case UART__DATA_8: UART->CR1 &=~ USART_CR1_M;
        if ((settings & UART__PAR_MASK) != UART__PAR_NONE) {
        uart->wordlenth9 = 1;
        UART->CR1 |= USART_CR1_M;
        }
        else
        uart->wordlenth9 = 0;
        break;
        case UART__DATA_9: UART->CR1 |= USART_CR1_M; uart->wordlenth9 = 1; break;
        default: return UART__EVENT_PARAM_NA;
    }
// ��������� ��������
    switch (settings & UART__PAR_MASK) {
        case UART__PAR_ODD:
            uart->parity = 1;
            UART->CR1 |= USART_CR1_PCE;
            UART->CR1 |= USART_CR1_PS;
            break;
        case UART__PAR_EVEN:
            uart->parity = 1;
            UART->CR1 |= USART_CR1_PCE;
            UART->CR1 &=~ USART_CR1_PS;
            break;
        default:
            uart->parity = 0;
            UART->CR1 &=~ USART_CR1_PCE;
            break;
    }

  //�������� �� ���������� �������!
    int M = (UART->CR1 & USART_CR1_M);     // data length
    int PCE = (UART->CR1 & USART_CR1_PCE); //ParityControl
    int UserDataLenth = (settings & UART__DATA_MASK);

    if (M == 0) {
        if (PCE == 0) {
            if(UserDataLenth != UART__DATA_8)
                return UART__EVENT_PARAM_NA;
        }
        else if (PCE !=0 ) {
            if((UserDataLenth != UART__DATA_7) & (!uart->wordlenth9))
                return UART__EVENT_PARAM_NA;
        }
    }
    else if (M != 0) {
         if (PCE == 0) {
            if(UserDataLenth != UART__DATA_9)
                return UART__EVENT_PARAM_NA;
        }
        else if (PCE !=0 ) {
           if(UserDataLenth != UART__DATA_8)
                return UART__EVENT_PARAM_NA;
        }
    }
    else
        return UART__EVENT_PARAM_NA;
 //��������� �������
    UART->BRR = hw_uart__16mhz_br_table[(settings & UART__BR_MASK) - 4];
    uart->settings = settings;
    uart->err_handler = err_handler;

    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}


/*******************************************************************************
 * ������� ��������������� ���������� UART-�. ���������� ���������� UART-� ���� �������� � ��������� ���.
 ******************************************************************************/
uart__event_e hw_uart__deinit(int uart_id) {
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__deinit(uart_id);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    USART_TypeDef  *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    hw_uart__tx_break(uart_id);
    hw_uart__rx_break(uart_id);

    //��������� ��������� USART
    UART->CR1 &=~ (USART_CR1_UE | USART_CR1_RE | USART_CR1_TE);
    uart__itf_dis(uart_id);
    uart->settings = 0;
    uart->err_handler = NULL;
    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� �������� ���������������� ������ � ��������� UART.
 ******************************************************************************/
uart__event_e hw_uart__tx(int uart_id, uint8_t *buff, int len, hw_uart__cb_t tx_isr_cb) {
    if ((buff == NULL) || (len == 0))
        return UART__EVENT_PARAM_NA;
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__tx(uart_id, buff, len, tx_isr_cb);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);

    USART_TypeDef  *UART = (USART_TypeDef *) usart__mass[HW_UART_ID];
    CRITICAL_SECTION_ON
    uart->ctrl.tx_start = 1;
    uart->tx_buff = buff;
    uart->tx_len = len;
    uart->tx_isr_cb = tx_isr_cb;
    uart->tx_bytes_count = 0;

    // ��������
    UART->ICR |= USART_ICR_TCCF;
    int temp = uart->tx_buff[uart->tx_bytes_count];

    if (uart->wordlenth9 == 1) {
        temp = (uart->tx_buff[uart->tx_bytes_count] & 0x01FF);
    }
    else {
        temp = uart->tx_buff[uart->tx_bytes_count];
    }
    UART->TDR = temp;
    UART->CR1 |= (USART_CR1_TE | USART_CR1_TXEIE | USART_CR1_TCIE); //��������� ���������� �� TXE � TC
    CRITICAL_SECTION_OFF

    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� ������ ������ �� ���������� �����.
 ******************************************************************************/
uart__event_e hw_uart__rx(int uart_id, hw_uart__rx_cb_t rx_isr_cb) {
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__rx(uart_id, buff, len, rx_isr_cb);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    USART_TypeDef *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    CRITICAL_SECTION_ON
    uart->ctrl.rx_start = 1;
    uart->rx_bytes_count = 0;
    uart->rx_isr_cb = rx_isr_cb;
    //���������� ���������� � ���������� �������� ������ (RXNE)
//    UART->ICR |= USART_ISR_RXNE;

    UART->CR1 |= USART_CR1_RXNEIE;
    //��������� �������� �� ������
    UART->CR1 |= USART_CR1_PEIE;  //�������� ���������� �� Parity erorr
    UART->CR3 |= USART_CR3_EIE;   //�������� ���������� �� FRAGMING/OVERFLOW/NOISE
    // �������� recieve
    UART->CR1 |= USART_CR1_RE;
    CRITICAL_SECTION_OFF
    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� ����������� ���������� �������� �������� ������ ��������� UART-��.
 ******************************************************************************/
uart__event_e hw_uart__tx_break(int uart_id) {
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__tx_break(uart_id, buff, len);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    USART_TypeDef *UART = (USART_TypeDef *) usart__mass[uart_id-1];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    //��������� �����������
    UART->CR1 &=~ (USART_CR1_TE | USART_CR1_TCIE | USART_CR1_TXEIE);
    uart->ctrl.tx_start = 0;

    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� ����������� ���������� �������� ������ ������ ��������� UART-��.
 ******************************************************************************/
uart__event_e hw_uart__rx_break(int uart_id) {
    if (uart_id > MCU__UART_MAX) {
#ifdef BB_UART__1
        return bb_uart__rx_break(uart_id, buff, len);
#else
        return UART__EVENT_PARAM_NA;
#endif
    }
#if HW_UART__COUNT != 0
    USART_TypeDef *UART = (USART_TypeDef *) usart__mass[uart_id];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    UART->CR1 &=~ (USART_CR1_RE | USART_CR1_RXNEIE | USART_CR1_PEIE);
    UART->CR3 &=~ USART_CR3_EIE;
    uart->ctrl.rx_start = 0;

    return UART__EVENT_OK;
#else
    return UART__EVENT_PARAM_NA;
#endif
}

/*******************************************************************************
 * ������� �������� ������ ��� ��������� �������� ��������� �������� UART.
 ******************************************************************************/
void hw_uart__cout(void) {
}

/*******************************************************************************
 * ���������� �������-���������� ���������� �� ������ � �������.
 ******************************************************************************/
void hw_uart__USART_IRQHandler(int uart_id) {
#if HW_UART__COUNT != 0
    USART_TypeDef *UART = (USART_TypeDef *) usart__mass[HW_UART_ID];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    uart__event_e event = UART__EVENT_FRAMING_ERR;

   hw_uart__isr_u status_reg;
   status_reg.isr_u32 = UART->ISR;

   __no_operation();

   //��������� �� ������� ������
   if (status_reg.isr.PE |status_reg.isr.FE | status_reg.isr.NE | status_reg.isr.ORE) {
       // �������� ��������.
       if (status_reg.isr.PE){
           event= UART__EVENT_PARITY_ERR;
       }
       // �������� �����. ���� ��� ������ ����, ������ ����� ������ ��������� TX/RX
       else if (status_reg.isr.FE) {
           event = UART__EVENT_FRAMING_ERR;
       }
       // �������� ������������.
       else if (status_reg.isr.ORE) {
           event = UART__EVENT_OVERFLOW;
       }
       // �������� �� ����.
       else if (status_reg.isr.NE) {
           event = UART__EVENT_NOISE;
       }

       UART->CR1 &=~ (USART_CR1_RE | USART_CR1_PEIE | USART_CR1_RXNEIE);  //��������� ���������� �� Parity erorr
       UART->CR3 &=~ USART_CR3_EIE;   //��������� ���������� �� FRAGMING/OVERFLOW/NOISE
       UART->ICR = 0x1FFFFF;
       if (uart->ctrl.rx_start) {
           uart->ctrl.rx_start = 0;
           uart->rx_isr_cb(uart_id, uart->rx_buff, UART__EVENT_ERROR);
       }
       if (uart->ctrl.tx_start) {
           uart->ctrl.tx_start = 0;
           uart->tx_isr_cb(uart_id, event, uart->tx_buff, UART__EVENT_ERROR);
       }
       return;
   }

   //RECIEVE
    if ((UART->CR1 & USART_CR1_RXNEIE) && (status_reg.isr.RXNE)) {
        //��� ��� ���� ��������� ��� ������ ��� ����������, ��� ���������� ��������� � �����
        volatile int buffer = UART->RDR;
        uint8_t temp[2], buff_len = 1;
        temp[0] = buffer;
        temp[1] = buffer >> 8;

        if (uart->wordlenth9 == 1) {
            if (uart->parity != 1) {
                buff_len = 2;
                temp[1] &= 0x01;
            }
        }
        else {
            if (uart->parity == 1) {
                temp[0] &= 0x7F;
            }
        }
        //���� ��� �� ��������� -> ���������� ����� �� ���� ������ overrun
        UART->RQR |= USART_RQR_RXFRQ;
        if (uart->ctrl.rx_start) {
            //���� ������ ��� ������ �� ��������
            uart->rx_isr_cb(uart_id, temp, buff_len);
//            if (uart->rx_bytes_count >= uart->rx_len) {
//                uart->ctrl.rx_start = 0;
//                //                        UART->CR1 &=~ (USART_CR1_RE | USART_CR1_PEIE | USART_CR1_RXNEIE);  //�������� ���������� �� Parity erorr
//                UART->CR3 &=~ USART_CR3_EIE;   //�������� ���������� �� FRAGMING/OVERFLOW/NOISE
//                if (!uart->ctrl.rx_start)
//                    UART->CR1 &=~ (USART_CR1_RE | USART_CR1_RXNEIE | USART_CR1_PEIE);
//
//            }
        }
        //        return;
    }
   if ((UART->CR1 & USART_CR1_TE) && ((status_reg.isr.TXE) || (status_reg.isr.TC))) {
        if (READ_BIT(UART->ISR, USART_ISR_TXE) && READ_BIT(UART->CR1, USART_CR1_TXEIE)) { //���� ����� �� ������
            UART->RQR |= USART_RQR_TXFRQ;

            if (uart->ctrl.tx_start) {
                uart->tx_bytes_count++;

                int temp = (uint16_t)uart->tx_buff[uart->tx_bytes_count];
                if (uart->wordlenth9 == 1) {
                    temp = ((uint16_t)uart->tx_buff[uart->tx_bytes_count] & 0x01FF);
                    if (uart->parity == 1)
                       ;;
                }

                if (uart->tx_bytes_count < uart->tx_len -1) {
                    // �������� len -1 ����
                   UART->TDR = temp;
                }
                else {
                    //�������� ���������  ����
                     UART->CR1 &=~ (USART_CR1_TXEIE); //�������� TXE
                     UART->CR1 |= (USART_CR1_TCIE); //��������� ���������� �� TC
                     if (uart->tx_len != 1) {
                     UART->TDR = temp;
                     }
                     return;
                }
            }
            else {
                UART->CR1 &=~ (USART_CR1_TCIE | USART_CR1_TXEIE | USART_CR1_TE);
            }
        }
        //��������� ���������� ��������� �������� � ���� ������
        else if (READ_BIT(UART->ISR, USART_ISR_TC) && READ_BIT(UART->CR1, USART_CR1_TCIE)) {

                if (!uart->ctrl.tx_start)
                    return;
                uart->ctrl.tx_start = 0;
                    uart->tx_bytes_count++;
                    uart->tx_isr_cb(uart_id, UART__EVENT_OK, uart->tx_buff, uart->tx_bytes_count);
                    if (!uart->ctrl.tx_start) {
                        UART->CR1 &=~ (USART_CR1_TCIE | USART_CR1_TE);
                        UART->ICR |= USART_ICR_TCCF;
                    }
        }
    }
#endif
}

typedef struct {
    int gpio[2];
    uint32_t* uart_clock;
    int uart_clock_flag;
    int uart_irq;
}hw_uart_desc__t;

static const hw_uart_desc__t hw_uart_desc[] = {
#ifdef HW_UART_1_TX
    HW_UART_1_TX, HW_UART_1_RX, (uint32_t*)&RCC->APB2ENR, RCC_APB2ENR_USART1EN, USART1_IRQn,
#endif
#ifdef HW_UART_2_TX
    HW_UART_2_TX, HW_UART_2_RX, (uint32_t*)&RCC->APB1ENR1, RCC_APB1ENR1_USART2EN, USART2_IRQn,
#endif
#ifdef HW_UART_3_TX
    HW_UART_3_TX, HW_UART_3_RX, (uint32_t*)&RCC->APB1ENR1, RCC_APB1ENR1_USART3EN, USART3_IRQn,
#endif
#ifdef HW_UART_4_TX
    HW_UART_4_TX, HW_UART_4_RX, (uint32_t*)&RCC->APB1ENR1, RCC_APB1ENR1_UART4EN, UART4_IRQn,
#endif
#ifdef HW_UART_5_TX
    HW_UART_5_TX, HW_UART_5_RX, (uint32_t*)&RCC->APB1ENR1, RCC_APB1ENR1_UART5EN, UART5_IRQn,
#endif
#ifdef HW_UART_6_TX
    HW_UART_6_TX, HW_UART_6_RX, (uint32_t*)&RCC->APB1ENR2, RCC_APB1ENR2_LPUART1EN, LPUART1_IRQn,
#endif
};

/*******************************************************************************
 * ������� ������������� GPIO ���������� � UART_1.
 ******************************************************************************/
#if HW_UART__COUNT != 0
static void uart__itf_en(int uart_id) {
    // �������� STM32 UART �������������
    // �������� ������������ ��� ��������� ����� � �����
    for(int x = 0; x < 2; x++)
        gpio__af_init(hw_uart_desc[HW_UART_ID].gpio[x], 0, 0x7);
    *hw_uart_desc[HW_UART_ID].uart_clock |= hw_uart_desc[HW_UART_ID].uart_clock_flag;
    NVIC_EnableIRQ((IRQn_Type)hw_uart_desc[HW_UART_ID].uart_irq);
//    USART1->ICR = 0xFFFFFFFF;
}

static void uart__itf_dis(int uart_id) {
    NVIC_DisableIRQ((IRQn_Type)hw_uart_desc[HW_UART_ID].uart_irq);
    *hw_uart_desc[HW_UART_ID].uart_clock &= ~hw_uart_desc[HW_UART_ID].uart_clock_flag;
    for(int x = 0; x < 2; x++)
        gpio__init(hw_uart_desc[HW_UART_ID].gpio[x], GPIO__DIR_IN, NULL);
}


#endif

