/***************************************************************************//**
 * @file wdt.h.
 * @brief ������ ���������� watchdog-�� �����������.
 *        ���������� ��� �� MSP430.
 * @author p.luchnikov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "standard.h"
#include "target.h"
#include "wdt.h"
#include "supervisor.h"

#define WDT__IWD_DELAY_MAX                      0xFFF
#define WDT__IWDG_KEY_RELOAD                    0xAAAA      /*!< IWDG Reload Counter Enable   */
#define WDT__IWDG_KEY_ENABLE                    0x0000CCCC  /*!< IWDG Peripheral Enable       */
#define WDT__IWDG_KEY_WRITE_ACCESS_ENABLE       0x00005555  /*!< IWDG KR Write Access Enable  */
#define WDT__IWDG_KEY_WRITE_ACCESS_DISABLE      0x00000000  /*!< IWDG KR Write Access Disable */

// ��������� ������ ���������� watchdog-��.
typedef struct {
    uint8_t wdt__clk_init;                      // ��������� ������ ������������ WDT.
    wdt__code_e cur_state;                      // ������� ��������� ������.
    uint8_t count_lock;                         // ���������� ����������� �������, ������������� �� ����������� watchdog
    uint16_t timeout;                           // ����� ������� �������� (����� �������� ����������).
} wdt__t;

// ���������� ��� ������� ������� LSI.
int wdt__capture_nb = 0;                        //���������� �������� �������
int wdt__lsi_freq;                              //������� LSI

static wdt__t wdt = {.cur_state = WDT__ERROR,
                     .wdt__clk_init = 0};
static supervisor__couts_t wdt__autoreset;

static wdt__code_e wdt__lsi_init(void);

static void wdt__reset(void);

/*******************************************************************************
 * @brief ������� ������������� watchdog-�.
 *        ������ ����-��� �������������watchdog-�.
 ******************************************************************************/
wdt__code_e wdt__init(int reset_timeout_ms) {
    wdt.cur_state = WDT__ERROR;
    if ((reset_timeout_ms <= WDT__IWD_DELAY_MAX) && (wdt__lsi_init() != WDT__ERROR)) {
        IWDG->KR = WDT__IWDG_KEY_ENABLE;                // ��������� IWDG
        IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // ��������� ������ � PR � RLR ��������
        for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);

        if (reset_timeout_ms < WDT__IWD_DELAY_MAX) {
            IWDG->PR = 3;
            if (reset_timeout_ms > 0)
                wdt.timeout = reset_timeout_ms;
            IWDG->RLR = wdt.timeout;
        }
        else {
            IWDG->PR = 7;
            IWDG->RLR = 0xFFF;
        }
        IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;
        for(int x = 0; x < 100000 && IWDG->SR; x++);
        IWDG->KR = WDT__IWDG_KEY_RELOAD;
        supervisor__cout_set(&wdt__autoreset, wdt__reset);
        wdt.cur_state = WDT__ON;
    }
    return  wdt.cur_state;
}

/*******************************************************************************
 * ������� ���������� watchdog-��.
 * IWDG ������ ���������. �������, ��� ���������� - ��� �������� �� ������������ �����.
 ******************************************************************************/
wdt__code_e wdt__cmd(wdt__code_e cmd) {
    switch (cmd) {
        case WDT__ON:
            if (wdt.count_lock != 0) {
                wdt.count_lock--;
            }
            if (wdt.count_lock == 0 && wdt.cur_state != WDT__ON) {
                wdt.cur_state = WDT__ON;
                wdt__init(0); //������ �������� ��������
                IWDG->KR = WDT__IWDG_KEY_RELOAD;  // ������������� �������
            }
            break;
        case WDT__OFF:
            if (wdt.count_lock < 0xFF)
                wdt.count_lock++;
            if (wdt.cur_state != WDT__OFF) {
                IWDG->KR = WDT__IWDG_KEY_RELOAD;    // ������������� �������
                wdt__init(WDT__IWD_DELAY_MAX);                       // ������ ������������ ��������
                wdt.cur_state = WDT__ERROR;           // �������, ��� ���� ���������.
            }
            break;
        case WDT__RESET:
            IWDG->KR = WDT__IWDG_KEY_RELOAD;  // ������������� �������
            break;
        case WDT__GET_STATE:
            // ������ ������ ������� ���������.
            break;
    }
    return wdt.cur_state;
}

static void wdt__reset(void) {
    IWDG->KR = WDT__IWDG_KEY_RELOAD;  // ������������� �������
}

/*******************************************************************************
 * ������� ��� ������������� LSI � ��������� ��� ������� ������������.
 ******************************************************************************/
static wdt__code_e wdt__lsi_init(void) {
    if (!wdt.wdt__clk_init) {
        wdt.wdt__clk_init = 1;
        // ���������� LSI
        // �������� ������������ PWR
        MODIFY_REG(RCC->APB1ENR1, 0, RCC_APB1ENR1_PWREN);     //���� ��� ������� � ����������� LSI
        if(!READ_BIT(PWR->CR1, PWR_CR1_DBP))
            SET_BIT(PWR->CR1, PWR_CR1_DBP);
        for(int x = 0;(x < 100000) && (!READ_BIT(PWR->CR1, PWR_CR1_DBP)); x++);
        // ���� ���� -> ���������� LSI
        RCC->CSR |= RCC_CSR_LSION;

        //���� ������������ LSI
        for(int x = 0;(x < 100000) && (!(RCC->CSR & RCC_CSR_LSIRDY)); x++);
    }
    return (RCC->CSR & RCC_CSR_LSIRDY) ? WDT__ON : WDT__ERROR;
}
