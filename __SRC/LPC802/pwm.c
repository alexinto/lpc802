#include "pwm.h"

typedef struct {
    uint8_t init        :1;
    uint8_t ch_en;
}pwm_t;

static pwm_t pwm;

static void pwm__init();

static void pwm__init() {
    LPC_SYSCON->SYSAHBCLKCTRL0 |= 1 << 25;
    LPC_CTIMER0->MCR = 1 << 10;
    LPC_CTIMER0->MR[0] = LPC_CTIMER0->MR[1] = LPC_CTIMER0->MR[2] = PWM_MAX;
    LPC_CTIMER0->MR[3] = PWM_MAX - 1;
    LPC_CTIMER0->PWMC = 0x00;
    LPC_CTIMER0->TCR = 0x01;
    pwm.init = 1;
}


void pwm__start(int ch, int pin) {
    uint32_t buff;
    if (!pwm.init)
        pwm__init();
    pwm.ch_en |= (1 << ch);
    ch *= 8;
    CRITICAL_SECTION_ON
    buff = LPC_SWM->PINASSIGN4 & (~(0xFF << ch));
    LPC_SWM->PINASSIGN4 = buff | (pin << ch);
    LPC_CTIMER0->PWMC |= 1 << ch;
    CRITICAL_SECTION_OFF
}

void pwm__stop(int ch) {
    pwm.ch_en &= ~(1 << ch);
    ch *= 8;
    if (!pwm.ch_en)
        pwm__deinit();
    else {
        LPC_SWM->PINASSIGN4 |= (0xFF << ch);
        LPC_CTIMER0->PWMC &= ~(1 << ch);
    }
}

void pwm__deinit() {
    LPC_CTIMER0->TCR = 0x00;
    LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1 << 25);
    LPC_SWM->PINASSIGN4 = 0xFFFFFFFF;
    pwm.ch_en = 0;
    pwm.init = 0;
}