/***************************************************************************//**
 * @file flash.c.
 * @brief ������� ������ � ���������� ����- ������� LPC802.
 * @authors a.tushentsov.
 ******************************************************************************/
#include <stddef.h>
#include <string.h>
#include "target.h"
#include "System/fstandard.h"
#include "LPC802/flash.h"


#define FLASH_ADDR 0x3C00

typedef enum {
    FLASH_READ =    01,
    FLASH_PREPARE = 50, // start_sector, end_sedctor, NULL
    FLASH_ERASE =   59, // START_PAGE, END_PAGE, NULL
    FLASH_WRITE =   51, // FLASH_ADDR, RAM_ADDR, LEN (64,128,256,512), NULL
    FLASH_COMPARE = 56, // addr_1, addr_2, len (������ 4), NULL
}flash_oper__e;

unsigned int command_param[5];
unsigned int status_result[5];
typedef void (*IAP)(unsigned int*,unsigned int*);
static IAP iap_entry = (IAP)0x0F001FF1;

flash_events__e flash_write(u8* buff, int len) {
    flash_events__e res = NO_VALID_IMAGE;
    CRITICAL_SECTION_ON
    *command_param = FLASH_PREPARE;
    command_param[1] = 15;
    command_param[2] = 15;
    command_param[3] = NULL;
    iap_entry (command_param,status_result);
    res = (flash_events__e)*status_result;
    *command_param = FLASH_ERASE;
    command_param[1] = 240;           // 832 ����� ��������
    command_param[2] = 253;
    command_param[3] = NULL;
    iap_entry (command_param,status_result);
    res = (flash_events__e)*status_result;
    *command_param = FLASH_PREPARE;
    command_param[1] = 15;
    command_param[2] = 15;
    command_param[3] = NULL;
    iap_entry (command_param,status_result);
    res = (flash_events__e)*status_result;
     *command_param = FLASH_WRITE;
    command_param[1] = FLASH_ADDR;
    command_param[2] = (int)buff;
    command_param[3] = len;
    command_param[4] = NULL;
    iap_entry (command_param,status_result);
    res = (flash_events__e)*status_result;
    CRITICAL_SECTION_OFF
    return res;
}

flash_events__e flash_read(u8* buff, int len) {
    memcpy(buff, (void const*)FLASH_ADDR, len);
    return CMD_SUCCESS;
}