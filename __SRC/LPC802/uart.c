/***************************************************************************//**
* @file hw_uart.h
* @brief �������������� ������� ����������� UART
*        ��� ���������� ������������ ������� ��������, � ������ �������������,
*        �������������� ���������� UART ����� ��������� �� ����� ����������
*        � ������� �������� #define UART_Ax_USE_BLOCKED.
* @author a.tushentsov.
******************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include "target.h"
#include "System/framework.h"
#include "System/supervisor.h"
#include "System/sw_timer.h"
#include "LPC802/mcu.h"
#include "LPC802/hw_uart.h"
#include "DRV/uart.h"
#include "DRV/gpio.h"

// ��������� �������� ������� ������ GPIO.
const LPC_USART_TypeDef * const usart__mass[] = {
    LPC_USART0,
    LPC_USART1,
};

// ��������� ���������� UART.
typedef struct {
    sw_timer__t timer;
    uint32_t settings;                                  // ������� ���������.
    uint8_t *buff;                                      // ��������� ���������������� ����� ��� ������ ������.
    hw_uart__cb_t isr_cb;                               // ��������� �� ���������������� ������� �� ������ ��������� ���������� ���� (�� ����������).
    int len;                                            // ������ ����������������� ������ ��� ������ ������ (�� ���������� ��������� �������).
    int bytes_count;                                    // ���������� �������� ����.
    supervisor__idle_sub_t idle_sub;                    // ������������� ���������� �� �������� ����� ������ (����� � ��� �� ������ � ������ �������).
    uint8_t busy         :1;
    uint8_t timeout      :1;
    events__e event;
    uint8_t parity;
    uint8_t wordlenth9;
} hw_uart__itf_t;





// ����������� ���������� ������������ UART-�� � �������.
#if (defined (UART_0) && defined (UART_1))
#define HW_UART__COUNT  2
#elif defined (UART_0) || defined (UART_1)
#define HW_UART__COUNT  1
#else
#define HW_UART__COUNT  0
#endif

// ������ ��� ��������� ��������� �� ��������� UART �� ID.
#define HW_UART__PTR_FROM_ID_GET(uart_id)   &hw_uart.itf[uart_id]
//������� ��� ��������� ID ��������� �� ��������� �� ���������
#define HW_UART__ID_FROM_PTR_GET(uart_ptr)  (&hw_uart.itf[uart_ptr] - &hw_uart.itf[0])



#if HW_UART__COUNT != 0

// ��������� �������� UART.
typedef struct {
    hw_uart__itf_t itf[HW_UART__COUNT];
} hw_uart__t;

static hw_uart__t hw_uart;
void  hw_uart__USART_IRQHandler(int uart_id);
extern void hw_uart__tx1_vector_handler(void);

#endif
static void uart__timeout_cb(struct sw_timer__t *timer, void *ext_data);

events__e hw_uart__init(int uart_id, u32 settings, uart__hw_event_handler_t hw_event_handler) {

    if (uart_id > MCU__UART_MAX) {
        return EVENT__PARAM_NA;
    }
#if HW_UART__COUNT != 0
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    uart->settings = 0; // ���� ����� ����� ������, �� ������� ��������� ������ ����������� - ����� ����� ������ ��������� �������.
    // ������ �������������
    LPC_USART_TypeDef  *UART = (LPC_USART_TypeDef *) usart__mass[uart_id];

    //�������� UARTx (x=1/2/3/4/5)
    LPC_SYSCON->SYSAHBCLKCTRL0 |= (UART0 << uart_id) | SWM;
    LPC_SYSCON->STARTERP1 |= (1 << (uart_id + 3)); // Wake up USART_x.
    uint8_t uart_tx, uart_rx, buff;
    uart_tx = (settings & TX__GPIO_MASK) >> 20;
    uart_rx = (settings & RX__GPIO_MASK) >> 25;
    gpio__init(0, uart_rx, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
    gpio__init(0, uart_tx, GPIO__DIR_IN | GPIO__PULL_UP, NULL);
    switch(uart_id) {
    case 0:
        LPC_SYSCON->UART0CLKSEL = 0x00;
        LPC_SWM->PINASSIGN0 = 0xFFFF0000 | (uart_tx) | (uart_rx << 8);
        break;
    case 1:
        LPC_SYSCON->UART1CLKSEL = 0x00;
        LPC_SWM->PINASSIGN1 = 0xFFFF0000 | (uart_tx << 8) | (uart_rx << 16);
        break;
    }
    LPC_SYSCON->PRESETCTRL0 &= (UART0_RST_N << uart_id); // Reset UART
    LPC_SYSCON->PRESETCTRL0 |= ~(UART0_RST_N << uart_id);

    //��������� ������ ������������ ������
    switch (settings & UART__DATA_MASK) {
    case UART__DATA_7: buff = 0; uart->wordlenth9 = 0; break;
    case UART__DATA_8:
        buff = 1 << 2;
        if ((settings & UART__PAR_MASK) != UART__PAR_NONE)
            uart->wordlenth9 = 1;
        else
            uart->wordlenth9 = 0;
        break;
    case UART__DATA_9: buff = 2 << 2; uart->wordlenth9 = 1; break;
    default: return EVENT__PARAM_NA;
    }
    // ��������� ��������
    switch (settings & UART__PAR_MASK) {
    case UART__PAR_ODD:
        UART->CFG = 0x01 | buff | (0x03 << 4);
        uart->parity = 1;
        break;
    case UART__PAR_EVEN:
        UART->CFG = 0x01 | buff | (0x02 << 4);
        uart->parity = 1;
        break;
    default:
        UART->CFG = 0x01 | buff;
        uart->parity = 0;
        break;
    }
    //��������� �������
    UART->BRG = ((supervisor__periph_clk_get() >> 4) / (150 << (settings & UART__BR_MASK))) - 1;

    uart->settings = settings;
    uart->busy = 0;

    UART->INTENCLR = 0xFFFFFFFF;
    NVIC->IP[0] |= 0x80000000;   // UART0 2 level prioritity
    NVIC->IP[1] |= 0x00000080;   // UART1 2 level prioritity
    NVIC_EnableIRQ((IRQn_Type)(UART0_IRQn + uart_id));


    return EVENT__OK;
#else
    return EVENT__PARAM_NA;
#endif
}


/*******************************************************************************
* ������� ��������������� ���������� UART-�. ���������� ���������� UART-� ���� �������� � ��������� ���.
******************************************************************************/
events__e hw_uart__deinit(int uart_id) {
    if (uart_id > MCU__UART_MAX)
        return EVENT__PARAM_NA;
#if HW_UART__COUNT != 0
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    //��������� ��������� USART
    NVIC_DisableIRQ((IRQn_Type)(UART0_IRQn + uart_id));
    LPC_SWM->PINASSIGN[uart_id] = 0xFFFFFFFF;
    LPC_SYSCON->PRESETCTRL0 &= (UART0_RST_N << uart_id); // Reset I2C
    LPC_SYSCON->PRESETCTRL0 |= ~(UART0_RST_N << uart_id);
    LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(UART0 << uart_id);
    uint8_t uart_tx, uart_rx;
    if (uart->settings) {
        uart_tx = (uart->settings & TX__GPIO_MASK) >> 20;
        uart_rx = (uart->settings & RX__GPIO_MASK) >> 25;
        gpio__init(0, uart_rx, GPIO__DIR_IN | GPIO__PULL_DISABLE, NULL);
        gpio__init(0, uart_tx, GPIO__DIR_IN | GPIO__PULL_DISABLE, NULL);
    }
    uart->settings = 0;
    uart->event = EVENT__WAIT;
    uart->busy = 0;
    return EVENT__OK;
#else
    return EVENT__PARAM_NA;
#endif
}

/*******************************************************************************
* ������� �������� ���������������� ������ � ��������� UART.
******************************************************************************/
events__e hw_uart__tx(int uart_id, uint8_t *buff, int len, hw_uart__cb_t tx_isr_cb) {
    if ((buff == NULL) || (len == 0) || (uart_id > MCU__UART_MAX))
        return EVENT__PARAM_NA;
#if HW_UART__COUNT != 0
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    if (uart->busy) {
        return EVENT__BUSY;
    }
    LPC_USART_TypeDef  *UART = (LPC_USART_TypeDef *) usart__mass[uart_id];
    supervisor__idle_lock(&uart->idle_sub);
    uart->busy = 1;
    uart->event = EVENT__WAIT;
    uart->buff = buff;
    uart->len = len;
    uart->isr_cb = tx_isr_cb;
    uart->bytes_count = 0;

    // ��������
    int temp = uart->buff[uart->bytes_count];

    if (uart->wordlenth9 == 1)
        temp &= 0x01FF;

    UART->TXDAT = temp;
    UART->INTENSET = 0x4C;
    return EVENT__OK;
#else
    return EVENT__PARAM_NA;
#endif
}

/*******************************************************************************
* ������� ������ ������ �� ���������� �����.
******************************************************************************/
events__e hw_uart__rx(int uart_id, uint8_t *buff, int len, hw_uart__cb_t rx_isr_cb) {
    LPC_USART_TypeDef  *UART = (LPC_USART_TypeDef *) usart__mass[uart_id];
    if (buff == NULL || len == 0 || rx_isr_cb == NULL || (uart_id > MCU__UART_MAX))
        return EVENT__PARAM_NA;

#if HW_UART__COUNT != 0
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    if (uart->busy) {
        return EVENT__BUSY;
    }
    uart->busy = 1;
    uart->event = EVENT__WAIT;
    uart->buff = buff;
    uart->len = len;
    uart->bytes_count = 0;
    uart->isr_cb = rx_isr_cb;
    //���������� ���������� � ���������� �������� ������ (RXNE)
    UART->INTENSET = 0xE901;// | (1 << 12);
    return EVENT__OK;
#else
    return EVENT__PARAM_NA;
#endif
}


/*******************************************************************************
* ������� �������� ������ ��� ��������� �������� ��������� �������� UART.
******************************************************************************/
void hw_uart__cout(void) {
    for(int x = 0; x < HW_UART__COUNT; x++) {
        hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(x);
        if (uart->event != EVENT__WAIT) {
            events__e result = uart->event;
            sw_timer__stop(&uart->timer);
            uart->timeout = 1;
            supervisor__idle_unlock(&uart->idle_sub);
            uart->event = EVENT__WAIT;
            uart->busy = 0;                     //to_do lock from ISR
            if (uart->isr_cb)
                uart->isr_cb(x, result, uart->buff, uart->bytes_count);
        }
        else if (((uart->bytes_count) && (!uart->timeout)) && ((uart->settings & UART__FRAME_MASK) == UART__FRAME_3_5)) {
            uart->timeout = 1;
            sw_timer__start(&uart->timer, 3, uart__timeout_cb, uart);
        }
    }
}

static void uart__timeout_cb(struct sw_timer__t *timer, void *ext_data) {
    hw_uart__itf_t* uart = (hw_uart__itf_t*)ext_data;
    CRITICAL_SECTION_ON
        if (uart->timeout)
            uart->event = EVENT__TIMEOUT;
    CRITICAL_SECTION_OFF
}

/*******************************************************************************
* ���������� �������-���������� ���������� �� ������ � �������.
******************************************************************************/
void hw_uart__USART_IRQHandler(int uart_id) {
    LPC_USART_TypeDef  *UART = (LPC_USART_TypeDef *) usart__mass[uart_id];
    hw_uart__itf_t *uart = HW_UART__PTR_FROM_ID_GET(uart_id);
    uint32_t status;
    int temp;
    status = UART->INTSTAT;
    uart->timeout = 0;
    //RECIEVE
    if ((status & 0x01) && (uart->busy)) {
        //��� ��� ���� ��������� ��� ������ ��� ����������, ��� ���������� ��������� � �����
        volatile int buffer = UART->RXDAT;
        if (uart->wordlenth9 == 1) {
            if (uart->parity == 1) {
                buffer = (buffer & 0x00FF);
            }
            else {
                buffer = (buffer & 0x01FF);
            }
        }
        else {
            if (uart->parity == 1) {
                buffer = (buffer & 0x007F);
            }
            else {
                buffer = (buffer & 0x00FF);
            }
        }
        uart->buff[uart->bytes_count] = buffer;
        uart->bytes_count++;
        if (uart->bytes_count >= uart->len) {
            uart->event = EVENT__OK;
        }
    }
    else if ((status & 0x0C) && (uart->busy)) {
        uart->bytes_count++;
        if (uart->bytes_count < uart->len) {
            temp = (uint16_t)uart->buff[uart->bytes_count];
            if (uart->wordlenth9 == 1) {
                temp = ((uint16_t)uart->buff[uart->bytes_count] & 0x01FF);
                if (uart->parity == 1)
                    ;;
            }
            // �������� len ����
            UART->TXDAT = temp;
        }
        else {
            uart->event = EVENT__OK;
        }
    }
    else
        uart->event = EVENT__ERROR;

    if (uart->event != EVENT__WAIT) {
        UART->INTENCLR = 0xFFFFFFFF;
        supervisor__idle_lock(&uart->idle_sub);
    }
}


