/***************************************************************************//**
 * @file supervisor.c.
 * @brief ������ ���������� �������� � �������� ���������������� �����������.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "LPC802/mcu.h"
#include "System/framework.h"
#include "System/hw_supervisor.h"
#include "System/supervisor.h"

#define SYS_CLOCK_MID       6000000     // ��������� ������� ��� ������������ �� MSI (� Hz)
#define SYS_CLOCK_LOW       1000000      // ��������� ������� ��� ��� (� Hz)
#define SYS_CLOCK_HIGH      12000000    // ��������� ������� ��� ������������ �� HSI (� Hz)

// ��������� ��������� ������.
typedef struct {
    uint32_t cpu_clk;                                           // ������� ������� CPU.
    uint32_t periph_clk;                                        // ������� ������� ���������.
} supervisor__struct_t;

static supervisor__struct_t supervisor;


/*******************************************************************************
 * ������� ��������� ���������� ������� ����������� (PDV, BOR � ��.).
 ******************************************************************************/
void hw_supervisor__init(void) {
    LPC_SYSCON->PDAWAKECFG |= 0x08;
    LPC_SYSCON->SYSAHBCLKCTRL[0] |= SWM;

}

/*******************************************************************************
 * ������� ������������ ����������.
 ******************************************************************************/
void hw_supervisor__apl_reset(void) {
//    SCB->AIRCR = 0x05FA0000 | SCB_AIRCR_SYSRESETREQ_Msk;
}

/*******************************************************************************
 * ������� �������� ����������� � SLEEP.
 * ���� �������� �������� ��� ������� �� ����� ������ �� ����������� ����������� �� ����������(���������� � hard fault).
 * ���������� � ��������� irq + ���������� NOP-�� ��������.
 ******************************************************************************/
void hw_supervisor__idle_on(void) {
    __disable_interrupt();
    LPC_PMU->PCON = 0x0A;
    SCB->SCR = 0x04;  // todo Check consumption!!!
    __WFI();
    __enable_interrupt();
}

/*******************************************************************************
  ������� ������ ����������� �� SLEEP.
 ******************************************************************************/
void hw_supervisor__idle_off(void) {
//    hw_supervisor__clk_set(SUPERVISOR__CLK_HIGH, 0);
}

/*******************************************************************************
 * ������� ��������� ������� ������� ������������ ����������.
 ******************************************************************************/
uint32_t hw_supervisor__cpu_clk_get(void) {
    return supervisor.cpu_clk;
}

/*******************************************************************************
 * ������� ��������� ������� ������� ������������ ���������.
 ******************************************************************************/
uint32_t hw_supervisor__periph_clk_get(void) {
    return supervisor.periph_clk;
}

/*******************************************************************************
 * ������� ��������� ������� ������������ ��.
 ******************************************************************************/
supervisor__event_e hw_supervisor__clk_set(supervisor__clk_e clk, int sleep) {
    switch (clk) {
        case SUPERVISOR__CLK_HIGH: {
            LPC_SYSCON->MAINCLKSEL = 0x00;
            LPC_SYSCON->MAINCLKUEN = 0x00;
            LPC_SYSCON->MAINCLKUEN = 0x01;
            supervisor.cpu_clk = SYS_CLOCK_HIGH;
            supervisor.periph_clk = SYS_CLOCK_HIGH;
            break;
        }
        case SUPERVISOR__CLK_MID: {
            LPC_SYSCON->MAINCLKSEL = 0x03;
            LPC_SYSCON->MAINCLKUEN = 0x00;
            LPC_SYSCON->MAINCLKUEN = 0x01;
            supervisor.cpu_clk = SYS_CLOCK_MID;
            supervisor.periph_clk = SYS_CLOCK_MID;
            break;
        }
        case SUPERVISOR__CLK_LOW: {
            LPC_SYSCON->MAINCLKSEL = 0x02;
            LPC_SYSCON->MAINCLKUEN = 0x00;
            LPC_SYSCON->MAINCLKUEN = 0x01;
            supervisor.cpu_clk = SYS_CLOCK_LOW;
            supervisor.periph_clk = SYS_CLOCK_LOW;
            break;
        }
        default:
            return SUPERVISOR__EVENT_ERROR;
    }
    return SUPERVISOR__EVENT_OK;
}

