/***************************************************************************//**
 * @file interrupts.c.
 * @brief �������� ������������ ���������� �� (���������� ��� LPC802).
 * @authors a.tushentsov
 ******************************************************************************/
#include <stdint.h>
#include "target.h"
#include "System/framework.h"
#include "LPC802/lpc8xx.h"


extern void I2C__IRQHandler(int i2c_id);
extern void gpio__pin_IRQHandler(int pinint);
extern void hw_timer__time_IRQHandler();
extern void hw_timer__alarm_IRQHandler();
extern void ADC__IRQHandler(int event);
extern void hw_uart__USART_IRQHandler(int uart_id);
extern void ctimer0_IRQHandler(u8 ch_mask);

static void PIN_IRQHandler(int pinint);

void SVC_Handler() {
    while(1);
}
void UART0_IRQHandler() {
    hw_uart__USART_IRQHandler(UART_0);

}
void UART1_IRQHandler() {
    hw_uart__USART_IRQHandler(UART_1);
}

void FLASH_IRQHandler() {
    while(1);
}

void SPI0_IRQHandler() {
    while(1);
}

void I2C0_IRQHandler() {
    I2C__IRQHandler(0);
}

void ADC_SEQA_IRQHandler() {
    ADC__IRQHandler(0);
}

void ADC_SEQB_IRQHandler() {
    while(1);
}

void ADC_THCMP_IRQHandler() {
    while(1);
}

void ADC_OVR_IRQHandler() {
    ADC__IRQHandler(-1);
}

void CTIMER0_IRQHandler() {
    u8 ch_mask = LPC_CTIMER0->IR;
    LPC_CTIMER0->IR = ch_mask;
    ctimer0_IRQHandler(ch_mask);
}


void MRT_IRQHandler() {
    while(1);
}

void WDT_IRQHandler() {
#ifdef WDT_ON
    LPC_WWDT->MOD = 0x0B;
    if (LPC_WWDT->WINDOW & 0x01)
        return;
    LPC_WWDT->WINDOW = 0xFFFFFF;
#else
     LPC_WWDT->MOD = 0x09;
#endif
    LPC_WWDT->FEED = 0xAA;
    LPC_WWDT->FEED = 0x55;
    hw_timer__time_IRQHandler();
}

void WKT_IRQHandler() {
    LPC_WKT->CTRL = 0x03;
    hw_timer__alarm_IRQHandler();
}

///*******************************************************************************
// * �������-���������� ���������� PININT0- PININT7.
// * Does not guarantee the sequence ISR!!!
// * If the last interrupt is rise, it will be processed before fall.
// ******************************************************************************/
void PIN_IRQHandler(int pinint) {
    LPC_PIN_INT->IST |= (1 << pinint);
    gpio__pin_IRQHandler(pinint);
}

void PININT0_IRQHandler() {
   PIN_IRQHandler(0);
}
void PININT1_IRQHandler() {
   PIN_IRQHandler(1);
}
void PININT2_IRQHandler() {
   PIN_IRQHandler(2);
}
void PININT3_IRQHandler() {
   PIN_IRQHandler(3);
}
void PININT4_IRQHandler() {
   PIN_IRQHandler(4);
}
void PININT5_IRQHandler() {
   PIN_IRQHandler(5);
}
void PININT6_IRQHandler() {
   PIN_IRQHandler(6);
}
void PININT7_IRQHandler() {
   PIN_IRQHandler(7);
}
void HardFault_Handler(void) {
    while(1);
}

__weak void I2C__IRQHandler(int i2c_id){    while(1);};
__weak void gpio__pin_IRQHandler(int pinint){    while(1);};
__weak void hw_timer__time_IRQHandler(){    while(1);};
__weak void hw_timer__alarm_IRQHandler(){    while(1);};
__weak void ADC__IRQHandler(int event){    while(1);};
__weak void hw_uart__USART_IRQHandler(int uart_id){    while(1);};
__weak void ctimer0_IRQHandler(u8 ch_mask){    while(1);};
