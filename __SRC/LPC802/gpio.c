/***************************************************************************//**
 * @file gpio.c.
 * @brief ������ ���������� ������� �����-������ ����������� stm32l15xRBx.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include <intrinsics.h>
#include "target.h"
#include "System/framework.h"
#include "DRV/gpio.h"
#include "LPC802/mcu.h"
#include "LPC802/lpc8xx.h"

typedef struct {
    uint8_t init    :1;
    uint8_t isr[18];
    gpio__isr_cb_t cb[8];
}gpio__state_t;

static gpio__state_t gpio_stat = {.init = 0};


// default = set pin to LOW level.
events__e gpio__set(u8 id, u8 gpio, gpio__state_e pin_state) {
    switch(pin_state) {
        case GPIO__STATE_HIGH:
            LPC_GPIO_PORT->SET[0] = 1 << gpio;
            break;
        case GPIO__STATE_TOGGLE:
            LPC_GPIO_PORT->NOT[0] = 1 << gpio;
            break;
        case GPIO__STATE_LOW:
        default:
            LPC_GPIO_PORT->CLR[0] = 1 << gpio;
            break;
    }
    return EVENT__OK;
}

// Call this function in CRITICAL section or
// INTERRUPT handler for valid pin state.
gpio__state_e gpio__get(u8 id,  u8 pin ) {
   return (LPC_GPIO_PORT->PIN[0] & (1 << pin)) ? GPIO__STATE_HIGH : GPIO__STATE_LOW;
}

  const uint8_t gpio__iocon_descr[] = {0x44, 0x2c, 0x18, 0x14, 0x10, 0x0c, 0x00, 0x3C, 0x38, 0x34, 0x20, 0x1C, 0x08, 0x04,
                                       0x48, 0x28, 0x24, 0x00};

events__e gpio__init(u8 id, u8 gpio, i32 pin_settings, gpio__isr_cb_t cb) {
    int intr_set;
    uint32_t* gpio_iocon, temp;
    if (!gpio_stat.init) {
        LPC_SYSCON->SYSAHBCLKCTRL[0] |= GPIO0;
        LPC_SYSCON->SYSAHBCLKCTRL[0] |= IOCON;
        LPC_SYSCON->SYSAHBCLKCTRL[0] |= GPIO_INT;
        LPC_SYSCON->STARTERP0 = 0xFF;  //wake up from SLEEP
        LPC_PIN_INT->ISEL = 0x0;      // For edge interrupt.
        int x;
        for(x = 0; x < 17; x++)
          if(x!=5)
            *(uint32_t*)((uint8_t*)LPC_IOCON + gpio__iocon_descr[x]) &= ~(0x03 << 3);
        gpio_stat.init = 1;
    }
    if (gpio_stat.isr[gpio]) {
        intr_set = gpio_stat.isr[gpio] - 1;
        NVIC->ICER[0] = (1 << (PININT0_IRQn + intr_set));
        LPC_PIN_INT->CIENF = (1 << intr_set);
        LPC_PIN_INT->CIENR = (1 << intr_set);
        LPC_PIN_INT->IST |= (1 << intr_set);
        gpio_stat.isr[gpio] = 0;
    }
    CRITICAL_SECTION_ON
    gpio_iocon = (uint32_t*)((uint8_t*)LPC_IOCON + gpio__iocon_descr[gpio]);
    temp = *gpio_iocon;
    temp &= ~(0x03 << 3);
    if ((pin_settings & GPIO__DIR_MASK) == GPIO__DIR_OUT) {
        LPC_GPIO_PORT->DIRSET[0] = (1 << gpio);
    }
    else {
        switch(pin_settings & GPIO__PULL_MASK) {
            case GPIO__PULL_UP:
                temp |= (0x02 << 3);
                break;
            case GPIO__PULL_DOWN:
                temp |= (0x01 << 3);
                break;
            case GPIO__PULL_DISABLE:
            default:
                break;
        }
        if ((pin_settings & GPIO__DIR_MASK) ==  GPIO__DIR_IN) {
            LPC_GPIO_PORT->DIRCLR[0] = (1 << gpio);
            if ((pin_settings & GPIO__INT_MASK) || (cb != NULL)) {
                intr_set = pin_settings & GPIO__INTR_MASK;
                gpio_stat.cb[intr_set] = cb;
                LPC_SYSCON->PINTSEL[intr_set] = gpio;
                gpio_stat.isr[gpio] = intr_set + 1;
                if (pin_settings & GPIO__INT_EDGE_RISING)
                    LPC_PIN_INT->SIENR = (1 << intr_set);
                if (pin_settings & GPIO__INT_EDGE_FALING)
                    LPC_PIN_INT->SIENF = (1 << intr_set);
                LPC_PIN_INT->IST |= (1 << intr_set);
                NVIC->ICPR[0] = (1 << (PININT0_IRQn + intr_set));
                NVIC->ISER[0] = (1 << (PININT0_IRQn + intr_set));
            }
        }
        //else
             // AF function
    }
    if (temp != *gpio_iocon)
        *gpio_iocon = temp;
    CRITICAL_SECTION_OFF
    return EVENT__OK;
}

void gpio__pin_IRQHandler(int pinint) {
    int pin = LPC_SYSCON->PINTSEL[pinint];
    gpio_stat.cb[pinint](0, pin, (LPC_GPIO_PORT->PIN[0] & (1 << pin)) ? GPIO__STATE_HIGH : GPIO__STATE_LOW);
    return;
}






