/***************************************************************************//**
 * @file hw_i2c.�
 * @brief ������� I2C (��������� ��������� ����� ��� LPC802).
 * @authors a.tushentsov
 ******************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include "target.h"
#include "System/framework.h"
#include "System/supervisor.h"
#include "LPC802/lpc8xx.h"
#include "LPC802/mcu.h"
#include "DRV/i2c.h"



#define I2C_MODE_READ	                1
#define I2C_MODE_WRITE	                0xfe
#define I2C_ADDRESS_7BITS(addr, mode)	((mode == I2C_MODE_READ) ? ((uint8_t)(addr | mode)) : ((uint8_t)(addr & mode)))

#define ISR_CHANGE 0xfffff7ff
// ���� ��������.
typedef enum {
    HW_I2C__NO_INIT = 0,
    HW_I2C__OPER_NONE,
    HW_I2C__OPER_WAIT,
    HW_I2C__OPER_TX,
    HW_I2C__OPER_RX,
} hw_i2c__oper_e;

// ��������� ���������� I2C.
typedef struct {
    uint8_t i2c_cur_port;                               // i2c_port for rx\tx.
    uint16_t settings;                                  // ������� ���������.
    int addr;                                           // ����� ���������.
    uint8_t *buff;                                      // ��������� �� ����� � ����������������� �������.
    int len;                                            // ������ ������ � ����������������� �������.
    i2c__cb_t isr_cb;                                   // ��������� �� ���������������� ������� (�� ����������).
    int bytes_count;                                    // ������� ������.
    int i2c_delay;                                      // �������� ��� ����� ������������������ (����������� ������� 100���).
    events__e event;
    supervisor__idle_sub_t sub;
    hw_i2c__oper_e cur_oper;                            // ������� ����������� ��������.
} hw_i2c__itf_t;



static hw_i2c__itf_t i2c = {.cur_oper = HW_I2C__NO_INIT};
static i2c_init_t i2c_ports[5] = {[0].i2c_id = -1, [1].i2c_id = -1};
static events__e i2c__init_continue(int i2c_port);
static void i2c__deinit_short(void);
void I2C__IRQHandler(int i2c_id);

/*******************************************************************************
 * ������� ������������� ����������� I2C �� ������ � ���������� �����������, � ��� �� �������� �������-����������� �������.
 *        Ex: while(hw_i2c__init(I2C_1, SDA__GPIO_PIN_12 | SCL__GPIO_PIN_16, 0, NULL) != 0){};
 *        �������� ������� ������������� �� ������� ������������ ���� � ������ ������������� I2C.
 ******************************************************************************/
events__e i2c__init(int i2c_id, uint16_t settings, int own_addr, i2c__cb_t event_handler) {
    events__e event = EVENT__NO_MEM;
    CRITICAL_SECTION_ON
    if ((settings == 0) || (i2c_id == -1))
        event = EVENT__PARAM_NA;
    else for(int x = 0; ((x <= 4) && (event == EVENT__NO_MEM)); x++) {
        if (i2c_ports[x].i2c_id == -1) {
            i2c_ports[x].i2c_id = i2c_id;
            i2c_ports[x].settings = settings;
            i2c_ports[x].pins = ((settings & 0x0F) + 4) | (((settings & 0xF0) + 0x40) << 4);
            i2c_ports[x].own_addr = own_addr;
            LPC_SYSCON->SYSAHBCLKCTRL0 |= I2C0 | SWM;
            LPC_SYSCON->I2C0CLKSEL = 0x00;
            LPC_SYSCON->STARTERP1 |= (1 << 8); // Wake up I2C.
            // �������������� ����� I2C � ��������� ���������� ����������
            event = i2c__init_continue(x);
        }
    }
    CRITICAL_SECTION_OFF

    return event;
}

/*******************************************************************************
 * ������� todo
 ******************************************************************************/
static events__e i2c__init_continue(int i2c_port) {
    CRITICAL_SECTION_ON
    i2c__deinit_short();
    LPC_SWM->PINASSIGN5 &= 0xFFFF0000;
    LPC_SWM->PINASSIGN5 |= i2c_ports[i2c_port].pins;
    i2c.cur_oper = HW_I2C__OPER_NONE;
    LPC_I2C0->CLKDIV = 0x07;
    LPC_I2C0->CFG |= 0x09;
    NVIC->IP[2] |= (0x02 << 6);
    NVIC_EnableIRQ(I2C0_IRQn);
    i2c.i2c_delay = 0xff;
    CRITICAL_SECTION_OFF

    return EVENT__OK;
}

/*******************************************************************************
 * ������� ����������� ����������� ���������� I2C �������� � ������������ ��������.
 ******************************************************************************/
events__e i2c__deinit() {
    CRITICAL_SECTION_ON
    i2c__deinit_short();
    for(int x = 0; x <= 1; x++)
        i2c_ports[x].i2c_id = -1;
    LPC_SWM->PINASSIGN5 |= 0xFFFF;
    LPC_SYSCON->SYSAHBCLKCTRL0 &= ~I2C0;
    i2c.cur_oper = HW_I2C__NO_INIT;
    CRITICAL_SECTION_OFF
    return EVENT__OK;
}

/*******************************************************************************
 * ������� ����������� ����������� ���������� I2C �������� � ������������ ��������.
 ******************************************************************************/
static void i2c__deinit_short() {
    NVIC_DisableIRQ(I2C0_IRQn);
    LPC_SYSCON->PRESETCTRL0 &= (I2C0_RST_N); // Reset I2C
    LPC_SYSCON->PRESETCTRL0 |= ~(I2C0_RST_N);
    supervisor__idle_unlock(&i2c.sub);
}

/*******************************************************************************
 * ������� ������ ������ �� ����������� I2C.
 ******************************************************************************/
events__e i2c__rx(int i2c_id, int addr, uint8_t *buff, int len, i2c__cb_t isr_cb, void* ext_data) {
    events__e res = EVENT__NO_MEM;
    CRITICAL_SECTION_ON
    if (buff == NULL || len == 0)
        res = EVENT__PARAM_NA;
    else if (i2c.cur_oper == HW_I2C__OPER_NONE){
        for (int x = 0; x <= 1; x++)
            if (i2c_id == i2c_ports[x].i2c_id) {
                res = i2c__init_continue(x);
                i2c.i2c_cur_port = x;
                i2c.cur_oper = HW_I2C__OPER_RX;
                supervisor__idle_lock(&i2c.sub);
                i2c.buff = buff;
                i2c.len = len;
                i2c.isr_cb = isr_cb;
                i2c.addr = addr;
                i2c.bytes_count = 0;
                i2c.i2c_delay = 0x00;
                // ������������ ������� START
                LPC_I2C0->MSTDAT = I2C_ADDRESS_7BITS(addr, I2C_MODE_READ);
                LPC_I2C0 ->MSTCTL = 0x00;
                //��������� ����������
                LPC_I2C0->INTENSET = ISR_CHANGE;
                LPC_I2C0 ->MSTCTL = 0x02;
            }
    }
    else
        res = EVENT__BUSY;
    CRITICAL_SECTION_OFF
    return res;
}

/*******************************************************************************
 * ������� ������ ������ �� ����������� I2C.
 ******************************************************************************/
events__e i2c__tx(int i2c_id, int addr, uint8_t *buff, int len, i2c__cb_t isr_cb, void* ext_data) {
    events__e res = EVENT__NO_MEM;
    CRITICAL_SECTION_ON
    if (buff == NULL || len == 0)
        res = EVENT__PARAM_NA;
    else if (i2c.cur_oper == HW_I2C__OPER_NONE) {
        for (int x = 0; x <= 1; x++)
            if (i2c_id == i2c_ports[x].i2c_id) {
                res = i2c__init_continue(x);
                supervisor__idle_lock(&i2c.sub);
                i2c.i2c_cur_port = x;
                i2c.cur_oper = HW_I2C__OPER_TX;
                i2c.buff = buff;
                i2c.len = len;
                i2c.isr_cb = isr_cb;
                i2c.addr = addr;
                i2c.bytes_count = 0;
                i2c.i2c_delay = 0x00;
                // ������������ ������� START
                LPC_I2C0->MSTDAT = I2C_ADDRESS_7BITS(addr, I2C_MODE_WRITE);
                LPC_I2C0 ->MSTCTL = 0x00;
                //��������� ����������
                LPC_I2C0->INTENSET = ISR_CHANGE;
                LPC_I2C0 ->MSTCTL = 0x02;
            }
    }
    else
        res = EVENT__BUSY;
    CRITICAL_SECTION_OFF
    return res;
}

/*******************************************************************************
 * ������� �������� ������ ��� ��������� �������� ��������� �������� I2C.
 ******************************************************************************/
void i2c__cout(void) {
    if (i2c.cur_oper != HW_I2C__OPER_WAIT)
        return;
    i2c.cur_oper = HW_I2C__OPER_NONE;
    supervisor__idle_unlock(&i2c.sub);
    if (i2c.isr_cb != NULL)
        i2c.isr_cb(i2c_ports[i2c.i2c_cur_port].i2c_id, i2c.event, i2c.addr, i2c.buff, i2c.len, NULL);
}

/*******************************************************************************
 * * �������-���������� ���������� �� ������ � �������� I2C.
 ******************************************************************************/
void I2C__IRQHandler(int i2c_id) {
    uint32_t i2c_state = LPC_I2C0->STAT, i2c_ISR_state = LPC_I2C0->INTSTAT;
    uint8_t i2c_error = 0;
    switch(i2c_state & 0x0F) { // + MASTERPENDING bit
        case 0x01:   // IDLE
        case 0x03:   // READ
        case 0x05:   // WRITE
            if (i2c.bytes_count < i2c.len) {
                (i2c.cur_oper == HW_I2C__OPER_TX) ? (LPC_I2C0->MSTDAT = *(i2c.buff + i2c.bytes_count)) : (*(i2c.buff + i2c.bytes_count) = LPC_I2C0->MSTDAT);
                i2c.bytes_count++;
                ((i2c.len > i2c.bytes_count) || (i2c.cur_oper == HW_I2C__OPER_TX)) ? (LPC_I2C0 ->MSTCTL = 0x01) : (LPC_I2C0 ->MSTCTL = 0x04);
            }
            else {
                if (i2c.cur_oper == HW_I2C__OPER_TX)
                    LPC_I2C0 ->MSTCTL = 0x04;
                LPC_I2C0->INTENCLR = 0xFFFFFFFF;
                i2c.event = EVENT__OK;
                i2c.cur_oper = HW_I2C__OPER_WAIT;
            }
            break;
        case 0x07:   // NACK
        case 0x09:
            i2c_error = 1;
        default:
            break;
    }
    if ((i2c_error) || (i2c_ISR_state & 0x30B8950)) {
        i2c__init_continue(i2c.i2c_cur_port);
        i2c.event = EVENT__ERROR;
        i2c.cur_oper = HW_I2C__OPER_WAIT;
    }
}
