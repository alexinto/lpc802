/***************************************************************************//**
 * @file adc.c.
 * @brief Исходный файл с функциями модуля ADC.
 * @author a.tushentsov.
 ******************************************************************************/
#include "System/framework.h"
#include "System/sw_timer.h"
#include "mcu.h"
#include "LPC802/lpc8xx.h"
#include "adc.h"

#define MCU_ADC__COUNT 12

typedef struct {
    uint8_t init        :1;
    uint8_t conversion  :2;
    adc__handler_t      adc_cb;
    uint16_t*           buff[MCU_ADC__COUNT];
} adc_t;

static adc_t adc;
static framework__sub_t cout_sub;

static void adc__cout(void);

events__e adc__init(uint16_t adc_ch, uint16_t* buff) {
    framework__cout_subscribe(&cout_sub, adc__cout);
    LPC_SYSCON->PDRUNCFG &= ~(1 << 4);  // Wake_up ADC power up.
    LPC_SYSCON->PDAWAKECFG &= ~(1 << 4);
//    LPC_SYSCON->ADCCLKSEL = 0x00;   // 0- FRO, 1- EXT_CLK, 2,3- no_clk
//    LPC_SYSCON->ADCCLKDIV = 100;   // 1- divide by 1, 255- divide by 255

    LPC_SYSCON->SYSAHBCLKCTRL[0] |= ADC; // Enable clock for ADC
    LPC_ADC->CTRL = 0xFF;                      // Делитель частоты ADC
    LPC_SWM->PINENABLE0 &= ~((1 << adc_ch) << 10); // ADC_0 = 10, ADC_11 = 21.
    LPC_ADC->SEQA_CTRL |= (1 << adc_ch);

//    NVIC->IP[4] = 0xFFFFFFFF;       // Priority ADC = lowest
    NVIC_EnableIRQ(ADC_SEQA_IRQn);
    NVIC_EnableIRQ(ADC_OVR_IRQn);
    adc.buff[adc_ch] = buff;
    adc.init = 1;
    adc.conversion = 0;
    return EVENT__OK;
}

events__e adc__start(adc__handler_t adc_handler) {
    events__e res = EVENT__OK;
    CRITICAL_SECTION_ON
    if (!adc.init)
        res = EVENT__ERROR;
    else {
        adc.conversion = 1;
        adc.adc_cb = adc_handler;
        LPC_ADC->INTEN = 0x05;
        LPC_ADC->SEQA_CTRL |= (1 << 26) | (0xC4000000);
    }
    CRITICAL_SECTION_OFF
    return res;
}


void adc__deinit(void) {
    CRITICAL_SECTION_ON
    LPC_SYSCON->PRESETCTRL0 &= ~(ADC_RST_N);
    LPC_SYSCON->PRESETCTRL0 |= ADC_RST_N;
    NVIC->ICER[0] = (0x09 << ADC_SEQA_IRQn);
    LPC_ADC->SEQA_CTRL = 0x00;
    LPC_ADC->INTEN = 0x00;
    LPC_ADC->FLAGS = 0xffffffff;
    LPC_SWM->PINENABLE0 |= (0xFFF << 10); // ADC_0 = 10, ADC_11 = 21.
    LPC_SYSCON->SYSAHBCLKCTRL[0] &= ~(ADC); // Enable clock for ADC
    LPC_SYSCON->PDRUNCFG |= (1 << 4);  // Wake_up ADC power down.
    NVIC_DisableIRQ(ADC_SEQA_IRQn);
    NVIC_DisableIRQ(ADC_OVR_IRQn);
    adc.init = 0;
    adc.conversion = 0;
    CRITICAL_SECTION_OFF
}


static void adc__cout(void) {
    int temp;
    if ((adc.conversion >= 2) && (adc.init)) {
        temp = LPC_ADC->SEQA_CTRL & 0xFFF;
        for(int x = 0; x < MCU_ADC__COUNT; x++) {
            if ((temp & (0x01)) && (adc.buff[x])) {
                *adc.buff[x] = (LPC_ADC->DAT[x] >> 4) & 0xFFF;
            }
            temp = (temp >> 1);
        }
        temp = adc.conversion;
        adc.conversion = 0;
        if (adc.adc_cb)
            adc.adc_cb(temp == 3 ? EVENT__ERROR : EVENT__OK);
    }
}


void ADC__IRQHandler(int event) {
    LPC_ADC->FLAGS = 0xffffffff;
    LPC_ADC->INTEN = 0x00;

    if (event == EVENT__OK)
        adc.conversion = 2;
    else
        adc.conversion = 3;
}

