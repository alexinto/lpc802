/***************************************************************************//**
 * @file hw_timer.c.
 * @brief ������ ����������� �������.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "target.h"
#include "System/framework.h"
#include "System/hw_timer.h"
#include "LPC802/lpc8xx.h"



// ���������� ��������� ������ ����������� �������.
typedef struct {
    sw_timer__sys_time_t cur_time;           // ������� �����.
    sw_timer__sys_time_t time_alrm;          // ����� ��������.
    hw_timer__isr_handler_t isr_handler;     // ��������� �� ����������, ��������� � ���������� ����������� �������.
    sw_timer__event_handler_t event_handler; // ��������� �� ���������� ����������� ��������.
    int init;                                // ���� ����, ��� ������������������ ������ �������.
} hw_timer__t;

static hw_timer__t hw_timer;

/*******************************************************************************
 * ������� ������������� ����������� �������, ������������� ��� ���������� ����������� ��������.
 ******************************************************************************/
events__e hw_timer__init(hw_timer__isr_handler_t isr_handler, sw_timer__event_handler_t event_handler) {
    CRITICAL_SECTION_ON
    LPC_SYSCON->LPOSCCLKEN = 0x03;
    LPC_SYSCON->PDSLEEPCFG &= ~(1 << 6);
    LPC_SYSCON->PDAWAKECFG &= ~(1 << 6); // Wake_up Lo_osc power up.
    LPC_SYSCON->SYSAHBCLKCTRL[0] |= WWDT | WKT;
    LPC_SYSCON->PDRUNCFG &= ~(1 << 6);
#ifdef WDT_ON
    LPC_WWDT->MOD = 0x2B;
    LPC_WWDT->WINDOW = 0xFFFFFE;
#else
    LPC_WWDT->MOD = 0x29;
#endif
    LPC_WWDT->TC = 250999;
    LPC_WWDT->WARNINT = 1000;
    LPC_WWDT->FEED = 0xAA;
    LPC_WWDT->FEED = 0x55;
    LPC_SYSCON->STARTERP1 |= (1 << 12) | (1 << 15);         // WKT wake-up enable
    LPC_WKT->CTRL = 0x03;
    LPC_WKT->COUNT = 0x00;
//    NVIC->IP[3] |= 1 << 30;   // Priority WKT < WDT, WDT = max_priority.
    CRITICAL_SECTION_OFF

    hw_timer.isr_handler = isr_handler;
    hw_timer.event_handler = event_handler;
    hw_timer.cur_time.timestamp = 0;
    hw_timer.cur_time.ms = 0;
    hw_timer.time_alrm.ms = -1;
    hw_timer.init = 1;
    NVIC_EnableIRQ(WDT_IRQn);
    NVIC_EnableIRQ(WKT_IRQn);

    return EVENT__OK;
}

/*******************************************************************************
 * ������� �������� ������ ��� ��������� �������� ��������� ������ hw_timer.
 ******************************************************************************/
void hw_timer__cout(void) {

}

/*******************************************************************************
 * ������� ��������� ���������� �������.
 * For 4ms before reset will be return (1004 > time.ms > 1000). May be use for
 * debug log.
 ******************************************************************************/
void hw_timer__sys_time_get(sw_timer__sys_time_t* time) {
    int time_ss, time_ss_1;
    sw_timer__sys_time_t res;
    CRITICAL_SECTION_ON
        do {
            time_ss = LPC_WWDT->TV;
            time_ss_1 = LPC_WWDT->TV;
        } while(time_ss != time_ss_1);

        time_ss = 250999 - time_ss;
        time_ss = time_ss / 250;
        if ((time_ss_1 % 250) < 125)
            time_ss++;
        hw_timer.cur_time.ms = time_ss;
        res = hw_timer.cur_time;
        if (LPC_WWDT->MOD & 0x04) {
            res.timestamp++;
            res.ms = 0;
        }
    CRITICAL_SECTION_OFF
    if (time)
        *time = res;
}

/*******************************************************************************
 * ������� ������� ������������ ���������� ������� � �������� �����.
 ******************************************************************************/
void hw_timer__start(sw_timer__sys_time_t time) {
    int time_ss, time_ss_cur, time_ss_cur_1;
    if (time.ms < 0) {  // ���������.
        LPC_WKT->COUNT = 0;
        return;
    }
    hw_timer.time_alrm = time;
    CRITICAL_SECTION_ON
            time_ss = (hw_timer.time_alrm.ms) * 250;
            time_ss = 250999 - time_ss;
            do {
                time_ss_cur = LPC_WWDT->TV;
                time_ss_cur_1 = LPC_WWDT->TV;
            }while(time_ss_cur != time_ss_cur_1);

            if (hw_timer.time_alrm.timestamp <= hw_timer.cur_time.timestamp) {
                if (((time_ss_cur - time_ss) <= 0) || ((time_ss_cur - time_ss) < 250) || (hw_timer.time_alrm.timestamp < hw_timer.cur_time.timestamp)) {
                    if ((time_ss = time_ss_cur - 250) < 0) {
                        hw_timer.time_alrm.timestamp++;
                        hw_timer.time_alrm.ms = 0;
                        time_ss_cur_1 = time_ss + 250;
                    }
                }
               LPC_WKT->COUNT = time_ss_cur_1 - time_ss;
            }
    CRITICAL_SECTION_OFF
}

/*******************************************************************************
 * �������-���������� ���������� ����������� �������.
 ******************************************************************************/
void hw_timer__alarm_IRQHandler(void) {
    hw_timer.isr_handler();
}

/*******************************************************************************
 * �������-���������� ���������� ����������� ������� �� ������������. ����������
 * ������ 1 ������.
 ******************************************************************************/
void hw_timer__time_IRQHandler(void) {
    if (!hw_timer.init)
        return;

    hw_timer.cur_time.timestamp++;
    hw_timer__sys_time_get(&hw_timer.cur_time);   // ������� ������ ����� � ���������� hw_timer.cur_time.

    if (hw_timer.time_alrm.ms < 0) {
        LPC_WKT->COUNT = 0;
        return;
    }
    if (hw_timer.time_alrm.timestamp < hw_timer.cur_time.timestamp)
        hw_timer.isr_handler();
    else if (hw_timer.time_alrm.timestamp == hw_timer.cur_time.timestamp) {
        if (hw_timer.time_alrm.ms <= hw_timer.cur_time.ms)
            hw_timer.isr_handler();
        else
            hw_timer__start(hw_timer.time_alrm);
    }
}


