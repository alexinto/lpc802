/***************************************************************************//**
 * @file ic_flash_mx25.c
 * @brief ������ ������ � ���� MX25R6435F.
 * @authors a.tushentsov.
 ******************************************************************************/

#include "target.h"
#include "hw.h"
#include "ic_flash_mx25.h"
#include "spi.h"
#include "sw_timer.h"

typedef enum {
    cmd =   0,
    addr =  1,
    value = 1,
}request_type_e;

typedef enum {
    CMD_READ_STATUS    = 0,     // �������� ������ STATUS ��������. �� ���������� � ��������� �������� ���� �� �������� ���� ���������.
    CMD_READ_DATA      = 1,     // �������� ������ ������.
    CMD_WRITE_STATUS   = 2,     // �������� ������ ������ ��������- ����� �� �������, �� ������������...
    CMD_WRITE_ENABLE   = 3,     // �������� ��������� ������ �� ������ � ����������.
    CMD_WRITE_DISABLE  = 4,     // �������� ��������� ������ �� ������ � ����������.
    CMD_WRITE_PAGE     = 5,     // �������� ������ ������. ����� ������� ��������, ������ ������ � ������ ������������� ��������� � �.�.
    CMD_ERASE_SECTOR   = 6,     // �������� �������� �������.
    CMD_WAIT           = 7,     // �������� �������� � ������������� CS ������. ������������ ������������� ����� ������ �������� �������.
    CMD_CHECK_WRITE    = 8,     // �������� �������� ������� �� ������- ���������� ERROR, ���� ��� �������.
    CMD_END            = 9,     // ������� ����� �������.
} mx25_cmd__e;

typedef struct {
    u8 cmd;     // ���� ������� (�� ��������).
    u8 len;     // ����� ������, ������� ���� �������. ������ ������� ������ 5!!!
} cmd_descr__t;
//                                          0        1        2        3       4        5        6        7        8        9
static const cmd_descr__t cmd_table[] = {0x05, 2, 0x03, 4, 0x01, 4, 0x06, 1, 0x04, 1, 0x02, 4, 0x20, 4, 0x00, 1, 0x05, 2, 0x00, 1};


// todo ����� ������� � ��������� ����!-----------------------------------------------------------------------------------------------------------------------
#define PAGE_SIZE 256 // todo ����� ����-�� ��������!!!

// ������� ��� ��������.
static const mx25_cmd__e oper_read[] = {CMD_WAIT, CMD_READ_STATUS, CMD_READ_DATA, CMD_READ_STATUS, CMD_END};
static const mx25_cmd__e oper_write[] = {CMD_WAIT, CMD_READ_STATUS, CMD_WRITE_ENABLE, CMD_CHECK_WRITE, CMD_WRITE_PAGE, CMD_READ_STATUS, CMD_WRITE_DISABLE, CMD_END};
static const mx25_cmd__e oper_erase_sector[] = {CMD_WAIT, CMD_READ_STATUS, CMD_WRITE_ENABLE, CMD_CHECK_WRITE, CMD_ERASE_SECTOR, CMD_READ_STATUS, CMD_WRITE_DISABLE, CMD_END};
// ������ �������������� �������� ���� - ������������� flash__op_e.
static const mx25_cmd__e* const oper_table[] = {NULL, oper_read, oper_write, oper_erase_sector, oper_erase_sector, NULL, NULL, NULL};
// -----------------------------------------------------------------------------------------------------------------------------------------------------------


#define MAX_ADDR(a) (a->page_size * a->pages_in_sector * a->sectors - 1)

static struct {
    u8 request[5], answer[2], err_counter;
    mx25_cmd__e* cur_oper;
    u8* buff;
    u32 len;
    u32 addr, offset;
    flash__hw_cb cb;
    sw_timer__t timer;
    flash__t* descr;
}mx25_data;

static ax_event__e mx25_cmd_exec(mx25_cmd__e cmd);
static void mx25_cmd_exec_cb(int id, int cs_id,  spi__event_e code,uint8_t *buff, int len, void *ext_data);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);


ax_event__e ic_flash_mx25_exec(struct flash__t* flash_ptr, flash__op_e oper, uint32_t addr, uint8_t *buff, uint32_t len, flash__hw_cb cb) {

  switch(oper) {
    case FLASH__OP_READ:
    case FLASH__OP_WRITE:
    case FLASH__OP_ERASE_PAGE:
    case FLASH__OP_ERASE_SECTOR:
        if ((cb) && (addr < MAX_ADDR(flash_ptr->type_descr->map)) && (mx25_data.cur_oper = (mx25_cmd__e*)oper_table[oper]))
            break;
    default:
        return AX_EVENT__PARAMS_NA;
    }
    mx25_data.offset = mx25_data.err_counter = 0;
    mx25_data.addr = addr;
    mx25_data.buff = buff;
    mx25_data.len = len;
    mx25_data.cb = cb;
    mx25_data.descr = flash_ptr;
    return mx25_cmd_exec(*mx25_data.cur_oper);
}

static ax_event__e mx25_cmd_exec(mx25_cmd__e cmd_exec) {
    cmd_descr__t cmd_descr = cmd_table[cmd_exec];
    u32 cur_len = cmd_descr.len, cur_addr = mx25_data.addr + mx25_data.offset;
    u8* buff_tx = mx25_data.request, *buff_rx = (((cur_len < 3) && (cmd_descr.cmd)) ? mx25_data.answer : NULL);
    u8 addr_len = (cur_len > 1) ?  cur_len - 2 : 0;
    mx25_data.request[cmd] = cmd_descr.cmd;
    for(u8 x = 0; x <= addr_len; x++)       // ���������� ������ � MSB first.
        mx25_data.request[addr + x] = *((u8*)&cur_addr + addr_len - x);
    spi__txrx(MX25_SPI_ID, cmd_descr.cmd ? IC_MX25__SPI_CS_ID : -1, IC_FLASH_MX25__SPI_SETTINGS, buff_rx, buff_tx, cur_len, 0, mx25_cmd_exec_cb, NULL);
    return AX_EVENT__OK;
}

static void mx25_cmd_exec_cb(int id, int cs_id,  spi__event_e code,uint8_t *buff, int len, void *ext_data) {
    ax_event__e res = code == SPI__EVENT_OK ? AX_EVENT__OK : AX_EVENT__ERROR;
    u8 status_check = 0x01;
    u16 write_offset = PAGE_SIZE - ((mx25_data.addr + mx25_data.offset) % PAGE_SIZE);
    mx25_cmd__e operation = (ext_data || (cs_id == -1)) ? CMD_WAIT : *mx25_data.cur_oper;
    switch(operation) {
    case CMD_CHECK_WRITE:
        status_check = 0x03;
    case CMD_READ_STATUS:
        if ((mx25_data.answer[value] & status_check) != (status_check -1)) {
            if (mx25_data.err_counter++ < 100) {
                mx25_data.cur_oper -= (status_check >> 1) + 1;
                sw_timer__start(&mx25_data.timer, 5, timer_cb, (void*)CMD_WAIT);
                return;
            }
            res = AX_EVENT__ERROR;
        }
    case CMD_WRITE_ENABLE:
    case CMD_WRITE_DISABLE:
    case CMD_ERASE_SECTOR:
    case CMD_WAIT:
        break;
    case CMD_READ_DATA:
        spi__txrx(MX25_SPI_ID, IC_MX25__SPI_CS_ID, IC_FLASH_MX25__SPI_SETTINGS, mx25_data.buff, NULL, mx25_data.len, 0, mx25_cmd_exec_cb, (void*)CMD_WAIT);
        return;
    case CMD_WRITE_PAGE:
        mx25_data.err_counter = 0;
        if ((mx25_data.offset + write_offset) > mx25_data.len)
            write_offset = mx25_data.len - mx25_data.offset;
        mx25_data.offset += write_offset;
        if (mx25_data.offset < mx25_data.len)
            mx25_data.cur_oper = (mx25_cmd__e*)oper_write;
        spi__txrx(MX25_SPI_ID, IC_MX25__SPI_CS_ID, IC_FLASH_MX25__SPI_SETTINGS, NULL, mx25_data.buff + mx25_data.offset - write_offset, write_offset, 0, mx25_cmd_exec_cb, (void*)CMD_WAIT);
        return;
    default:
        break;
    }
    if ((*mx25_data.cur_oper == CMD_END) || (res != AX_EVENT__OK) || ((res = mx25_cmd_exec((cs_id == -1) ? *++mx25_data.cur_oper : CMD_WAIT)) != AX_EVENT__OK))
        mx25_data.cb(mx25_data.descr, res, mx25_data.addr, mx25_data.buff, mx25_data.len);
}

static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    mx25_cmd_exec_cb(MX25_SPI_ID, -1, SPI__EVENT_OK, NULL, 0, ext_data);
}