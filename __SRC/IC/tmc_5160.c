/***************************************************************************//**
 * @file tmc_5160.c.
 * @brief  ������� ���������� TMC5160.
 * @author a.tushentsov.
 ******************************************************************************/
#include "string.h"
#include "target.h"
#include "spi.h"
#include "tmc_5160.h"

typedef enum {
    TMC_IDLE  = 0,
    TMC_INIT =  1,
    TMC_READ =  2,
    TMC_WRITE = 3,
}tmc_5160__state_e;

typedef struct {
    u8 addr;
    u8 data[4];
}tmc_5160__param_t;

typedef struct {
    u32 cur_max_speed[TMC_5160_NONE];
    tmc_5160__state_e state;
    tmc_5160__param_e param_id;
    tmc_5160__param_t param_buff;
    tmc_5160__cb_t cb;
    void* ext_data;
}tmc_5160__data_t;

typedef struct {
    u8 reg;
    u8 offset;
    u32 mask;
}tmc_5160__param_destr_t;

static tmc_5160__param_destr_t param_descr_tbl[] = {
    1,  0, 0x1F,         // TMC_I_HOLD
    1,  8, 0x1F,         // TMC_I_RUN
    4,  0, 0xFFFFF,      // TMC_STEALTH_SPEED
    6,  0, 0xFFFFFFFF,   // TMC_CUR_POS
    7,  0, 0xFFFFFFFF,   // TMC_DEST_POS
    8,  0, 0x3FFFF,      // TMC_MIN_SPEED
    12, 0, 0x7FFFFF,     // TMC_MAX_SPEED
    11, 0, 0xFFFF,       // TMC_ACC
    13, 0, 0xFFFF,       // TMC_DEC
    17, 9, 0x1,          // TMC_POS_REACHED
    0,  0, 0x0F,         // TMC_TOFF
    15, 0, 0x3FFFF,      // TMC_STOP_SPEED
    0,  0, 0x00,         // TMC_INITIALIZE
    12, 0, 0x7FFFFF,     // TMC_SAVE_MAX_SPEED
    12, 0, 0x7FFFFF,     // TMC_LOAD_MAX_SPEED
};

static const tmc_5160__param_t default_reg_tbl[] = {
    0xEC, 0x00, 0x01, 0x00, 0xC3, // 0 CHOPCONF: TOFF=3, HSTRT=4, HEND=1, TBL=2, CHM=0 (SpreadCycle)
    0x90, 0x00, 0x06, 0x0F, 0x01, // 1 IHOLD_IRUN: IHOLD=1, IRUN=16 (max. current), IHOLDDELAY=6
    0x91, 0x00, 0x00, 0x00, 0x06, // 2 TPOWERDOWN=6: Delay before power down in stand still
    0x80, 0x00, 0x00, 0x30, 0x05, // 3 EN_PWM_MODE=1 enables StealthChop (with default PWM_CONF), DIAG1 = COMPARE, OFF = Calibration
    0x93, 0x00, 0x00, 0x00, 0x10, // 4 TPWM_THRS=500 yields a switching velocity about 35000 = ca. 30RPM // StealthChop En
    0xA0, 0x00, 0x00, 0x00, 0x00, // 5 RAMPMODE = 0: Positioning mode (using all A, D and V parameters)
    0xA1, 0x00, 0x00, 0x00, 0x00, // 6 Reset pos
    0xAD, 0x00, 0x00, 0x00, 0x00, // 7 XTARGET = 00
    0xA3, 0x00, 0x00, 0x0F, 0x32, // 8 VStart = 50 First speed
    0xA4, 0x00, 0x00, 0xF3, 0xE8, // 9 A1 = 1 000 First acceleration
    0xA5, 0x00, 0x00, 0x00, 0x00, // 10 V1 = Disable, 2 000 Acceleration threshold velocity V1
    0xA6, 0x00, 0x00, 0xFF, 0xD0, // 11 AMAX = 2000 Acceleration above V1
    0xA7, 0x00, 0x04, 0x00, 0x00, // 12 VMAX = VAL - 512
    0xA8, 0x00, 0x00, 0xFF, 0xD0, // 13 DMAX = 2000 Deceleration above V1
    0xAA, 0x00, 0x00, 0xF3, 0xE8, // 14 D1 = 50 Deceleration below V1
    0xAB, 0x00, 0x00, 0x0F, 0x32, // 15 VSTOP = 50 Stop velocity (Near to zero)
    0xA0, 0x00, 0x00, 0x00, 0x00, // 16 RAMPMODE = 0 (Target position move)
    0x35, 0x00, 0x00, 0x00, 0x00, // 17 TARGET POS REACHED !!! todo ����� ISR ��� ���� �������)
};

static tmc_5160__param_t tmc_reg_tbl[TMC_5160_NONE][sizeof(default_reg_tbl)/ sizeof(*default_reg_tbl)];

static tmc_5160__data_t tmc_5160_data;


static int u8_to_int(u8* source);
static void int_to_u8(int source, u8* dest);
static void tmc_5160__spi_cb(int id, int cs_id,  events__e code, uint8_t *buff, int len, void *ext_data);


/*******************************************************************************
 * ������� ��������.
 ******************************************************************************/
static void tmc_5160__spi_cb(int id, int cs_id,  events__e code, uint8_t *buff, int len, void *ext_data) {
    int tmc_id = (int)ext_data, param_val = 0;
    events__e res = EVENT__CONTINUE;
    tmc_5160__param_t* buff_rx = (tmc_5160__param_t*)buff, *buff_tx = &tmc_5160_data.param_buff;
    len = 5;
    if (cs_id != -1) {
        tmc_id = -1;
        len = 1;
    }
    else {
        switch(tmc_5160_data.state) {
        case TMC_INIT:
            if (tmc_5160_data.param_id < (sizeof(default_reg_tbl)/ sizeof(*default_reg_tbl) - 1)) {
                tmc_5160_data.param_id++;
                buff_tx = &tmc_reg_tbl[tmc_id][tmc_5160_data.param_id];
                break;
            }
            tmc_5160_data.param_id = TMC_INITIALIZE;
        case TMC_WRITE:
            param_val = *(int*)&tmc_5160_data.param_buff.data;
            res = EVENT__OK;
            break;
        case TMC_READ:
            if (!buff) {
                buff_rx = &tmc_5160_data.param_buff;
                buff_tx = NULL;
                break;
            }
            param_val = (u8_to_int(tmc_5160_data.param_buff.data) >> param_descr_tbl[tmc_5160_data.param_id].offset) & param_descr_tbl[tmc_5160_data.param_id].mask;
             res = EVENT__OK;
             break;
        }
    }
    if ((code != EVENT__OK) || (res != EVENT__CONTINUE) || (spi__txrx(TMC_5160_SPI, tmc_id, (u8*)buff_rx, (u8*)buff_tx, len, tmc_5160__spi_cb, ext_data) != EVENT__OK)) {
        tmc_5160_data.state = TMC_IDLE;
        tmc_5160_data.cb((int)ext_data, res != EVENT__CONTINUE ? res : EVENT__ERROR, tmc_5160_data.param_id, param_val, tmc_5160_data.ext_data);
    }
}

/*******************************************************************************
 * ������� ��������� ���������� � TMC5160.
 ******************************************************************************/
events__e tmc_5160__set_param(int tmc_id, tmc_5160__param_e param_id, int param, tmc_5160__cb_t cb, void* ext_data) {
    if ((tmc_5160_data.state) || (param_id >= TMC_PARAM_NONE))
        return EVENT__BUSY;
    tmc_5160_data.state = TMC_WRITE;
    tmc_5160_data.param_id = param_id;
    switch(param_id) {
        case TMC_INITIALIZE:
            tmc_5160_data.state = TMC_INIT;
            tmc_5160_data.param_id = (tmc_5160__param_e)0;
            memcpy(&tmc_reg_tbl[tmc_id], default_reg_tbl, sizeof(default_reg_tbl));
            tmc_5160_data.cur_max_speed[tmc_id] = u8_to_int((u8*)&tmc_reg_tbl[tmc_id][param_descr_tbl[TMC_MAX_SPEED].reg].data);
            break;
        case TMC_MAX_SPEED:
            tmc_5160_data.cur_max_speed[tmc_id] = param;
            break;
        case TMC_SET_HOME_SPEED:
            if (param < 0x10000)
                param = 0x10000;
            break;
        case TMC_RESTORE_SPEED:
            param = tmc_5160_data.cur_max_speed[tmc_id];
            break;
    }
    tmc_5160__param_destr_t* param_ptr = &param_descr_tbl[param_id];
    u8* reg_val = (u8*)&tmc_reg_tbl[tmc_id][param_ptr->reg].data;
    int temp_reg = u8_to_int(reg_val);
    events__e  res = EVENT__ERROR;
    temp_reg &= ~(param_ptr->mask << param_ptr->offset);
    param &= param_ptr->mask;
    temp_reg |= param << param_ptr->offset;
    int_to_u8(temp_reg, reg_val);
    tmc_5160_data.cb = cb;
    tmc_5160_data.ext_data = ext_data;
    *(int*)&tmc_5160_data.param_buff.data = param;
    if (spi__txrx(TMC_5160_SPI, tmc_id, NULL, (u8*)&tmc_reg_tbl[tmc_id][param_ptr->reg], 5, tmc_5160__spi_cb, (void*)tmc_id) == EVENT__OK) {
        res = EVENT__OK;
    }
    return res;
}

/*******************************************************************************
 * ������� ������ ���������� �� TMC5160.
 ******************************************************************************/
events__e tmc_5160__get_param(int tmc_id, tmc_5160__param_e param_id, tmc_5160__cb_t cb, void* ext_data) {
    if ((tmc_5160_data.state) || (param_id >= TMC_PARAM_NONE))
        return EVENT__BUSY;
    tmc_5160__param_destr_t* param_ptr = &param_descr_tbl[param_id];
    events__e res = EVENT__ERROR;
    tmc_5160_data.param_buff.addr =  tmc_reg_tbl[tmc_id][param_ptr->reg].addr & (~0x80);
    tmc_5160_data.cb = cb;
    tmc_5160_data.ext_data = ext_data;
    tmc_5160_data.param_id = param_id;
    if (spi__txrx(TMC_5160_SPI, tmc_id, NULL, (u8*)&tmc_5160_data.param_buff, 5, tmc_5160__spi_cb, (void*)tmc_id) == EVENT__OK) {
        tmc_5160_data.state = TMC_READ;
        res = EVENT__OK;
    }
    return res;
}

/*******************************************************************************
 * ������� ������������ �������� SPI_MSB � int_little_endian
 ******************************************************************************/
static int u8_to_int(u8* source) {
    int dest;
    for(int i = 0; i < 4; i++)
        ((u8*)&dest)[3 - i] = *(source + i);
    return dest;
}

/*******************************************************************************
 * ������� ������������ �������� �� int_little_endian � SPI_MSB
 ******************************************************************************/
static void int_to_u8(int source, u8* dest) {
    for(int i = 0; i < 4; i++)
        dest[3 - i] = *(((u8*)&source) + i);
}
