﻿/***************************************************************************//**
 * @file ads1115.c.
 * @brief  Драйвер микросхемы ADS1115.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/sw_timer.h"
#include "i2c.h"
#include "ads1115.h"

#ifndef ADS1115__CNT
#define ADS1115__CNT 1
#endif

#define ADS1115_ADDR (0x90 >> 1)
#define ADC_BUSY 0x80
#define ADS1115_ERR_TIMEOUT 50
#define ADS1115__CH_NUM     4

#pragma pack(push, 1)
typedef struct {
    u8 reg;
    u8 hi_byte;
    u8 lo_byte;
}ads1115_pkt_t;
#pragma pack(pop)

typedef enum {
    ADS1115_CFG_WRITE   = 0,
    ADS1115_CFG_READ    = 1,
    ADS1115_CFG_CHECK   = 2,
    ADS1115_DATA_READ   = 3,
    ADS1115_DATA_CHECK  = 4,
    ADS1115_STATE_IDLE  = 5,
    ADS1115_STATE_ERROR = 6,
}ads1115_state_e;

typedef struct {
    ads1115_pkt_t cfg_buff[ADS1115__CH_NUM];   //ch0, ch1, ch2, ch3
    ads1115_pkt_t data_reg;
    u8 cur_ch;
    sw_timer__t timer;
    ads1115__cb_t cb;
    ads1115_state_e state;
    int i2c_id;
    void* ext_data;
    u16 cmd_interval;
    u8 addr;
}ads1115_data_t;

const float vref_tbl[] = {6.144, 4.096, 2.048, 1.024, 0.512, 0.256};
const u16 interval_tbl[] = {128, 64, 32, 16, 8, 5, 3, 2};


ads1115_data_t ads1115_data[ADS1115__CNT];

static void ads1115__i2c_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);
static void ads1115_tmr_cb(struct sw_timer__t *timer, void *ext_data);


/*******************************************************************************
 * Функция инициализации ADS1115. Конфигурирование параметров.
 ******************************************************************************/
events__e ads1115__init(uint8_t id, uint8_t i2c_id, uint8_t addr, ads1115__vref_e vref, ads1115__speed_e speed) {
    if (id >= ADS1115__CNT)
        return EVENT__PARAM_NA;
    ads1115_data_t* d = &ads1115_data[id];
    d->i2c_id = i2c_id;
    d->cmd_interval = interval_tbl[speed];
    for(int i = 0; i < ADS1115__CH_NUM; i++) {
        d->cfg_buff[i].reg = 0x01;
        d->cfg_buff[i].hi_byte = 0x01 | ((0x0C + i) << 4) | (vref << 1);
        d->cfg_buff[i].lo_byte = 0x03 | (speed << 5);
    }
    d->state = ADS1115_STATE_IDLE;
    d->addr = addr;
    d->cur_ch = 0;
    return i2c__init(i2c_id, 0, 0, NULL);
}


/*******************************************************************************
 * Функция считывания данных ADS1115. Считывание 4х каналов.
 ******************************************************************************/
events__e ads1115__get(u8 id, u8 ch_id, ads1115__cb_t cb, void* ext_data) {
    if (id >= ADS1115__CNT)
        return EVENT__PARAM_NA;
    ads1115_data_t* d = &ads1115_data[id];
    if (d->state != ADS1115_STATE_IDLE)
        return EVENT__BUSY;
    d->cb = cb;
    d->ext_data = ext_data;
    d->cur_ch = ch_id;
    d->state = ADS1115_CFG_WRITE;
    return i2c__tx(d->i2c_id, d->addr, (u8*)&d->cfg_buff[ch_id], sizeof(ads1115_pkt_t), ads1115__i2c_cb, (void*)d);
}


static void ads1115__i2c_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
    ads1115_data_t* d = (ads1115_data_t*)ext_data;
    d->state++;
    if (event != EVENT__OK) {
        d->state = ADS1115_STATE_ERROR;
    }
    sw_timer__start(&d->timer, d->state == ADS1115_CFG_READ ? d->cmd_interval : 0, ads1115_tmr_cb, ext_data);
}


static void ads1115_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    ads1115_data_t* d = (ads1115_data_t*)ext_data;
    float vref, voltage = 0;
    events__e res = EVENT__ERROR;
    switch(d->state) {
        case ADS1115_DATA_READ:
        case ADS1115_CFG_READ:
            res = i2c__rx(d->i2c_id, ADS1115_ADDR, (u8*)&d->data_reg.hi_byte, 2, ads1115__i2c_cb, ext_data);
            break;
        case ADS1115_CFG_CHECK:
            if (d->data_reg.hi_byte & ADC_BUSY) {
                res = EVENT__TIMEOUT;
            }
            else if (((d->data_reg.hi_byte | ADC_BUSY) == d->cfg_buff->hi_byte) && (d->data_reg.lo_byte == d->cfg_buff->lo_byte))
                res = i2c__tx(d->i2c_id, ADS1115_ADDR, &d->data_reg.reg, 1, ads1115__i2c_cb, ext_data);
            break;
        case ADS1115_DATA_CHECK:
            vref = vref_tbl[(d->cfg_buff[d->cur_ch].hi_byte & 0x0F) >> 1];
            voltage = d->data_reg.lo_byte | ((u16)d->data_reg.hi_byte << 8);
            voltage = vref * voltage / 32768;
            break;
        default:
            break;
    }
    if ((res != EVENT__OK) || (d->state == ADS1115_DATA_CHECK)) {
        d->state = ADS1115_STATE_IDLE;
        d->cb(d - ads1115_data, d->cur_ch, res, voltage, d->ext_data);
    }
}
