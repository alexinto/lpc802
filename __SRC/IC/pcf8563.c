/***************************************************************************//**
 * @file hw_rtc.h.
 * @brief ������ ������ � ������ ��������� ������� RTC (������������������ �����).
 *        ������ �������� ������ � UTC ��������.
 *        ���������� ��� ���������� PCF8563.
 * @authors a.tushentsov.
 ******************************************************************************/
#include <time.h>
#include "standard.h"
#include "target.h"
#include "hw_rtc.h"
#include "rtc.h"
#include "sw_timer.h"
#include "i2c.h"

#ifndef DEVICE__ITF_PCF8563T
    #error "You must declare DEVICE__ITF_PCF8563T in device.h"
#endif

#define HW_RTC__ADDR 0x51   // ����� ���������� �� ���� PCF8563.

typedef enum {
    IDLE = 0,
    READ_TIME,
    SET_TIME,
    INIT
}hw_rtc__state_e;

// ��������� ������ hw_rtc.
typedef struct {
    hw_rtc__event_handler_t event_handler;
    sw_timer__sys_time_t time;
    hw_rtc__state_e state;
    uint8_t buff[8];
} hw_rtc__struct_t;

const uint8_t commands = 0x02;  // 0x02- �������, 0x03- ������ � �.�.

hw_rtc__struct_t hw_rtc ;

static void hw_rtc__rx_cb(int i2c_id, i2c__event_e event, int addr, uint8_t *buff, int len, void *ext_data);
static void hw_rtc__tx_cb(int i2c_id, i2c__event_e event, int addr, uint8_t *buff, int len, void *ext_data);

/*******************************************************************************
 *  ������������� ������ ������ � RTC.
 ******************************************************************************/
rtc__event_e hw_rtc__init(hw_rtc__event_handler_t event_handler) {
    // todo ��� ���� ��� ����������? ��������� I2C?, ������?
    hw_rtc.state = INIT;
    hw_rtc.event_handler = event_handler;
    hw_rtc.buff[0] = 0x00;
    hw_rtc.buff[1] = 0x00;
    hw_rtc.buff[2] = 0x00;

    int res = i2c__tx(DEVICE__ITF_PCF8563T, HW_RTC__ADDR, hw_rtc.buff, 3, 0, hw_rtc__tx_cb, NULL);
    switch(res) {
        case I2C__EVENT_BUSY:   res = RTC__EVENT_BUSY;  hw_rtc.state = IDLE; break;
        case I2C__EVENT_OK:     res = RTC__EVENT_OK;    break;
        default:                res = RTC__EVENT_ERR;   hw_rtc.state = IDLE; break;
    }
    return (rtc__event_e) res;
}

/*******************************************************************************
 *  ������� ��������� �������� UTC ������� (timestamp).
 ******************************************************************************/
rtc__event_e hw_rtc__utc_get() {
    if (hw_rtc.state)
        return RTC__EVENT_BUSY;
    hw_rtc.state = READ_TIME;
    if (i2c__tx(DEVICE__ITF_PCF8563T, HW_RTC__ADDR, (uint8_t*)&commands, 1, 0, hw_rtc__tx_cb, NULL) == I2C__EVENT_OK)
        return RTC__EVENT_OK;
    hw_rtc.state = IDLE;
    // todo ���������
    return RTC__EVENT_ERR;
}

/*******************************************************************************
 *  ������� ��������� UTC ������� (timestamp).
 ******************************************************************************/
rtc__event_e hw_rtc__utc_set(int32_t utc) {
    struct tm buff_time;
    if (hw_rtc.state)
        return RTC__EVENT_BUSY;
    hw_rtc.state = SET_TIME;
    rtc__gmtime(utc, &buff_time);
    hw_rtc.buff[0] = 0x02;
    hw_rtc.buff[1] = ((buff_time.tm_sec / 10) << 4) | ((buff_time.tm_sec % 10));
    hw_rtc.buff[2] = ((buff_time.tm_min / 10) << 4) | ((buff_time.tm_min % 10));
    hw_rtc.buff[3] = ((buff_time.tm_hour / 10) << 4) | ((buff_time.tm_hour % 10));
    hw_rtc.buff[4] = ((buff_time.tm_mday / 10) << 4) | ((buff_time.tm_mday % 10));
    hw_rtc.buff[5] = 0;                     // ���� ������
    hw_rtc.buff[6] = (((buff_time.tm_mon / 10) << 4) | ((buff_time.tm_mon % 10))) + 1;
    hw_rtc.buff[7] = ((buff_time.tm_year / 10) << 4) | ((buff_time.tm_year % 10));
    if (i2c__tx(DEVICE__ITF_PCF8563T, HW_RTC__ADDR, hw_rtc.buff, 8, 0, hw_rtc__tx_cb, NULL) == I2C__EVENT_OK)
        return RTC__EVENT_OK;
    hw_rtc.state = IDLE;
    return RTC__EVENT_ERR;
}

/*******************************************************************************
 * ������� ������ ������� ������ hw_rtc.h.
 ******************************************************************************/
static void hw_rtc__tx_cb(int i2c_id, i2c__event_e event, int addr, uint8_t *buff, int len, void *ext_data) {
    rtc__event_e res = RTC__EVENT_ERR;
    if (event == I2C__EVENT_OK) {
        switch (hw_rtc.state) {
        case READ_TIME:
            if (i2c__rx(DEVICE__ITF_PCF8563T, HW_RTC__ADDR, hw_rtc.buff, 8, 0, hw_rtc__rx_cb, NULL) == I2C__EVENT_OK)
                return;
            break;
        case INIT:
            hw_rtc.state = READ_TIME;
            if (i2c__tx(DEVICE__ITF_PCF8563T, HW_RTC__ADDR, (uint8_t*)&commands, 1, 0, hw_rtc__tx_cb, NULL) == I2C__EVENT_OK)
                return;
            break;
        case SET_TIME:
            res = RTC__EVENT_SET_OK;
            break;
        }
    }
    hw_rtc.state = IDLE;
    hw_rtc.event_handler(res, hw_rtc.time.timestamp);
}

/*******************************************************************************
 * ������� ������ ������� ������ hw_rtc.h.
 ******************************************************************************/
static void hw_rtc__rx_cb(int i2c_id, i2c__event_e event, int addr, uint8_t *buff, int len, void *ext_data) {
    rtc__event_e res = RTC__EVENT_OK;
    if (event == I2C__EVENT_OK){
        if (!(hw_rtc.buff[0] & 0x80)) {
            struct tm buff_time = {.tm_sec = (((hw_rtc.buff[0] & 0x7F)  >> 4) * 10) + (hw_rtc.buff[0] & 0x0f),
            .tm_min = (((hw_rtc.buff[1] & 0x7F)  >> 4) * 10) + (hw_rtc.buff[1] & 0x0f),
            .tm_hour = (((hw_rtc.buff[2] & 0x3F)  >> 4) * 10) + (hw_rtc.buff[2] & 0x0f),
            .tm_mday = (((hw_rtc.buff[3] & 0x3F)  >> 4) * 10) + (hw_rtc.buff[3] & 0x0f),
            .tm_mon = (((hw_rtc.buff[5] & 0x1F) >> 4) * 10) + (hw_rtc.buff[5] & 0x0f) - 1,        // 4 - ���� ������.
            .tm_year = ((hw_rtc.buff[6]  >> 4) * 10) + (hw_rtc.buff[6] & 0x0f) };
            hw_rtc.time.timestamp = rtc__mktime(&buff_time);
        }
        else
            res = RTC__EVENT_SET_ERR;
    }
    else
        res = RTC__EVENT_ERR;
    hw_rtc.state = IDLE;
    hw_rtc.event_handler(res, hw_rtc.time.timestamp);
}
