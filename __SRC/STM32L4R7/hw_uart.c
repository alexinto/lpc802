/***************************************************************************//**
 * @file hw_uart.h
 * @brief �������������� ������� ����������� UART.
 * @author a.tushentsov
 ******************************************************************************/
#include <string.h>
#include "standard.h"
#include "target.h"
#include "mcu.h"
#include "hw_uart.h"
#include "uart.h"
#include "supervisor.h"
#include "sw_timer.h"
#include "list.h"
#include "fifo.h"

#ifndef HW_UART__FIFO_SIZE        // �� ����� 512!
    #define HW_UART__FIFO_SIZE 64
#endif

#ifndef UART__CTS_TIMEOUT
    #define UART__CTS_TIMEOUT 5000 // TODO: ���� ���, ����� �� �������� ��� �����?
#endif

#ifdef HW_UART_1_TX
    #define HW_UART_1_USED  1
#else
    #define HW_UART_1_USED  0
#endif
#ifdef HW_UART_2_TX
    #define HW_UART_2_USED  1
#else
    #define HW_UART_2_USED  0
#endif
#ifdef HW_UART_3_TX
    #define HW_UART_3_USED  1
#else
    #define HW_UART_3_USED  0
#endif
#ifdef HW_UART_4_TX
    #define HW_UART_4_USED  1
#else
    #define HW_UART_4_USED  0
#endif
#ifdef HW_UART_5_TX
    #define HW_UART_5_USED  1
#else
    #define HW_UART_5_USED  0
#endif
#ifdef HW_UART_6_TX
    #define HW_UART_6_USED  1
#else
    #define HW_UART_6_USED  0
#endif

#define HW_UART__COUNT (HW_UART_1_USED + HW_UART_2_USED + HW_UART_3_USED + HW_UART_4_USED + HW_UART_5_USED + HW_UART_6_USED)

#define HW_UART_ID_FROM_MCU_ID  (hw_uart__route_tbl[uart_id & UART__ID_MSK])   // ������ ��������� ����������� ������ UART � ������� UART-�� �� ������ ����������� UART. MCU_UART_ID -> HW_UART_ID.

#define UART_CRITICAL_SECTION_ON(uart_id)                                        \
{                                                                                   \
    NVIC_DisableIRQ((IRQn_Type)hw_uart_descr[uart_id & UART__ID_MSK].uart_irq);

#define UART_CRITICAL_SECTION_OFF(uart_id)                                       \
    NVIC_EnableIRQ((IRQn_Type)hw_uart_descr[uart_id & UART__ID_MSK].uart_irq);   \
}

// ������� ���� � ������� ������� ����������.
 typedef struct {
    u32 PE      :1;
    u32 FE      :1;
    u32 NE      :1;
    u32 ORE     :1;
    u32 IDLE    :1;
    u32 RXNE    :1;
    u32 TC      :1;
    u32 TXE     :1;
    u32 LBDF    :1;
    u32 CTSIF   :1;
    u32 CTS     :1;
    u32 RTOF    :1;
    u32 EOBF    :1;
    u32         :1;
    u32 ABRE    :1;
    u32 ABRF    :1;
    u32 BUSY    :1;
    u32 CMF     :1;
    u32 SBKF    :1;
    u32 RWU     :1;
    u32 WUF     :1;
    u32 TEACK   :1;
    u32 REACK   :1;
 } hw_uart__isr_t;

 // ����������� ��� �������� ������� ����������.
 typedef union {
     u32 isr_u32;
     hw_uart__isr_t isr;
 } hw_uart__isr_u;

// ��������� ���������� UART.
typedef struct {
    u32 settings;
    uart__event_e tx_event, rx_event;
    uart__hw_event_handler_t hw_event_handler;       // ��������� �� �������-���������� ������/������� UART.
    u8 *tx_buff;                                     // ��������� �� ����� � ����������������� ������� ��� ��������.
    hw_uart__cb_t tx_isr_cb;                         // ��������� �� ���������������� ������� �� �������� (�� ����������).
    int tx_len;                                      // ������ ������ � ����������������� ������� ��� ��������.
    int tx_bytes_count, rx_bytes_count, save_bytes, offset_bytes;     // �������� ����������\�������� ������.
    u8 rx_buff[HW_UART__FIFO_SIZE];                  // ����� ��� ������ ������.
    u8 rx_offset, init;
    u8 parity, wordlenth9;
    hw_uart__cb_t rx_isr_cb;                         // ��������� �� ���������������� ������� �� ������ ��������� ���������� ���� (�� ����������).
    supervisor__idle_sub_t  idle_sub;
    sw_timer__t timer;
    sw_timer__t cts_timer;
} hw_uart__itf_t;

//
typedef struct {
    int gpio[4];
    u32* uart_clock;
    int uart_clock_flag;
    int uart_irq;
    USART_TypeDef * uart_cmsis;
    u8 af_nb;
} hw_uart_descr__t;

static const hw_uart_descr__t hw_uart_descr[] = {
#ifdef HW_UART_1_TX
    { { HW_UART_1_TX, HW_UART_1_RX, HW_UART_1_RTS, HW_UART_1_CTS }, (u32*)&RCC->APB2ENR, RCC_APB2ENR_USART1EN, USART1_IRQn, USART1, 0x07 },
#endif
#ifdef HW_UART_2_TX
    { { HW_UART_2_TX, HW_UART_2_RX, HW_UART_2_RTS, HW_UART_2_CTS }, (u32*)&RCC->APB1ENR1, RCC_APB1ENR1_USART2EN, USART2_IRQn, USART2, 0x07 },
#endif
#ifdef HW_UART_3_TX
    { { HW_UART_3_TX, HW_UART_3_RX, HW_UART_3_RTS, HW_UART_3_CTS }, (u32*)&RCC->APB1ENR1, RCC_APB1ENR1_USART3EN, USART3_IRQn, USART3, 0x07 },
#endif
#ifdef HW_UART_4_TX
    { { HW_UART_4_TX, HW_UART_4_RX, HW_UART_4_RTS, HW_UART_4_CTS }, (u32*)&RCC->APB1ENR1, RCC_APB1ENR1_UART4EN, UART4_IRQn, UART4, 0x08 },
#endif
#ifdef HW_UART_5_TX
    { { HW_UART_5_TX, HW_UART_5_RX, HW_UART_5_RTS, HW_UART_5_CTS }, (u32*)&RCC->APB1ENR1, RCC_APB1ENR1_UART5EN, UART5_IRQn, UART5, 0x08 },
#endif
#ifdef HW_UART_6_TX
    { { HW_UART_6_TX, HW_UART_6_RX, HW_UART_6_RTS, HW_UART_6_CTS }, (u32*)&RCC->APB1ENR2, RCC_APB1ENR2_LPUART1EN, LPUART1_IRQn, LPUART1, 0x08 },
#endif
};

// ������� �������� �������� ��� ������������ UART �������� ������ ������ + �������� ����-���� �� �������� ���������� �����.
static const u32 hw_uart__16mhz_br_table[11] = {
    0x3412,    //2400
    0x1A05,    //4800
    0x0D02,    //9600
    0x0681,    //19200
    0x0340,    //38400
    0x0225,    //57600
    0x0112,    //115200
    0x0085,    //230400
    0x0042,    //460800
    0x0021,    //921600
};

// ������� �������� ����������������� ��������� ID (uart_id) � ����������� ����������� ������ UART �������� ��������.

// ������� �������� ����������� ����������� ������ UART �������� �������� � ����������������� ��������� ID (uart_id).
// MCU_UART_ID -> HW_UART_ID, ��� MCU_UART_ID - �� 0 �� N (N-���������� UART ��). UART_ID - �� 0 �� K (K - ���������� ���������� UART).
const int hw_uart__route_tbl[MCU__UART_COUNT] = {
    HW_UART_1_USED - 1,
    HW_UART_1_USED + HW_UART_2_USED - 1,
    HW_UART_1_USED + HW_UART_2_USED + HW_UART_3_USED - 1,
    HW_UART_1_USED + HW_UART_2_USED + HW_UART_3_USED + HW_UART_4_USED - 1,
    HW_UART_1_USED + HW_UART_3_USED + HW_UART_3_USED + HW_UART_4_USED + HW_UART_5_USED - 1,
    HW_UART_1_USED + HW_UART_3_USED + HW_UART_3_USED + HW_UART_4_USED + HW_UART_5_USED + HW_UART_6_USED - 1,
};

static hw_uart__itf_t hw_uart[HW_UART__COUNT];

void hw_uart__USART_IRQHandler(int uart_id);
static void hw_uart__itf_en(int uart_id);
static void hw_uart__itf_dis(int uart_id);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void cts_timeout_cb(struct sw_timer__t *timer, void *ext_data);

__weak int uart__cmd_handler(int uart_id, uart__cmd_e cmd) {return 0;}

/*******************************************************************************
 * ������� ������������� ���������� UART-� ������������ �����������.
 ******************************************************************************/
uart__event_e hw_uart__init(int uart_id, u32 settings, uart__hw_event_handler_t hw_event_handler) {
    USART_TypeDef* UART = hw_uart_descr[HW_UART_ID_FROM_MCU_ID].uart_cmsis;
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    uart__event_e res = UART__EVENT_OK;
    uart->settings = settings;
    uart->hw_event_handler = hw_event_handler;

    supervisor__idle_lock(&uart->idle_sub);
    UART_CRITICAL_SECTION_ON(uart_id)
// ������ �������������
        hw_uart__itf_en(uart_id);
        MODIFY_REG(RCC->CCIPR, 0xFFF, 0xAAA);            // UARTs clk = 16Mhz

        if ((settings & UART__FC_MASK) == UART__FC_485)
            uart__cmd_handler(uart_id, UART__CMD_FC_DIR_RX);

        else if ((settings & UART__FC_MASK) == UART__FC_CTS_RTS)
        {
            if (hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[2] != -1) // RTS
            {
                gpio__af_init(hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[2], 0, hw_uart_descr[HW_UART_ID_FROM_MCU_ID].af_nb);
                SET_BIT(UART->CR3, USART_CR3_RTSE);
            }
            if (hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[3] != -1) // CTS
            {
                gpio__af_init(hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[3], 0, hw_uart_descr[HW_UART_ID_FROM_MCU_ID].af_nb);
                SET_BIT(UART->CR3, USART_CR3_CTSE);
            }
        }
        else if ((settings & UART__FC_MASK) == UART__FC_NONE)
        {
            if (hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[2] != -1) // RTS
                gpio__init(hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[2], GPIO__DIR_OUT, NULL);

            if (hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[3] != -1) // CTS
                gpio__init(hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[3], GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);

        }

        //�������� UARTx (x=1/2/3/4/5)
        WRITE_REG(UART->CR1, USART_CR1_UE | USART_CR1_UESM | USART_CR1_OVER8 | USART_CR1_TE);    // ������� �������� ���������� �� ����.
        WRITE_REG(UART->ICR, 0x1B5F | USART_ICR_CMCF | USART_ICR_WUCF);
//��������� ���� ����
        switch (settings & UART__STOP_MASK) {
            case UART__STOP_0_5:
                MODIFY_REG(UART->CR2, USART_CR2_STOP, USART_CR2_STOP_0);
                break;
            case UART__STOP_1:
                CLEAR_BIT(UART->CR2, USART_CR2_STOP);
                break;
            case UART__STOP_1_5:
                SET_BIT(UART->CR2, USART_CR2_STOP);
                break;
            case UART__STOP_2:
                MODIFY_REG(UART->CR2, USART_CR2_STOP, USART_CR2_STOP_1);
                break;
            default:
                res = UART__EVENT_PARAM_NA;
        }
        //��������� ������ ������������ ������
        switch (settings & UART__DATA_MASK) {
            case UART__DATA_7:
                CLEAR_BIT(UART->CR1, USART_CR1_M);
                uart->wordlenth9 = 0;
                break;
            case UART__DATA_8:
                CLEAR_BIT(UART->CR1, USART_CR1_M);
                if (READ_BIT(settings, UART__PAR_MASK) != UART__PAR_NONE) {
                    uart->wordlenth9 = 1;
                    SET_BIT(UART->CR1, USART_CR1_M);
                }
                else
                    uart->wordlenth9 = 0;
                break;
            case UART__DATA_9:
                SET_BIT(UART->CR1, USART_CR1_M);
                uart->wordlenth9 = 1;
                break;
            default:
                res = UART__EVENT_PARAM_NA;
        }
// ��������� ��������
        switch (READ_BIT(settings, UART__PAR_MASK)) {
            case UART__PAR_ODD:
                uart->parity = 1;
                SET_BIT(UART->CR1, USART_CR1_PCE | USART_CR1_PS);
                break;
            case UART__PAR_EVEN:
                uart->parity = 1;
                MODIFY_REG(UART->CR1, USART_CR1_PS, USART_CR1_PCE);
                break;
            default:
                uart->parity = 0;
                CLEAR_BIT(UART->CR1, USART_CR1_PCE);
                break;
        }

        //�������� �� ���������� �������!
        int M = READ_BIT(UART->CR1, USART_CR1_M);     // data length
        int PCE = READ_BIT(UART->CR1, USART_CR1_PCE);    //ParityControl
        int UserDataLenth = READ_BIT(settings, UART__DATA_MASK);

        if (M == 0) {
            if (PCE == 0) {
                if (UserDataLenth != UART__DATA_8)
                    res = UART__EVENT_PARAM_NA;
            }
            else if (PCE != 0) {
                if ((UserDataLenth != UART__DATA_7) & (!uart->wordlenth9))
                    res = UART__EVENT_PARAM_NA;
            }
        }
        else if (M != 0) {
            if (PCE == 0) {
                if (UserDataLenth != UART__DATA_9)
                    res = UART__EVENT_PARAM_NA;
            }
            else if (PCE != 0) {
                if (UserDataLenth != UART__DATA_8)
                    res = UART__EVENT_PARAM_NA;
            }
        }
        else
            res = UART__EVENT_PARAM_NA;
        //��������� �������
        UART->BRR = hw_uart__16mhz_br_table[(settings & UART__BR_MASK) - 4];
        uart->tx_event = uart->rx_event = UART__EVENT_CONTINUE;
        uart->rx_bytes_count = uart->save_bytes = 0;
        uart->rx_isr_cb = NULL;
        uart->init = 1;
        NVIC_SetPriority((IRQn_Type)hw_uart_descr[uart_id & UART__ID_MSK].uart_irq, 2);
    UART_CRITICAL_SECTION_OFF(uart_id)
    if (res == UART__EVENT_OK) {
        int timeout = uart__cmd_handler(uart_id, (uart->settings & UART__FC_MASK) == UART__FC_485 ? UART__CMD_PWR_ON_485 : UART__CMD_PWR_ON_232);
        sw_timer__start(&uart->timer, timeout, timer_cb, (void*)uart_id);
    }
    else
        hw_uart__deinit(uart_id);

    return UART__EVENT_OK;
}

/*******************************************************************************
 * ������� ������ ��������� ��������� ����� RTS
 ******************************************************************************/
void hw_uart__rts_set(int uart_id, gpio__state_e state)
{
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    if (((uart->settings & UART__FC_MASK) == UART__FC_NONE) && (hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[2] != -1))
    {
        gpio__set(hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[2], state);
    }
}

/*******************************************************************************
 * ������� ��������� ��������� ����� �TS
 ******************************************************************************/
gpio__state_e hw_uart__cts_get(int uart_id)
{
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    if (((uart->settings & UART__FC_MASK) == UART__FC_NONE) && (hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[3] != -1))
    {
        return gpio__get(hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[3]);
    }
    return GPIO__STATE_ERROR;
}

/*******************************************************************************
 * ������� ������� ������ ������ �� ���������� ����� �� ���������� ��������� �����.
 ******************************************************************************/
uart__event_e hw_uart__rx(int uart_id, hw_uart__cb_t rx_isr_cb) {
    USART_TypeDef* UART = hw_uart_descr[HW_UART_ID_FROM_MCU_ID].uart_cmsis;
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    UART_CRITICAL_SECTION_ON(uart_id)
        uart->rx_isr_cb = rx_isr_cb;
        //���������� ���������� � ���������� �������� ������ (RXNE)
        SET_BIT(UART->CR1, USART_CR1_RXNEIE_RXFNEIE | USART_CR1_PEIE);
        //��������� �������� �� ������
        SET_BIT(UART->CR3, USART_CR3_EIE);    //�������� ���������� �� FRAGMING/OVERFLOW/NOISE
        // �������� recieve
        SET_BIT(UART->CR1, USART_CR1_RE);
    UART_CRITICAL_SECTION_OFF(uart_id)
    return UART__EVENT_OK;
}

/*******************************************************************************
 * ������� �������� ���������������� ������ � ��������� UART.
 ******************************************************************************/
uart__event_e hw_uart__tx(int uart_id, u8* buff, int len, hw_uart__cb_t tx_isr_cb) {
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    USART_TypeDef* UART = hw_uart_descr[HW_UART_ID_FROM_MCU_ID].uart_cmsis;
    if ((uart->settings & UART__FC_MASK) == UART__FC_485) {
        uart__cmd_handler(uart_id, UART__CMD_FC_DIR_TX);
        CLEAR_BIT(UART->CR1, USART_CR1_RE);
        uart->rx_bytes_count = uart->save_bytes = 0;
    }
    if ((uart->settings & UART__FC_MASK) == UART__FC_CTS_RTS)
        sw_timer__start(&uart->cts_timer, UART__CTS_TIMEOUT, cts_timeout_cb, (void*)uart_id);

    UART_CRITICAL_SECTION_ON(uart_id)
        uart->tx_buff = buff;
        uart->tx_len = len;
        uart->tx_isr_cb = tx_isr_cb;
        uart->tx_bytes_count = 0;
        // ��������
        SET_BIT(UART->ICR, USART_ICR_TCCF);
        int temp = uart->tx_buff[uart->tx_bytes_count];

        if (uart->wordlenth9 == 1)
            temp = (uart->tx_buff[uart->tx_bytes_count] & 0x01FF);
        else
            temp = uart->tx_buff[uart->tx_bytes_count];

        WRITE_REG(UART->TDR, temp);
        SET_BIT(UART->CR1, USART_CR1_TCIE);    //��������� ���������� �� TC
    UART_CRITICAL_SECTION_OFF(uart_id)
    return UART__EVENT_OK;
}

/*******************************************************************************
 * ������� ��������������� ���������� UART-�. ���������� ���������� UART-� ���� �������� � ��������� ���.
 ******************************************************************************/
uart__event_e hw_uart__deinit(int uart_id) {
    hw_uart_descr__t* UART = (hw_uart_descr__t*)&hw_uart_descr[HW_UART_ID_FROM_MCU_ID];
    NVIC_DisableIRQ((IRQn_Type)UART->uart_irq);
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    //��������� ��������� USART
    uart__cmd_handler(uart_id, UART__CMD_PWR_OFF);
    UART->uart_cmsis->CR1 = 0;
    UART->uart_cmsis->CR3 = 0;
    hw_uart__itf_dis(uart_id);
    uart->init = 0;
    supervisor__idle_unlock(&uart->idle_sub);
    uart->hw_event_handler(uart_id, UART__EVENT_CLOSE, NULL);
    return UART__EVENT_OK;
}

/*******************************************************************************
 * ������� ������� ����������� ��������� ������.
 ******************************************************************************/
void hw_uart__rx_flush(int uart_id) {
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    uart->rx_bytes_count = uart->save_bytes = 0;
    uart->rx_isr_cb = NULL;
}

/*******************************************************************************
 * ������� �������� ������ ��� ��������� �������� ��������� �������� UART.
 ******************************************************************************/
void hw_uart__cout(void) {
    for(int i = 0; i < HW_UART__COUNT; i++) {
        hw_uart__itf_t* uart = &hw_uart[i];
        if (!uart->init)
            continue;
        uart__event_e event = uart->tx_event;
        int bytes_cnt;
        int uart_id = i + HW_UART_1;
        u8* cur_buff = uart->rx_buff + uart->rx_offset;
        if ((event != UART__EVENT_CONTINUE)&& (uart->tx_isr_cb)) {
            uart->tx_event = UART__EVENT_CONTINUE;
            uart->tx_isr_cb(uart_id, event, uart->tx_buff, uart->tx_bytes_count);
        }
        if (((event = uart->rx_event) != UART__EVENT_CONTINUE) && (uart->rx_isr_cb)) {
            uart->rx_event = UART__EVENT_CONTINUE;
            if (uart->save_bytes) {
                bytes_cnt = uart->save_bytes;
                cur_buff = uart->rx_offset ? cur_buff - uart->rx_offset : cur_buff + HW_UART__FIFO_SIZE / 2;
                cur_buff += uart->offset_bytes;
            }
            else {
                uart->offset_bytes = 0;
                UART_CRITICAL_SECTION_ON(uart_id)
                    bytes_cnt = uart->rx_bytes_count;
                    uart->rx_bytes_count = 0;
                    uart->rx_offset ^= HW_UART__FIFO_SIZE / 2;
                UART_CRITICAL_SECTION_OFF(uart_id)
            }
            uart->save_bytes = uart->rx_isr_cb(uart_id, event, cur_buff, bytes_cnt);
            UART_CRITICAL_SECTION_ON(uart_id)
            if (((uart->save_bytes != 0) || (uart->rx_bytes_count)) && (uart->rx_event == UART__EVENT_CONTINUE))
                uart->rx_event = UART__EVENT_OK;
            UART_CRITICAL_SECTION_OFF(uart_id)
            uart->offset_bytes += bytes_cnt - uart->save_bytes;
        }
    }
}

/*******************************************************************************
 * ���������� �������-���������� ���������� �� ������ � �������.
 ******************************************************************************/
void hw_uart__USART_IRQHandler(int uart_id) {
    USART_TypeDef* UART = hw_uart_descr[HW_UART_ID_FROM_MCU_ID].uart_cmsis;
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    hw_uart__isr_u status_reg;
    status_reg.isr_u32 = UART->ISR;

    //��������� �� ������� ������
    if (status_reg.isr.PE | status_reg.isr.FE | status_reg.isr.NE | status_reg.isr.ORE) {
        UART->ICR = 0x1FFFFF;
        if (UART->CR1 & USART_CR1_RE) {
            UART->CR1 &= ~(USART_CR1_RE | USART_CR1_PEIE | USART_CR1_RXNEIE_RXFNEIE);
            UART->CR3 &= ~ USART_CR3_EIE;    //��������� ���������� �� FRAGMING/OVERFLOW/NOISE
//            uart->rx_bytes_count = uart->save_bytes = 0;
            uart->rx_event = UART__EVENT_ERROR;
        }
    }
    //RECEIVE
    if ((UART->CR1 & USART_CR1_RXNEIE_RXFNEIE) && (status_reg.isr.RXNE)) {
        //��� ��� ���� ��������� ��� ������ ��� ����������, ��� ���������� ��������� � �����
        volatile int buffer = UART->RDR;
        u8 temp[2], buff_len = 1;
        temp[0] = buffer;
        temp[1] = buffer >> 8;

        if (uart->parity == 1)
            temp[0] &= 0x7F;
        else if (uart->wordlenth9 == 1) {
            buff_len = 2;
            temp[1] &= 0x01;
        }
        //���� ��� �� ��������� -> ���������� ����� �� ���� ������ overrun
        UART->RQR |= USART_RQR_RXFRQ;
        if ((uart->rx_bytes_count + buff_len) < (HW_UART__FIFO_SIZE / 2)) {
#ifdef _DEBUG_
            if (uart->rx_bytes_count < 0)
                uart->rx_bytes_count = 0;
#endif
            memcpy(uart->rx_buff + uart->rx_offset + uart->rx_bytes_count, &temp, buff_len);
            uart->rx_bytes_count += buff_len;
            uart->rx_event = UART__EVENT_OK;
        }
        else {
            UART->CR1 &= ~(USART_CR1_RE | USART_CR1_PEIE | USART_CR1_RXNEIE_RXFNEIE);
            UART->CR3 &= ~ USART_CR3_EIE;    //��������� ���������� �� FRAGMING/OVERFLOW/NOISE
            uart->rx_event = UART__EVENT_FIFO_OVERFLOW;
        }
    }
    if (READ_BIT(UART->CR1, USART_CR1_TCIE) && (status_reg.isr.TC)) {
        sw_timer__stop(&uart->cts_timer);
        uart->tx_bytes_count++;
        int temp = (uint16_t) uart->tx_buff[uart->tx_bytes_count];
        if (uart->wordlenth9 == 1) {
            temp = ((uint16_t) uart->tx_buff[uart->tx_bytes_count] & 0x01FF);
            if (uart->parity == 1) {
                ;
            }
        }
        if (uart->tx_bytes_count < uart->tx_len)
            UART->TDR = temp;
        else {
            //������� ���������  ����
            //��������� ���������� ��������� �������� � ���� ������
            UART->CR1 &= ~ USART_CR1_TCIE;
            uart->tx_event = UART__EVENT_OK;
            if ((uart->settings & UART__FC_MASK) == UART__FC_485) {
                UART->ICR = 0x1FFFFF;
                (volatile void*)UART->RDR; (volatile void*)UART->RDR;
                SET_BIT(UART->CR1, USART_CR1_RE);
                uart__cmd_handler(HW_UART_ID_FROM_MCU_ID, UART__CMD_FC_DIR_RX);
            }
        }
    }
}

/*******************************************************************************
 * ������� ������������� GPIO ���������� � UART.
 ******************************************************************************/
static void hw_uart__itf_en(int uart_id) {
    // �������� STM32 UART �������������
    // �������� ������������ ��� ��������� ����� � �����
    for (int x = 0; x < 2; x++)
        gpio__af_init(hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[x], 0, hw_uart_descr[HW_UART_ID_FROM_MCU_ID].af_nb);
    *hw_uart_descr[HW_UART_ID_FROM_MCU_ID].uart_clock |= hw_uart_descr[HW_UART_ID_FROM_MCU_ID].uart_clock_flag;
}

/*******************************************************************************
 * ������� ��������������� GPIO ���������� � UART.
 ******************************************************************************/
static void hw_uart__itf_dis(int uart_id) {
    *hw_uart_descr[HW_UART_ID_FROM_MCU_ID].uart_clock &= ~hw_uart_descr[HW_UART_ID_FROM_MCU_ID].uart_clock_flag;
    for (int x = 0; x < 4; x++)
    {
        if (hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[x] != -1)
            gpio__init(hw_uart_descr[HW_UART_ID_FROM_MCU_ID].gpio[x], GPIO__DIR_IN, NULL);
    }
}

static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    int uart_id  = (int)ext_data;
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    uart->hw_event_handler(uart_id, UART__EVENT_OPEN, NULL);
}

static void cts_timeout_cb(struct sw_timer__t *timer, void *ext_data) {
    int uart_id  = (int)ext_data;
    hw_uart__itf_t* uart = &hw_uart[HW_UART_ID_FROM_MCU_ID];
    uart->tx_isr_cb(uart_id, UART__EVENT_BUSY, NULL, 0);
}