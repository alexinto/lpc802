/***************************************************************************//**
 * @file pulse_cnt.c
 * @brief ���� � ��������� �������� ���������.
 * @author a.tushentsov
 ******************************************************************************/

#include "gpio.h"
#include "pulse_cnt.h"


typedef struct {
    LPTIM_TypeDef* timer;
    u32 timer_en, pin;
    IRQn_Type timer_isr;
    u64 value;
    pulse_cnt__mode__e mode;
}pulse_cnt_const_data__t;

static pulse_cnt_const_data__t pulse_cnt_const_data[PULSE_CNT__DESCRIPTOR_COUNT] = {
#ifdef PULSE_CNT_1_PIN
    [0].timer = LPTIM1,  [0].timer_en = RCC_APB1ENR1_LPTIM1EN, [0].pin = PULSE_CNT_1_PIN, [0].timer_isr = LPTIM1_IRQn,
#endif
#ifdef PULSE_CNT_2_PIN
    [1].timer = LPTIM1, [1].timer_en = RCC_APB1ENR1_LPTIM1EN, [1].pin = PULSE_CNT_2_PIN, [1].timer_isr = LPTIM1_IRQn,
#endif
};

static void pulse_cnt__sw_isr(int gpio, gpio__state_e pin_state);

/*******************************************************************************
 * ������� ������� �������� ���������.
 ******************************************************************************/
pulse_cnt__e pulse_cnt__start(u8 counter_id, pulse_cnt__mode__e mode) {
    pulse_cnt_const_data__t* count_data = (pulse_cnt_const_data__t*)&pulse_cnt_const_data[counter_id];
    count_data->value = 0;
    if ((count_data->mode = mode) == COUNT_HW) {
        RCC->APB1ENR1 |= count_data->timer_en;  //�������� ������������
        gpio__af_init(count_data->pin, 0, 1);
        count_data->timer->CFGR = LPTIM_CFGR_CKSEL;//������� ������� �����
        count_data->timer->IER = LPTIM_IER_ARRMIE;  //�������� ���������� �� capture input
        count_data->timer->ICR = 0x3F;
        count_data->timer->CR = LPTIM_CR_ENABLE; //�������� ������
        count_data->timer->CR |= LPTIM_CR_CNTSTRT; //�������� ������
        count_data->timer->ARR = 65535;
        NVIC_SetPriority(count_data->timer_isr, 1);
        NVIC_EnableIRQ(count_data->timer_isr);
    }
    else
        gpio__init(count_data->pin, GPIO__DIR_IN | GPIO__INT_EDGE_RISING | GPIO__PULL_DOWN, pulse_cnt__sw_isr);
    return PULSE_CNT_OK;
}


u64 pulse_cnt__get(u8 counter_id) {
    pulse_cnt_const_data__t* count_data = (pulse_cnt_const_data__t*)&pulse_cnt_const_data[counter_id];
    u16 cnt_buff = 0;
    u64 res = pulse_cnt_const_data[counter_id].value;
    if (count_data->mode == COUNT_HW) {
        NVIC_DisableIRQ(pulse_cnt_const_data[counter_id].timer_isr);        // ��������� ����������� ������
        for(int x = 0; (cnt_buff != READ_REG(count_data->timer->CNT)) && (x < 100000); x++)
            cnt_buff = READ_REG(count_data->timer->CNT);
        res =  count_data->value + cnt_buff;
        if (count_data->timer->ISR & LPTIM_ISR_ARRM)
            res += 65536 - cnt_buff;
        NVIC_EnableIRQ(pulse_cnt_const_data[counter_id].timer_isr);
    }
    return res;
}


void pulse_cnt__stop(u8 counter_id) {
    pulse_cnt_const_data__t* count_data = (pulse_cnt_const_data__t*)&pulse_cnt_const_data[counter_id];
    if (count_data->mode == COUNT_HW) {
        NVIC_DisableIRQ(count_data->timer_isr);
        count_data->timer->CR &=~ LPTIM_CR_ENABLE; //��������� ������
        RCC->APB1ENR1 &=~ count_data->timer_en;  //��������� ������������
    }
    gpio__init(count_data->pin, GPIO__DIR_IN, NULL);
}

void pulse_cnt_ISR(uint8_t counter_id) {
    pulse_cnt_const_data[counter_id].timer->ICR = 0x3F;
    /* Get the Input Capture value */
    pulse_cnt_const_data[counter_id].value += 65536;
}

static void pulse_cnt__sw_isr(int gpio, gpio__state_e pin_state) {
    u8 counter_id = 0;
    while((pulse_cnt_const_data[counter_id].pin != gpio) && (counter_id < PULSE_CNT__DESCRIPTOR_COUNT)) counter_id++;
    if (counter_id < PULSE_CNT__DESCRIPTOR_COUNT)
        pulse_cnt_const_data[counter_id].value++;
}
