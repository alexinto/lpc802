/**
******************************************************************************
* @file    usbd_cdc.c
* @author  MCD Application Team
* @brief   This file provides the high layer firmware functions to manage the
*          following functionalities of the USB CDC Class:
*           - Initialization and Configuration of high and low layer
*           - Enumeration as CDC Device (and enumeration for each implemented memory interface)
*           - OUT/IN data transfer
*           - Command IN transfer (class requests management)
*           - Error management
*
*  @verbatim
*
*          ===================================================================
*                                CDC Class Driver Description
*          ===================================================================
*           This driver manages the "Universal Serial Bus Class Definitions for Communications Devices
*           Revision 1.2 November 16, 2007" and the sub-protocol specification of "Universal Serial Bus
*           Communications Class Subclass Specification for PSTN Devices Revision 1.2 February 9, 2007"
*           This driver implements the following aspects of the specification:
*             - Device descriptor management
*             - Configuration descriptor management
*             - Enumeration as CDC device with 2 data endpoints (IN and OUT) and 1 command endpoint (IN)
*             - Requests management (as described in section 6.2 in specification)
*             - Abstract Control Model compliant
*             - Union Functional collection (using 1 IN endpoint for control)
*             - Data interface class
*
*           These aspects may be enriched or modified for a specific user application.
*
*            This driver doesn't implement the following aspects of the specification
*            (but it is possible to manage these features with some modifications on this driver):
*             - Any class-specific aspect relative to communication classes should be managed by user application.
*             - All communication classes other than PSTN are not managed
*
*  @endverbatim
*
******************************************************************************
* @attention
*
* <h2><center>&copy; Copyright (c) 2015 STMicroelectronics.
* All rights reserved.</center></h2>
*
* This software component is licensed by ST under Ultimate Liberty license
* SLA0044, the "License"; You may not use this file except in compliance with
* the License. You may obtain a copy of the License at:
*                      www.st.com/SLA0044
*
******************************************************************************
*/

/* BSPDependencies
- "stm32xxxxx_{eval}{discovery}{nucleo_144}.c"
- "stm32xxxxx_{eval}{discovery}_io.c"
EndBSPDependencies */

/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc.h"
#include "usbd_ctlreq.h"
#include "hw_uart_usb.h"

extern void hw_uart_usb_tx_cb(uint8_t port);

static uint8_t  USBD_CDC_Init(USBD_HandleTypeDef *pdev,
                              uint8_t cfgidx);

static uint8_t  USBD_CDC_DeInit(USBD_HandleTypeDef *pdev,
                                uint8_t cfgidx);

static uint8_t  USBD_CDC_Setup(USBD_HandleTypeDef *pdev,
                               USBD_SetupReqTypedef *req);

static uint8_t  USBD_CDC_DataIn(USBD_HandleTypeDef *pdev,
                                uint8_t epnum);

static uint8_t  USBD_CDC_DataOut(USBD_HandleTypeDef *pdev,
                                 uint8_t epnum);

static uint8_t  USBD_CDC_EP0_RxReady(USBD_HandleTypeDef *pdev);

static uint8_t  *USBD_CDC_GetFSCfgDesc(uint16_t *length);


uint8_t  *USBD_CDC_GetDeviceQualifierDescriptor(uint16_t *length);

/* USB Standard Device Descriptor */
__ALIGN_BEGIN static uint8_t USBD_CDC_DeviceQualifierDesc[USB_LEN_DEV_QUALIFIER_DESC] __ALIGN_END =
{
    USB_LEN_DEV_QUALIFIER_DESC,
    USB_DESC_TYPE_DEVICE_QUALIFIER,
    0x00,
    0x02,
    0x00,
    0x00,
    0x00,
    0x40,
    0x01,
    0x00,
};

/** @defgroup USBD_CDC_Private_Variables
* @{
*/

/* CDC interface class callbacks structure */
USBD_ClassTypeDef  USBD_CDC =
{
    USBD_CDC_Init,
    USBD_CDC_DeInit,
    USBD_CDC_Setup,
    NULL,                 /* EP0_TxSent, */
    USBD_CDC_EP0_RxReady,
    USBD_CDC_DataIn,
    USBD_CDC_DataOut,
    NULL,
    NULL,
    NULL,
    USBD_CDC_GetFSCfgDesc,
    USBD_CDC_GetFSCfgDesc,
    USBD_CDC_GetFSCfgDesc,
    USBD_CDC_GetDeviceQualifierDescriptor,
};

/* USB CDC device Configuration Descriptor */__ALIGN_BEGIN uint8_t USBD_CDC_CfgFSDesc[USB_CDC_CONFIG_DESC_SIZ] __ALIGN_END ={
    /*Configuration Descriptor*/
    0x09, /* bLength: Configuration Descriptor size */
    USB_DESC_TYPE_CONFIGURATION, /* bDescriptorType: Configuration */
    LOBYTE(USB_CDC_CONFIG_DESC_SIZ), /* wTotalLength:no of returned bytes */
    HIBYTE(USB_CDC_CONFIG_DESC_SIZ),
    USB_UART_COUNT << 1, /* bNumInterfaces: ���������� interface�� */
    0x01, /* bConfigurationValue: Configuration value */
    0x00, /* iConfiguration: Index of string descriptor describing the configuration */
    0xC0, /* bmAttributes: self powered */
    0x32, /* MaxPower 100 mA */
    /*---------------------------------------------------------------------------*/
#ifdef USB_UART_1
    // IAD0
    0x08, // bLength: Interface Descriptor size
    0x0B, // bDescriptorType: IAD
    0x00, // bFirstInterface
    0x02, // bInterfaceCount
    0x02, // bFunctionClass: CDC
    0x02, // bFunctionSubClass
    0x01, // bFunctionProtocol
    0x02, // iFunction
    /*Cmd interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x00, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x00, /* bNumEndpoints: Null endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    /*---------------------------------------------------------------------------*/
    /*Data class interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x01, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x02, /* bNumEndpoints: Two endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_OUT_EP, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
    /*Endpoint IN Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_IN_EP, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
    /*---------------------------------------------------------------------------*/
#endif
#ifdef USB_UART_2
    // IAD1
    0x08, // bLength: Interface Descriptor size
    0x0B, // bDescriptorType: IAD
    0x02, // bFirstInterface
    0x02, // bInterfaceCount
    0x02, // bFunctionClass: CDC
    0x02, // bFunctionSubClass
    0x01, // bFunctionProtocol
    0x02, // iFunction
    /*Cmd interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x02, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x00, /* bNumEndpoints: Null endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    /*---------------------------------------------------------------------------*/
    /*Data class interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x03, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x02, /* bNumEndpoints: Two endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_OUT_EP2, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
    /*Endpoint IN Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_IN_EP2, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
#endif
#ifdef USB_UART_3
    //141
    // IAD2
    0x08, // bLength: Interface Descriptor size
    0x0B, // bDescriptorType: IAD
    0x04, // bFirstInterface
    0x02, // bInterfaceCount
    0x02, // bFunctionClass: CDC
    0x02, // bFunctionSubClass
    0x01, // bFunctionProtocol
    0x02, // iFunction
    /*Cmd interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x04, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x00, /* bNumEndpoints: Null endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    /*---------------------------------------------------------------------------*/
    /*Data class interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x05, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x02, /* bNumEndpoints: Two endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_OUT_EP3, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
    /*Endpoint IN Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_IN_EP3, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
#endif
#ifdef USB_UART_4
    //141
    // IAD3
    0x08, // bLength: Interface Descriptor size
    0x0B, // bDescriptorType: IAD
    0x06, // bFirstInterface
    0x02, // bInterfaceCount
    0x02, // bFunctionClass: CDC
    0x02, // bFunctionSubClass
    0x01, // bFunctionProtocol
    0x02, // iFunction
    /*Cmd interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x06, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x00, /* bNumEndpoints: Null endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    /*---------------------------------------------------------------------------*/
    /*Data class interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x07, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x02, /* bNumEndpoints: Two endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_OUT_EP4, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
    /*Endpoint IN Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_IN_EP4, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
#endif
#ifdef USB_UART_5
    // IAD4
    0x08, // bLength: Interface Descriptor size
    0x0B, // bDescriptorType: IAD
    0x08, // bFirstInterface
    0x02, // bInterfaceCount
    0x02, // bFunctionClass: CDC
    0x02, // bFunctionSubClass
    0x01, // bFunctionProtocol
    0x02, // iFunction
    /*Cmd interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x08, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x00, /* bNumEndpoints: Null endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    /*---------------------------------------------------------------------------*/
    /*Data class interface descriptor*/
    0x09, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_INTERFACE, /* bDescriptorType: */
    0x09, /* bInterfaceNumber: Number of Interface */
    0x00, /* bAlternateSetting: Alternate setting */
    0x02, /* bNumEndpoints: Two endpoints used */
    0x02, /* bInterfaceClass: CDC */
    0x00, /* bInterfaceSubClass: */
    0x00, /* bInterfaceProtocol: */
    0x00, /* iInterface: */
    /*Endpoint OUT Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_OUT_EP5, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
    /*Endpoint IN Descriptor*/
    0x07, /* bLength: Endpoint Descriptor size */
    USB_DESC_TYPE_ENDPOINT, /* bDescriptorType: Endpoint */
    CDC_IN_EP5, /* bEndpointAddress */
    0x02, /* bmAttributes: Bulk */
    LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE), /* wMaxPacketSize: */
    HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
    0x00, /* bInterval: ignore for Bulk transfer */
#endif
};
/**
* @}
*/

/** @defgroup USBD_CDC_Private_Functions
* @{
*/

/**
* @brief  USBD_CDC_Init
*         Initialize the CDC interface
* @param  pdev: device instance
* @param  port: Configuration port
* @retval status
*/
static uint8_t  USBD_CDC_Init(USBD_HandleTypeDef *pdev, uint8_t port)
{
    uint8_t ret = 0U;
    USBD_CDC_HandleTypeDef   *hcdc;
    /* Init  physical Interface components */
    ((USBD_CDC_ItfTypeDef *)pdev->pUserData)->Init();
    /* Open EP IN, OUT*/
#ifdef USB_UART_1
    USBD_LL_OpenEP(pdev, CDC_IN_EP, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_IN_PACKET_SIZE);
    pdev->ep_in[CDC_IN_EP & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_OUT_EP, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_OUT_PACKET_SIZE);
    pdev->ep_out[CDC_OUT_EP & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_CMD_EP, USBD_EP_TYPE_INTR, CDC_CMD_PACKET_SIZE);
    pdev->ep_in[CDC_CMD_EP & 0xFU].is_used = 1U;
    hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[0];
    /* Init Xfer states */
    hcdc->TxState = 0U;
    hcdc->RxState = 0U;
    /* Prepare Out endpoint to receive next packet */
    USBD_LL_PrepareReceive(pdev, CDC_OUT_EP, hcdc->RxBuffer,
                           CDC_DATA_FS_OUT_PACKET_SIZE);
#endif
#ifdef USB_UART_2
    USBD_LL_OpenEP(pdev, CDC_IN_EP2, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_IN_PACKET_SIZE);
    pdev->ep_in[CDC_IN_EP2 & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_OUT_EP2, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_OUT_PACKET_SIZE);
    pdev->ep_out[CDC_OUT_EP2 & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_CMD_EP2, USBD_EP_TYPE_INTR, CDC_CMD_PACKET_SIZE);
    pdev->ep_in[CDC_CMD_EP2 & 0xFU].is_used = 1U;
    hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[1];
    /* Init Xfer states */
    hcdc->TxState = 0U;
    hcdc->RxState = 0U;
    USBD_LL_PrepareReceive(pdev, CDC_OUT_EP2, hcdc->RxBuffer,
                           CDC_DATA_FS_OUT_PACKET_SIZE);
#endif
#ifdef USB_UART_3
    USBD_LL_OpenEP(pdev, CDC_IN_EP3, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_IN_PACKET_SIZE);
    pdev->ep_in[CDC_IN_EP3 & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_OUT_EP3, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_OUT_PACKET_SIZE);
    pdev->ep_out[CDC_OUT_EP3 & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_CMD_EP3, USBD_EP_TYPE_INTR, CDC_CMD_PACKET_SIZE);
    pdev->ep_in[CDC_CMD_EP3 & 0xFU].is_used = 1U;
    hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[2];
    /* Init Xfer states */
    hcdc->TxState = 0U;
    hcdc->RxState = 0U;
    USBD_LL_PrepareReceive(pdev, CDC_OUT_EP3, hcdc->RxBuffer,
                           CDC_DATA_FS_OUT_PACKET_SIZE);
#endif
#ifdef USB_UART_4
    USBD_LL_OpenEP(pdev, CDC_IN_EP4, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_IN_PACKET_SIZE);
    pdev->ep_in[CDC_IN_EP4 & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_OUT_EP4, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_OUT_PACKET_SIZE);
    pdev->ep_out[CDC_OUT_EP4 & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_CMD_EP4, USBD_EP_TYPE_INTR, CDC_CMD_PACKET_SIZE);
    pdev->ep_in[CDC_CMD_EP4 & 0xFU].is_used = 1U;
    hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[3];
    /* Init Xfer states */
    hcdc->TxState = 0U;
    hcdc->RxState = 0U;
    USBD_LL_PrepareReceive(pdev, CDC_OUT_EP4, hcdc->RxBuffer,
                           CDC_DATA_FS_OUT_PACKET_SIZE);
#endif
#ifdef USB_UART_5
    USBD_LL_OpenEP(pdev, CDC_IN_EP5, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_IN_PACKET_SIZE);
    pdev->ep_in[CDC_IN_EP5 & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_OUT_EP5, USBD_EP_TYPE_BULK,
                   CDC_DATA_FS_OUT_PACKET_SIZE);
    pdev->ep_out[CDC_OUT_EP5 & 0xFU].is_used = 1U;
    USBD_LL_OpenEP(pdev, CDC_CMD_EP5, USBD_EP_TYPE_INTR, CDC_CMD_PACKET_SIZE);
    pdev->ep_in[CDC_CMD_EP5 & 0xFU].is_used = 1U;
    hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[4];
    /* Init Xfer states */
    hcdc->TxState = 0U;
    hcdc->RxState = 0U;
    USBD_LL_PrepareReceive(pdev, CDC_OUT_EP5, hcdc->RxBuffer,
                           CDC_DATA_FS_OUT_PACKET_SIZE);
#endif

    return ret;
}

/**
* @brief  USBD_CDC_Init
*         DeInitialize the CDC layer
* @param  pdev: device instance
* @param  cfgidx: Configuration index
* @retval status
*/
static uint8_t  USBD_CDC_DeInit(USBD_HandleTypeDef *pdev, uint8_t cfgidx)
{
    uint8_t ret = 0U;
#ifdef USB_UART_1
    USBD_LL_CloseEP(pdev, CDC_IN_EP);
    pdev->ep_in[CDC_IN_EP & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_OUT_EP);
    pdev->ep_out[CDC_OUT_EP & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_CMD_EP);
    pdev->ep_in[CDC_CMD_EP & 0xFU].is_used = 0U;
#endif
#ifdef USB_UART_2
    USBD_LL_CloseEP(pdev, CDC_IN_EP2);
    pdev->ep_in[CDC_IN_EP2 & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_OUT_EP2);
    pdev->ep_out[CDC_OUT_EP2 & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_CMD_EP2);
    pdev->ep_in[CDC_CMD_EP2 & 0xFU].is_used = 0U;
#endif
#ifdef USB_UART_3
    USBD_LL_CloseEP(pdev, CDC_IN_EP3);
    pdev->ep_in[CDC_IN_EP3 & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_OUT_EP3);
    pdev->ep_out[CDC_OUT_EP3 & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_CMD_EP3);
    pdev->ep_in[CDC_CMD_EP3 & 0xFU].is_used = 0U;
#endif
#ifdef USB_UART_4
    USBD_LL_CloseEP(pdev, CDC_IN_EP4);
    pdev->ep_in[CDC_IN_EP4 & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_OUT_EP4);
    pdev->ep_out[CDC_OUT_EP4 & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_CMD_EP4);
    pdev->ep_in[CDC_CMD_EP4 & 0xFU].is_used = 0U;
#endif
#ifdef USB_UART_5
    USBD_LL_CloseEP(pdev, CDC_IN_EP5);
    pdev->ep_in[CDC_IN_EP5 & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_OUT_EP5);
    pdev->ep_out[CDC_OUT_EP5 & 0xFU].is_used = 0U;
    USBD_LL_CloseEP(pdev, CDC_CMD_EP5);
    pdev->ep_in[CDC_CMD_EP5 & 0xFU].is_used = 0U;
#endif
    /* DeInit  physical Interface components */
    ((USBD_CDC_ItfTypeDef *)pdev->pUserData)->DeInit();

    return ret;
}

/**
* @brief  USBD_CDC_Setup
*         Handle the CDC specific requests
* @param  pdev: instance
* @param  req: usb requests
* @retval status
*/
static uint8_t  USBD_CDC_Setup(USBD_HandleTypeDef *pdev,
                               USBD_SetupReqTypedef *req)
{
    USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[(req->wIndex) >> 1];
    uint8_t ifalt = 0U;
    uint16_t status_info = 0U;
    uint8_t ret = USBD_OK;

    switch (req->bmRequest & USB_REQ_TYPE_MASK)
    {
        case USB_REQ_TYPE_CLASS :
        if (req->wLength)
        {
            if (req->bmRequest & 0x80U)
            {
                ((USBD_CDC_ItfTypeDef *)pdev->pUserData)->Control(req->bRequest,
                                                                  (uint8_t *)(void *)hcdc->data,
                                                                  req->wLength);

                  USBD_CtlSendData(pdev, (uint8_t *)(void *)hcdc->data, req->wLength);
            }
            else
            {
                hcdc->CmdOpCode = req->bRequest;
                hcdc->CmdLength = (uint8_t)req->wLength;

                USBD_CtlPrepareRx(pdev, (uint8_t *)(void *)hcdc->data, req->wLength);
            }
        }
        else
        {
            ((USBD_CDC_ItfTypeDef *)pdev->pUserData)->Control(req->bRequest,
                                                              (uint8_t *)(void *)req, 0U);
        }
        break;

        case USB_REQ_TYPE_STANDARD:
        switch (req->bRequest)
        {
            case USB_REQ_GET_STATUS:
            if (pdev->dev_state == USBD_STATE_CONFIGURED)
            {
                USBD_CtlSendData(pdev, (uint8_t *)(void *)&status_info, 2U);
            }
            else
            {
                USBD_CtlError(pdev, req);
                ret = USBD_FAIL;
            }
            break;

            case USB_REQ_GET_INTERFACE:
            if (pdev->dev_state == USBD_STATE_CONFIGURED)
            {
                USBD_CtlSendData(pdev, &ifalt, 1U);
            }
            else
            {
                USBD_CtlError(pdev, req);
                ret = USBD_FAIL;
            }
            break;

            case USB_REQ_SET_INTERFACE:
            if (pdev->dev_state != USBD_STATE_CONFIGURED)
            {
                USBD_CtlError(pdev, req);
                ret = USBD_FAIL;
            }
            break;

            default:
            USBD_CtlError(pdev, req);
            ret = USBD_FAIL;
            break;
        }
        break;

        default:
        USBD_CtlError(pdev, req);
        ret = USBD_FAIL;
        break;
    }

    return ret;
}

/**
* @brief  USBD_CDC_DataIn
*         Data sent on non-control IN endpoint
* @param  pdev: device instance
* @param  epnum: endpoint number
* @retval status
*/

static uint8_t  USBD_CDC_DataIn(USBD_HandleTypeDef *pdev, uint8_t epnum)
{
    uint8_t port;
    if (epnum > 0)
        port = epnum - 1;

    USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef *)&pdev->pClassData[port];
    PCD_HandleTypeDef *hpcd = pdev->pData;

    if ((pdev->ep_in[epnum].total_length > 0U) && ((pdev->ep_in[epnum].total_length % hpcd->IN_ep[epnum].maxpacket) == 0U))
    {
        /* Update the packet total length */
        pdev->ep_in[epnum].total_length = 0U;
        /* Send ZLP */
        USBD_LL_Transmit(pdev, epnum, NULL, 0U);
    }
    else
    {
        hcdc->TxState = 0U;
        hw_uart_usb_tx_cb(port);
    }
    return USBD_OK;
}

/**
* @brief  USBD_CDC_DataOut
*         Data received on non-control Out endpoint
* @param  pdev: device instance
* @param  epnum: endpoint number
* @retval status
*/
static uint8_t  USBD_CDC_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum)
{
    uint8_t port;
    if (epnum > 0)
        port = epnum - 1;
    USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[port];
    /* Get the received data length */
    hcdc->RxLength = USBD_LL_GetRxDataSize(pdev, epnum);
    /* USB data will be immediately processed, this allow next USB traffic being
    NAKed till the end of the application Xfer */
    ((USBD_CDC_ItfTypeDef *)pdev->pUserData)->Receive(hcdc->RxBuffer, &hcdc->RxLength, port);
    return USBD_OK;
}

/**
* @brief  USBD_CDC_EP0_RxReady
*         Handle EP0 Rx Ready event
* @param  pdev: device instance
* @retval status
*/
static uint8_t  USBD_CDC_EP0_RxReady(USBD_HandleTypeDef *pdev)
{
    for (int x = 0; x < USB_UART_COUNT; x++) {
        USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[x];
        if (hcdc->CmdOpCode != 0xFFU)
            hcdc->CmdOpCode = 0xFFU;
    }
    return USBD_OK;
}

/**
* @brief  USBD_CDC_GetFSCfgDesc
*         Return configuration descriptor
* @param  speed : current device speed
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
static uint8_t  *USBD_CDC_GetFSCfgDesc(uint16_t *length)
{
    *length = sizeof(USBD_CDC_CfgFSDesc);
    return USBD_CDC_CfgFSDesc;
}

/**
* @brief  DeviceQualifierDescriptor
*         return Device Qualifier descriptor
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
uint8_t  *USBD_CDC_GetDeviceQualifierDescriptor(uint16_t *length)
{
    *length = sizeof(USBD_CDC_DeviceQualifierDesc);
    return USBD_CDC_DeviceQualifierDesc;
}

/**
* @brief  USBD_CDC_RegisterInterface
* @param  pdev: device instance
* @param  fops: CD  Interface callback
* @retval status
*/
uint8_t  USBD_CDC_RegisterInterface(USBD_HandleTypeDef   *pdev, USBD_CDC_ItfTypeDef *fops)
{
    uint8_t  ret = USBD_FAIL;
    if (fops != NULL) {
        pdev->pUserData = fops;
        ret = USBD_OK;
    }

    return ret;
}

/**
* @brief  USBD_CDC_SetTxBuffer
* @param  pdev: device instance
* @param  pbuff: Tx Buffer
* @retval status
*/
uint8_t  USBD_CDC_SetTxBuffer(USBD_HandleTypeDef   *pdev,
                              uint8_t  *pbuff,
                              uint16_t length, uint8_t port)
{
    USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[port];

    hcdc->TxBuffer = pbuff;
    hcdc->TxLength = length;

    return USBD_OK;
}


/**
* @brief  USBD_CDC_SetRxBuffer
* @param  pdev: device instance
* @param  pbuff: Rx Buffer
* @retval status
*/
uint8_t  USBD_CDC_SetRxBuffer(USBD_HandleTypeDef   *pdev,
                              uint8_t  *pbuff, uint8_t port)
{
    USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[port];

    hcdc->RxBuffer = pbuff;

    return USBD_OK;
}

/**
* @brief  USBD_CDC_TransmitPacket
*         Transmit packet on IN endpoint
* @param  pdev: device instance
* @retval status
*/
uint8_t  USBD_CDC_TransmitPacket(USBD_HandleTypeDef *pdev, uint8_t port)
{
    USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[port];
    uint8_t ep_in_port = port + 1;

    if ((hcdc->TxState == 0U) && (pdev->dev_state == USBD_STATE_CONFIGURED))
    {
        /* Tx Transfer in progress */
        hcdc->TxState = 1U;
        /* Update the packet total length */
        pdev->ep_in[ep_in_port & 0xFU].total_length = hcdc->TxLength;
        /* Transmit next packet */
        USBD_LL_Transmit(pdev, ep_in_port, hcdc->TxBuffer, (uint16_t)hcdc->TxLength);
        return USBD_OK;
    }
    else
        return USBD_BUSY;
}


/**
* @brief  USBD_CDC_ReceivePacket
*         prepare OUT Endpoint for reception
* @param  pdev: device instance
* @retval status
*/
uint8_t  USBD_CDC_ReceivePacket(USBD_HandleTypeDef *pdev, uint8_t port)
{
    USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef *) &pdev->pClassData[port];
    /* Suspend or Resume USB Out process */
    /* Prepare Out endpoint to receive next packet */
    USBD_LL_PrepareReceive(pdev,
                           (port + 1),
                           hcdc->RxBuffer,
                           CDC_DATA_FS_OUT_PACKET_SIZE);
    return USBD_OK;
}
/**
* @}
*/

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
