/***************************************************************************//**
 * @file hw_rtc.h.
 * @brief ������ ������ � ������ ��������� ������� RTC (������������������ �����).
 *        ������ �������� ������ � UTC ��������.
 *        ���������� ��� ���������� STM32L4R7.
 * @authors a.tushentsov.
 ******************************************************************************/
#include <time.h>
#include "standard.h"
#include "target.h"
#include "hw_rtc.h"
#include "rtc.h"
#include "sw_timer.h"
#include "i2c.h"

// ��������� ������ hw_rtc.
typedef struct {
    hw_rtc__event_handler_t event_handler;
    sw_timer__sys_time_t time;
} hw_rtc__struct_t;

const uint8_t commands = 0x02;  // 0x02- �������, 0x03- ������ � �.�.

hw_rtc__struct_t hw_rtc ;

/*******************************************************************************
 *  ������������� ������ ������ � RTC.
 ******************************************************************************/
rtc__event_e hw_rtc__init(hw_rtc__event_handler_t event_handler) {
    hw_rtc.event_handler = event_handler;
    hw_rtc__utc_get();
    return RTC__EVENT_OK;
}

/*******************************************************************************
 *  ������� ��������� �������� UTC ������� (timestamp).
 ******************************************************************************/
rtc__event_e hw_rtc__utc_get() {
    uint32_t timestamp = RTC->TR, datestamp = RTC->DR;
    struct tm buff_time = {.tm_sec = (((timestamp & 0x7F)  >> 4) * 10) + (timestamp & 0x0f),
    .tm_min = (((timestamp & 0x7F00)  >> 12) * 10) + ((timestamp & 0x0f00) >> 8),
    .tm_hour = (((timestamp & 0x3F0000)  >> 20) * 10) + ((timestamp & 0x0f0000) >> 16),
    .tm_mday = (((datestamp & 0x3F)  >> 4) * 10) + (datestamp & 0x0f),
    .tm_mon = (((datestamp & 0x1F00) >> 12) * 10) + ((datestamp & 0x0f00) >> 8),
    .tm_year = (((datestamp & 0xFF0000) >> 20) * 10) + ((datestamp & 0x0f0000) >> 16) };
    hw_rtc.event_handler(RTC__EVENT_OK, rtc__mktime(&buff_time));
    return RTC__EVENT_OK;
}

/*******************************************************************************
 *  ������� ��������� UTC ������� (timestamp).
 ******************************************************************************/
rtc__event_e hw_rtc__utc_set(int32_t utc) {    //todo ������ ��������� � hw_timer.c
    struct tm buff_time;
    int err_cnt = 0;
    uint32_t timestamp, datestamp;
    rtc__gmtime(utc, &buff_time);
    timestamp = ((buff_time.tm_sec / 10) << 4) | (buff_time.tm_sec % 10);
    timestamp |= ((buff_time.tm_min / 10) << 12) | ((buff_time.tm_min % 10) << 8);
    timestamp |= ((buff_time.tm_hour / 10) << 20) | ((buff_time.tm_hour % 10) << 16);
    datestamp = ((buff_time.tm_mday / 10) << 4) | (buff_time.tm_mday % 10);
    datestamp |= ((buff_time.tm_mon / 10) << 12) | ((buff_time.tm_mon % 10) << 8);
    datestamp |= ((buff_time.tm_year / 10) << 20) | ((buff_time.tm_year % 10) << 16);
    RTC->ISR |= RTC_ISR_INIT;
    while((err_cnt++ < 100000) && ((RTC->ISR & RTC_ISR_INIT) != RTC_ISR_INIT)){};
    RTC->TR = timestamp;
    RTC->DR = datestamp;
    RTC->ISR &= ~RTC_ISR_INIT;
    hw_rtc.event_handler(err_cnt < 100000 ? RTC__EVENT_OK : RTC__EVENT_ERR, hw_rtc.time.timestamp);
    return RTC__EVENT_OK;
}