/***************************************************************************//**
 * @file hw_bootloader.c.
 * @brief ������ ����������.
 *        �������� ���������-��������� ������� ������ � ���������� ������� ����������������
 *        � � ������� SPI ����- �������. ������� ������ � ���������� ������� �� ������������� � ���.
 * @author a.tushentsov.
 ******************************************************************************/
#include "hw_bootloader.h"
#include "target.h"

typedef struct {
    int spi_cs;
    int leds[4];
    GPIO_TypeDef* led_ports[4];
    SPI_TypeDef* spi_reg;
    GPIO_TypeDef* spi_cs_port;
}hw_bootloader__struct_t;

static hw_bootloader__struct_t boot_data = {.spi_reg = BOOTLOADER_SPI, .spi_cs = BOOTLOADER_SPI_CS};

__ramfunc static void spi__txrx(uint8_t *rx_buff, uint8_t *tx_buff, uint32_t len);
static void spi__itf_en();
static void spi__itf_dis();
static void hw_bootloader__gpio_init(void);

__ramfunc static ax_event__e flash_ext_ram__read(uint32_t addr, uint8_t *buff, uint32_t len);
__ramfunc static ax_event__e flash_int_ram__execute(flash__op_e oper, uint32_t addr, uint8_t *buff, uint32_t len);
__ramfunc static void hw_bootloader__leds(void);

/*******************************************************************************
 * ������� ������.
 ******************************************************************************/
__ramfunc ax_event__e flash__read(int flash_id, uint32_t addr, uint8_t *buff, uint32_t len, flash__event_cb read_cb, void *ext_data) {
    ax_event__e res = AX_EVENT__PARAMS_NA;
    hw_bootloader__leds();
    if (flash_id == FLASH__ID_INT)
        res = flash_int_ram__execute(FLASH__OP_READ, addr, buff, len);
    else
        res = flash_ext_ram__read(addr, buff, len);
    return res;
}
/*******************************************************************************
 * ������� ������.
 ******************************************************************************/
__ramfunc ax_event__e flash__write(int flash_id, uint32_t addr, uint8_t *buff, uint32_t len, flash__event_cb write_cb, void *ext_data) {
    return flash_int_ram__execute(FLASH__OP_WRITE, addr, buff, len);
}

/*******************************************************************************
 * ������� �������� ��������.
 ******************************************************************************/
__ramfunc ax_event__e flash__page_erase(int flash_id, uint32_t addr, flash__event_cb erase_cb, void *ext_data) {
       return flash_int_ram__execute(FLASH__OP_ERASE_PAGE, addr, NULL, NULL);
}



#define FLASH_PEKEY1               ((uint32_t)0x45670123U) /*!< Flash program erase key1 */
#define FLASH_PEKEY2               ((uint32_t)0xCDEF89ABU)  /*!< The following values must be written consecutively to unlock the FLASH_CR
                                                                 register allowing flash programming/erasing operations */

#define FLASH_OPTKEY1              ((uint32_t)0x08192A3BU) /*!< Flash option key1 */
#define FLASH_OPTKEY2              ((uint32_t)0x4C5D6E7FU) /*!< The following values must be written consecutively to unlock the FLASH_OPTR
                                                                register allowing option byte programming/erasing operations. */

/*******************************************************************************
 * ������� ��������, ������ , ������ ���������� ������ ��.
 ******************************************************************************/
__ramfunc static ax_event__e flash_int_ram__execute(flash__op_e oper, uint32_t addr, uint8_t *buff, uint32_t len) {
    uint32_t page;
    if (addr < MCU__FLASH_INT_MAIN_MEM_BEGIN || addr >= MCU__FLASH_INT_MAIN_MEM_END)
        return AX_EVENT__PARAMS_NA;
    if (((addr - MCU__FLASH_INT_MAIN_MEM_BEGIN) % 8) || (len % 4))
        return AX_EVENT__PARAMS_NA;
    if (FLASH->SR & FLASH_SR_BSY)
        return AX_EVENT__BUSY;
    if ((READ_BIT(FLASH->CR, FLASH_CR_LOCK)) || (READ_BIT(FLASH->CR, FLASH_CR_OPTLOCK))) {
        WRITE_REG(FLASH->KEYR, FLASH_PEKEY1);
        WRITE_REG(FLASH->KEYR, FLASH_PEKEY2);
        WRITE_REG(FLASH->OPTKEYR, FLASH_OPTKEY1);
        WRITE_REG(FLASH->OPTKEYR, FLASH_OPTKEY2);
    }
    switch (oper) {
        case FLASH__OP_READ:
            for (uint32_t i = 0; i < (len / 4); i++)
                *((uint32_t*)buff + i) = *((uint32_t*)addr + i);             // ������ ������.
            break;
        case FLASH__OP_WRITE:
            SET_BIT(FLASH->CR, FLASH_CR_PG);
            for (uint32_t i = 0; i < (len / 4); i++)
                *((uint32_t*)addr + i) = *((uint32_t*)buff + i);
            if (len % 8) {
                *((uint32_t*)addr + len / 4) = 0xFFFFFFFF;
            }
            for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 1e7); x++);
            break;
        case FLASH__OP_ERASE_PAGE:
            page = ((addr - MCU__FLASH_INT_MAIN_MEM_BEGIN)/FLASH_PAGE_SIZE); //���������� ������ ��������
            if(page < 256)
                CLEAR_BIT(FLASH->CR, FLASH_CR_BKER);
            else
                SET_BIT(FLASH->CR, FLASH_CR_BKER);
            SET_BIT(FLASH->CR, FLASH_CR_PER);
            MODIFY_REG(FLASH->CR, FLASH_CR_PNB, ((page & 0xFFU) << FLASH_CR_PNB_Pos));
            SET_BIT(FLASH->CR, FLASH_CR_STRT);
            for(int x = 0; (FLASH->SR & FLASH_SR_BSY) && (x < 1e7); x++);
            break;
        default:
            break;
    }
    FLASH->CR = FLASH_CR_LOCK | FLASH_CR_OPTLOCK;
    return AX_EVENT__OK;
}

/*******************************************************************************
 * ������� ������ ������� SPI flash ������.
 ******************************************************************************/
__ramfunc static ax_event__e flash_ext_ram__read(uint32_t addr, uint8_t *buff, uint32_t len) {
    u8 cmd[5];
    for(u8 x = 1; x < 4; x++)       // ���������� ������ � MSB first.
        cmd[x] = *((u8*)&addr + 3 - x);
    cmd[0] = 0x03;

    spi__txrx(NULL, cmd, 4);
    spi__txrx(buff, NULL, len);
    return AX_EVENT__OK;
}

typedef struct {
    int gpio[3];
    uint32_t* spi_clock, *spi_rst;
    int spi_clock_flag, spi_rst_flag;
    int spi_irq;
} hw_spi_desc__t;

static const hw_spi_desc__t hw_spi_desc = {
     HW_SPI_1_CLK, HW_SPI_1_MOSI, HW_SPI_1_MISO, (uint32_t*)&RCC->APB2ENR, (uint32_t*)&RCC->APB2RSTR, RCC_APB2ENR_SPI1EN, RCC_APB2RSTR_SPI1RST, SPI1_IRQn
};

static void spi__itf_en() {
    for(int spi_param_desc_cnt = 0; spi_param_desc_cnt < 3; spi_param_desc_cnt++)
        gpio__af_init(hw_spi_desc.gpio[spi_param_desc_cnt], 0, 0x5);
    gpio__init(boot_data.spi_cs, GPIO__DIR_OUT, NULL);
    gpio__set(boot_data.spi_cs, GPIO__STATE_HIGH);
    *hw_spi_desc.spi_clock |= hw_spi_desc.spi_clock_flag;
    NVIC_SetPriority((IRQn_Type)hw_spi_desc.spi_irq, 3);
    NVIC_EnableIRQ((IRQn_Type)hw_spi_desc.spi_irq);
}

static void spi__itf_dis() {
    NVIC_DisableIRQ((IRQn_Type)hw_spi_desc.spi_irq);
    *hw_spi_desc.spi_clock &= ~hw_spi_desc.spi_clock_flag;
    for(int spi_param_desc_cnt = 0; spi_param_desc_cnt < 3; spi_param_desc_cnt++)
        gpio__init(hw_spi_desc.gpio[spi_param_desc_cnt],  GPIO__DIR_IN, NULL);
    gpio__init(boot_data.spi_cs, GPIO__DIR_IN, NULL);
    gpio__set(boot_data.spi_cs, GPIO__STATE_LOW);
}

__ramfunc static void hw_bootloader__leds(void) {
    static u8 led_num;
    u8 cur_led = (led_num >> 2) & 0x03;
    WRITE_REG(boot_data.led_ports[cur_led]->BSRR, 0x10000<<(boot_data.leds[cur_led] & 0x0F));
    cur_led = (++led_num >> 2) & 0x03;
    WRITE_REG(boot_data.led_ports[cur_led]->BSRR, 0x1<<(boot_data.leds[cur_led] & 0x0F));
}


__ramfunc void hw_bootloader__reset(void) {
    MODIFY_REG(RCC->APB1ENR1, 0, RCC_APB1ENR1_PWREN);
    IWDG->KR = 0x0000CCCC;                // ��������� IWDG
    IWDG->KR = 0x00005555;   // ��������� ������ � PR � RLR ��������
    for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
    IWDG->PR = 3;
    IWDG->RLR = 10;
    IWDG->KR = 0x00000000;
    while(1);
}

__ramfunc static void spi__txrx(uint8_t *rx_buff, uint8_t *tx_buff, uint32_t len) {
    int pin_mask = 1<<(boot_data.spi_cs & 0x0F);
    u8 rx_data = boot_data.spi_reg->DR;
    if (tx_buff) {
        WRITE_REG(boot_data.spi_cs_port->BSRR, pin_mask); // gpio_set(HIGH)
        for(int i = 0; i < 100; i++); // delay CS
    }
    WRITE_REG(boot_data.spi_cs_port->BSRR, pin_mask << 16); // gpio_set(LOW)
    for(int bytes_cnt = 0, err_cnt = 0; (bytes_cnt < len) && (err_cnt < 100000); bytes_cnt++) {
        *(uint8_t*)&boot_data.spi_reg->DR = tx_buff ? tx_buff[bytes_cnt] : 0xFF;
        for(err_cnt = 0; (err_cnt < 100000) && ((boot_data.spi_reg->SR & SPI_SR_RXNE) != SPI_SR_RXNE); err_cnt++);
        boot_data.spi_reg->SR = 0x00;
        rx_data = boot_data.spi_reg->DR;
        if (rx_buff)
            rx_buff[bytes_cnt] = rx_data;
    }
}

void hw_bootloader__deinit() {
    boot_data.spi_reg->CR1 &= ~SPI_CR1_SPE;
    *hw_spi_desc.spi_rst |= hw_spi_desc.spi_rst_flag;
    spi__itf_dis();                                     // delay �� ����� ������ SPI
    *hw_spi_desc.spi_rst &= ~hw_spi_desc.spi_rst_flag;
}

static const u8 gpio__outs[] = {
    DEVICE__LED_GSM_R,      DEVICE__LED_GSM_G,      DEVICE__LED_GSM_B,
    DEVICE__LED_DATA_R,     DEVICE__LED_DATA_G,     DEVICE__LED_DATA_B,
    DEVICE__LED_MODE_R,     DEVICE__LED_MODE_G,     DEVICE__LED_MODE_B,
    DEVICE__LED_ALARM_R,    DEVICE__LED_ALARM_G,    DEVICE__LED_ALARM_B,
    DEVICE__DC1_3V3_EN,     DEVICE__DC2_3V3_EN,     DEVICE__DC3_3V3_EN,
    DEVICE__ITF3_DIR,
    DEVICE__GSM_RTS,        DEVICE__GSM_PWR_ON,     DEVICE__GSM_ON,         DEVICE__GSM_SIM_SEL,
    DEVICE__FLASH1_CS,      DEVICE__FLASH_PWR_ON,
    DEVICE__FLASH2_CS,
    DEVICE__DI1_EN,         DEVICE__DI2_EN,
#ifdef KAM25
    DEVICE__DC4_3V3_EN,
    DEVICE__ITF1_CTS,
    DEVICE__ITF2_CTS,
    DEVICE__ITF3_IRQ_EN,
    DEVICE__ITF4_DIR,
#else
    DEVICE__ITF1_DIR,       DEVICE__ITF1_MODE,      DEVICE__ITF1_IRQ_EN,
    DEVICE__ITF2_RTS_DIR,   DEVICE__ITF2_MODE,
    DEVICE__ITF3_MODE,
    DEVICE__GSM_DSCS_ON,
    DEVICE__BT_WF_RTS,      DEVICE__BT_WF_PWR_ON,   DEVICE__BT_WF_RESET, DEVICE__BT_WF_BOOT,
    DEVICE__BME_EN,
#endif
    0xFF};

extern const GPIO_TypeDef* const gpio__mass[];

void hw_bootloader__init() {
    hw_bootloader__gpio_init();
    int port = boot_data.spi_cs >> 4;
    boot_data.spi_cs_port = (GPIO_TypeDef*) gpio__mass[port];  // ��������� ������ ����� � ������
    hw_bootloader__deinit();
    spi__itf_en();
    boot_data.spi_reg->CR1 = SPI_CR1_MSTR | SPI_CR1_BR_1 /*| SPI_CR1_CPHA | SPI_CR1_CPOL*/ | SPI_CR1_SSM | SPI_CR1_SSI; // MASTER, baud rate = 1/8 * Fclk, SPI__CPHA_0_CPOL_0, MSB,
    boot_data.spi_reg->CR2 = SPI_CR2_NSSP | SPI_CR2_FRXTH; // DATA_FRAME_8_BIT
    boot_data.spi_reg->CR1 |= SPI_CR1_SPE;
    for(int i = 0; i < 4; i++) {
        boot_data.leds[i] = gpio__outs[2 + i * 3];
        boot_data.led_ports[i] = (GPIO_TypeDef*) gpio__mass[boot_data.leds[i] >> 4];
    }
    for(int x = 0; x < 1000000; x++);     // �������� �� ��� �������� (��������� FLASH � �.�.)
}

static void hw_bootloader__gpio_init(void) {
    for(int x = 0; gpio__outs[x] != 0xFF; x++) {
        gpio__init(gpio__outs[x], GPIO__DIR_OUT, NULL);
        gpio__set(gpio__outs[x], GPIO__STATE_LOW);
    }
#ifdef KAM25
    gpio__init(DEVICE__ITF3_IRQ, GPIO__DIR_IN | GPIO__PULL_DOWN | GPIO__INT_EDGE_RISING, NULL);
    gpio__init(DEVICE__PWR_DETECT, GPIO__DIR_IN, NULL);
#else
    gpio__set(DEVICE__BME_EN, GPIO__STATE_HIGH);
    gpio__set(DEVICE__GSM_DSCS_ON, GPIO__STATE_HIGH);
    gpio__init(DEVICE__ITF1_IRQ, GPIO__DIR_IN | GPIO__PULL_DOWN , NULL);
    gpio__init(DEVICE__BT_WF_CTS, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);
#endif
    gpio__init(DEVICE__GSM_CTS, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);
    gpio__set(DEVICE__FLASH1_CS, GPIO__STATE_HIGH);
    gpio__set(DEVICE__FLASH2_CS, GPIO__STATE_HIGH);
    gpio__init(DEVICE__USER_BTN_IRQ, GPIO__DIR_IN | GPIO__PULL_DOWN , NULL);  // ��������- ������ �� �����!!!
    gpio__init(DEVICE__ITF2_CTS, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);
    gpio__init(DEVICE__GSM_IRQ, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    gpio__init(DEVICE__GSM_SIM_DETECT_IRQ, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    gpio__init(DEVICE__GSM_LED_GSM, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    gpio__init(DEVICE__KAM_BUS_ADDR, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);   // todo ����?
    gpio__init(DEVICE__KAM_BUS_RESET, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);  // todo ����?
    gpio__init(DEVICE__DI1_IRQ, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    gpio__init(DEVICE__DI2_IRQ, GPIO__DIR_IN | GPIO__PULL_UP , NULL);
    gpio__init(DEVICE__USB_DM, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);  // todo ����?
    gpio__init(DEVICE__USB_DP, GPIO__DIR_IN | GPIO__PULL_DOWN, NULL);  // todo ����?
}