/***************************************************************************//**
 * @file supervisor.c.
 * @brief ������ ���������� �������� � �������� ���������������� �����������.
 * @author s.korotkov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "mcu.h"
#include "standard.h"
#include "hw_supervisor.h"
#include "supervisor.h"
#include "target.h"

#define SYS_CLOCK_LOW_SLEEP_0_HZ    4200000     // ��������� ������� ��� ������������ �� MSI (� Hz)
#define SYS_CLOCK_LOW_SLEEP_1_HZ    131000      // ��������� ������� ��� ��� (� Hz)
#define SYS_CLOCK_HIGH_SLEEP_0_HZ   48000000    // ��������� ������� ��� ������������ �� HSI (� Hz)

#define WDT__IWD_DELAY_MAX                      0xFFF
#define WDT__IWDG_KEY_RELOAD                    0xAAAA      /*!< IWDG Reload Counter Enable   */
#define WDT__IWDG_KEY_ENABLE                    0x0000CCCC  /*!< IWDG Peripheral Enable       */
#define WDT__IWDG_KEY_WRITE_ACCESS_ENABLE       0x00005555  /*!< IWDG KR Write Access Enable  */
#define WDT__IWDG_KEY_WRITE_ACCESS_DISABLE      0x00000000  /*!< IWDG KR Write Access Disable */

// ��������� ��������� ������.
typedef struct {
    supervisor__reset_reason_e reset_reason;                    // ������� ������������.
    supervisor__wdt_e cur_state;                                // ������� ��������� ������ wdt.
    uint8_t count_lock;                                         // ���������� ����������� �������, ������������� �� ����������� watchdog
    uint32_t cpu_clk;                                           // ������� ������� CPU.
    uint32_t periph_clk;                                        // ������� ������� ���������.
    u8 cpu_usage;
    u32 time_active, cpu_cnt;
} supervisor__struct_t;

static supervisor__struct_t supervisor;

typedef enum {
    LVL_2_0V = 0,
    LVL_2_2V,
    LVL_2_4V,
    LVL_2_5V,
    LVL_2_6V,
    LVL_2_8V,
    LVL_2_9V,
} supervisor__pvd_e;

typedef enum {
    LVL_1_8V,
    LVL_1_5V,
    LVL_1_2V,
} supervisor__pvr_e;

typedef enum {
    LVL_1_7 = 0,
    LVL_2_0,
    LVL_2_2,
    LVL_2_5,
    LVL_2_8,
} supervisor__bor_e;

/* static void hw_supervisor__set_voltage_regulator_range(supervisor__pvr_e range); */
#ifdef WE_NEED_PVD
static void hw_supervisor__set_pvd_threshold(supervisor__pvd_e threshold);
#endif
static void hw_supervisor__set_bor_lvl(supervisor__bor_e borlvl);
#ifdef CPU_USAGE
static void hw_supervisor__cpu_usage_init();
#endif

/*******************************************************************************
 * ������� ��������� ���������� ������� ����������� (PDV, BOR � ��.).
 ******************************************************************************/
void hw_supervisor__init(void) {
#ifdef _DEBUG_
    DBGMCU->CR = DBGMCU_CR_DBG_STOP;
    DBGMCU->APB1FZR1 = 0xFFFFFFFF;
    DBGMCU->APB2FZ = 0xFFFFFFFF;
    DBGMCU->APB1FZR2 |= 1 << 5;
#endif
    hw_supervisor__set_bor_lvl(LVL_2_8);
#ifdef WE_NEED_PVD
    hw_supervisor__set_pvd_threshold(LVL_2_9V);
#endif
#ifdef CPU_USAGE
    hw_supervisor__cpu_usage_init();
#endif
}

/*******************************************************************************
 * ������� ������������ ����������.
 ******************************************************************************/
void hw_supervisor__apl_reset(supervisor__reset_reason_e reset_type) {
    __disable_irq();
    RCC->CSR |= RCC_CSR_RMVF;
    switch(reset_type) {
        case SUPERVISOR__RESET_WDT:
                MODIFY_REG(RCC->APB1ENR1, 0, RCC_APB1ENR1_PWREN);
                IWDG->KR = WDT__IWDG_KEY_ENABLE;                // ��������� IWDG
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // ��������� ������ � PR � RLR ��������
                for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
                IWDG->PR = 3;
                IWDG->RLR = 10;
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;
            break;
        default:
            SCB->AIRCR = 0x05FA0000 | SCB_AIRCR_SYSRESETREQ_Msk;
            break;
    }
    while(1);
}

/*******************************************************************************
 * �������, ������������ ������� ������������ ����������.
 ******************************************************************************/
static const supervisor__reset_reason_e hw_reset_reasons[] = {SUPERVISOR__RESET_UNKNOWN, SUPERVISOR__RESET_FIREWALL, SUPERVISOR__RESET_OPT_BYTE, SUPERVISOR__RESET_PIN,
                                                              SUPERVISOR__RESET_BOR, SUPERVISOR__RESET_SW, SUPERVISOR__RESET_WDT, SUPERVISOR__RESET_WDT, SUPERVISOR__RESET_IN_SLEEP};

supervisor__reset_reason_e hw_supervisor__reset_reason_get(void) {
    uint8_t res, reason = RCC->CSR >> 24;
    if (reason) {
        RCC->CSR |= RCC_CSR_RMVF;
        for(res = 0;(reason >> res) && (res < 8); res++);
        supervisor.reset_reason = hw_reset_reasons[res];
    }
    return supervisor.reset_reason;
}

/*******************************************************************************
 * ������� ������ � WDT.
 ******************************************************************************/
supervisor__wdt_e hw_supervisor__wdt_cmd(supervisor__wdt_e cmd) {
    supervisor__wdt_e res = SUPERVISOR__WDT_ERROR;
    switch (cmd) {
        case SUPERVISOR__WDT_ON_1S:
        case SUPERVISOR__WDT_ON_2S:
        case SUPERVISOR__WDT_ON_3S:
        case SUPERVISOR__WDT_ON_4S:
            if (supervisor.count_lock > 0)
                supervisor.count_lock--;
            if (!supervisor.count_lock) {
                MODIFY_REG(RCC->APB1ENR1, 0, RCC_APB1ENR1_PWREN);
                IWDG->KR = WDT__IWDG_KEY_ENABLE;                // ��������� IWDG
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // ��������� ������ � PR � RLR ��������
                for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
                IWDG->PR = 4;
                IWDG->RLR = (cmd * 1000 + 500)/2; // ���������� watchdog ��������� ����� 500�� ����� ������������
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;
                IWDG->KR = WDT__IWDG_KEY_RELOAD;  // ������������� �������

                // ��������� ����������� watchdog
                RCC->APB1ENR1 |= RCC_APB1ENR1_TIM7EN;  //�������� ������������
                TIM7->PSC = (supervisor.cpu_clk / 5000) - 1; // ����� ������������ - 5���
                TIM7->ARR = (5000 * cmd) - 1; // ������
                TIM7->CNT = 0;
                TIM7->EGR |= TIM_EGR_UG; // ��������� ��������
                TIM7->CR1 |= TIM_CR1_CEN; // ��������� ������

                TIM7->SR &= ~TIM_SR_UIF; // ���������� ����
                TIM7->DIER |= TIM_DIER_UIE; // ��� ���������� ����������

                NVIC_SetPriority(TIM7_IRQn, 0);
                NVIC_EnableIRQ(TIM7_IRQn);

                supervisor.cur_state = cmd;
            }

        case SUPERVISOR__WDT_RESET:
            IWDG->KR = WDT__IWDG_KEY_RELOAD;  // ������������� �������
            TIM7->CNT = 0;
        case SUPERVISOR__WDT_GET_STATE:
            res = supervisor.cur_state;
            break;
        case SUPERVISOR__WDT_OFF:
            if (!supervisor.count_lock) {
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // ��������� ������ � PR � RLR ��������
                for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
                IWDG->PR = 7;
                IWDG->RLR = 0xFFF;
                IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;
                supervisor.cur_state = cmd;
            }
            if (supervisor.count_lock < 0xFF)
                supervisor.count_lock++;
//<<<<<<< Updated upstream
            IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_ENABLE;   // ��������� ������ � PR � RLR ��������
            for(int x = 0; x < 100000 && ((IWDG->SR & IWDG_SR_PVU) || (IWDG->SR & IWDG_SR_RVU)); x++);
            IWDG->PR = 7;
            IWDG->RLR = 0xFFF;
            IWDG->KR = WDT__IWDG_KEY_WRITE_ACCESS_DISABLE;

            TIM7->CR1 &= ~TIM_CR1_CEN; // ������������ � ���������� �������
            TIM7->CNT = 0;

            supervisor.cur_state = cmd;
//=======
//>>>>>>> Stashed changes
        default:
            // ��� ����� �������.
            break;
    }
    return res;
}

/*******************************************************************************
 * ������� �������� ����������� � SLEEP.
 * ���� �������� �������� ��� ������� �� ����� ������ �� ����������� ����������� �� ����������(���������� � hard fault).
 * ���������� � ��������� irq + ���������� NOP-�� ��������.
 ******************************************************************************/
void hw_supervisor__idle_on(void) {

    MODIFY_REG(RCC->APB1ENR1, 0, RCC_APB1ENR1_PWREN);     //��������� ������������ PWR �������, ��� ��� �� ����� � ����!
    supervisor.cpu_clk = 0;
    MODIFY_REG(SCB->SCR, 0, SCB_SCR_SLEEPDEEP_Msk ); // low-power mode = stop mode
    __disable_irq();
    __WFI();
    __enable_irq();
}

/*******************************************************************************
  ������� ������ ����������� �� SLEEP.
 ******************************************************************************/
void hw_supervisor__idle_off(void) {
    hw_supervisor__clk_set(SUPERVISOR__CLK_HIGH, 0);
}

/*******************************************************************************
 * ������� ��������� ������� ������� ������������ ����������.
 ******************************************************************************/
uint32_t hw_supervisor__cpu_clk_get(void) {
    return supervisor.cpu_clk;
}

/*******************************************************************************
 * ������� ��������� ������� ������� ������������ ���������.
 ******************************************************************************/
uint32_t hw_supervisor__periph_clk_get(void) {
    return supervisor.periph_clk;
}

/*******************************************************************************
 * ������� ��������� ������� ������������ ��.
 ******************************************************************************/
supervisor__event_e hw_supervisor__clk_set(supervisor__clk_e clk, int sleep) {
    switch (clk) {
        case SUPERVISOR__CLK_LOW: {
            // ������������ �� MSI, SYSCLK = HCLK = 4.2 MHz, APB1,APB2 = 1. ��� ��������� 4.2 MHz
            break;
        }
        case SUPERVISOR__CLK_HIGH: {
            // ������������ �� HSI, SYSCLK = HCLK = 48 MHz.
            /* Enable Prefetch Buffer */
//            FLASH->ACR |= FLASH_ACR_PRFTEN;
            /* Flash 2 wait state */
            MODIFY_REG(FLASH->ACR, FLASH_ACR_LATENCY, 2);
            while ((FLASH->ACR &  FLASH_ACR_LATENCY) == 0) {;}

            MODIFY_REG(RCC->APB1ENR1, 0, RCC_APB1ENR1_PWREN);     //��������� ������������ PWR �������, ��� ��� �� ����� � ����!
            PWR->CR1 = PWR_CR1_VOS_0 | PWR_CR1_LPMS_STOP2 | PWR_CR1_DBP;  // Stop 2 mode cannot be entered when LPR bit is set. Stop 1 is entered instead.
            //������ HSI
            MODIFY_REG(RCC->CR, 0, RCC_CR_HSION);
            //���� ������������ HSI
            while ((RCC->CR & RCC_CR_HSIRDY) == 0) {;}
             /*��������� ���������� ��������� �����, ��� ������� �� ��������� �������
              ���������� ����������� ��� ����������� PLL c��. 144 � REF.MANUAL */
             //��������� PLL
            MODIFY_REG(RCC->CR, RCC_CR_PLLON, 0);
            // �������� ��������� ������� (HCLK = SYSCLK)
            //�������� ���� APB1 (PCLK2 = HCLK)
            //�������� ���� APB2 (PCLK1 = HCLK)
            //���������  - ����� �� ���� � HSI �����������.
            MODIFY_REG(RCC->CFGR, 0, RCC_CFGR_HPRE_DIV1 | RCC_CFGR_PPRE1_DIV1 | RCC_CFGR_PPRE2_DIV1 | RCC_CFGR_STOPWUCK);
            //������ �������� �������� � ����������
            RCC->PLLCFGR = RCC_PLLCFGR_PLLSRC_HSI | RCC_PLLCFGR_PLLREN | (0x0C << RCC_PLLCFGR_PLLN_Pos) | RCC_PLLCFGR_PLLM_0 | RCC_PLLCFGR_PLLQEN;  //todo USB ��� ��������?
            //�������� PLL
            MODIFY_REG(RCC->CR,0 , RCC_CR_PLLON);
            //���� ������������ PLL
            while((RCC->CR & RCC_CR_PLLRDY) == 0) {;}
           //�������� PLL ��� ������������ SysClk
            MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, RCC_CFGR_SW_PLL);
            //���� ������������ PLL
            while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL) {;}
            // MSI OFF
            MODIFY_REG(RCC->CR, RCC_CR_MSION, 0);
            supervisor.cpu_clk = SYS_CLOCK_HIGH_SLEEP_0_HZ;
            supervisor.periph_clk = SYS_CLOCK_HIGH_SLEEP_0_HZ;

            break;
        }
        default:
            return SUPERVISOR__EVENT_ERROR;
    }
    return SUPERVISOR__EVENT_OK;
}

/*******************************************************************************
 * ������� ��������� PVR �� ������ �������.
 ******************************************************************************/
/*
static void hw_supervisor__set_voltage_regulator_range(supervisor__pvr_e range) {
    RCC->CR &=~  RCC_CR_HSEON;
    //�������� ������������ PWR
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    //���� ��������� VOSF � �������� PWR_CSR � ����
    while(READ_BIT(PWR->CSR, PWR_CSR_VOSF) == 1) {;}
    switch (range) {
        case LVL_1_8V: // 1.8V MAX  RANGE 1
            PWR->CR |= PWR_CR_VOS_0;
            PWR->CR &=~ PWR_CR_VOS_1;
            break;
        case LVL_1_5V: // 1.5V medium RANGE 2
            PWR->CR |= PWR_CR_VOS_1;
            PWR->CR &=~ PWR_CR_VOS_0;
            break;
        case LVL_1_2V: // 1.2V LOW RANGE 3
            PWR->CR |= PWR_CR_VOS_0;
            PWR->CR |= PWR_CR_VOS_1;
            break;
        default:   //�������� ��� ���������, �������� ���������� RANGE
            PWR->CR &=~ PWR_CR_VOS_0;
            PWR->CR &=~ PWR_CR_VOS_1;
            break;
    }
    //���� ��������� VOSF � �������� PWR_CSR � ����
    while(READ_BIT(PWR->CSR, PWR_CSR_VOSF) == 1) {;}
    //��������� ������ �� 104 ���. � RM
}
*/


#ifdef WE_NEED_PVD
/*******************************************************************************
 * ������� ��������� PVD �� ������ �������.
 ******************************************************************************/
static void hw_supervisor__set_pvd_threshold(supervisor__pvd_e threshold) {
    //�������� ������������ PWR
    MODIFY_REG(RCC->APB1ENR1, 0, RCC_APB1ENR1_PWREN);
//    //�������� PVD
    MODIFY_REG(PWR->CR2, 0, PWR_CR2_PVDE);
//    //���������� ��������� ������� ����������
     MODIFY_REG(PWR->CR2, PWR_CR2_PLS_Msk, threshold << PWR_CR2_PLS_Pos);
}
#endif
/*******************************************************************************
 * ������� ��������� BOR �� ������ �������.
 ******************************************************************************/
static void hw_supervisor__set_bor_lvl(supervisor__bor_e borlvl) {
    WRITE_REG(FLASH->KEYR, 0x45670123);
    WRITE_REG(FLASH->KEYR, 0xCDEF89AB);
    WRITE_REG(FLASH->OPTKEYR, 0x08192A3B);
    WRITE_REG(FLASH->OPTKEYR, 0x4C5D6E7F);
    MODIFY_REG(FLASH->OPTR, FLASH_OPTR_BOR_LEV_Msk | FLASH_OPTR_IWDG_STOP , borlvl << FLASH_OPTR_BOR_LEV_Pos | FLASH_OPTR_DBANK);
    FLASH->CR = (uint32_t)(0x03 << 30);
}

#ifdef CPU_USAGE
static void hw_supervisor__cpu_usage_init() {
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM6EN;  //�������� ������������
    TIM6->DIER = TIM_DIER_UIE;  //�������� ���������� �� ������������
    TIM6->PSC = 47999;
    TIM6->CR1 = TIM_CR1_CEN; //�������� ������
    TIM6->ARR = 999;
    NVIC_SetPriority(TIM6_IRQn, 1);
    NVIC_EnableIRQ(TIM6_IRQn);
}

void hw_supervisor__cpu_usage_handler() {
    int i = 100 - (supervisor.cpu_cnt - 800) / 30; //��������� �� �������
    if (i > 100)
        i = 100;
    supervisor.cpu_usage = i < 0 ? 0 : i;
    supervisor.cpu_cnt = 0;
    supervisor.time_active++;
}

u32 hw_supervisor__time_active_get() {
    return supervisor.time_active;
}

u8 hw_supervisor__cpu_usage_get() {
    return supervisor.cpu_usage;
}

void hw_supervisor__cout() {
    supervisor.cpu_cnt++;
}
#endif