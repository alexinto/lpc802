﻿/*******************************************************************************
 * @file sw_fifo.c
 * @brief Утилита для работы с FIFO буфером.
 * @author a.tushentsov
 ******************************************************************************/
#include <stdint.h>
#include <string.h>
#include "target.h"
#include "System/sw_fifo.h"
#include "System/resource.h"

static uint32_t sw_fifo__offset;

#if (!defined CRITICAL_SECTION_ON) || (!defined CRITICAL_SECTION_OFF)
    #error "Must be define CRITICAL_SECTION_ON and CRITICAL_SECTION_OFF in target.h!"
#endif

/*******************************************************************************
 * Функция установки смещения на буфер.
 ******************************************************************************/
void sw_fifo__offset_set(uint32_t offset) {
    sw_fifo__offset = offset;
}

/*******************************************************************************
 * Функция инициализации FIFO буфера.
 ******************************************************************************/
int sw_fifo__init(fifo__struct_t *fifo, uint8_t* buff, int size) {
    if (buff) {
        while(resource__lock(fifo) != EVENT__OK){}  // Ждем захвата ресурса
        CRITICAL_SECTION_ON                         // Атомарность при инициализации
        fifo->tx = fifo->rx = fifo->buff = buff;
        fifo->size = size;
        CRITICAL_SECTION_OFF
        resource__unlock(fifo);
        return size;
    }
    return 0;
}

/*******************************************************************************
 * Функция очистки FIFO буфера.
 ******************************************************************************/
int sw_fifo__flush(fifo__struct_t *fifo) {
    if (fifo == NULL)
        return 0;
    while(resource__lock(fifo) != EVENT__OK){}      // Ждем захвата ресурса
    CRITICAL_SECTION_ON                             // Атомарность при очистке буфера
    fifo->tx = fifo->rx = fifo->buff;
    CRITICAL_SECTION_OFF
    resource__unlock(fifo);
    return fifo->size;
}
/*******************************************************************************
 * Размер свободного места
 ******************************************************************************/
int sw_fifo__free(fifo__struct_t *fifo) {
    if (fifo == NULL)
        return 0;
    int fifo_count = sw_fifo__count(fifo);
    return fifo->size - fifo_count;
}
/*******************************************************************************
 * Размер полезных данных
 ******************************************************************************/
int sw_fifo__count(fifo__struct_t *fifo) {
    if (fifo == NULL)
        return 0;
    uint8_t* rx_buff = fifo->rx;      // Обеспечивается сохранение указателей при условном переходе
    uint8_t* tx_buff = fifo->tx;
    int fifo_count = rx_buff > tx_buff ? (tx_buff + fifo->size - rx_buff) : (tx_buff - rx_buff);   // Макрос для получения полезных данных в fifo.
    return fifo_count;
}

/*******************************************************************************
 * Функция добавления данных в FIFO.
 ******************************************************************************/
int sw_fifo__put(fifo__struct_t *fifo, uint8_t *data, int len) {
    if ((fifo == NULL) || (data == NULL) || (len == 0))
        return 0;
    uint8_t* rx_buff = fifo->rx;    // Обеспечивается сохранение указателей при условном переходе
    uint8_t* tx_buff = fifo->tx;
    int fifo_count = rx_buff > tx_buff ? (tx_buff + fifo->size - rx_buff) : (tx_buff - rx_buff);   // Макрос для получения полезных данных в fifo.
    if (len > (fifo->size - fifo_count))         // Количество свободных байт данных в буфере.
        return 0;
    int chain_len;
    if (rx_buff > tx_buff)
        chain_len = rx_buff - tx_buff;
    else
        chain_len = fifo->buff + fifo->size - tx_buff;
    if (chain_len >= len) {
        memcpy(tx_buff + sw_fifo__offset, data, len);
        tx_buff += len;
    }
    else {
        memcpy(tx_buff + sw_fifo__offset, data, chain_len);
        memcpy(fifo->buff + sw_fifo__offset, &data[chain_len], len - chain_len);
        tx_buff = &fifo->buff[len - chain_len];
    }
    if (tx_buff >= fifo->buff + fifo->size)
        tx_buff = fifo->buff;
    fifo->tx = tx_buff;
    return len;
}

/*******************************************************************************
 * Функция чтения данных из FIFO.
 ******************************************************************************/
int sw_fifo__get(fifo__struct_t *fifo, uint8_t *data, int len) {
    if ((fifo == NULL) || (data == NULL) || (len == 0))
        return 0;
    uint8_t* rx_buff = fifo->rx;     // Обеспечивается сохранение указателей при условном переходе
    uint8_t* tx_buff = fifo->tx;
    int fifo_count = rx_buff > tx_buff ? (tx_buff + fifo->size - rx_buff) : (tx_buff - rx_buff);   // Макрос для получения полезных данных в fifo.
    if (len > fifo_count)            // выдадим столько сколько есть
        len = fifo_count;
    int chain_len;
    if (len) {
        if (rx_buff <= tx_buff) {
            chain_len = tx_buff - rx_buff;
            if (data)
                memcpy(data, rx_buff + sw_fifo__offset, len);
            rx_buff += len;
            if (rx_buff >= fifo->buff + fifo->size)
                rx_buff = fifo->buff;
        }
        else {
            chain_len = fifo->buff + fifo->size - rx_buff;
            if (len > chain_len) {
                if (data) {
                    memcpy(data, rx_buff + sw_fifo__offset, chain_len);
                    memcpy(&data[chain_len], fifo->buff + sw_fifo__offset, len - chain_len);
                }
                rx_buff = fifo->buff + len - chain_len;
            }
            else {
                if (data)
                    memcpy(data, rx_buff + sw_fifo__offset, len);
                rx_buff += len;
            }
        }
    }
    if (rx_buff >= fifo->buff + fifo->size)
        rx_buff = fifo->buff;
    fifo->rx = rx_buff;
    return len;
}
