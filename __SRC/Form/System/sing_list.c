﻿/***************************************************************************//**
 * @file sing_list.c.
 * @brief Вспомогательный модуль для работы с односвязными списками.
 * @author a.tushentsov
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "target.h"
#include "System/sing_list.h"

static void sing_list__include_after(void *list_item, void *new_item);
static void *sing_list__prev(void *item);
static void sing_list__exclude_me(sing_list__head_t* list, void *item);

/*******************************************************************************
 * Функция получения следующего элемента списка.
 ******************************************************************************/
void *sing_list__next_get(void *list, void *item) {
    if (list == NULL || item == list) {
        return NULL;
    }
    if (item == NULL)
        item = ((sing_list__item_t*) list);
    return ((sing_list__item_t*) item)->next == list ? NULL : ((sing_list__item_t*) item)->next;
}

/*********************************************************************************
 * Функция начальной инициализации списка, т.е. заголовка списка.
 ******************************************************************************/
void *sing_list__init(void *list) {
    if (list == NULL)
        return NULL;
    CRITICAL_SECTION_ON
    ((sing_list__head_t*) list)->head.next = list;
    ((sing_list__head_t*) list)->tail = list;
    CRITICAL_SECTION_OFF
    return list;
}

/*******************************************************************************
 * Функция извлечения элемента из списка.
 ******************************************************************************/
void *sing_list__exclude(void *list, void *list_item) {
    sing_list__item_t *item = NULL;
    CRITICAL_SECTION_ON
    if (list_item != NULL && ((sing_list__item_t*) list_item)->next != ((sing_list__item_t*) list_item)) {
        item = ((sing_list__item_t*) list_item);
        sing_list__exclude_me(list, item);
    }
    CRITICAL_SECTION_OFF
    return item;
}

/*******************************************************************************
 * Упрощенная функция для быстрого извлечения указанного элемента из списка.
 ******************************************************************************/
static void sing_list__exclude_me(sing_list__head_t* list, void *item) {
    sing_list__item_t* prev_item = sing_list__prev(item);
    prev_item->next = ((sing_list__item_t*) item)->next;
    ((sing_list__item_t*)item)->next = NULL;
    if (list->tail == item)
        list->tail = prev_item;
}

/*******************************************************************************
 * Функция добавления элемента в список.
 ******************************************************************************/
void *sing_list__include(void *list, void *new_item, sing_list__order_e order) {
    if (list == NULL || new_item == NULL)
        return NULL;
    CRITICAL_SECTION_ON
    switch(order) {
        case SING_LIST__HEAD:
            sing_list__include_after(list, new_item);
            break;
        case SING_LIST__TAIL:
            sing_list__include_after(((sing_list__head_t*)list)->tail, new_item);
            ((sing_list__head_t*)list)->tail = new_item;
            break;
    }
    CRITICAL_SECTION_OFF
    return new_item;
}

/*******************************************************************************
 * Упрощенная функция для быстрого добавления элемента после указанного элемента.
 ******************************************************************************/
static void sing_list__include_after(void *list_item, void *new_item) {
    ((sing_list__item_t*) new_item)->next = ((sing_list__item_t*) list_item)->next;
    ((sing_list__item_t*) list_item)->next = ((sing_list__item_t*) new_item);
}

/*******************************************************************************
 * Функция проверки списка на пустоту.
 ******************************************************************************/
int sing_list__is_empty(void *list) {
    if (((sing_list__item_t*) list)->next == (sing_list__item_t*) list)
        return 1;
    return 0;
}

/*******************************************************************************
 * Функция получения указателя на предыдущий элемент списка (для первого элемента вернет указатель на голову).
 ******************************************************************************/
static void *sing_list__prev(void *item) {
    sing_list__item_t *prev_item = (sing_list__item_t*) item;
        while (prev_item->next != (sing_list__item_t*) item) {
            prev_item = prev_item->next;
        }
        return prev_item;
}
