﻿/***************************************************************************//**
 * @file crc.c.
 * @brief Функции вычисления различных CRC различными методами.
 * @authors a.tushentsov.
 ******************************************************************************/
#include "System/crc.h"

/*******************************************************************************
 * Функция вычисления CRC8 CCITT для указанного буфера.
 ******************************************************************************/
uint8_t crc__8_ccitt(uint8_t *buff, uint32_t len) {
    return crc__8_ccitt_thread(buff, len, CRC__8_THREAD_CCITT);
}

uint8_t crc__8_ccitt_thread(uint8_t *buff, uint32_t len, uint8_t prev_crc) {
    while (len--) {
            prev_crc ^= *buff++;
            for (int i = 0; i < 8; i++)
                prev_crc = prev_crc & 0x80 ? (uint8_t) (prev_crc << 1) ^ 0x07 : (uint8_t) (prev_crc << 1);
    }
    return prev_crc;
}

/*******************************************************************************
 * Функция вычисления CRC32/MPEG2 для указанного буфера.
 ******************************************************************************/
uint32_t crc__32b(uint8_t* buff, uint32_t len) {
   return crc__32b_thread(buff, len, CRC__32b_THREAD_START_CONST);
}

uint32_t crc__32b_thread(uint8_t *buff, uint32_t len, uint32_t prev_crc) {
   uint32_t i, j;
   uint32_t crc, msb;
   crc = prev_crc;
   for(i = 0; i < len; i++) {
      crc ^= (((uint32_t)buff[i])<<24);
      for (j = 0; j < 8; j++) {
            msb = crc>>31;
            crc <<= 1;
            crc ^= (0 - msb) & 0x04C11DB7;
      }
   }
   return crc;
}
