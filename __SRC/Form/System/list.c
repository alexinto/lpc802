﻿/***************************************************************************//**
 * @file list.c.
 * @brief Вспомогательный модуль для работы со списками.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "target.h"
#include "System/list.h"

/*******************************************************************************
 * Функция начальной инициализации списка, т.е. заголовка списка.
 ******************************************************************************/
void *list__init(void *list) {
    CRITICAL_SECTION_ON
    ((list__item_t*) list)->prev = ((list__item_t*) list)->next = list;
    CRITICAL_SECTION_OFF
    return list;
}

/*******************************************************************************
 * Функция извлечения элемента из списка.
 ******************************************************************************/
void *list__exclude(void *list_item, list__order_e order) {
    list__item_t *item = NULL;
    CRITICAL_SECTION_ON
    if (list_item != NULL && ((list__item_t*) list_item)->next != ((list__item_t*) list_item)) {
        switch (order) {
            case LIST__HEAD:
                item = ((list__item_t*) list_item)->next;
                break;
            case LIST__TAIL:
                item = ((list__item_t*) list_item)->prev;
                break;
            case LIST__THIS:
                item = ((list__item_t*) list_item);
                break;
        }
        list__exclude_me(item);
    }
    CRITICAL_SECTION_OFF
    return item;
}

/*******************************************************************************
 * Функция добавления элемента в список.
 ******************************************************************************/
void *list__include(void *list_item, void *new_item, list__order_e order) {
    if (list_item == NULL || new_item == NULL)
        return NULL;
    switch (order) {
        case LIST__HEAD:
            CRITICAL_SECTION_ON
            list__head_include(list_item, new_item);
            CRITICAL_SECTION_OFF
            break;
        case LIST__TAIL:
            CRITICAL_SECTION_ON
            list__tail_include(list_item, new_item);
            CRITICAL_SECTION_OFF
            break;
        default:
            return NULL;
    }
    return new_item;
}

/*******************************************************************************
 * Функция обхода элементов списка.
 ******************************************************************************/
void *list__foreach(void *list, void *prev_item, list__order_e order) {
    if (prev_item == NULL)
        prev_item = ((list__item_t*) list);
    switch ((int) order) {
        case LIST__HEAD:
            return ((list__item_t*) prev_item)->next == list ? NULL : ((list__item_t*) prev_item)->next;
        case LIST__TAIL:
            return ((list__item_t*) prev_item)->prev == list ? NULL : ((list__item_t*) prev_item)->prev;
    }
    return NULL;
}

/*******************************************************************************
 * Функция проверки списка на пустоту.
 ******************************************************************************/
int list__is_empty(void *list) {
    if (((list__item_t*) list)->prev == (list__item_t*) list)
        return 1;
    return 0;
}

/*******************************************************************************
 * Функция проверки элемента на принадлежность очереди (перебор)
 ******************************************************************************/
int list__is_item(void *list, void* item) {
    for (list__item_t* item1 = list; (item1 = item1->next) != list; )
        return 1;
    return 0;
}

/*******************************************************************************
 * Объявление inline функций для работоспособности в IAR, Vitis.
 ******************************************************************************/
#if defined(__ICC430__) || defined(__ICCARM__) || (defined (__GNUC__))
    extern inline void list__head_include(void *list_item, void *new_item);
    extern inline void list__tail_include(void *list_item, void *new_item);
    extern inline void list__exclude_me(void *item);
    extern inline void *list__foreach_head(void *list, void **next);
#endif
