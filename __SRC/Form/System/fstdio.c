/*******************************************************************************
 * @file fstdio.c
 * @brief Набор стандартных функций.
 * @author a.tushentsov
 ******************************************************************************/
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>

typedef struct {
    int32_t len;
    int32_t num1;
    int32_t num2;
    char pad_character;
    int32_t do_padding;
    int32_t left_flag;
    int32_t unsigned_flag;
} params_t;

static int32_t fgetnum(char** linep);
static char* foutnum(char* buff, const int32_t n, const int32_t base, params_t *par);
static char* fouts(char* buff, const char* lp, params_t *par);

int sprintf(char* buff, const char* ctrl1, ...) {
    int32_t Check;
    int32_t dot_flag;
    params_t par;
    char ch;
    va_list argp;
    char *ctrl = (char *) ctrl1;
    char* p_buff = buff;
    va_start(argp, ctrl1);
    while ((ctrl != NULL) && (*ctrl != (char) 0)) {
        /* move format string chars to buffer until a  */
        /* format control is found.                    */
        if (*ctrl != '%') {
            *p_buff++ = *ctrl;
            ctrl += 1;
            continue;
        }
        /* initialize all the flags for this format.   */
        dot_flag = 0;
        par.unsigned_flag = 0;
        par.left_flag = 0;
        par.do_padding = 0;
        par.pad_character = ' ';
        par.num2 = 32767;
        par.num1 = 0;
        par.len = 0;
        try_next: if (ctrl != NULL) {
            ctrl += 1;
        }
        if (ctrl != NULL) {
            ch = *ctrl;
        } else {
            break;
        }

        if (isdigit((int32_t)ch) != 0) {
            if (dot_flag != 0) {
                par.num2 = fgetnum(&ctrl);
            } else {
                if (ch == '0') {
                    par.pad_character = '0';
                }
                if (ctrl != NULL) {
                    par.num1 = fgetnum(&ctrl);
                }
                par.do_padding = 1;
            }
            if (ctrl != NULL) {
                ctrl -= 1;
            }
            goto try_next;
        }
        switch (tolower((int32_t )ch)) {
            case '%':
                *p_buff++ = '%';
                Check = 1;
                break;
            case '-':
                par.left_flag = 1;
                Check = 0;
                break;
            case '.':
                dot_flag = 1;
                Check = 0;
                break;
            case 'l':
                Check = 0;
                break;
            case 'u':
                par.unsigned_flag = 1;
                /* fall through */
            case 'i':
            case 'd':
                p_buff = foutnum(p_buff, va_arg(argp, int32_t), 10L, &par);
                Check = 1;
                break;
            case 'p':
            case 'X':
            case 'x':
                par.unsigned_flag = 1;
                p_buff = foutnum(p_buff, (int32_t) va_arg(argp, int32_t), 16L, &par);
                Check = 1;
                break;
            case 's':
                p_buff = fouts(p_buff, va_arg(argp, char*), &par);
                Check = 1;
                break;
            case 'c':
                *p_buff++ = va_arg(argp, int32_t);
                Check = 1;
                break;
            case '\\':
                switch (*ctrl) {
                    case 'a':
                        *p_buff++ = ((char) 0x07);
                        break;
                    case 'h':
                        *p_buff++ = ((char) 0x08);
                        break;
                    case 'r':
                        *p_buff++ = ((char) 0x0D);
                        break;
                    case 'n':
                        *p_buff++ = ((char) 0x0D);
                        *p_buff++ = ((char) 0x0A);
                        break;
                    default:
                        *p_buff++ = *ctrl;
                        break;
                }
                ctrl += 1;
                Check = 0;
                break;
            default:
                Check = 1;
                break;
        }
        if (Check == 1) {
            if (ctrl != NULL) {
                ctrl += 1;
            }
            continue;
        }
        goto try_next;
    }
    va_end(argp);
    if (ctrl != NULL)
        *p_buff++ = *ctrl;
    return p_buff - buff;
}

static int32_t fgetnum(char** linep) {
    int32_t n;
    int32_t ResultIsDigit = 0;
    char* cptr;
    n = 0;
    cptr = *linep;
    if (cptr != NULL) {
        ResultIsDigit = isdigit(((int32_t )*cptr));
    }
    while (ResultIsDigit != 0) {
        if (cptr != NULL) {
            n = ((n * 10) + (((int32_t) *cptr) - (int32_t) '0'));
            cptr += 1;
            if (cptr != NULL) {
                ResultIsDigit = isdigit(((int32_t )*cptr));
            }
        }
        ResultIsDigit = isdigit(((int32_t )*cptr));
    }
    *linep = ((char*) (cptr));
    return (n);
}

static char* fpadding(char* buff, const int32_t l_flag, const params_t *par) {
    int32_t i;
    if ((par->do_padding != 0) && (l_flag != 0) && (par->len < par->num1)) {
        i = (par->len);
        for (; i < (par->num1); i++) {
            *buff++ = par->pad_character;
        }
    }
    return buff;
}

static char* foutnum(char* buff, const int32_t n, const int32_t base, params_t *par) {
    int32_t negative;
    int32_t i;
    char outbuf[32];
    const char digits[] = "0123456789ABCDEF";
    uint32_t num;
    for (i = 0; i < 32; i++) {
        outbuf[i] = '0';
    }
    /* Check if number is negative                   */
    if ((par->unsigned_flag == 0) && (base == 10) && (n < 0L)) {
        negative = 1;
        num = (-(n));
    } else {
        num = n;
        negative = 0;
    }
    /* Build number (backwards) in outbuf            */
    i = 0;
    do {
        outbuf[i] = digits[(num % base)];
        i++;
        num /= base;
    } while (num > 0);
    if (negative != 0) {
        outbuf[i] = '-';
        i++;
    }
    outbuf[i] = '\0';
    i--;
    /* Move the converted number to the buffer and   */
    /* add in the padding where needed.              */
    par->len = (int32_t) strlen(outbuf);
    buff = fpadding(buff, !(par->left_flag), par);
    while (&outbuf[i] >= outbuf) {
        *buff++ = outbuf[i];
        i--;
    }
    buff = fpadding(buff, par->left_flag, par);
    return buff;
}

static char* fouts(char* buff, const char* lp, params_t *par) {
    char* LocalPtr;
    LocalPtr = (char*)lp;
    /* pad on left if needed                         */
    if (LocalPtr != NULL) {
        par->len = (int32_t) strlen(LocalPtr);
    }
    buff = fpadding(buff, !(par->left_flag), par);
    /* Move string to the buffer                     */
    while (((*LocalPtr) != (char) 0) && ((par->num2) != 0)) {
        (par->num2)--;
        *buff++ = *LocalPtr;
        LocalPtr += 1;
    }
    buff = fpadding(buff, par->left_flag, par);
    return buff;
}
