﻿/***************************************************************************//**
 * @file resource.c.
 * @brief Модуль работы с ресурсом.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/resource.h"
#include "System/hw_resource.h"

/*******************************************************************************
 * Функция инициализации. Пока делает немного, но мб в будущем будет полезной...
 ******************************************************************************/
events__e resource__init(int id) {
    return hw_resource__init(id);
}

/*******************************************************************************
 * Функция захвата ресурса.
 ******************************************************************************/
events__e resource__lock(void* addr) {
    return hw_resource__lock(addr);
}

/*******************************************************************************
 * Функция освобождения ресурса.
 ******************************************************************************/
events__e resource__unlock(void* addr) {
    return hw_resource__unlock(addr);
}

/*******************************************************************************
 * Функция возвращает состояние ресурса.
 ******************************************************************************/
events__e resource__get_state(void* addr) {
    return hw_resource__get_state(addr);
}



WEAK_FUNC events__e hw_resource__init(int id){return EVENT__OK;}
WEAK_FUNC events__e hw_resource__lock(void* addr){return EVENT__OK;}
WEAK_FUNC events__e hw_resource__unlock(void* addr){return EVENT__OK;}
WEAK_FUNC events__e hw_resource__get_state(void* addr){return EVENT__OK;}
