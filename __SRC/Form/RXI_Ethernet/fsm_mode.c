/***************************************************************************//**
 * @file fsm_mode.c.
 * @brief Модуль, реализующий режимы работы изделия.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include <stdlib.h>
#include "target.h"
#include "RXI_Ethernet/fsm_mode.h"

#ifdef SETTINGS_ID
    #include "DRV/dev_setts.h"
#endif

#pragma pack(push,1)
typedef struct {
    union {
        uint8_t flags;
        struct {
            uint8_t password :1;
            uint8_t block :1;
            uint8_t script :1;
        };
    };
}fsm_mode__flags_t;
#pragma pack(pop)


static struct {
    fsm_mode__flags_t* cur_flags;              // Флаги текущего режима работы
    fsm_mode__flags_t* flags[FSM_MODE_NUM];    // Флаги защиты
    fsm_mode__state_e cur_mode;                // Текущий режим работы.
    uint8_t* password, pass_len, pass_en;      // пароль, длина пароля, подтверждение
}fsm_data;

static int fsm_cmd_find(uint16_t cmd_id);
static events__e fsm_cmd_tbl_set (fsm_mode__state_e mode);

/*******************************************************************************
 * Функция инициализации модуля. Вызывается 1 раз при старте системы.
 ******************************************************************************/
events__e fsm_mode__init() {
    cmd_tbl__tbl_t* cmd_tbl__addr = cmd_tbl__get_addr();
#ifdef SETTINGS_ID
    uint16_t len = 0;
    fsm_data.pass_len = dev_setts__get(SETTINGS_ID, DEV_SETTS__PASSWORD, NULL);
    if (fsm_data.pass_len) {                          // Если есть пароль, то запоминаем
        fsm_data.password = malloc(fsm_data.pass_len);
        dev_setts__get(SETTINGS_ID, DEV_SETTS__PASSWORD, fsm_data.password);
    }
#endif
    uint16_t tbl_size = fsm_cmd_find(0) + 1;                                  // Вычисляем количество команд в таблице
    for(int i = 0; i < FSM_MODE_NUM; i++) {                                   // Считываем настройки режимов работы из флеш
        fsm_data.flags[i] = malloc(tbl_size);                                 // Таблица может только расти!
        if (fsm_data.flags[i] == NULL)
            continue;
        memset(fsm_data.flags[i], 0, tbl_size);                               // если режим не стандартный, то все разблокируем, или падаем в hard fault
#ifdef INT_PARAM_ID
        len = dev_setts__get(INT_PARAM_ID, (dev_setts__params_e)i, NULL);
        if (len > tbl_size)                                                   // Если таблица меньше, то перезапись всех настроек
            len= 0;
        else
            dev_setts__get(INT_PARAM_ID, (dev_setts__params_e)i, (uint8_t*)fsm_data.flags[i]);
        if (len < tbl_size) {                                                 // Если настроек нет или они устарели
            for (int j = len; j < tbl_size; j++)                              // считываем табличку, рассматриваем ее как дефолтные
                (fsm_data.flags[i] + j)->flags = cmd_tbl__addr[j].flags;      // 0 - не опечатка!!!
            dev_setts__set(INT_PARAM_ID, (dev_setts__params_e)i, (uint8_t*) fsm_data.flags[i], tbl_size); // и сразу записываем их во флеш
        }
#endif
    }
#ifndef SETTINGS_ID
    fsm_mode__flags_t* p_flags = fsm_data.flags[0];
    if (p_flags) {
        for(int j = 0; cmd_tbl__addr[j].func_id; j++)
            p_flags[j].flags = cmd_tbl__addr[j].flags;
    }
#endif
    return fsm_cmd_tbl_set(FSM_MODE__STATE_WORK);
}

/*******************************************************************************
 * Функция установки режима работы модуля.
 ******************************************************************************/
events__e fsm_mode__set(fsm_mode__state_e mode, uint8_t* password) {
    uint16_t tbl_size = 0;
    if (!password) {
        fsm_data.pass_en = 0;
        return EVENT__OK;
    }
    if (mode >= FSM_MODE_NUM)
        return EVENT__PARAM_NA;
#ifdef ADMIN_PASSWORD
    if (memcmp(password, ADMIN_PASSWORD, 16))
#endif
    if ((!fsm_data.password) || ((fsm_data.pass_en == 0) && memcmp(password, fsm_data.password, fsm_data.pass_len)))
        return EVENT__PARAM_NOT_FOUND;
    fsm_data.pass_en = 1;
    switch(mode) {
        case FSM_MODE__STATE_WORK:
#ifdef INT_PARAM_ID
            if (fsm_data.cur_mode == FSM_MODE__STATE_TEST) {  // Если переключаемся из режима ТЕСТ, то сохраняем настройки.
                tbl_size = fsm_cmd_find(0) + 1;
                for(int i = 0; i < FSM_MODE_NUM; i++)
                    if (fsm_data.flags[i])
                        dev_setts__set(INT_PARAM_ID, (dev_setts__params_e)i, (uint8_t*)fsm_data.flags[i], tbl_size);
            }
#endif
            break;
        case FSM_MODE__STATE_SCRIPT:
        case FSM_MODE__STATE_TEST:
            break;
        default:
            fsm_data.pass_en = tbl_size;
            return EVENT__PARAM_NA;
    }
    fsm_data.cur_mode = mode;
    return fsm_cmd_tbl_set(fsm_data.cur_mode);
}

/*******************************************************************************
 * Функция чтения текущего режима работы модуля.
 ******************************************************************************/
fsm_mode__state_e fsm_mode__get(uint8_t* pass_en) {
    if (pass_en)
        *pass_en = fsm_data.pass_en;
    return fsm_data.cur_mode;
}

/*******************************************************************************
 * Функция установки конфигурации команды.
 ******************************************************************************/
events__e fsm_mode__cmd_set(fsm_mode__state_e mode, uint16_t cmd_id, fsm_mode__settings_t* settings) {
    int cmd_num = fsm_cmd_find(cmd_id);
    if ((mode >= FSM_MODE_NUM) || (cmd_num < 0))
        return EVENT__PARAM_NOT_FOUND;
    if (fsm_data.flags[mode] == NULL)
        return EVENT__NO_MEM;
    fsm_mode__flags_t* p_flags = fsm_data.flags[mode];
    p_flags[cmd_num].password = settings->password;
    p_flags[cmd_num].block = settings->block;
    p_flags[cmd_num].script = settings->script;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция чтения конфигурации команды.
 ******************************************************************************/
events__e fsm_mode__cmd_get(uint16_t cmd_id, cmd_tbl__tbl_t** d) {
    events__e res = EVENT__PARAM_NA;
    cmd_tbl__tbl_t* cmd_tbl__addr = cmd_tbl__get_addr();
    int cmd_num = fsm_cmd_find(cmd_id);
    if ((cmd_num < 0) || (fsm_data.cur_flags == NULL))
        return EVENT__NOT_EXIST;
    cmd_tbl__tbl_t* p_cmd = &cmd_tbl__addr[cmd_num];
    if (d)
        *d = p_cmd;
    if (fsm_data.cur_flags[cmd_num].block)
        res = EVENT__BLOCK;
    else if (fsm_data.cur_flags[cmd_num].password && (fsm_data.pass_en == 0))
        res = EVENT__PROTECT;
    else if (fsm_data.cur_flags[cmd_num].script)
        res = EVENT__EXECUTE;
    else if (p_cmd->func_exec)
        res = EVENT__OK;
    return res;
}

/*******************************************************************************
 * Функция возвращает номер команды в таблице.
 ******************************************************************************/
static int fsm_cmd_find(uint16_t cmd_id) {
    cmd_tbl__tbl_t* cmd_tbl__addr = cmd_tbl__get_addr();
    uint16_t res = 0;
    while((cmd_tbl__addr[res].func_id != cmd_id) && (cmd_tbl__addr[res].func_id))
        res++;
    return cmd_tbl__addr[res].func_id == cmd_id ? res : -1;
}

/*******************************************************************************
 * Функция установки конфигурации для текущего режима работы.
 ******************************************************************************/
static events__e fsm_cmd_tbl_set (fsm_mode__state_e mode) {
    if ((mode >= FSM_MODE_NUM) || (fsm_data.flags[mode] == NULL))
        return EVENT__PARAM_NOT_FOUND;
    fsm_data.cur_flags = fsm_data.flags[mode];
    return EVENT__OK;
}

events__e fsm_mode__set_default() {
    cmd_tbl__tbl_t* cmd_tbl__addr = cmd_tbl__get_addr();
    uint16_t tbl_size = fsm_cmd_find(0) + 1;                                  // Вычисляем количество команд в таблице
#ifdef INT_PARAM_ID
    dev_setts__clear(INT_PARAM_ID);
#endif
    fsm_mode__flags_t* p_flags = fsm_data.flags[0];
    if (p_flags == NULL)
        return EVENT__PARAM_NOT_FOUND;
    for(int j = 0; j < tbl_size; j++)
        p_flags[j].flags = cmd_tbl__addr[j].flags;
#ifdef INT_PARAM_ID
    for (int i = 0; i < FSM_MODE_NUM; i++) {
        dev_setts__set(INT_PARAM_ID, (dev_setts__params_e)i, (uint8_t*) p_flags, tbl_size);// и сразу записываем их во флеш
        memcpy(&fsm_data.flags[i], p_flags, tbl_size);
    }
#endif
    fsm_cmd_tbl_set(fsm_data.cur_mode);
    return EVENT__OK;
}
