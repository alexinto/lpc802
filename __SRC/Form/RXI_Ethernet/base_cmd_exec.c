/***************************************************************************//**
 * @file base_cmd_exec.с.
 * @brief Модуль выполнения команд, реализующий общие команды процессора.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stdio.h>
#include "stdlib.h"
#include <string.h>
#include "target.h"
#include "RXI_Ethernet/fsm_mode.h"
#include "RXI_Ethernet/RxiCmd.h"

/*******************************************************************************
 * Функция установки режима работы.
 ******************************************************************************/
events__e cmd__op_mode_set(cmd__u* cmd) {
    uint8_t password[16];
    uint16_t mode = *(uint16_t*)cmd->cmd.param;
    memcpy(password, &cmd->cmd.param[2], 16);
    return fsm_mode__set((fsm_mode__state_e)mode, password);
}

/*******************************************************************************
 * Функция чтения режима работы.
 ******************************************************************************/
events__e cmd__op_mode_get(cmd__u* cmd) {
    *(uint16_t*)cmd->cmd.param = fsm_mode__get(&cmd->cmd.param[2]);
    cmd->cmd.hdr.param_len = 3;
    return EVENT__EXECUTE;
}

/*******************************************************************************
 * Функция чтения настроек команды.
 ******************************************************************************/
events__e cmd__cfg_cmd_get(cmd__u* cmd) {
    uint16_t* p_sett = (uint16_t*)&cmd->cmd.param[2];
    switch(fsm_mode__cmd_get(*(uint16_t*)cmd->cmd.param, NULL)) {
        case EVENT__OK:
            *p_sett = 0;
            break;
        case EVENT__BLOCK:
            *p_sett = 2;
            break;
        case EVENT__PROTECT:
            *p_sett = 1;
            break;
        default:
            return EVENT__PARAM_NOT_FOUND;
    }
    cmd->cmd.hdr.param_len = 4;
    return EVENT__EXECUTE;
}

/*******************************************************************************
 * Функция установки настроек команды.
 ******************************************************************************/
events__e cmd__cfg_cmd_set(cmd__u* cmd) {
    fsm_mode__settings_t settings;
    memcpy(&settings, &cmd->cmd.param[4], sizeof(fsm_mode__settings_t));
    if (fsm_mode__cmd_set((fsm_mode__state_e)*(uint16_t*)cmd->cmd.param, *(uint16_t*)&cmd->cmd.param[2], &settings) != EVENT__OK)
        return EVENT__PARAM_NA;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция установки параметров по-умолчанию.
 ******************************************************************************/
events__e cmd__set_default(cmd__u* cmd) {
    fsm_mode__set_default();
    return EVENT__OK;
}

/*******************************************************************************
 * Функция возвращает время выполнения команд в пакете.
 ******************************************************************************/
events__e cmd__task_time_req(cmd__u* cmd) {
    uint32_t time_end = time__get_mks();
    uint32_t time_begin = RxiCmd__get_time_pkt(0);
    cmd->cmd.hdr.param_len = 4;
    *(uint32_t*)cmd->cmd.param = (time_end < time_begin) ? 0xFFFFFFFF - time_begin + time_end : time_end - time_begin;
    return EVENT__EXECUTE;
}

/*******************************************************************************
 * Функция включения/выключения защиты команд паролем.
 ******************************************************************************/
events__e cmd__pass_en(cmd__u* cmd) {
    return fsm_mode__set(fsm_mode__get(NULL), cmd->cmd.param[16] ? cmd->cmd.param : NULL);
}


/*******************************************************************************
 * Функция чтения состояния защиты команд паролем.
 ******************************************************************************/
events__e cmd__pass_en_req(cmd__u* cmd) {
    cmd->cmd.hdr.param_len = 1;
    fsm_mode__get(cmd->cmd.param);
    return EVENT__EXECUTE;
}

/*******************************************************************************
 * Функция чтения идентификатора модуля.
 ******************************************************************************/
#if (!defined IDN_DESCR) || (!defined SW_VERSION)
#error "Must be define IDN_DESCR (select from idn_types.h) and SW_VERSION in target.h!!!"
#endif

events__e cmd__idn_req(cmd__u* cmd) {
    uint8_t ver[] = SW_VERSION;
    uint8_t len = (uint8_t)strlen(IDN_DESCR) + 1;
    memcpy(cmd->cmd.param, ver, sizeof(ver));
    memcpy(&cmd->cmd.param[sizeof(ver)], IDN_DESCR, len);
#ifdef SW_HASH
    uint8_t sw_hash[40] = SW_HASH;
    memcpy(&cmd->cmd.param[88], sw_hash, sizeof(sw_hash));
#endif
    cmd->cmd.hdr.param_len = 128;
    return EVENT__EXECUTE;
}

/*******************************************************************************
 * Функция возвращает размер буфера в байтах.
 ******************************************************************************/
#ifndef RXI_CMD_BUFF
#error "Must be define RXI_CMD_BUFF in target.h!!!"
#endif
events__e cmd__buf_size_req(cmd__u* cmd) {
    uint32_t buf_size = RXI_CMD_BUFF;
    cmd->cmd.hdr.param_len = 5;
    *cmd->cmd.param = 1;
    memcpy(&cmd->cmd.param[1], &buf_size, 4);
    return EVENT__EXECUTE;
}

/*******************************************************************************
 * Функция чтения UTC времени.
 ******************************************************************************/
events__e cmd__utc(cmd__u* cmd) {
    time__set(*(uint32_t*)cmd->cmd.param);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция установки UTC времени.
 ******************************************************************************/
events__e cmd__utc_req(cmd__u* cmd) {
    *(uint32_t*)cmd->cmd.param = time__get();
    cmd->cmd.hdr.param_len = 4;
    return EVENT__EXECUTE;
}

/*******************************************************************************
 * Функция записи параметра во флеш.
 ******************************************************************************/
events__e cmd__memory_wr(cmd__u* cmd) {
    uint32_t* addr, data = 0;
    uint8_t size = *cmd->cmd.param;
    if((size > 4) || (cmd->cmd.hdr.param_len < 5))
        return EVENT__PARAM_NA;
    memcpy(&addr, &cmd->cmd.param[1], 4);
    memcpy(&data, &cmd->cmd.param[5], size);
    *addr = data;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция чтения параметра из флеш-памяти.
 ******************************************************************************/
events__e cmd__memory_rd(cmd__u* cmd) {
    uint32_t* addr, data;
    uint8_t size = *cmd->cmd.param;
    if((size > 4) || (cmd->cmd.hdr.param_len < 5))
        return EVENT__PARAM_NA;
    memcpy(&addr, &cmd->cmd.param[1], 4);
    data = *addr;
    memcpy(&cmd->cmd.param[5], &data, 4);
    cmd->cmd.hdr.param_len = 9;
    return EVENT__EXECUTE;
}

#ifndef WEAK_FUNC
    #warning "Must be define WEAK_FUNC in target.h!!!"
#else
// Реализация функций- заглушек
WEAK_FUNC events__e time__init(){return EVENT__OK;}
WEAK_FUNC uint32_t time__get(){return 0;}
WEAK_FUNC uint32_t time__get_mks(){return 0;}
WEAK_FUNC void time__set(uint32_t timestamp){}

#endif
