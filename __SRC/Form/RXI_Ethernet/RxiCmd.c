﻿/***************************************************************************//**
 * @file RxiCmd.с.
 * @brief Модуль, реализующий выборку команд RXI.
 * @author a.tushentsov.
 ******************************************************************************/
#include "System/fstandard.h"
#include "System/RxiTime.h"
#include "RXI_Ethernet/RxiCmd.h"
#include "RXI_Ethernet/cmd_tbl.h"
#include "RXI_Ethernet/fsm_mode.h"
#include "System/framework.h"
#include "System/sw_fifo.h"

// Перечень состояний автомата-обработчика RXI команд
typedef enum {
    RXI_CMD__FSM_IDLE         = 0,    // Ожидаем пакет c командами
    RXI_CMD__CHECK_CMD        = 1,    // Выбираем следующую команду в пакете
    RXI_CMD__ADD_ANSWER       = 2,    // Добавляем ответ на команду в буфер ответов
    RXI_CMD__FSM_TX           = 3,    // Отправляем пакет с ответами
}rxi_cmd__fsm_e;

typedef struct {
    uint8_t wait_answer;              // Флаг ожидания ответа на команду при асинхронном выполнении.
    rxi_cmd__fsm_e fsm;               // Текущее состояние автомата-обработчика RXI команд
    RxiEthernet__proc_e proc;         // Процессор, для которого предназначени пакет. Только для мастера!
    int answer_len;                   // Текущий размер полезных данных в буфере ответов.
    fifo__struct_t* fifo_cmd;         // Указатель на fifo-буфер с пакетами команд.
    fifo__struct_t* fifo_answer;      // Указатель на fifo-буфер для ответных пакетов.
    cmd__u cur_cmd;                   // Текущая команда\ответ.
    RxiEthernet__hdr_t cur_pkt;       // Хэдер текущего пакета с командами.
    uint8_t answer_buff[RXIETHERNET_TRANSMIT]; // буфер ответов на команды RXI.
}rxi_proc_data_t;

static rxi_proc_data_t rxi_proc_data[RXIETHERNET__NUM];

static int module_init;               // Флаг инициализации модуля.
static framework__sub_t cout_sub;     // Экземпляр подписчика на функцию внешнего вызова (cout).

static void RxiCmd__hdr_fill(events__e event, answer__hdr_t* hdr);
static events__e rxi_cmd__exec(uint8_t id);
static events__e rxi_cmd__send_answer(uint8_t id);
static events__e rxi_cmd__add_answer(uint8_t id, cmd__u* cmd);
static void RxiCmd__cout();

/*******************************************************************************
 * Функция инициализации модуля. Вызывается 1 раз при старте системы.
 ******************************************************************************/
events__e RxiCmd__init(uint8_t id, RxiEthernet__proc_e proc, fifo__struct_t* fifo_cmd, fifo__struct_t* fifo_answer) {
    rxi_proc_data_t* p_data = &rxi_proc_data[id];
    if (!module_init) {
        module_init = 1;
        framework__cout_subscribe(&cout_sub, RxiCmd__cout);
    }
    p_data->proc = proc;
    p_data->fifo_cmd = fifo_cmd;
    p_data->fifo_answer = fifo_answer;
    return EVENT__OK;
}

/*******************************************************************************
 * Функции возвращает указатель на буфер с контекстом и размер данных.
 ******************************************************************************/
uint8_t* RxiCmd__context_get(uint8_t id, int* size) {
    rxi_proc_data_t* res =  &rxi_proc_data[id];
    if (size)
        *size = sizeof(rxi_proc_data_t);
    return (uint8_t*)res;
}

/*******************************************************************************
 * Функции устанавливает контекст, предварительно сохраненный RxiCmd__context_get.
 ******************************************************************************/
events__e RxiCmd__context_set(uint8_t id, uint8_t* buff, int size) {
    events__e res = EVENT__ERROR;
    rxi_proc_data_t* p_data =  &rxi_proc_data[id];
    if (size == sizeof(rxi_proc_data_t)) {
        memcpy(p_data, buff, size);
        res = EVENT__OK;
    }
    return res;
}


/*******************************************************************************
 * Функция добавления ответа на команду при асинхронном выполнении.
 ******************************************************************************/
events__e RxiCmd__set_answer(uint8_t id, events__e event, cmd__u* cmd) {
    rxi_proc_data_t* p_data = &rxi_proc_data[id];
    RxiCmd__hdr_fill(event, &cmd->answer.hdr);
    p_data->wait_answer = 0;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция поиска и обработки результатов выполнения команды из таблицы cmd_tbl.с.
 ******************************************************************************/
static events__e rxi_cmd__exec(uint8_t id) {
    cmd_tbl__tbl_t* cmd_ptr = NULL;
    cmd__u* cmd = &rxi_proc_data[id].cur_cmd;
    // Проверяем права на выполнение команды для текущего режима
    events__e res = fsm_mode__cmd_get(cmd->cmd.hdr.cmd_id, &cmd_ptr);
    switch(res) {
        case EVENT__OK:                                          // Выполняем команду в номральном режиме
        case EVENT__EXECUTE:                                     // Выполняем команду в режиме скриптов
            res = cmd_ptr->func_exec(cmd);
            break;
        default:
            break;
    }
    if (res == EVENT__CONTINUE)                                  // Ожидаем результата выполнения команды
        return res;
    RxiCmd__hdr_fill(res, &cmd->answer.hdr);
    return res;
}

/*******************************************************************************
 * Функция формирует hdr ответа на команду в соответствии с RXI_Ethernet.
 ******************************************************************************/
static void RxiCmd__hdr_fill(events__e event, answer__hdr_t* hdr) {
    hdr->status = ANSWER_CMD__ERROR;
    // Обработка результата выполнения команды
    switch(event) {
        case EVENT__TIMEOUT:
            hdr->err_num = CMD_ERR__TIMEOUT;
            break;
        case EVENT__ERROR:
        case EVENT__HW_ERROR:
            hdr->err_num = CMD_ERR__HW_ERROR;
            break;
        case EVENT__PROTECT:
            hdr->err_num = CMD_ERR__PROTECT;
            break;
        case EVENT__BLOCK:
            hdr->err_num = CMD_ERR__BLOCK;
            break;
        case EVENT__PARAM_NA:
            hdr->err_num = CMD_ERR__PARAM_NA;
            break;
        case EVENT__PARAM_NOT_FOUND:
            hdr->err_num = CMD_ERR__PARAM_NOT_FOUND;
            break;
        case EVENT__OK:
            hdr->status = ANSWER_CMD__DONE;  // ответ DONE
            break;
        case EVENT__EXECUTE:
            hdr->status = ANSWER_CMD__ANSWER;// Добавляем ответ в пакет
            break;
        case EVENT__NO_MEM:
            hdr->err_num = CMD_ERR__NO_MEMORY;
            break;
        case EVENT__BUSY:
            hdr->err_num = CMD_ERR__BUSY;
            break;
        case EVENT__NOT_EXIST:
        default:
            hdr->err_num = CMD_ERR__NOT_EXIST;
            break;
    }
}

/*******************************************************************************
 * Функция обработки внутренних состояний модуля.
 ******************************************************************************/
static void RxiCmd__cout() {
    cmd__u* cmd;
    rxi_proc_data_t* p_data;
    for(int id = 0; id < RXIETHERNET__NUM; id++) {
        p_data = &rxi_proc_data[id];
        cmd = &p_data->cur_cmd;
        switch(p_data->fsm) {
            case RXI_CMD__FSM_IDLE:                     // Ищем пакет в буфере FIFO
                if (!sw_fifo__count(p_data->fifo_cmd))
                    break;
                sw_fifo__get(p_data->fifo_cmd, (uint8_t*)&p_data->cur_pkt, sizeof(p_data->cur_pkt));  // без break!!!
                p_data->cur_pkt.microseconds = time__get_mks();
                p_data->cur_pkt.timestamp = time__get();
    //            p_data->cur_answer.hdr.type = PKT_TYPE__ANSWER_PART;  // todo Пока заполняется при принятии пакета по Ethernet...
            case RXI_CMD__CHECK_CMD:
                if (!sw_fifo__get(p_data->fifo_cmd, (uint8_t*)cmd, sizeof(cmd->cmd.hdr)))
                    break;
                p_data->cur_pkt.len -= sizeof(cmd->cmd.hdr);
                p_data->cur_pkt.len -= sw_fifo__get(p_data->fifo_cmd, cmd->cmd.param, cmd->cmd.hdr.param_len);
                p_data->wait_answer = 1;                             // Команд без ответов не бывает
                p_data->fsm = RXI_CMD__ADD_ANSWER;
                if (rxi_cmd__exec(id) == EVENT__CONTINUE)            // Ожидаем ответа на команду
                    break;
                p_data->wait_answer = 0;                             // без break!!!
            case RXI_CMD__ADD_ANSWER:
                if (p_data->wait_answer)
                    break;
                if (rxi_cmd__add_answer(id, cmd) != EVENT__OK) {     // если нет места в буфере, то пытаемся отправить
                    rxi_cmd__send_answer(id);
                    break;
                }
                if (p_data->cur_pkt.len) {
                    p_data->fsm = RXI_CMD__CHECK_CMD;                // не тратим дополнительный cout на обход
                    break;
                }
                p_data->fsm = RXI_CMD__FSM_TX;
            case RXI_CMD__FSM_TX:
                if (rxi_cmd__send_answer(id) != EVENT__OK)           // Нет места в буфере на отправку, ждем...
                    break;
                // если все ответы на все команды в пакете отправлены, то к следующему пакету в RXI_CMD__FSM_IDLE
                // иначе- добавляем ответ в освободившийся буфер
                p_data->fsm = (p_data->cur_pkt.len) ? RXI_CMD__ADD_ANSWER : RXI_CMD__FSM_IDLE;
                break;
            default:
                break;
        }
    }
}

/*******************************************************************************
 * Функция отправки буфера ответов (пакета) в fifo буфер.
 ******************************************************************************/
static events__e rxi_cmd__send_answer(uint8_t id) {
    events__e res = EVENT__NO_MEM;
    rxi_proc_data_t* p_data = &rxi_proc_data[id];
    int cur_len;
    if (sw_fifo__free(p_data->fifo_answer) > (p_data->answer_len + sizeof(p_data->cur_pkt))) {
        cur_len = p_data->cur_pkt.len;                  // Сохраняем текущий счетчик байт пакета
        p_data->cur_pkt.len = p_data->answer_len;
        sw_fifo__put(p_data->fifo_answer, (uint8_t*)&p_data->cur_pkt, sizeof(p_data->cur_pkt));
        sw_fifo__put(p_data->fifo_answer, p_data->answer_buff, p_data->answer_len);
        p_data->cur_pkt.hdr.pkt_part_id++;      // ответный пакет отправлен, либо в буфере на отправку
        p_data->answer_len = 0;
        p_data->cur_pkt.len = cur_len;             // Восстанавливаем счетчик байт пакета.
        res = EVENT__OK;
    }
    return res;
}

/*******************************************************************************
 * Функция добавления ответа на команду в буфер ответов.
 ******************************************************************************/
static events__e rxi_cmd__add_answer(uint8_t id, cmd__u* cmd) {
    events__e res = EVENT__NO_MEM;
    rxi_proc_data_t* p_data = &rxi_proc_data[id];
    answer__hdr_t* answer = &cmd->answer.hdr;
    int cur_len = sizeof(answer__hdr_t);
    uint8_t* buff = p_data->answer_buff + p_data->answer_len;
    switch (answer->status) {
        case ANSWER_CMD__DONE:
            cur_len = 1;
            break;
        case ANSWER_CMD__ANSWER:
            cur_len += answer->param_len;
            break;
        case ANSWER_CMD__ERROR:
            break;
        default:
            return EVENT__OK;
    }
    if ((p_data->answer_len + cur_len) < sizeof(p_data->answer_buff)) { // добавляем ответ в пакет
        p_data->answer_len += cur_len;
        memcpy(buff, answer, cur_len);
        if (!p_data->cur_pkt.len)                                             // если нет команд в пакете
            p_data->cur_pkt.hdr.type = PKT_TYPE__ANSWER;
        res = EVENT__OK;
    }
    return res;
}

/*******************************************************************************
 * Функция возвращает время принятия пакета.
 ******************************************************************************/
uint32_t RxiCmd__get_time_pkt(uint8_t id) {
    rxi_proc_data_t* p_data = &rxi_proc_data[id];
    return p_data->cur_pkt.microseconds;
}
