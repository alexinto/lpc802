﻿/***************************************************************************//**
 * @file RxiEthernet.h.
 * @brief Модуль, реализующий протокол RXI_Ethernet.
 * @author a.tushentsov.
 ******************************************************************************/
#include "RXI_Ethernet/RxiEthernet.h"
#include "System/fstandard.h"
#include "System/framework.h"
#include "System/sw_fifo.h"
#include "RXI_Ethernet/fsm_mode.h"
#include "System/sw_timer.h"
#include "System/fdebug.h"

#ifndef RXIETHERNET__SOCKET_NUM          // Количество сокетов (IP адрес + порт) в системе
    #define RXIETHERNET__SOCKET_NUM 20
#endif
#ifndef RXIETHERNET__TIMEOUT_RACK
#define RXIETHERNET__TIMEOUT_RACK 100    // Таймаут ожидания RACK, мс
#endif


#pragma pack(push, 1)
// Структура хэдера ответа на RXI команду
typedef struct {
     uint8_t status;
     uint16_t param_len;
}RxiEthernet__response_t;
// Структура хэдера команды
typedef struct {
     uint16_t cmd_id;
     uint16_t param_len;
}RxiEthernet__request_t;
typedef struct {
    uint8_t type;
    uint16_t id;
}RxiEthernet__pkt_hdr_t;
// Структура пакета
 typedef struct {
     RxiEthernet__pkt_hdr_t hdr;
     RxiEthernet__request_t cmd;
 }RxiEthernet__pkt_cmd_t;
// Структура пакета ответов
typedef struct {
    RxiEthernet__pkt_answer_hdr_t hdr;
    RxiEthernet__response_t answer;
}RxiEthernet__pkt_answer_t;
// Структура пакета ошибок
typedef struct {
    RxiEthernet__pkt_hdr_t hdr;
    uint16_t id_new;
}RxiEthernet__pkt_err_t;
// Объединение структур хэдеров пакета
typedef union {
    RxiEthernet__pkt_hdr_t hdr;
    RxiEthernet__pkt_cmd_t cmd;
    RxiEthernet__pkt_answer_t answer;
    RxiEthernet__pkt_err_t err;
}RxiEthernet__pkt_u;
#pragma pack(pop)
// Перечень состояний автомата на отправку ответного пакета
typedef enum {
    RXI__TX_FSM_IDLE         =0,     // Ожидаем пакета на отправку
    RXI__TX_FSM_GET_DATA     =1,     // Получили hdr пакета, ожидаем остальные данные пакета
    RXI__TX_FSM_TRANSFER     =2,     // Пакет получен, пытаемся отправить...
    RXI__TX_FSM_WAIT_RACK    =3,     // Пакет отправлен, ожидаем подтверждения RACK
}rxi__tx_fsm_e;
// тип структуры сокета
typedef struct {
    uint32_t ip_addr;
    uint32_t port;
    uint16_t pkt_id;
}RxiEthernet__sock_t;
// тип структуры инстанса. На каждый процессор свой инстанс.
typedef struct {
    fifo__struct_t* fifo_cmd;
    fifo__struct_t* fifo_answer;
}RxiEthernet__inst_t;
// тип внутренней структуры модуля
typedef struct {
    uint8_t hw_init;                                      // Инициализация транспорта
    uint8_t cur_proc;
    int answer_len;
    rxi__tx_fsm_e fsm;
    events__e rack;                  // Событие получения RACK
    RxiEthernet__inst_t inst[RXIETHERNET__PROC_NUM];       // [0] - Интерфейсный, [1] - Командный
    union {
        RxiEthernet__hdr_t cur_pkt;
        uint8_t buff[RXIETHERNET_TRANSMIT + sizeof(RxiEthernet__hdr_t)];
    };
    sw_timer__t timer;
}rxi__data_t;


static RxiEthernet__sock_t sock_tbl[RXIETHERNET__SOCKET_NUM];

static int module_init;
static framework__sub_t cout_sub;
static rxi__data_t rxi_data[RXIETHERNET__NUM];

static int RxiEthernet__sock_find(uint32_t addr, uint32_t port);
static void RxiEthernet__cout();
static void RxiEthernet__rx_cb(uint8_t id, events__e event, uint8_t* buff, uint32_t len, uint32_t addr, uint32_t port);
static events__e RxiEthernet__send_answer(uint8_t id);
static void rxi_timer_cb(struct sw_timer__t *timer, void *ext_data);

/*******************************************************************************
 * Функция инициализации.
 ******************************************************************************/
events__e RxiEthernet__init(uint8_t id, uint32_t addr, uint32_t port) {
    events__e res = HwRxiEthernet__init(id, addr, port);
    FDEBUG(FDEBUG__LVL5, "Initialization ethernet %s\n\r", res == EVENT__OK ? "OK" : "ERROR");
    return res;
}

/*******************************************************************************
 * Функция инициализации буферов FIFO.
 ******************************************************************************/
events__e RxiEthernet__init_fifo(uint8_t id, RxiEthernet__proc_e proc, fifo__struct_t* fifo_cmd, fifo__struct_t* fifo_answer) {
    rxi__data_t* p_data = &rxi_data[id];
    if (!module_init) {
        module_init = 1;
        framework__cout_subscribe(&cout_sub, RxiEthernet__cout);
    }
    p_data->inst[proc].fifo_cmd = fifo_cmd;
    p_data->inst[proc].fifo_answer = fifo_answer;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция начала обработки пакетов RXI_EThernet.
 ******************************************************************************/
events__e RxiEthernet__receive(uint8_t id) {
    HwRxiEthernet__rx(id, RxiEthernet__rx_cb);
    return EVENT__OK;
}

events__e RxiEthernet__deinit(uint8_t id) {
    rxi__data_t* p_data = &rxi_data[id];
    events__e res = HwRxiEthernet__deinit(id);
    for(int i = 0; i < RXIETHERNET__PROC_NUM; i++) {
        p_data->inst[i].fifo_cmd = NULL;
        p_data->inst[i].fifo_answer = NULL;
    }
    return res;
}

/*******************************************************************************
 * Коллбэк принятых пакетов RXI_EThernet.
 ******************************************************************************/
static void RxiEthernet__rx_cb(uint8_t id, events__e event, uint8_t* buff, uint32_t len, uint32_t addr, uint32_t port) {
    rxi__data_t* p_data = &rxi_data[id];
    RxiEthernet__pkt_u* pkt = (RxiEthernet__pkt_u*)buff;
    fifo__struct_t* p_fifo = p_data->inst[RXIETNERNET__IO_PROC].fifo_cmd;
    if ((event != EVENT__OK) || (pkt == NULL) || (len > RXIETHERNET_BUFF_RECEIVE) || (len < sizeof(pkt->hdr)))
        return;
    int sock_id = RxiEthernet__sock_find(addr, port);
    if (sock_id < 0)
        return;
    RxiEthernet__sock_t* sock = &sock_tbl[sock_id];
    RxiEthernet__hdr_t pkt_hdr = {.sock_id = sock_id, .len = len - sizeof(pkt->hdr), .hdr = {PKT_TYPE__ANSWER_PART, sock->pkt_id, 0}};
    int fifo_len = pkt_hdr.len + sizeof(pkt_hdr);
    switch(pkt->cmd.hdr.type) {
        case PKT_TYPE__IGNORE_ID:
            break;
        case PKT_TYPE__CMD_PROC:
            p_fifo = p_data->inst[RXIETNERNET__CMD_PROC].fifo_cmd;
        case PKT_TYPE__IO_PROC:                // Если есть место и прилетела хотя бы 1 валидная команда
            if ((pkt->hdr.id == sock->pkt_id) && (sw_fifo__free(p_fifo) > fifo_len) && (pkt_hdr.len >= sizeof(cmd__hdr_t))) {
                fifo_len -= sw_fifo__put(p_fifo, (uint8_t*)&pkt_hdr, sizeof(pkt_hdr));
                fifo_len -= sw_fifo__put(p_fifo, buff + sizeof(pkt->hdr), pkt_hdr.len);
                if (fifo_len) while(1);         // не должно возникать такой ситуации!
                sock->pkt_id++;
            }
            break;
        case PKT_TYPE__RACK:
            if (p_data->cur_pkt.hdr.id == pkt->hdr.id)
                p_data->rack = EVENT__OK;
            return;
        case PKT_TYPE__ERR_ID:   // Заглушка для мастера. Работает только в режиме 1 запрос =  1 ответ!!!
        case PKT_TYPE__ACK:
        case PKT_TYPE__ANSWER:
        case PKT_TYPE__ANSWER_PART:
        default:
            return;
    }
    if (fifo_len) {                                   // Ошибка
        pkt->err.hdr.type = PKT_TYPE__ERR_ID;
        pkt->err.id_new = sock->pkt_id;
        len = sizeof(pkt->err);
    }
    else {
        pkt->hdr.type = PKT_TYPE__ACK;
        len = sizeof(pkt->hdr);
    }
    HwRxiEthernet__tx(id, buff, len, addr, port, NULL);
}

/*******************************************************************************
 * Функция отправки ответного пакета RXI_EThernet.
 ******************************************************************************/
static events__e RxiEthernet__send_answer(uint8_t id) {
    rxi__data_t* p_data = &rxi_data[id];
    RxiEthernet__sock_t* sock;
    events__e res = EVENT__NO_MEM;
    switch(p_data->fsm) {
        case RXI__TX_FSM_IDLE:
            for (int i = 0; i < RXIETHERNET__PROC_NUM; i++) {
                if (sw_fifo__count(p_data->inst[i].fifo_answer)) {
                    p_data->cur_proc = i;
                    sw_fifo__get(p_data->inst[i].fifo_answer, (uint8_t*) &p_data->cur_pkt, sizeof(p_data->cur_pkt));
                    p_data->answer_len = p_data->cur_pkt.len;
                    p_data->cur_pkt.len -= sw_fifo__get(p_data->inst[i].fifo_answer, (uint8_t*)p_data->buff + sizeof(p_data->cur_pkt), p_data->cur_pkt.len);
                    p_data->rack = EVENT__CONTINUE;
                    p_data->fsm = RXI__TX_FSM_TRANSFER;
                    if (p_data->cur_pkt.len)
                        p_data->fsm = RXI__TX_FSM_GET_DATA;
                }
            }
            break;
        case RXI__TX_FSM_TRANSFER:
            sock = &sock_tbl[p_data->cur_pkt.sock_id];
            if (HwRxiEthernet__tx(id, (uint8_t*)&p_data->cur_pkt.hdr, p_data->answer_len + sizeof(p_data->cur_pkt.hdr), sock->ip_addr, sock->port, NULL) == EVENT__OK) {
                p_data->fsm = RXI__TX_FSM_WAIT_RACK;      // Все отправили, переходим к ожиданию RACK.
                sw_timer__start(&p_data->timer, RXIETHERNET__TIMEOUT_RACK, rxi_timer_cb, p_data);
            }
            break;
        case RXI__TX_FSM_GET_DATA:
            if (!sw_fifo__count(p_data->inst[p_data->cur_proc].fifo_answer))
                break;
            p_data->cur_pkt.len -= sw_fifo__get(p_data->inst[p_data->cur_proc].fifo_answer, (uint8_t*)p_data->buff + sizeof(p_data->cur_pkt), p_data->cur_pkt.len);
            if (!p_data->cur_pkt.len) {
                p_data->rack = EVENT__CONTINUE;
                p_data->fsm = RXI__TX_FSM_TRANSFER;
            }
            break;
        case RXI__TX_FSM_WAIT_RACK:
            switch(p_data->rack) {
                case EVENT__CONTINUE:                    // ожидаем RACK
                    break;
                case EVENT__OK:
                    sw_timer__stop(&p_data->timer);
                    p_data->fsm = RXI__TX_FSM_IDLE;      // Все отправили, переходим к ожиданию следующего пакета.
                    break;
                default:                                 // таймаут- повторная отправка пакета
                    p_data->rack = EVENT__CONTINUE;
                    p_data->fsm = RXI__TX_FSM_TRANSFER;
                    break;
            }
            break;
        default:
            break;
    }
    return res;
}

/*******************************************************************************
 * Коллбэк таймера по событию отсутсвия RACK.
 ******************************************************************************/
static void rxi_timer_cb(struct sw_timer__t *timer, void *ext_data) {
    rxi__data_t* p_data = ext_data;
    p_data->rack = EVENT__TIMEOUT;
}

static void RxiEthernet__cout() {
    HwRxiEthernet__cout();
    for(int i = 0; i < RXIETHERNET__NUM; i++) {
        RxiEthernet__send_answer(i);
    }
}

/*******************************************************************************
 * Функция поиска сокета по связке IP-адрес + порт.
 ******************************************************************************/
static int RxiEthernet__sock_find(uint32_t addr, uint32_t port) {
    int res = -1;
    for(int i = 0; i < RXIETHERNET__SOCKET_NUM; i++) {
        if (sock_tbl[i].ip_addr == 0) {
            sock_tbl[i].ip_addr = addr;
            sock_tbl[i].port = port;
        }
        else if ((sock_tbl[i].ip_addr != addr) || (sock_tbl[i].port != port))
                continue;
        res = i;
        break;
    }
    return res;
}
