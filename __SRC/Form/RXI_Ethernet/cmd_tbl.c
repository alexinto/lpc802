/***************************************************************************//**
 * @file cmd_tbl.с.
 * @brief Модуль с таблицей команд процессора.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stddef.h>
#ifdef __cplusplus
extern "C" {
#endif
#include "target.h"
#include "CommonCommand.h"
#include "RXI_Ethernet/cmd_tbl.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_dcl.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_diag.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_pg.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_pin400.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_pin.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_ppmu.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_pwr.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_dps.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_robo_cal.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_awg2m.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_addclock.h"
#include "RXI_Ethernet/CmdTblGrp/cmd_tbl_extif.h"

// Добавлять команды ТОЛЬКО в конец таблицы!!!
static const cmd_tbl__tbl_t cmd_tbl__default[] = {
    {CMD__TASK_TIME_REQ,        cmd__task_time_req,      },
    {CMD__PASS_EN,              cmd__pass_en,            },
    {CMD__PASS_EN_REQ,          cmd__pass_en_req,        },
    {CMD__REBOOT,               cmd__reboot,             },
    {CMD__RST,                  cmd__rst,                },
    {CMD__OPC,                  cmd__opc,                },
    {CMD__OPC_REQ,              cmd__opc_req,            },
    {CMD__IDN_REQ,              cmd__idn_req,            },
    {CMD__BUF_SIZE_REQ,         cmd__buf_size_req,       },
    {CMD__PCIE_BDF,             cmd__pcie_bdf,           },
    {CMD__PCIE_NUM,             cmd__pcie_num,           },
    {CMD__DATA_SW,              cmd__data_sw,            },
    {CMD__UTC,                  cmd__utc,                },
    {CMD__UTC_REQ,              cmd__utc_req,            },
    {CMD__SOCKET,               cmd__socket,             },
    {CMD__SOCKET_REQ,           cmd__socket_req,         },
    {CMD__PASSWORD,             cmd__password,           },
    {CMD__JRNL_WR,              cmd__jrnl_wr,            },
    {CMD__CALIB_WR,             cmd__calib_wr,           },
    {CMD__SETTINGS_WR,          cmd__settings_wr,        },
    {CMD__FLASH_CLR,            cmd__flash_clr,          },
    {CMD__JRNL_RD,              cmd__jrnl_rd,            },
    {CMD__CALIB_RD,             cmd__calib_rd,           },
    {CMD__SETTINGS_RD,          cmd__settings_rd,        },
    {CMD__TABLE_CHECK,          cmd__table_check,        },
    {CMD__TABLE_REQ,            cmd__table_req,          },
    {CMD__DEV_CONFIG,           cmd__dev_config,         },
    {CMD__SYS_CONFIG,           cmd__sys_config,         },
    {CMD__SET_DEFAULT,          cmd__set_default,        .password = 1},
    {CMD__OP_MODE_SET,          cmd__op_mode_set,        .password = 1},
    {CMD__OP_MODE_GET,          cmd__op_mode_get,        },
    {CMD__CFG_CMD_SET,          cmd__cfg_cmd_set,        },
    {CMD__CFG_CMD_GET,          cmd__cfg_cmd_get,        },
    {CMD__SCRIPT_BEGIN,         cmd__script_load,        .script = 1},
    {CMD__SCRIPT_END,           cmd__script_load_end,    .script = 1},
    {CMD__SCRIPT_READ,          cmd__script_read,        .script = 1},
    {CMD__SCRIPT_STATE,         cmd__script_state,       .script = 1},
    {CMD__SCRIPT_STATE_REQ,     cmd__script_state_req,   .script = 1},
    {CMD__SCMD,                 cmd__scmd,               },
    {CMD__SCMD_REQ,             cmd__scmd_req,           },
    {CMD__TEST_SCMD,            cmd__test_scmd,          },
    {CMD__TEST_SCMD_REQ,        cmd__test_scmd_req,      },
    {CMD__TRIGGER,              cmd__trigger,            },
    {CMD__TRIGGER_REQ,          cmd__trigger_req,        },
    {CMD__TRIGGER_STATE,        cmd__trigger_state,      },
    {CMD__TRIGGER_STATE_REQ,    cmd__trigger_state_req,  },
    {CMD__TRIGGER_OUTPUT,       cmd__trigger_output,     },
    {CMD__MEMORY_WR,            cmd__memory_wr,          },
    {CMD__MEMORY_RD,            cmd__memory_rd,          },
    {CMD__SYNC_CLK_INIT,        cmd__sync_clk_init,      },
    {CMD__ROSCILLATOR_SET,      cmd__roscillator_set,    },
    {CMD__ROSCILLATOR_SET_REQ,  cmd__roscillator_set_req,},
    {CMD__FSALIGMENT,           cmd__fsaligment,         },
    {CMD__SSALIGMENT,           cmd__ssaligment,         },
    {CMD__ROSCILLATOR_CH,       cmd__roscillator_ch,     },
    {CMD__ROSCILLATOR_CH_REQ,   cmd__roscillator_ch_req, },
    {CMD__ROSCILLATOR_CC_SET,   cmd__roscillator_cc_set, },
    {CMD__ROSCILLATOR_CC_GET,   cmd__roscillator_cc_get, },
    {CMD__ROSCILLATOR_CC_MODE_SET,cmd__roscillator_cc_mode, },
    {CMD__SYNC_CLK_TUNE,        cmd__sync_clk_autotune, },
    {CMD__SITE_INIT,            cmd__site_init,          },
    {CMD__PG_NUM_GET,           cmd__pg_num_get,         },
    {CMD__SITE,                 cmd__site,               },
    {CMD__COUNT_ERR,            cmd__count_err,          },
    {CMD__SITE_CMD,             cmd__site_cmd,           },
    {CMD__PG_STATUS_GET,        cmd__pg_status_get,      },
    {CMD__SITE_CSTD,            cmd__site_cstd,          },
    {CMD__EMUL_INIT,            cmd__emul_init,          },
    {CMD__EMUL_TP_SET,          cmd__emul_tp_set,        },
    {CMD__EMUL_TP_PG_SET,       cmd__emul_tp_pg_set,     },
    {CMD__EMUL_TP_CLR,          cmd__emul_tp_clr,        },
    {CMD__EMUL_TP_PG_CLR,       cmd__emul_tp_pg_clr,     },
    {CMD__EMUL_PG_CFG,          cmd__emul_pg_cfg,        },
    {CMD__EMUL_PRBS_CFG,        cmd__emul_prbs_cfg,      },
    {CMD__EMUL_PRBS_CFG_GET,    cmd__emul_prbs_cfg_get,  },
    {CMD__EMUL_PG_CFG_GET,      cmd__emul_pg_cfg_get,    },
    {CMD__EMUL_TP_USR_SET,      cmd__emul_tp_usr_set,    },
    {CMD__TEST_CMD,             cmd__test_cmd,           },
    {CMD__TEST_TRIGGER,         cmd__test_trigger,       },
    {CMD__TEST_TRIGGER_REQ,     cmd__test_trigger_req,   },
    {CMD__TEST_PG,              cmd__test_pg,            },
    {CMD__FW_NUM,               cmd__fw_num,             },
    {CMD__FW_VER,               cmd__fw_ver,             },
    {CMD__FW_UPD_CTRL,          cmd__fw_upd_ctrl,        },
    {CMD__FW_UPD_DATA,          cmd__fw_upd_data,        },
    {CMD__PSP_PWR_ON,           cmd__psp_pwr_on,         },
    {CMD__PSP_STATE,            cmd__psp_state,          },
#ifdef CMD_TBL_EXTIF_EN
    CMD_TBL_EXTIF
#endif
#ifdef CMD_TBL_ADDCLOCK_EN
    CMD_TBL_ADDCLOCK
#endif
#ifdef CMD_TBL_DCL_EN
    CMD_TBL_DCL
#endif
#ifdef CMD_TBL_DIAG_EN
    CMD_TBL_DIAG
#endif
#ifdef CMD_TBL_PG_EN
    CMD_TBL_PG
#endif
#ifdef CMD_TBL_PIN400_EN
    CMD_TBL_PIN400
#endif
#ifdef CMD_TBL_PIN_EN
    CMD_TBL_PIN
#endif
#ifdef CMD_TBL_PPMU_EN
    CMD_TBL_PPMU
#endif
#ifdef CMD_TBL_PWR_EN
    CMD_TBL_PWR
#endif
#ifdef CMD_TBL_DPS_EN
    CMD_TBL_DPS
#endif /* CMD_TBL_DPS */
#ifdef CMD_TBL_ROBOCAL_EN
    CMD_TBL_ROBOCAL
#endif
#ifdef CMD_TBL_AWG2M_EN
    CMD_TBL_AWG2M
#endif
    // Конец таблицы. Добавлять команды ТОЛЬКО сюда!!!
    {0x0000, NULL}
};

cmd_tbl__tbl_t* cmd_tbl__get_addr() {
    return (cmd_tbl__tbl_t*)cmd_tbl__default;
}

#ifndef WEAK_FUNC
    #warning "Must be define WEAK_FUNC in target.h!!!"
#else
// Реализация функций - "пустышек"
WEAK_FUNC events__e cmd__reboot(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__rst(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__opc(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__opc_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__task_time_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__buf_size_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pcie_bdf(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pcie_num(cmd__u* cmd){cmd->answer.hdr.param_len = 2; *(uint16_t*)cmd->answer.param = 0; return EVENT__EXECUTE;}
WEAK_FUNC events__e cmd__data_sw(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__op_mode_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__op_mode_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__cfg_cmd_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__cfg_cmd_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__utc(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__utc_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__socket(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__socket_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__password(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pass_en(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pass_en_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__memory_wr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__memory_rd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__jrnl_wr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__calib_wr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__settings_wr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__flash_clr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__jrnl_rd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__calib_rd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__settings_rd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__table_check(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__table_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dev_config(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__sys_config(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__set_default(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__scmd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__scmd_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__test_scmd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__test_scmd_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__idn_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__script_load(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__script_load_end(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__script_read(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__script_delete(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__script_state(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__script_state_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__trigger(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__trigger_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__trigger_state(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__trigger_state_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__trigger_output(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__sync_clk_init(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__roscillator_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__roscillator_set_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__fsaligment(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ssaligment(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__roscillator_ch(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__roscillator_ch_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__roscillator_cc_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__roscillator_cc_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__roscillator_cc_mode(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__sync_clk_autotune(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__site_init(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_num_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__site(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__count_err(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__site_cmd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_status_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__site_cstd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_init(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_tp_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_tp_pg_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_tp_clr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_tp_pg_clr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_pg_cfg(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_prbs_cfg(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_prbs_cfg_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_pg_cfg_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__emul_tp_usr_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__test_cmd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__test_trigger(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__test_trigger_req(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__test_pg(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__internal_interrupt(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__fw_num(cmd__u* cmd){cmd->answer.hdr.param_len = 4; *(uint32_t*)cmd->answer.param = 0; return EVENT__EXECUTE;}
WEAK_FUNC events__e cmd__fw_ver(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__fw_upd_ctrl(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__fw_upd_data(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_spi_init(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_spi_txrx(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_gpio_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_pwr_ctrl(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_i2c_init(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_i2c_start(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_i2c_write(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_i2c_read(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_i2c_readn(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ext_if_i2c_stop(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__addclock_sett_wr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__addclock_sett_rd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__addclock_sin_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__addclock_trig_cfg(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__addclock_rect_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__addclock_sync_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__addclock_gen_ctrl(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__addclock_test(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__psp_pwr_on(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__psp_state(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_cfg_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_cfg_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_id_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_temp_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_ald_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_vrng_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_vrng_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_vil_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_vil_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_vih_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_vih_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_tmode_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_tmode_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_iol_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_iol_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_ioh_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_ioh_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_iload_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_iload_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rout_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rout_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_roh_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_roh_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rol_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rol_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rvtt_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rvtt_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_routi_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_routi_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_ril_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_ril_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rih_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rih_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rit_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_rit_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__cmp_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__cmp_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__cmp_rng_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__cmp_rng_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_cpl_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_cpl_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_cph_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_cph_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_mode_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_mode_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_state_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__drv_state_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_dcl(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_dcl_state_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_pg(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_pg_state_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_static(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_static_state_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_dinamic(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_dinamic_state_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_pmu(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_pmu_state_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_meas(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__diag_meas_state_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_fc_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_fc_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_addr_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_addr_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_pattern_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_pattern_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_flag_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_flag_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_state_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_break_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_break_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_format_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_format_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_cmp_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_cmp_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_mux_mode_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_mux_mode_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_diff_mode_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_diff_mode_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_err_ram_mode_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_err_ram_mode_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_first_err_clk_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pg_err_ram_num_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pin400_settings_wr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pin400_settings_rd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pin_init(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pin_reset(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_connect(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_is_connect(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_fn_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_fn_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_fv_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_fv_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_fi_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_fi_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_fr_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_fr_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_meas_u(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__ppmu_meas_i(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pwr_measure_v(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pwr_measure_i(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pwr_status_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__pwr_ctrl(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_fv(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_fn(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_mv(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_set_perf_mode(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_set_load(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_get_perf_mode(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_get_mode(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_rangei(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_slew_rate(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_get_u(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_get_load(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_get_iclamp_hi(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_get_iclamp_lo(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_get_iclamp_state(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_meas_u(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_meas_i(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_set_voltage(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_set_irange(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_set_iclamp_hi(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_set_iclamp_lo(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__dps_set_slew(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__robocal_movexy(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_part_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_img_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_img_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_syncclk_mux_div_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_syncclk_mux_div_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_fpga_mux_div_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_fpga_mux_div_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_dac_clk_change_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_dac_clk_change_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_ctrl(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_diff_mode_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_diff_mode_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_res_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_res_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_v_offset_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_v_offset_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_range_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_range_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_dac_chg_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_dac_chg_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_lfp_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_lfp_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_dac_wr(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_dac_rd(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_img_ctrl_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_img_ctrl_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_dds_ctrl_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_dds_ctrl_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_cfg_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_cfg_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_start(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_stop(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_io_diag(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_acc_ctrl_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_acc_ctrl_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_acc_range_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_acc_range_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_acc_measure(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_pwr_diag(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_unit_diag(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_dc_diag(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_dc_diag_meas(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_fast_diag(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_acc_tune(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_tune(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_cfg2_set(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_cfg2_get(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_fix2(cmd__u* cmd){return EVENT__NOT_EXIST;}
WEAK_FUNC events__e cmd__awg2m_ch_ctrl_get(cmd__u* cmd){return EVENT__NOT_EXIST;}



#endif
#ifdef __cplusplus
}
#endif

