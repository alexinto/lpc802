﻿/***************************************************************************//**
 * @file at25df321.с.
 * @brief Драйвер микросхемы флеш-памяти AT25DF321.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "System/sw_timer.h"
#include "DRV/spi.h"
#include "DRV/AT25DF321/at25df321.h"

typedef enum {
    cmd =   0,
    addr =  1,
}request_type_e;

typedef enum {
    CMD_READ_STATUS    = 0,     // Операция чтения STATUS регистра. Не приступает к следующей операции пока не снимется флаг занятости.
    CMD_READ_DATA      = 1,     // Операция чтения данных.
    CMD_WRITE_STATUS   = 2,     // Операция записи статус регистра- задел на будущее, не используется...
    CMD_WRITE_ENABLE   = 3,     // Операция открывает доступ на запись в микросхему.
    CMD_WRITE_DISABLE  = 4,     // Операция закрывает доступ на запись в микросхему.
    CMD_WRITE_PAGE     = 5,     // Операция записи данных. Самая сложная операция, парсит данные с учетом межстраничных переходов и т.д.
    CMD_ERASE_SECTOR   = 6,     // Операция стирания сектора. размер сектора возвращает в коллбэке.
    CMD_WAIT           = 7,     // Операция задержки с переключением CS вывода. Используется автоматически после каждой операции скрипта.
    CMD_CHECK_WRITE    = 8,     // Операция проверки доступа на запись- возвращает ERROR, если нет доступа.
    CMD_IDN_READ       = 9,     // Операция считывания IDN флеш. Используется при инициализации модуля.
    CMD_END            = 10,    // Признак конца таблицы.
} at25df321__cmd_e;

typedef struct {
    uint8_t cmd;     // Байт команды (из даташита).
    uint8_t len;     // Длина пакета, включая байт команды. Нельзя ставить больше 5!!!
} cmd_descr__t;
//                                           0          1          2          3         4           5          6          7          8          9          10
static const cmd_descr__t cmd_table[] = {{0x05, 2}, {0x03, 4}, {0x01, 2}, {0x06, 1}, {0x04, 1}, {0x02, 4}, {0x20, 4}, {0x00, 1}, {0x05, 2}, {0x9F, 2}, {0x00, 1}};


// todo Можно вынести в отдельный файл!-----------------------------------------------------------------------------------------------------------------------
#define PAGE_SIZE 256 // todo Нужно куда-то засунуть!!!

// Скрипты для операций.
static const at25df321__cmd_e oper_init[] = {CMD_WAIT, CMD_IDN_READ, CMD_END};
static const at25df321__cmd_e oper_read[] = {CMD_WAIT, CMD_READ_STATUS, CMD_READ_DATA, CMD_READ_STATUS, CMD_END};
static const at25df321__cmd_e oper_write[] = {CMD_WAIT, CMD_READ_STATUS, CMD_WRITE_ENABLE, CMD_WRITE_STATUS, CMD_WRITE_ENABLE, CMD_CHECK_WRITE, CMD_WRITE_PAGE, CMD_READ_STATUS, CMD_WRITE_DISABLE, CMD_END};
static const at25df321__cmd_e oper_erase_sector[] = {CMD_WAIT, CMD_READ_STATUS, CMD_WRITE_ENABLE, CMD_WRITE_STATUS, CMD_WRITE_ENABLE, CMD_CHECK_WRITE, CMD_ERASE_SECTOR, CMD_READ_STATUS, CMD_WRITE_DISABLE, CMD_END};
// Список поддерживаемых операций флеш - соответствует flash__op_e.
static const at25df321__cmd_e* const oper_table[] = {NULL, oper_erase_sector, oper_read, oper_write, NULL, NULL, NULL, NULL};
// -----------------------------------------------------------------------------------------------------------------------------------------------------------
static struct {
    uint8_t request[PAGE_SIZE + 8], answer[PAGE_SIZE + 8], err_counter;
    at25df321__cmd_e* cur_oper;
    uint8_t* buff;
    uint32_t len;
    uint32_t addr, offset;
    int flash_id;
    flash__hw_cb_t cb;
    sw_timer__t timer;
}mx25_data;

static events__e at25df321__execute(at25df321__cmd_e cmd);
static void at25df321__exec_cb(int id, events__e event, uint8_t *buff, int len, void *ext_data);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);

events__e ad25df321__init(int flash_id, uint32_t settings, flash__hw_cb_t cb) {
    mx25_data.cur_oper = (at25df321__cmd_e*)oper_init;
    mx25_data.flash_id = flash_id;
    mx25_data.cb = cb;
    return at25df321__execute(*mx25_data.cur_oper);
}

events__e ad25df321__exec(int flash_id, flash__oper_e oper, uint32_t addr, uint8_t *buff, uint32_t len, flash__hw_cb_t cb) {
    switch (oper) {
        case FLASH__OP_READ:
        case FLASH__OP_WRITE:
        case FLASH__OP_ERASE:
            break;
        default:
            return EVENT__PARAM_NA;
    }
    mx25_data.cur_oper = (at25df321__cmd_e*) oper_table[oper];
    mx25_data.offset = mx25_data.err_counter = 0;
    mx25_data.flash_id = flash_id;
    mx25_data.addr = addr;
    mx25_data.buff = buff;
    mx25_data.len = len;
    mx25_data.cb = cb;
    return at25df321__execute(*mx25_data.cur_oper);
}

static events__e at25df321__execute(at25df321__cmd_e cmd_exec) {
    events__e res = EVENT__OK;
    cmd_descr__t cmd_descr = cmd_table[cmd_exec];
    uint32_t cur_len = cmd_descr.len, cur_addr = mx25_data.addr + mx25_data.offset;
    uint8_t* buff_tx = mx25_data.request, *buff_rx = (((cur_len < 3) && (cmd_descr.cmd)) ? mx25_data.answer : NULL);
    uint8_t addr_len = (cur_len > 1) ?  cur_len - 2 : 0;
    uint16_t write_offset = PAGE_SIZE - ((mx25_data.addr + mx25_data.offset) % PAGE_SIZE);
    mx25_data.request[cmd] = cmd_descr.cmd;
    for(uint8_t x = 0; x <= addr_len; x++)       // Перезапись адреса в MSB first.
        mx25_data.request[addr + x] = *((uint8_t*)&cur_addr + addr_len - x);
    if (cmd_descr.cmd) {
        switch (*mx25_data.cur_oper) {
            case CMD_READ_DATA:
                res = spi__txrx(mx25_data.flash_id, mx25_data.answer, mx25_data.len + cur_len, buff_tx, cur_len, at25df321__exec_cb, NULL);
                break;
            case CMD_WRITE_PAGE:
                mx25_data.err_counter = 0;
                if ((mx25_data.offset + write_offset) > mx25_data.len)
                    write_offset = mx25_data.len - mx25_data.offset;
                mx25_data.offset += write_offset;
                memcpy(mx25_data.request + cur_len, mx25_data.buff + mx25_data.offset - write_offset, write_offset);
                res = spi__txrx(mx25_data.flash_id, NULL, 0, buff_tx, write_offset + cur_len, at25df321__exec_cb, NULL);
                break;
            case CMD_WRITE_STATUS:
                mx25_data.request[addr] = 0;
            default:
                res = spi__txrx(mx25_data.flash_id, buff_rx, cur_len, buff_tx, cur_len, at25df321__exec_cb, NULL);
                break;
        }
    }
    else
        sw_timer__start(&mx25_data.timer, 0, timer_cb, (void*)CMD_WAIT);
    return res;
}

static void at25df321__exec_cb(int id, events__e event, uint8_t *buff, int len, void *ext_data) {
    events__e res = event == EVENT__OK ? EVENT__OK : EVENT__ERROR;
    uint8_t status_check = 0x01;
    at25df321__cmd_e operation = (ext_data || (id == -1)) ? CMD_WAIT : *mx25_data.cur_oper;
    switch(operation) {
    case CMD_CHECK_WRITE:
        status_check = 0x03;
    case CMD_READ_STATUS:
        if ((mx25_data.answer[len - 1] & status_check) != (status_check -1)) {
            if (mx25_data.err_counter++ < 100) {
                mx25_data.cur_oper -= (status_check >> 1) + 1;
                sw_timer__start(&mx25_data.timer, 5, timer_cb, (void*)CMD_WAIT);
                return;
            }
            res = EVENT__ERROR;
        }
    case CMD_WRITE_ENABLE:
    case CMD_WRITE_DISABLE:
    case CMD_WAIT:
        break;
    case CMD_ERASE_SECTOR:
        mx25_data.len = 0x1000;
        break;
    case CMD_READ_DATA:
        memcpy(mx25_data.buff, mx25_data.answer + len - mx25_data.len, mx25_data.len);
        break;
    case CMD_WRITE_PAGE:
        if (mx25_data.offset < mx25_data.len)
            mx25_data.cur_oper = (at25df321__cmd_e*)oper_write;
        break;
    case CMD_IDN_READ:
        if (res == EVENT__OK) {
            if ((mx25_data.answer[len - 1] == 0) || (mx25_data.answer[len - 1] == 0xFF))
                res = EVENT__ERROR;
        }
        break;
    default:
        break;
    }
    if ((*mx25_data.cur_oper == CMD_END) || (res != EVENT__OK) || ((res = at25df321__execute((id == -1) ? *++mx25_data.cur_oper : CMD_WAIT)) != EVENT__OK))
        mx25_data.cb(mx25_data.flash_id, res, mx25_data.addr, mx25_data.buff, mx25_data.len);
}

static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    at25df321__exec_cb(-1, EVENT__OK, NULL, 0, ext_data);
}
