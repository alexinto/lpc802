﻿/***************************************************************************//**
 * @file dev_setts.c.
 * @brief Модуль, реализующий функции работы с настройками в энергонезависимой памяти.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include <stdlib.h>
#include "target.h"
#include "System/crc.h"
#include "DRV/dev_setts.h"

#ifndef DEV_PARAM_MAX_LEN
    #define DEV_PARAM_MAX_LEN       128
#endif

#ifndef DEV_SETTS__NUM
    #define DEV_SETTS__NUM 1
#endif

#define MAX_MEM_ADDR (d->cur_page + d->p_size - sizeof(dev_setts__t))

typedef struct {
    uint32_t write_count;
    uint32_t page_addr;
}dev_setts__struct__t;

typedef struct {
    union {
        uint8_t crc;
        uint8_t reserved[8];
    };
    struct {
        uint16_t len;
        uint16_t param_len;
        dev_setts__params_e param_id;
    }hdr;
}dev_setts__t;

typedef struct {
    uint8_t busy;
    uint32_t dev_id, cur_page, addr, p_size, param_max;
    uint32_t* p_tbl;                                        // Указатель на таблицу параметров
    union {
        dev_setts__t param;
        uint8_t buff[DEV_PARAM_MAX_LEN + sizeof(dev_setts__t)];
    };
    dev_setts__struct__t active_page, backup_page;
    flash__exec_t func;
}dev_setts__data_t;

static dev_setts__data_t data_tbl[DEV_SETTS__NUM];

static uint32_t param_find(dev_setts__data_t* d, dev_setts__params_e param);
static uint32_t dev_setts__reinit_page(uint8_t id);
static uint16_t dev_setts__write_param(uint8_t id, uint8_t dev_id, uint32_t addr, dev_setts__params_e param, uint8_t* buff, uint16_t len);
static void dev_setts__init_tbl(dev_setts__data_t* d);
//static void dev_setts__init_cb(uint8_t dev_id, flash__oper_e oper, events__e event, uint32_t addr, uint8_t *buff, uint32_t len, void* ext_data);


events__e dev_setts__init(uint8_t id, uint8_t dev_id, uint32_t addr, uint32_t size, uint32_t param_max, flash__exec_t func, dev_setts__cb_t cb, void* ext_data) {
    if (id >= DEV_SETTS__NUM)
        return EVENT__PARAM_NA;
    dev_setts__data_t* d = &data_tbl[id];
    d->func = func;
    d->dev_id = dev_id;
    d->addr = addr;
    d->p_size = size / 2;
    d->param_max = param_max;
    d->p_tbl = malloc(sizeof(uint32_t) * d->param_max);
    d->func(d->dev_id, FLASH__OP_READ, addr, (uint8_t*)&d->active_page, sizeof(dev_setts__struct__t), NULL, NULL);
    d->func(d->dev_id, FLASH__OP_READ, addr + d->p_size, (uint8_t*)&d->backup_page, sizeof(dev_setts__struct__t), NULL, NULL);
    d->cur_page = (d->active_page.write_count > d->backup_page.write_count) ? addr + d->p_size : addr;
    if (!param_find(d, DEV_SETTS_PARAM_CLOSE)) {
        d->cur_page = (d->cur_page == addr) ? addr + d->p_size : addr;
        dev_setts__reinit_page(id); // init dev_setts
    }
    dev_setts__init_tbl(d);
    return EVENT__OK;
}




/*******************************************************************************
 * Функция записи параметра во флеш.
 ******************************************************************************/
events__e dev_setts__set(uint8_t id, dev_setts__params_e param, uint8_t* buff, uint16_t len) {
    if (id >= DEV_SETTS__NUM)
        return EVENT__PARAM_NA;
    dev_setts__data_t* d = &data_tbl[id];
    if ((param >= d->param_max) || (d->p_tbl == NULL) || (!len) || (len > DEV_PARAM_MAX_LEN) || (d->func == NULL))
        return EVENT__PARAM_NA;
    uint32_t addr = param_find(d, DEV_SETTS_PARAM_NONE); // Поиск последней записи
    if ((!addr) || (addr + 8 + len >= MAX_MEM_ADDR))
        addr = (uint32_t)dev_setts__reinit_page(id);
    dev_setts__write_param(id, d->dev_id, addr, param, buff, len);
    *(d->p_tbl + param) = addr;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция записи параметра во флеш.
 ******************************************************************************/
static uint16_t dev_setts__write_param(uint8_t id, uint8_t dev_id, uint32_t addr, dev_setts__params_e param, uint8_t* buff, uint16_t len) {
    dev_setts__data_t* d = &data_tbl[id];
    dev_setts__t msg_hdr;
    uint8_t wire_len = len % 8 ? 8 - len % 8 : 0; // Выравнивание по 8 байт
    msg_hdr.hdr.len = len + sizeof(dev_setts__t) + wire_len;
    msg_hdr.hdr.param_len = len;
    msg_hdr.hdr.param_id = param;
    msg_hdr.crc = crc__8_ccitt((uint8_t*)&msg_hdr.hdr, sizeof(msg_hdr.hdr));
    msg_hdr.crc = crc__8_ccitt_thread(buff, len, msg_hdr.crc);
    d->func(dev_id, FLASH__OP_WRITE, addr, (uint8_t*)&msg_hdr, sizeof(dev_setts__t), NULL, NULL);
    d->func(dev_id, FLASH__OP_WRITE, addr + sizeof(dev_setts__t), buff, len, NULL, NULL);
    return msg_hdr.hdr.len;
}

/*******************************************************************************
 * Функция чтения параметра из флеш.
 ******************************************************************************/
uint32_t dev_setts__get(uint8_t id, dev_setts__params_e param, uint8_t* buff) {
    if (id >= DEV_SETTS__NUM)
        return EVENT__PARAM_NA;
    dev_setts__data_t* d = &data_tbl[id];
    uint32_t* p_addr = d->p_tbl + param;
    if ((param >= d->param_max) || (d->p_tbl == NULL) || (*p_addr == 0) || (d->func == NULL))
        return 0;
    d->func(d->dev_id, FLASH__OP_READ, *p_addr, d->buff, sizeof(dev_setts__t), NULL, NULL);
    if (buff)
        d->func(d->dev_id, FLASH__OP_READ, *p_addr + sizeof(dev_setts__t), buff, d->param.hdr.param_len, NULL, NULL);
    return d->param.hdr.param_len;
}


/*******************************************************************************
 * Функция поиска параметра во флеш.
 ******************************************************************************/
static uint32_t param_find(dev_setts__data_t* d, dev_setts__params_e param) {
    dev_setts__t cur_param;
    uint32_t prev_addr = 0, cur_addr = d->cur_page + sizeof(dev_setts__struct__t), last_len = 0;
    d->func(d->dev_id, FLASH__OP_READ, cur_addr, (uint8_t*)&cur_param, sizeof(dev_setts__t), NULL, NULL);
    while((cur_addr < MAX_MEM_ADDR) && (cur_param.hdr.param_id != DEV_SETTS_PARAM_NONE)) {
        if ((cur_addr + cur_param.hdr.len > MAX_MEM_ADDR) || (!cur_param.hdr.len)) {
             cur_addr = 0;
             break;
        }
        if (cur_param.hdr.param_id == param) {
            d->func(d->dev_id, FLASH__OP_READ, cur_addr, d->buff, sizeof(dev_setts__t) + cur_param.hdr.param_len, NULL, NULL);
            if (d->param.crc == crc__8_ccitt((uint8_t*)&d->param.hdr, sizeof(d->param.hdr) + d->param.hdr.param_len))
                prev_addr = cur_addr;
        }
        cur_addr += cur_param.hdr.len;
        last_len = cur_param.hdr.len;
        d->func(d->dev_id, FLASH__OP_READ, cur_addr, (uint8_t*)&cur_param, sizeof(dev_setts__t), NULL, NULL);
    }

    //todo
    if ((param == DEV_SETTS_PARAM_NONE) && (cur_addr) && (last_len)) {
        d->func(d->dev_id, FLASH__OP_READ, cur_addr - last_len, d->buff, last_len, NULL, NULL);
        if (d->param.crc == crc__8_ccitt((uint8_t*) &d->param.hdr, sizeof(d->param.hdr) + d->param.hdr.param_len))
            prev_addr = cur_addr;
    }
    return prev_addr;
}

/*******************************************************************************
 * Функция очистки и копирования параметров во флеш.
 ******************************************************************************/
static uint32_t dev_setts__reinit_page(uint8_t id) {
    dev_setts__data_t* d = &data_tbl[id];
    dev_setts__struct__t *prepare_page = &d->backup_page;
    uint32_t backup_addr = d->addr;
    d->func(d->dev_id, FLASH__OP_READ, d->addr, (uint8_t*)&d->active_page, sizeof(dev_setts__struct__t), NULL, NULL);
    d->func(d->dev_id, FLASH__OP_READ, d->addr + d->p_size, (uint8_t*)&d->backup_page, sizeof(dev_setts__struct__t), NULL, NULL);
    if (d->cur_page == d->addr) {
        backup_addr = d->addr + d->p_size;
        prepare_page = &d->active_page;
    }
    prepare_page->write_count--;
    d->func(d->dev_id, FLASH__OP_ERASE, backup_addr, NULL, d->p_size, NULL, NULL);
    d->func(d->dev_id, FLASH__OP_WRITE, backup_addr, (uint8_t*)prepare_page, sizeof(dev_setts__struct__t), NULL, NULL);
    uint32_t cur_wr_param = backup_addr + sizeof(dev_setts__struct__t);
    for(dev_setts__params_e param_id = DEV_SETTS_PARAM_NONE; ++param_id < d->param_max; )
        if (param_find(d, param_id)) {
            d->func(d->dev_id, FLASH__OP_WRITE, cur_wr_param, d->buff, d->param.hdr.len, NULL, NULL);
            cur_wr_param += d->param.hdr.len;
        }
    dev_setts__write_param(id, d->dev_id, cur_wr_param, DEV_SETTS_PARAM_CLOSE, (uint8_t*)prepare_page, sizeof(dev_setts__struct__t));
    d->cur_page = backup_addr;
    dev_setts__init_tbl(d);
    return param_find(d, DEV_SETTS_PARAM_NONE);
}

/*******************************************************************************
 * Функция очистки модуля. Стирает диапазон адресов во флеш.
 ******************************************************************************/
void dev_setts__clear(uint8_t id) {
    if (id >= DEV_SETTS__NUM)
        return;
    dev_setts__data_t* d = &data_tbl[id];
    if (d->func == NULL)
        return;
    d->cur_page = d->addr + d->p_size;
    d->func(d->dev_id, FLASH__OP_ERASE, d->addr, NULL, d->p_size * 2, NULL, NULL);
    dev_setts__reinit_page(id);
}

/*******************************************************************************
 * Функция инициализации таблицы параметров модуля.
 ******************************************************************************/
static void dev_setts__init_tbl(dev_setts__data_t* d) {
    dev_setts__params_e param_id = DEV_SETTS_PARAM_NONE;
    if (d->p_tbl) {
        memset(d->p_tbl, 0, sizeof(uint32_t) * d->param_max);
        while (++param_id < d->param_max)
            *(d->p_tbl + param_id) = param_find(d, param_id);
    }
}
