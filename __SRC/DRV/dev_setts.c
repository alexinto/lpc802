﻿/***************************************************************************//**
 * @file dev_setts.c.
 * @brief Модуль, реализующий функции работы с настройками в энергонезависимой памяти.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/crc.h"
#include "DRV/dev_setts.h"

#define DEV_SETTS_PARAM_CLOSE  0xFFFFFFFE
#define DEV_SETTS_PARAM_NONE   0xFFFFFFFF


// Максимальная длина одного параметра, байт
#ifndef DEV_PARAM_MAX_LEN
    #define DEV_PARAM_MAX_LEN       128
#endif
// Количество экземпляров модуля настроек
#ifndef DEV_SETTS__NUM
    #define DEV_SETTS__NUM 1
#endif

#define MAX_MEM_ADDR (d->cur_page + d->p_size - sizeof(dev_setts__t))
// Выравнивание в памяти, байт
#define DEV_SETTS__ALIGN  8

#pragma pack(push, 1)
typedef struct {
    u32 write_count;
    u32 page_addr;
}dev_setts__struct__t;

typedef struct {
    union {
        u8 crc;
        u8 reserved[DEV_SETTS__ALIGN];
    };
    struct {
        u16 len;
        u16 param_len;
        u32 param_id;
    }hdr;
}dev_setts__t;
#pragma pack(pop)

typedef struct {
    u32 dev_id, cur_page, addr, p_size, param_max;
    u32* p_tbl;                                        // Указатель на таблицу параметров
    union {
        dev_setts__t desc;
        u8 buff[DEV_PARAM_MAX_LEN + sizeof(dev_setts__t)];
    }p;
    dev_setts__struct__t active_page, backup_page;
    flash__exec_t func;
}dev_setts__data_t;

static dev_setts__data_t data_tbl[DEV_SETTS__NUM];

static u32 param_find(dev_setts__data_t* d, u32 param);
static u32 dev_setts__reinit_page(u8 id);
static u16 dev_setts__write_param(u8 id, u8 dev_id, u32 addr, u32 param, u8* buff, u16 len);
static void dev_setts__init_tbl(dev_setts__data_t* d);
//static void dev_setts__init_cb(u8 dev_id, flash__oper_e oper, events__e event, u32 addr, u8 *buff, u32 len, void* ext_data);

/*******************************************************************************
 * Функция инициализации настроек.
 * Вычитывает параметры из флеш-памяти, строит таблицу индексов для быстрой работы с параметрами.
 ******************************************************************************/
events__e dev_setts__init(u8 id, u8 dev_id, u32 addr, u32 size, u32 param_max, flash__exec_t func) {
    if (id >= DEV_SETTS__NUM)
        return EVENT__PARAM_NA;
    dev_setts__data_t* d = &data_tbl[id];
    d->func = func;
    d->dev_id = dev_id;
    d->addr = addr;
    d->p_size = size / 2;
    d->param_max = param_max;
    if (!d->p_tbl)
        d->p_tbl = malloc(sizeof(u32) * d->param_max);
    events__e res = d->func(d->dev_id, FLASH__OP_READ, addr, (u8*)&d->active_page, sizeof(dev_setts__struct__t), NULL, NULL);
    if (res == EVENT__OK)
        res = d->func(d->dev_id, FLASH__OP_READ, addr + d->p_size, (u8*)&d->backup_page, sizeof(dev_setts__struct__t), NULL, NULL);
    if (res == EVENT__OK) {
        d->cur_page = (d->active_page.write_count > d->backup_page.write_count) ? addr + d->p_size : addr;
        if (!param_find(d, DEV_SETTS_PARAM_CLOSE)) {
            d->cur_page = (d->cur_page == addr) ? addr + d->p_size : addr;
            dev_setts__reinit_page(id); // init dev_setts
        }
        dev_setts__init_tbl(d);
    }
    return res;
}

/*******************************************************************************
 * Функция записи параметра во флеш.
 ******************************************************************************/
events__e dev_setts__set(u8 id, u32 param, u8* buff, u16 len) {
    dev_setts__data_t* d = &data_tbl[id];
    if ((id >= DEV_SETTS__NUM) || (param >= d->param_max) || (d->p_tbl == NULL) || (!len) || (len > DEV_PARAM_MAX_LEN) || (d->func == NULL))
        return EVENT__PARAM_NA;
    u32 addr = param_find(d, DEV_SETTS_PARAM_NONE); // Поиск последней записи
    if ((!addr) || (addr + DEV_SETTS__ALIGN + len >= MAX_MEM_ADDR))
        addr = (u32)dev_setts__reinit_page(id);
    u16 param_len = dev_setts__write_param(id, d->dev_id, addr, param, buff, len);
    *(d->p_tbl + param) = addr;
    return param_len ? EVENT__OK : EVENT__ERROR;
}

/*******************************************************************************
 * Функция записи параметра во флеш.1
 ******************************************************************************/
static u16 dev_setts__write_param(u8 id, u8 dev_id, u32 addr, u32 param, u8* buff, u16 len) {
    u8 add_buff[DEV_SETTS__ALIGN];
    dev_setts__data_t* d = &data_tbl[id];
    dev_setts__t msg_hdr;
    u8 extra_len = len % DEV_SETTS__ALIGN; // Лишние байты
    u8 add_len = 0; // Выравнивание по DEV_SETTS__ALIGN байт
    if (extra_len) {
        add_len = DEV_SETTS__ALIGN - extra_len;
        memcpy(add_buff, &buff[len - extra_len], extra_len);
    }
    msg_hdr.hdr.len = len + sizeof(dev_setts__t) + add_len;
    msg_hdr.hdr.param_len = len;
    msg_hdr.hdr.param_id = param;
    msg_hdr.crc = crc__8_ccitt((u8*)&msg_hdr.hdr, sizeof(msg_hdr.hdr));
    msg_hdr.crc = crc__8_ccitt_thread(buff, len, msg_hdr.crc);
    events__e res = d->func(dev_id, FLASH__OP_WRITE, addr, (u8*)&msg_hdr, sizeof(dev_setts__t), NULL, NULL);
    if (res == EVENT__OK) {
        if (len - extra_len)
            res = d->func(dev_id, FLASH__OP_WRITE, addr + sizeof(dev_setts__t), buff, len - extra_len, NULL, NULL);
        if ((res == EVENT__OK) && (extra_len))
            res = d->func(dev_id, FLASH__OP_WRITE, addr + sizeof(dev_setts__t) + len - extra_len, add_buff, DEV_SETTS__ALIGN, NULL, NULL);
    }
    return (res == EVENT__OK) ? msg_hdr.hdr.len : 0;
}

/*******************************************************************************
 * Функция чтения параметра из флеш.
 ******************************************************************************/
u32 dev_setts__get(u8 id, u32 param, u8* buff) {
    dev_setts__data_t* d = &data_tbl[id];
    if ((id >= DEV_SETTS__NUM) || (param >= d->param_max) || (d->p_tbl == NULL) || (*(d->p_tbl + param) == 0) || (d->func == NULL))
        return 0;
    u32* p_addr = d->p_tbl + param;
    events__e res = d->func(d->dev_id, FLASH__OP_READ, *p_addr, d->p.buff, sizeof(dev_setts__t), NULL, NULL);
    if ((res != EVENT__OK) || (d->p.desc.hdr.param_len > DEV_PARAM_MAX_LEN) || (d->p.desc.hdr.param_id != param))
        d->p.desc.hdr.param_len = 0;
    else if (buff)
        d->func(d->dev_id, FLASH__OP_READ, *p_addr + sizeof(dev_setts__t), buff, d->p.desc.hdr.param_len, NULL, NULL);
    return d->p.desc.hdr.param_len;
}


/*******************************************************************************
 * Функция поиска параметра во флеш.
 ******************************************************************************/
static u32 param_find(dev_setts__data_t* d, u32 param) {
    dev_setts__t cur_param;
    u32 prev_addr = 0, cur_addr = d->cur_page + sizeof(dev_setts__struct__t), last_len = 0;

    d->func(d->dev_id, FLASH__OP_READ, cur_addr, (u8*)&cur_param, sizeof(dev_setts__t), NULL, NULL);
    while((cur_addr < MAX_MEM_ADDR) && (cur_param.hdr.param_id != DEV_SETTS_PARAM_NONE)) {
        if ((cur_addr + cur_param.hdr.len > MAX_MEM_ADDR) || (!cur_param.hdr.len)) {
             cur_addr = 0;
             break;
        }
        if ((cur_param.hdr.param_id == param) && (cur_param.hdr.param_len <= DEV_PARAM_MAX_LEN)) {
            d->func(d->dev_id, FLASH__OP_READ, cur_addr, d->p.buff, sizeof(dev_setts__t) + cur_param.hdr.param_len, NULL, NULL);
            if (d->p.desc.crc == crc__8_ccitt((u8*)&d->p.desc.hdr, sizeof(d->p.desc.hdr) + d->p.desc.hdr.param_len))
                prev_addr = cur_addr;
        }
        cur_addr += cur_param.hdr.len;
        last_len = cur_param.hdr.len;
        d->func(d->dev_id, FLASH__OP_READ, cur_addr, (u8*)&cur_param, sizeof(dev_setts__t), NULL, NULL);
    }

    //todo
    if ((param == DEV_SETTS_PARAM_NONE) && (cur_addr) && (last_len) && (last_len <= DEV_PARAM_MAX_LEN)) {
        d->func(d->dev_id, FLASH__OP_READ, cur_addr - last_len, d->p.buff, last_len, NULL, NULL);
        if (d->p.desc.crc == crc__8_ccitt((u8*) &d->p.desc.hdr, sizeof(d->p.desc.hdr) + d->p.desc.hdr.param_len))
            prev_addr = cur_addr;
    }
    return prev_addr;
}

/*******************************************************************************
 * Функция очистки и копирования параметров во флеш.
 ******************************************************************************/
static u32 dev_setts__reinit_page(u8 id) {
    dev_setts__data_t* d = &data_tbl[id];
    dev_setts__struct__t *prepare_page = &d->backup_page;
    u32 backup_addr = d->addr;
    d->func(d->dev_id, FLASH__OP_READ, d->addr, (u8*)&d->active_page, sizeof(dev_setts__struct__t), NULL, NULL);
    d->func(d->dev_id, FLASH__OP_READ, d->addr + d->p_size, (u8*)&d->backup_page, sizeof(dev_setts__struct__t), NULL, NULL);
    if (d->cur_page == d->addr) {
        backup_addr = d->addr + d->p_size;
        prepare_page = &d->active_page;
    }
    prepare_page->write_count--;
    d->func(d->dev_id, FLASH__OP_ERASE, backup_addr, NULL, d->p_size, NULL, NULL);
    d->func(d->dev_id, FLASH__OP_WRITE, backup_addr, (u8*)prepare_page, sizeof(dev_setts__struct__t), NULL, NULL);
    u32 cur_wr_param = backup_addr + sizeof(dev_setts__struct__t);
    for(u32 param_id = DEV_SETTS_PARAM_NONE; ++param_id < d->param_max; )
        if (param_find(d, param_id)) {
            d->func(d->dev_id, FLASH__OP_WRITE, cur_wr_param, d->p.buff, d->p.desc.hdr.len, NULL, NULL);
            cur_wr_param += d->p.desc.hdr.len;
        }
    dev_setts__write_param(id, d->dev_id, cur_wr_param, DEV_SETTS_PARAM_CLOSE, (u8*)prepare_page, sizeof(dev_setts__struct__t));
    d->cur_page = backup_addr;
    dev_setts__init_tbl(d);
    return param_find(d, DEV_SETTS_PARAM_NONE);
}

/*******************************************************************************
 * Функция очистки модуля. Стирает диапазон адресов во флеш.
 ******************************************************************************/
void dev_setts__clear(u8 id) {
    if (id >= DEV_SETTS__NUM)
        return;
    dev_setts__data_t* d = &data_tbl[id];
    if (d->func == NULL)
        return;
    d->cur_page = d->addr + d->p_size;
    d->func(d->dev_id, FLASH__OP_ERASE, d->addr, NULL, d->p_size * 2, NULL, NULL);
    dev_setts__reinit_page(id);
}

/*******************************************************************************
 * Функция инициализации таблицы параметров модуля.
 ******************************************************************************/
static void dev_setts__init_tbl(dev_setts__data_t* d) {
    u32 param_id = DEV_SETTS_PARAM_NONE;
    if (d->p_tbl) {
        memset(d->p_tbl, 0, sizeof(u32) * d->param_max);
        while (++param_id < d->param_max)
            *(d->p_tbl + param_id) = param_find(d, param_id);
    }
}
