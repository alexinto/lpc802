/***************************************************************************//**
 * @file max7219.h.
 * @brief ������ ������ � ������������ �������
 * @authors a.tushentsov.
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "target.h"
#include "System/sw_timer.h"
#include "DRV/spi.h"
#include "DRV/max7219.h"

#define MAX7219__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_750_KBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB)

#ifndef MAX7219__COUNT
#define MAX7219__COUNT 1
#endif

typedef struct {
    uint8_t busy;
    uint8_t acc;
    uint8_t cur_seg;
    int dev_id;
    uint8_t spi_pkt[2];
    uint8_t data[8];
    uint8_t cur_buff[16];
    max7219__cb_t cb;
    void* ext_data;
    uint16_t* cur_ptr;
}max7219__struct_t;

static max7219__struct_t max7219__data[MAX7219__COUNT];

static const uint16_t init_tbl[] = {0x000C, 0x000F, 0xFF09, 0x070B, 0x010C, 0x0000};

static void max7219__init_cb(int dev_id, events__e code, uint8_t *buff, int len, void *ext_data);
static void max7219__cb(int dev_id, events__e code,uint8_t *buff, int len, void *ext_data);


events__e max7219__init(int id, int dev_id, max7219__cb_t cb, void* ext_data) {
    if (id >= MAX7219__COUNT)
        return EVENT__PARAM_NA;
    max7219__struct_t* d = &max7219__data[id];
    d->dev_id = dev_id;
    d->cur_ptr = (uint16_t*)init_tbl;
    d->cb = cb;
    d->ext_data = ext_data;
    spi__init(d->dev_id, MAX7219__SPI_SETTINGS);
    return spi__txrx(d->dev_id, NULL, 0, (uint8_t*)d->cur_ptr, 2, max7219__init_cb, (void*)d);
}

static void max7219__init_cb(int dev_id, events__e code, uint8_t *buff, int len, void *ext_data) {
    max7219__struct_t* d = (max7219__struct_t*)ext_data;
    if (*++d->cur_ptr != 0) {
        spi__txrx (d->dev_id, NULL, 0, (uint8_t*)d->cur_ptr, 2, max7219__init_cb, (void*)d);
    }
    else if (d->cb)
        d->cb(d - max7219__data, code, d->ext_data);
}

events__e max7219__set(int id, char* buff, max7219__cb_t cb, void* ext_data) {
    max7219__struct_t* d = &max7219__data[id];
    if (d->busy)
        return EVENT__BUSY;
    int buff_len = strlen(buff);
    if (buff_len > sizeof(d->cur_buff))
        return EVENT__PARAM_NA;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->cur_seg = 1;
    char* p_buff;
    p_buff = buff + buff_len - 1;
    memcpy(d->cur_buff, buff, buff_len);
    uint8_t dot = 0;
    for(int i = 0; i < 8; i++) {
        if (p_buff < buff) {
            d->data[i] = 0x0F;              // blank
            continue;
        }
        if (*p_buff == '.') {
            i--;
            dot = 1;
        }
        else {
            d->data[i] = dot << 7;
            if ((*p_buff < '0') || (*p_buff > '9'))
                d->data[i] |= 0x0F;              // blank
            else
                d->data[i] |= *p_buff - '0';     // digit
            dot = 0;
        }
        p_buff--;
    }
    d->spi_pkt[1] = d->data[d->cur_seg - 1];
    d->spi_pkt[0] = d->cur_seg;
    return spi__txrx (d->dev_id, NULL, 0, d->spi_pkt, 2, max7219__cb, (void*)d);
}


static void max7219__cb(int dev_id, events__e code, uint8_t *buff, int len, void *ext_data) {
    max7219__struct_t* d = ext_data;
    if ((++d->cur_seg < 9) && (code == EVENT__OK)) {
        d->spi_pkt[1] = d->data[d->cur_seg - 1];
        d->spi_pkt[0] = d->cur_seg;
        if ((code = spi__txrx (d->dev_id, NULL, 0, d->spi_pkt, 2, max7219__cb, (void*)d)) == EVENT__OK)
            return;
    }
    d->busy = 0;
    d->cb(d - max7219__data, code, d->ext_data);
}

char* max7219__get(int id) {
    max7219__struct_t* d = &max7219__data[id];
    return (char*)d->cur_buff;
}
