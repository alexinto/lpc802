/***************************************************************************//**
 * @file key_code.c.
 * @brief Модуль управления аппаратной клавиатурой по uart.
 * @author a.tushentsov
 ******************************************************************************/
#include <windows.h>
#include <utility.h>
#include "cvi_uart.h"
#include "DRV/KBD/key_code.h"

typedef struct {
	key_code__e key;
	char* key_name;
}key_code__descr_tbl_t;

static const key_code__descr_tbl_t key_code__descr_tbl[] = {
	{key_none, "key_none"},	{key_none, NULL},	{key_none, NULL},	{key_none, NULL},	{key_a, "a"},	{key_b, "b"},	{key_c, "c"},	{key_d, "d"},	{key_e, "e"},	{key_f, "f"},	{key_g, "g"},	{key_h, "h"},
	{key_i, "i"},	{key_j, "j"},	{key_k, "k"},	{key_l, "l"},	{key_m, "m"},	{key_n, "n"},	{key_o, "o"},	{key_p, "p"},	{key_q, "q"},	{key_r, "r"},	{key_s, "s"},	{key_t, "t"},	{key_u, "u"},
	{key_v, "v"},	{key_w, "w"},	{key_x, "x"},	{key_y, "y"},	{key_z, "z"},	{key_1, "1"},	{key_2, "2"},	{key_3, "3"},	{key_4, "4"},	{key_5, "5"},	{key_6, "6"},	{key_7, "7"},	{key_8, "8"},
	{key_9, "9"},	{key_0, "0"},	{key_enter, "enter"},	{key_esc, "esc"},	{key_del, "del"},	{key_tab, "tab"},	{key_space, "space"},	{key_tire, "tire"},	{key_equal, "="},	{key_ls, "ls"},	{key_rs, "rs"},
	{key_slash, "slash"},	{key_ntire, "ntire"},	{key_ddot, "ddot"},	{key_tilda, "tilda"},	{key_grave, "grave"},	{key_ndot, "ndot"},	{key_dot, "dot"},	{key_slash, "slash"},	{key_cslck, "cslck"},
	{key_F1, "F1"},	{key_F2, "F2"},	{key_F3, "F3"},	{key_F4, "F4"},	{key_F5, "F5"},	{key_F6, "F6"},	{key_F7, "F7"},	{key_F8, "F8"},	{key_F9, "F9"},	{key_F10, "F10"},	{key_F11, "F11"},	{key_F12, "F12"},
	{key_prnscr, "prnscr"},	{key_slock, "slock"},	{key_pause, "pause"},	{key_insert, "insert"},	{key_home, "home"},	{key_pageup, "pageup"},	{key_delete, "delete"},	{key_end, "end"},	{key_pagedn, "pagedn"},
	{key_right, "right"},	{key_left, "left"},	{key_down, "down"},	{key_up, "up"},	{kpad_num, "num"},	{kpad_slsh, "slsh"},	{kpad_star, "star"},	{kpad_tire, "tire"},	{kpad_min, "min"},	{kpad_entr, "entr"},
	{kpad_1, "1"},	{kpad_2, "2"},	{kpad_3, "3"},	{kpad_4, "4"},	{kpad_5, "5"},	{kpad_6, "6"},	{kpad_7, "7"},	{kpad_8, "8"},	{kpad_9, "9"},	{kpad_0, "0"},	{kpad_dot, "dot"},	{key_n_US, "n_US"},
};

static struct {
	key_code__struct_t* keys;
	int init;
	events__e event;
}k_data;


static uint8_t key_code__uart_cb(events__e event, uint8_t* data, uint32_t len);
static int CVICALLBACK uart_thread(void *functionId);

events__e key_code__init(uint8_t itf) {
	if (!k_data.init) {
		uart_thread(NULL);
	}
	uart__deinit();
	uart__init(itf, key_code__uart_cb);
	uart__cout(UART__INIT, 0, 0);
	return EVENT__OK;
}

char* key_code_get_name(key_code__e key) {
	if (key >= (sizeof(key_code__descr_tbl) / sizeof(key_code__descr_tbl[0])))
		return NULL;
	return key_code__descr_tbl[key].key_name;
}

events__e key_code__set(key_code__struct_t* keys) {
	k_data.event = EVENT__CONTINUE;
	k_data.keys = keys;
	while (uart__cout(UART__TX, sizeof(key_code__struct_t), (char*)keys) != EVENT__OK) {Sleep(1);}
	while (k_data.event == EVENT__CONTINUE) Sleep(1);
	return k_data.event;
}


void key_code__deinit() {
	uart__deinit();
	uart_thread(NULL);
}


uint8_t key_code__uart_cb(events__e event, uint8_t* data, uint32_t len) {
	if (event == EVENT__OK) {
		if (memcmp(k_data.keys, data, sizeof(key_code__struct_t)))
			event = EVENT__HW_ERROR;
	}
	k_data.event = event;
	return 0;
}

static int CVICALLBACK uart_thread(void *functionId) {
	if(functionId == NULL) { // Srart/Stop
		volatile static int static__functionId = -1;
		if(static__functionId == -1)
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, uart_thread, &static__functionId, &static__functionId);
		else
			for(static__functionId = 0; static__functionId !=-1; Sleep(100));
		return 0;
	}
	while(*(int*)functionId)
		Sleep(1), uart__cout(0, 0, NULL);
	*(int*)functionId = -1;
	return 0;
}
