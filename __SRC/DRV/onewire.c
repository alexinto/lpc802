﻿/***************************************************************************//**
 * @file onewire.c.
 * @brief Файл, реализующий протокол 1wire.
 * @author a.tushentsov.
 ******************************************************************************/
#include "string.h"
#include "target.h"
#include "System/fstandard.h"
#include "DRV/onewire.h"

#ifndef ONEWIRE__CNT
#define ONEWIRE__CNT 1
#endif


#define ONEWIRE_UART_RXTX_SET (UART__BR_115200 | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FC_NONE | UART__PIN_MODE)
#define ONEWIRE_UART_RESET_SET (UART__BR_9600 | UART__PAR_NONE | UART__STOP_1 | UART__DATA_8 | UART__FC_NONE | UART__PIN_MODE)

typedef enum {
    ONEWIRE__STATE_IDLE = 0,
    ONEWIRE__STATE_INIT,
    ONEWIRE__STATE_TX,
    ONEWIRE__STATE_RX,
    ONEWIRE__STATE_CMD_FIND,
    ONEWIRE__STATE_FIND,
}onewire__state_e;

typedef struct {
    uart__sub_t uart_sub;
    events__e event;
    i32 uart_id;
    u8* tx_buff, *rx_buff, byte_tx[16], byte_rx[16];
    i32 tx_len, rx_len, bytes_cnt;
    onewire__cb_t cb;
    void* ext_data;
    onewire__state_e state;
    u8 cb_cnt;          // костыль- счетчик коллбэков
}onewire__struct_t;

static onewire__struct_t onewire__data[ONEWIRE__CNT];

static void onewire__uart_eh(struct uart__sub_t* sub, events__e event, void* ext_data);
static void onewire__uart_cb(uart__sub_t* sub, events__e event, u8* buff, i32 len, void* ext_data);
static events__e onewire__exec(onewire__struct_t* data);


events__e onewire__init(u8 id, u8 uart_id) {
    onewire__struct_t* data = &onewire__data[id];
    if (id >= ONEWIRE__CNT)
        return EVENT__PARAM_NA;
    data->uart_id = uart_id;
    data->event = EVENT__OK;
    data->bytes_cnt = 0;
    data->state = ONEWIRE__STATE_IDLE;
    return EVENT__OK;
}


events__e onewire__reset(u8 id, onewire__cb_t cb, void* ext_data) {
    onewire__struct_t* data = &onewire__data[id];
    if (id >= ONEWIRE__CNT)
        return EVENT__PARAM_NA;
    if (data->state != ONEWIRE__STATE_IDLE)
        return EVENT__BUSY;
    data->state = ONEWIRE__STATE_INIT;
    data->event = EVENT__OK;
    data->bytes_cnt = 0;
    data->cb = cb;
    data->ext_data = ext_data;
    return uart__open(&data->uart_sub, data->uart_id, ONEWIRE_UART_RESET_SET, onewire__uart_eh, data);
}

events__e onewire__txrx(u8 id, u8* tx_buff, i32 tx_len, u8* rx_buff, i32 rx_len, onewire__cb_t cb, void* ext_data) {
    onewire__struct_t* data = &onewire__data[id];
    if (id >= ONEWIRE__CNT)
        return EVENT__PARAM_NA;
    if (data->state != ONEWIRE__STATE_IDLE)
        return EVENT__BUSY;
    data->event = EVENT__OK;
    data->tx_buff = tx_buff;
    data->tx_len = tx_len;
    data->rx_buff = rx_buff;
    data->rx_len = rx_len;
    data->bytes_cnt = 0;
    data->cb = cb;
    data->ext_data = ext_data;
    if (tx_buff) {
        for(i32 i = 0; i < 8; i++)
            data->byte_tx[i] = (data->tx_buff[data->bytes_cnt] & (1 << i)) ? 0xFF : 0x00;
        data->state = ONEWIRE__STATE_TX;
    }
    else {
        for(i32 i = 0; i < 8; i++)
            data->byte_tx[i] = 0xFF;
        data->state = ONEWIRE__STATE_RX;
    }
    if (rx_buff)
        memset(rx_buff, 0, rx_len);
    return uart__open(&data->uart_sub, data->uart_id, ONEWIRE_UART_RXTX_SET, onewire__uart_eh, data);
}

events__e onewire__search(u8 id, u8 cmd, uint64_t* uuid, u8 pos, onewire__cb_t cb, void* ext_data) {
    onewire__struct_t* data = &onewire__data[id];
    if ((id >= ONEWIRE__CNT) || (!uuid) || (pos >= 64))
        return EVENT__PARAM_NA;
    if (data->state != ONEWIRE__STATE_IDLE)
        return EVENT__BUSY;
    if (cmd) {
        data->state = ONEWIRE__STATE_CMD_FIND;
        for(i32 i = 0; i < 8; i++)
            data->byte_tx[i] = (cmd & (1 << i)) ? 0xFF : 0x00;
        data->byte_tx[8] = data->byte_tx[9] = 0xFF;
        data->bytes_cnt = 0;
    }
    else {
        data->state = ONEWIRE__STATE_FIND;
        data->byte_tx[1] = data->byte_tx[2] = 0xFF;
        data->bytes_cnt = pos;
    }
    data->event = EVENT__OK;
    data->rx_buff = (u8*)uuid;
    data->cb = cb;
    data->ext_data = ext_data;
    return uart__open(&data->uart_sub, data->uart_id, ONEWIRE_UART_RXTX_SET, onewire__uart_eh, data);
}

static void onewire__uart_eh(struct uart__sub_t* sub, events__e event, void* ext_data) {
    onewire__struct_t* data = (onewire__struct_t*)sub;
    switch(event) {
    case EVENT__OPEN:
        event = onewire__exec(data);
        break;
    default:
        event = EVENT__ERROR;
    case EVENT__CLOSE:
        break;
    }
    if (event != EVENT__OK) {
        if (event != EVENT__CLOSE)
            data->event = event;
        data->state = ONEWIRE__STATE_IDLE;
        if (data->cb)
            data->cb((u8)(data - onewire__data), data->event, data->rx_buff, data->bytes_cnt, data->ext_data);
    }
}

static void onewire__uart_cb(uart__sub_t* sub, events__e event, u8* buff, i32 len, void* ext_data) {
    onewire__struct_t* data = (onewire__struct_t*)sub;
    if (event == EVENT__OK) {
        if (++data->cb_cnt < 2)
            return;
        switch(data->state) {
            case ONEWIRE__STATE_INIT:
                if (data->byte_tx == data->byte_rx)
                    data->event = EVENT__TIMEOUT;
                uart__close(sub);
                break;
            case ONEWIRE__STATE_RX:
                for(i32 i = 0; i < 8; i++) {
                    if (data->byte_tx[i] == data->byte_rx[i])    // todo Как быть с "мусорным" байтом?
                        data->rx_buff[data->bytes_cnt] |= 1 << i;
                }
                data->bytes_cnt++;
                if (data->rx_len <= data->bytes_cnt) {
                    uart__close(sub);
                    break;
                }
                event = onewire__exec(data);
                break;
            case ONEWIRE__STATE_TX:
                data->bytes_cnt++;
                if (data->tx_len <= data->bytes_cnt) {
                    if (data->rx_buff == NULL) {
                        uart__close(sub);
                        break;
                    }
                    data->bytes_cnt = 0;
                    for(i32 i = 0; i < 8; i++)
                        data->byte_tx[i] = 0xFF;
                    data->state = ONEWIRE__STATE_RX;
                }
                else {
                    for(i32 i = 0; i < 8; i++)
                        data->byte_tx[i] = (data->tx_buff[data->bytes_cnt] & (1 << i)) ? 0xFF : 0x00;
                }
                event = onewire__exec(data);
                break;
            case ONEWIRE__STATE_CMD_FIND:
                data->byte_rx[1] = data->byte_rx[8];
                data->byte_rx[2] = data->byte_rx[9];
                data->byte_tx[1] = data->byte_tx[2] = 0xFF;
                data->state = ONEWIRE__STATE_FIND;  // без break!!!
            case ONEWIRE__STATE_FIND:
                if ((data->byte_rx[1] == data->byte_rx[2]) || ((data->byte_rx[1] != 0xFF) && (data->byte_rx[2] != 0xFF)))
                    event = (data->byte_rx[1] == 0xFF) ? EVENT__NOT_EXIST : EVENT__FIND;
                else {
                    if (data->byte_rx[1] == 0xFF)
                        *(u64*)data->rx_buff |= ((u64)1 << data->bytes_cnt);
                    else
                        *(u64*)data->rx_buff &= ~((u64)1 << data->bytes_cnt);
                    if (data->bytes_cnt < 63)
                        event = onewire__exec(data);
                    else
                        uart__close(sub);
                }
                break;
            default:
                break;
        }
    }
    if (event != EVENT__OK) {
        data->event = event;
        uart__close(sub);
    }
}

static events__e onewire__exec(onewire__struct_t* data) {
    void* ext_data = data;
    i32 len = 8;
    switch(data->state) {
        case ONEWIRE__STATE_INIT:
            len = 1;
            data->byte_tx[0] = 0xF0;
            break;
        case ONEWIRE__STATE_CMD_FIND:
            len = 10;
            break;
        case ONEWIRE__STATE_FIND:
            len = 3;
            data->byte_tx[0] = (*(u64*)data->rx_buff & ((u64)1 << data->bytes_cnt)) ? 0xFF : 0x00;
            data->bytes_cnt++;
            break;
        case ONEWIRE__STATE_RX:
        case ONEWIRE__STATE_TX:
            break;
        default:
            return EVENT__ERROR;
    }
    data->cb_cnt = 0;
    events__e res = uart__rx(&data->uart_sub, UART__FRAME_COUNT, data->byte_rx, len, 10, onewire__uart_cb, ext_data); // 10 - таймаут ождания передачи байта, мс.
    if (res == EVENT__OK)
        res = uart__tx(&data->uart_sub, data->byte_tx, len, onewire__uart_cb, ext_data);
    return res;
}
