/***************************************************************************//**
 * @file ltc2500.c.
 * @brief Драйвер LTC2500 (аппаратно зависимая часть).
 * @authors a.tushentsov.
 ******************************************************************************/
#include <math.h>
#include "target.h"
#include "System/sw_timer.h"
#include "DRV/spi.h"
#include "DRV/LTC2500/ltc2500.h"

#ifndef LTC2500__ERR_TIMEOUT         // Таймаут ошибки, мс.
#define LTC2500__ERR_TIMEOUT 5000
#endif

// Настройки интерфейса SPI
#define LTC2500__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_6_MBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_1_CPOL_1 | SPI__MSB)

#ifndef LTC2500__COUNT
#define LTC2500__COUNT 1
#endif

#define LTC2500__NO_DEV    0xFF   // Магическая константа для определения "свободного" устройства
#define LTC2500_MEAS_TIME   20    // Минимальное время измерения, мс. todo пересчитать с учетом частоты и усреднения!!!
#define LTC2500_MEAS_K    (0.5)   // Коэффициент фильтрации Калмана

static const u8 filt_meas_cnt[] = {1, 2, 2, 3, 4, 7, 7, 7}; // Число преобразований для фильтров

#pragma pack(push, 1)
typedef struct {
    u16 df             :4;
    u16 dce            :1;
    u16 dgc            :1;
    u16 valid          :2;
    u16 reserved       :4;
    u16 flt_type       :4;
}ltc2500__tx_pkt_t;
typedef struct {
    u8 data[4];
    u8 filter_type     :4;
    u8 df              :4;
}ltc2500__cfg_pkt_t;
typedef struct {
    u8  data_h         :7;
    u8  overrange      :1;
    u8  data_m[2];
    u8  mode           :7;
    u8  data_l         :1;
}ltc2500__ch_b_pkt_t;
#pragma pack(pop)

typedef enum {
    LTC2500__OP_NONE      = 0,
    LTC2500__OP_START,
    LTC2500__OP_CHK_DONE,
    LTC2500__OP_READ_A,
    LTC2500__OP_READ_CFG,
    LTC2500__OP_CHK_TIME,
}ltc200__op_e;

typedef struct {
    double offset;                            // Смещение постоянной составляющей, В
    double vref;                              // Опорное напряжение, В
    i32 time, err_cnt;
    u8 meas_cnt;
    u8 busy;
    u8 dev_id;
    ltc2500__tx_pkt_t tx_pkt;
    union {
        ltc2500__cfg_pkt_t rx_a_pkt;
        ltc2500__ch_b_pkt_t rx_b_pkt;
    };
    union {
        ltc2500__cfg_t* cfg;
        u32* data;
    };
    ltc2500__cfg_t cur_cfg;
    ltc2500__cb_t cb;
    void* ext_data;
    ltc200__op_e* oper;
    sw_timer__t timer;
    double* value;
}ltc2500__struct_t;

static const ltc200__op_e ltc2500__read_a_tbl[] = {LTC2500__OP_START, LTC2500__OP_CHK_DONE, LTC2500__OP_READ_A, LTC2500__OP_CHK_TIME, LTC2500__OP_NONE};
static const ltc200__op_e ltc2500__read_cfg_tbl[] = {LTC2500__OP_START, LTC2500__OP_CHK_DONE, LTC2500__OP_READ_CFG, LTC2500__OP_NONE};

static ltc2500__struct_t ltc2500__data[LTC2500__COUNT];
static ltc2500__handler_t ltc2500__handler;
static u8 module__init;

static ltc2500__struct_t* ltc2500__free_get(i32 dev_id);
static void ltc2500__spi_a_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data);
static void ltc2500__spi_b_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data);
static void ltc2500__spi_cfg_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data);
static void ltc2500__spi_cfg_get_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data);
static events__e lts2500__exec(ltc2500__struct_t* d);
static void ltc2500__tmr_cb(struct sw_timer__t *timer, void *ext_data);

events__e ltc2500__init(i32 dev_id, u32 settings, double vref) {
    if (!module__init) {
        module__init = 1;
        for(i32 i = 0; i < LTC2500__COUNT; i++)
            ltc2500__data[i].dev_id = LTC2500__NO_DEV;
    }
    ltc2500__struct_t* d = ltc2500__free_get(LTC2500__NO_DEV);
    if ((!d) || (dev_id == LTC2500__NO_DEV))
        return EVENT__PARAM_NA;
    d->dev_id = dev_id;
    d->vref = vref;
    events__e res = EVENT__OK;
    if ((settings & LTC2500__SPI_NO_INIT) == 0)
        res = spi__init(dev_id, LTC2500__SPI_SETTINGS);
    return res;
}

events__e ltc2500__handler_set(ltc2500__handler_t handler) {
    ltc2500__handler = handler;
    return EVENT__OK;
}

events__e ltc2500__vref_set(i32 dev_id, double vref, double offset) {
    ltc2500__struct_t* d = ltc2500__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->vref = vref;
    d->offset = offset;
    return EVENT__OK;
}

double ltc2500__vref_get(i32 dev_id) {
    ltc2500__struct_t* d = ltc2500__free_get(dev_id);
    return (d == NULL) ? 0 : d->vref;
}

events__e ltc2500__cfg_set(i32 dev_id, ltc2500__filter_e filter, ltc2500__df_e df, u8 dgc_on, u8 dce_on, ltc2500__cb_t cb, void* ext_data) {
    ltc2500__struct_t* d = ltc2500__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->tx_pkt.valid = 0x02;  // Валидная конфигурация
    d->tx_pkt.dgc = dgc_on;
    d->tx_pkt.dce = dce_on;
    d->tx_pkt.df = df;
    d->tx_pkt.flt_type = filter;
    d->cur_cfg.df = df;
    d->cur_cfg.filter = filter;
    if (ltc2500__handler) {
        ltc2500__handler(dev_id, LTC2500__EVENT_RDLA);
        ltc2500__handler(dev_id, LTC2500__EVENT_SYNC);
        ltc2500__handler(dev_id, LTC2500__EVENT_MCLK_800); // костыль
    }
    events__e res = spi__txrx(dev_id, NULL, 0, (u8*)&d->tx_pkt, sizeof(d->tx_pkt), cb ? ltc2500__spi_cfg_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        d->busy = 0;
    }
    return res;
}

static void ltc2500__spi_cfg_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data) {
    ltc2500__struct_t* d = (ltc2500__struct_t*)ext_data;
    d->busy = 0;
    if (d->cb) {
        d->cb(dev_id, code, 0, d->ext_data);
    }
}


events__e ltc2500__cfg_get(i32 dev_id, ltc2500__cfg_t* cfg, ltc2500__cb_t cb, void* ext_data) {
    ltc2500__struct_t* d = ltc2500__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->cfg = cfg;
    d->oper = (ltc200__op_e*)ltc2500__read_cfg_tbl;
    events__e res = lts2500__exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while((d->oper) && (res == EVENT__OK)) {
            d->oper++;
            res = lts2500__exec(d);
        }
        if (res == EVENT__OK) {
            if (d->cfg) {
                d->cfg->df = d->rx_a_pkt.df;
                d->cfg->filter = d->rx_a_pkt.filter_type;
            }
            if ((d->cur_cfg.df != d->rx_a_pkt.df) || (d->cur_cfg.filter != d->rx_a_pkt.filter_type))
                res = EVENT__ERROR;
        }
        d->busy = 0;
    }
    return res;
}

static void ltc2500__spi_cfg_get_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data) {
    ltc2500__struct_t* d = (ltc2500__struct_t*)ext_data;
    d->busy = 0;
    if (d->cb) {
        if (code == EVENT__OK) {
            if (d->cfg) {
                d->cfg->df = d->rx_a_pkt.df;
                d->cfg->filter = d->rx_a_pkt.filter_type;
            }
            if ((d->cur_cfg.df != d->rx_a_pkt.df) || (d->cur_cfg.filter != d->rx_a_pkt.filter_type))
                code = EVENT__ERROR;
        }
        d->cb(dev_id, code, 0, d->ext_data);
    }
}

events__e ltc2500__read_a(i32 dev_id, double* value, i32 time, ltc2500__cb_t cb, void* ext_data) {
    ltc2500__struct_t* d = ltc2500__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->value = value;
    d->time = time;
    d->meas_cnt = 0;
    d->oper = (ltc200__op_e*)ltc2500__read_a_tbl;
    events__e res = lts2500__exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while((d->oper) && (res == EVENT__OK)) {
            d->oper++;
            res = lts2500__exec(d);
        }
        d->busy = 0;
    }
    return res;
}

static void ltc2500__spi_a_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data) {
    ltc2500__struct_t* d = (ltc2500__struct_t*)ext_data;
    i32 adc_val;
    d->oper++;
    events__e res = lts2500__exec(d);
    if ((res != EVENT__OK) || (!d->oper)) {
        adc_val = (d->rx_a_pkt.data[0] << 24) | (d->rx_a_pkt.data[1] << 16) | (d->rx_a_pkt.data[2] << 8) | d->rx_a_pkt.data[3];
        adc_val = round(d->vref * adc_val / 0xFFFFFFFF) + d->offset;
        d->busy = 0;
        if (d->cb)
            d->cb(d->dev_id, res, adc_val, d->ext_data);
    }
}

events__e ltc2500__read_b(i32 dev_id, u32* data, ltc2500__cb_t cb, void* ext_data) {
    ltc2500__struct_t* d = ltc2500__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->data = data;
    u32 adc_val;
    if (ltc2500__handler)
        ltc2500__handler(dev_id, LTC2500__EVENT_RDLB);
    events__e res = spi__txrx(dev_id, (u8*)&d->rx_b_pkt, sizeof(d->rx_b_pkt), NULL, 0, cb ? ltc2500__spi_b_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        if (res == EVENT__OK) {
            adc_val = (d->rx_b_pkt.data_h << 17) | (d->rx_b_pkt.data_m[0] << 9) | (d->rx_b_pkt.data_m[1] << 1) | d->rx_b_pkt.data_l;
            if (d->data)
                *d->data = adc_val;
        }
        d->busy = 0;
    }
    return res;
}

static void ltc2500__spi_b_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data) {
    ltc2500__struct_t* d = (ltc2500__struct_t*)ext_data;
    u32 adc_val = 0;
    d->busy = 0;
    if (d->cb) {
        if (code == EVENT__OK) {
            adc_val = (d->rx_b_pkt.data_h << 17) | (d->rx_b_pkt.data_m[0] << 9) | (d->rx_b_pkt.data_m[1] << 1) | d->rx_b_pkt.data_l;
            if (d->data)
                *d->data = adc_val;
        }
        d->cb(dev_id, code, adc_val, d->ext_data);
    }
}

static ltc2500__struct_t* ltc2500__free_get(i32 dev_id) {
    ltc2500__struct_t* d = NULL;
    for(i32 i = 0; i < LTC2500__COUNT; i++) {
        d = &ltc2500__data[i];
        if ((d->dev_id == dev_id) && (d->busy == 0))
            return d;
    }
    return NULL;
}

static events__e lts2500__exec(ltc2500__struct_t* d) {
    events__e res = EVENT__OK;
    double cur_value;
    switch(*d->oper) {
        case LTC2500__OP_START:
            d->err_cnt = LTC2500__ERR_TIMEOUT;
            if (ltc2500__handler)
                res = ltc2500__handler(d->dev_id, LTC2500__EVENT_MCLK_ON);
            if (res == EVENT__OK)
                sw_timer__start(&d->timer, 0, d->cb ? ltc2500__tmr_cb : NULL, (void*)d);
            break;
        case LTC2500__OP_CHK_DONE:
            if (ltc2500__handler)
                res = ltc2500__handler(d->dev_id, LTC2500__EVENT_GET_STATE);
            if ((res == EVENT__BUSY) && (d->err_cnt > 0)) {
                d->err_cnt--;
                d->oper--;
                res = EVENT__OK;
            }
            else {
                if (ltc2500__handler)
                    ltc2500__handler(d->dev_id, LTC2500__EVENT_MCLK_OFF);
            }
            if (res == EVENT__OK)
                sw_timer__start(&d->timer, 1, d->cb ? ltc2500__tmr_cb : NULL, (void*)d);
            break;
        case LTC2500__OP_READ_A:
            if (ltc2500__handler)
                res = ltc2500__handler(d->dev_id, LTC2500__EVENT_RDLA);
            if (res == EVENT__OK)
                res = spi__txrx(d->dev_id, (u8*)&d->rx_a_pkt.data, sizeof(d->rx_a_pkt.data), NULL, 0, d->cb ? ltc2500__spi_a_cb : NULL, (void*)d);
            break;
        case LTC2500__OP_READ_CFG:
            if (ltc2500__handler)
                res = ltc2500__handler(d->dev_id, LTC2500__EVENT_RDLA);
            if (res == EVENT__OK)
                res = spi__txrx(d->dev_id, (u8*)&d->rx_a_pkt, sizeof(d->rx_a_pkt), NULL, 0, d->cb ? ltc2500__spi_cfg_get_cb : NULL, (void*)d);
            break;
        case LTC2500__OP_CHK_TIME:
            if (d->meas_cnt < filt_meas_cnt[d->cur_cfg.filter]) {
                d->meas_cnt++;
                if ((d->meas_cnt == filt_meas_cnt[d->cur_cfg.filter]) && (d->value))
                    *d->value = d->vref * ((d->rx_a_pkt.data[0] << 24) | (d->rx_a_pkt.data[1] << 16) | (d->rx_a_pkt.data[2] << 8) | d->rx_a_pkt.data[3]) / 0xFFFFFFFF + d->offset;
                d->oper = (ltc200__op_e*)ltc2500__read_a_tbl;
                if (ltc2500__handler)
                    res = ltc2500__handler(d->dev_id, LTC2500__EVENT_MCLK_ON);
                sw_timer__start(&d->timer, 0, d->cb ? ltc2500__tmr_cb : NULL, (void*)d);
                break;
            }
            cur_value = d->vref * ((d->rx_a_pkt.data[0] << 24) | (d->rx_a_pkt.data[1] << 16) | (d->rx_a_pkt.data[2] << 8) | d->rx_a_pkt.data[3]) / 0xFFFFFFFF + d->offset;
            if (d->value)
                *d->value += LTC2500_MEAS_K * (cur_value - *d->value);
            d->time -= LTC2500_MEAS_TIME;
            if (d->time > 0) {
                d->oper = (ltc200__op_e*)ltc2500__read_a_tbl;
                if (ltc2500__handler)
                    res = ltc2500__handler(d->dev_id, LTC2500__EVENT_MCLK_ON);
            }
            sw_timer__start(&d->timer, 0, d->cb ? ltc2500__tmr_cb : NULL, (void*)d);
            break;
        default:
            d->oper = NULL;
            break;
    }
    return res;
}

static void ltc2500__tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    ltc2500__struct_t* d = (ltc2500__struct_t*)ext_data;
    d->oper++;
    events__e res = lts2500__exec(d);
    if ((res != EVENT__OK) || (!d->oper)) {
        d->busy = 0;
        if (d->cb)
            d->cb(d->dev_id, res, 0, d->ext_data);
    }
}
