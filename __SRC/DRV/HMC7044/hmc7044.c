﻿/***************************************************************************//**
 * @file hmc7044.с.
 * @brief Драйвер микросхемы HMC7044.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "DRV/HMC7044/hmc7044.h"

// Настройки интерфейса SPI
#define HMC7044__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_3_MBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB | SPI__3WIRE)

#pragma pack(push, 1)
typedef struct {
    u8 addr_hi :5;
    u8 mode    :2;
    u8 rw      :1;
    u8 addr_lo;
    u8 data;
}hmc7044__spi_t;                    // MSB first
#pragma pack (pop)

events__e hmc7044__init(i32 id) {
    return spi__init(id, HMC7044__SPI_SETTINGS);
}

events__e hmc7044__read(i32 id, u16 addr, u8* data, i32 size) {
    hmc7044__spi_t buff = {.mode = 3, .rw = 1};
    events__e res = EVENT__OK;
    for (i32 i = 0; (i < size) && (res == EVENT__OK); i++, addr++) {
        buff.addr_hi = addr >> 8;
        buff.addr_lo = addr;
        res = spi__txrx(id, (u8*)&buff, sizeof(buff), (u8*)&buff, 2, NULL, NULL);
        data[i] = buff.data;
    }
    return res;
}

events__e hmc7044__write(i32 id, u16 addr, u8* data, i32 size) {
    hmc7044__spi_t buff = {.mode = 3, .rw = 0, .addr_hi = addr >> 8};
    events__e res = EVENT__OK;
    for (i32 i = 0; (i < size) && (res == EVENT__OK); i++, addr++) {
        buff.addr_hi = addr >> 8;
        buff.addr_lo = addr;
        buff.data = data[i];
        res = spi__txrx(id, NULL, 0, (u8*)&buff, sizeof(buff), NULL, NULL);
    }
    return res;
}
