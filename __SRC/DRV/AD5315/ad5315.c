/***************************************************************************//**
 * @file ad5315.c.
 * @brief Драйвер AD5315.
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "DRV/AD5315/ad5315.h"
#include "DRV/i2c.h"

#define AD5315__I2C_SETTINGS 0

#ifndef AD5315__COUNT
#define AD5315__COUNT 1
#endif

#pragma pack(push, 1)
typedef struct {
    uint8_t dac_id;
    uint16_t data;
}ad5315__pkt_t;
#pragma pack(pop)


typedef struct {
    ad5315__pkt_t pkt;
    uint8_t busy;
    uint8_t addr;         // 7 битный адрес на шине I2C
    uint16_t data;
    int i2c_id;
    ad5315__cb_t cb;
    void* ext_data;
}ad5315__data_t;

static ad5315__data_t ad5315__data[AD5315__COUNT];

static void ad5315__init_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);
static void ad5315__rw_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);
static void ad5315__read_reg_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);


events__e ad5315__init(int id, int i2c_id, uint8_t addr, ad5315__cb_t cb, void* ext_data) {
    ad5315__data_t* d = &ad5315__data[id];
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->addr = addr;
    d->i2c_id = i2c_id;
    d->cb = cb;
    d->ext_data = ext_data;
    events__e res = i2c__init(i2c_id, AD5315__I2C_SETTINGS, 0, cb ? ad5315__init_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL))
        d->busy = 0;
    return res;
}

static void ad5315__init_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
    ad5315__data_t* d = ext_data;
    d->busy = 0;
    if (d->cb)
        d->cb(d - ad5315__data, event, 0, 0, d->ext_data);
}

events__e ad5315__read(int id, ad5315__dac_e dac, uint16_t* data, ad5315__cb_t cb, void* ext_data) {
    ad5315__data_t* d = &ad5315__data[id];
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->pkt.dac_id = dac;
    events__e res = i2c__txrx(d->i2c_id, d->addr, (uint8_t*)&d->pkt.data, sizeof(d->pkt.data), &d->pkt.dac_id, 1, cb ? ad5315__rw_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        if (data) {
            *data = (d->pkt.data & 0x0F << 6) | (d->pkt.data >> 10);
        }
        d->busy = 0;
    }
    return res;
}

events__e ad5315__write(int id, ad5315__dac_e dac, uint16_t data, ad5315__cb_t cb, void* ext_data) {
    ad5315__data_t* d = &ad5315__data[id];
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->data = data;
    d->pkt.dac_id = dac;
    events__e res = i2c__txrx(d->i2c_id, d->addr, (uint8_t*)&d->pkt.data, sizeof(d->pkt.data), &d->pkt.dac_id, 1, cb ? ad5315__read_reg_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        if (res == EVENT__OK) {
            d->pkt.data &= 0x00F0;
            d->pkt.data |= (d->data << 10) | ((d->data >> 6) & 0x0F);
            i2c__txrx(d->i2c_id, d->addr, NULL, 0, (uint8_t*)&d->pkt, sizeof(d->pkt), NULL, NULL);
        }
        d->busy = 0;
    }
    return res;
}

static void ad5315__read_reg_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
    ad5315__data_t* d = ext_data;
    d->pkt.data &= 0x00F0;
    d->pkt.data |= (d->data << 10) | ((d->data >> 6) & 0x0F);
    if (event == EVENT__OK)
        event = i2c__txrx(d->i2c_id, d->addr, NULL, 0, (uint8_t*)&d->pkt, sizeof(d->pkt), ad5315__rw_cb, (void*)d);
    if (event != EVENT__OK) {
        d->busy = 0;
        if (d->cb)
            d->cb(d - ad5315__data, event, d->pkt.dac_id, d->pkt.data, d->ext_data);
    }
}

events__e ad5315__cfg(int id, ad5315__dac_e dac, ad5315__op_mode_e mode, ad5315__cb_t cb, void* ext_data) {
    ad5315__data_t* d = &ad5315__data[id];
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->pkt.dac_id = dac;
    d->pkt.data = mode << 13;
    events__e res = i2c__txrx(d->i2c_id, d->addr, NULL, 0, (uint8_t*)&d->pkt, sizeof(d->pkt), cb ? ad5315__rw_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL))
        d->busy = 0;
    return res;
}


static void ad5315__rw_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
    ad5315__data_t* d = ext_data;
    d->busy = 0;
    if (d->cb)
        d->cb(d - ad5315__data, event, d->pkt.dac_id, buff ? ((uint16_t)buff[0] & 0x0F << 6) | (buff[1] >> 2) : 0, d->ext_data);
}
