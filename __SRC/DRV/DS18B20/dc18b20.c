﻿/***************************************************************************//**
 * @file ds18b20.c.
 * @brief  Драйвер микросхемы DS18B20.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include <string.h>
#include "System/sw_timer.h"
#include "DRV/DS18B20/ds18b20.h"
#include "System/crc.h"

#ifndef DS18B20__CNT
#define DS18B20__CNT 1
#endif

typedef enum {
    DS18B20__CMD_NONE       = 0x00,
    DS18B20__CMD_RESET,
    DS18B20__CMD_SEARCH     = 0xF0,
    DS18B20__CMD_READ_ROM   = 0x33,
    DS18B20__CMD_MATCH_ROM  = 0x55,
    DS18B20__CMD_SKIP_ROM   = 0xCC,
    DS18B20__CMD_CONVERT    = 0x44,
    DS18B20__CMD_WAIT,
    DS18B20__CMD_WR_EEPROM  = 0x48,
    DS18B20__CMD_WR_SCRATCH = 0x4E,
    DS18B20__CMD_RD_SCRATCH = 0xBE,
    DS18B20__CMD_CALC_TEMP,
    DS18B20__CMD_CHECK_ROM,
    DS18B20__CMD_CHECK_MODE,

}ds18b20__cmd_e;

typedef struct {
    sw_timer__t timer;
    ds18b20__cb_t cb;
    void* ext_data;
    u8* p_cmd;
    uint64_t uuid;
    float temp;
    u8 buff[9];
    u8 onewire;
    u8 valid;                       // Валидные данные в temp
    u8 mode;
}ds18b20__struct_t;

static ds18b20__struct_t ds18b20__data[DS18B20__CNT];

static const u8 ds18b20__get_temp_tbl[] = {DS18B20__CMD_RESET, DS18B20__CMD_CHECK_ROM, DS18B20__CMD_CONVERT, DS18B20__CMD_WAIT, DS18B20__CMD_RESET, DS18B20__CMD_CHECK_ROM, DS18B20__CMD_RD_SCRATCH, DS18B20__CMD_CALC_TEMP, DS18B20__CMD_CHECK_MODE, DS18B20__CMD_NONE};
static const u8 ds18b20__set_mode_tbl[] = {DS18B20__CMD_RESET, DS18B20__CMD_RESET, DS18B20__CMD_CHECK_ROM, DS18B20__CMD_WR_SCRATCH, DS18B20__CMD_RESET, DS18B20__CMD_CHECK_ROM, DS18B20__CMD_WR_EEPROM, DS18B20__CMD_WAIT, DS18B20__CMD_NONE};


static void ds18b20__tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void ds18b20_onewire_cb(u8 id, events__e event, u8* buff, i32 len, void* ext_data);
static events__e onewire__cmd_exec(ds18b20__struct_t* data);
static void ds18b20__tmr_wait_cb(struct sw_timer__t *timer, void *ext_data);

/*******************************************************************************
 * Функция инициализации модуля.
 ******************************************************************************/
events__e ds18b20__init(u8 id, u8 itf_id) {
    if (id > DS18B20__CNT)
        return EVENT__PARAM_NA;
    ds18b20__struct_t* data = &ds18b20__data[id];
    data->p_cmd = NULL;
    data->onewire = itf_id;
    data->valid = 0;
    data->temp = 0;
    data->uuid = 0;
    data->mode = 3;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция запуска процесса измерения температуры.
 ******************************************************************************/
events__e ds18b20__meas_temp(u8 id, ds18b20__cb_t cb, void* ext_data) {
    ds18b20__struct_t* data = &ds18b20__data[id];
    if ((id > DS18B20__CNT) || (cb == NULL))
        return EVENT__PARAM_NA;
    if(data->p_cmd)
        return EVENT__BUSY;
    data->p_cmd =(u8*)ds18b20__get_temp_tbl;
    data->cb = cb;
    data->ext_data = ext_data;
    return onewire__cmd_exec(data);
}

/*******************************************************************************
 * Функция установки режима преобразования.
 ******************************************************************************/
events__e ds18b20__set_mode(u8 id, u8 mode) {
    ds18b20__struct_t* data = &ds18b20__data[id];
    if ((id > DS18B20__CNT) || (mode > 3))
        return EVENT__PARAM_NA;
    data->mode = mode;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция возвращает валидность считанных данных.
 ******************************************************************************/
u8 ds18b20__is_valid(u8 id) {
    ds18b20__struct_t* data = &ds18b20__data[id];
    if (id > DS18B20__CNT)
        return 0;
    return data->valid;
}

/*******************************************************************************
 * Функция возвращает последнее считанное значение температуры.
 ******************************************************************************/
float ds18b20__get_temp(u8 id) {
    ds18b20__struct_t* data = &ds18b20__data[id];
    if (id > DS18B20__CNT)
        return 0;
    return data->temp;
}

/*******************************************************************************
 * Функция возвращает UUID заданного датчика.
 ******************************************************************************/
uint64_t ds18b20__get_uuid(u8 id) {
    ds18b20__struct_t* data = &ds18b20__data[id];
    if (id > DS18B20__CNT)
        return 0;
    return data->uuid;
}

/*******************************************************************************
 * Функция устанавливает UUID заданному датчику.
 ******************************************************************************/
events__e ds18b20__set_uuid(u8 id, u64 uuid) {
    ds18b20__struct_t* data = &ds18b20__data[id];
    if (id > DS18B20__CNT)
        return EVENT__PARAM_NA;
    data->uuid = uuid;
    return EVENT__OK;
}


static void ds18b20_onewire_cb(u8 id, events__e event, u8* buff, i32 len, void* ext_data) {
    ds18b20__struct_t* data = (ds18b20__struct_t*)ext_data;
    if (event == EVENT__OK) {
        data->p_cmd++;
        event = onewire__cmd_exec(data);
    }
    if ((event != EVENT__OK) || (*data->p_cmd == DS18B20__CMD_NONE)) {
        data->p_cmd = NULL;
        if (data->cb)
            data->cb(data - ds18b20__data, event, data->temp, data->ext_data);
    }
}


static events__e onewire__cmd_exec(ds18b20__struct_t* data) {
    if (!data->p_cmd)
        return EVENT__ERROR;
    events__e res = EVENT__OK;
    switch(*data->p_cmd) {
        case DS18B20__CMD_RESET:
            res = onewire__reset(data->onewire, ds18b20_onewire_cb, data);
            break;
        case DS18B20__CMD_CHECK_ROM:
            if (!data->uuid) {
                data->buff[0] = DS18B20__CMD_SKIP_ROM;
                res = onewire__txrx(data->onewire, data->buff, 1, NULL, 0, ds18b20_onewire_cb, data);
                break;
            }
            data->buff[0] = DS18B20__CMD_MATCH_ROM;
            memcpy(&data->buff[1], &data->uuid, 8);
            res = onewire__txrx(data->onewire, data->buff, 9, NULL, 0, ds18b20_onewire_cb, data);
            break;
        case DS18B20__CMD_CONVERT:
            res = onewire__txrx(data->onewire, data->p_cmd, 1, data->buff, 1, ds18b20_onewire_cb, data);
            break;
        case DS18B20__CMD_RD_SCRATCH:
            res = onewire__txrx(data->onewire, data->p_cmd, 1, data->buff, 9, ds18b20_onewire_cb, data);
            break;
        case DS18B20__CMD_CALC_TEMP:
            data->valid = crc__8_1wire(data->buff, 9) ? 0 : 1;
            if (data->valid)
                data->temp = (i16)(data->buff[0] | (data->buff[1] << 8)) * 0.0625;
            sw_timer__start(&data->timer, 0, ds18b20__tmr_cb, data);
            break;
        case DS18B20__CMD_READ_ROM:
            res = onewire__txrx(data->onewire, data->p_cmd, 1, (u8*)&data->uuid, 8, ds18b20_onewire_cb, data);
            break;
        case DS18B20__CMD_WAIT:
            if (data->buff[0])
                sw_timer__start(&data->timer, 0, ds18b20__tmr_cb, data);
            else
                sw_timer__start(&data->timer, 10 + 10 * data->mode, ds18b20__tmr_wait_cb, data);
            break;
        case DS18B20__CMD_CHECK_MODE:
            if ((!crc__8_1wire(data->buff, 9)) && ((data->buff[4] >> 5) != data->mode))
                data->p_cmd =(u8*)ds18b20__set_mode_tbl;
            sw_timer__start(&data->timer, 0, ds18b20__tmr_cb, data);
            break;
        case DS18B20__CMD_WR_SCRATCH:
            data->buff[0] = DS18B20__CMD_WR_SCRATCH;
            data->buff[1] = 0x00;
            data->buff[2] = 0x00;
            data->buff[3] = (data->mode << 5) | 0x1F;
            res = onewire__txrx(data->onewire, data->buff, 4, NULL, 0, ds18b20_onewire_cb, data);
            break;
        case DS18B20__CMD_WR_EEPROM:
            res = onewire__txrx(data->onewire, data->p_cmd, 1, data->buff, 1, ds18b20_onewire_cb, data);
            break;
        case DS18B20__CMD_NONE:
            break;
        default:
            return EVENT__ERROR;
    }
    return res;
}

static void ds18b20__tmr_wait_cb(struct sw_timer__t *timer, void *ext_data) {
    ds18b20__struct_t* data = (ds18b20__struct_t*)ext_data;
    data->p_cmd--;
    events__e event = onewire__txrx(data->onewire, NULL, 0, data->buff, 1, ds18b20_onewire_cb, data);
    if (event != EVENT__OK) {
        data->p_cmd = NULL;
        if (data->cb)
            data->cb(data - ds18b20__data, event, data->temp, data->ext_data);
    }

}

static void ds18b20__tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    ds18b20__struct_t* data = (ds18b20__struct_t*)ext_data;
    events__e event = EVENT__OK;
    data->p_cmd++;
    event = onewire__cmd_exec(data);
    if ((event != EVENT__OK) || (*data->p_cmd == DS18B20__CMD_NONE)) {
        data->p_cmd = NULL;
        if (data->cb)
            data->cb(data - ds18b20__data, event, data->temp, data->ext_data);
    }
}
