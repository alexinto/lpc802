/***************************************************************************//**
 * @file m24aa64.c.
 * @brief Драйвер микросхемы 24AA64.
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "System/sw_timer.h"
#include "DRV/M24AA64/m24aa64.h"
#include "DRV/i2c.h"


#define M24AA64__I2C_SETTINGS (0)   // todo добавить валидные настройки
#define PAGE_SIZE 32                // размер страницы памяти
#define M24AA64__ERR_MAX      100   // Максимальное кол-во попыток при неудачных транзакциях

#ifndef M24AA64__COUNT
#define M24AA64__COUNT 1
#endif

#pragma pack(push, 1)
typedef struct {
    u8 addr_hi;                     // Старший байт адреса
    u8 addr_low;                    // Младший байт адреса
    u8 data[32];                    // Буфер для данных (чтение/запись)
}m24aa64__pkt_t;
#pragma pack(pop)

typedef enum {
    M24AA64__OP_NONE  = 0,          // Свободное состояние
    M24AA64__OP_READ  = 1,          // Производится чтение данных из памяти
    M24AA64__OP_WRITE = 2,          // Производится запись данных в память
}m24aa64__oper_e;

typedef struct {
    m24aa64__pkt_t pkt;             // структура пакета
    u8 dev_addr;                    // 7 битный адрес микросхемы на шине I2C
    u16 page_align;                 // кол-во байт для текущей итерации записи
    m24aa64__oper_e oper;           // текущая операция
    i32 err_counter;                // счетчик повторов при ошибках
    u32 addr;                       // Начальный адрес памяти
    u32 offset;                     // Смещение относительно addr, байт.
    u8* buff;                       // Указатель на буфер с пользовательскими данными
    i32 len;                        // Размер пользовательских данных, байт.
    i32 i2c_id;                     // Идентификатор интерфейса, к которому подключена микросхема
    m24aa64__cb_t cb;               // Пользовательский коллбэк
    void* ext_data;                 // Указатель на пользовательские данные
    sw_timer__t timer;              // Таймер для обработки ошибок
}m24aa64__data_t;

static m24aa64__data_t m24aa64__data[M24AA64__COUNT];

static void m24aa64__init_cb(i32 i2c_id, events__e event, i32 addr, u8 *buff, i32 len, void* ext_data);
static void m24aa64__i2c_cb(i32 i2c_id, events__e event, i32 addr, u8 *buff, i32 len, void* ext_data);
static events__e m24aa64__wr_exec(m24aa64__data_t* p_data, events__e event);
static events__e m24aa64__rd_exec(m24aa64__data_t* p_data, events__e event);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);

/*******************************************************************************
 * Функция инициализации. Инициализирует интерфейс.
 ******************************************************************************/
events__e m24aa64__init(i32 id, i32 i2c_id, u8 dev_addr, m24aa64__cb_t cb, void* ext_data) {
    m24aa64__data_t* d = &m24aa64__data[id];
    if (d->oper != M24AA64__OP_NONE)
        return EVENT__BUSY;
    d->oper = M24AA64__OP_READ;
    d->dev_addr = dev_addr;
    d->i2c_id = i2c_id;
    d->cb = cb;
    d->ext_data = ext_data;
    events__e res = i2c__init(i2c_id, M24AA64__I2C_SETTINGS, 0, cb ? m24aa64__init_cb : NULL, (void*)d);
    if ((cb == NULL) || (res != EVENT__OK))
        d->oper = M24AA64__OP_NONE;
    return res;
}

/*******************************************************************************
 * Коллбэк по окончанию процесса инициализации интерфейса.
 ******************************************************************************/
static void m24aa64__init_cb(i32 i2c_id, events__e event, i32 addr, u8 *buff, i32 len, void* ext_data) {
    m24aa64__data_t* d = ext_data;
    d->oper = M24AA64__OP_NONE;
    if (d->cb)
        d->cb(d - m24aa64__data, event, 0, 0, 0, d->ext_data);
}

/*******************************************************************************
 * Функция чтения данных из памяти.
 ******************************************************************************/
events__e m24aa64__read(i32 id, u32 addr, u8* buff, i32 len, m24aa64__cb_t cb, void* ext_data) {
    m24aa64__data_t* d = &m24aa64__data[id];
    if (d->oper != M24AA64__OP_NONE)
        return EVENT__BUSY;
    d->oper = M24AA64__OP_READ;
    d->cb = cb;
    d->ext_data = ext_data;
    d->addr = addr;
    d->buff = buff;
    d->len = len;
    d->offset = d->err_counter = 0;
    d->pkt.addr_low = addr;
    d->pkt.addr_hi = addr >> 8;
    d->page_align = 1;            // чтение производится за 1 транзакцию I2C. 1- произвольная цифра...
    return m24aa64__rd_exec(d, EVENT__OK);
}

/*******************************************************************************
 * Функция записи данных в память.
 ******************************************************************************/
events__e m24aa64__write(i32 id, u32 addr, u8* buff, i32 len, m24aa64__cb_t cb, void* ext_data) {
    m24aa64__data_t* d = &m24aa64__data[id];
    if (d->oper != M24AA64__OP_NONE)
        return EVENT__BUSY;
    d->oper = M24AA64__OP_WRITE;
    d->cb = cb;
    d->ext_data = ext_data;
    d->addr = addr;
    d->buff = buff;
    d->len = len;
    d->offset = d->err_counter = 0;
    d->pkt.addr_low = addr;
    d->pkt.addr_hi = addr >> 8;
    return m24aa64__wr_exec(d, EVENT__OK);
}

/*******************************************************************************
 * Функция выполняет одну итерацию записи данных в память.
 ******************************************************************************/
static events__e m24aa64__wr_exec(m24aa64__data_t* p_data, events__e event) {
    m24aa64__data_t* d = p_data;
    u32 cur_addr;
    do {
        cur_addr = d->addr + d->offset;
        if ((event != EVENT__OK) && (++d->err_counter < M24AA64__ERR_MAX)) {
            if (!d->cb)  // Если выполнение синхронное (нет cb), то ждем 1мс и повторяем запись.
                sw_timer__start(&d->timer, 1, NULL, (void*)d);
        }
        else {
            d->page_align = PAGE_SIZE - (cur_addr % PAGE_SIZE);
            if ((d->offset + d->page_align) > d->len)
                d->page_align = d->len - d->offset;
            d->err_counter = 0;
            memcpy(d->pkt.data, d->buff + d->offset, d->page_align);
            d->pkt.addr_low = cur_addr;
            d->pkt.addr_hi = cur_addr >> 8;
            d->offset += d->page_align;
        }
        if (d->page_align)   // Если есть что записывать...
            event = i2c__txrx(d->i2c_id, d->dev_addr, NULL, 0, (u8*)&d->pkt, 2 + d->page_align, d->cb ? m24aa64__i2c_cb : NULL, (void*)d);
    } while((d->cb == NULL) && (d->err_counter < M24AA64__ERR_MAX) && (d->page_align));

    if ((d->cb == NULL) || (d->err_counter >= M24AA64__ERR_MAX) || (!d->page_align)) {
        d->oper = M24AA64__OP_NONE;
        if (d->cb)
            d->cb(d - m24aa64__data, event, d->addr, d->buff, d->len, d->ext_data);
    }
    else if (event != EVENT__OK) {
        sw_timer__start(&d->timer, 1, timer_cb, (void*)d);
        return EVENT__OK;
    }
    return event;
}

/*******************************************************************************
 * Функция выполняет одну итерацию чтения данных из памяти.
 ******************************************************************************/
static events__e m24aa64__rd_exec(m24aa64__data_t* p_data, events__e event) {
    m24aa64__data_t* d = p_data;
    do {
        if ((event != EVENT__OK) && (++d->err_counter < M24AA64__ERR_MAX)) {
            if (!d->cb)
                sw_timer__start(&d->timer, 1, NULL, (void*)d);
        }
        if (d->page_align)
            event = i2c__txrx(d->i2c_id, d->dev_addr, d->buff, d->len, (u8*)&d->pkt, 2, d->cb ? m24aa64__i2c_cb : NULL, (void*)d);
    } while((d->cb == NULL) && (d->err_counter < M24AA64__ERR_MAX) && (event != EVENT__OK));
    if ((d->cb == NULL) || (d->err_counter >= M24AA64__ERR_MAX) || (!d->page_align)) {
        d->oper = M24AA64__OP_NONE;
        if (d->cb)
            d->cb(d - m24aa64__data, event, d->addr, d->buff, d->len, d->ext_data);
    }
    else if (event != EVENT__OK) {
        sw_timer__start(&d->timer, 1, timer_cb, (void*)d);
        return EVENT__OK;
    }
    return event;
}

/*******************************************************************************
 * Коллбэк по окончанию одной итерации записи/чтения памяти.
 ******************************************************************************/
static void m24aa64__i2c_cb(i32 i2c_id, events__e event, i32 addr, u8 *buff, i32 len, void* ext_data) {
    m24aa64__data_t* d = (m24aa64__data_t*)ext_data;
    if (event == EVENT__OK) {
        if (d->oper == M24AA64__OP_WRITE)
            m24aa64__wr_exec(d, event);
        else {
            d->page_align = 0; // Чтение производится за 1 транзакцию I2C
            m24aa64__rd_exec(d, event);
        }
    }
    else // Если ошибка, то повторим позже...
        sw_timer__start(&d->timer, 1, timer_cb, (void*)d);
}

/*******************************************************************************
 * Коллбэк таймера по ошибочной транзакции записи/чтения.
 ******************************************************************************/
static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    m24aa64__data_t* d = ext_data;
    if (d->oper == M24AA64__OP_WRITE)
        m24aa64__wr_exec(d, EVENT__ERROR);
    else
        m24aa64__rd_exec(d, EVENT__ERROR);
}
