/***************************************************************************//**
 * @file pca9535.c.
 * @brief Драйвер PCA9535.
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "DRV/PCA9535/pca9535.h"
#include "DRV/i2c.h"

#define PCA9535__I2C_SETTINGS 0

#ifndef PCA9535__COUNT
#define PCA9535__COUNT 1
#endif

#pragma pack(push, 1)
typedef struct {
    u8 command;
    u16 data;
}pca9535__pkt_t;
#pragma pack(pop)


typedef struct {
    pca9535__pkt_t pkt;
    u8 busy;
    u8 addr;         // 7 битный адрес на шине I2C
    pca9535__reg_e reg;
    i32 i2c_id;
    pca9535__cb_t cb;
    u16* data_read;
    void* ext_data;
}pca9535__data_t;

static pca9535__data_t pca9535__data[PCA9535__COUNT];

static void pca9535__init_cb(i32 i2c_id, events__e event, i32 addr, u8 *buff, i32 len, void* ext_data);
static void pca9535__rw_cb(i32 i2c_id, events__e event, i32 addr, u8 *buff, i32 len, void* ext_data);


events__e pca9535__init(i32 id, i32 i2c_id, u8 addr, pca9535__cb_t cb, void* ext_data) {
    pca9535__data_t* d = &pca9535__data[id];
    if (id >= PCA9535__COUNT)
        return EVENT__PARAM_NA;
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->addr = addr;
    d->i2c_id = i2c_id;
    d->ext_data = ext_data;
    d->cb = cb;
    events__e res = i2c__init(i2c_id, PCA9535__I2C_SETTINGS, 0, cb ? pca9535__init_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL))
        d->busy = 0;
    return res;
}

static void pca9535__init_cb(i32 i2c_id, events__e event, i32 addr, u8 *buff, i32 len, void* ext_data) {
    pca9535__data_t* d = ext_data;
    d->busy = 0;
    if (d->cb)
        d->cb(d - pca9535__data, event, 0, 0, d->ext_data);
}

events__e pca9535__read(i32 id, pca9535__reg_e reg, u16* data, pca9535__cb_t cb, void* ext_data) {
    pca9535__data_t* d = &pca9535__data[id];
    if (id >= PCA9535__COUNT)
        return EVENT__PARAM_NA;
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->reg = reg;
    d->pkt.command = reg;
    d->data_read = data;
    events__e res = i2c__txrx(d->i2c_id, d->addr, (u8*)&d->pkt.data, sizeof(d->pkt.data), &d->pkt.command, 1, cb ? pca9535__rw_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        if ((data) && (res == EVENT__OK))
            *data = d->pkt.data;
        d->busy = 0;
    }
    return res;
}

events__e pca9535__write(i32 id, pca9535__reg_e reg, u16 data, pca9535__cb_t cb, void* ext_data) {
    pca9535__data_t* d = &pca9535__data[id];
    if (id >= PCA9535__COUNT)
        return EVENT__PARAM_NA;
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->reg = reg;
    d->pkt.command = reg;
    d->pkt.data = data;
    d->data_read = NULL;
    events__e res = i2c__txrx(d->i2c_id, d->addr, NULL, 0, (u8*)&d->pkt, sizeof(d->pkt), cb ? pca9535__rw_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL))
        d->busy = 0;
    return res;
}

static void pca9535__rw_cb(i32 i2c_id, events__e event, i32 addr, u8 *buff, i32 len, void* ext_data) {
    pca9535__data_t* d = ext_data;
    d->busy = 0;
    if ((d->data_read) && (event == EVENT__OK))
        *d->data_read = d->pkt.data;
    if (d->cb)
        d->cb(d - pca9535__data, event, d->reg, d->pkt.data, d->ext_data);
}
