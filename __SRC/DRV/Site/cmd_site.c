/***************************************************************************//**
 * @file cmd_site.c.
 * @brief Модуль, реализующий управление сайтами.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include <string.h>
#include "DRV/cmd_site.h"

#define SITE_FREE 0                                     // Зарезервирован- свободный сайт

#ifdef SITE__MAIN_INFO
#include "DRV/pg_emul.h"
#else
    #define ERR_CNT_WR_ADDR     0x4800020C     // SYS_STOP_ERR_CNTR
    #define ERR_CNT_RD_ADDR     0x4800021C     // SYS_SITE_ERR_CNTR
    #define GTS_CTRL_ADDR       0x48000200     // SYS_FC_CONTROL
    #define CSTD_COUNT_RW_ADR   0x48000204     // SYS_CSTD_COUNT
    #define LINK_ADDR           0x4800022C
    #define MB_CLKD_SEL_MASK    0x48000208
#endif

#ifndef SITE_CSTD_MIN
    #define SITE_CSTD_MIN 40
#endif

#ifndef SITE_NUM
    #define SITE_NUM 8
#endif

typedef enum {
    SET_MASK_MODE   = 0,    // Запись значений побитовым ИЛИ
    SET_NORMAL_MODE = 1,    // Запись значений с предварительным обнулением буфера
    CLR_MASK_MODE   = 2,    // Запись значений инверсией + побитовым &
}site_set_mode_e;

typedef struct {
    cmd_site__status_e state;
    uint8_t site_id;
    cmd_site__pg_t* gts_ptr;
}site_tbl_t;

static site_tbl_t site_tbl[SITE_NUM];
static cmd_site__pg_t* pg_addr_tbl[SITE_NUM];

static site_tbl_t* site_find(uint8_t site_id);
static void site_16_set(uint8_t id, uint32_t addr, uint16_t data, site_set_mode_e mode);
static uint16_t site_16_get(uint8_t id, uint32_t addr);

#ifdef SITE__MAIN_INFO
static void site_8_set(uint8_t id, uint32_t addr, uint8_t data, site_set_mode_e mode);
static uint8_t site_8_get(uint8_t id, uint32_t addr);
#endif /* SITE__MAIN_INFO */

/*******************************************************************************
 * Функция инициализации библиотеки управления сайтами.
 ******************************************************************************/
events__e cmd_site__init() {
    *(uint32_t*)GTS_CTRL_ADDR = 0;
#ifdef SITE__MAIN_INFO
    uint32_t* pg_addr;
    for(int i = 0; i < SITE_NUM; i++) {
        pg_addr = (uint32_t*)pg_addr_tbl[i];
        for(int j = 0; j < (sizeof(cmd_site__pg_t) / 4); j++)
            *(pg_addr + j) = 0;
        site_8_set(i, MB_MERGE_MASK, 0, SET_NORMAL_MODE); // очищаем ГТП
        site_16_set(i, MB_STOP_ERR_CNTR, 0, SET_NORMAL_MODE);
    }
    *(uint32_t*)MB_BUS_ENA_MASK = 0;
#else
    for(int i = 0; i < SITE_NUM; i++) {
        site_16_set(i, LINK_ADDR, 0, SET_NORMAL_MODE);    // очищаем линки
        site_16_set(i, ERR_CNT_WR_ADDR, 0, SET_NORMAL_MODE);   // записываем err_cnt
    }
#endif
    *(uint32_t*)MB_CLKD_SEL_MASK = 0;
    memset(site_tbl, 0, sizeof(site_tbl));
    return cmd_site__cstd_set(1,1);
}

/*******************************************************************************
 * Функция настройки сайта.
 ******************************************************************************/
events__e cmd_site__cfg(uint8_t site_id, uint8_t part_id, uint8_t link_id, uint16_t gts_id, uint16_t err_cnt, uint8_t pg_sync) {
    if ((!site_id) || (link_id > 16) || ((part_id != SITE_DOM__1) && (part_id != SITE_DOM__2)))     // 0- недопустимый ID
        return EVENT__PARAM_NA;
    site_tbl_t* cur_site = site_find(site_id);          // Ищем указатель по ID
    if (gts_id == 0) {                                  // Удаляем элемент
        if (cur_site) {
            cur_site->site_id = 0;
#ifdef SITE__MAIN_INFO
            site_8_set(cur_site - site_tbl, MB_MERGE_MASK, 0, SET_NORMAL_MODE); // очищаем ГТП
#else
            site_16_set(cur_site - site_tbl, LINK_ADDR, 0, SET_NORMAL_MODE);    // очищаем линки
#endif
            return EVENT__OK;
        }
        return EVENT__ERROR;
    }
    if (!cur_site) {                                    // Если элемент новый, то ищем место, чтобы добавить
        cur_site = site_find(SITE_FREE);
        if (!cur_site)
            return EVENT__ERROR;
        cur_site->site_id = site_id;                    // запоминаем новый элемент
    }
    uint8_t site_real = cur_site - site_tbl;            // вычиляем индекс (для вычисления смещения регистров)
    cur_site->gts_ptr = NULL;
    uint32_t temp = 0;
    temp = *(uint32_t*)MB_CLKD_SEL_MASK;                // выставляем бит выбора ССВД для сайта
    temp &= ~(1 << site_real);                          // SITE_DOM__1
    if (part_id == SITE_DOM__2)
        temp |= (1 << site_real);
    *(uint32_t*)MB_CLKD_SEL_MASK = temp;
#ifdef SITE__MAIN_INFO
    uint8_t slot_id, gts_more = ((gts_id & (gts_id - 1)) || (pg_sync)) ? 1 : 0; // Если ГТП больше 1го
    SLOT_ID_GET(slot_id);
    for(int i  = 0; i < SITE_NUM; i++) {
        if (gts_id & (1 << i)) {
            if (!gts_more)
                cur_site->gts_ptr = pg_addr_tbl[i];
            pg_addr_tbl[i]->pg_stop_err_cntr = gts_more ? 0 : err_cnt;
            pg_addr_tbl[i]->pg_clkd_sel_mask = (part_id == SITE_DOM__1) ? 0 : 1;
            pg_addr_tbl[i]->pg_bus_ena_mask = link_id ? 1 : gts_more;
        }
    }
    site_8_set(0, MB_BUS_ENA_MASK, 1 << site_real, CLR_MASK_MODE);
    site_8_set(1, MB_BUS_ENA_MASK, 1 << site_real, CLR_MASK_MODE);
    if (link_id) {
        if ((link_id > slot_id * 2 + 2 ) || (link_id <= slot_id * 2))  // Проверка на соответствие Link ID
            return EVENT__ERROR;
        err_cnt = 0;
        site_8_set((link_id + 1) % 2, MB_BUS_ENA_MASK, 1 << site_real, SET_NORMAL_MODE);
    }
    else if (!gts_more) {                        // Если ГТП 1
        err_cnt = gts_id = 0;
    }
    site_16_set(site_real, MB_STOP_ERR_CNTR, err_cnt, SET_NORMAL_MODE);
    site_8_set(site_real, MB_MERGE_MASK, gts_id, SET_NORMAL_MODE);
#else
    if (link_id) {
        link_id--;
        site_16_set(site_real, LINK_ADDR, 1 << link_id, SET_MASK_MODE);      // добавляем link
    }
    site_16_set(site_real, ERR_CNT_WR_ADDR, err_cnt, SET_NORMAL_MODE);   // записываем err_cnt
#endif
    return EVENT__OK;
}

/*******************************************************************************
 * Функция поиска элемента в массиве по ID.
 ******************************************************************************/
static site_tbl_t* site_find(uint8_t site_id) {
    for(int i = 0; i < SITE_NUM; i++)
        if (site_tbl[i].site_id == site_id) {
            return &site_tbl[i];
        }
    return NULL;
}

/*******************************************************************************
 * Функция записи 16-битных значений.
 ******************************************************************************/
static void site_16_set(uint8_t id, uint32_t addr, uint16_t data, site_set_mode_e mode) {
    uint32_t* cur_addr = (uint32_t*)addr + id / 2;
    uint32_t buff = *cur_addr;
    if (id % 2) {                                // если четный, то старшие 16 бит
        if (mode)
            buff &= 0x0000FFFF;
        buff |= (uint32_t)data << 16;
    }
    else {
        if (mode)
            buff &= 0x0000;
        buff |= data;
    }
    *cur_addr = buff;
}

/*******************************************************************************
 * Функция чтения 16-битных значений.
 ******************************************************************************/
static uint16_t site_16_get(uint8_t id, uint32_t addr) {
    uint32_t* cur_addr = (uint32_t*)addr + id / 2;
    uint32_t buff = *cur_addr;
    if (id % 2)                                 // если четный, то старшие 16 бит
        buff >>= 16;
    return (uint16_t)buff;
}

#ifdef SITE__MAIN_INFO
/*******************************************************************************
 * Функция записи 8-битных значений.
 ******************************************************************************/
static void site_8_set(uint8_t id, uint32_t addr, uint8_t data, site_set_mode_e mode) {
    uint32_t* cur_addr = (uint32_t*)addr + id / 4;
    uint32_t buff = *cur_addr, value = (uint32_t)data << (8 * (id % 4));
    switch(mode) {
        case SET_NORMAL_MODE:                  // Без break!!!
            buff &= ~(0xFF << (8 * (id % 4)));     // Очищаем байт
        case SET_MASK_MODE:
            buff |= value;
            break;
        case CLR_MASK_MODE:
            buff &= ~value;
            break;
    }
    *cur_addr = buff;
}

/*******************************************************************************
 * Функция чтения 8-битных значений.
 ******************************************************************************/
static uint8_t site_8_get(uint8_t id, uint32_t addr) {
    uint32_t* cur_addr = (uint32_t*)addr + id / 4;
    uint32_t buff = *cur_addr;
        buff >>= (8 * (id % 4));     // Получаем байт
    return (uint8_t)buff;
}
#endif /* SITE__MAIN_INFO */
/*******************************************************************************
 * Функция возвращает значение счетчика ошибок.
 ******************************************************************************/
int cmd_site__err_cnt_get(uint8_t site_id, uint8_t part_id) {
    site_tbl_t* cur_site = site_find(site_id);
    uint16_t err_cnt;
    if (!site_id)
        return -1;
    uint8_t site_real = cur_site - site_tbl;            // вычиляем индекс (для вычисления смещения регистров)
#ifdef SITE__MAIN_INFO
    uint8_t gts_id = site_8_get(site_real, MB_MERGE_MASK);
    if (gts_id)                          // Если ГТП управляется из мейна
        err_cnt = site_16_get(site_real, ERR_CNT_RD_ADDR);
    else if (cur_site->gts_ptr) {
        err_cnt = cur_site->gts_ptr->pg_err_cntr;
    }
#else
        err_cnt = site_16_get(site_real, ERR_CNT_RD_ADDR);
#endif
    return err_cnt;
}

/*******************************************************************************
 * Функция подготовки и запуска сайтов.
 ******************************************************************************/
events__e cmd_site__set_state(uint8_t site_id, cmd_site__status_e state) {
    site_tbl_t* cur_site = site_find(site_id);          // Ищем указатель по ID
    if(!cur_site)
        return EVENT__PARAM_NA;
    uint8_t site_real = cur_site - site_tbl;                  // вычиляем индекс (для вычисления смещения регистров)
    uint32_t* gts_addr = (uint32_t*)GTS_CTRL_ADDR, buff = 0x0000;
    switch(state) {
        case SITE_STATUS__STOP:
            cur_site->state = SITE_STATUS__STOP;
            if (cur_site->gts_ptr)                         // Если ГТП один
                cur_site->gts_ptr->pg_fc_control = 0x02;
            else
                buff = 0x100 << site_real;
            break;
        case SITE_STATUS__IDLE:
            cur_site->state = state;
            break;
        case SITE_STATUS__WORK:
            for(int i = 0; i < SITE_NUM; i++) {
                if (site_tbl[i].state == SITE_STATUS__IDLE) {
                    site_tbl[i].state = SITE_STATUS__WORK;
                    if (site_tbl[i].gts_ptr)                        // Если ГТП один
                        site_tbl[i].gts_ptr->pg_fc_control = 0x01;
                    else
                        buff |= 1 << i;
                }
            }
            break;
        default:
            return EVENT__PARAM_NA;
    }
    if (buff)
        *gts_addr = buff;
    return EVENT__OK;
}

/*******************************************************************************
 * Функция возвращает состояние сайта.
 ******************************************************************************/
cmd_site__status_e cmd_site__get_state(uint8_t site_id) {
    site_tbl_t* cur_site = site_find(site_id);          // Ищем указатель по ID
    if(!cur_site)
        return SITE_STATUS__ERR;
#ifdef SITE__MAIN_INFO
    uint8_t gts_id, state_break = 1, state_pause = 1, state_run = 1;
    gts_id = site_8_get(cur_site - site_tbl, MB_MERGE_MASK);
    for(int i = 0; i < SITE_NUM; i++) {
        if (gts_id & (1 << i)) {
            if ((pg_addr_tbl[i]->pg_fc_status & 0x08) == 0)  // 1 – выполнение ФК было завершено (состояние BREAK).
                state_break = 0;
            if ((pg_addr_tbl[i]->pg_fc_status & 0x01) == 0)  // 1 - при последнем выполнении ФК было достигнуто состояние RUN. При переходе в состояние PAUSE бит сбрасывается.
                state_run = 0;
            if ((pg_addr_tbl[i]->pg_fc_status & 0x04) == 0)  // 1 - при последнем выполнении ФК было достигнуто состояние PAUSE.
                state_pause = 0;
        }
    }
    if (gts_id) {
        if (state_break)
            cur_site->state = SITE_STATUS__BREAK;
        else if (state_pause & state_run)
            cur_site->state = SITE_STATUS__WORK;
        else if (state_pause)
            cur_site->state = SITE_STATUS__PAUSE;
    }
#endif
    return cur_site->state;
}

static void swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

static int gcd(int a, int b) {
    if (a < b) {
        swap(&a, &b);
    }
    while (a % b != 0) {
        a = a % b;
        swap(&a, &b);
    }
    return b;
}

/*******************************************************************************
 * Функция установки ССВД.
 ******************************************************************************/
events__e cmd_site__cstd_set(uint16_t div1, uint16_t div2) {
    if ((div1 == 0) || (div2 == 0))
        return EVENT__PARAM_NA;
// Поиск значений счетчиков ССВД
    uint32_t nok = div1 * div2 / gcd(div1, div2);
    uint32_t cnt1 = nok / div2;
    uint32_t cnt2 = nok / div1;
    if ((cnt1 < SITE_CSTD_MIN) || (cnt2 < SITE_CSTD_MIN)) {
        if (cnt1 < cnt2)
            nok = (SITE_CSTD_MIN / cnt1 + 1) * cnt1;
        else
            nok = (SITE_CSTD_MIN / cnt2 + 1) * cnt2;
        cnt1 *= nok;
        cnt2 *= nok;
    }
    cnt1--; cnt2--;           // Перевод периодов ОЧФК в счетчик с 0
    if ((cnt1 > 0xFFFF) || (cnt2 > 0xFFFF))
        return EVENT__PARAM_NA;

    uint32_t* cstd_addr = (uint32_t*)CSTD_COUNT_RW_ADR, cstd_data = (uint32_t)cnt2 << 16 | cnt1;
#ifdef SITE__MAIN_INFO
    if (*(uint32_t*)MAIN_PG_INFO == 0)
        return EVENT__OK;
    for(int i = 0; i < SITE_NUM; i++) {
        pg_addr_tbl[i]->pg_cstd_count = cstd_data;
    }
#endif
    *cstd_addr = cstd_data;
    return EVENT__OK;
}

events__e cmd_site__pg_addr_set(uint8_t pg_num, cmd_site__pg_t* pg_addr) {
    if (pg_num >= SITE_NUM)
        return EVENT__ERROR;
    pg_addr_tbl[pg_num] = pg_addr;
    return EVENT__OK;
}

