/***************************************************************************//**
 * @file ad3551.c.
 * @brief Драйвер AD3551 (аппаратно зависимая часть).
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "System/framework.h"
#include "DRV/spi.h"
#include "DRV/AD3551/ad3551.h"

// Настройки интерфейса SPI
#define AD3551__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_6_MBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB | SPI__TYPE_QUAD)

#ifndef AD3551__COUNT
#define AD3551__COUNT 1
#endif

#pragma pack(push, 1)
typedef struct {
    uint8_t addr : 7;
    uint8_t rw   : 1;
    uint8_t data[3];
}ad3551__pkt_t;
#pragma pack(pop)

typedef enum {
    AD3551__WRITE = 0,
    AD3551__READ  = 1
}ad3551__rw_e;

typedef enum {
    AD3551__OP_NONE = 0,
    AD3551__OP_READ_ID,
    AD3551__OP_CHK_ID,
    AD3551__OP_SET_RST,
    AD3551__OP_CLR_RST,
    AD3551__OP_WR_RST,
    AD3551__OP_READ_RST,
    AD3551__OP_CHK_RST,
    AD3551__OP_READ,
    AD3551__OP_RD_CHK,
    AD3551__OP_WRITE,
    AD3551__OP_L_VOUT,
    AD3551__OP_MODE_CLR_FAST,
    AD3551__OP_MODE_CLR_PREC,

}ad3551__op_e;

static const ad3551__op_e ad3551__init_tbl[] = {AD3551__OP_READ_ID, AD3551__OP_CHK_ID, AD3551__OP_SET_RST, AD3551__OP_CLR_RST, AD3551__OP_WR_RST, AD3551__OP_READ_RST, AD3551__OP_CHK_RST, AD3551__OP_NONE};
static const ad3551__op_e ad3551__read_tbl[] = {AD3551__OP_READ, AD3551__OP_RD_CHK, AD3551__OP_NONE};
static const ad3551__op_e ad3551__write_tbl[] = {AD3551__OP_WRITE, AD3551__OP_NONE};
static const ad3551__op_e ad3551__vout_tbl[] = {AD3551__OP_WRITE, AD3551__OP_L_VOUT, AD3551__OP_NONE};
static const ad3551__op_e ad3551__mode_set_tbl[] = {AD3551__OP_MODE_CLR_FAST, AD3551__OP_MODE_CLR_PREC, AD3551__OP_WRITE, AD3551__OP_NONE};

typedef struct {
    ad3551__pkt_t pkt;
    uint8_t err_cnt;
    uint8_t busy;
    uint8_t reg;
    uint8_t len;
    int dev_id;
    ad3551__op_e* oper;
    uint32_t data_int;
    uint32_t* data;
    ad3551__cb_t cb;
    void* ext_data;
}ad3551__struct_t;

static ad3551__struct_t ad3551__data[AD3551__COUNT];
static ad3551__handler_t ad3551_handler;

static ad3551__struct_t* ad3551__free_get(int dev_id);
static void ad3551__spi_cb(int dev_id, events__e code, uint8_t *buff, int len, void *ext_data);
static int ad3551__reg_len_get(uint32_t addr);
static events__e ad3551__rw(ad3551__struct_t* d, ad3551__rw_e op, uint32_t reg, uint32_t data);
static events__e ad3551__cmd_exec(ad3551__struct_t* d);

events__e ad3551__init(int dev_id, uint32_t settings, ad3551__cb_t cb, void* ext_data) {
    ad3551__struct_t* d = ad3551__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->data_int = 0;
    d->data = &d->data_int;
    d->ext_data = ext_data;
    d->oper = (ad3551__op_e*)ad3551__init_tbl;
    events__e res = spi__init(dev_id, AD3551__SPI_SETTINGS);
    res = ad3551__cmd_exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while(((res == EVENT__OK) || (res == EVENT__CONTINUE)) && (*d->oper != AD3551__OP_NONE)) {
            ++d->oper;
            res = ad3551__cmd_exec(d);
        }
        d->busy = 0;
    }
    return res;
}

events__e ad3551__set_ref(int dev_id, ad3551__ref_e ref, ad3551__cb_t cb, void* ext_data) {
    return ad3551__write(dev_id, AD3551_REG->ref_cfg, (ref == AD3551__REF_EXT) ? 2 : 0, cb, ext_data);
}

events__e ad3551__set_gain(int dev_id, ad3551__gain_e p_gain, ad3551__gain_e n_gain, ad3551__off_e polarity, ad3551__cb_t cb, void* ext_data) {
    uint32_t data = (n_gain << 5) | (p_gain << 3) | (polarity << 2);
    return ad3551__write(dev_id, AD3551_REG->ch0_gain, data, cb, ext_data);
}


events__e ad3551__get_vout(int dev_id, uint32_t* data, ad3551__cb_t cb, void* ext_data) {
    return ad3551__read(dev_id, AD3551_REG->ch0_dac_24b, data, cb, ext_data);
}

events__e ad3551__set_vrng(int dev_id, ad3551__vrng_e range, ad3551__cb_t cb, void* ext_data) {
    return ad3551__write(dev_id, AD3551_REG->ch0_output_range, range, cb, ext_data);
}

events__e ad3551__set_voffset(int dev_id, int8_t offset, ad3551__cb_t cb, void* ext_data) {
    return ad3551__write(dev_id, AD3551_REG->ch0_offset, offset, cb, ext_data);
}

events__e ad3551__set_mode(int dev_id, ad3551__mode_e mode, ad3551__cb_t cb, void* ext_data) {
    ad3551__struct_t* d = ad3551__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->oper = (ad3551__op_e*)ad3551__mode_set_tbl;
    d->cb = cb;
    d->ext_data = ext_data;
    d->reg = (mode == AD3551__MODE_PRESICION) ? AD3551_REG->ch_select_24b : AD3551_REG->ch_select_16b;
    d->data_int = 1;
    events__e res = ad3551__cmd_exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while(((res == EVENT__OK) || (res == EVENT__CONTINUE)) && (*d->oper != AD3551__OP_NONE)) {
            ++d->oper;
            res = ad3551__cmd_exec(d);
        }
        d->busy = 0;
    }
    return res;
}

events__e ad3551__set_vout(int dev_id, uint32_t data, ad3551__cb_t cb, void* ext_data) {
    ad3551__struct_t* d = ad3551__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->oper = (ad3551__op_e*)ad3551__vout_tbl;
    d->cb = cb;
    d->ext_data = ext_data;
    d->data_int = data << 8;
    d->reg = AD3551_REG->ch0_input_24b;
    events__e res = ad3551__cmd_exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while(((res == EVENT__OK) || (res == EVENT__CONTINUE)) && (*d->oper != AD3551__OP_NONE)) {
            ++d->oper;
            res = ad3551__cmd_exec(d);
        }
        d->busy = 0;
    }
    return res;
}

events__e ad3551__write(int dev_id, uint32_t reg, uint32_t data, ad3551__cb_t cb, void* ext_data) {
    ad3551__struct_t* d = ad3551__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->oper = (ad3551__op_e*)ad3551__write_tbl;
    d->cb = cb;
    d->ext_data = ext_data;
    d->reg = reg;
    d->data_int = data;
    events__e res = ad3551__cmd_exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while(((res == EVENT__OK) || (res == EVENT__CONTINUE)) && (*d->oper != AD3551__OP_NONE)) {
            ++d->oper;
            res = ad3551__cmd_exec(d);
        }
        d->busy = 0;
    }
    return res;
}

events__e ad3551__read(int dev_id, uint32_t reg, uint32_t* data, ad3551__cb_t cb, void* ext_data) {
    ad3551__struct_t* d = ad3551__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->busy = 1;
    d->oper = (ad3551__op_e*)ad3551__read_tbl;
    d->data = data;
    d->cb = cb;
    d->ext_data = ext_data;
    d->data = data;
    d->reg = reg;
    events__e res = ad3551__cmd_exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while(((res == EVENT__OK) || (res == EVENT__CONTINUE)) && (*d->oper != AD3551__OP_NONE)) {
            ++d->oper;
            res = ad3551__cmd_exec(d);
        }
        d->busy = 0;
    }
    return res;
}

events__e ad3551__handler_set(ad3551__handler_t handler) {
    ad3551_handler = handler;
    return EVENT__OK;
}

static events__e ad3551__rw(ad3551__struct_t* d, ad3551__rw_e op, uint32_t reg, uint32_t data) {
    events__e res = EVENT__ERROR;
    int len = reg ? ad3551__reg_len_get(reg) : 1;
    d->len = len;
    d->pkt.addr = reg ? reg + len - 1 : reg;   // descending
    switch(op) {
        case AD3551__READ:
            d->pkt.rw = 1;
            res = spi__txrx(d->dev_id, (uint8_t*)&d->pkt, len + 1, (uint8_t*)&d->pkt, 1, d->cb ? ad3551__spi_cb : NULL, (void*)d);
            if ((res == EVENT__OK) && (d->cb == NULL) && (d->data)) {
                *d->data = 0;
                for(int i = 0; i < len; i++)
                    *d->data |= (uint32_t)d->pkt.data[i] << ((len - i - 1) * 8);
            }
            break;
        case AD3551__WRITE:
            d->pkt.rw = 0;
            for(int i = 0; i < len; i++)
                d->pkt.data[i] = data >> ((len - i - 1) * 8);
            res = spi__txrx(d->dev_id, NULL, 0, (uint8_t*)&d->pkt, len + 1, d->cb ? ad3551__spi_cb : NULL, (void*)d);
            break;
    }
    return res;
}






static void ad3551__spi_cb(int dev_id, events__e event, uint8_t *buff, int len, void *ext_data) {
    ad3551__struct_t* d = (ad3551__struct_t*)ext_data;
    if ((event == EVENT__OK) || (*d->oper == AD3551__OP_WR_RST) || (*d->oper == AD3551__OP_READ_RST)) {  // игнорим ошибки- ждем ресета микросхемы
        do {
            ++d->oper;
            event = ad3551__cmd_exec(d);
        } while (event == EVENT__CONTINUE);
    }
    if ((event != EVENT__OK) || (*d->oper == AD3551__OP_NONE)) {
        d->busy = 0;
        if (d->cb)
            d->cb(dev_id, event, (d->pkt.addr && len) ? d->pkt.addr + len - 1 : d->pkt.addr, d->data_int, d->ext_data);
    }
}

static events__e ad3551__cmd_exec(ad3551__struct_t* d) {
    events__e event = EVENT__OK;
    switch(*d->oper) {
        case AD3551__OP_READ_ID:
            d->data = &d->data_int;
            event = ad3551__rw(d, AD3551__READ, AD3551_REG->product_id, 0);
            break;
        case AD3551__OP_CHK_ID:
            event = ((!d->data_int) || (d->data_int == 0xFFFF)) ? EVENT__HW_ERROR : EVENT__CONTINUE;
            break;
        case AD3551__OP_SET_RST:
            event = ad3551__rw(d, AD3551__WRITE, AD3551_REG->itf_cfg_a, 0x91);
            break;
        case AD3551__OP_CLR_RST:
            event = ad3551__rw(d, AD3551__WRITE, AD3551_REG->itf_cfg_a, 0x10);
            d->err_cnt = 0;
            break;
        case AD3551__OP_WR_RST:
            event = ad3551__rw(d, AD3551__WRITE, AD3551_REG->scratch_pad, 0xA5);
            if (!d->cb)
                event = EVENT__OK;
            break;
        case AD3551__OP_READ_RST:
            d->data_int = 0x00;
            d->data = &d->data_int;
            event = ad3551__rw(d, AD3551__READ, AD3551_REG->scratch_pad, 0);
            if (!d->cb)
                event = EVENT__OK;
            break;
        case AD3551__OP_CHK_RST:
            if (d->data_int != 0xA5) {
                d->oper -= 3;
                event = (++d->err_cnt < 200) ? EVENT__CONTINUE : EVENT__ERROR;
            }
            break;
        case AD3551__OP_READ:
            event = ad3551__rw(d, AD3551__READ, d->reg, 0);
            break;
        case AD3551__OP_RD_CHK:
            d->data_int = 0;
            if (d->len > 2)
                d->len = 2;
            for(int i = 0; i < d->len; i++)
                d->data_int |= (uint32_t)d->pkt.data[i] << ((d->len - i - 1) * 8);
            if (d->data)
                *d->data = d->data_int;
            event = EVENT__CONTINUE;
            break;
        case AD3551__OP_WRITE:
            event = ad3551__rw(d, AD3551__WRITE, d->reg, d->data_int);
            break;
        case AD3551__OP_L_VOUT:
            event = ad3551__rw(d, AD3551__WRITE, AD3551_REG->sw_ldac_24b, 1);
            break;
        case AD3551__OP_MODE_CLR_FAST:
            event = ad3551__rw(d, AD3551__WRITE, AD3551_REG->ch_select_16b, 0);
            break;
        case AD3551__OP_MODE_CLR_PREC:
            event = ad3551__rw(d, AD3551__WRITE, AD3551_REG->ch_select_24b, 0);
            break;
        default:
            break;
    }
    return event;
}


static ad3551__struct_t* ad3551__free_get(int dev_id) {
    for(int i = 0; i < AD3551__COUNT; i++) {
        if (ad3551__data[i].busy == 0) {
            ad3551__data[i].dev_id = dev_id;
            return &ad3551__data[i];
        }
    }
    return NULL;
}

static int ad3551__reg_len_get(uint32_t addr) {
    int res = 1;
    if ((addr == 0x04) || (addr == 0x0C) || ((addr >= 0x28) && (addr <= 0x33)))
        res = 2;
    else if (addr >= 0x37)
        res = 3;
    return res;
}

