﻿/***************************************************************************//**
 * @file uart.c
 * @brief Драйвер UART с концепцией uart__open -> uart__close.
 * @authors a.tushetsov
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "DRV/uart.h"
#if (defined HW_UART_COUNT) || (defined USB_UART_COUNT) || (defined BB_UART_COUNT)
#include "System/sw_timer.h"
#include "System/list.h"
#include "System/framework.h"

#ifdef HW_UART_COUNT
    #include "DRV/hw_uart.h"
#endif
#ifdef USB_UART_COUNT
    #include "DRV/hw_uart_usb.h"
#endif

// Прототипы функций для вызова аппаратнозависимой части.
typedef events__e (*uart__hw_init_func_t)(i32 uart_id, u32 settings, uart__hw_event_handler_t hw_event_handler);
typedef events__e (*uart__hw_deinit_func_t)(i32 uart_id);
typedef events__e (*uart__hw_tx_func_t)(i32 uart_id, u8 *buff, i32 len, hw_uart__cb_t tx_isr_cb);
typedef events__e (*uart__hw_rx_func_t)(i32 uart_id, hw_uart__cb_t rx_isr_cb);
typedef void (*uart__hw_rx_flush_func_t)(i32 uart_id);

// Структура с прототипами функций для вызова аппаратнозависимой части.
typedef struct {
    uart__hw_init_func_t hw_init_func;          // Указатель на функцию инициализации аппаратной части UART.
    uart__hw_deinit_func_t hw_deinit_func;      // Указатель на функцию деинициализации аппаратной части UART.
    uart__hw_rx_func_t hw_rx_func;              // Указатель на функцию приема аппаратной части UART.
    uart__hw_tx_func_t hw_tx_func;              // Указатель на функцию передачи аппаратной части UART.
    uart__hw_rx_flush_func_t hw_flush_rx_func;  // Указатель на функцию очистки приемного буфера UART.
} uart__hw_descr_t;

// Таблица привязок к аппаратной части для различных видов UART.
static const uart__hw_descr_t uart__table[] = {
#ifdef HW_UART_COUNT
    { hw_uart__init, hw_uart__deinit, hw_uart__rx, hw_uart__tx, hw_uart__rx_flush },
#else
    { 0, 0, 0, 0, 0 },
#endif
#ifdef USB_UART_COUNT
    { hw_uart_usb__init, hw_uart_usb__deinit, hw_uart_usb__rx, hw_uart_usb__tx, hw_uart_usb__rx_flush },
#else
    { 0, 0, 0, 0, 0 },
#endif
#ifdef BB_UART_COUNT
    { bb_uart__init, bb_uart__deinit, bb_uart__rx, bb_uart__tx, bb_uart__rx_flush },
#else
    { 0, 0, 0, 0, 0 },
#endif
};

// Структура заголовка списка подписчиков на захват UART().
typedef struct {
    uart__sub_t* prev;
    uart__sub_t* next;
} uart__head_t;

// Состояние основного автомата UART.
typedef enum {
    UART__FSM_STATE_OFF,
    UART__FSM_STATE_HW_INIT,
    UART__FSM_STATE_WORK,
    UART__FSM_STATE_IDLE,
    UART__FSM_STATE_DEINIT,
} uart__fsm_state_e;

// Состояние автомата старт-стоп последовательности UART.
typedef enum {
    UART__FSM_SEQ_IDLE   = 0,
    UART__FSM_SEQ_START  = 1,
    UART__FSM_SEQ_STOP   = 2,
    UART__FSM_SEQ_CB     = 3,
} uart__fsm_seq_e;

// Структура-описатель UART (индивидуальна для каждого UART).
typedef struct {
    u8 seq_pos;
    u8 sequence[UART__START_STOP_SEC_LEN_MAX];
    uart__fsm_seq_e seq_state;
    events__e tx_event, rx_event;
    uart__fsm_state_e state, rx_state;
    i32 rx_len, rx_bytes_count, rx_cnt_3_5, timeout;
    uart__cb_t tx_cb, rx_cb;
    uart__head_t subs;
    uart__sub_t* cur_sub;
    sw_timer__t rx_timer;
    i32 wait_f;
    void* rx_ext_data, *tx_ext_data;
    u8* rx_buff;
    u32 rx_frame_event;
} uart__t;

static framework__sub_t uart_sub;

#ifdef HW_UART_COUNT
    static uart__t uart__hw_uarts[HW_UART_COUNT];
#endif
#ifdef USB_UART_COUNT
    static uart__t uart__usb_uarts[USB_UART_COUNT];
#endif
#ifdef BB_UART_COUNT
    static uart__t uart__bb_uarts[BB_UART_COUNT];
#endif

static const u32 uart__speeds[] = {
    110, 300, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600
};


static i32 uart__fsm(uart__t *uarts, i32 cnt);
static void uart__rx_fsm(uart__t* uart);
static i32 uart__rx_hw_callback(i32 uart_id, events__e event, u8* buff, i32 len);
static i32 uart__tx_hw_callback(i32 uart_id, events__e event, u8* buff, i32 len);
static void uart__hw_event_handler(i32 uart_id, events__e event, void* data);
static void uart__rx_timeout_cb(sw_timer__t* timer, void* ext_data);
static uart__t* uart__ptr_from_id_get(i32 uart_id);
static void uart__cout(void);
static i32 uart__find_seq(u8* buff, i32 len, u8* seq, u8 seq_len);

/*******************************************************************************
 *  Функция начальной инициализации библиотеки UART.
 ******************************************************************************/
void uart__init(void) {
    framework__cout_subscribe(&uart_sub, uart__cout);
#ifdef HW_UART_COUNT
    for (i32 i = 0; i < HW_UART_COUNT; i++) {
        list__init(&uart__hw_uarts[i].subs);
    }
#endif
#ifdef USB_UART_COUNT
    for (i32 i = 0; i < USB_UART_COUNT; i++) {
        list__init(&uart__usb_uarts[i].subs);
    }
#endif
}

/*******************************************************************************
 *  Функция открытия UART.
 ******************************************************************************/
events__e uart__open(uart__sub_t* sub, i32 uart_id, u32 settings, uart__event_handler_t event_handler, void* ext_data) {
    events__e res = EVENT__BUSY;
    uart__t* uart = uart__ptr_from_id_get(uart_id);
    if (uart && !list__is_item(&uart->subs, sub)) {
        sub->uart_id = uart_id;
        sub->settings = settings;
        sub->event_handler = event_handler;
        sub->ext_data = ext_data;
        list__include(&uart->subs, sub, LIST__TAIL);
        res = EVENT__OK;
    }
    return res;
}

/*******************************************************************************
 * Функция постановки на прием данных с указанием критериев завершения операции и вызова коллбэка.
 ******************************************************************************/
events__e uart__rx(uart__sub_t* sub, u32 frame_event, u8* buff, i32 len, i32 timeout, uart__cb_t cb, void* ext_data) {
    uart__fsm_seq_e start_stop = UART__FSM_SEQ_IDLE;
    events__e res = EVENT__BUSY;
    uart__t* uart = uart__ptr_from_id_get(sub->uart_id);
    if (uart && (uart->cur_sub == sub) && (uart->state == UART__FSM_STATE_WORK) && (uart->rx_event == EVENT__CONTINUE)) {
        switch (frame_event & UART__FRAME_MASK) {
            case UART__FRAME_START_SEC:
            case UART__FRAME_START_SEC | UART__FRAME_STOP_SEC:
                start_stop = UART__FSM_SEQ_START;
                break;
            case UART__FRAME_STOP_SEC:
                start_stop = UART__FSM_SEQ_STOP;
                break;
            default:
                break;
        }
        if (start_stop != UART__FSM_SEQ_IDLE) {
            if ((!(frame_event & UART__STOP_SEC_LEN_MASK)) || ((frame_event & UART__STOP_SEC_LEN_MASK) > UART__START_STOP_SEC_LEN_MAX))
                return EVENT__PARAM_NA;
            memcpy(uart->sequence, buff, frame_event & UART__STOP_SEC_LEN_MASK);
        }
        uart->seq_state = start_stop;
        uart->seq_pos = 0;
        uart->rx_event = EVENT__LOCK;
        uart->rx_buff = buff;
        uart->rx_len = len;
        uart->rx_frame_event = frame_event;
        uart->timeout = timeout;
        uart->rx_cb = cb;
        uart->rx_ext_data = ext_data;
        res = EVENT__OK;
    }
    return res;
}

/*******************************************************************************
 * Функция запуска процедуры передачи данных.
 ******************************************************************************/
events__e uart__tx(uart__sub_t* sub, u8* buff, i32 len, uart__cb_t cb, void* ext_data) {
    events__e res = EVENT__BUSY;
    uart__t* uart = uart__ptr_from_id_get(sub->uart_id);
    hw_uart__cb_t tx_cb = NULL;
    if (uart && (uart->cur_sub == sub) && (uart->state == UART__FSM_STATE_WORK) && (uart->tx_event == EVENT__CONTINUE)) {
        if (cb) {
            uart->tx_event = EVENT__LOCK;
            uart->tx_cb = cb;
            uart->tx_ext_data = ext_data;
            tx_cb = uart__tx_hw_callback;
        }
        res = (uart__table[sub->uart_id >> 8].hw_tx_func(sub->uart_id, buff, len, tx_cb) == EVENT__OK) ? EVENT__OK : EVENT__ERROR;
    }
    return res;
}

/*******************************************************************************
 * Функция закрытия UART.
 ******************************************************************************/
events__e uart__close(uart__sub_t* sub) {
    uart__t* uart = uart__ptr_from_id_get(sub->uart_id);
    if (uart) {
        if (list__is_item(&uart->subs, sub)) {
            list__exclude(sub, LIST__THIS);
            if (sub != uart->cur_sub)
                uart->cur_sub->event_handler(uart->cur_sub, EVENT__CLOSE, uart->cur_sub->ext_data);
        }
    }
    return EVENT__OK;
}

/*******************************************************************************
 * Служебная функция, возвращающая скорость UART, соответствующую маски скорости.
 ******************************************************************************/
u32 uart__br_to_speed(u32 br_settings) {
    u32 br = (br_settings & UART__BR_MASK) - 1;
    if (br >= sizeof(uart__speeds) / sizeof(uart__speeds[0]))
        return 0;
    return uart__speeds[br];
}

/*******************************************************************************
 * Служебная функция, возвращающая маску скорости UART, соответствующую скорости.
 ******************************************************************************/
u32 uart__speed_to_br(u32 speed) {
    for (i32 i = 0; i < sizeof(uart__speeds) / sizeof(uart__speeds[0]); i++)
        if (speed == uart__speeds[i])
            return i + 1;
    return 0;
}

/*******************************************************************************
 * Служебная функция проверки, открыт ли UART.
 ******************************************************************************/
events__e uart__is_open(i32 uart_id) {
    uart__t* uart = uart__ptr_from_id_get(uart_id);
    if (uart && (uart->rx_state == UART__FSM_STATE_WORK))
        return EVENT__OPEN;
    return EVENT__CLOSE;
}

/*******************************************************************************
 * Функция внешнего вызова для обработки текущего состояния всех UART. Добавляется в список коллаутов, вызываемых framework.
 ******************************************************************************/
static void uart__cout(void) {
#ifdef HW_UART_COUNT
    hw_uart__cout();
    uart__fsm(uart__hw_uarts, HW_UART_COUNT);
#endif
#ifdef USB_UART_COUNT
    hw_uart_usb__cout();
    uart__fsm(uart__usb_uarts, USB_UART_COUNT);
#endif
}

/*******************************************************************************
 * Автомат UART.
 ******************************************************************************/
static i32 uart__fsm(uart__t *uarts, i32 cnt) {
    for (i32 i = 0; i < cnt; i++) {
        uart__t *uart = uarts + i;
        switch (uart->state) {
            case UART__FSM_STATE_OFF:    // Проверяем список подписчиков.
                uart->cur_sub = list__foreach(&uart->subs, NULL, LIST__HEAD);
                if (uart->cur_sub) {
                    uart->state = UART__FSM_STATE_HW_INIT;
                    uart->wait_f = 1;
                    uart__table[uart->cur_sub->uart_id >> 8].hw_init_func(uart->cur_sub->uart_id, uart->cur_sub->settings, uart__hw_event_handler);
                }
                break;
            case UART__FSM_STATE_HW_INIT:    // Запускаем модуль UART.
                if (uart->wait_f == 0) {
                    uart->tx_event = uart->rx_event = EVENT__CONTINUE;
                    uart->state = UART__FSM_STATE_WORK;
                    uart->rx_state = UART__FSM_STATE_IDLE;
                    uart__table[uart->cur_sub->uart_id >> 8].hw_flush_rx_func(uart->cur_sub->uart_id);
                    uart__table[uart->cur_sub->uart_id >> 8].hw_rx_func(uart->cur_sub->uart_id, NULL);    // Встаем на прием во внутреннеий буфер приемника.
                    uart->cur_sub->event_handler(uart->cur_sub, EVENT__OPEN, uart->cur_sub->ext_data);
                }
                break;
            case UART__FSM_STATE_WORK:    // Ожидаем закрытия UART
                if (uart->cur_sub != list__foreach(&uart->subs, NULL, LIST__HEAD))
                    uart->state = UART__FSM_STATE_IDLE;
                uart__rx_fsm(uart);
                break;
            case UART__FSM_STATE_IDLE: {   // Ищем еще желающих пользоваться UARTом.
                i32 uart_id = uart->cur_sub->uart_id;
                if (uart->rx_event != EVENT__CONTINUE)
                    uart->rx_cb(uart->cur_sub, EVENT__CLOSE, uart->rx_buff, uart->rx_bytes_count, uart->rx_ext_data);
                if (uart->tx_event != EVENT__CONTINUE)
                    uart->tx_cb(uart->cur_sub, EVENT__CLOSE, NULL, 0, uart->tx_ext_data);
                uart->cur_sub->event_handler(uart->cur_sub, EVENT__CLOSE, uart->cur_sub->ext_data);
                uart->cur_sub = list__foreach(&uart->subs, NULL, LIST__HEAD);
                if (uart->cur_sub) {
                    uart->state = UART__FSM_STATE_HW_INIT;
                    uart->wait_f = 1;
                    uart__table[uart->cur_sub->uart_id >> 8].hw_init_func(uart->cur_sub->uart_id, uart->cur_sub->settings, uart__hw_event_handler);
                }
                else {
                    uart->state = UART__FSM_STATE_DEINIT;
                    uart__table[uart_id >> 8].hw_deinit_func(uart_id);
                }
                break;
            }
            case UART__FSM_STATE_DEINIT:    // Ждем выключения ITF...
                if (uart->wait_f == 0)
                    uart->state = UART__FSM_STATE_OFF;
                break;
        }
    }
    return 0;
}

/*******************************************************************************
 * Автомат приема. Проверяет состояние приемной части интерфейса и вызывает колбэк.
 ******************************************************************************/
static void uart__rx_fsm(uart__t* uart) {
    i32 uart_id = uart->cur_sub->uart_id;
    switch (uart->rx_state) {
        case UART__FSM_STATE_IDLE:
        default:
            if (uart->rx_event != EVENT__CONTINUE) {
                uart->rx_state = UART__FSM_STATE_WORK;
                    uart->rx_bytes_count = uart->rx_cnt_3_5 = 0;
                if (uart__table[uart_id >> 8].hw_rx_func(uart_id, uart__rx_hw_callback) != EVENT__OK)
                    uart->rx_event = EVENT__ERROR;
                else if (uart->timeout)
                    sw_timer__start(&uart->rx_timer, uart->timeout, uart__rx_timeout_cb, uart);
            }
            break;
        case UART__FSM_STATE_WORK:
            switch (uart->rx_frame_event & UART__FRAME_MASK) {
                case UART__FRAME_START_SEC:
                case UART__FRAME_COUNT:
                    if (uart->rx_event == EVENT__NO_MEM)  // не смогли сохранить- они вернутся позже...
                        uart->rx_event = EVENT__OK;
                    break;
                case UART__FRAME_3_5:
                    if ((!uart->rx_cnt_3_5) && (uart->rx_bytes_count)) {
                        uart->rx_cnt_3_5 = uart->rx_bytes_count;
                        sw_timer__start(&uart->rx_timer, 2, uart__rx_timeout_cb, uart);
                    }
                    break;
                default:
                    break;
            }
            if (uart->rx_event != EVENT__LOCK) {
                sw_timer__stop(&uart->rx_timer);
                events__e cb_event = uart->rx_event;
//                uart->seq_state = UART__FSM_SEQ_IDLE;
                uart->seq_pos = 0;
                uart->rx_event = EVENT__CONTINUE;
                uart->rx_cb(uart->cur_sub, cb_event, uart->rx_buff, uart->rx_bytes_count, uart->rx_ext_data);
                if (uart->rx_event == EVENT__CONTINUE)
                    uart->rx_state = UART__FSM_STATE_DEINIT;
                else {
                    uart->rx_bytes_count = uart->rx_cnt_3_5 = 0;
                    if (uart__table[uart_id >> 8].hw_rx_func(uart_id, uart__rx_hw_callback) != EVENT__OK)
                        uart->rx_event = EVENT__ERROR;
                    else if (uart->timeout)
                        sw_timer__start(&uart->rx_timer, uart->timeout, uart__rx_timeout_cb, uart);
                }
            }
            break;
        case UART__FSM_STATE_DEINIT:
            uart__table[uart_id >> 8].hw_flush_rx_func(uart_id);
            uart->rx_state = UART__FSM_STATE_IDLE;
            break;
    }
}

/*******************************************************************************
 * Коллбэк аппаратного UART по приему данных.
 ******************************************************************************/
static i32 uart__rx_hw_callback(i32 uart_id, events__e event, u8* buff, i32 len) {
    uart__t* uart = uart__ptr_from_id_get(uart_id);
    i32 res = 0, seq_idx = 0;
    if ((uart == NULL) || (uart->rx_state != UART__FSM_STATE_WORK) || (uart->seq_state == UART__FSM_SEQ_CB))
        return len;
    if ((uart->rx_bytes_count + len) >= uart->rx_len) {
        // Возвращаем сколько байт не смогли сохранить- они вернутся позже...
        res = len - (uart->rx_len - uart->rx_bytes_count);
        len -= res;
        if (event == EVENT__OK)
            event = EVENT__NO_MEM;
    }
    switch(uart->seq_state) {                    // todo проверить на нулевую длину
        case UART__FSM_SEQ_START:
            seq_idx = uart__find_seq(buff, len, &uart->sequence[uart->seq_pos], (uart->rx_frame_event & UART__STOP_SEC_LEN_MASK) - uart->seq_pos);
            if ((seq_idx < 0) || (uart->seq_pos && (seq_idx))) {
                uart->seq_pos = seq_idx = len = 0;
                uart->rx_bytes_count = 0;
                if (event == EVENT__NO_MEM)
                    event = EVENT__LOCK;
                break;
            }
            uart->seq_pos++;
            res += len - seq_idx - 1;
            len = 1;
            if (uart->seq_pos < (uart->rx_frame_event & UART__STOP_SEC_LEN_MASK))
                break;
            uart->seq_pos = seq_idx = 0;
            uart->seq_state = (uart->rx_frame_event & UART__FRAME_STOP_SEC) ? UART__FSM_SEQ_STOP : UART__FSM_SEQ_CB;
            break;
        case UART__FSM_SEQ_STOP:
            seq_idx = uart__find_seq(buff, len, &uart->sequence[uart->seq_pos], (uart->rx_frame_event & UART__STOP_SEC_LEN_MASK) - uart->seq_pos);
            if ((seq_idx < 0) || (uart->seq_pos && (seq_idx))) {
                uart->seq_pos = seq_idx = 0;
                break;
            }
            uart->seq_pos++;
            res += len - seq_idx - 1;
            len = seq_idx + 1;
            seq_idx = 0;
            if (uart->seq_pos < (uart->rx_frame_event & UART__STOP_SEC_LEN_MASK))
                break;
            uart->rx_event = EVENT__OK;
            uart->seq_state = UART__FSM_SEQ_CB;
            break;
        default:
            break;
    }
    memcpy(uart->rx_buff + uart->rx_bytes_count, buff + seq_idx, len);
    uart->rx_bytes_count += len;
    if (event != EVENT__OK)
        uart->rx_event = event;
    return res;
}

/*******************************************************************************
 *  Коллбэк аппаратного UART по окончанию передачи.
 ******************************************************************************/
static i32 uart__tx_hw_callback(i32 uart_id, events__e event, u8* buff, i32 len) {
    uart__t* uart = uart__ptr_from_id_get(uart_id);
    if (uart != NULL) {
        uart->tx_event = EVENT__CONTINUE;
        uart->tx_cb(uart->cur_sub, event, buff, len, uart->tx_ext_data);
    }
    return 0;
}

/*******************************************************************************
 *  Обработчик rx-таймера 3.5 символа
 ******************************************************************************/
static void uart__rx_timeout_cb(sw_timer__t* timer, void* ext_data) {
    uart__t* uart = (uart__t*) ext_data;
    if ((uart == NULL) || (uart->rx_state != UART__FSM_STATE_WORK))
        return;
    if ((uart->rx_bytes_count != uart->rx_cnt_3_5) && (uart->rx_cnt_3_5)) {
        uart->rx_cnt_3_5 = uart->rx_bytes_count;
        sw_timer__start(timer, 2, uart__rx_timeout_cb, ext_data);
    }
    else if (uart->rx_bytes_count)
        uart->rx_event = EVENT__OK;
    else
        uart->rx_event = EVENT__TIMEOUT;
}


/*******************************************************************************
 * Функция обработки результатов инициализации hw-интерфейсов
 ******************************************************************************/
static void uart__hw_event_handler(i32 uart_id, events__e event, void* data) {
    uart__t* uart = uart__ptr_from_id_get(uart_id);
    if (uart) {
        uart->wait_f = 0;
        if ((event == EVENT__CLOSE) && (uart->cur_sub))
            uart__close(uart->cur_sub);
    }
}

/*******************************************************************************
 *  Функция поиска указателя на структуру экземпляра UART по пользовательскому ID.
 ******************************************************************************/
static uart__t* uart__ptr_from_id_get(i32 uart_id) {
    switch (uart_id & UART__TYPE_ID_MSK) {
#ifdef HW_UART_COUNT
        case UART__TYPE_HW:
            return &uart__hw_uarts[uart_id & UART__ID_MSK];
#endif
#ifdef USB_UART_COUNT
        case UART__TYPE_USB:
            return &uart__usb_uarts[uart_id & UART__ID_MSK];
#endif
#ifdef BB_UART_COUNT
        case UART__TYPE_BB:
            return &uart__bb_uarts[uart_id & UART__ID_MSK];
#endif
        default:
            return NULL;
    }
}

/*******************************************************************************
 *  Функция поиска последовательности старт-стоп в буфере. Возвращает индекс элемента.
 ******************************************************************************/
static i32 uart__find_seq(u8* buff, i32 len, u8* seq, u8 seq_len) {
    i32 idx = 0, pos = 0;
    for(; pos < len; pos++) {
        if (buff[pos] != seq[idx]) {
            idx = 0;
            continue;
        }
        idx++;
        if (idx >= seq_len)
            break;
    }
    return idx ? pos - idx + 1 : -1;
}

#else
void uart__init(){}
events__e uart__open(uart__sub_t* sub, i32 uart_id, u32 settings, uart__event_handler_t event_handler, void* ext_data) {return EVENT__ERROR;}
events__e uart__rx(uart__sub_t* sub, u32 frame_event, u8* buff, i32 len, i32 timeout_ms, uart__cb_t cb, void* ext_data) {return EVENT__ERROR;}
events__e uart__tx(uart__sub_t* sub, u8* buff, i32 len, uart__cb_t cb, void* ext_data) {return EVENT__ERROR;}
events__e uart__close(uart__sub_t* sub) {return EVENT__ERROR;}
u32 uart__br_to_speed(u32 br_settings) {return 0;}
u32 uart__speed_to_br(u32 speed) {return 0;}
events__e uart__is_open(i32 uart_id) {return EVENT__ERROR;}
#endif
