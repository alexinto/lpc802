﻿/***************************************************************************//**
 * @file ada4255.с.
 * @brief Драйвер микросхемы ADA4255.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "DRV/ADA4255/ada4255.h"

#pragma pack(push, 1)
typedef struct {
    uint8_t addr_hi :5;
    uint8_t mode    :2;
    uint8_t rw      :1;
    uint8_t addr_lo;
    uint8_t data;
}ada4255__spi_t;                    // MSB first
#pragma pack (pop)

events__e ada4255__read(int id, uint16_t addr, uint8_t* data, int size) {
    ada4255__spi_t buff = {.mode = 3, .rw = 1};
    for (int i = 0; i < size; i++, addr++) {
        buff.addr_hi = addr >> 8;
        buff.addr_lo = addr;
        spi__txrx(id, (uint8_t*)&buff, sizeof(buff), (uint8_t*)&buff, 2, NULL, NULL);
        data[i] = buff.data;
    }
    return EVENT__OK;
}

events__e ada4255__write(int id, uint16_t addr, uint8_t* data, int size) {
    ada4255__spi_t buff = {.mode = 3, .rw = 0, .addr_hi = addr >> 8};
    for (int i = 0; i < size; i++, addr++) {
        buff.addr_hi = addr >> 8;
        buff.addr_lo = addr;
        buff.data = data[i];
        spi__txrx(id, NULL, 0, (uint8_t*)&buff, sizeof(buff), NULL, NULL);
    }
    return EVENT__OK;
}
