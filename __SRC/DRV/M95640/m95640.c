/***************************************************************************//**
 * @file m95640.c.
 * @brief Драйвер микросхемы m95640.
 * @authors a.khamidullin.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "System/sw_timer.h"
#include "DRV/M95640/m95640.h"
#include "DRV/spi.h"

#define M95640__PAGE_SIZE               32
#define M95640__EEPROM_TOTAL_SIZE       8196

#define M95640__CMD_ADDR_HEADER_SIZE    3
#define M95640__BUFF_SIZE               M95640__PAGE_SIZE + M95640__CMD_ADDR_HEADER_SIZE

#ifndef M95640__COUNT
#define M95640__COUNT 1
#endif

#define M95640__EEPROM_CMD_WREN    		0x06    // Write Enable
#define M95640__EEPROM_CMD_WRDI    		0x04    // Write Disable
#define M95640__EEPROM_CMD_RDSR    		0x05    // Read Status Register
#define M95640__EEPROM_CMD_WRSR    		0x01    // Write Status Register
#define M95640__EEPROM_CMD_READ    		0x03    // Read from Memory Array
#define M95640__EEPROM_CMD_WRITE   		0x02    // Write to Memory Array

#define M95640__EEPROM_STATUS_SRWD    	0x80    // Status Register Write Disable
#define M95640__EEPROM_STATUS_BP      	0x0C    // Block Protect
#define M95640__EEPROM_STATUS_WEL     	0x02    // Write Enable
#define M95640__EEPROM_STATUS_WIP     	0x01    // Write in Progress
#define M95640__EEPROM_STATUS_MSQ		0xF0	// Most significant quartet for masking

#define SPI_DUMMY        0xFF

static u8 M95640__GET_STATUS[2] = {M95640__EEPROM_CMD_RDSR, 0xFF};

static u8 M95640__internal_tx_buf[2];
static u8 M95640__internal_rx_buf[2];

static u8 M95460__txrx_buf[M95640__BUFF_SIZE] = {0};

#pragma pack(push, 1)
typedef struct {
    u8 addr_hi;
    u8 addr_low;
    u8 data[32];
}m95640__pkt_t;
#pragma pack(pop)

typedef enum {
    M95640__OP_NONE  = 0,
    M95640__OP_READ,
    M95640__OP_WRITE,
    M95640__OP_INIT,
    M95640__OP_WR_IN_PROGRESS_CHECKOUT,
    M95640__OP_WR_ENABLING,
}m95640__oper_e;

typedef struct {
    m95640__pkt_t pkt;
    u8 id;
    u16 page_align;
    m95640__oper_e oper;
    m95640__oper_e oper_stored;
    i32 err_counter;
    u32 addr, offset;
    u8* buff;
    i32 len;
    u8* buff_stored;
    i32 len_stored;
    m95640__cb_t cb;
    void* ext_data;
    sw_timer__t timer;
    u8 write_in_progress_checked;
    u8 write_enabled;
}m95640__data_t;

static m95640__data_t m95640__data[M95640__COUNT];

//static framework__sub_t m95640_sub;

static void m95640__init_cb(i32 id, events__e event, u8 *buff, i32 len, void* ext_data);
static void m95640__spi_cb(i32 dev_id, events__e event, u8 *buff, i32 len, void* ext_data);
static events__e m95640__wr_exec(m95640__data_t* p_data, events__e event);
static events__e m95640__rd_exec(m95640__data_t* p_data, events__e event);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static void m95640__enable_write(m95640__data_t* p_data);

events__e m95640__init(u8 id, m95640__cb_t cb, void* ext_data) {
    events__e res = EVENT__OK;

    m95640__data_t* d = &m95640__data[id];
    if (d->oper != M95640__OP_NONE)
        return EVENT__BUSY;

/*	if (m95640_sub.cout == NULL) {
		framework__cout_subscribe(&m95640_sub, m95640__cout);
//		for(u8 i=0; i< M95640__COUNT; i++)
//			m95640__data[i].oper = M95640__OP_NONE;
	}*/

    d->id = id;
    d->cb = cb;
    d->ext_data = ext_data;
    d->write_in_progress_checked = 0;
    d->write_enabled = 0;
    uint32_t spi_sett = SPI__CPHA_0_CPOL_0 | SPI__DATA_FRAME_8_BIT | SPI__MSB | SPI__MODE_MASTER | (1 << SPI__BR_POS);
    spi__init(id, spi_sett);

    spi__txrx(id, M95640__internal_rx_buf, sizeof(M95640__internal_rx_buf), M95640__GET_STATUS, sizeof(M95640__GET_STATUS), cb ? m95640__init_cb : NULL, ext_data);

    d->oper = M95640__OP_INIT;

    if ((cb == NULL) || (res != EVENT__OK))
        d->oper = M95640__OP_NONE;
    return res;
}


static void m95640__init_cb(i32 id, events__e event, u8 *buff, i32 len, void* ext_data) {
    m95640__data_t* d = &m95640__data[id];

    if( ( M95640__internal_rx_buf[1] & M95640__EEPROM_STATUS_MSQ ) == M95640__EEPROM_STATUS_SRWD ) {
        d->oper = M95640__OP_NONE;
        if (d->cb)
            d->cb(d - m95640__data, event, 0, 0, 0, d->ext_data);
    }
}


static void m95640__ensure_end_of_write(m95640__data_t* p_data) {
    u8 cmd[2] = {M95640__EEPROM_CMD_RDSR, SPI_DUMMY};
    M95640__internal_tx_buf[0] = M95640__EEPROM_CMD_RDSR;
    M95640__internal_tx_buf[1] = SPI_DUMMY;
//    u8 rx_buff[2] = {0xFF, 0xFF};

    if (p_data->oper == M95640__OP_WR_IN_PROGRESS_CHECKOUT)
        return;
    else {
        p_data->oper_stored = p_data->oper;
        p_data->oper = M95640__OP_WR_IN_PROGRESS_CHECKOUT;
    }

    if(p_data->cb) {
        spi__txrx(p_data->id, M95640__internal_rx_buf, sizeof(M95640__internal_rx_buf), M95640__internal_tx_buf, sizeof(M95640__internal_tx_buf), m95640__spi_cb, NULL);
    } else {
        do {
            spi__txrx(p_data->id, M95640__internal_rx_buf, sizeof(M95640__internal_rx_buf), cmd, sizeof(cmd), NULL, NULL);
        } while( M95640__internal_rx_buf[1] & M95640__EEPROM_STATUS_WIP );
        p_data->oper = p_data->oper_stored;
    }
    return;
}

static void m95640__enable_write(m95640__data_t* p_data) {
    M95640__internal_tx_buf[0] = M95640__EEPROM_CMD_WREN;
    M95640__internal_tx_buf[1] = SPI_DUMMY;

    if (p_data->oper == M95640__OP_WR_ENABLING)
        return;
    else {
        p_data->oper = M95640__OP_WR_ENABLING;
    }

    if(p_data->cb) {
        spi__txrx(p_data->id, M95640__internal_rx_buf, sizeof(M95640__internal_rx_buf), M95640__internal_tx_buf, sizeof(M95640__internal_tx_buf), m95640__spi_cb, NULL);
    } else {
        spi__txrx(p_data->id, M95640__internal_rx_buf, sizeof(M95640__internal_rx_buf), M95640__internal_tx_buf, sizeof(M95640__internal_tx_buf), NULL, NULL);
    }

    return;
}

events__e m95640__read(i32 id, u32 addr, u8* buff, i32 len, m95640__cb_t cb, void* ext_data)
{
    m95640__data_t* d = &m95640__data[id];
    if (d->oper != M95640__OP_NONE)
        return EVENT__BUSY;
    d->oper = M95640__OP_READ;
    d->cb = cb;
    d->id = id;
    d->ext_data = ext_data;
    d->addr = addr;
    d->buff = buff;
    d->len = len;
    d->offset = d->err_counter = 0;
    d->pkt.addr_low = addr;
    d->pkt.addr_hi = addr >> 8;
    d->page_align = 0;
    memset(M95460__txrx_buf, 0, sizeof(M95460__txrx_buf));

    return m95640__rd_exec(d, EVENT__OK);
}

events__e m95640__write(i32 id, u32 addr, u8* buff, i32 len, m95640__cb_t cb, void* ext_data) {
    m95640__data_t* d = &m95640__data[id];
    if (d->oper != M95640__OP_NONE)
        return EVENT__BUSY;
    d->oper = M95640__OP_WRITE;
    d->cb = cb;
    d->id = id;
    d->ext_data = ext_data;
    d->addr = addr;
    d->buff = buff;
    d->len = len;
    d->offset = d->err_counter = 0;
    d->pkt.addr_low = addr;
    d->pkt.addr_hi = addr >> 8;
    d->page_align = 0;
    d->write_in_progress_checked = 0;
    d->write_enabled = 0;
    return m95640__wr_exec(d, EVENT__OK);
}

static events__e m95640__wr_exec(m95640__data_t* p_data, events__e event) {
    m95640__data_t* d = p_data;

    if(d->page_align == 1){
        return EVENT__OK;
    }

    if(d->len > M95640__EEPROM_TOTAL_SIZE) {
        return EVENT__PARAM_NA;
    }

    if(d->len + d->addr > M95640__EEPROM_TOTAL_SIZE) {
        return EVENT__PARAM_NA;
    }

    memset(M95460__txrx_buf, 0, sizeof(M95460__txrx_buf));

    while (d->len > 0) {
        u32 page_end = (d->addr / M95640__PAGE_SIZE + 1) * M95640__PAGE_SIZE;
        u32 curr_len = page_end - d->addr;

        M95460__txrx_buf[0] = M95640__EEPROM_CMD_WRITE;
        M95460__txrx_buf[1] = (u8)(d->addr >> 8);
        M95460__txrx_buf[2] = (u8)(d->addr & 0xFF);
        memcpy(M95460__txrx_buf+M95640__CMD_ADDR_HEADER_SIZE, d->buff, curr_len);

        if(!d->write_in_progress_checked) {
            m95640__ensure_end_of_write(d);
            if (d->cb)
                return EVENT__OK;
        }

        if(!d->write_enabled) {
            m95640__enable_write(d);
            if (d->cb)
                return EVENT__OK;
        }

        event = spi__txrx(p_data->id, NULL, 0, M95460__txrx_buf, curr_len + M95640__CMD_ADDR_HEADER_SIZE, d->cb ? m95640__spi_cb : NULL, NULL);

        d->page_align = 1;
        d->len -= curr_len;
        d->addr += curr_len;
        d->buff += curr_len;
        d->write_in_progress_checked = 0;
        d->write_enabled = 0;
        if( d->cb ) {
            return EVENT__OK;
        }
    }

    if ((d->cb == NULL) || (d->err_counter >= 100) || (!d->page_align)) {
        d->oper = M95640__OP_NONE;
        if (d->cb)
            d->cb(d - m95640__data, event, d->addr, d->buff, d->len, d->ext_data);
    }
    else if (event != EVENT__OK)
        sw_timer__start(&d->timer, 1, timer_cb, (void*)d);

    return event;
}


static events__e m95640__rd_exec(m95640__data_t* p_data, events__e event) {
    m95640__data_t* d = p_data;

    if(d->page_align == 1){
        return EVENT__OK;
    }

    if(d->len > M95640__EEPROM_TOTAL_SIZE) {
        return EVENT__PARAM_NA;
    }

    if(d->len + d->addr > M95640__EEPROM_TOTAL_SIZE) {
        return EVENT__PARAM_NA;
    }

    u8 cmd[M95640__CMD_ADDR_HEADER_SIZE] = {0};

    while (d->len > 0) {
        u32 page_end = (d->addr / M95640__PAGE_SIZE + 1) * M95640__PAGE_SIZE;
        u32 curr_len = page_end - d->addr;

        cmd[0] = M95640__EEPROM_CMD_READ;
        cmd[1] = (u8)(d->addr >> 8);
        cmd[2] = (u8)(d->addr & 0xFF);

        if(!d->write_in_progress_checked) {
            m95640__ensure_end_of_write(d);
            if (d->cb)
                return EVENT__OK;
        }

        event = spi__txrx(p_data->id, M95460__txrx_buf, curr_len + M95640__CMD_ADDR_HEADER_SIZE, cmd, sizeof(cmd), d->cb ? m95640__spi_cb : NULL, NULL);

        if( (!d->cb) && ( event == EVENT__OK )) {
            memcpy(d->buff, M95460__txrx_buf+3, d->len);
        }

        d->page_align = 1;
        d->buff_stored = d->buff;
        d->len_stored = curr_len;

        d->len -= curr_len;
        d->addr += curr_len;
        d->buff += curr_len;
        if( d->cb ) {
            return EVENT__OK;
        }

    }

    if ((d->cb == NULL) || (d->err_counter >= 100) || (!d->page_align)) {
        d->oper = M95640__OP_NONE;
        if (d->cb)
            d->cb(d - m95640__data, event, d->addr, d->buff, d->len, d->ext_data);
    }
    else if (event != EVENT__OK)
        sw_timer__start(&d->timer, 1, timer_cb, (void*)d);

    return event;
}
//*/

static void m95640__spi_cb(i32 dev_id, events__e event, u8 *buff, i32 len, void* ext_data) {

    m95640__data_t* d = &m95640__data[dev_id];
    if (event == EVENT__OK) {
        d->page_align = 0;
        if (d->oper == M95640__OP_WRITE)
            m95640__wr_exec(d, event);
        else if (d->oper == M95640__OP_READ) {
            if ( (d->cb) && ( event == EVENT__OK ) ) {
                memcpy(d->buff_stored, M95460__txrx_buf+3, d->len_stored);
            }
            m95640__rd_exec(d, event);
        }
        else if (d->oper == M95640__OP_WR_IN_PROGRESS_CHECKOUT) {
            if ( !(M95640__internal_rx_buf[1] & M95640__EEPROM_STATUS_WIP) ) {
                d->write_in_progress_checked = 1;
            } else {
                d->write_in_progress_checked = 0;
            }
            d->oper = d->oper_stored;
            if(d->oper == M95640__OP_WRITE)
                m95640__wr_exec(d, event);
            else
                m95640__rd_exec(d, event);
        }
        else if(d->oper == M95640__OP_WR_ENABLING) {
            d->write_enabled = 1;
            d->oper = M95640__OP_WRITE;
            m95640__wr_exec(d, event);
        }
    }
    else
        sw_timer__start(&d->timer, 1, timer_cb, (void*)d);
}


static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    m95640__data_t* d = ext_data;
    if (d->oper == M95640__OP_WRITE)
        m95640__wr_exec(d, EVENT__ERROR);
    else
        m95640__rd_exec(d, EVENT__ERROR);
}
