/***************************************************************************//**
 * @file lcd2004.h.
 * @brief ������ ������ � ������������ �������
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include <stdio.h>
#include <string.h>
#include "System/sw_timer.h"
#include "DRV/i2c.h"
#include "DRV/lcd2004.h"

#ifndef LCD2004__COUNT
#define LCD2004__COUNT 1
#endif

#pragma pack(push, 1)
typedef struct {
    uint8_t rs     :1;
    uint8_t rw     :1;
    uint8_t e      :1;
    uint8_t led    :1;
    uint8_t data   :4;
}lcd2004__pkt_t;
#pragma pack(pop)

typedef enum {
    LCD2004__TYPE_CMD  = 0,
    LCD2004__TYPE_DATA = 1,
}lcd2004__type_e;

typedef struct {
    lcd2004__pkt_t pkt[4];
    uint8_t busy;
    int i2c_id, addr;
    int len;
    lcd2004__cb_t cb;
    void* ext_data;
    uint8_t* cur_ptr, *buff, *row;
    sw_timer__t timer;
}lcd2004__struct_t;

static lcd2004__struct_t lcd2004__data[LCD2004__COUNT];
//                                                                                 0x00 - end!
static const uint8_t lcd2004__row_addr[] = {0x80, 0xA8, 0x94, 0xBC, 0x00};
static const uint8_t lcd2004__init_tbl[] = {0x20, 0x2C, 0x80, 0x40, 0x01, 0x06, 0x0C, 0x00};


static void lcd2004__set_data(lcd2004__struct_t* d, lcd2004__type_e type, uint8_t data);
static void lcd2004__init_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);
static void lcd2004__i2c_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data);
static void init_tmr_cb(struct sw_timer__t *timer, void *ext_data);
static void data_tmr_cb(struct sw_timer__t *timer, void *ext_data);


events__e lcd2004__init(int id, int i2c_id, int addr, lcd2004__cb_t cb, void* ext_data) {
    if (id >= LCD2004__COUNT)
        return EVENT__PARAM_NA;
    lcd2004__struct_t* d = &lcd2004__data[id];
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->i2c_id = i2c_id;
    d->addr = addr;
    d->cur_ptr = (uint8_t*)lcd2004__init_tbl;
    i2c__init(i2c_id, 0, 0, NULL, NULL);
    sw_timer__start(&d->timer, 40, init_tmr_cb,  (void*)d);
    return EVENT__OK;
}

static void lcd2004__init_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
    lcd2004__struct_t* d = ext_data;
    if ((*++d->cur_ptr) && (event == EVENT__OK)) {
        sw_timer__start(&d->timer, 2, init_tmr_cb, ext_data);
        return;
    }
    d->busy = 0;
    if (d->cb)
        d->cb(d - lcd2004__data, event, d->ext_data);
}

static void init_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    lcd2004__struct_t* d = ext_data;
    lcd2004__set_data(d, LCD2004__TYPE_CMD, *d->cur_ptr);
    events__e event = i2c__txrx(d->i2c_id, d->addr, NULL, 0, (uint8_t*)&d->pkt, 4, lcd2004__init_cb, (void*)d);
    if (event != EVENT__OK) {
        d->busy = 0;
        if (d->cb)
            d->cb(d - lcd2004__data, event, d->ext_data);
    }
}

events__e lcd2004__set(int id, char* buff, int len, lcd2004__cb_t cb, void* ext_data) {
    lcd2004__struct_t* d = &lcd2004__data[id];
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->cur_ptr = d->buff = (uint8_t*)buff;
    d->len = len;
    d->row = (uint8_t*)lcd2004__row_addr;
    lcd2004__set_data(d, LCD2004__TYPE_CMD, *d->row);
    sw_timer__start(&d->timer, 1, data_tmr_cb, (void*)d);
    return EVENT__OK;
}

events__e lcd2004__clr(int id, lcd2004__cb_t cb, void* ext_data) {
    lcd2004__struct_t* d = &lcd2004__data[id];
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->cb = cb;
    d->ext_data = ext_data;
    d->cur_ptr = d->buff = NULL;
    d->len = 0;
    lcd2004__set_data(d, LCD2004__TYPE_CMD, 0x01);
    sw_timer__start(&d->timer, 5, data_tmr_cb, (void*)d);
    return EVENT__OK;
}

static void lcd2004__i2c_cb(int i2c_id, events__e event, int addr, uint8_t *buff, int len, void* ext_data) {
    lcd2004__struct_t* d = ext_data;
    if ((d->cur_ptr < d->buff + d->len) && (event == EVENT__OK)) {
        if (*d->cur_ptr == '\n')
            lcd2004__set_data(d, LCD2004__TYPE_CMD, *(d->row + 1) ? *++d->row : *d->row);
        else
            lcd2004__set_data(d, LCD2004__TYPE_DATA, *d->cur_ptr);
        d->cur_ptr++;
        sw_timer__start(&d->timer, 1, data_tmr_cb, ext_data);
        return;
    }
    d->busy = 0;
    if (d->cb)
        d->cb(d - lcd2004__data, event, d->ext_data);
}

static void data_tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    lcd2004__struct_t* d = ext_data;
    events__e event = i2c__txrx(d->i2c_id, d->addr, NULL, 0, (uint8_t*)&d->pkt, 4, lcd2004__i2c_cb, (void*)d);
    if (event != EVENT__OK) {
        d->busy = 0;
        if (d->cb)
            d->cb(d - lcd2004__data, event, d->ext_data);
    }
}

static void lcd2004__set_data(lcd2004__struct_t* d, lcd2004__type_e type, uint8_t data) {
    d->pkt[0].data = data >> 4;
    d->pkt[0].rs = type;
    d->pkt[0].rw = 0;      // ������ �����
    d->pkt[0].led = 1;     // ���������
    d->pkt[0].e = 1;       // �������������
    d->pkt[1] = d->pkt[2] = d->pkt[3] = d->pkt[0];
    d->pkt[1].e = d->pkt[3].e = 0;
    d->pkt[2].data = d->pkt[3].data = data;
}
