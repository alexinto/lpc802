﻿/***************************************************************************//**
 * @file spi.с.
 * @brief Модуль, реализующий управление программным SPI.
 * @author a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "DRV/spi.h"

WEAK_FUNC const spi__cfg_tbl_t spi_cfg__tbl[SPI__DEV_COUNT];
extern const spi__cfg_tbl_t spi_cfg__tbl[];

typedef struct {
    u8 busy;
    u32 sett_wr; // Настойки для операции записи
    u32 sett_rd; // Настойки для операции чтения
    spi__cb_t callback;
    void *ext_data;
}spi__struct_t;

static spi__struct_t spi__data[SPI__DEV_COUNT];

static void spi__hw_cb(u8 spi_id, u8 cs_id, events__e code, u8 *buff, i32 len, void* ext_data);

/*******************************************************************************
 * Функция инициализации SPI. Реализуется в соответствии с Контекстом регистров ПЛИС.
 ******************************************************************************/
events__e spi__init(i32 dev_id, u32 settings) {
    if ((dev_id >= SPI__DEV_COUNT) || (spi_cfg__tbl[dev_id].func == NULL))
        return EVENT__PARAM_NA;
    spi__struct_t* d = &spi__data[dev_id];
    const spi__cfg_tbl_t* d_cfg = &spi_cfg__tbl[dev_id];
    d->sett_wr = d->sett_rd = settings;
    return d_cfg->func(HW_SPI__CMD_INIT, d_cfg->spi_id, d_cfg->cs_id, d->sett_wr, d->sett_rd, NULL, 0, NULL, 0, NULL, NULL);
}

/*******************************************************************************
 * Функция приема-передачи данных по SPI.
 ******************************************************************************/
events__e spi__txrx(i32 dev_id, u8 *rx_buff, u32 rx_len, u8 *tx_buff, u32 tx_len, spi__cb_t callback, void *ext_data) {
    const spi__cfg_tbl_t* d_cfg = &spi_cfg__tbl[dev_id];
    if ((dev_id >= SPI__DEV_COUNT) || (d_cfg->func == NULL))
        return EVENT__PARAM_NA;
    spi__struct_t* d = &spi__data[dev_id];
    if (d->busy)
        return EVENT__BUSY;
    d->busy = 1;
    d->callback = callback;
    d->ext_data = ext_data;
    events__e res = d_cfg->func(HW_SPI__CMD_TXRX, d_cfg->spi_id, d_cfg->cs_id, d->sett_wr, d->sett_rd, rx_buff, rx_len, tx_buff, tx_len, callback ? spi__hw_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (callback == NULL))
        d->busy = 0;
    return res;
}

events__e spi__set_cfg(i32 dev_id, spi__op_e op, u32 settings) {
    spi__struct_t* d = &spi__data[dev_id];
    if ((dev_id >= 0) && (dev_id < SPI__DEV_COUNT)) {
        if (op == SPI__OP_WRITE)
            d->sett_wr = settings;
        else
            d->sett_rd = settings;
        return EVENT__OK;
    }
    return EVENT__PARAM_NA;
}

u32 spi__get_cfg(i32 dev_id, spi__op_e op) {
    spi__struct_t* d = &spi__data[dev_id];
    if ((dev_id >= 0) && (dev_id < SPI__DEV_COUNT))
        return (op == SPI__OP_WRITE) ? d->sett_wr : d->sett_rd;
    return 0;
}

static void spi__hw_cb(u8 spi_id, u8 cs_id, events__e code, u8 *buff, i32 len, void* ext_data) {
    spi__struct_t* d = (spi__struct_t*)ext_data;
    d->busy = 0;
    if (d->callback) {
        d->callback(d - spi__data, code, buff, len, d->ext_data);
    }
}


