/***************************************************************************//**
 * @file max7301.c.
 * @brief Драйвер MAX7301 (аппаратно зависимая часть).
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "System/framework.h"
#include "DRV/spi.h"
#include "DRV/MAX7301/max7301.h"

// Настройки интерфейса SPI
#define MAX7301__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_6_MBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB)

#ifndef MAX7301__COUNT
#define MAX7301__COUNT 1
#endif

#define MAX7301__PIN_MIN 4
#define MAX7301__PIN_MAX 31

#define MAX7301__RESET_ADDR    0x04
#define MAX7301__CFG_ADDR      0x09
#define MAX7301__PIN_ADDR      0x24

#pragma pack(push, 1)
typedef struct {
    u8 addr : 7;
    u8 rw   : 1;
    u8 data;
}max7301__pkt_t;
#pragma pack(pop)

typedef enum {
    MAX7301__FSM_IDLE = 0,
    MAX7301__FSM_INIT,
    MAX7301__FSM_CFG,
    MAX7301__FSM_SET,
    MAX7301__FSM_GET,
}max7301__fsm_e;

typedef struct {
    max7301__pkt_t pkt;
    u8 pin;
    u8 data;
    max7301__fsm_e fsm;
    max7301__cb_t cb;
    void* ext_data;
}max7301__struct_t;

static max7301__pkt_t pkt_nop;      // Используется только при чтении! Для передачи NOP операции.

static max7301__struct_t max7301__data[MAX7301__COUNT];

static max7301__struct_t* max7301__free_get();
static void max7301__spi_rd_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data);
static void max7301__spi_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data);

events__e max7301__init(i32 dev_id, u32 settings, max7301__cb_t cb, void* ext_data) {
    max7301__struct_t* d = max7301__free_get();
    if (d == NULL)
        return EVENT__BUSY;
    d->fsm = MAX7301__FSM_INIT;
    d->cb = cb;
    d->ext_data = ext_data;
    if ((settings & MAX7301__SPI_NO_INIT) != MAX7301__SPI_NO_INIT)
        spi__init(dev_id, MAX7301__SPI_SETTINGS);
    d->pkt.rw = 0; // запись
    d->pkt.addr = MAX7301__RESET_ADDR;
    d->pkt.data = 0x00;
    events__e res = spi__txrx(dev_id, NULL, 0, (u8*)&d->pkt, sizeof(max7301__pkt_t), cb ? max7301__spi_rd_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        d->pkt.data = 0x01;
        if (res == EVENT__OK)
           res = spi__txrx(dev_id, NULL, 0, (u8*)&d->pkt, sizeof(max7301__pkt_t), NULL, NULL);
        d->fsm = MAX7301__FSM_IDLE;
    }
    return res;
}

events__e max7301__pin_cfg(i32 dev_id, u8 pin, max7301__pin_cfg_e cfg, max7301__cb_t cb, void* ext_data) {
    max7301__struct_t* d = max7301__free_get();
    if (d == NULL)
        return EVENT__BUSY;
    if ((pin < MAX7301__PIN_MIN) || (pin > MAX7301__PIN_MAX))
        return EVENT__PARAM_NA;
    d->fsm = MAX7301__FSM_CFG;
    d->cb = cb;
    d->ext_data = ext_data;
    d->pin = pin - MAX7301__PIN_MIN;
    d->data = (u8)cfg;
    d->pkt.rw = 1; // чтение
    d->pkt.addr = MAX7301__CFG_ADDR + d->pin / 4;
    events__e res = spi__txrx(dev_id, NULL, 0, (u8*)&d->pkt, sizeof(max7301__pkt_t), cb ? max7301__spi_rd_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        d->pkt.data = 0x01;
        if (res == EVENT__OK)
           res = spi__txrx(dev_id, (u8*)&d->pkt, sizeof(max7301__pkt_t), (u8*)&pkt_nop, sizeof(pkt_nop), NULL, NULL);
        d->pkt.rw = 0; // запись
        d->pkt.data &= ~(0x03 << ((d->pin % 4) << 1));
        d->pkt.data |= ((d->data & 0x03) << ((d->pin % 4) << 1));
        if (res == EVENT__OK) {
            if (d->pkt.addr != (MAX7301__CFG_ADDR + d->pin / 4)) // раздел Reading Device Registers даташита
                res = EVENT__ERROR;
            else
                res = spi__txrx(dev_id, NULL, 0, (u8*)&d->pkt, sizeof(max7301__pkt_t), NULL, NULL);
        }
        d->fsm = MAX7301__FSM_IDLE;
    }
    return res;
}

events__e max7301__pin_get(i32 dev_id, u8 pin, u8* state, max7301__cb_t cb, void* ext_data) {
    max7301__struct_t* d = max7301__free_get();
    if (d == NULL)
        return EVENT__BUSY;
    if ((pin < MAX7301__PIN_MIN) || (pin > MAX7301__PIN_MAX))
        return EVENT__PARAM_NA;
    d->fsm = MAX7301__FSM_GET;
    d->cb = cb;
    d->ext_data = ext_data;
    d->pin = pin - MAX7301__PIN_MIN;
    d->pkt.rw = 1; // чтение
    d->pkt.addr = MAX7301__PIN_ADDR + d->pin;
    events__e res = spi__txrx(dev_id, NULL, 0, (u8*)&d->pkt, sizeof(max7301__pkt_t), cb ? max7301__spi_rd_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        if (res == EVENT__OK) {
            res = spi__txrx(dev_id, (u8*) &d->pkt, sizeof(max7301__pkt_t), (u8*)&pkt_nop, sizeof(pkt_nop), NULL, NULL);
            if (state)
                *state = d->pkt.data & 0x01;
        }
        d->fsm = MAX7301__FSM_IDLE;
    }
    return res;
}


events__e max7301__pin_set(i32 dev_id, u8 pin, u8 state, max7301__cb_t cb, void* ext_data) {
    max7301__struct_t* d = max7301__free_get();
    if (d == NULL)
        return EVENT__BUSY;
    if ((pin < MAX7301__PIN_MIN) || (pin > MAX7301__PIN_MAX))
        return EVENT__PARAM_NA;
    d->fsm = MAX7301__FSM_SET;
    d->cb = cb;
    d->ext_data = ext_data;
    d->pin = pin - MAX7301__PIN_MIN;
    d->pkt.rw = 0; // запись
    d->pkt.addr = MAX7301__PIN_ADDR + d->pin;
    d->pkt.data = state & 0x01;
    events__e res = spi__txrx(dev_id, NULL, 0, (u8*)&d->pkt, sizeof(max7301__pkt_t), cb ? max7301__spi_cb : NULL, (void*)d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        d->fsm = MAX7301__FSM_IDLE;
    }
    return res;
}

static void max7301__spi_rd_cb(i32 dev_id, events__e event, u8 *buff, i32 len, void *ext_data) {
    max7301__struct_t* d = (max7301__struct_t*)ext_data;
    if (event == EVENT__OK) {
        switch(d->fsm) {
            case MAX7301__FSM_INIT:
                d->pkt.data = 0x01;
                event = spi__txrx(dev_id, NULL, 0, (u8*)&d->pkt, sizeof(max7301__pkt_t), max7301__spi_cb, (void*)d);
                break;
            case MAX7301__FSM_GET:
                event = spi__txrx(dev_id, (u8*)&d->pkt, sizeof(max7301__pkt_t), (u8*)&pkt_nop, sizeof(pkt_nop), max7301__spi_cb, (void*)d);
                break;
            case MAX7301__FSM_CFG: // чтение конфигурации выводов
                d->fsm = MAX7301__FSM_SET;
                event = spi__txrx(dev_id, (u8*)&d->pkt, sizeof(max7301__pkt_t), (u8*)&pkt_nop, sizeof(pkt_nop), max7301__spi_rd_cb, (void*)d);
                break;
            case MAX7301__FSM_SET:
                d->pkt.rw = 0; // запись конфигурации выаодов
                d->pkt.data &= ~(0x03 << ((d->pin % 4) << 1));
                d->pkt.data |= ((d->data & 0x03) << ((d->pin % 4) << 1));
                if (d->pkt.addr != (MAX7301__CFG_ADDR + d->pin / 4)) // раздел Reading Device Registers даташита
                    event = EVENT__ERROR;
                else
                    event = spi__txrx(dev_id, NULL, 0, (u8*)&d->pkt, sizeof(max7301__pkt_t), max7301__spi_cb, (void*)d);
                break;
            default:
                event = EVENT__PARAM_NA;
                break;
        }
    }
    if (event == EVENT__OK)
        return;
    d->fsm = MAX7301__FSM_IDLE;
    if (d->cb) {
        d->cb(dev_id, event, 0, 0, d->ext_data);
    }
}

static void max7301__spi_cb(i32 dev_id, events__e event, u8 *buff, i32 len, void *ext_data) {
    max7301__struct_t* d = (max7301__struct_t*)ext_data;
    d->fsm = MAX7301__FSM_IDLE;
    if (d->cb) {
        d->cb(dev_id, event, d->pin + MAX7301__PIN_MIN, d->pkt.data & 0x01, d->ext_data);
    }
}

static max7301__struct_t* max7301__free_get() {
    for(i32 i = 0; i < MAX7301__COUNT; i++) {
        if (max7301__data[i].fsm == MAX7301__FSM_IDLE)
            return &max7301__data[i];
    }
    return NULL;
}
