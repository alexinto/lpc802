﻿/***************************************************************************//**
 * @file flash.с.
 * @brief Драйвер микросхемы флеш-памяти FLASH.
 * @author a.tushentsov.
 ******************************************************************************/
#include <string.h>
#include "target.h"
#include "System/sw_timer.h"
#include "DRV/spi.h"
#include "DRV/flash.h"

#ifndef FLASH__COUNT
#define FLASH__COUNT 1
#endif

// Настройки интерфейса SPI по-умолчанию
#define FLASH__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_48_MBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB )

typedef enum {
    cmd =   0,
    addr =  1,
}request_type_e;

typedef enum {
    FLASH_RD_STATUS     = 0,     // Операция чтения STATUS регистра. Не приступает к следующей операции пока не снимется флаг занятости.
    FLASH_RD_DATA       = 1,     // Операция чтения данных.
    FLASH_WR_STATUS     = 2,     // Операция записи статус регистра- задел на будущее, не используется...
    FLASH_WR_ENABLE     = 3,     // Операция открывает доступ на запись в микросхему.
    FLASH_WR_DISABLE    = 4,     // Операция закрывает доступ на запись в микросхему.
    FLASH_WR_PAGE       = 5,     // Операция записи данных. Самая сложная операция, парсит данные с учетом межстраничных переходов и т.д.
    FLASH_ERASE_SECTOR  = 6,     // Операция стирания сектора. размер сектора возвращает в коллбэке.
    FLASH_WAIT          = 7,     // Операция задержки с переключением CS вывода. Длительность = 1 cout.
    FLASH_CHECK_WR      = 8,     // Операция проверки доступа на запись- возвращает ERROR, если нет доступа.
    FLASH_IDN_RD        = 9,     // Операция считывания IDN флеш. Используется при инициализации модуля.
    FLASH_END           = 10,    // Признак конца таблицы.
} flash__cmd_e;

typedef struct {
    u8 cmd;     // Байт команды (из даташита).
    u8 len;     // Длина пакета, включая байт команды. Нельзя ставить больше 5!!!
} cmd_descr__t;

typedef struct {
    u32 addr;                   // Начальный адрес области памяти
    u32 offset;                 // Размер области памяти секторов
    u32 size;                   // Размер сектора в заданной области памяти
    u8 erase_cmd;               // Если необходимо, то уникальная команда стирания сектора для заданного диапазона памяти
}flash__sect_t;

typedef struct {
    u32 read_offset;              // Кол-во "пустых" тактов для чтения, не более 8!!!
    cmd_descr__t* cmd_tbl;             // Соответствие команд (flash__cmd_e) и байта команды по даташиту
    flash__cmd_e* oper_tbl[4];         // Скрипты операций флеш-памяти
    flash__sect_t* sect_tbl;           // Адресное пространство флеш- памяти
}flash__descr_t;

static const flash__descr_t flash__cmd_tbl[] = {
    {    // s25fl256
        .read_offset = 1,      //       0          1          2          3         4           5          6          7          8          9          10
        .cmd_tbl = (cmd_descr__t[]){{0x05, 2}, {0x0C, 5}, {0x01, 2}, {0x06, 1}, {0x04, 1}, {0x12, 5}, {0x21, 5}, {0x00, 1}, {0x05, 2}, {0x9F, 2}, {0x00, 1}},
        .oper_tbl = {
            [FLASH__OP_INIT]  = (flash__cmd_e[]){FLASH_WAIT, FLASH_IDN_RD, FLASH_END},
            [FLASH__OP_ERASE] = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_WR_ENABLE, FLASH_CHECK_WR, FLASH_ERASE_SECTOR, FLASH_RD_STATUS, FLASH_WR_DISABLE, FLASH_END},
            [FLASH__OP_READ]  = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_RD_DATA, FLASH_RD_STATUS, FLASH_END},
            [FLASH__OP_WRITE] = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_WR_ENABLE, FLASH_CHECK_WR, FLASH_WR_PAGE, FLASH_RD_STATUS, FLASH_WR_DISABLE, FLASH_END},
        },
        .sect_tbl = (flash__sect_t[]){{0, 0x1FFFF, 4096}, {0x20000, 0x1FDFFFF, 65536, 0xDC}, {0, 0, 0}}
    },
    {    // mx25l256
        .read_offset = 1,      //       0          1          2          3         4           5          6          7          8          9          10
        .cmd_tbl = (cmd_descr__t[]){{0x05, 2}, {0x0C, 5}, {0x01, 2}, {0x06, 1}, {0x04, 1}, {0x12, 5}, {0x21, 5}, {0x00, 1}, {0x05, 2}, {0x9F, 2}, {0x00, 1}},
        .oper_tbl = {
            [FLASH__OP_INIT]  = (flash__cmd_e[]){FLASH_WAIT, FLASH_IDN_RD, FLASH_END},
            [FLASH__OP_ERASE] = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_WR_ENABLE, FLASH_CHECK_WR, FLASH_ERASE_SECTOR, FLASH_RD_STATUS, FLASH_WR_DISABLE, FLASH_END},
            [FLASH__OP_READ]  = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_RD_DATA, FLASH_RD_STATUS, FLASH_END},
            [FLASH__OP_WRITE] = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_WR_ENABLE, FLASH_CHECK_WR, FLASH_WR_PAGE, FLASH_RD_STATUS, FLASH_WR_DISABLE, FLASH_END},
        },
        .sect_tbl = (flash__sect_t[]){{0, 0x1FFFFFF, 4096}, {0, 0, 0}}
    },
    {    // at25df321
        .read_offset = 0,      //       0          1          2          3         4           5          6          7          8          9          10
        .cmd_tbl = (cmd_descr__t[]){{0x05, 2}, {0x03, 4}, {0x01, 2}, {0x06, 1}, {0x04, 1}, {0x02, 4}, {0x20, 4}, {0x00, 1}, {0x05, 2}, {0x9F, 2}, {0x00, 1}},
        .oper_tbl = {
            [FLASH__OP_INIT]  = (flash__cmd_e[]){FLASH_WAIT, FLASH_IDN_RD, FLASH_END},
            [FLASH__OP_ERASE] = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_WR_ENABLE, FLASH_WR_STATUS, FLASH_WR_ENABLE, FLASH_CHECK_WR, FLASH_ERASE_SECTOR, FLASH_RD_STATUS, FLASH_WR_DISABLE, FLASH_END},
            [FLASH__OP_READ]  = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_RD_DATA, FLASH_RD_STATUS, FLASH_END},
            [FLASH__OP_WRITE] = (flash__cmd_e[]){FLASH_WAIT, FLASH_RD_STATUS, FLASH_WR_ENABLE, FLASH_WR_STATUS, FLASH_WR_ENABLE, FLASH_CHECK_WR, FLASH_WR_PAGE, FLASH_RD_STATUS, FLASH_WR_DISABLE, FLASH_END},
        },
        .sect_tbl = (flash__sect_t[]){{0, 0x3FFFFF, 4096}, {0, 0, 0}}
    },
};

// todo Можно вынести в отдельный файл!-----------------------------------------------------------------------------------------------------------------------
#define PAGE_SIZE 256 // todo Нужно куда-то засунуть!!!
// -----------------------------------------------------------------------------------------------------------------------------------------------------------
typedef struct {
    u8 init;          // Флаг инициализации.
    u8 request[PAGE_SIZE + 8], answer[PAGE_SIZE + 8 + 8], err_counter;
    flash__cmd_e* cur_oper;    // Указатель на текущую (выполняемую) команду скрипта.
    u8* buff;
    i32 len;
    u32 addr, offset;
    i32 dev_id;
    flash__descr_t* descr;     // Указатель на таблицу параметров типа флеш-памяти.
    flash__oper_e oper;        // Пользовательская операция с флеш-памятью.
    flash__cb_t cb;
    void* ext_data;
    sw_timer__t timer;
}flash__struct_t;

static flash__struct_t flash__data[FLASH__COUNT];

static events__e flash__script_exec(flash__struct_t* p_data);
static void flash__exec_cb(i32 id, events__e event, u8 *buff, i32 len, void *ext_data);
static void timer_cb(struct sw_timer__t *timer, void *ext_data);
static events__e flash__check_res(flash__struct_t* p_data, i32 len);
static flash__struct_t* flash__struct_get(i32 dev_id);
static flash__sect_t* flash__get_sect_size(flash__struct_t* p_data, u32 addr);

/*******************************************************************************
 * Функция инициализации экземпляра flash.
 ******************************************************************************/
events__e flash__init(i32 dev_id, flash__type_e type, u32 settings, flash__cb_t cb) {
    flash__struct_t* p_data = flash__struct_get(dev_id);  // была ли уже проинициализирована флеш...
    if (p_data == NULL) {
        for(i32 i = 0; i < FLASH__COUNT; i++) {
            if (!flash__data[i].init) {
                p_data = &flash__data[i];
                break;
            }
        }
    }
    if (!p_data)
        return EVENT__NO_MEM;
    if (p_data->cur_oper)
        return EVENT__BUSY;
    if (type >= (sizeof(flash__cmd_tbl) / sizeof(flash__cmd_tbl[0])))
        return EVENT__PARAM_NA;
    p_data->descr = (flash__descr_t*)&flash__cmd_tbl[type];
    p_data->cur_oper = p_data->descr->oper_tbl[FLASH__OP_INIT];
    p_data->dev_id = dev_id;
    p_data->cb = cb;
    events__e res = EVENT__OK;
    if ((settings & FLASH__SPI_NO_INIT) == 0)
        res = spi__init(dev_id, FLASH__SPI_SETTINGS);
    if (res == EVENT__OK)
        res = flash__script_exec(p_data);
    else
        p_data->cur_oper = NULL;
    return res;
}

/*******************************************************************************
 * Функция-точка входа для выполнения всех операций с flash.
 ******************************************************************************/
events__e flash__exec(u8 dev_id, flash__oper_e oper, u32 addr, u8 *buff, u32 len, flash__cb_t cb, void* ext_data) {
    flash__struct_t* p_data = flash__struct_get(dev_id);  // была ли уже проинициализирована флеш...
    if (!p_data)
        return EVENT__PARAM_NA;
    if (p_data->cur_oper)
        return EVENT__BUSY;
    p_data->cur_oper = p_data->descr->oper_tbl[oper];
    if (!p_data->cur_oper)
        return EVENT__PARAM_NA;
    p_data->oper = oper;
    p_data->offset = p_data->err_counter = 0;
    p_data->addr = addr;
    p_data->buff = buff;
    p_data->len = len;
    p_data->cb = cb;
    p_data->ext_data = ext_data;
    return flash__script_exec(p_data);
}

/*******************************************************************************
 * Функция выполнения скрипта.
 ******************************************************************************/
static events__e flash__script_exec(flash__struct_t* p_data) {
    events__e res = EVENT__OK;
    u8 erase_cmd;
    cmd_descr__t cmd_descr;
    u32 cur_len, cur_addr, spi_rx_len;
    u8* buff_tx, *buff_rx, addr_len;
    u16 write_offset;
    spi__cb_t spi_cb = p_data->cb ? flash__exec_cb : NULL;
    flash__cmd_e cmd_exec = *p_data->cur_oper;
    do {
        cmd_descr = p_data->descr->cmd_tbl[cmd_exec];
        cur_len = cmd_descr.len;
        cur_addr = p_data->addr + p_data->offset;
        buff_tx = p_data->request;
        buff_rx = (((cur_len < 3) && (cmd_descr.cmd)) ? p_data->answer : NULL);
        addr_len = (cur_len > 1) ?  cur_len - 2 : 0;
        write_offset = PAGE_SIZE - ((p_data->addr + p_data->offset) % PAGE_SIZE);
        p_data->request[cmd] = cmd_descr.cmd;
        for(u8 x = 0; x <= addr_len; x++)       // Перезапись адреса в MSB first.
            p_data->request[addr + x] = *((u8*)&cur_addr + addr_len - x);
        spi_rx_len = cur_len;
        switch (*p_data->cur_oper) {
            case FLASH_RD_DATA:
                res = spi__txrx(p_data->dev_id, p_data->answer, p_data->len + cur_len + p_data->descr->read_offset, buff_tx, cur_len, spi_cb, p_data);
                spi_rx_len = p_data->len + cur_len + p_data->descr->read_offset;
                break;
            case FLASH_WR_PAGE:
                if ((p_data->offset + write_offset) > p_data->len)
                    write_offset = p_data->len - p_data->offset;
                p_data->offset += write_offset;
                memcpy(p_data->request + cur_len, p_data->buff + p_data->offset - write_offset, write_offset);
                res = spi__txrx(p_data->dev_id, NULL, 0, buff_tx, write_offset + cur_len, spi_cb, p_data);
                spi_rx_len = 0;
                break;
            case FLASH_WAIT:
                sw_timer__start(&p_data->timer, 0, p_data->cb ? timer_cb : NULL, p_data);
                break;
            case FLASH_ERASE_SECTOR:
                erase_cmd = flash__get_sect_size(p_data, cur_addr)->erase_cmd;
                if (erase_cmd)
                    p_data->request[cmd] = erase_cmd;
                res = spi__txrx(p_data->dev_id, buff_rx, cur_len, buff_tx, cur_len, spi_cb, p_data);
                break;
            case FLASH_WR_STATUS:
                p_data->request[addr] = 0;
            default:
                res = spi__txrx(p_data->dev_id, buff_rx, cur_len, buff_tx, cur_len, spi_cb, p_data);
                break;
        }
    } while((p_data->cb == NULL) && (res == EVENT__OK) && ((res = flash__check_res(p_data, spi_rx_len)) == EVENT__OK) && ((cmd_exec = *++p_data->cur_oper) != FLASH_END));
    if ((p_data->cb == NULL) || (res != EVENT__OK))
        p_data->cur_oper = NULL;
    return res;
}

static void flash__exec_cb(i32 id, events__e event, u8 *buff, i32 len, void *ext_data) {
    flash__struct_t* p_data = (flash__struct_t*)ext_data;
    if (event == EVENT__OK)
        event = flash__check_res(p_data, len);
    if ((*++p_data->cur_oper == FLASH_END) || (event != EVENT__OK) || ((event = flash__script_exec(p_data)) != EVENT__OK)) {
        p_data->cur_oper = NULL;
        if (p_data->cb)
            p_data->cb(p_data->dev_id, p_data->oper, event, p_data->addr, p_data->buff, p_data->len, p_data->ext_data);
    }
}

/*******************************************************************************
 * Функция обработки результата выполнения операции с флеш-памятью.
 ******************************************************************************/
static events__e flash__check_res(flash__struct_t* p_data, i32 len) {
    u32 sect_size;
    events__e res = EVENT__OK;
    u8 status_check = 0x01;
    switch(*p_data->cur_oper) {
    case FLASH_CHECK_WR:
        status_check = 0x03;
    case FLASH_RD_STATUS:
        if ((p_data->answer[len - 1] & status_check) != (status_check -1)) {
            if (p_data->err_counter++ < 100) {
                p_data->cur_oper -= (status_check >> 1) + 1;
                sw_timer__start(&p_data->timer, 5, p_data->cb ? timer_cb : NULL, p_data);
                break;
            }
            res = EVENT__ERROR;
        }
        else
            p_data->err_counter = 0;
    case FLASH_WR_ENABLE:
    case FLASH_WR_DISABLE:
    case FLASH_WAIT:
        break;
    case FLASH_ERASE_SECTOR:
        sect_size = flash__get_sect_size(p_data, p_data->addr + p_data->offset)->size;
        p_data->offset += sect_size;
        if ((p_data->offset < p_data->len) && (sect_size))
            p_data->cur_oper = p_data->descr->oper_tbl[FLASH__OP_ERASE];
        else
            p_data->len = p_data->offset;       // Возвращаем реальный размер стертой памяти
        break;
    case FLASH_RD_DATA:
        memcpy(p_data->buff, p_data->answer + len - p_data->len, p_data->len);
        break;
    case FLASH_WR_PAGE:
        if (p_data->offset < p_data->len)
            p_data->cur_oper = p_data->descr->oper_tbl[FLASH__OP_WRITE];
        break;
    case FLASH_IDN_RD:
        if ((p_data->answer[len - 1] == 0) || (p_data->answer[len - 1] == 0xFF))
            res = EVENT__ERROR;
        else
            p_data->init = 1;         // Иницализация прошла успешно.
        break;
    default:
        break;
    }
    return res;
}

/*******************************************************************************
 * Коллбэк таймера - ожидания готовности аппаратной части.
 ******************************************************************************/
static void timer_cb(struct sw_timer__t *timer, void *ext_data) {
    flash__exec_cb(-1, EVENT__OK, NULL, 0, ext_data);
}

/*******************************************************************************
 * Функция - получения структуры конкретного устройства флеш -памяти (экземпляра).
 ******************************************************************************/
static flash__struct_t* flash__struct_get(i32 dev_id) {
    for(i32 i = 0; i < FLASH__COUNT; i++) {
        if ((flash__data[i].dev_id == dev_id) && (flash__data[i].init))
            return &flash__data[i];
    }
    return NULL;
}

static flash__sect_t* flash__get_sect_size(flash__struct_t* p_data, u32 addr) {
    flash__sect_t* p_tbl = p_data->descr->sect_tbl;
    while(p_tbl->size && (addr > (p_tbl->addr + p_tbl->offset))) p_tbl++;
    return p_tbl;
}
