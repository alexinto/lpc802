#include <string.h>
#include "target.h"
#include "xspi.h"
#include "System/resource.h"
#include "DRV/FPGA/XIL_SPI/hw_spi_xil.h"
#include "System/framework.h"

#ifndef HW_SPI_XIL__COUNT
#define HW_SPI_XIL__COUNT 1
#endif

static XSpi Spi; /* The instance of the SPI device */

static i32 QSpi_init(u16 DeviceId);

typedef struct {
    u8 spi_id;
    u8 cs_id;
    events__e event;
    u8 *rx_buff;
    u32 rx_len;
    hw_spi__cb_t cb;
    void* ext_data;
}hw_spi__struct_t;

static framework__sub_t cout_sub;

static hw_spi__struct_t hw_spi__data[HW_SPI_XIL__COUNT];

static void hw_spi__cout();

events__e hw_spi_xil__func(hw_spi__cmd_e cmd, u8 spi_id, u8 cs_id, u32 sett_wr, u32 sett_rd, u8 *rx_buff, u32 rx_len, u8 *tx_buff, u32 tx_len, hw_spi__cb_t callback, void* ext_data) {
    if (spi_id >= HW_SPI_XIL__COUNT)
        return EVENT__PARAM_NA;
    if (cout_sub.cout == NULL) {
        framework__cout_subscribe(&cout_sub, hw_spi__cout);
        for(i32 i = 0; i < HW_SPI_XIL__COUNT; i++)
            hw_spi__data[i].event = EVENT__WAIT;
    }
    hw_spi__struct_t* d = &hw_spi__data[spi_id];
    d->cb = callback;
    d->ext_data = ext_data;
    d->cs_id = cs_id;
    d->spi_id = spi_id;
    d->rx_buff = rx_buff;
    d->rx_len = rx_len;
    events__e res = EVENT__OK;
    switch(cmd) {
        case HW_SPI__CMD_INIT:
            if (QSpi_init(spi_id) == XST_SUCCESS) {
                XSpi_Start(&Spi);
                //Disable Global interrupt to use polled mode operation
                XSpi_IntrGlobalDisable(&Spi);
                XSpi_SetSlaveSelect(&Spi, cs_id);
            }
            break;
        case HW_SPI__CMD_TXRX:
            if ((rx_buff) && (rx_len < tx_len))
                return EVENT__PARAM_NA;
            while(resource__lock(MUTEX__SPI_XIL) != EVENT__OK){}
            XSpi_Transfer(&Spi, tx_buff, rx_buff, rx_len > tx_len ? rx_len : tx_len);
            resource__unlock(MUTEX__SPI_XIL);
            break;
        default:
            res = EVENT__PARAM_NA;
            break;
    }
    d->event = res;
    if ((d->cb) && (res == EVENT__OK))
        d->event = res;
    return res;
}

static i32 QSpi_init(u16 DeviceId) {
    i32 Status = XST_DEVICE_NOT_FOUND;
    XSpi_Config *ConfigPtr; /* Pointer to Configuration data */
//---- Initialize the SPI driver so that it is  ready to use -----------
    while(resource__lock(MUTEX__SPI_XIL) != EVENT__OK){}
    ConfigPtr = XSpi_LookupConfig(DeviceId);
    if (ConfigPtr != NULL) {
        Status = XSpi_CfgInitialize(&Spi, ConfigPtr, ConfigPtr->BaseAddress);
        if (Status == XST_SUCCESS) {
            Status = XSpi_SetOptions(&Spi, XSP_MASTER_OPTION | XSP_MANUAL_SSELECT_OPTION);
        }
    }
    resource__unlock(MUTEX__SPI_XIL);
    if (Status != XST_SUCCESS)
        Status =~Status +1;
    return Status;
}

static void hw_spi__cout() {
    hw_spi__struct_t* d;
    events__e res;
    for(i32 i = 0; i < HW_SPI_XIL__COUNT; i++) {
        d = &hw_spi__data[i];
        res = d->event;
        if (res != EVENT__WAIT) {
            d->event = EVENT__WAIT;
            if (d->cb) {
                d->cb(d->cs_id, d->cs_id, res, d->rx_buff, d->rx_len, d->ext_data);
            }
        }
    }
}

