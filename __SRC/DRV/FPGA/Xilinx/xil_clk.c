/***************************************************************************//**
 * @file xil_clk.h.
 * @brief Драйвер Ip-core Xilinx Clocking Wizard v6.0.
 * @authors a.tushentsov.
 ******************************************************************************/
#include <math.h>
#include "target.h"
#include "System/sw_timer.h"
#include "DRV/FPGA/freg.h"
#include "DRV/FPGA/Xilinx/xil_clk.h"

#ifndef XIL_CLK__CNT
#define XIL_CLK__CNT 1
#endif

#ifndef XIL_CLK__CH_CNT
#define XIL_CLK__CH_CNT 4          // кол-во каналов по-умолчанию
#endif

#ifndef XIL_CLK__ERR_CNT
#define XIL_CLK__ERR_CNT 10        // Счетчик ошибок- ожидание выставления флага, мс.
#endif

#define XIL_CLK__CH_DIV_MIN 1      // Минимальное значение делителя канала
#define XIL_CLK__CH_DIV_MAX 128    // Максимальное значение делителя канала

#ifndef XIL_CLK__FOUT_MAX
#define XIL_CLK__FOUT_MAX 275000000   // Максимальная выходная частота каналов, Hz
#endif

#define XIL_CLK__VCO_MIN 800          // Mhz
#define XIL_CLK__VCO_MAX 1866         // Mhz
#define XIL_CLK__RESET 0x0000000A     // Reset value

#pragma pack(push, 4)
typedef volatile struct {
    uint32_t integer  :8;    // Целая часть
    uint32_t frac     :10;   // Дробная часть
    uint32_t reserved :14;
}xil_clk__div_t;

typedef volatile struct {
    uint32_t divider  :8;    // Делитель- общий для всех каналов
    uint32_t mul_int  :8;    // Множитель - целая часть
    uint32_t mul_frac :10;   // Множитель - дробная часть
    uint32_t reserved :6;
}xil_clk__cfg_t;

typedef volatile struct {
    uint32_t load     :1;    // Loads Clock Configuration Register values to the internal register used for dynamic reconfiguration and initiates reconfiguration state machine.
    uint32_t enable   :1;    // When written 0, default configuration done in the Clocking Wizard GUI is loaded for dynamic reconfiguration
    uint32_t reserved :30;
}xil_clk__dyn_cfg_t;

typedef volatile struct {
    xil_clk__div_t divider;
    int32_t phase;           // Знаковое +- фаза, 50% = 50000, Диапазон -360000 to +360000.
    uint32_t duty;           // 50% = 50000,
}xil_clk__ch_reg_t;

typedef volatile struct {
    uint32_t clk0_up     :1;    // User clock 0 frequency is greater than the specifications.
    uint32_t clk1_up     :1;    // User clock 1 frequency is greater than the specifications.
    uint32_t clk2_up     :1;    // User clock 2 frequency is greater than the specifications.
    uint32_t clk3_up     :1;    // User clock 3 frequency is greater than the specifications.
    uint32_t clk0_dn     :1;    // User clock 0 frequency is lesser than the specifications.
    uint32_t clk1_dn     :1;    // User clock 1 frequency is lesser than the specifications.
    uint32_t clk2_dn     :1;    // User clock 2 frequency is lesser than the specifications.
    uint32_t clk3_dn     :1;    // User clock 3 frequency is lesser than the specifications.
    uint32_t clk0_glitch :1;    // Glitch occurred in user clock 0.
    uint32_t clk1_glitch :1;    // Glitch occurred in user clock 1.
    uint32_t clk2_glitch :1;    // Glitch occurred in user clock 2.
    uint32_t clk3_glitch :1;    // Glitch occurred in user clock 3.
    uint32_t clk0_stop   :1;    // Clock stop on user clock 0.
    uint32_t clk1_stop   :1;    // Clock stop on user clock 0.
    uint32_t clk2_stop   :1;    // Clock stop on user clock 0.
    uint32_t clk3_stop   :1;    // Clock stop on user clock 0.
    uint32_t reserved    :16;
}xil_clk__err_t;

typedef volatile struct {
    uint32_t reset;            // 0x00
    uint32_t status;           // 0x04  1- locked, 0- during configuration
    xil_clk__err_t error;      // 0x08
    uint32_t isr;              // 0x0C
    uint32_t ier;              // 0x10
    uint32_t reserved[123];    // 0x14
    xil_clk__cfg_t config;     // 0x200
    int32_t phase;             // 0x204
    xil_clk__ch_reg_t ch[7];   // 0x208
    xil_clk__dyn_cfg_t dyn_cfg;// 0x25C
}xil_clk__reg_t;
#pragma pack(pop)

typedef struct {
    xil_clk__reg_t* reg;
    sw_timer__t tmr;
    uint32_t vco_freq;
    uint32_t ch_freq[XIL_CLK__CH_CNT];
}xil_clk__struct_t;

static xil_clk__struct_t xil_spi__reg_tbl[XIL_CLK__CNT];

/*******************************************************************************
 * Функция инициализации модуля. Вызывается 1 раз при старте системы.
 ******************************************************************************/
events__e xil_clk__init(uint8_t id, u32 addr, float clk_in, float div, float mux, float phase) {
    float vco_freq = clk_in * mux / div / 1000000; // перевод в Мгц
    events__e res = EVENT__OK;
    if (id >= XIL_CLK__CNT)
        return EVENT__PARAM_NA;
    if ((XIL_CLK__VCO_MIN > vco_freq) || (XIL_CLK__VCO_MAX < vco_freq))
        res = EVENT__ERROR;
    xil_clk__struct_t* d = &xil_spi__reg_tbl[id];
    d->reg = (xil_clk__reg_t*)addr;
    xil_clk__reg_t* p_reg = d->reg;
    fstd__u temp;
    temp.u32 = 0x03;
    FREG_SET(p_reg->dyn_cfg, temp);
    p_reg->reset = XIL_CLK__RESET;
    // Ожидаем lock... Если не защелкнулась, то начальные параметры неверные, повторим позже с требуемыми параметрами.
    for(int i = 0; (i < XIL_CLK__ERR_CNT) && (!p_reg->status); i++) {
        sw_timer__start(&d->tmr, 1, NULL, NULL);
    }
    temp.u32 = 0x03;
    FREG_SET(p_reg->dyn_cfg, temp);
    xil_clk__cfg_t cfg = p_reg->config;
    cfg.divider = round(div);
    cfg.mul_int = round(mux);
    cfg.mul_frac = round((mux - round(mux)) * 1000);
    FREG_SET(p_reg->config, cfg);                          // Set VCO
    p_reg->phase = round(phase * 1000);
    d->vco_freq = vco_freq * 1000000;            // перевод в Гц
    for(int i = 0; (i < XIL_CLK__ERR_CNT) && (!p_reg->status); i++) {
        sw_timer__start(&d->tmr, 1, NULL, NULL);
    }
    if (!p_reg->status)      // Если не защелкнулась с требуемыми параметрами, то ошибка!
        res = EVENT__ERROR;
    temp.u32 = 0x03;
    FREG_SET(p_reg->dyn_cfg, temp);
    return res;
}

/*******************************************************************************
 * Функция возвращает множитель и делитель входной частоты для получения частоты ГУН.
 ******************************************************************************/
float xil_clk__cfg_get(uint8_t id, float* div) {
    if (id >= XIL_CLK__CNT)
        return 0;
    xil_clk__cfg_t cfg = xil_spi__reg_tbl[id].reg->config;
    if (div)
        *div = cfg.divider;
    return cfg.mul_int + cfg.mul_frac / 1000;
}

/*******************************************************************************
 * Функция установки выходного сигнала канала PLL.
 ******************************************************************************/
uint32_t xil_clk__set(uint8_t id, uint8_t ch, float div, float phase, float duty) {
    if ((id >= XIL_CLK__CNT) || (ch >= XIL_CLK__CH_CNT) || (div < XIL_CLK__CH_DIV_MIN) || (div > XIL_CLK__CH_DIV_MAX))
        return 0;
    xil_clk__struct_t* d = &xil_spi__reg_tbl[id];
    float res_freq = (float)d->vco_freq / div;
    if (XIL_CLK__FOUT_MAX < res_freq)
        res_freq = 0;
    xil_clk__reg_t* p_reg = d->reg;
    fstd__u temp;
    xil_clk__div_t ch_div = p_reg->ch[ch].divider;
    ch_div.integer = round(div);
    ch_div.frac = round((div - round(div)) * 1000);
    FREG_SET(p_reg->ch[ch].divider, ch_div);
    p_reg->ch[ch].phase = round(phase * 1000);
    p_reg->ch[ch].duty = round(duty * 1000);
    for(int i = 0; (i < XIL_CLK__ERR_CNT) && (!p_reg->status); i++) {
        sw_timer__start(&d->tmr, 1, NULL, NULL);
    }
    if (!p_reg->status)
        res_freq = 0;
    temp.u32 = 0x03;
    FREG_SET(p_reg->dyn_cfg, temp);
    d->ch_freq[ch] = res_freq;
    return res_freq;
}

/*******************************************************************************
 * Функция возвращает текущие настройки PLL.
 ******************************************************************************/
uint32_t xil_clk__get(uint8_t id, uint8_t ch, float* div, float* phase, float* duty) {
    if ((id >= XIL_CLK__CNT) || (ch >= XIL_CLK__CH_CNT))
        return 0;
    xil_clk__struct_t* d = &xil_spi__reg_tbl[id];
    xil_clk__reg_t* p_reg = d->reg;
    xil_clk__div_t ch_div = p_reg->ch[ch].divider;
    if(div)
        *div = ch_div.integer + (float)ch_div.frac / 1000;
    if(phase)
        *phase = (float)p_reg->ch[ch].phase / 1000;
    if(duty)
        *duty = (float)p_reg->ch[ch].duty / 1000;
    return d->ch_freq[ch];
}
