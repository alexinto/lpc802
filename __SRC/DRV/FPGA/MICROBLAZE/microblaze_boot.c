/***************************************************************************//**
 * @file microblaze_boot.c.
 * @brief Модуль, реализующий инициализацию глобальных переменных. Должны быть созданы
 *        следующие секции линкеру:
 *        __data_start - начало секции инициализированных данных
 *        __data_end - конец секции инициализированных данных
 *        _load_data_start - секция, содержащая данные для инициализации
 *        __bss_start - начало секции данных с 0 значением
 *        __bss_end - конец секции данных с 0 значением
 * @author a.tushentsov.
 ******************************************************************************/
#include "System/fstandard.h"
#include <string.h>
#include "DRV/FPGA/MICROBLAZE/microblaze_boot.h"

extern u32 __data_start, __data_end, _load_data_start, __bss_start, __bss_end;

/*******************************************************************************
 * Функция инициализации глобальных переменных.
 ******************************************************************************/
void microblaze_boot__data() {
    u8* data = (u8*)&__data_start;
    u8* init = (u8*)&_load_data_start;
    u32 size = (u32)&__data_end - (u32)&__data_start;
    memcpy(data, init, size);
    data = (u8*)&__bss_start;
    size = (u32)&__bss_end - (u32)&__bss_start;
    memset(data, 0, size);
}
