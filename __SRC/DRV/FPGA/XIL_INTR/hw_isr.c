/***************************************************************************//**
 * @file hw_isr.c.
 * @brief Драйвер работы с прерываниями (аппаратно зависимая часть).
 * @authors a.tushentsov.
 ******************************************************************************/
#include "target.h"
#include "System/fstandard.h"
#include "DRV/FPGA/freg.h"
#include "DRV/FPGA/XIL_INTR/hw_isr.h"
#ifdef __MICROBLAZE__
#include "mb_interface.h"
#endif

#ifndef HW_ISR__CNT
#define HW_ISR__CNT 1
#endif

#define HW_ISR__MAX_CNT 4   // Максимальное кол-во контроллеров прерываний, поддерживаемое драйвером!

#if (HW_ISR__CNT > HW_ISR__MAX_CNT)
#error "HW_ISR__CNT must be less than HW_ISR__MAX_CNT !!!"
#endif


// Структура регистров ПЛИС - AXI Interrupt Controller (INTC) v4.1
typedef volatile struct {
    uint32_t isr;  //    00h ISR Interrupt Status Register (ISR)
    uint32_t ipr;  //    04h IPR Interrupt Pending Register (IPR)
    uint32_t ier;  //    08h IER Interrupt Enable Register (IER)
    uint32_t iar;  //    0Ch IAR Interrupt Acknowledge Register (IAR)
    uint32_t sie;  //    10h SIE Set Interrupt Enables (SIE)
    uint32_t cie;  //    14h CIE Clear Interrupt Enables (CIE)
    uint32_t ivr;  //    18h IVR Interrupt Vector Register (IVR)
    uint32_t mer;  //    1Ch MER Master Enable Register (MER)
    uint32_t imr;  //    20h IMR Interrupt Mode Register (IMR)
    uint32_t ilr;  //    24h ILR Interrupt Level Register (ILR)
    uint32_t reserved[54];
    uint32_t ivar[32];     // 100h to 17Ch IVAR Interrupt Vector Address Register (IVAR)
//    uint32_t reserved1[32]; // 0x180
//    uint64_t ivear[32];    // 200h to 2FCh IVEAR Interrupt Vector Extended Address Register (IVEAR)
}hw_isr__reg_t;

typedef struct {
    hw_isr__cb_t cb[32];
}hw_isr__data_t;

typedef struct {
    hw_isr__reg_t* reg;
    hw_isr__mode_e mode;
}hw_isr__tbl_t;

typedef void (*hw_isr__func_t)(void);

static void hw_isr__isr_handler(void *InstancePtr);
static void hw_isr__isr0_cb(void) __attribute__ ((fast_interrupt));
static void hw_isr__isr1_cb(void) __attribute__ ((fast_interrupt));
static void hw_isr__isr2_cb(void) __attribute__ ((fast_interrupt));
static void hw_isr__isr3_cb(void) __attribute__ ((fast_interrupt));


static hw_isr__func_t hw_isr__func_tbl[HW_ISR__MAX_CNT] = {hw_isr__isr0_cb, hw_isr__isr1_cb, hw_isr__isr2_cb, hw_isr__isr3_cb};

// Расположение контроллеров в одной цепочке в таблице должно быть последовательным!!!
static hw_isr__tbl_t hw_isr__tbl[HW_ISR__CNT];

static uint8_t module__init;
static hw_isr__data_t hw_isr__data[HW_ISR__CNT];

events__e hw_isr__init(uint8_t id, uint32_t settings, u32 addr, hw_isr__mode_e mode) {
    if (id >= HW_ISR__CNT)
        return EVENT__PARAM_NA;
    hw_isr__tbl_t* p_tbl = &hw_isr__tbl[id];
    p_tbl->mode = mode;
    p_tbl->reg = (hw_isr__reg_t*)addr;
    p_tbl->reg->mer = 0;
    p_tbl->reg->ier = ((mode != HW_ISR__MODE_NORMAL) && (mode != HW_ISR__MODE_LAST)) ? (1 << 31) : 0; // Включаем каскадные прерывания
    p_tbl->reg->iar = 0xFFFFFFFF;
    for(int i = 0; i < 32; i++) {
        p_tbl->reg->ivar[i] = (uint32_t)(void*)hw_isr__func_tbl[id];
    }
    if (!module__init) {
        module__init = 1;
#ifdef __MICROBLAZE__
        microblaze_register_handler(hw_isr__isr_handler, NULL);
#endif
    }
    p_tbl->reg->mer = (settings & HW_ISR__MODE_SW) ? 1 : 3;
    p_tbl->reg->imr = 0xFFFFFFFF; // Fast interrupt mode
    return EVENT__OK;
}

events__e hw_isr__ctrl(uint8_t id, uint8_t enable) {
    if (id >= HW_ISR__CNT)
        return EVENT__PARAM_NA;
    hw_isr__reg_t* reg = hw_isr__tbl[id].reg;
    reg->mer = enable ? 1 : 0;
    return EVENT__OK;
}

events__e hw_isr__start(uint8_t id, uint8_t interrupt, hw_isr__cb_t cb) {
    if (id >= HW_ISR__CNT)
        return EVENT__PARAM_NA;
    hw_isr__reg_t* reg = hw_isr__tbl[id].reg;
    hw_isr__data_t* d = &hw_isr__data[id];
    d->cb[interrupt] = cb;
    reg->sie = (1 << interrupt);
    return EVENT__OK;
}

events__e hw_isr__stop(uint8_t id, uint8_t interrupt) {
    hw_isr__reg_t* reg = hw_isr__tbl[id].reg;
    reg->cie = (1 << interrupt);
    return EVENT__OK;
}

static void hw_isr__check(uint8_t id) {
    hw_isr__reg_t* reg = hw_isr__tbl[id].reg;
    hw_isr__data_t* d = &hw_isr__data[id];
    uint8_t cur_isr = reg->ivr & 0xFF;
    if (cur_isr > 31)
        return;
    if (d->cb[cur_isr])
        d->cb[cur_isr](id, cur_isr);
    reg->iar = (1 << cur_isr);
}

// todo Режим NORMAL пока не реализован!!!
static void hw_isr__isr_handler(void *InstancePtr) {

}

static void hw_isr__isr0_cb(void) {
    hw_isr__check(0);
}

static void hw_isr__isr1_cb(void) {
    hw_isr__check(1);
}

static void hw_isr__isr2_cb(void) {
    hw_isr__check(2);
}

static void hw_isr__isr3_cb(void) {
    hw_isr__check(3);
}
