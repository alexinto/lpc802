﻿/***************************************************************************//**
 * @file cmd_oscillator.с.
 * @brief Модуль, реализующий управление генераторами частот.
 * @author a.tushentsov.
 ******************************************************************************/
#include <stddef.h>
#include <string.h>
#include <math.h>
#include "target.h"
#include "DRV/SyncClock/cmd_oscillator.h"
#include "DRV/HMC7044/hmc7044.h"
#include "System/rxiTime.h"
#include "System/fdebug.h"

#ifndef CMD_OSCILLATOR_NUM
#define CMD_OSCILLATOR_NUM 1
#endif

#define SPI_EXEC_ADD(a,b,c,d,e) {oscil_hw_func((int)a + SYNCCLOCK__ITF_MAX * oscil_id, b, c, d, e); \
                                   }
#define SYNCCLOCK__FREQ_NUM 764                      // Количество частот в сетке
#define FREQ_CHECK        2800000000                 // Проверка 2.8Ггц
#define FREQ_FIN          ((double)100)              // Входная частота 100Мгц
#define FREQ_FGUN_MAX     ((double)3200)             // Максимальная частота ГУНа
#define FREQ_FGUN_MIN     ((double)2400)             // Минимальная частота ГУНа
#define FREQ_M            (FREQ_FGUN_MAX / FREQ_FIN) //
#define FREQ_N            (FREQ_FGUN_MIN / FREQ_FIN) //
#define FREQ_OUT_MIN      ((double)55)               // Минимальная выходная частота
#define FREQ_OUT_MAX      ((double)110)              // Максимальная выходная частота

#define NO_PARAM          0xFFFF                     // критерий окончания таблиц
#define SYNC_1_CFG        0x13                       // правильная конфигурация 1й микросхемы hmc7044
#define SYNC_2_CFG        0x13                       // правильная конфигурация 2й микросхемы hmc7044

typedef struct {
    double freqreal;                                 // FreqReal - Полученная частота после приближения
    int n[2];                                        // N[0]     - множитель первого ГУН = 32
    int d[2];                                        // D[0]     - делитель первого  ГУН
    uint32_t freq_gun[2];                            // [0] - частота 1го ГУН, [1] - частота 2-го ГУН
    uint16_t sysref[2];
}freq_cal_t;

typedef struct {
    uint16_t reg;
    uint8_t data;
}param_tbl__t;

typedef struct {
    uint32_t freq_real;               // Реальная частота
    uint16_t ch_div;                  // Делители каналов (для однодоменной синхронизации)
    cmd_oscillator__ch_t ch_cfg;      // Текущий статус каналов HMC7044 и частота
    hmc7044__out_struct_t* ch_addr;   // Адреса регистров каналов
}oscil_ch_cfg__t;

typedef struct {
    uint8_t init;
    uint8_t ref_clk;                                                    // Источник опорной частоты. 0- 100Мгц, 1- внешний DUT.
    uint8_t set_freq_ok;                                                // 1 - установка частоты произведена успешно.
    uint8_t syncro_check;                                               // Проверка статуса получения синхроимпульса
    uint8_t calib_mode;                                                 // Режим работы с калибровочными параметрами
    cmd_oscillator__setup_e mode;                                       // Режим расчета частоты
    freq_cal_t p_cal;                                                   // Расчетные параметры микросхем
    oscil_ch_cfg__t ch[SYNCCLOCK__CH_MAX];                              // Текущая конфигурация каналов
    cmd_oscillator__pcal_t freq_tbl[MAX_PARAM_NUM * FREQ_IN_PARAM + 1]; // Таблица калибровочных параметров
}oscil_data__t;

static oscil_ch_cfg__t ch_default_tbl[] = {
            {100000000, 0, {OSCIL_CH_ID__FDOM1, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[0]},
            {100000000, 0, {OSCIL_CH_ID__FDOM1, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[1]},
            {100000000, 0, {OSCIL_CH_ID__FDOM1, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[2]},
            {100000000, 0, {OSCIL_CH_ID__FDOM1, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[3]},
            {100000000, 0, {OSCIL_CH_ID__FDOM1, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[4]},
            {100000000, 0, {OSCIL_CH_ID__FDOM1, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[5]},
            {100000000, 0, {OSCIL_CH_ID__FDOM1, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[6]},
            {100000000, 0, {OSCIL_CH_ID__FDOM2, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[7]},
            {100000000, 0, {OSCIL_CH_ID__FDOM2, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[8]},
            {100000000, 0, {OSCIL_CH_ID__FDOM2, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[9]},
            {100000000, 0, {OSCIL_CH_ID__FDOM2, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[10]},
            {100000000, 0, {OSCIL_CH_ID__FDOM2, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[11]},
            {100000000, 0, {OSCIL_CH_ID__FDOM2, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[12]},
            {100000000, 0, {OSCIL_CH_ID__FDOM2, OSCIL_CH_MODE__OFF, 100000000},   (hmc7044__out_struct_t*)HMC7044_REG->out_ctrl[13]},
};

static oscillator__transport_t oscil_hw_func;
static oscil_data__t oscil_data[CMD_OSCILLATOR_NUM];

// Таблица значений по- умолчанию HMC7044 первой ступени
static const param_tbl__t p1_default_tbl[] = {
    {HMC7044_REG->out_ctrl[2].channel, 0xF0},     {HMC7044_REG->out_ctrl[3].channel, 0xF0},     {HMC7044_REG->out_ctrl[6].channel, 0xF0},
    {HMC7044_REG->out_ctrl[7].channel, 0xF0},     {HMC7044_REG->out_ctrl[8].channel, 0xF0},     {HMC7044_REG->out_ctrl[9].channel, 0xF0},
    {HMC7044_REG->out_ctrl[10].channel, 0xF0},    {HMC7044_REG->out_ctrl[11].channel, 0xF0},    {HMC7044_REG->out_ctrl[12].channel, 0xF0},
    {HMC7044_REG->out_ctrl[13].channel, 0xF0},    {HMC7044_REG->clk_out_drv_pw[0], 0x4D},       {HMC7044_REG->clk_out_drv_pw[1], 0xDF},
    {HMC7044_REG->pll1_delay, 0x06},              {HMC7044_REG->pll1_holdover, 0x06},           {HMC7044_REG->vtune_preset, 0x09},
    {HMC7044_REG->gpi_ctrl[2], 0x00},             {HMC7044_REG->gpi_ctrl[3], 0x00},             {HMC7044_REG->en_ctrl[0], 0x2E},
    {HMC7044_REG->pll2_r2[0], 0x01},              {HMC7044_REG->pll2_r2[1], 0x00},              {HMC7044_REG->pll2_n2[0], 32},
    {HMC7044_REG->pll2_n2[1], 0x00},              {HMC7044_REG->pll2_doubler, 0x01},            {HMC7044_REG->pll1_lock_det, 0x20},
    {HMC7044_REG->clk_in_psc[0], 0x01},           {HMC7044_REG->pll1_r1[0], 0x32},              {HMC7044_REG->pll1_r1[1], 0x00},
    {HMC7044_REG->pll1_n1[0], 0x32},              {HMC7044_REG->pll1_n1[1], 0x00},              {HMC7044_REG->glob_mode, 0x41},
    {HMC7044_REG->sync, 0x02},                    {HMC7044_REG->clk_in_buff[0], 0x07},          {HMC7044_REG->oscin_buf_ctrl, 0x07},
    {HMC7044_REG->sysref_tmr[0], 0x80},           {HMC7044_REG->sysref_tmr[1], 0x04},           {HMC7044_REG->out_ctrl[4].ch_div[0], 128},
    {HMC7044_REG->out_ctrl[4].ch_div[1], 0},      {HMC7044_REG->out_ctrl[4].driver_mode, 0x08}, {HMC7044_REG->out_ctrl[4].channel, 0xD1},
    {HMC7044_REG->out_ctrl[1].ch_div[0], 128},    {HMC7044_REG->out_ctrl[1].ch_div[1], 0},      {HMC7044_REG->out_ctrl[1].driver_mode, 0x10},
    {HMC7044_REG->out_ctrl[1].channel, 0xD1},     {HMC7044_REG->pulse_gen, 0x01},               {HMC7044_REG->out_ctrl[0].ch_div[0], 0x80},
    {HMC7044_REG->out_ctrl[0].ch_div[1], 0x04},   {HMC7044_REG->out_ctrl[0].driver_mode, 0xA3}, {HMC7044_REG->out_ctrl[0].channel, 0xDD},
    {HMC7044_REG->mode_ctrl[0], 0x62},            {HMC7044_REG->mode_ctrl[0], 0x60},
    {NO_PARAM, 0}
};

// Таблица значений по- умолчанию HMC7044 второй ступени
static const param_tbl__t p2_default_tbl[] = {
    {HMC7044_REG->clk_out_drv_pw[0], 0x4D},       {HMC7044_REG->clk_out_drv_pw[1], 0xDF},       {HMC7044_REG->pll1_delay, 0x06},
    {HMC7044_REG->pll1_holdover, 0x06},           {HMC7044_REG->vtune_preset, 0x09},            {HMC7044_REG->gpi_ctrl[2], 0x00},
    {HMC7044_REG->gpi_ctrl[3], 0x00},             {HMC7044_REG->en_ctrl[0], 0x2E},              {HMC7044_REG->pll2_r2[0], 0x01},
    {HMC7044_REG->pll2_r2[1], 0x00},              {HMC7044_REG->pll2_n2[0], 0x80},              {HMC7044_REG->pll2_n2[1], 0x00},
    {HMC7044_REG->pll2_doubler, 0x01},            {HMC7044_REG->glob_mode, 0x40},               {HMC7044_REG->sync, 0x02},
    {HMC7044_REG->pulse_gen, 0x01},               {HMC7044_REG->oscin_buf_ctrl, 0x07},          {HMC7044_REG->sysref_tmr[0], 0x40},{HMC7044_REG->sysref_tmr[1], 0x04},
    {HMC7044_REG->out_ctrl[0].ch_div[0], 32},     {HMC7044_REG->out_ctrl[0].ch_div[1], 0},      {HMC7044_REG->out_ctrl[0].driver_mode, 0x10},{HMC7044_REG->out_ctrl[0].channel, 0xC0},{HMC7044_REG->out_ctrl[0].mux_mode, 0x01},{HMC7044_REG->out_ctrl[0].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[1].ch_div[0], 32},     {HMC7044_REG->out_ctrl[1].ch_div[1], 0},      {HMC7044_REG->out_ctrl[1].driver_mode, 0x10},{HMC7044_REG->out_ctrl[1].channel, 0xC0},{HMC7044_REG->out_ctrl[1].mux_mode, 0x01},{HMC7044_REG->out_ctrl[1].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[2].ch_div[0], 32},     {HMC7044_REG->out_ctrl[2].ch_div[1], 0},      {HMC7044_REG->out_ctrl[2].driver_mode, 0x10},{HMC7044_REG->out_ctrl[2].channel, 0xC0},{HMC7044_REG->out_ctrl[2].mux_mode, 0x01},{HMC7044_REG->out_ctrl[2].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[3].ch_div[0], 32},     {HMC7044_REG->out_ctrl[3].ch_div[1], 0},      {HMC7044_REG->out_ctrl[3].driver_mode, 0x10},{HMC7044_REG->out_ctrl[3].channel, 0xC0},{HMC7044_REG->out_ctrl[3].mux_mode, 0x01},{HMC7044_REG->out_ctrl[3].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[4].ch_div[0], 32},     {HMC7044_REG->out_ctrl[4].ch_div[1], 0},      {HMC7044_REG->out_ctrl[4].driver_mode, 0x10},{HMC7044_REG->out_ctrl[4].channel, 0xC0},{HMC7044_REG->out_ctrl[4].mux_mode, 0x01},{HMC7044_REG->out_ctrl[4].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[5].ch_div[0], 32},     {HMC7044_REG->out_ctrl[5].ch_div[1], 0},      {HMC7044_REG->out_ctrl[5].driver_mode, 0x10},{HMC7044_REG->out_ctrl[5].channel, 0xC0},{HMC7044_REG->out_ctrl[5].mux_mode, 0x01},{HMC7044_REG->out_ctrl[5].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[6].ch_div[0], 32},     {HMC7044_REG->out_ctrl[6].ch_div[1], 0},      {HMC7044_REG->out_ctrl[6].driver_mode, 0x10},{HMC7044_REG->out_ctrl[6].channel, 0xC0},{HMC7044_REG->out_ctrl[6].mux_mode, 0x01},{HMC7044_REG->out_ctrl[6].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[7].ch_div[0], 32},     {HMC7044_REG->out_ctrl[7].ch_div[1], 0},      {HMC7044_REG->out_ctrl[7].driver_mode, 0x10},{HMC7044_REG->out_ctrl[7].channel, 0xC0},{HMC7044_REG->out_ctrl[7].mux_mode, 0x01},{HMC7044_REG->out_ctrl[7].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[8].ch_div[0], 32},     {HMC7044_REG->out_ctrl[8].ch_div[1], 0},      {HMC7044_REG->out_ctrl[8].driver_mode, 0x10},{HMC7044_REG->out_ctrl[8].channel, 0xC0},{HMC7044_REG->out_ctrl[8].mux_mode, 0x01},{HMC7044_REG->out_ctrl[8].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[9].ch_div[0], 32},     {HMC7044_REG->out_ctrl[9].ch_div[1], 0},      {HMC7044_REG->out_ctrl[9].driver_mode, 0x10},{HMC7044_REG->out_ctrl[9].channel, 0xC0},{HMC7044_REG->out_ctrl[9].mux_mode, 0x01},{HMC7044_REG->out_ctrl[9].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[10].ch_div[0], 32},    {HMC7044_REG->out_ctrl[10].ch_div[1], 0},     {HMC7044_REG->out_ctrl[10].driver_mode, 0x10},{HMC7044_REG->out_ctrl[10].channel, 0xC0},{HMC7044_REG->out_ctrl[10].mux_mode, 0x01},{HMC7044_REG->out_ctrl[10].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[11].ch_div[0], 32},    {HMC7044_REG->out_ctrl[11].ch_div[1], 0},     {HMC7044_REG->out_ctrl[11].driver_mode, 0x10},{HMC7044_REG->out_ctrl[11].channel, 0xC0},{HMC7044_REG->out_ctrl[11].mux_mode, 0x01},{HMC7044_REG->out_ctrl[11].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[12].ch_div[0], 32},    {HMC7044_REG->out_ctrl[12].ch_div[1], 0},     {HMC7044_REG->out_ctrl[12].driver_mode, 0x10},{HMC7044_REG->out_ctrl[12].channel, 0xC0},{HMC7044_REG->out_ctrl[12].mux_mode, 0x01},{HMC7044_REG->out_ctrl[12].a_delay, 0x0A},
    {HMC7044_REG->out_ctrl[13].ch_div[0], 32},    {HMC7044_REG->out_ctrl[13].ch_div[1], 0},     {HMC7044_REG->out_ctrl[13].driver_mode, 0x10},{HMC7044_REG->out_ctrl[13].channel, 0xC0},{HMC7044_REG->out_ctrl[13].mux_mode, 0x01},{HMC7044_REG->out_ctrl[13].a_delay, 0x0A},
    {HMC7044_REG->mode_ctrl[0], 0x62},            {HMC7044_REG->mode_ctrl[0], 0x60},
    {NO_PARAM, 0}
};

static freq_cal_t* find_rational_fraction(uint8_t oscil_id, uint32_t frequency, double precision);
static cmd_oscillator__pcal_t* cmd_param_find(uint8_t oscil_id, uint32_t freq);
static events__e osclil__set_freq(uint8_t oscil_id, uint8_t ch, uint32_t freq, double precision);
static void oscil__reset(uint8_t oscil_id, syncclock__spi_id_e id);
static void oscil__out_ctrl(uint8_t oscil_id, uint8_t id, cmd_oscillator__ch_mode_e mode);
static cmd_oscillator__pcal_t* oscil__config(uint8_t oscil_id);
static void oscil_ch_div_set(uint8_t oscil_id, uint8_t ch, uint32_t freq);
static uint32_t find_freq(uint32_t frequency);
static events__e cmd_oscillator__sync_check(uint8_t oscil_id);
static events__e get_ch_id(uint8_t oscil_id, cmd_oscillator__ch_id_e grp, uint8_t* ch_prev);

/*******************************************************************************
 * Функция инициализации модуля. Конфигурирует все SyncClock-и с параметрами по-умолчанию.
 ******************************************************************************/
events__e cmd_oscillator__init(oscillator__transport_t hw_func) {
    if (hw_func)
        oscil_hw_func = hw_func;
    events__e result = EVENT__OK;
    param_tbl__t* cur_p_tbl[2];
    uint8_t buff[2];
    uint8_t check[2] = {SYNC_1_CFG, SYNC_2_CFG}; // Результат конфигурации SYNCCLOCK__GEN1, SYNCCLOCK__GEN2
    oscil_data__t* data;
    for(uint8_t oscil_id = 0; oscil_id < CMD_OSCILLATOR_NUM; oscil_id++) {
        cur_p_tbl[0] = (param_tbl__t*)p1_default_tbl;
        cur_p_tbl[1] = (param_tbl__t*)p2_default_tbl;
        data = &oscil_data[oscil_id];
        if (!data->init) {
            data->init = 1;
            memset(data->freq_tbl, 0xFF, sizeof(data->freq_tbl));        // Заполняем 0xFF для бинарного поиска
        }
        // Инициализация значениями по-умолчанию
        memcpy(data->ch, ch_default_tbl, sizeof(data->ch));
        buff[0] = 0;
        buff[1] = 1;
        for (int i = 0; i < 2; i++) {                // Сброс 2х HMC7044
            SPI_EXEC_ADD(i, SYNCCLOCK__OP_WRITE, HMC7044_REG->soft_reset, &buff[1], 1);
            DELAY_MS(40);
            SPI_EXEC_ADD(i, SYNCCLOCK__OP_WRITE, HMC7044_REG->soft_reset, &buff[0], 1);
            DELAY_MS(40);
        }
        DELAY_MS(100);
        data->set_freq_ok = 1;
        // Конфигурация 2х генераторов (TRIPLE_SPI__HMC_7044_1 и TRIPLE_SPI__HMC_7044_2)
        for (int osc_num = 0; osc_num < 2; osc_num++) {
            while (cur_p_tbl[osc_num]->reg != NO_PARAM) {
                SPI_EXEC_ADD(osc_num, SYNCCLOCK__OP_WRITE, cur_p_tbl[osc_num]->reg, &cur_p_tbl[osc_num]->data, 1);
                if (cur_p_tbl[osc_num]->reg == HMC7044_REG->pll2_doubler)
                    DELAY_MS(40);
                cur_p_tbl[osc_num]++;
            }
            DELAY_MS(100);
            SPI_EXEC_ADD(osc_num, SYNCCLOCK__OP_READ, HMC7044_REG->alarm_read[0], buff, 1);
            if (*buff != check[osc_num]) {
                data->set_freq_ok = 0;
                result = EVENT__HW_ERROR;
            }
            FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: Init step %d = %s \n\r", oscil_id, osc_num, data->set_freq_ok ? "OK" : "ERROR");
        }
        cmd_oscillator__set(oscil_id, 0, OSCIL_SETUP__FDOM1);
    }
    FDEBUG(FDEBUG__LVL3, "SyncClock init = %s \n\r", result == EVENT__OK ? "OK" : "ERROR");
    return result;
}

/*******************************************************************************
 * Функция группировки каналов. Назначает каналам группу.
 ******************************************************************************/
events__e cmd_oscillator__ch_grp(uint8_t oscil_id, uint32_t ch_mask, cmd_oscillator__ch_id_e group) {
    oscil_data__t* data = &oscil_data[oscil_id];
    ch_mask &= (0xFFFFFFFF >> (32 - SYNCCLOCK__CH_MAX));
    for(int i = 0; ch_mask; ch_mask >>= 1, i++) {
        if (ch_mask & 0x01) {
            data->ch[i].ch_cfg.ch = group;
            ch_default_tbl[i].ch_cfg.ch = group;
        }
    }
    return EVENT__OK;
}

/*******************************************************************************
 * Функция возвращает указатель на таблицу калибровки. При 1м обращении заполняет
 * таблицу 0xFF - создание границ для бинарного поиска.
 ******************************************************************************/
cmd_oscillator__pcal_t* cmd_oscillator__get_tbl(uint8_t oscil_id) {
    oscil_data__t* data = &oscil_data[oscil_id];
    if (!data->init) {
        data->init = 1;
        memset(data->freq_tbl, 0xFF, sizeof(data->freq_tbl));        // Заполняем 0xFF для бинарного поиска
    }
    return data->freq_tbl;
}

/*******************************************************************************
 * Функция считывает калибровочные параметры из микросхемы и возвращает их.
 ******************************************************************************/
cmd_oscillator__pcal_t  cmd_oscillator__get_pcal(uint8_t oscil_id, cmd_oscillator__ch_id_e ch) {
    uint8_t buff, ch_id = 0xFF;
    cmd_oscillator__pcal_t res = {.freq = 0};
    if (get_ch_id(oscil_id, ch, &ch_id) != EVENT__OK) {
        FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: get_pcal from ch %d = ERROR\n\r", oscil_id, ch);
        return res;
    }
    oscil_data__t* data = &oscil_data[oscil_id];
    res.freq = data->ch[ch_id].freq_real;  // возвращаем текущую частоту канала
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_READ, HMC7044_REG->pll2_stat, &buff, 1);  // todo Уточнить у Владимира про банки
    res.oscil1_bank = buff;
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_READ, HMC7044_REG->pll2_stat, &buff, 1);
    res.oscil2_bank = buff;
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_READ, HMC7044_REG->out_ctrl[1].a_delay, &buff, 1);
    res.oscil1_a_delay = buff;
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_READ, HMC7044_REG->out_ctrl[1].d_delay, &buff, 1);
    res.oscil1_d_delay = buff;
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_READ, (uint32_t) &data->ch[ch_id].ch_addr->a_delay, &buff, 1);
    res.oscil2_a_delay = buff;
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_READ, (uint32_t) &data->ch[ch_id].ch_addr->d_delay, &buff, 1);
    res.oscil2_d_delay = buff;
    return res;
}

/*******************************************************************************
 * Функция установки режима работы с калибровочными параметрами.
 ******************************************************************************/
void cmd_oscillator__pcal_mode(uint8_t oscil_id, uint8_t mode) {
    uint8_t buff = 0;
    oscil_data__t* data = &oscil_data[oscil_id];
    if (mode) {
        SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->bank_cap, &buff, 1);
        SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, HMC7044_REG->bank_cap, &buff, 1);
    }
    data->calib_mode = mode;
    FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: set_pcal_mode %d\n\r", oscil_id, mode);
}

/*******************************************************************************
 * Функция установки задержек канала.
 ******************************************************************************/
void cmd_oscillator__ch_delay(uint8_t oscil_id, cmd_oscillator__ch_id_e ch, uint8_t d_delay, uint8_t a_delay) {
    uint8_t buff = d_delay, ch_id = 0xFF;
    oscil_data__t* data = &oscil_data[oscil_id];
    if (get_ch_id(oscil_id, ch, &ch_id) != EVENT__OK) {
        FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: set_ch_delay(%d, %d) to grp %d = ERROR\n\r", oscil_id, d_delay, a_delay, ch);
        return;
    }
    do {
        SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, (uint32_t) &data->ch[ch_id].ch_addr->d_delay, &buff, 1);
        buff = a_delay;
        SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, (uint32_t) &data->ch[ch_id].ch_addr->a_delay, &buff, 1);
    } while(get_ch_id(oscil_id, ch, &ch_id) == EVENT__OK);
}

/*******************************************************************************
 * Функция настройки генератора.
 ******************************************************************************/
events__e cmd_oscillator__set(uint8_t oscil_id, uint8_t mode, cmd_oscillator__setup_e setup) {
    cmd_oscillator__ch_id_e grp = OSCIL_CH_ID__FDOM1;
    switch(setup) {
        case OSCIL_SETUP__FDOM2:
            grp = OSCIL_CH_ID__FDOM2;
        case OSCIL_SETUP__FDOM1:
            break;
        default:
            return EVENT__PARAM_NA;
    }
    oscil_data__t* data = &oscil_data[oscil_id];
    data->mode = setup;
    uint8_t ch = 0xFF;
    events__e res = get_ch_id(oscil_id, grp, &ch);
    if (res == EVENT__OK) {
        if (data->ref_clk != mode) {
            oscil_hw_func(SYNCCLOCK__SWITCH, SYNCCLOCK__OP_WRITE, 0, &mode, 1);
            data->ref_clk = mode;
        }
        if (!mode)
            res = osclil__set_freq(oscil_id, ch, data->ch[ch].ch_cfg.freq, 1);
    }
    return res;
}

/*******************************************************************************
 * Функция получения настроек генератора.
 ******************************************************************************/
cmd_oscillator__t cmd_oscillator__get(uint8_t oscil_id) {
    cmd_oscillator__t res = {0};
    res.setup = oscil_data[oscil_id].mode;
    res.mode = 0;
    return res;
}

/*******************************************************************************
 * Функция настройки канала генератора.
 ******************************************************************************/
events__e cmd_oscillator__set_ch(uint8_t oscil_id, cmd_oscillator__ch_id_e ch, cmd_oscillator__ch_mode_e mode, uint32_t freq) {
    uint8_t ch_id = 0xFF;
    oscil_data__t* data = &oscil_data[oscil_id];
    if (get_ch_id(oscil_id, ch, &ch_id) != EVENT__OK)
        return EVENT__PARAM_NA;
    uint8_t cur_ch = ch_id;
    do {
        data->ch[cur_ch].ch_cfg.state = mode;
    } while (get_ch_id(oscil_id, ch, &cur_ch) == EVENT__OK);
    return osclil__set_freq(oscil_id, ch_id, freq, 1);
}

/*******************************************************************************
 * Функция получения настроек канала генератора.
 ******************************************************************************/
cmd_oscillator__ch_t* cmd_oscillator__get_ch(uint8_t oscil_id, cmd_oscillator__ch_id_e ch, uint32_t* real_freq) {
    uint8_t ch_id = 0xFF;
    if (get_ch_id(oscil_id, ch, &ch_id) != EVENT__OK)
        return NULL;
    cmd_oscillator__sync_check(oscil_id);
    if (real_freq)
        *real_freq = oscil_data[oscil_id].ch[ch_id].freq_real;
    return &oscil_data[oscil_id].ch[ch_id].ch_cfg;
}

/*******************************************************************************
 * Функция выравнивания фаз генераторов.
 ******************************************************************************/
events__e cmd_oscillator__align(uint8_t oscil_id, uint8_t mode) {
    events__e result = EVENT__OK;
    uint8_t buff[] = {0xD1, 0x91}, buff_check;
    switch(mode) {
        case 1:
            DELAY_MS(2);
            oscil_data[oscil_id].syncro_check = 1;
        case 0:
            SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->out_ctrl[4].channel, &buff[mode], 1);
            SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_READ, HMC7044_REG->out_ctrl[4].channel, &buff_check, 1);
            if (buff[mode] != buff_check)             // Проверка на записанные данные
                result = EVENT__HW_ERROR;
            break;
        default:
            result = EVENT__PARAM_NA;
    }
    FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: align[%d] is 0x%x = %s(%d)\n\r", oscil_id, mode, buff_check, (result == EVENT__OK) ? "OK": "ERROR", result);
    return result;
}

/*******************************************************************************
 * Функция формирования синхроимпульса.
 ******************************************************************************/
events__e cmd_oscillator__pulse(uint8_t mode) {
    uint8_t pulse_data = 1;
    if (mode > 1)
        return EVENT__PARAM_NA;
    oscil_hw_func(SYNCCLOCK__PULSE, SYNCCLOCK__OP_WRITE, 0, &pulse_data, 1);
    DELAY_MS(1);
    return EVENT__OK;
}

/*******************************************************************************
 * Функция возвращает делитель группы каналов
 ******************************************************************************/
uint16_t oscil_ch_div_get(uint8_t oscil_id, cmd_oscillator__ch_id_e grp) {
    uint8_t ch_id = 0xFF;
    if (get_ch_id(oscil_id, grp, &ch_id) != EVENT__OK)
        return 0;
    oscil_data__t* data = &oscil_data[oscil_id];
    return data->ch[ch_id].ch_div;
}

/*******************************************************************************
 * Функция установки множителя и делителя каналов 2го генератора
 ******************************************************************************/
events__e oscil_mux_set(uint8_t oscil_id, cmd_oscillator__ch_id_e grp, uint16_t mux_value, uint16_t div_value) {
    oscil_data__t* data = &oscil_data[oscil_id];
    uint8_t ch_id = 0xFF;
    uint8_t buff[2];
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_READ, HMC7044_REG->pll2_n2[0], buff, 2);
    if (*(uint16_t*)buff != mux_value) {
        *(uint16_t*)buff = mux_value;
        SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, HMC7044_REG->pll2_n2[0], buff, 2);
        DELAY_MS(40);
        oscil__reset(oscil_id, SYNCCLOCK__GEN2);                      // Обновляем делители каналов
    }
    *(uint16_t*)buff = div_value;
    while (get_ch_id(oscil_id, grp, &ch_id) == EVENT__OK) {
        SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, (uint32_t)&data->ch[ch_id].ch_addr->ch_div[0], buff, 2);
    }
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_READ, HMC7044_REG->alarm_read[0], buff, 1);
    return (*buff == SYNC_2_CFG) ? EVENT__OK : EVENT__HW_ERROR;
}


/*******************************************************************************
 * Функция проверки синхронизации.
 ******************************************************************************/
static events__e cmd_oscillator__sync_check(uint8_t oscil_id) {
    events__e result = EVENT__OK;
    syncclock__spi_id_e ch[] = {SYNCCLOCK__GEN1, SYNCCLOCK__GEN2};
    uint8_t buff, buff_old, check[] = {0x05, 0x05};
    oscil_data__t* data = &oscil_data[oscil_id];
    if (!data->syncro_check)
        return EVENT__OK;
    data->syncro_check = 0;
    for (int i = 0; i < 2; i++) {
        do {
            SPI_EXEC_ADD(ch[i], SYNCCLOCK__OP_READ, HMC7044_REG ->alarm_read[0], &buff_old, 1);
            SPI_EXEC_ADD(ch[i], SYNCCLOCK__OP_READ, HMC7044_REG ->alarm_read[0], &buff, 1);
        } while(buff != buff_old);
        if (buff != check[i]) {
            FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: sync_check = %s (step[%d] = %x)\n\r", oscil_id, "ERROR", i, buff);
            for(int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
                data->ch[i].ch_cfg.state = OSCIL_CH_MODE__ERR;
                oscil__out_ctrl(oscil_id, i, data->ch[i].ch_cfg.state);   // Устанавливаем режим выходов
            }
            result = EVENT__ERROR;
            break;
        }
    }
    if (result == EVENT__OK)
        FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: sync_check = %s (%x)\n\r", oscil_id,"OK", buff);
    return result;
}


/*******************************************************************************
 * Функция поиска калибровочного параметра в таблице.
 ******************************************************************************/
static cmd_oscillator__pcal_t* cmd_param_find(uint8_t oscil_id, uint32_t freq) {
    oscil_data__t* data = &oscil_data[oscil_id];
    cmd_oscillator__pcal_t* first = data->freq_tbl, *last = &data->freq_tbl[MAX_PARAM_NUM * FREQ_IN_PARAM];
    if ((!data->calib_mode) && (freq >= first->freq)) {                        // Проверка на границы
        while (first + 1 < last)                                               // Бинарный поиск
            if (freq > (first + ((last - first) / 2))->freq)
                first += (last - first) / 2;
            else
                last -= (last - first) / 2;
        if (first->freq != 0xFFFFFFFF)
            return (last->freq - freq) > (freq - first->freq) ? first : last;  // Ищем ближайшую частоту
    }
    return NULL;
}

/*******************************************************************************
 * Функция чтения калибровочного параметра в таблице.
 ******************************************************************************/
events__e cmd_oscillator__cc_rd(uint8_t oscil_id, int param, cmd_oscillator__pcal_t* data) {
    oscil_hw_func(SYNCCLOCK__CC, SYNCCLOCK__OP_READ, param * sizeof(data), (uint8_t*)data, sizeof(data));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция записи калибровочного параметра в таблицу.
 ******************************************************************************/
events__e cmd_oscillator__cc_wr(uint8_t oscil_id, int param, cmd_oscillator__pcal_t* data) {
    oscil_hw_func(SYNCCLOCK__CC, SYNCCLOCK__OP_WRITE, param * sizeof(data), (uint8_t*)data, sizeof(data));
    return EVENT__OK;
}

/*******************************************************************************
 * Функция установки частоты канала HMC7044
 ******************************************************************************/
static events__e osclil__set_freq(uint8_t oscil_id, uint8_t ch, uint32_t freq, double precision) {
    oscil_data__t* data = &oscil_data[oscil_id];
    uint8_t buff[2], need_cfg = 0, cur_ch;
    uint32_t old_freq = data->ch[ch].ch_cfg.freq;
    cmd_oscillator__ch_id_e grp = data->ch[ch].ch_cfg.ch;
    uint32_t freq_alg = find_freq(freq);
    FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: set_freq(%lu Hz) to grp %d\n\r", oscil_id, freq, grp);
    switch(data->mode) {
        case OSCIL_SETUP__FDOM1:
            if (data->ch[ch].ch_cfg.ch == OSCIL_CH_ID__FDOM1)
                need_cfg = 1;
            break;
        case OSCIL_SETUP__FDOM2:
            if (data->ch[ch].ch_cfg.ch == OSCIL_CH_ID__FDOM2)
                need_cfg = 1;
            break;
        default:
            return EVENT__PARAM_NA;
    }
    if (!freq_alg)
        return EVENT__PARAM_NOT_FOUND;
    cur_ch = ch;
    data->syncro_check = 0;
    do {
        data->ch[cur_ch].ch_cfg.freq = freq;                        // Запоминаем частоту
    } while (get_ch_id(oscil_id, grp, &cur_ch) == EVENT__OK);
    if (need_cfg) {
        if (find_rational_fraction(oscil_id, freq_alg, precision) == NULL) {// Если частоты не существует, то выходим
            cur_ch = ch;
            do {
                data->ch[cur_ch].ch_cfg.freq = old_freq;                    // Восстанавливаем частоту
            } while (get_ch_id(oscil_id, grp, &cur_ch) == EVENT__OK);
            return EVENT__ERROR;
        }
        cur_ch = ch;
        do {
            data->ch[cur_ch].ch_div = data->p_cal.d[1];
        } while (get_ch_id(oscil_id, grp, &cur_ch) == EVENT__OK);
        cmd_oscillator__pcal_t* cur_calib = oscil__config(oscil_id); // конфигурация 2х микросхем HMC7044
        for (int i = 0; i < SYNCCLOCK__CH_MAX; i++) {             // записываем делители каналов
            oscil_ch_div_set(oscil_id, i, data->ch[i].ch_cfg.freq);
            *(uint16_t*) buff = data->ch[i].ch_div;
            SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, (uint32_t) &data->ch[i].ch_addr->ch_div[0], buff, 2);
        }
        if (cur_calib) {                                     // Если частота найдена в таблице калибровки,
            buff[0] = cur_calib->oscil2_a_delay;             // то записываем задержки каналов
            buff[1] = cur_calib->oscil2_d_delay;
            for (int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
                SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, (uint32_t) &data->ch[i].ch_addr->a_delay, buff, 1);
                SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, (uint32_t) &data->ch[i].ch_addr->d_delay, &buff[1], 1);
            }
        }
        oscil__reset(oscil_id, SYNCCLOCK__GEN2);                      // Обновляем делители каналов
        // Проверка конфигурации
        SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_READ, HMC7044_REG->alarm_read[0], buff, 1);
        SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_READ, HMC7044_REG->alarm_read[0], &buff[1], 1);
        if ((buff[0] != SYNC_1_CFG) || (buff[1] != SYNC_2_CFG)) {
            for(int i = 0; i < SYNCCLOCK__CH_MAX; i++)
                data->ch[i].ch_cfg.state = OSCIL_CH_MODE__ERR;     // выключаем выходы
            data->set_freq_ok = 0;
        }
        else
            data->set_freq_ok = 1;
    }
    else {
        cur_ch = ch;
        do {
            oscil__out_ctrl(oscil_id, cur_ch, OSCIL_CH_MODE__ERR);             // выключаем выход
            oscil_ch_div_set(oscil_id, cur_ch, freq_alg);
            // todo Проверить нужно ли сбрасывать делители при изменении делителя одного канала!!!
            *(uint16_t*) buff = data->ch[cur_ch].ch_div;
            SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, (uint32_t)&data->ch[cur_ch].ch_addr->ch_div[0], buff, 2);
        } while (get_ch_id(oscil_id, grp, &cur_ch) == EVENT__OK);
    }
    for (int i = 0; i < SYNCCLOCK__CH_MAX; i++) {              // Пересчитываем выходную частоту каналов
        data->ch[i].freq_real = data->p_cal.freq_gun[1] / data->ch[i].ch_div;
        oscil__out_ctrl(oscil_id, i, data->ch[i].ch_cfg.state);   // Устанавливаем режим выходов
    }
    DELAY_MS(10);                   // Задержка на включение каналов
    FDEBUG(FDEBUG__LVL3, "SyncClock[%d]: set_freq(%lu Hz) to grp %d = %s\n\r", oscil_id, data->ch[ch].freq_real, grp, data->set_freq_ok ? "OK": "ERROR");
    return data->set_freq_ok ? EVENT__OK : EVENT__HW_ERROR;
}

/*******************************************************************************
 * Функция пересчета делителя канала
 ******************************************************************************/
static void oscil_ch_div_set(uint8_t oscil_id, uint8_t ch, uint32_t freq) {
    oscil_data__t* data = &oscil_data[oscil_id];
    int div;
    double d_div;
    d_div = (double)data->p_cal.freq_gun[1] / freq;
    div = trunc(d_div);
    if (div % 2) {
        if ((d_div - trunc(d_div) > 0.5))
            div += 1;
        else
            div -= 1;
    }
    data->ch[ch].ch_div = div;
}

/*******************************************************************************
 * Функция конфигурации двух микросхем HMC7044
 ******************************************************************************/
static cmd_oscillator__pcal_t* oscil__config(uint8_t oscil_id) {
    oscil_data__t* data = &oscil_data[oscil_id];
    freq_cal_t* freq_cfg = &data->p_cal;
    uint8_t buff[2];
    cmd_oscillator__pcal_t* cur_calib = cmd_param_find(oscil_id, freq_cfg->freqreal * 1000000);
    // 1ая микросхема
    buff[0] = (freq_cfg->freq_gun[0] > FREQ_CHECK) ? 0x2E : 0x36;  // todo Больше + равно ???
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->en_ctrl[0], buff, 1);
    if (cur_calib) {           // Если частота найдена в таблице калибровки- записываем банк1
        *buff = 0x01;
        SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->bank_cap, buff, 1);
        *buff = (cur_calib->oscil1_bank << 2) | 0x01;
        SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->bank_cap, buff, 1);
    }
    *(uint16_t*)buff = freq_cfg->n[0];
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->pll2_n2[0], buff, 2);
    DELAY_MS(40);
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->sysref_tmr[0], (uint8_t*)&data->p_cal.sysref[0], 2);
    *(uint16_t*)buff = freq_cfg->d[0];
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->out_ctrl[4].ch_div[0], buff, 2);
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->out_ctrl[1].ch_div[0], buff, 2);
    SPI_EXEC_ADD(SYNCCLOCK__GEN1, SYNCCLOCK__OP_WRITE, HMC7044_REG->out_ctrl[0].ch_div[0], (uint8_t*)&data->p_cal.sysref[0], 2);
    oscil__reset(oscil_id, SYNCCLOCK__GEN1);
    // 2ая микросхема
    buff[0] = (freq_cfg->freq_gun[1] > FREQ_CHECK) ? 0x2E : 0x36; // todo Больше + равно ???
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, HMC7044_REG->en_ctrl[0], buff, 1);
    if (cur_calib) {           // Если частота найдена в таблице калибровки- записываем банк2
        *buff = 0x01;
        SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, HMC7044_REG->bank_cap, buff, 1);
        *buff = (cur_calib->oscil2_bank << 2) | 0x01;
        SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, HMC7044_REG->bank_cap, buff, 1);
    }
    *(uint16_t*)buff = freq_cfg->n[1];
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, HMC7044_REG->pll2_n2[0], buff, 2);
    DELAY_MS(40);
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, HMC7044_REG->sysref_tmr[0], (uint8_t*)&data->p_cal.sysref[1], 2);
    return cur_calib;
}

/*******************************************************************************
 * Функция сброса делителей HMC7044
 ******************************************************************************/
static void oscil__reset(uint8_t oscil_id, syncclock__spi_id_e id) {
    uint8_t buff[2] = {0x62, 0x60};
    for(int i = 0; i < 2; i++)
        SPI_EXEC_ADD(id, SYNCCLOCK__OP_WRITE, HMC7044_REG->mode_ctrl[0], &buff[i], 1);
    DELAY_MS(100);
}

/*******************************************************************************
 * Функция управления выходами HMC7044
 ******************************************************************************/
static void oscil__out_ctrl(uint8_t oscil_id, uint8_t id, cmd_oscillator__ch_mode_e mode) {
    uint8_t buff = (mode == OSCIL_CH_MODE__ON) ? 0xC1 : 0xC0;
    SPI_EXEC_ADD(SYNCCLOCK__GEN2, SYNCCLOCK__OP_WRITE, (uint32_t)&oscil_data[oscil_id].ch[id].ch_addr->channel, &buff, 1);
}

/*******************************************************************************
 * Функция получения коэффициентов n1, d1, n2, d2 и ближайшей частоты.
 ******************************************************************************/
static freq_cal_t* find_rational_fraction(uint8_t oscil_id, uint32_t frequency, double precision) {
    oscil_data__t* data = &oscil_data[oscil_id];
    freq_cal_t* res = NULL;
    double f1, f2;
    int n1, n2, d1, d2, d_1, i, j, max_div = 0;
    double delta, pmin;
    double freq = (double)frequency / 1000000;
    n1 = FREQ_M;
    pmin = 100;
    for (d_1 = 2 * n1; d_1 >= n1; d_1--) {
        d1 = 2 * d_1;
        f1 = FREQ_FIN * n1 / d1;
        i = round(FREQ_FGUN_MAX / f1);
        j = round(FREQ_FGUN_MIN / f1);
        for (n2 = i; n2 >= j; n2--) {
            d2 = round(FREQ_FIN * n1 * n2 / (freq * d1));
            if ((d2 % 2) == 1)
                continue;
            f2 = (FREQ_FIN * n1 * n2) / (d1 * d2);
            delta = fabs(f2 - freq);
            if (delta < pmin) {
                pmin = delta;
                if (pmin < precision) {
                    data->p_cal.freqreal = f2;
                    data->p_cal.n[0] = n1;
                    data->p_cal.d[0] = d1;
                    data->p_cal.n[1] = n2;
                    data->p_cal.d[1] = d2;
                    data->p_cal.freq_gun[0] = FREQ_FIN * n1 * 1000000;
                    data->p_cal.freq_gun[1] = ( FREQ_FIN * n1 / d1) * n2 * 1000000;
                    data->p_cal.sysref[0] = (((data->p_cal.freq_gun[0] / 3) / d1) / 1000000 + 1) * d1;
                    max_div = d2;
                    res = &data->p_cal;
                }
            }
        }
    }
    if (res) {
        for (int i = 0; i < SYNCCLOCK__CH_MAX; i++) {
            oscil_ch_div_set(oscil_id, i, frequency);                  // Пересчитываем делители всех каналов
            oscil__out_ctrl(oscil_id, i, OSCIL_CH_MODE__ERR);          // выключаем выходы
            if (data->ch[i].ch_div > max_div)
                max_div = data->ch[i].ch_div;
        }
        data->p_cal.sysref[1] = (((data->p_cal.freq_gun[1] / 3) / max_div) / 1000000 + 1) * max_div;
    }
    return res;
}

/*******************************************************************************
 * Функция поиска частоты в соответствии с таблицей частот.
 ******************************************************************************/
static uint32_t find_freq(uint32_t frequency) {
    int count = SYNCCLOCK__FREQ_NUM;
    double f1 = 0, lf0, step, f_prev;
    double freq = (double)frequency / 1000000;
    if ((freq < FREQ_OUT_MIN) || (freq > FREQ_OUT_MAX))
        return 0;
    for(int i = 0; i <= count-1; i++) {
        lf0 = log(FREQ_OUT_MIN);
        step = (log(FREQ_OUT_MAX) - lf0) / (count - 1);
        f_prev = f1;
        f1 = exp(lf0 + i * step);
        if (freq < f1)
            break;
    }
    freq = (f1 - freq > freq - f_prev) ? f_prev : f1;
    return freq * 1000000;
}

/*******************************************************************************
 * Функция поиска частоты в соответствии с таблицей частот.
 ******************************************************************************/
uint32_t get_freq(uint32_t freq_num) {
    double lf0, step, f1;
    if (freq_num >= SYNCCLOCK__FREQ_NUM)
        return 0;
    lf0 = log(FREQ_OUT_MIN);
    step = (log(FREQ_OUT_MAX) - lf0) / (SYNCCLOCK__FREQ_NUM - 1);
    f1 = exp(lf0 + freq_num * step);
    return f1 * 1000000;
}

/*******************************************************************************
 * Функция получения номера канала из группы.
 ******************************************************************************/
static events__e get_ch_id(uint8_t oscil_id, cmd_oscillator__ch_id_e grp, uint8_t* ch_prev) {
    events__e result = EVENT__PARAM_NOT_FOUND;
    oscil_data__t* data = &oscil_data[oscil_id];
    for(uint8_t i = *ch_prev + 1; i < SYNCCLOCK__CH_MAX; i++) {
        if (data->ch[i].ch_cfg.ch == grp) {
            *ch_prev = i;
            result = EVENT__OK;
            break;
        }
    }
    return result;
}




