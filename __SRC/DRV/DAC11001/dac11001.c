/***************************************************************************//**
 * @file dac11001.c.
 * @brief Драйвер DAC11001 (аппаратно зависимая часть).
 * @authors a.tushentsov.
 ******************************************************************************/
#include <math.h>
#include "target.h"
#include "System/fstandard.h"
#include "System/framework.h"
#include "System/sw_timer.h"
#include "DRV/spi.h"
#include "DRV/DAC11001/dac11001.h"

// Настройки интерфейса SPI
#define DAC11001__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_24_MBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB)

#ifndef DAC11001__COUNT
#define DAC11001__COUNT 1
#endif

#define DAC11001__NO_DEV 0xFF

typedef enum {
    DAC11001__RW_WR_DATA = 0,
    DAC11001__RW_WR_ADDR = 1,
    DAC11001__RW_RD_DATA = 2,
}dac11001__rw_e;

typedef enum {
    DAC11001__OP_NONE     = 0,
    DAC11001__OP_WRITE    = 1,
    DAC11001__OP_WR_ADDR  = 2,
    DAC11001__OP_RD_REG   = 3,
    DAC11001__OP_RD_DATA  = 4,
    DAC11001__OP_WR_RST   = 5,
    DAC11001__OP_CLR_RST  = 6,
    DAC11001__OP_CHK_RST  = 7,
    DAC11001__OP_RD_VALUE = 8,
}dac11001__op_e;

#pragma pack(push, 4)
typedef struct {
    u8 addr : 7;
    u8 rw   : 1;
    u8 data[3];
}dac11001__pkt_t;
#pragma pack(pop)

static const dac11001__op_e dac11001__init_tbl[] = {DAC11001__OP_WR_RST, DAC11001__OP_CLR_RST, DAC11001__OP_WR_ADDR, DAC11001__OP_RD_REG, DAC11001__OP_RD_DATA, DAC11001__OP_CHK_RST, DAC11001__OP_NONE};
static const dac11001__op_e dac11001__wr_tbl[] = {DAC11001__OP_WRITE, DAC11001__OP_NONE};
static const dac11001__op_e dac11001__rd_tbl[] = {DAC11001__OP_WR_ADDR, DAC11001__OP_RD_REG, DAC11001__OP_RD_DATA, DAC11001__OP_NONE};
static const dac11001__op_e dac11001__rd_val_tbl[] = {DAC11001__OP_WR_ADDR, DAC11001__OP_RD_REG, DAC11001__OP_RD_VALUE, DAC11001__OP_NONE};

typedef struct {
    u8 err_cnt;
    u8 dev_id;
    u8 init_ok;
    double vref;
    double offset;
    dac11001__pkt_t pkt;
    dac11001__op_e* p_oper;
    dac11001__reg_e reg;
    dac11001__data_u data;
    union {
        dac11001__data_u* p_data;
        double* p_value;
    };
    dac11001__cb_t cb;
    sw_timer__t tmr;
    void* ext_data;
}dac11001__struct_t;

static dac11001__struct_t dac11001__data[DAC11001__COUNT];
static dac11001__handler_t dac11001_handler;
static i32 module__init;

static dac11001__struct_t* dac11001__free_get(i32 dev_id);
static u32 dac__data_get(u8* buff);
static events__e dac11001__rw(dac11001__struct_t* d, dac11001__rw_e rw, dac11001__reg_e reg, dac11001__data_u data, spi__cb_t cb);
static void dac11001__spi_cb(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data);
static events__e dac11001__exec(dac11001__struct_t* d);
static void dac11001__tmr_cb(struct sw_timer__t *timer, void *ext_data);

events__e dac11001__init(i32 dev_id, u32 settings, double vref, dac11001__cb_t cb, void* ext_data) {
    if (!module__init) {
        module__init = 1;
        for(i32 i = 0; i < DAC11001__COUNT; i++)
            dac11001__data[i].dev_id = DAC11001__NO_DEV;
    }
    dac11001__struct_t* d = dac11001__free_get(dev_id);
    if ((d == NULL) && ((d = dac11001__free_get(DAC11001__NO_DEV)) == NULL))
        return EVENT__BUSY;
    events__e res = EVENT__OK;
    if ((settings & DAC11001__SPI_NO_INIT) == 0)
        res = spi__init(dev_id, DAC11001__SPI_SETTINGS);
    d->p_oper = (dac11001__op_e*)dac11001__init_tbl;
    d->dev_id = dev_id;
    d->vref = vref;
    d->cb = cb;
    d->ext_data = ext_data;
    d->err_cnt = 0;
    d->p_data = &d->data;
    d->init_ok = 1;
    if (res == EVENT__OK)
        res = dac11001__exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while((res == EVENT__OK) && (d->p_oper)) {
            ++d->p_oper;
            res = dac11001__exec(d);
        }
        d->p_oper = NULL;
    }
    return res;
}

events__e dac11001__set_vout(i32 dev_id, double voltage, dac11001__cb_t cb, void* ext_data) {
    dac11001__struct_t* d = dac11001__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    voltage += (double)d->vref / 2  + d->offset;
    d->p_oper = (dac11001__op_e*)dac11001__wr_tbl;
    d->cb = cb;
    d->ext_data = ext_data;
    d->reg = DAC11001__DATA;
    d->data.data = round(voltage / d->vref * 0xFFFFF);
    events__e res = EVENT__PARAM_NA;
    if (d->data.data <= 0xFFFFF)
        res = dac11001__exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while((res == EVENT__OK) && (d->p_oper)) {
            ++d->p_oper;
            res = dac11001__exec(d);
        }
        if (dac11001_handler)
            dac11001_handler(dev_id, DAC11001__EVENT_LDAC);
        d->p_oper = NULL;
    }
    return res;
}

events__e dac11001__get_vout(i32 dev_id, double* voltage, dac11001__cb_t cb, void* ext_data) {
    dac11001__struct_t* d = dac11001__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->p_oper = (dac11001__op_e*)dac11001__rd_val_tbl;
    d->cb = cb;
    d->ext_data = ext_data;
    d->reg = DAC11001__DATA;
    d->p_value = voltage;
    events__e res = dac11001__exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while((res == EVENT__OK) && (d->p_oper)) {
            ++d->p_oper;
            res = dac11001__exec(d);
        }
        d->p_oper = NULL;
    }
    return res;
}


events__e dac11001__set_vref(i32 dev_id, double vref, double offset) {
    dac11001__struct_t* d = dac11001__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->vref = vref;
    d->offset = offset;
    return EVENT__OK;
}

double dac11001__get_vref(i32 dev_id) {
    dac11001__struct_t* d = dac11001__free_get(dev_id);
    if (d == NULL)
        return 0;
    return d->vref;
}


static events__e dac11001__exec(dac11001__struct_t* d) {
    events__e res = EVENT__OK;
    if (!d->init_ok)
        return EVENT__DEINIT;
    switch(*d->p_oper) {
        case DAC11001__OP_WRITE:
            res = dac11001__rw(d, DAC11001__RW_WR_DATA, d->reg, d->data, d->cb ? dac11001__spi_cb : NULL);
            break;
        case DAC11001__OP_WR_ADDR:
            d->data.data = 0;
            res = dac11001__rw(d, DAC11001__RW_WR_ADDR, d->reg, d->data, d->cb ? dac11001__spi_cb : NULL);
            break;
        case DAC11001__OP_RD_REG:
            res = dac11001__rw(d, DAC11001__RW_RD_DATA, d->reg, d->data, d->cb ? dac11001__spi_cb : NULL);
            break;
        case DAC11001__OP_RD_DATA:
            d->data.data = dac__data_get(d->pkt.data);
            if (d->p_data)
                *d->p_data = d->data;
            sw_timer__start(&d->tmr, 0, d->cb ? dac11001__tmr_cb : NULL, (void*)d);
            break;
        case DAC11001__OP_WR_RST:
            d->data.data = 0;
            d->data.trigger.srst = 1;
            res = dac11001__rw(d, DAC11001__RW_WR_DATA, DAC11001__TRIGGER, d->data, d->cb ? dac11001__spi_cb : NULL);
            break;
        case DAC11001__OP_CLR_RST:
            d->data.data = 0;
            res = dac11001__rw(d, DAC11001__RW_WR_DATA, DAC11001__TRIGGER, d->data, d->cb ? dac11001__spi_cb : NULL);
            d->reg = DAC11001__CONFIG1;
            break;
        case DAC11001__OP_CHK_RST:
            if (d->data.data != 0x4C8) {                             // 0x4C8 - Значение после сброса
                d->p_oper = (dac11001__op_e*)dac11001__init_tbl;     // Повторяем чтение пока мк не загрузится...
                if (++d->err_cnt < 200)                                  // Таймаут 200мс
                    sw_timer__start(&d->tmr, 1, d->cb ? dac11001__tmr_cb : NULL, (void*)d);
                else {
                    d->init_ok = 0;
                    res = EVENT__ERROR;
                }
            }
            break;
        case DAC11001__OP_RD_VALUE:
            d->data.data = dac__data_get(d->pkt.data);
            if (d->p_value) {
                *d->p_value = (double)d->data.data * d->vref / 0xFFFFF - (double)d->vref / 2 - d->offset;
            }
            sw_timer__start(&d->tmr, 0, d->cb ? dac11001__tmr_cb : NULL, (void*)d);
            break;
        case DAC11001__OP_NONE:
        default:
            d->p_oper = NULL;
            break;
    }
    return res;
}

events__e dac11001__write(i32 dev_id, dac11001__reg_e reg, dac11001__data_u data, dac11001__cb_t cb, void* ext_data) {
    dac11001__struct_t* d = dac11001__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->p_oper = (dac11001__op_e*)dac11001__wr_tbl;
    d->cb = cb;
    d->ext_data = ext_data;
    d->reg = reg;
    d->data = data;
    events__e res = dac11001__exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while((res == EVENT__OK) && (d->p_oper)) {
            ++d->p_oper;
            res = dac11001__exec(d);
        }
        if (dac11001_handler)
            dac11001_handler(dev_id, DAC11001__EVENT_LDAC);
        d->p_oper = NULL;
    }
    return res;
}

events__e dac11001__read(i32 dev_id, dac11001__reg_e reg, dac11001__data_u* data, dac11001__cb_t cb, void* ext_data) {
    dac11001__struct_t* d = dac11001__free_get(dev_id);
    if (d == NULL)
        return EVENT__BUSY;
    d->p_oper = (dac11001__op_e*)dac11001__rd_tbl;
    d->cb = cb;
    d->ext_data = ext_data;
    d->reg = reg;
    d->p_data = data;
    events__e res = dac11001__exec(d);
    if ((res != EVENT__OK) || (cb == NULL)) {
        while((res == EVENT__OK) && (d->p_oper)) {
            ++d->p_oper;
            res = dac11001__exec(d);
        }
        d->p_oper = NULL;
    }
    return res;
}

static events__e dac11001__rw(dac11001__struct_t* d, dac11001__rw_e rw, dac11001__reg_e reg, dac11001__data_u data, spi__cb_t cb) {
    d->pkt.rw = 0;
    d->pkt.addr = reg;
    d->pkt.data[0] = data.data >> 12;
    d->pkt.data[1] = data.data >> 4;
    d->pkt.data[2] = data.data << 4;
    events__e res;
    switch(rw) {
        case DAC11001__RW_WR_ADDR:
            d->pkt.rw = 1;
        case DAC11001__RW_WR_DATA:
            res = spi__txrx(d->dev_id, NULL, 0, (u8*)&d->pkt, sizeof(dac11001__pkt_t), cb, (void*)d);
            break;
        case DAC11001__RW_RD_DATA:
            d->pkt.rw = 1;
            res = spi__txrx(d->dev_id, (u8*) &d->pkt, sizeof(dac11001__pkt_t), NULL, 0, cb, (void*)d);
            break;
    }
    return res;
}

events__e dac11001__handler_set(dac11001__handler_t handler) {
    dac11001_handler = handler;
    return EVENT__OK;
}

static void dac11001__spi_cb(i32 dev_id, events__e event, u8 *buff, i32 len, void *ext_data) {
    dac11001__struct_t* d = (dac11001__struct_t*)ext_data;
    if (event == EVENT__OK) {  // игнорим ошибки- ждем ресета микросхемы
        if (d->p_oper) {
            ++d->p_oper;
            event = dac11001__exec(d);
        };
    }
    if ((event != EVENT__OK) || (d->p_oper == NULL)) {
        d->p_oper = NULL;
        if (d->cb)
            d->cb(d->dev_id, event, (dac11001__reg_e)d->pkt.addr, d->data, d->ext_data);
    }
}

static u32 dac__data_get(u8* buff) {
    return (((u32)buff[0] << 12) | ((u32)buff[1] << 4) | ((u32)buff[2] >> 4));
}

static dac11001__struct_t* dac11001__free_get(i32 dev_id) {
    for(i32 i = 0; i < DAC11001__COUNT; i++) {
        if ((dac11001__data[i].dev_id == dev_id) && (dac11001__data[i].p_oper == NULL))
            return &dac11001__data[i];
    }
    return NULL;
}

static void dac11001__tmr_cb(struct sw_timer__t *timer, void *ext_data) {
    dac11001__struct_t* d = (dac11001__struct_t*)ext_data;
    events__e event = EVENT__OK;
    if (d->p_oper) {
        ++d->p_oper;
        event = dac11001__exec(d);
    }
    if ((event != EVENT__OK) || (d->p_oper == NULL)) {
        d->p_oper = NULL;
        if (d->cb)
            d->cb(d->dev_id, event, (dac11001__reg_e)d->pkt.addr, d->data, d->ext_data);
    }
}
