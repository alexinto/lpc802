﻿/***************************************************************************//**
 * @file hw_supervisor.h.
 * @brief Аппаратно зависимый модуль управления частотой и режимами энергосбережения контроллера.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef HW_SUPERVISOR_H_
#define HW_SUPERVISOR_H_

#include "System/fstandard.h"
#include "System/supervisor.h"

/***************************************************************************//**
 * @brief Функция настройки параметров запуска супервизора (PDV, BOR и др.).
 * @param void.
 * @return void.
 ******************************************************************************/
void hw_supervisor__init(void);

/***************************************************************************//**
 * @brief Функция перезагрузки приложения.
 * @param void.
 * @return void.
 ******************************************************************************/
void hw_supervisor__apl_reset(void);

/***************************************************************************//**
 * @brief Функция перехода контроллера в SLEEP.
 * @param void.
 * @return void.
 ******************************************************************************/
void hw_supervisor__idle_on(void);

/***************************************************************************//**
 * @brief Функция выхода контроллера из SLEEP.
 * @param void.
 * @return void.
 ******************************************************************************/
void hw_supervisor__idle_off(void);

/***************************************************************************//**
 * @brief Функция получения текущей частоты тактирования процессора.
 * @param void.
 * @return Текущая частота процессора.
 ******************************************************************************/
u32 hw_supervisor__cpu_clk_get(void);

/***************************************************************************//**
 * @brief Функция получения текущей частоты тактирования периферии.
 * @param void.
 * @return Текущая частота периферии.
 ******************************************************************************/
u32 hw_supervisor__periph_clk_get(void);

/***************************************************************************//**
 * @brief Функция настройки частоты CPU.
 * @param clk - частота в Гц.
 * @param sleep - флаг того, что нужно уходить в сон:
 *          1 - нужно уйти в сон,
 *          0 - не нужно уходить в сон.
 * @return SUPERVISOR__EVENT_OK - успешная смена частоты,
 *         SUPERVISOR__EVENT_ERROR - неуспешная смена частоты.
 ******************************************************************************/
supervisor__event_e hw_supervisor__clk_set(supervisor__clk_e clk, i32 sleep);

#endif  /* HW_SUPERVISOR_H_ */
