﻿/***************************************************************************//**
 * @file menu_tree.h.
 * @brief Модуль работы с меню.
 * @author a.tushentsov.
 ******************************************************************************/

#ifndef MENU_TREE_H_
#define MENU_TREE_H_

#include "System/fstandard.h"

typedef struct menu__item__t {
    struct menu__item__t *prev;           // Предыдущий элемент меню.
    struct menu__item__t *next;           // Следующий элемент меню.
    struct menu__item__t *parrent;        // Предыдущая группа меню.
    struct menu__item__t *child;          // Следующая группа меню. NULL - параметр.
    char* item_descr;                     // Нуль терминированная строка.
    void* data;                           // Данные элемента меню.
    uint8_t read_only : 1;                // Настройка только чтение
}menu__item__t;

typedef enum {
     MENU__CMD_ADD_CHILD   =  0,
     MENU__CMD_ADD_ITEM    =  1,
     MENU__CMD_DEL_CHILD   =  2,
     MENU__CMD_DEL_ITEM    =  3,
}menu__cmd__e;


/*******************************************************************************
 * @brief Функция- обработчик команд модуля.
 * @param cmd - команда модулю меню.
 *        MENU__CMD_ADD_CHILD - добавляет группу меню(new_item) элементу меню(menu_item).
 *        MENU__CMD_ADD_ITEM  - добавляет элемент меню(new_item) в группу меню(menu_item). Возвращает указатель на новый элемент.
 *        MENU__CMD_DEL_CHILD - удаляет группу меню из элемента меню(menu_item).
 *        MENU__CMD_DEL_ITEM  - удаляет элемент меню(menu_item). Возвращает соседний элемент (сперва предыдущий, если нет, то следующий).
 * @return Возвращаемое значение зависит от команды.
 ******************************************************************************/
menu__item__t* menu__cmd_exec(menu__cmd__e cmd, menu__item__t* menu_item, menu__item__t* new_item);




#endif