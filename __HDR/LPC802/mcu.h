/***************************************************************************//**
 * @file mcu.h
 * @brief ���� � ��������� ����������������.
 *        ��: LPC802M001.
 * @authors a.tushentsov
 ******************************************************************************/
#ifndef MCU_H_
#define MCU_H_

#include <intrinsics.h>
#include "System/fstandard.h"
#include "lpc8xx.h"

#define CRITICAL_SECTION_ON                             \
{                                                       \
    __istate_t critical_section_state = __get_interrupt_state();         \
    __disable_interrupt();

#define CRITICAL_SECTION_OFF                            \
    __set_interrupt_state(critical_section_state);                       \
}


#define LPC802
/*******************************************************************************
 * WDT reset. If #define WDT_ON in target.h use only!
 * CRITICAL_SECTION or IRQ_handler must be < 4ms.
 * If #define WDT_AUTO_RESET_SLEEP wdt will be reset automatically in sleep mode.
 ******************************************************************************/
#define wdt__reset() LPC_WWDT->WINDOW = 0xFFFFFE;
#define NOP __no_operation

/*******************************************************************************
 * �������� GPIO ��.
 ******************************************************************************/
#define MCU__GPIO_PIN_0         (0x00)
#define MCU__GPIO_PIN_1         (0x01)
#define MCU__GPIO_PIN_2         (0x02)
#define MCU__GPIO_PIN_3         (0x03)
#define MCU__GPIO_PIN_4         (0x04)
#define MCU__GPIO_PIN_5         (0x05)
#define MCU__GPIO_PIN_7         (0x07)
#define MCU__GPIO_PIN_8         (0x08)
#define MCU__GPIO_PIN_9         (0x09)
#define MCU__GPIO_PIN_10        (0x0A)
#define MCU__GPIO_PIN_11        (0x0B)
#define MCU__GPIO_PIN_12        (0x0C)
#define MCU__GPIO_PIN_13        (0x0D)
#define MCU__GPIO_PIN_14        (0x0E)
#define MCU__GPIO_PIN_15        (0x0F)
#define MCU__GPIO_PIN_16        (0x10)
#define MCU__GPIO_PIN_17        (0x11)

// UARTs Pins
#define TX__GPIO_PIN_0         (0x00 << 20)
#define TX__GPIO_PIN_1         (0x01 << 20)
#define TX__GPIO_PIN_2         (0x02 << 20)
#define TX__GPIO_PIN_3         (0x03 << 20)
#define TX__GPIO_PIN_4         (0x04 << 20)
#define TX__GPIO_PIN_5         (0x05 << 20)
#define TX__GPIO_PIN_7         (0x07 << 20)
#define TX__GPIO_PIN_8         (0x08 << 20)
#define TX__GPIO_PIN_9         (0x09 << 20)
#define TX__GPIO_PIN_10        (0x0A << 20)
#define TX__GPIO_PIN_11        (0x0B << 20)
#define TX__GPIO_PIN_12        (0x0C << 20)
#define TX__GPIO_PIN_13        (0x0D << 20)
#define TX__GPIO_PIN_14        (0x0E << 20)
#define TX__GPIO_PIN_15        (0x0F << 20)
#define TX__GPIO_PIN_16        (0x10 << 20)
#define TX__GPIO_PIN_17        (0x11 << 20)
#define RX__GPIO_PIN_0         (0x00 << 25)
#define RX__GPIO_PIN_1         (0x01 << 25)
#define RX__GPIO_PIN_2         (0x02 << 25)
#define RX__GPIO_PIN_3         (0x03 << 25)
#define RX__GPIO_PIN_4         (0x04 << 25)
#define RX__GPIO_PIN_5         (0x05 << 25)
#define RX__GPIO_PIN_7         (0x07 << 25)
#define RX__GPIO_PIN_8         (0x08 << 25)
#define RX__GPIO_PIN_9         (0x09 << 25)
#define RX__GPIO_PIN_10        (0x0A << 25)
#define RX__GPIO_PIN_11        (0x0B << 25)
#define RX__GPIO_PIN_12        (0x0C << 25)
#define RX__GPIO_PIN_13        (0x0D << 25)
#define RX__GPIO_PIN_14        (0x0E << 25)
#define RX__GPIO_PIN_15        (0x0F << 25)
#define RX__GPIO_PIN_16        (0x10 << 25)
#define RX__GPIO_PIN_17        (0x11 << 25)
#define TX__GPIO_MASK          (0x1F << 20)
#define RX__GPIO_MASK          (0x1F << 25)
/*******************************************************************************
 * �������� ����������� ��.
 ******************************************************************************/

#define UART_0 0
#define UART_1 1
#define MCU__UART_MAX 2

#define SPI_1 0
#define SPI_2 1
#define MCU__SPI_MAX 2

#define I2C_0 0
#define I2C_1 1
#define I2C_2 2
#define I2C_3 3
#define I2C_4 4
#define MCU__I2C_MAX 5





#endif /* MCU_H_ */
