﻿/***************************************************************************//**
 * @file flash.h
 * @brief Драйвер встроенной флеш памяти LPC802.
 * @authors a.tushentsov
 ******************************************************************************/
#ifndef __FLASH_H
#define __FLASH_H

typedef enum {
    CMD_SUCCESS = 0x00,
    INVALID_COMMAND,
    SRC_ADDR_ERROR,
    DST_ADDR_ERROR,
    SRC_ADDR_NOT_MAPPED,
    DST_ADDR_NOT_MAPPED,
    COUNT_ERROR,
    INVALID_SECTOR,
    SECTOR_NOT_BLANK,
    SECTOR_NOT_PREPARED,
    COMPARE_ERROR,
    FLASH_BUSY,
    PARAM_ERROR,
    ADDR_ERROR,
    ADDR_NOT_MAPPED,
    CMD_LOCKED,
    INVALID_CODE,
    INVALID_BAUD_RATE,
    INVALID_STOP_BIT,
    PROTECTION_ENABLED,
    Reserved1,
    USER_CODE_CHECKSUM,
    Reserved2,
    EFRO_NO_POWER,
    FLASH_NO_POWER,
    Reserved3,
    FLASH_ERASE_PROGRAM,
    INVALID_PAGE,
    Reserved4,
    FLASH_NO_CLOCK,
    REINVOKE_ISP_CONFIG,
    NO_VALID_IMAGE,
}flash_events__e;

flash_events__e flash_write(u8* buff, int len);

flash_events__e flash_read(u8* buff, int len);

#endif