﻿/*******************************************************************************
 * @file flash_int.h.
 * @brief Модуль работы со встроенной памятью МК. Поддерживаются флеш с размером страницы 256 байт.
 *        Кол-во экземпляров флеш задается при помощи дефайна FLASH_INT__COUNT в target.h.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef FLASH_INT_H_
#define FLASH_INT_H_

#include "System/framework.h"
#include "DRV/flash.h"

/***************************************************************************//**
 * @brief Функция инициализации FLASH.
 * @param dev_id - идентификатор устройства.
 * @param type - тип микросхемы.
 * @param addr - адрес во FLASH, по которому необходимо произвести операцию.
 * @param buff - указатель на буфер, куда необходимо поместить прочитанные/записываемые данные.
 * @param len - количество байт, которые необходимо прочитать/записать.
 * @param cb - коллбэк по завершению операции, может быть NULL. В этом случае функция синхронная.
 * @param ext_data - указатель на дополнительные пользовательские данные, который будет передаваться в пользовательский коллбэк при его вызове.
                NULL - если не используется.
 * @return EVENT__OK - операция произведена успешно.
 ******************************************************************************/
events__e flash_int__init(i32 dev_id, flash__type_e type, u32 settings, flash__cb_t cb);

/***************************************************************************//**
 * @brief Функция запуска операции с FLASH_INT. Должна быть реализована драйвером микросхемы.
 * @param dev_id - идентификатор устройства.
 * @param oper - операция, которую нужно выполнить.
 * @param addr - адрес во FLASH_INT, по которому необходимо произвести операцию.
 * @param buff - указатель на буфер, куда необходимо поместить прочитанные/записываемые данные.
 * @param len - количество байт, которые необходимо прочитать/записать.
 * @param cb - коллбэк по завершению операции, может быть NULL. В этом случае функция синхронная.
 * @param ext_data - указатель на дополнительные пользовательские данные, который будет передаваться в пользовательский коллбэк при его вызове.
                NULL - если не используется.
 * @return EVENT__OK - операция произведена успешно.
 ******************************************************************************/
events__e flash_int__exec(u8 dev_id, flash__oper_e oper, u32 addr, u8 *buff, u32 len, flash__cb_t cb, void* ext_data);


#endif /* FLASH_INT_INT_H_ */
