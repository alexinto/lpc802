﻿/***************************************************************************//**
 * @file adc.h.
 * @brief Драйвер ADC для STM32F103xC.
 * @author a.tushentsov.
 ******************************************************************************/
#ifdef STM32F10X_HD

#ifndef ADC_H_
#define ADC_H_

#include "System/framework.h"

typedef void (*adc__cb_t)(u8 id, u8 ch_id, events__e event, float voltage, void* ext_data);

/***************************************************************************//**
 * @brief Функция- инициализации модуля ADS1115. Задается опорное напряжение и скорость
 *        преобразования. Vref задается один на все каналы!
 * @param id - идентификатор ADC.
 * @param ch_id - идентификатор канала (0..13).
 * @param vref - опорное напряжение преобразователя, В.
 * @return EVENT__OK - успешное выполнение.
 *         EVENT__ERROR - ошибка инициализации.
 ******************************************************************************/
events__e adc__init(u8 id, u8 ch_id, float vref);

/***************************************************************************//**
 * @brief Функция получения значений каналов модуля ADS1115. Полученные значения
 *        записываются в переданную структуру. По завершению операции вызывается коллбэк.
 * @param id - идентификатор микросхемы.
 * @param ch_id - идентификатор канала (0..13).
 * @param cb - коллбэк окончания операции. Не может быть NULL.
 * @param ext_data - пользовательские данные.
 * @return EVENT__OK - успешное выполнение.
 *         EVENT__BUSY - модуль занят.
 *         EVENT__PARAM_NA - неверные параметры.
 ******************************************************************************/
events__e adc__get(u8 id, u8 ch_id, adc__cb_t cb, void* ext_data);



#endif  /* ADC_H_ */

#endif /* STM32F10X_HD */
