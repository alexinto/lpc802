/***************************************************************************//**
 * @file pixel.c.
 * @brief ������ ������� ����� � ������.
 * @author a.tushentsov
 ******************************************************************************/
#ifndef _H_PIXEL
#define _H_PIXEL
#include <stdint.h>
#include "System/events.h"

#pragma pack(push, 4)
typedef struct {
	uint8_t b;
	uint8_t g;
	uint8_t r;
	uint8_t y;
}pixel__t;
#pragma pack(pop)

/***************************************************************************//**
 * @brief ������� ���������� �� ���������� ������ ��������� ���������� ��������� (�������).
 * @param handle - ������������� ��������. ���� 0 ��� �������� �� ����������- ��������� ����� �������.
 * @param x - ����������. 0 - ����� ����� �������.
 * @param y - ����������. 0 - ������� ����� �������.
 * @param width - ������������� ������ ��������, �������.
 * @param height - ������������ ������ ��������, �������.
 * @return int- ������������� ��������.
 ******************************************************************************/
int pixel__create(int handle, int x, int y, int width, int height);

events__e pixel__scan(int handle);

events__e pixel__draw(int handle, int x, int y);

pixel__t pixel__get_color(int handle, uint32_t index);

int pixel__cmp(pixel__t* pix1, pixel__t* pix2, uint8_t acc, uint8_t gyst);  // acc - �������� 0..7, gyst - ����������

events__e pixel__save(int handle, char* filename);

int pixel__load(char* filename);









#endif
