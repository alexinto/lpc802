﻿/***************************************************************************//**
 * @file windows_api.h
 * @brief Обертка windows_api вызовов
 * @author p.adaskin
 ******************************************************************************/
#ifndef WINDOWS_API_H
#define WINDOWS_API_H

#define WINDOWS_API__SND_SYNC            0x0000  /* play synchronously (default) */
#define WINDOWS_API__SND_ASYNC           0x0001  /* play asynchronously */
#define WINDOWS_API__SND_NODEFAULT       0x0002  /* silence (!default) if sound not found */
#define WINDOWS_API__SND_MEMORY          0x0004  /* pszSound points to a memory file */
#define WINDOWS_API__SND_LOOP            0x0008  /* loop the sound until next sndPlaySound */
#define WINDOWS_API__SND_NOSTOP          0x0010  /* don't stop any currently playing sound */

#define WINDOWS_API__SND_NOWAIT      0x00002000L /* don't wait if the driver is busy */
#define WINDOWS_API__SND_ALIAS       0x00010000L /* name is a registry alias */
#define WINDOWS_API__SND_ALIAS_ID    0x00110000L /* alias is a predefined ID */
#define WINDOWS_API__SND_FILENAME    0x00020000L /* name is file name */
#define WINDOWS_API__SND_RESOURCE    0x00040004L /* name is resource name or atom */
#define WINDOWS_API__SND_PURGE           0x0040  /* purge non-static events for task */
#define WINDOWS_API__SND_APPLICATION     0x0080  /* look for application specific association */
#define WINDOWS_API__SND_SENTRY      0x00080000L /* Generate a SoundSentry event with this sound */
#define WINDOWS_API__SND_RING        0x00100000L /* Treat this as a "ring" from a communications app - don't duck me */
#define WINDOWS_API__SND_SYSTEM      0x00200000L /* Treat this as a system sound */
/*******************************************************************************
 * см.PlaySound in MMSystem.h
 ******************************************************************************/
int windows_api__play_sound(char* wav, unsigned int fuSound);
void windows_api__Sleep(int ms);
#endif
