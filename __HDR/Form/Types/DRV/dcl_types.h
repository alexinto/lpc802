/*******************************************************************************
 * @file dcl_types.h.
 * @brief Типы данных модуля управления канальной электроникой.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DCL_TYPES_H_
#define DCL_TYPES_H_

typedef enum {
    DCL__TERM_MODE_HIZ    = 0,     /*!< высокоимпедансное состояние */
    DCL__TERM_MODE_VTT    = 1,     /*!< драйвер подключен к каналу и формирует напряжение среднего уровня vtt */
    DCL__TERM_MODE_ALD    = 2,     /*!< к каналу подлючена активная нагрузка */
}dcl__term_mode_e;

typedef enum {
    DCL__V_RANGE_0    = 0,     /*!< -0,5В..3,5В */
    DCL__V_RANGE_1    = 1,     /*!< -1,0В..7,0В */
    DCL__V_RANGE_2    = 2,     /*!< -1,5В..6,5В */
}dcl__v_range_e;

typedef enum {
    DCL__MODE_HTL     = 0,    /*!< управление от ГВП */
    DCL__MODE_STATIC  = 1,    /*!< управление через SPI-интерфейс */
}dcl__mode_e;

typedef enum {
    DCL__STATE_L     = 0,    /*!< низкий */
    DCL__STATE_H     = 1,    /*!< высокий */
    DCL__STATE_T     = 2,    /*!< средний */
}dcl__state_e;








#endif
