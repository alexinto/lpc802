/*******************************************************************************
 * @file scmd_types.h.
 * @brief Типы данных модуля синхрокоманд.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef SCMD_TYPES_H_
#define SCMD_TYPES_H_

#pragma pack(push, 1)
/**
 * \brief Настройка отправки синхрокоманды на IP-адрес
 */
typedef struct{
    uint8_t     scmd_number;        /*!< Условный номер синхрокоманды */
    uint32_t    scmd_ip_address;    /*!< IP адрес получателя синхрокоманд */
    uint16_t    scmd_port;          /*!< Порт получателя синхрокоманды */
    uint32_t    scmd_script;        /*!< Номер скрипта получателя синхрокоманды */
}cmd_scmd_generate__in_out_t;


#pragma pack(pop)



#endif
