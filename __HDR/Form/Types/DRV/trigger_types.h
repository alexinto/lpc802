/*******************************************************************************
 * @file trigger_types.h.
 * @brief Типы данных модуля работы с триггерными сигналами.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TRIGGER_TYPES_H_
#define TRIGGER_TYPES_H_

#pragma pack(push, 1)
/**
 * \brief Настройки триггерного сигнала.
 */
typedef struct {
    uint8_t num;                /*!< Номер триггера */
    uint8_t script;             /*!< Номер скрипта */
    uint16_t delay;             /*!< Задержка выполнения [мкс] */
    uint8_t slope;              /*!< Настройки срабатывания */
    uint8_t mode;               /*!< Режим работы */
}cmd_trigger__io_t;

#pragma pack(pop)



#endif
