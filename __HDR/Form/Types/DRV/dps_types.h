/**
 * @file dps_types.h
 * @author Alexander Tolstunov (tolstunov@form.ru)
 * @brief Файл содержащий уникальные типы данных для измерительного модуля DPS
 * @version 0.1
 * @date 2023-02-15
 * 
 * @copyright Copyright (c) 2023
 * 
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _DPS_TYPES_H_
#define _DPS_TYPES_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

#define DPS__BUFF_SIZE 184

/**
 * @brief Перечисление режимов канала
 */
typedef enum{
    DPS_CH_MODE__NONE = 0,              /*!< Нет режима (для подрежимов) */
    DPS_CH_MODE__FORCE_NOTHING,         /*!< Канал в режиме Force Nothing */
    DPS_CH_MODE__FORCE_VOLTAGE,         /*!< Канал в режиме Force Voltage */
    DPS_CH_MODE__MEASURE_VOLTAGE,       /*!< Канал в режиме Measure Voltage */
    DPS_CH_MODE__MEASURE_CURRENT,       /*!< Канал в режиме Measure Current */
	DPS_CH_MODE__GANG_MODE				/*!< Канал в режиме объединения */
}dps_types__ch_mode_e;

/**
 * @brief Перечень скоростей нарастания напряжения канала
 */
typedef enum
{
    DPS_SLEW_RATE__1V_PER_US = 0,       /*!< Скорость нарастания: 1 V/μs */
    DPS_SLEW_RATE__0875V_PER_US,        /*!< Скорость нарастания: 0.875 V/μs */
    DPS_SLEW_RATE__075V_PER_US,         /*!< Скорость нарастания: 0.75 V/μs */
    DPS_SLEW_RATE__062V_PER_US,         /*!< Скорость нарастания: 0.62 V/μs */
    DPS_SLEW_RATE__05V_PER_US,          /*!< Скорость нарастания: 0.5 V/μs */
    DPS_SLEW_RATE__043V_PER_US,         /*!< Скорость нарастания: 0.43 V/μs */
    DPS_SLEW_RATE__035V_PER_US,         /*!< Скорость нарастания: 0.35 V/μs */
    DPS_SLEW_RATE__03125V_PER_US,       /*!< Скорость нарастания: 0.3125 V/μs*/
}dps_types__slew_rate_e;

/**
 * @brief Перечень диапозонов силы тока
 */
typedef enum
{
    DPS_CURR_RANGE__5UA = 0,            /*!< Диапозон силы тока: ±5μA */
    DPS_CURR_RANGE__25UA,               /*!< Диапозон силы тока: ±25μA */
    DPS_CURR_RANGE__250UA,              /*!< Диапозон силы тока: ±250μA */
    DPS_CURR_RANGE__2_5MA,              /*!< Диапозон силы тока: ±2.5mA */
    DPS_CURR_RANGE__25MA,               /*!< Диапозон силы тока: ±25mA */
    DPS_CURR_RANGE__100MA,              /*!< Диапозон силы тока: ±100mA */
    DPS_CURR_RANGE__500MA               /*!< Диапозон силы тока: ±500mA */
}dps_types__curr_range_e;

/**
 * @brief Режимы производительности канала
 */
typedef enum
{
    DPS_PERF_MODE__SAFE = 0,            /*!< Режим канала: режим пониженной производительности */
    DPS_PERF_MODE__COMPENSATION         /*!< Режим канала:  */
}dps_types__perf_mode_e;

/**
 * @brief Выбор номера триггерного сигнала (выбор блока контактов прижимного устройства)
 */
typedef enum
{
    DPS_TRIG_SEL_GRP_0 = 0,             /*!< Блок контактов группы каналов 16 – 31 */
    DPS_TRIG_SEL_GRP_1                  /*!< Блок контактов группы каналов 0 – 15 */
}dps_types__trig_sel_e;

#endif /* _DPS_TYPES_H_ */
