/*******************************************************************************
 * @file dev_setts_types.h.
 * @brief Типы данных модуля настроек.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DEV_SETTS_TYPES_H_
#define DEV_SETTS_TYPES_H_
#include "Types/System/fdebug_types.h"
#include "Types/System/cmd_oscillator_types.h"

/**
 * \brief Перечень настроечных параметров.
 */
typedef enum {
    /**
    * Серийный номер.
    */
    DEV_SETTS__SER_NUM    = 0,
    /**
    * Версия платы.
    */
    DEV_SETTS__HW_VER     = 1,
    /**
    * Пароль. 16 байт.
    */
    DEV_SETTS__PASSWORD   = 2,
    /**
    * Настройки FDEBUG. \ref fdebug__struct_t
    */
    DEV_SETTS__FDEBUG     = 3,
    /**
    * Настройки SYNCCLOCK. Массив из \ref SYNCCLOCK__CH_MAX структур \ref cmd_oscillator__ch_t. 0й канал= 0ая структура в массиве.
    */
    DEV_SETTS__SYNCCLOCK  = 4,
    /**
    * Максимальное число параметров.
    */
    DEV_SETTS_PARAM_MAX,
    /**
    * Принудительный размер enum в 4 байта.
    */
    DEV_SETTS_PARAM_CLOSE = 0xFFFFFFFE,
    DEV_SETTS_PARAM_NONE = 0xFFFFFFFF
}dev_setts__params_e;











#endif
