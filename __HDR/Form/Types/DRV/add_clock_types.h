/*******************************************************************************
 * @file add_clock_types.h.
 * @brief Типы данных мезонина Add Clock.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef ADD_CLOCK_TYPES_H_
#define ADD_CLOCK_TYPES_H_

/**
 * \brief Перечень настроек AddClock, хранящихся в энергонезависимой памяти.
 */
typedef enum {
    ADD_CLOCK__SPI_FREQ    = 0,      /*!<  Частота сигнала синхронизации интерфейса SPI (в Гц, предварительный допускаемый диапазон от 1000000 до 50000000) */
    ADD_CLOCK__SPI_CS      = 1,      /*!<  Выбор канала CS (из четырех, дополнительно предусмотреть автоопределение по результатам чтения какого-либо тестового регистра). */
}add_clock__settings_e;

/**
 * \brief Режимы вывода выходных сигналов модуля.
 */
typedef enum {
    ADD_CLOCK__OUT_NORMAL    = 0,      /*!<  Нормальный режим работы выхода */
    ADD_CLOCK__OUT_DIFF      = 1,      /*!<  Дифференциальный режим */
}add_clock__out_mode_e;

/**
 * \brief Способы запуска и остановки генераторов модуля.
 */
typedef enum {
    ADD_CLOCK__START_ALWAYS         = 0,    /*!<  Запуск сразу после конфигурирования всех регистров  */
    ADD_CLOCK__START_EXT            = 1,    /*!<  Запуск по перепаду внешнего сигнала (от SYNC_CLOCK) */
    ADD_CLOCK__START_STOP_TRIG      = 2,    /*!<  Запуск и остановка управляется по триггерному сигналу \ref CMD__ADDCLOCK_TRIG_CFG */
}add_clock__start_stop_mode_e;

/**
 * \brief Настройки событий триггеров.
 */
typedef enum {
    ADD_CLOCK__TRIG_RISE    = 0,    /*!<  Событие нарастающего фронта   */
    ADD_CLOCK__TRIG_FALL    = 1,    /*!<  Событие спадающего фронта */
    ADD_CLOCK__TRIG_ANY     = 2,    /*!<  Любое изменение триггерного сигнала */
}add_clock__trig_mode_e;

/**
 * \brief Перечень генераторов модуля.
 */
typedef enum {
    ADD_CLOCK__GEN_SIN      = 0,    /*!<  Генератор синусоидального сигнала */
    ADD_CLOCK__GEN_RECT     = 1,    /*!<  Генератор прямоугольного сигнала  */
    ADD_CLOCK__GEN_SYNC     = 2,    /*!<  Генератор синхроимпульсов */
}add_clock__gen_id_e;

/**
 * \brief Режимы тактирования генераторов модуля.
 */
typedef enum {
    ADD_CLOCK__CLK_INT     = 0,    /*!<  Тактирование от встроенного синтезатора частот */
    ADD_CLOCK__CLK_DDS     = 1,    /*!<  Работа в режиме DDS  */
    ADD_CLOCK__CLK_EXT     = 2,    /*!<  Тактирование от внешнего сигнала (SYNC_CLOCK) */
}add_clock__clk_mode_e;

/**
 * \brief Команды генераторам модуля.
 */
typedef enum {
    ADD_CLOCK__GEN_STOP      = 0,    /*!<  Остановка генерации выходного сигнала */
    ADD_CLOCK__GEN_START     = 1,    /*!<  Запуск генерации выходного сигнала  */
}add_clock__gen_cmd_e;


#endif
