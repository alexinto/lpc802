/***************************************************************************//**
 * @file pg_types.h.
 * @brief  Типы данных ГТП.
 * @author a.tushentsov.
 ******************************************************************************/

#ifndef PG_TYPES_H_
#define PG_TYPES_H_

typedef enum {
    PG__DRV_MODE_OFF   = 0,     /*!< Статическая блокировка выходов каналов */
    PG__DRV_MODE_R0    = 1,     /*!< Перепад с низкого уровня на высокий (в момент Т1) и обратно (в момент Т2) при задании в данном векторе 1. Низкий уровень при задании в векторе 0. */
    PG__DRV_MODE_R1    = 2,     /*!< Перепад с высокого уровня на низкий (в момент Т1) и обратно (в момент Т2) при задании в данном векторе 0. Высокий уровень при задании в векторе 1. */
    PG__DRV_MODE_NRZ   = 3,     /*!< Потенциальный активный драйвер (устанавливается заданный входной активный потенциал; потенциал H, по метке T1, потенциал L по метке Т2) */
    PG__DRV_MODE_DNRZ  = 4,     /*!< Драйвер с блокировкой (в заданное время T1 устанавливается заданный активный потенциал, если задан низкий или высокий уровень, а в момент Т2 – состояние Z, если задана блокировка канала). */
}pg__drv_mode_e;

typedef enum {
    PG__CMP_MODE_MASK   = 0,   /*!< Контроль состояния входных сигналов не производится, результат контроля в канале всегда «Годен» */
    PG__CMP_MODE_DETECT = 1,   /*!< Производится анализ логического состояния сигнала в заданный момент времени (обе метки Т3 и Т4 в этом режиме должны быть поставлены в одно и то же заданное положение) */
}pg__cmp_mode_e;

typedef enum {
    PG__PATTERN_TYPE_STD   = 0,   /*!< стандартный */
    PG__PATTERN_TYPE_CH_16 = 1,   /*!< 16 каналов */
    PG__PATTERN_TYPE_CH_8  = 2,   /*!< 8 канала */
    PG__PATTERN_TYPE_CH_4  = 3,   /*!< 4 канала */
    PG__PATTERN_TYPE_CH_2  = 4,   /*!< 2 канала */
    PG__PATTERN_TYPE_CH_1  = 5,   /*!< 1 канал */
}pg__pattern_type_e;

typedef enum {
    PG__ERR_RAM_MODE_ALL        = 0,   /*!< Все такты ФК */
    PG__ERR_RAM_MODE_ERR_ONLY   = 1,   /*!< Только такты с ошибками */
}pg__err_ram_mode_e;







typedef enum {
    PG_STATE_OFF      = 0,
    PG_STATE_PRERUN   = 1,
    PG_STATE_RUN      = 2,
    PG_STATE_PAUSE    = 3,
    PG_STATE_BREAK    = 4,
}pg__fc_ctrl_e;





typedef struct {
    uint32_t run_state :1;              /*!< 1 - при последнем выполнении ФК было достигнуто состояние RUN */
    uint32_t start_source :1;           /*!< Источник команды старт */
    uint32_t pause_state :1;            /*!< 1 - при последнем выполнении ФК было достигнуто состояние PAUSE */
    uint32_t break_state :1;            /*!< 1 – выполнение ФК было завершено (состояние BREAK) */
    uint32_t stop_source :2;            /*!< Источник команды стоп */
    uint32_t gtp_err_gen :1;            /*!< 1 – в ходе выполнения ФК эмулятор ГТП генерировал результаты контроля, содержащие ошибки. */
    uint32_t site_err_check :1;         /*!< 1 - в ходе выполнения ФК производился проверка корректности трансляции и\или объединения результатов контроля с ошибками */
    uint32_t site_err_check_fail :1;    /*!< Результат проверки корректности трансляции и\или объединения результатов контроля с ошибками */
}pg__fc_status_t;

typedef struct{
    uint32_t gen_prbs0_seed : 8;
    uint32_t chk_prbs0_seed : 8;
    uint32_t chk_prbs1_seed : 8;
    uint32_t chk_prbs2_seed : 8;
    uint32_t chk_prbs3_seed : 8;
    uint32_t chk_prbs4_seed : 8;
    uint32_t chk_prbs5_seed : 8;
    uint32_t chk_prbs6_seed : 8;
}pg__error_prbs_seed_t;

typedef struct {
    uint32_t gen_prbs0_en : 1;   /*!< Состояние генератора PRBS, формирующего результаты ФК */
    uint32_t chk_prbs0_en : 1;   /*!< Состояние генераторов PRBS, используемых для проверки корректности трансляции и объединения результатов ФК */
    uint32_t chk_prbs1_en : 1;   /*!< --||--||-- */
    uint32_t chk_prbs2_en : 1;   /*!< --||--||-- */
    uint32_t chk_prbs3_en : 1;   /*!< --||--||-- */
    uint32_t chk_prbs4_en : 1;   /*!< --||--||-- */
    uint32_t chk_prbs5_en : 1;   /*!< --||--||-- */
    uint32_t chk_prbs6_en : 1;   /*!< --||--||-- */
}pg__error_prbs_ctrl_t;    /*!< Регистр управления генераторами PRBS */;


typedef struct{
    //PG_SOFT_RESET
    uint32_t soft_reset : 1;                     /*!< Полный сброс PG */
    uint32_t reserved_rst : 31;                    /*!< Пустые поля регистра PG_SOFT_RESET */
    //PG_INFO
    uint32_t magic_const : 16;                       /*!< 0xF0A5 */
    uint32_t pg_version : 16;                        /*!< Версия конфигурационного файла ПЛИС PG */
    //PG_TEST
    uint32_t pg_test_[2];                       /*!< Произвольные проверочные данные */
    //PG_TP_CTRL
    uint32_t test_pin_0 : 4;                     /*!< Тестовый вывод 0 */
    uint32_t test_pin_1 : 4;                     /*!< Тестовый вывод 1 */
    uint32_t test_pin_2 : 4;                     /*!< Тестовый вывод 2 */
    uint32_t reserved_tp : 20;                       /*!< Пустые поля регистра */
    //PG_TRIG_CTRL
    uint32_t pg_trig_ctrl;                      /*!< Регистр управления шиной BUS_SYNC_TRIG */
    //PG_TRIG_STATE
    uint32_t pg_trig_state;                     /*!< Регистр состояния шины BUS_SYNC_TRIG */
    //PG_EMUL_RST
    uint32_t pg_emul_rst : 1;                    /*!< Регистр программного сброса */
    uint32_t reserved_emul_rst :31;               /*!< Пустые поля регистра PG_EMUL_RST */
    //PG_FC_LENGTH
    uint32_t pg_fc_length;                      /*!< Максимальное кол-во векторов тестовой последовательности */
    //PG_FC_FREQ_SEL
    uint32_t clkd_divider : 4;                   /*!< Делитель частоты ОЧФК */
    uint32_t reserved_fc_freq_sel : 28;            /*!< Пустые поля регистра PG_FC_FREQ_SEL */
    //PG_FC_SEQ_CTRL
    uint32_t tmbs_ready_time : 9;               /*!< Время (в тактах ОЧФК) появления сигнала tmbs_ready в состоянии PRERUN (имитирует сигнал готовности буферов реального ГТП). */
    uint32_t pause_en : 1;
    uint32_t pause_vector_num : 22;             /*!< Номер вектора ФК, после исполнения которого, эмулятор ГТП перейдет в состояние PAUSE. */
    //PG_FC_FLAG_CTRL
    uint32_t flag : 1;                           /*!< Разрешения продолжения ФК в состоянии PRERUN после ветвления (состояние PAUSE) */
    uint32_t reserved_flag : 31;                   /*!< Пустые поля регистра PG_FC_FLAG_CTRL */
    //PG_ERROR_PRBS_CTRL
    union{
        pg__error_prbs_ctrl_t pg_error_prbs_ctrl;    /*!< Регистр управления генераторами PRBS */;
        uint32_t reserved_err_prbs_ctrl;
    };
    //PG_ERROR_PRBS_SEED
    union{
        pg__error_prbs_seed_t pg_error_prbs_seed;
        uint32_t reserved_prbs_seed[2];
    };

    //PG_FC_STATUS
    //pg_fc_status_t pg_fc_status;                /*!< Регистр состояния последнего выполнения ФК */
    union {
        pg__fc_status_t pg_fc_status;
        uint32_t resrved_fc_status;              // 0x3C
    };
    uint32_t reserved_empty[112];                // 0x40
    uint32_t fc_ctrl;                            // 0x200 pg__fc_ctrl_e
}pg__struct_t;

#endif
