/*******************************************************************************
 * @file pwr_types.h.
 * @brief Типы данных модуля управления питанием.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef PWR_TYPES_H_
#define PWR_TYPES_H_

typedef enum {
    PWR__STATE_OFF       = 0, /*!< Модуль выключен. */
    PWR__STATE_ON        = 1, /*!< Модуль включен. */
    PWR__STATE_POWERDOWN = 2, /*!< Модуль в режиме энергосбережения. */
}pwr__state_e;

typedef enum {
    PWR__ERR_STATE_NO_ERROR  = 0, /*!< Модуль работает штатно. Ошибок нет. */
    PWR__ERR_STATE_NA        = 1, /*!< Неизвестная ошибка питания. */
}pwr__err_state_e;

typedef enum {
    PWR__CMD_OFF       = 0, /*!< Выключить модуль. */
    PWR__CMD_ON        = 1, /*!< Включить модуль. */
    PWR__CMD_POWERDOWN = 2, /*!< Перевести модуль в режим энергосбережения. */
}pwr__cmd_e;












#endif
