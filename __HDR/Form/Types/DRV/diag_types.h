/*******************************************************************************
 * @file diag_types.h.
 * @brief Типы данных модуля диагностики.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DIAG_TYPES_H_
#define DIAG_TYPES_H_

typedef enum {
    DIAG__STATE_IDLE     = 0,        /*!< Тест не проводился. */
    DIAG__STATE_OK       = 1,        /*!< Тест пройден успешно. */
    DIAG__STATE_ERR      = 2,        /*!< Тест не пройден. */
    DIAG__STATE_PROGRESS = 3,        /*!< В данный момент проводится тест. */
}diag__state_e;

typedef enum {
    DIAG__DCL_TEST_ALL     = 0,        /*!< Будет какой-то тест. Что-то будет делать. */
}diag__dcl_test_e;

typedef enum {
    DIAG__PG_TEST_ALL      = 0,        /*!< Будет какой-то тест. Что-то будет делать. */
}diag__pg_test_e;

typedef enum {
    DIAG__STATIC_TEST_ALL  = 0,        /*!< Будет какой-то тест. Что-то будет делать. */
}diag__static_test_e;

typedef enum {
    DIAG__DINAMIC_TEST_ALL = 0,        /*!< Будет какой-то тест. Что-то будет делать. */
}diag__dinamic_test_e;

typedef enum {
    DIAG__PMU_TEST_ALL     = 0,        /*!< Будет какой-то тест. Что-то будет делать. */
}diag__pmu_test_e;

typedef enum {
    DIAG__MEAS_TEST_ALL    = 0,        /*!< Будет какой-то тест. Что-то будет делать. */
}diag__meas_test_e;










#endif
