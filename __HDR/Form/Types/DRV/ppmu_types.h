/*******************************************************************************
 * @file ppmu_types.h.
 * @brief Типы данных модуля управления PPMU.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef PPMU_TYPES_H_
#define PPMU_TYPES_H_

typedef enum {
    PPMU__STATE_DISCONNECTED  = 0,     /*!< высокоимпедансное состояние */
    PPMU__STATE_CONNECTED     = 1,     /*!< PPMU подключен к каналу */
}ppmu__state_e;

typedef enum {
    PPMU__I_RNG_2uA   = 0,     /*!< 2мкА */
    PPMU__I_RNG_8uA   = 1,     /*!< 8мкА */
    PPMU__I_RNG_32uA  = 2,     /*!< 32мкА */
    PPMU__I_RNG_128uA = 3,     /*!< 128мкА */
    PPMU__I_RNG_512uA = 4,     /*!< 512мкА */
    PPMU__I_RNG_2mA   = 5,     /*!< 2мА */
    PPMU__I_RNG_8mA   = 6,     /*!< 8мА */
    PPMU__I_RNG_32mA  = 7,     /*!< 32мА */
}ppmu__i_rng_e;

typedef enum {
    PPMU__R_LOAD_31   = 0,     /*!< 31 Ом */
    PPMU__R_LOAD_125  = 1,     /*!< 125 Ом */
    PPMU__R_LOAD_500  = 2,     /*!< 500 Ом */
    PPMU__R_LOAD_2K   = 3,     /*!< 2 кОм */
    PPMU__R_LOAD_8K   = 4,     /*!< 8 кОм */
    PPMU__R_LOAD_31K  = 5,     /*!< 31 кОм */
    PPMU__R_LOAD_125K = 6,     /*!< 125 кОм */
    PPMU__R_LOAD_500K = 7,     /*!< 500 кОм */
}ppmu__r_load_e;

typedef enum {
    PPMU__MEAS_U_3V   = 0,     /*!< (0,1В..2,9В) */
    PPMU__MEAS_U_14V  = 1,     /*!< (-1,5В..6,5В) */
}ppmu__meas_u_rng_e;





#endif
