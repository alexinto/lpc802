/***************************************************************************//**
 * @file cmd_oscillator_types.h.
 * @brief Модуль с описанием типов SyncClock.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_OSCILLATOR_TYPES_H_
#define CMD_OSCILLATOR_TYPES_H_

#include <stdint.h>

#define SYNCCLOCK__CH_MAX      14                         // Количество каналов модуля SyncCLock

#ifdef __cplusplus
    #pragma pack(push, 1)
#endif

/**
 * \brief Структура калибровочного параметра.
 */
typedef struct {
    /**
    * Частота, Гц.
    */
    uint32_t freq;
    /**
    * Банк конденсаторов 1 генератора (порядковый номер).
    */
    int8_t oscil1_bank;
    /**
    * Цифровая задержка 1 генератора, шагов.
    */
    int8_t oscil1_d_delay;
    /**
    * Аналоговая задержка 1 генератора, шагов.
    */
    int8_t oscil1_a_delay;
    /**
    * Банк конденсаторов 2 генератора (порядковый номер).
    */
    int8_t oscil2_bank;
    /**
    * Цифровая задержка 2 генератора, шагов.
    */
    int8_t oscil2_d_delay;
    /**
    * Аналоговая задержка 2 генератора, шагов.
    */
    int8_t oscil2_a_delay;
    /**
    * Зарезервировано.
    */
    uint8_t reserved[6];
}cmd_oscillator__pcal_t;

/**
 * \brief Структура настроек генератора
 */
typedef struct {
    /**
    * Режим работы. Определяет выбор опорной частоты.
    */
    uint8_t mode;
    /**
    * Настройка способа расчета выходной частоты каналов. \ref cmd_oscillator__setup_e
    */
    uint8_t setup;
} cmd_oscillator__t;

/**
 * \brief Структура настроек канала генератора
 */
typedef enum {
    /**
    * Зарезервировано.
    */
    OSCIL_SETUP__NA     = 0,
    /**
    * Точность выходной частоты определяется настройками канала OSCIL_CH_ID__FDOM1.
    */
    OSCIL_SETUP__FDOM1  = 1,
    /**
    * Точность выходной частоты определяется настройками канала OSCIL_CH_ID__FDOM2.
    */
    OSCIL_SETUP__FDOM2  = 2,
    /**
    * Выходная частота считается на основе настроек каналов OSCIL_CH_ID__FDOM1 и OSCIL_CH_ID__FDOM2.
    */
    OSCIL_SETUP__BOTH   = 3,
}cmd_oscillator__setup_e;

/**
 * \brief Структура настроек канала генератора
 */
typedef struct {
    /**
    * Идентификатор канала \ref cmd_oscillator__ch_id_e
    */
    uint16_t ch;
    /**
    * Режим работы канала \ref cmd_oscillator__ch_mode_e
    */
    uint16_t state;
    /**
    * Частота канала, Гц.
    */
    uint32_t freq;
} cmd_oscillator__ch_t;

/**
 * \brief Идентификаторы каналов генератора.
 */
typedef enum {
    OSCIL_CH_ID__SYNC     = 1,
    OSCIL_CH_ID__FDOM1    = 2,
    OSCIL_CH_ID__FDOM2    = 3,
    OSCIL_CH_ID__RUSER1   = 4,
    OSCIL_CH_ID__RUSER2   = 5,
    OSCIL_CH_ID__RUSER3   = 6,
    OSCIL_CH_ID__RUSER4   = 7,
}cmd_oscillator__ch_id_e;

// Режимы каналов генератора
typedef enum {
    OSCIL_CH_MODE__ON    = 1, // включено
    OSCIL_CH_MODE__OFF   = 2, // выключено
    OSCIL_CH_MODE__ERR   = 3, // ошибка
}cmd_oscillator__ch_mode_e;

#if (defined __cplusplus) && (!defined(__ICCARM__))
    #pragma pack(pop)
#endif

#endif
