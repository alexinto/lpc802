/***************************************************************************//**
 * @file bootloader_types.h.
 * @brief Модуль с описанием типов загрузчика прошивок.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef BOOTLOADER_TYPES_H_
#define BOOTLOADER_TYPES_H_
#include "Types/RXI_Ethernet/RxiEthernet_types.h"

typedef enum {
    BOOTLOADER_CMD_NONE     = 0,
    BOOTLOADER_UPD_APL      = 1,
    BOOTLOADER_UPD_RESERVE  = 2,
    BOOTLOADER_CMD_RESET    = 3,
}bootloader__cmd_e;

#define DATA_SW_SIZE (((uint16_t)RXIETHERNET_CMD_SIZE - 4) / 8 * 8) /*!< Количество байт данных в команде обновления прошивки \ref DATA_SW. */

/**
 *  Перечень команд сабмодулю при обновлении прошивки. \ref CMD__FW_UPD_CTRL.
 */
typedef enum {
    BOOT__FW_CMD_START  = 0, /*!< Начало обновления прошивки (стирает флеш) */
    BOOT__FW_CMD_END    = 1, /*!< Окончание процесса обновления (перезагружает сабмодуль) */
}bootloader__fw_cmd_e;

#endif
