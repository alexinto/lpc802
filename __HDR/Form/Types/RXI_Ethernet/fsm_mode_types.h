/***************************************************************************//**
 * @file fsm_mode_types.h.
 * @brief Модуль с описанием типов fsm_mode.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef FSM_MODE_TYPES_H_
#define FSM_MODE_TYPES_H_

#ifndef FSM_MODE_NUM
    #define FSM_MODE_NUM 1
#endif

typedef struct {
    uint16_t password :1;         // 1 - Защищена паролем, 0- нет
    uint16_t block    :1;         // 1 - Заблокирована, 0- доступна
    uint16_t script   :1;         // 1 - выполняется в режиме записи скриптов
} fsm_mode__settings_t;

typedef enum {
    FSM_MODE__STATE_WORK    = 0,  // Нормальный режим работы
    FSM_MODE__STATE_TEST    = 1,  // Режим записи настроек
    FSM_MODE__STATE_SCRIPT  = 2,  // Режим записи скриптов
}fsm_mode__state_e;

#endif
