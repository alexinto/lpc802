﻿/***************************************************************************//**
 * @file cmd_tbl_types.h.
 * @brief Модуль с описанием типов cmd_tbl.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef __CMD_TBL_TYPES_H_
#define __CMD_TBL_TYPES_H_

/**
 * Размер параметров команды определяется полем "длина параметров" RXI_Ethernet протокола.
 */
#define CMD_SIZE_NA 0xFFFF

typedef enum {
    FLASH_JOURNAL    = 1,
    FLASH_PARAMS     = 2,
    FLASH_SETTINGS   = 3,
    FLASH_FORMAT_ALL = 255,  // "DEAD" следующие 4 байта
}cmd_tbl__flash_e;

#endif
