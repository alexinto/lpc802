#ifndef PSPCOMMANDS_H_
#define PSPCOMMANDS_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_PSP_PWR_ON PSP_PWR_ON
 * \ingroup CMD_Commands PSPCommands
 * \brief Команда включения/выключения ПСП.
 */
/**@{*/
#define CMD__PSP_PWR_ON                     0x0200
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t command;        /*!< Команда включения: 1 - вкл; 0 - выкл */
}cmd_psp_pwr_on__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_psp_pwr_on__out_t;
/**@}*/

/**
 * \defgroup GRP_PSP_STATE PSP_STATE
 * \ingroup CMD_Commands PSPCommands
 * \brief Команда запроса состояния ПСП.
 */
/**@{*/
#define CMD__PSP_STATE                     0x0201
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_psp_state__in_t;
/**
 * Возвращаемые значения команды.
*/
typedef struct {
    uint8_t state;      /*!< Состояние ПСП */
}cmd_psp_state__out_t;
/**@}*/


#pragma pack(pop)

#endif





