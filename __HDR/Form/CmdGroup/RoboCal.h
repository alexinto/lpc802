#ifndef ROBOCAL_H_
#define ROBOCAL_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_ROBOCAL_MOVEXY ROBOCAL_MOVEXY
 * \ingroup IO_Commands RoboCommands
 * \brief Перемещение каретки по заданным абсолютным координатам X и Y.
 */
/**@{*/
#define CMD__ROBOCAL_MOVEXY               0x0900
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t id;        /*!< Идентификатор плоскости. */
    uint32_t x;         /*!< Координата X. */
    uint32_t y;         /*!< Координата Y. */
}cmd_robocal_movexy__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_robocal_movexy__out_t;
/**@}*/









#pragma pack(pop)

#endif
