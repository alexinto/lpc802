#ifndef SYSTEMCOMMANDS_H_
#define SYSTEMCOMMANDS_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_IDN_REQ IDN_REQ
 * \ingroup IO_Commands SystemCommands
 * \brief Запрос идентификатора устройства.
 * \image html IDN_REQ.png Алгоритм работы команды IDN_REQ
 */
/**@{*/
#define CMD__IDN_REQ                0x0013
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_idn_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    struct {
            uint8_t major;
            uint8_t minor;
            uint16_t micro;
    } sw_version;           /*!< Версия ПО (3 байта). */
    char idn[84];          /*!< Дескриптор устройства (string) максимум = 84 байт. (Ex: \ref IDN_PG) */
    uint8_t hash[40];
}cmd_idn_req__out_t;
/**@}*/

/**
 * \defgroup GRP_TASK_TIME_REQ TASK_TIME_REQ
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Позволяет замерять время выполнения последовательности команд.
 * Возвращает время, прошедшее с момента выполнения первой команды в пакете содержащем TASK_TIME_REQ до момента начала обработки команды TASK_TIME_REQ.
 * \image html TASK_TIME_REQ.png Алгоритм работы команды TASK_TIME_REQ
 */
/**@{*/
#define CMD__TASK_TIME_REQ          0x0014
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_task_time__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t time_us;   /*!< Время в микросекундах. */
}cmd_task_time__out_t;
/**@}*/

/**
 * \defgroup GRP_PASS_EN PASS_EN
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Вкл/выкл выполнения команд защищенных паролем.
 * \image html PASS_EN.png Алгоритм работы команды PASS_EN
 */
/**@{*/
#define CMD__PASS_EN                0x0037
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t password[16];   /*!< Текущий пароль (16 символов). */
    uint8_t pass_en;        /*!< 1- включить, 0- выключить. */
}cmd_pass_en__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_pass_en__out_t;
/**@}*/

/**
 * \defgroup GRP_PASS_EN_REQ PASS_EN_REQ
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Запрос состояния защиты.
 * \image html PASS_EN_REQ.png Алгоритм работы команды PASS_EN_REQ
 */
/**@{*/
#define CMD__PASS_EN_REQ            0x0038
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_pass_en_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pass_en;    /*!< 1- защита включена, 0- выключена. */
}cmd_pass_en_req__out_t;
/**@}*/

/**
 * \defgroup GRP_REBOOT REBOOT
 * \ingroup IO_Commands SystemCommands
 * \brief Hw-сброс системы. Полная перезагрузка с потерей всех данных.
 */
/**@{*/
#define CMD__REBOOT                    0x000F
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_reboot__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_reboot__out_t;
/**@}*/

/**
 * \defgroup GRP_RST RST
 * \ingroup IO_Commands SystemCommands
 * \brief SW-сброс системы. "Мягкая" перезагрузка с инициализацией модулей.
 * \image html RST.png Алгоритм работы команды RST
 */
/**@{*/
#define CMD__RST                    0x0010
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_rst__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_rst__out_t;
/**@}*/

/**
 * \defgroup GRP_OPC OPC
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Сбрасывает все очереди команд (буферы).
 * \image html OPC.png Алгоритм работы команды OPC
 */
/**@{*/
#define CMD__OPC                    0x0011
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_opc__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_opc__out_t;
/**@}*/

/**
 * \defgroup GRP_OPC_REQ OPC_REQ
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Запрос текущего количества команд в очереди на выполнение.
 */
/**@{*/
#define CMD__OPC_REQ                0x0012
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_opc_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t n_cmds;    /*!< Количество команд в очереди. */
}cmd_opc_req__out_t;
/**@}*/

/**
 * \defgroup GRP_BUF_SIZE_REQ BUF_SIZE_REQ
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Запрос размера буфера устройства.
 * \image html BUFF_SIZE_REQ.png Алгоритм работы команды BUFF_SIZE_REQ
 */
/**@{*/
#define CMD__BUF_SIZE_REQ           0x0015
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_buf_size__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t n_buf;      /*!< Количество буферов. */
    uint32_t buf_size;  /*!< Размер одного буфера. */
}cmd_buf_size__out_t;
/**@}*/

/**
 * \defgroup GRP_PCIE_BDF PCIE_BDF
 * \ingroup IO_Commands SystemCommands
 * \brief Запрос адреса BDF (Bus Device Function). Поддерживается всеми устройствами в крейте.
 *        Нумерация устройств от 0. Если устройства не существует, то вернет ошибку \ref CMD_ERR__PARAM_NOT_FOUND.
 */
/**@{*/
#define CMD__PCIE_BDF               0x0016
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pcie_id;    /*!< Порядковый номер устройства. Возвращается командой \ref CMD__PCIE_NUM. */
}cmd_bdf_addr__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pcie_id;    /*!< Порядковый номер устройства. */
    uint8_t bdf_id;     /*!< Позиционный номер устройства в модуле. */
    uint16_t bdf_addr;  /*!< Значение PCIe BDF */
} cmd_bdf_addr__out_t;
/**@}*/

/**
 * \defgroup GRP_PCIE_NUM PCIE_NUM
 * \ingroup IO_Commands SystemCommands
 * \brief Запрос количества pcie устройств. Поддерживается всеми устройствами в крейте.
 *        Нумерация устройств от 0.
 */
/**@{*/
#define CMD__PCIE_NUM               0x0017
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_pcie_num__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t pcie_num;  /*!< Количество pcie устройств. */
} cmd_pcie_num__out_t;
/**@}*/

/**
 * \defgroup GRP_UTC UTC
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Установка даты-времени в формате UTC.
 * \image html UTC.png Алгоритм работы команды UTC
 */
/**@{*/
#define CMD__UTC                    0x0032
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t timestamp; /*!< UNIX time. */
}cmd_utc__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_utc__out_t;
/**@}*/

/**
 * \defgroup GRP_UTC_REQ UTC_REQ
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Чтение даты-времени в формате UNIX time.
 * \image html UTC_REQ.png Алгоритм работы команды UTC_REQ
 */
/**@{*/
#define CMD__UTC_REQ                0x0033
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_utc_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t timestamp; /*!< UNIX time. */
}cmd_utc_req__out_t;
/**@}*/

/**
 * \defgroup GRP_SOCKET SOCKET
 * \ingroup IO_Commands SystemCommands
 * \brief Установка параметров сокета.
 * \image html SOCKET.png Алгоритм работы команды SOCKET
 */
/**@{*/
#define CMD__SOCKET                 0x0034
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t sock_id;    /*!< Номер сокета. */
    uint32_t ip_addr;   /*!< IP адрес. */
    uint16_t port;      /*!< Порт. */
}cmd_socket__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_socket__out_t;
/**@}*/

/**
 * \defgroup GRP_SOCKET_REQ SOCKET_REQ
 * \ingroup IO_Commands SystemCommands
 * \brief Чтение параметров сокета.
 * \image html SOCKET_REQ.png Алгоритм работы команды SOCKET_REQ
 */
/**@{*/
#define CMD__SOCKET_REQ             0x0035
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t sock_id;    /*!< Номер сокета. */
}cmd_socket_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t sock_id;    /*!< Номер сокета. */
    uint32_t ip_addr;   /*!< IP адрес. */
    uint16_t port;      /*!< Порт. */
}cmd_socket_req__out_t;
/**@}*/

/**
 * \defgroup GRP_PASSWORD PASSWORD
 * \ingroup IO_Commands SystemCommands
 * \brief Установка пароля.
 * \image html PASSWORD.png Алгоритм работы команды PASSWORD
 */
/**@{*/
#define CMD__PASSWORD               0x0036
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t old_pass[16];   /*!< Текущий пароль. */
    uint8_t new_pass[16];   /*!< Новый пароль. */
}cmd_password__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_password__out_t;
/**@}*/

/**
 * \defgroup GRP_SETTINGS_WR SETTINGS_WR
 * \ingroup IO_Commands SystemCommands
 * \brief Запись параметра в энергонезависимую память.
 * \image html SETTINGS_WR.png Алгоритм работы команды SETTINGS_WR
 */
/**@{*/
#define CMD__SETTINGS_WR               0x0082
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Номер параметра в таблице. \ref dev_setts__params_e */
    uint16_t data_len;  /*!< Размер параметра, байт. */
    uint8_t data[128];  /*!< Значение параметра - 128 байт. */
}cmd_settings_wr__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_settings_wr__out_t;
/**@}*/

/**
 * \defgroup GRP_FLASH_CLR FLASH_CLR
 * \ingroup IO_Commands SystemCommands
 * \brief Функция очистки энергонезависимой памяти. Вызывается 1 раз при производстве изделия.
 * \image html FLASH_CLR.png Алгоритм работы команды FLASH_CLR
 */
/**@{*/
#define CMD__FLASH_CLR               0x0083
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t data[4];    /*!< Должна быть последовательность "DEAD" (0x44, 0x45, 0x41, 0x44). */
}cmd_flash_clr__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_flash_clr__out_t;
/**@}*/

/**
 * \defgroup GRP_SETTINGS_RD SETTINGS_RD
 * \ingroup IO_Commands SystemCommands
 * \brief Чтение настроечных параметров. Если параметра нет, то возвращает \ref CMD_ERR__PARAM_NOT_FOUND.
 * \image html SETTINGS_RD.png Алгоритм работы команды SETTINGS_RD
 */
/**@{*/
#define CMD__SETTINGS_RD               0x0086
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Номер параметра. \ref dev_setts__params_e */
}cmd_settings_rd__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Идентификатор параметра. */
    uint16_t data_len;  /*!< Размер параметра, байт. */
    uint8_t data[128];  /*!< Значение параметра - 128 байт. */
} cmd_settings_rd__out_t;
/**@}*/

/**
 * \defgroup GRP_SET_DEFAULT SET_DEFAULT
 * \ingroup IO_Commands SystemCommands
 * \brief Сбрасывает настройки. Для использования необходимо авторизоваться.
 */
/**@{*/
#define CMD__SET_DEFAULT              0x0091
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_set_default__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
}cmd_set_default__out_t;
/**@}*/

/**
 * \defgroup GRP_OP_MODE_SET OP_MODE_SET
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Установка режима работы нормальный, тестовый и т.д. ( \ref fsm_mode__state_e )
 * При передаче неверного режима работы авторизация прекращается и ее
 * необходимо снова получать при помощи пароля.
 * \image html OP_MODE_SET.png Алгоритм работы команды OP_MODE_SET
 */
/**@{*/
#define CMD__OP_MODE_SET              0x0130
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t mode;          /*!< Режим работы. \ref fsm_mode__state_e */
    uint8_t password[16];   /*!< Текущий пароль. Не проверяется, в случае авторизированного доступа (пароль введен ранее). */
}cmd_op_mode_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_op_mode_set__out_t;
/**@}*/

/**
 * \defgroup GRP_OP_MODE_GET OP_MODE_GET
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Возвращает текущий режим работы изделия и статус авторизации.
 * \image html OP_MODE_GET.png Алгоритм работы команды OP_MODE_GET
 */
/**@{*/
#define CMD__OP_MODE_GET               0x0131
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_op_mode_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
     uint16_t mode;         /*!< Режим работы. \ref fsm_mode__state_e  */
     uint8_t  autorization; /*!< Состояние авторизации. 0- не авторизированный доступ, 1- авторизация. */
}cmd_op_mode_get__out_t;
/**@}*/

/**
 * \defgroup GRP_CFG_CMD_SET CFG_CMD_SET
 * \ingroup IO_Commands SystemCommands
 * \brief Устанавливает настроечные параметры команды (защиту паролем, блокировку).
 * Сохранение настроек во флеш происходит автоматически при смене режима работы командой OP_MODE_SET.
 * \image html CFG_CMD_SET.png Алгоритм работы команды CFG_CMD_SET
 */
/**@{*/
#define CMD__CFG_CMD_SET               0x0132
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t mode;      /*!< Режим работы. \ref fsm_mode__state_e. */
    uint16_t cmd_id;    /*!< Идентификатор команды. Ex: CMD__CFG_CMD_SET */
    uint16_t settings;  /*!< Настройки команды. \ref fsm_mode__settings_t. */
}cmd_cfg_cmd_set__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_cfg_cmd_set__out_t;
/**@}*/

/**
 * \defgroup GRP_CFG_CMD_GET CFG_CMD_GET
 * \ingroup CMD_Commands IO_Commands SystemCommands
 * \brief Возвращает настроечные параметры команды (защиту паролем, блокировку) для текущего режима работы.
 * \image html CFG_CMD_GET.png Алгоритм работы команды CFG_CMD_GET
 */
/**@{*/
#define CMD__CFG_CMD_GET               0x0133
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t cmd_id;    /*!< Идентификатор команды. */
}cmd_cfg_cmd_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t cmd_id;    /*!< Идентификатор команды. */
    uint16_t settings;  /*!< Настройки команды.  \ref fsm_mode__settings_t. */
}cmd_cfg_cmd_get__out_t;
/**@}*/




#pragma pack(pop)

#endif
