/***************************************************************************//**
 * @file FirmwareCommands.h.
 * @brief Модуль функций работы с прошивками.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef FIRMWARECOMMANDS_H_
#define FIRMWARECOMMANDS_H_

#include "Types/System/bootloader_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_DATA_SW DATA_SW
 * \ingroup IO_Commands FirmwareCommands
 * \brief Команда для передачи данных при прошивке. При chunk_index == 0 производится очистка памяти
 *        и запись начинается сначала. После записи прошивки необходимо перезагрузить систему \ref CMD__REBOOT.
 * \image html DATA_SW.png Алгоритм работы команды DATA_SW
 */
/**@{*/
#define CMD__DATA_SW                0x0018
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t chunk_index;         /*!< Порядковый номер блока данных. */
    uint8_t data[DATA_SW_SIZE];   /*!< Бинарные данные. \ref DATA_SW_SIZE. */
} cmd_sw_data__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
} cmd_sw_data__out_t;
/**@}*/

/**
 * \defgroup GRP_FW_NUM FW_NUM
 * \ingroup IO_Commands FirmwareCommands
 * \brief Команда возвращает количество дополнительных прошивок модуля. 0- модуль не имеет прошивок.
 */
/**@{*/
#define CMD__FW_NUM                0x0019
/**
 * Не имеет входных параметров.
 */
typedef struct {
} cmd_fw_num__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t fw_num;    /*!< Количество прошивок. */
} cmd_fw_num__out_t;
/**@}*/

/**
 * \defgroup GRP_FW_VER FW_VER
 * \ingroup IO_Commands FirmwareCommands
 * \brief Команда возвращает идентификатор прошивки. 0- идентификатор основной прошивки.
 */
/**@{*/
#define CMD__FW_VER                0x001A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t fw_id;         /*!< Порядковый номер прошивки (с нуля). Получен командой \ref FW_NUM */
} cmd_fw_ver__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    struct {
            uint8_t major;
            uint8_t minor;
            uint16_t micro;
    } sw_version;           /*!< Версия ПО (3 байта). */
    char idn[84];          /*!< Дескриптор устройства (string) максимум = 124 байт. (Ex: \ref IDN_PG) */
    uint8_t hash[40];
} cmd_fw_ver__out_t;
/**@}*/

/**
 * \defgroup GRP_FW_UPD_CTRL FW_UPD_CTRL
 * \ingroup IO_Commands FirmwareCommands
 * \brief Команда перевода модуля в режим обновления прошивки. После загрузки прошивки в модуль при помощи команды \ref FW_UPD_DATA
 *        необходимо завершить процесс обновления fw_cmd = \ref BOOT__FW_CMD_END.
 */
/**@{*/
#define CMD__FW_UPD_CTRL                0x001B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t fw_id;         /*!< Порядковый номер сабмодуля (с нуля). Получен командой \ref FW_NUM */
    uint16_t fw_cmd;        /*!< Команда сабмодулю. \ref bootloader__fw_cmd_e */
} cmd_fw_upd_ctrl__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
} cmd_fw_upd_ctrl__out_t;
/**@}*/

/**
 * \defgroup GRP_FW_UPD_DATA FW_UPD_DATA
 * \ingroup IO_Commands FirmwareCommands
 * \brief Команда загрузки образа прошивки в сабмодуль. Перед выполнением необходимо исплользовать команду \ref FW_UPD_CTRL
 *        с параметром fw_cmd = \ref BOOT__FW_CMD_START.
 */
/**@{*/
#define CMD__FW_UPD_DATA                0x001C
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t fw_id;             /*!< Порядковый номер сабмодуля (с нуля). Получен командой \ref FW_NUM */
    uint16_t data_len;          /*!< Длина данных */
    uint8_t data[DATA_SW_SIZE]; /*!< Данные для прошивки */
} cmd_fw_upd_data__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
} cmd_fw_upd_data__out_t;
/**@}*/








#pragma pack(pop)

#endif
