/***************************************************************************//**
 * @file PinCommands.h.
 * @brief Модуль функций управления устройствами внешними выводами.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef PINCOMMANDS_H_
#define PINCOMMANDS_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_PIN_INIT PIN_INIT
 * \ingroup CMD_Commands PinCommands
 * \brief Настраивает группу каналов в соответствии с заданными настройками.
 */
/**@{*/
#define CMD__PIN_INIT                      0x1080
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_mask[2];       /*!< Маска каналов. 64 штуки.*/
}cmd_pin_init__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pin_init__out_t;
/**@}*/

/**
 * \defgroup GRP_PIN_RESET PIN_RESET
 * \ingroup CMD_Commands PinCommands
 * \brief Производит сброс настроек группы каналов к значениям производителя оборудования.
 */
/**@{*/
#define CMD__PIN_RESET                      0x1081
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t ch_mask[2];       /*!< Маска каналов. 64 штуки. */
}cmd_pin_reset__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_pin_reset__out_t;
/**@}*/





#pragma pack(pop)

#endif
