#ifndef SYNCCLOCKCOMMANDS_H_
#define SYNCCLOCKCOMMANDS_H_

#include "Types/System/cmd_oscillator_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_CALIB_WR CALIB_WR
 * \ingroup IO_Commands SyncClockCommands
 * \brief Запись калибрововчных параметров.
 * param_id - номер "строки".
 * Если последняя "строка" не кратна 8, то оставшиеся параметры должны принимать значения последнего "значащего" параметра (не мусор).
 * \image html CALIB_WR.png Алгоритм работы команды CALIB_WR
 */
/**@{*/
#define CMD__CALIB_WR               0x0081
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;                      /*!< Идентификатор параметра. */
    union {                                 /*!< Значение параметра - 128 байт. */
        uint8_t data[128];                  /*!< Значение параметра - \ref cmd_oscillator__pcal_t. */
        cmd_oscillator__pcal_t params[8];
    };
}cmd_calib_wr__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_calib_wr__out_t;
/**@}*/

/**
 * \defgroup GRP_CALIB_RD CALIB_RD
 * \ingroup IO_Commands SyncClockCommands
 * \brief Чтение калибровочных параметров. Если параметра нет, то возвращает \ref CMD_ERR__PARAM_NOT_FOUND.
 * \image html CALIB_RD.png Алгоритм работы команды CALIB_RD
 */
/**@{*/
#define CMD__CALIB_RD               0x0085
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;                      /*!< Номер параметра. */
}cmd_calib_rd__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t param_id;                      /*!< Идентификатор параметра. */
    union {                                 /*!< Значение параметра - 128 байт. */
         uint8_t data[128];
         cmd_oscillator__pcal_t params[8];
     };
} cmd_calib_rd__out_t;
/**@}*/

/**
 * \defgroup GRP_SYNC_CLK_INIT SYNC_CLK_INIT
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Команда инициализации группы "Настройка SYNC_CLOCK". Производит пересчет частот всех каналов на частоту 100Мгц.
 *        Состояние модуля идентично состоянию "при включении питания".
 */
/**@{*/
#define CMD__SYNC_CLK_INIT        0x019F
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_sync_clk_init__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_sync_clk_init__out_t;
/**@}*/

/**
 * \defgroup GRP_ROSCILLATOR_SET ROSCILLATOR_SET
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Устанавливает режим работы генератора. Производит пересчет частот всех каналов.
 */
/**@{*/
#define CMD__ROSCILLATOR_SET        0x01A0
/**
 * Входные параметры команды.
 */
typedef struct {
    cmd_oscillator__t data;
}cmd_roscillator_set__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_roscillator_set__out_t;
/**@}*/

/**
 * \defgroup GRP_ROSCILLATOR_SET_REQ ROSCILLATOR_SET_REQ
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Возвращает текущие настройки генератора.
 */
/**@{*/
#define CMD__ROSCILLATOR_SET_REQ    0x01A1
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_roscillator_set_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    cmd_oscillator__t data;
}cmd_roscillator_set_req__out_t;
/**@}*/

/**
 * \defgroup GRP_FSALIGMENT FSALIGMENT
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Синхронизация первой ступени модуля.
 */
/**@{*/
#define CMD__FSALIGMENT             0x01A2
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_fsaligment__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_fsaligment__out_t;
/**@}*/

/**
 * \defgroup GRP_SSALIGMENT SSALIGMENT
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Синхронизация второй ступени модуля.
 */
/**@{*/
#define CMD__SSALIGMENT             0x01A3
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_ssaligment__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_ssaligment__out_t;
/**@}*/

/**
 * \defgroup GRP_ROSCILLATOR_CH ROSCILLATOR_CH
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Устанавливает частоту для канала \ref cmd_oscillator__ch_id_e в соответствии с режимом расчета \ref cmd_oscillator__setup_e
 * и устанавливает состояние канала в соответствии с \ref cmd_oscillator__ch_mode_e.
 * В случае если текущая частота равна заданной, то
 * изменяется только состояние канала.
 */
/**@{*/
#define CMD__ROSCILLATOR_CH         0x01A4
/**
 * Входные параметры команды.
 */
typedef struct {
    cmd_oscillator__ch_t data;
}cmd_roscillator_ch__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    cmd_oscillator__ch_t data;
}cmd_roscillator_ch__out_t;
/**@}*/

/**
 * \defgroup GRP_ROSCILLATOR_CH_REQ ROSCILLATOR_CH_REQ
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Возвращает текущие настройки канала.
 */
/**@{*/
#define CMD__ROSCILLATOR_CH_REQ     0x01A5
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t ch_id;             /*!< Идентификатор канала \ref cmd_oscillator__ch_id_e. */
}cmd_roscillator_ch_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    cmd_oscillator__ch_t data;
}cmd_roscillator_ch_req__out_t;
/**@}*/

/**
 * \defgroup GRP_ROSCILLATOR_CС_SET ROSCILLATOR_CС_SET
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Устанавливает настройки канала без записи во флеш. Используется при отладке и калибровке.
 */
/**@{*/
#define CMD__ROSCILLATOR_CC_SET     0x01A6
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t ch_id;             /*!< Идентификатор канала микросхемы SyncClock 0..13. */
    uint8_t d_delay;            /*!< Цифровая задержка канала. */
    uint8_t a_delay;            /*!< Аналоговая задержка канала. */
}cmd_roscillator_cc_set__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_roscillator_cc_set__out_t;
/**@}*/

/**
 * \defgroup GRP_ROSCILLATOR_CС_GET ROSCILLATOR_CС_GET
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Считывает настройки канала микросхемы SyncClock. Используется при отладке и калибровке.
 */
/**@{*/
#define CMD__ROSCILLATOR_CC_GET     0x01A7
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t ch_id;             /*!< Идентификатор канала микросхемы SyncClock 0..13. */
}cmd_roscillator_cc_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    cmd_oscillator__pcal_t data;
}cmd_roscillator_cc_get__out_t;
/**@}*/

/**
 * \defgroup GRP_ROSCILLATOR_CC_MODE_SET ROSCILLATOR_CC_MODE_SET
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Устанавливает режим работы микросхемы SyncClock с калибровочными коэффициентами.
 *        также устанавливает активный SyncClock. Параметр oscil_id задает активный SyncClock и вся
 *        дальнейшая работа будет вестись с ним. Используется при отладке и калибровке.
 */
/**@{*/
#define CMD__ROSCILLATOR_CC_MODE_SET     0x01A8
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t oscil_id;         /*!< Идентификатор модуля SyncClock 0..255. */
    uint8_t mode;             /*!< Режим работы. 0- нормальный режим, 1- режим автокалибровки. */
}cmd_roscillator_cc_mode_set__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_roscillator_cc_mode_set__out_t;
/**@}*/

/**
 * \defgroup GRP_SYNC_CLK_TUNE SYNC_CLK_TUNE
 * \ingroup CMD_Commands SyncClockCommands
 * \brief Команда автонастройки мезонина SYNC_CLOCK. Производит вычитку и сохранение калибровочных коэффициентов для каждой
 *        частоты (из сетки). Вычитать результат работы можно при помощи команды \ref CMD__CALIB_RD.
 */
/**@{*/
#define CMD__SYNC_CLK_TUNE        0x01A9
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_sync_clk_tune__in_t;
/**
 * Не возвращает результата.
 */
typedef struct {
}cmd_sync_clk_tune__out_t;
/**@}*/











#pragma pack(pop)

#endif
