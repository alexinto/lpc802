/***************************************************************************//**
 * @file DiagCommands.h.
 * @brief Модуль функций диагностики.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DIAGCOMMANDS_H_
#define DIAGCOMMANDS_H_

#include "Types/DRV/diag_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_DIAG_DCL DIAG_DCL
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_DCL              0x1140
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t drv_mask[2];   /*!< Маска драйверов. 64 штуки.*/
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__dcl_test_e . */
}cmd_diag_dcl__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_diag_dcl__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_DCL_STATE_GET DIAG_DCL_STATE_GET
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_DCL_STATE_GET     0x1141
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__dcl_test_e . */
}cmd_diag_dcl_state_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__dcl_test_e . */
    uint8_t state;          /*!< Состояние теста \ref diag__state_e . */
}cmd_diag_dcl_state_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_PG DIAG_PG
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_PG              0x1142
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;       /*!< Идентификатор теста \ref diag__pg_test_e . */
}cmd_diag_pg__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_diag_pg__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_PG_STATE_GET DIAG_PG_STATE_GET
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_PG_STATE_GET     0x1143
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__pg_test_e . */
}cmd_diag_pg_state_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__pg_test_e . */
    uint8_t state;          /*!< Состояние теста \ref diag__state_e . */
}cmd_diag_pg_state_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_STATIC DIAG_STATIC
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_STATIC              0x1144
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;       /*!< Идентификатор теста \ref diag__static_test_e . */
}cmd_diag_static__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_diag_static__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_STATIC_STATE_GET DIAG_STATIC_STATE_GET
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_STATIC_STATE_GET     0x1145
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__static_test_e . */
}cmd_diag_static_state_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__static_test_e . */
    uint8_t state;          /*!< Состояние теста \ref diag__state_e . */
}cmd_diag_static_state_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_DINAMIC DIAG_DINAMIC
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_DINAMIC              0x1146
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;       /*!< Идентификатор теста \ref diag__dinamic_test_e . */
}cmd_diag_dinamic__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_diag_dinamic__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_DINAMIC_STATE_GET DIAG_DINAMIC_STATE_GET
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_DINAMIC_STATE_GET     0x1147
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__dinamic_test_e . */
}cmd_diag_dinamic_state_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__dinamic_test_e . */
    uint8_t state;          /*!< Состояние теста \ref diag__state_e . */
}cmd_diag_dinamic_state_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_PMU DIAG_PMU
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_PMU              0x1148
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;       /*!< Идентификатор теста \ref diag__pmu_test_e . */
}cmd_diag_pmu__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_diag_pmu__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_PMU_STATE_GET DIAG_PMU_STATE_GET
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_PMU_STATE_GET     0x1149
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__pmu_test_e . */
}cmd_diag_pmu_state_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__pmu_test_e . */
    uint8_t state;          /*!< Состояние теста \ref diag__state_e . */
}cmd_diag_pmu_state_get__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_MEAS DIAG_MEAS
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_MEAS              0x114A
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;       /*!< Идентификатор теста \ref diag__meas_test_e . */
}cmd_diag_meas__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_diag_meas__out_t;
/**@}*/

/**
 * \defgroup GRP_DIAG_MEAS_STATE_GET DIAG_MEAS_STATE_GET
 * \ingroup CMD_Commands DiagCommands
 * \brief
 */
/**@{*/
#define CMD__DIAG_MEAS_STATE_GET     0x114B
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__meas_test_e . */
}cmd_diag_meas_state_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t test_id;        /*!< Идентификатор теста \ref diag__meas_test_e . */
    uint8_t state;          /*!< Состояние теста \ref diag__state_e . */
}cmd_diag_meas_state_get__out_t;
/**@}*/


























#pragma pack(pop)

#endif
