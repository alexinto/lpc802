#ifndef JOURNALCOMMANDS_H_
#define JOURNALCOMMANDS_H_

#include "Types/System/log_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_JRNL_WR JRNL_WR
 * \ingroup IO_Commands JournalCommands
 * \brief Запись сообщения в журнал.
 * \image html JRNL_WR.png Алгоритм работы команды JRNL_WR
 */
/**@{*/
#define CMD__JRNL_WR               0x0080
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t event; /*!< Код события. */
    uint16_t p1;    /*!< Значение параметра 1. */
    uint16_t p2;    /*!< Значение параметра 2. */
    uint16_t p3;    /*!< Значение параметра 3. */
}cmd_jrnl_wr__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_jrnl_wr__out_t;
/**@}*/

/**
 * \defgroup GRP_JRNL_RD JRNL_RD
 * \ingroup IO_Commands JournalCommands
 * \brief Чтение журнала аварий. Если записи не существует, то \ref ANSWER_CMD__ERROR \ref CMD_ERR__PARAM_NOT_FOUND.
 *        Если индекс записи больше, чем индекс последней записи, то возвращаемое значение- последняя запись журнала.
 * \image html JRNL_RD.png Алгоритм работы команды JRNL_RD
 */
/**@{*/
#define CMD__JRNL_RD               0x0084
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;      /*!< Индекс записи. */
}cmd_jrnl_rd__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    union {                 /*!< Структура журанала аварий. */
        uint8_t data[16];
        log__msg_t msg;
    };
}cmd_jrnl_rd__out_t;
/**@}*/

/**
 * \defgroup GRP_TABLE_CHECK TABLE_CHECK
 * \ingroup IO_Commands JournalCommands
 * \brief Чтение записи из журнала аварий. Сбрасывает флаг "новая запись".
 * Если индекс больше, чем записей в журнале, то будет возвращена последняя запись.
 * Возвращает \ref CMD_ERR__PARAM_NOT_FOUND, если записи нет.
 * \image html TABLE_CHECK.png Алгоритм работы команды TABLE_CHECK
 */
/**@{*/
#define CMD__TABLE_CHECK            0x0087
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;      /*!< Индекс записи. */
}cmd_table_check__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    union {                 /*!< Структура журанала аварий. */
        uint8_t data[16];
        log__msg_t msg;
    };
}cmd_table_check__out_t;
/**@}*/

/**
 * \defgroup GRP_TABLE_REQ TABLE_REQ
 * \ingroup IO_Commands JournalCommands
 * \brief Максимальное число параметров таблицы
 * Возвращает максимальное число параметров таблицы.
 * Возвращает \ref CMD_ERR__PARAM_NOT_FOUND, если таблицы не существует.
 * \image html TABLE_REQ.png Алгоритм работы команды TABLE_REQ
 */
/**@{*/
#define CMD__TABLE_REQ              0x0088
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t table_id;   /*!< Номер таблицы \ журнала. \ref cmd_tbl__flash_e */
}cmd_table_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t table_id;   /*!< Номер таблицы \ журнала. */
    uint16_t param_num; /*!< Количество параметров. */
}cmd_table_req__out_t;
/**@}*/







#pragma pack(pop)

#endif
