#ifndef PIN400MODULE_H_
#define PIN400MODULE_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_PIN400_SETTINGS_WR PIN400_SETTINGS_WR
 * \ingroup IO_Commands Pin400Module
 * \brief Запись параметра в энергонезависимую память.
 */
/**@{*/
#define CMD__PIN400_SETTINGS_WR               0x1120
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Номер параметра в таблице. */
    uint16_t data_len;  /*!< Размер параметра, байт. */
    uint8_t data[128];  /*!< Значение параметра - 128 байт. */
}cmd_pin400_settings_wr__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_pin400_settings_wr__out_t;
/**@}*/

/**
 * \defgroup GRP_PIN400_SETTINGS_RD PIN400_SETTINGS_RD
 * \ingroup IO_Commands Pin400Module
 * \brief Чтение настроечных параметров. Если параметра нет, то возвращает \ref CMD_ERR__PARAM_NOT_FOUND.
 */
/**@{*/
#define CMD__PIN400_SETTINGS_RD               0x1121
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Номер параметра. */
}cmd_pin400_settings_rd__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t param_id;  /*!< Идентификатор параметра. */
    uint16_t data_len;  /*!< Размер параметра, байт. */
    uint8_t data[128];  /*!< Значение параметра - 128 байт. */
}cmd_pin400_settings_rd__out_t;
/**@}*/








#pragma pack(pop)

#endif
