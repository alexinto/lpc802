#ifndef SYNCMMMODULE_H_
#define SYNCMMMODULE_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_PG_NUM_GET PG_NUM_GET
 * \ingroup SyncmmModule
 * \brief Возвращает количество ГТП модуля.
 */
/**@{*/
#define CMD__PG_NUM_GET         0x01BF
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_pg_num_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t pg_num;    /*!< Количество ГТП модуля. */
}cmd_pg_num_get__out_t;
/**@}*/


/**
 * \defgroup GRP_PG_STATUS_GET PG_STATUS_GET
 * \ingroup SyncmmModule
 * \brief Возвращает состояние заданного ГТП. Возвращает CMD_ERR__HW_ERROR в случае если состояния не существует.
 */
/**@{*/
#define CMD__PG_STATUS_GET         0x01C3
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t pg_id;     /*!< Идентификатор модуля. Может быть получен командой \ref CMD__PG_NUM_GET */
}cmd_pg_status_get__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t pg_status; /*!< Состояние ГТП модуля. \ref pg__fc_ctrl_e */
}cmd_pg_status_get__out_t;
/**@}*/







#pragma pack(pop)

#endif
