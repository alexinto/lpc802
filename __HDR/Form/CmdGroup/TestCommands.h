#ifndef TESTCOMMANDS_H_
#define TESTCOMMANDS_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_TEST_CMD TEST_CMD
 * \ingroup CMD_Commands TestCommands
 *
 */
/**@{*/
#define CMD__TEST_CMD               0x01E0
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_test_cmd__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_test_cmd__out_t;
/**@}*/

/**
 * \defgroup GRP_TEST_SCMD TEST_SCMD
 * \ingroup CMD_Commands TestCommands
 * \brief Инициирует отправку синхрокоманды с параметрами указанными в команде SCMD_GENerate.
 */
/**@{*/
#define CMD__TEST_SCMD               0x00E1
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t scmd_num; /*!< Битовое поле соотв. синхрокомандам */
}cmd_test_scmd__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_test_scmd__out_t;
/**@}*/

/**
 * \defgroup GRP_TEST_SCMD_REQ TEST_SCMD_REQ
 * \ingroup CMD_Commands TestCommands
 * \brief Запрос результата приема синхрокоманды.
 * Запрос отправляется тому модулю, у которого мы хотим узнать получил ли он синхрокоманду.
 * После ответа поля структуры очищаются.
 */
/**@{*/
#define CMD__TEST_SCMD_REQ               0x00E2
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_test_scmd_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint32_t source_ip;     /*!< IP-адрес источника */
    uint16_t source_port;   /*!< Порт */
    uint8_t script_num;     /*!< Номер скрипта */
}cmd_test_scmd_req__out_t;
/**@}*/

/**
 * \defgroup GRP_TEST_TRIGGER TEST_TRIGGER
 * \ingroup CMD_Commands TestCommands
 * \brief Инициирует формирование триггерного сигнала соотв. заданному биту
 */
/**@{*/
#define CMD__TEST_TRIGGER              0x01E3
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t trigger_num; /*!< Битовое поле, соответствующее триггерным сигналам */
}cmd_test_trigger__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_test_trigger__out_t;
/**@}*/

/**
 * \defgroup GRP_TEST_TRIGGER_REQ TEST_TRIGGER_REQ
 * \ingroup CMD_Commands TestCommands
 * \brief Запрос результата приема триггерного сигнала.
 * Запрос отправляется тому модулю, у которого мы хотим узнать получил ли он триггерный сигнал.
 */
/**@{*/
#define CMD__TEST_TRIGGER_REQ          0x01E4
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_test_trigger_req__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t trigger_num;    /*!< Номер триггера */
    uint8_t trigger_slope;  /*!< Уровень срабатывания (slope) */
}cmd_test_trigger_req__out_t;
/**@}*/

/**
 * \defgroup GRP_TEST_PG TEST_PG
 * \ingroup CMD_Commands TestCommands
 * \brief Тест ГТП и шины BUS_SYNC_DATA
 */
/**@{*/
#define CMD__TEST_PG                    0x01E5
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pg_num;         /*!< Номер PG */
    uint8_t pg_test_num;    /*!< Номер теста PG */
}cmd_test_pg__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t pg_num;         /*!< Номер PG */
    uint8_t pg_test_num;    /*!< Номер теста PG */
    uint8_t pg_result;      /*!< Результат */
}cmd_test_pg__out_t;
/**@}*/







#pragma pack(pop)

#endif
