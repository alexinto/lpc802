#ifndef EXTIFCOMMANDS_H_
#define EXTIFCOMMANDS_H_

#include "Types/DRV/ext_if_types.h"

#pragma pack(push, 1)

/**
 * \defgroup GRP_EXTIF_SPI_INIT EXTIF_SPI_INIT
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Инициализация внешнего интерфейса SPI мезонина ExtIf.
 */
/**@{*/
#define CMD__EXTIF_SPI_INIT                    0x01E6
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t spi_id;         /*!< Номер интерфейса SPI */
    uint8_t spi_mode;       /*!< режим 0 (CPOL = 0, CPHA = 0), режим 1 (CPOL = 0, CPHA = 1), режим 2 (CPOL = 1, CPHA = 0), режим 3 (CPOL = 1, CPHA = 1). */
    uint8_t spi_pwr;        /*!< Способ питания устройств SPI \ref ext_if__spi_pwr_mode_e */
    uint8_t spi_clk_mode;   /*!< 0 - тактовый сигнал SPI активен только в моменты транзакций. 1 - тактовый сигнал SPI активен постоянно */
    uint32_t spi_freq;      /*!< Частота интерфейса SPI, Гц (732000 - 24000000)*/
}cmd_extif_spi_init__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_extif_spi_init__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_SPI_TXRX EXTIF_SPI_TXRX
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда отправки\приема данных по внешнему интерфейсу SPI мезонина ExtIf.
 */
/**@{*/
#define CMD__EXTIF_SPI_TXRX                    0x01E7
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t spi_id;                     /*!< Номер интерфейса SPI */
    uint32_t data_rd_size;              /*!< Размер данных для чтения (0 - только запись) */
    uint32_t data_wr_size;              /*!< Размер данных для записи (0 - только чтение) */
    uint8_t data[EXT_IF__BUFF_SIZE];    /*!< Данные для записи */
}cmd_extif_spi_txrx__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
        uint16_t data_len;                  /*!< Размер данных, байт */
        uint8_t data[EXT_IF__BUFF_SIZE]; /*!< Прочитанные данные */
}cmd_extif_spi_txrx__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_GPIO_SET EXTIF_GPIO_SET
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда управления выводами общего назначения мезонина ExtIf.
 */
/**@{*/
#define CMD__EXTIF_GPIO_SET                    0x01E8
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t gpio_id;                    /*!< Номер вывода (0..15) */
    uint8_t gpio_state;                 /*!< Состояние вывода \ref ext_if__gpio_state_e */
}cmd_extif_gpio_set__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_extif_gpio_set__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_EXT_PWR_CTRL EXTIF_EXT_PWR_CTRL
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда управления внешними источниками питания мезонина ExtIf.
 */
/**@{*/
#define CMD__EXTIF_EXT_PWR_CTRL                    0x01E9
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t pwr_id;                    /*!< Номер источника питания \ref ext_if__pwr_id_e */
    uint8_t pwr_state;                 /*!< Состояние (0 - отключен, 1- подключен) */
}cmd_extif_ext_pwr_ctrl__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_extif_ext_pwr_ctrl__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_I2C_INIT EXTIF_I2C_INIT
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда настройки внешнего интерфейса I2C мезонина ExtIf. Параметр timeout определяет время ожидания освобождения линии
 *        SCL (и SDA при командах \ref CMD__EXTIF_I2C_START, \ref CMD__EXTIF_I2C_WRITE, \ref CMD__EXTIF_I2C_WRITE).
 *        Например, для интерфейса SMBUS timeout = 25 мс.
 */
/**@{*/
#define CMD__EXTIF_I2C_INIT                    0x01EA
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t speed;                     /*!< Скорость интерфейса I2C \ref ext_if__speed_e */
    uint8_t pwr_mode;                  /*!< Способ питания устройств I2C \ref ext_if__i2c_pwr_mode_e */
    uint32_t timeout;                  /*!< Таймаут ожидания освобождения линий I2C (1..65535), мс. 0- таймаут не изменяется */
}cmd_extif_i2c_init__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_extif_i2c_init__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_I2C_START EXTIF_I2C_START
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда формирует событие "Start" внешнего интерфейса I2C мезонина ExtIf.
 *        При возникновении события "арбитраж" возвращает \ref CMD_ERR__BUSY
 *        При невозможности формирования возвращает \ref CMD_ERR__TIMEOUT
 */
/**@{*/
#define CMD__EXTIF_I2C_START                    0x01EB
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_extif_i2c_start__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_extif_i2c_start__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_I2C_WRITE EXTIF_I2C_WRITE
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда записывает 1 байт данных при помощи внешнего интерфейса I2C мезонина ExtIf.
 *        При возникновении события "арбитраж" возвращает \ref CMD_ERR__BUSY
 *        При отсутствии ACK от слейва возвращает \ref CMD__HW_ERROR
 *        При истечении таймаута ожидания SCL возвращает \ref CMD_ERR__TIMEOUT
 */
/**@{*/
#define CMD__EXTIF_I2C_WRITE                    0x01EC
/**
 * Входные параметры команды.
 */
typedef struct {
    uint16_t data_len;                    /*!< Данные для записи  */
    uint8_t data[EXT_IF__BUFF_SIZE];   /*!< Данные для записи  */
}cmd_extif_i2c_write__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_extif_i2c_write__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_I2C_READ EXTIF_I2C_READ
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда читает data_len байт данных при помощи внешнего интерфейса I2C мезонина ExtIf (отвечает ACK при чтении).
 *        Используется при чтении нескольких байт с устройства. Для чтения одного байта рекомендуется использовать команду \ref EXTIF_I2C_READN
 *        При истечении таймаута ожидания SCL возвращает \ref CMD_ERR__TIMEOUT
 *        При отсутствии ACK от слейва возвращает \ref CMD__HW_ERROR
*/
/**@{*/
#define CMD__EXTIF_I2C_READ                    0x01ED
/**
 * Не имеет входных параметров.
 */
typedef struct {
    uint16_t data_len;                /*!< Размер данных, байт  */
}cmd_extif_i2c_read__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t data_len;               /*!< Размер данных, байт  */
    uint8_t data[EXT_IF__BUFF_SIZE]; /*!< Прочитанные данные  */
}cmd_extif_i2c_read__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_I2C_READN EXTIF_I2C_READN
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда читает data_len байт данных при помощи внешнего интерфейса I2C мезонина ExtIf (отвечает NACK при чтении).
 *        Используется при чтении последнего байта.
 *        При истечении таймаута ожидания SCL возвращает \ref CMD_ERR__TIMEOUT
 *        При отсутствии ACK от слейва возвращает \ref CMD__HW_ERROR
 */
/**@{*/
#define CMD__EXTIF_I2C_READN                    0x01EE
/**
 * Не имеет входных параметров.
 */
typedef struct {
    uint16_t data_len;                /*!< Размер данных, байт  */
}cmd_extif_i2c_readn__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint16_t data_len;               /*!< Размер данных, байт  */
    uint8_t data[EXT_IF__BUFF_SIZE]; /*!< Прочитанные данные  */
}cmd_extif_i2c_readn__out_t;
/**@}*/

/**
 * \defgroup GRP_EXTIF_I2C_STOP EXTIF_I2C_STOP
 * \ingroup CMD_Commands ExtIfCommands
 * \brief Команда формирует событие "Stop" внешнего интерфейса I2C мезонина ExtIf.
 *        При возникновении события "арбитраж" возвращает \ref CMD_ERR__BUSY
 *        При невозможности формирования возвращает \ref CMD_ERR__TIMEOUT
 */
/**@{*/
#define CMD__EXTIF_I2C_STOP                    0x01EF
/**
 * Не имеет входных параметров.
 */
typedef struct {
}cmd_extif_i2c_stop__in_t;
/**
* Не возвращает результата выполнения.
*/
typedef struct {
}cmd_extif_i2c_stop__out_t;
/**@}*/





#pragma pack(pop)

#endif
