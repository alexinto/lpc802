/**
 * @file DPSModuleCommands.h
 * @author Alexander Tolstunov (tolstunov@form.ru)
 * @brief
 * @version 0.1
 * @date 2023-02-15
 *
 * @copyright Copyright (c) 2023
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _DPSMODULECOMMANDS_H_
#define _DPSMODULECOMMANDS_H_

/* Includes ------------------------------------------------------------------*/
#include "Types/DRV/dps_types.h"

/**
 * @defgroup GRP_DPS_FV DPS_FV
 * @ingroup DPSModule
 * @brief Команда задания выходного напряжения канала и перехода в режим FV
 */
/**@{*/
#define CMD__DPS_FV                                 0x1500
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t channels;          /*!< Список каналов (битовая маска) */
    double voltage;             /*!< Задаваемое напряжение */
    uint16_t i_clamp_hi;        /*!< Верхнее ограничение силы тока */
    uint16_t i_clamp_lo;        /*!< Нижнее ограничение силы тока */
    uint8_t curr_range;         /*!< Диапазон силы тока @ref dps_types__curr_range_e */
    uint8_t slew_rate;          /*!< Скорость нарастания напряжения @ref dps_types__slew_rate_e */
}cmd_dps_fv__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct{
}cmd_dps_fv__out_t;
/**@}*/

/**
 * @defgroup GRP_DPS_FN DPS_FN
 * @ingroup DPSModule
 * @brief Команда перехода канала в режим FN
 */
/**@{*/
#define CMD__DPS_FN                                 0x1501
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t channels;          /*!< Список каналов (битовая маска) */
}cmd_dps_fn__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct{
}cmd_dps_fn__out_t;
/**@}*/

/**
 * @defgroup GRP_DPS_MV DPS_MV
 * @ingroup DPSModule
 * @brief Команда перехода канала в режим MV
 */
/**@{*/
#define CMD__DPS_MV                                 0x1502
/**
 * Входные параметры команды.
 */
typedef struct {
    uint32_t channels;          /*!< Список каналов (битовая маска) */
}cmd_dps_mv__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct{
}cmd_dps_mv__out_t;
/**@}*/

/**
 * @defgroup GRP_DPS_SET_PERF_MODE DPS_SET_PERF_MODE
 * @ingroup DPSModule
 * @brief Выбор режима производительности каналов
 */
/**@{*/
#define CMD__DPS_SET_PERF_MODE                      0x1503
/**
 * Входные параметры команды.
 */
typedef struct
{
    uint32_t channels;          /*!< Список каналов (битовая маска) */
    uint8_t perf_mode;          /*!< Режим производительности @ref dps_types__perf_mode_e */
}cmd_dps_set_perf_mode__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct{
}cmd_dps_set_perf_mode__out_t;
/**@}*/

/**
 * @defgroup GRP_DPS_SET_LOAD DPS_SET_LOAD
 * @ingroup DPSModule
 * @brief Команда задания значения ёмкостной нагрузки
 */
#define CMD__DPS_SET_LOAD                           0x1504
/**
 * Входные параметры команды.
 */
/**@{*/
typedef struct 
{
    uint32_t channels;          /*!< Список каналов (битовая маска) */
    double cdut;                /*!< Значения ёмкостной нагрузки (Ф) */
    double esr;                 /*!< Последовательное сопротивление нагрузки (Ом) */
}cmd_dps_set_load__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct{
}cmd_dps_set_load__out_t;
/**@}*/

/**
 * @defgroup GRP_DPS_GET_PERF_MODE DPS_GET_PERF_MODE
 * @ingroup DPSModule
 * @brief Команда возвращает используемые режимы каналов.
 */
/**@{*/
#define CMD__DPS_GET_PERF_MODE                      0x1505
/**
 * Входные параметры команды.
 */
typedef struct{
    uint16_t channel;          /*!< Номер канала */
}cmd_dps_get_perf_mode__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    uint8_t data;   /*!< Данные @ref dps_types__perf_mode_e */
}cmd_dps_get_perf_mode__out_t;
/**@}*/

/**
 * @defgroup GRP_DPS_GET_MODE DPS_GET_MODE
 * @ingroup DPSModule
 * @brief Команда чтения используемого режима канала
 */
/**@{*/
#define CMD__DPS_GET_MODE                           0x1506
/**
 * Входные параметры команды.
 */
typedef struct{
    uint16_t channel;           /*!< Номер канала */
}cmd_dps_get_mode__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    uint8_t data;               /*!< Данные @ref dps_types__ch_mode_e */
}cmd_dps_get_mode__out_t;
/**@}*/

/**
 * @defgroup GRP_DPS_RANGEI DPS_RANGEI
 * @ingroup DPSModule
 * @brief Команда чтения используемого диапазона силы тока
 */
#define CMD__DPS_RANGEI                             0x1507
/**
 * Входные параметры команды.
 */
typedef struct{
    uint16_t channel;           /*!< Номер канала */
}cmd_dps_get_range_i__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    uint8_t data;               /*!< Данные @ref dps_types__curr_range_e */
}cmd_dps_get_range_i__out_t;

/**
 * @defgroup GRP_DPS_GET_SLEW_RATE DPS_GET_SLEW_RATE
 * @ingroup DPSModule
 * @brief Команда чтения параметра скорости нарастания напряжения основного усилителя канала
 */
#define CMD__DPS_SLEW_RATE                          0x1508
/**
 * Входные параметры команды.
 */
typedef struct{
    uint16_t channel;           /*!< Номер канала */
}cmd_dps_get_slew_rate__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    uint8_t data;               /*!< Данные @ref dps_types__slew_rate_e */
}cmd_dps_get_slew_rate__out_t;

/**
 * @defgroup GRP_DPS_GET_U DPS_GET_U
 * @ingroup DPSModule
 * @brief Команда чтения заданного напряжения на канале
 */
#define CMD__DPS_GET_U                              0x1509
/**
 * Входные параметры команды.
 */
typedef struct{
    uint16_t channel;           /*!< Номер канала */
}cmd_dps_get_u__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    double data;                /*!< Данные напряжения типа double */
}cmd_dps_get_u__out_t;

/**
 * @defgroup GRP_DPS_GET_LOAD DPS_GET_LOAD
 * @ingroup DPSModule
 * @brief Команда чтения заданного значения ёмкостной нагрузки (Cdut) и эквивалентного последовательного сопротивления нагрузки (ESRdut).
 */
#define CMD__DPS_GET_LOAD                           0x150A
/**
 * Входные параметры команды.
 */
typedef struct{
    uint16_t channel;           /*!< Номер канала */
}cmd_dps_get_load__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    double cDut;                /*!< Ёмкостная нагрузка (Ф) */
    double esrDut;              /*!< Сопротивление нагрузки (Ом) */
}cmd_dps_get_load__out_t;

/**
 * @defgroup GRP_DPS_GET_ICLAMP_HI DPS_GET_ICLAMP_HI
 * @ingroup DPSModule
 * @brief Команды чтения заданного значения врхнего ограничения силы тока канала
 */
#define CMD__DPS_GET_ICLAMP_HI                      0x150B
/**
 * Входные параметры команды.
 */
typedef struct{
    uint16_t channel;           /*!< Номер канала */
}cmd_dps_get_iclamp_hi__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    double data;                /*!< Данные верхнего ограничения */
}cmd_dps_get_iclamp_hi__out_t;

/**
 * @defgroup GRP_DPS_GET_ICLAMP_LO DPS_GET_ICLAMP_LO
 * @ingroup DPSModule
 * @brief Команды чтения заданного значения нижнего ограничения силы тока канала
 */
#define CMD__DPS_GET_ICLAMP_LO                      0x150C
/**
 * Входные параметры команды.
 */
typedef struct{
    uint16_t channel;               /*!< Номер канала */
}cmd_dps_get_iclamp_lo__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    double data;                    /*!< Данные нижнего ограничения */
}cmd_dps_get_iclamp_lo__out_t;

/**
 * @defgroup GRP_DPS_GET_ICLAMP_STATE DPS_GET_ICLAMP_STATE
 * @ingroup DPSModule
 * @brief Команда чтения флага ограничения силы тока канала
 */
#define CMD__DPS_GET_ICLAMP_STATE                   0x150D
/**
 * Не имеет входных параметров.
 */
typedef struct{
}cmd_dps_get_iclamp_state__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    uint32_t iclamp_state;          /*!< Возвращаемые значение – 32-х битовая маска состояний для каждого канала: 0 - ограничение включено; 1 - ограничение выключено*/
}cmd_dps_get_iclamp_state__out_t;

/**
 * @defgroup GRP_DPS_MEAS_U DPS_MEAS_U
 * @ingroup DPSModule
 * @brief Команда измерения напряжения каналов
 */
#define CMD__DPS_MEAS_U                             0x150E
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t channels;              /*!< Список каналов (битовая маска)  */
}cmd_dps_meas_u__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    double value_u;                 /*!< Измеренное значение */
    double rms_u;                   /*!< Среднеквадратичное отклонение */
}cmd_dps_meas_u__out_t;

/**
 * @defgroup GRP_DPS_MEAS_I DPS_MEAS_I
 * @ingroup DPSModule
 * @brief Команда измерения напряжения каналов
 */
#define CMD__DPS_MEAS_I                             0x150F
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t channels;              /*!< Список каналов (битовая маска)  */
}cmd_dps_meas_i__in_t;
/**
* Возвращаемые значения команды
*/
typedef struct{
    double value_i;                 /*!< Измеренное значение */
    double rms_i;                   /*!< Среднеквадратичное отклонение */
}cmd_dps_meas_i__out_t;

/**
 * @defgroup GRP_DPS_SET_VOLTAGE DPS_SET_VOLTAGE
 * @ingroup DPSModule
 * @brief Задание выходного напряжения каналов
 */
#define CMD__DPS_SET_VOLTAGE                        0x1510
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t channels;              /*!< Список каналов (битовая маска) */
    double value;                   /*!< Значение напряжения */
}cmd_dps_set_voltage__in_t;
/**
 * Не имеет входных параметров.
 */
typedef struct{
}cmd_dps_set_voltage__out_t;

/**
 * @defgroup GRP_DPS_SET_IRANGE DPS_SET_IRANGE
 * @ingroup DPSModule
 * @brief Задание диапазона силы тока каналов
 */
#define CMD__DPS_SET_IRANGE                         0x1511
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t channels;              /*!< Список каналов (битовая маска)  */
    uint8_t value;                  /*!< Значение дипозона @ref dps_types__curr_range_e */
}cmd_dps_set_irange__in_t;
/**
 * Не имеет входных параметров.
 */
typedef struct{
}cmd_dps_set_irange__out_t;

/**
 * @defgroup GRP_DPS_SET_ICLAMP_HI DPS_SET_ICLAMP_HI
 * @ingroup DPSModule
 * @brief Задание ограничения силы тока верхнего уровня («вытекающий» ток).
 */
#define CMD__DPS_SET_ICLAMP_HI                      0x1512
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t channels;              /*!< Список каналов (битовая маска)  */
    uint16_t value;                 /*!< Значение ограничения */
}cmd_dps_set_iclamp_hi__in_t;
/**
 * Не имеет входных параметров.
 */
typedef struct{
}cmd_dps_set_iclamp_hi__out_t;

/**
 * @defgroup GRP_DPS_SET_ICLAMP_LO DPS_SET_ICLAMP_LO
 * @ingroup DPSModule
 * @brief Задание ограничения силы тока нижнего уровня («втекающий» ток).
 */
#define CMD__DPS_SET_ICLAMP_LO                      0x1513
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t channels;                  /*!< Список каналов (битовая маска)  */
    uint16_t value;                     /*!< Значение ограничения */
}cmd_dps_set_iclamp_lo__in_t;
/**
 * Не имеет входных параметров.
 */
typedef struct{    
}cmd_dps_set_iclamp_lo__out_t;

/**
 * @defgroup GRP_DPS_SET_SLEW DPS_SET_SLEW
 * @ingroup DPSModule
 * @brief Задание ограничения силы тока нижнего уровня («втекающий» ток).
 */
#define CMD__DPS_SET_SLEW                           0x1514
/**
 * Входные параметры команды.
 */
typedef struct{
    uint32_t channels;                  /*!< Список каналов (битовая маска)  */
    uint8_t value;                      /*!< Значение скорости нарастания напряжения @ref dps_types__slew_rate_e */
}cmd_dps_set_slew__in_t;
/**
 * Не имеет входных параметров.
 */
typedef struct{    
}cmd_dps_set_slew__out_t;

#endif /* _DPSMODULECOMMANDS_H_ */
