#ifndef HWCOMMANDS_H_
#define HWCOMMANDS_H_

#pragma pack(push, 1)

/**
 * \defgroup GRP_MEMORY_WR MEMORY_WR
 * \ingroup CMD_Commands IO_Commands HwCommands
 * \brief Записывает данные по заданному адресу.
 * \image html MEMORY_WR.png Алгоритм работы команды MEMORY_WR
 */
/**@{*/
#define CMD__MEMORY_WR              0x0180
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t size;   /*!< Размер данных, в байтах (0..4). Недостающие данные дополняются нулями. */
    uint32_t addr;  /*!< Адрес, в который необходимо записать данные. */
    uint32_t data;  /*!< Данные, little endian. */
}cmd_memory_wr__in_t;
/**
 * Не возвращает результата выполнения.
 */
typedef struct {
}cmd_memory_wr__out_t;
/**@}*/

/**
 * \defgroup GRP_MEMORY_RD MEMORY_RD
 * \ingroup CMD_Commands IO_Commands HwCommands
 * \brief Читает данные по заданному адресу.
 */
/**@{*/
#define CMD__MEMORY_RD              0x0181
/**
 * Входные параметры команды.
 */
typedef struct {
    uint8_t size;       /*!< Размер данных, в байтах (0..4). */
    uint32_t addr;      /*!< Адрес, с которого необходимо прочитать данные. */
}cmd_memory_rd__in_t;
/**
 * Возвращаемые значения команды.
 */
typedef struct {
    uint8_t size;       /*!< Размер данных, в байтах (0..4). Недостающие данные содержат "мусор". */
    uint32_t addr;      /*!< Адрес, с которого прочитаны данные. */
    uint32_t data;      /*!< Данные, little endian. */
}cmd_memory_rd__out_t;
/**@}*/






#pragma pack(pop)

#endif
