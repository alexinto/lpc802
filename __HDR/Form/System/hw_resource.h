﻿/***************************************************************************//**
 * @file hw_resource.h.
 * @brief Описание функций аппаратно-зависимой части ресурса. Должны быть реализованы
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef HW_RESOURCE_H_
#define HW_RESOURCE_H_

#include "System/events.h"

events__e hw_resource__init(int id);
events__e hw_resource__lock(void* addr);
events__e hw_resource__unlock(void* addr);
events__e hw_resource__get_state(void* addr);

#endif
