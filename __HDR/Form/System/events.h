/***************************************************************************//**
 * @file events.h.
 * @brief Файл, содержащий перечень событий.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef EVENTS_H_
#define EVENTS_H_

/**
 * @defgroup GRP_EVENTS Модуль перечня событий
 * @brief Модуль содержащий перечень событий
 * @{
 */

typedef enum {
    EVENT__NOT_EXIST        = -10,// Событие параметра не существует.
    EVENT__HW_ERROR         = -9, // Аппаратная ошибка (железо).
    EVENT__PARAM_NOT_FOUND  = -8,
    EVENT__NO_MEM           = -7, // Не хватает памяти, буффер переполнен.
    EVENT__TIMEOUT          = -6,
    EVENT__BLOCK            = -5,
    EVENT__PROTECT          = -4,
    EVENT__PARAM_NA         = -3,
    EVENT__BUSY             = -2,
    EVENT__ERROR            = -1,
    EVENT__OK               =  0,
    EVENT__CONTINUE         =  1, // Внутреннее событие модуля
    EVENT__OPEN             =  2,
    EVENT__CLOSE            =  3,
    EVENT__INIT             =  4,
    EVENT__DEINIT           =  5,
    EVENT__LOCK             =  6, // Захват ресурса.
    EVENT__EXECUTE          =  8, // Событие обработки результата выполнения.
    EVENT__NO_OPT           =  9, // Событие успешного завершения без опциональных особенностей
    EVENT__WAIT             =  10,// Внутреннее событие модуля
}events__e;

/**
 * @}
 */

#endif
