﻿/***************************************************************************//**
 * @file RxiTime.h.
 * @brief Модуль работы со временем.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef RXI_TIME_H_
#define RXI_TIME_H_

/**
 * @defgroup GRP_RXI_TIME Модуль работы со временем
 * @brief Модуль работы со временем
 * @{
 */

#include "stdint.h"
#include "System/events.h"

#define DELAY_MS(x)     for(volatile int i = 0; i < x * 10000; i++)  // todo Заменить на нормальный таймер когда сделаем!!!

events__e time__init();

uint32_t time__get();

uint32_t time__get_mks();

void time__set(uint32_t timestamp);

/**
 * @}
 */

#endif
