﻿/***************************************************************************//**
 * @file fdebug.h
 * @brief Модуль для вывода отладочной информации.
 *        Для использования необходимо определить FDEBUG__ON в target.h.
 *        Также необходимо выбрать интерфейс для вывода отладочной информации FDEBUG__ITF_ID
 *        в файле target.h.
 * @author a.tushentsov
 ******************************************************************************/

#ifndef FDEBUG_H_
#define FDEBUG_H_

#include <stdint.h>
#include <stdio.h>
#include "target.h"
#include "System/events.h"
#include "Types/System/fdebug_types.h"

typedef void (*fdebug__handler_t)(events__e event, fdebug__struct_t* settings, char* buff, int len);

/***************************************************************************//**
 * @brief Функция инициализации Debug.
 *        Так же будет вызван event_handler  с событием EVENT__INIT.
 * @param settings - указатель на настройки.
 * @param handler - обработчик событий модуля Debug. Может быть NULL.
 *                  Используется для получения событий окончания процесса инициализации, деинициализации.
 *                  Так же используется для передачи отладочной информации по пользовательским интерфейсам, ethernet.
 * @return void.
 ******************************************************************************/
void fdebug__init(fdebug__struct_t* settings, fdebug__handler_t handler);

/***************************************************************************//**
 * @brief Функция деинициализации Debug. Может использовтаься для смены настроек либо
 *        для освобождения интерфейса.
 *        Так же будет вызван event_handler  с событием EVENT__DEINIT.
 * @return void.
 ******************************************************************************/
void fdebug__deinit();

#ifdef FDEBUG__ON
extern char fdebug__buff[];
int fdebug__check(uint8_t level);
void fdebug__print();

#define FDEBUG(x, args...)    do{                                 \
                                if (fdebug__check(x)) {           \
                                    sprintf(fdebug__buff, args);  \
                                    fdebug__print(x);             \
                                }                                 \
                             }while(0)

#else
    #define FDEBUG(x, args...)  do{}while(0)
#endif


#endif /* DEBUG_H_ */
