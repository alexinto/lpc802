/***************************************************************************//**
 * @file fstandard.h.
 * @brief Модуль с типами данных.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef FSTANDARD_H_
#define FSTANDARD_H_
#include <stddef.h>
#include <stdint.h>
#include <string.h>

typedef uint8_t   u8;
typedef int8_t    i8;
typedef uint16_t u16;
typedef int16_t  i16;
typedef uint32_t u32;
typedef int32_t  i32;
typedef double   d64;

typedef union {
    uint8_t    u8;
    int8_t     i8;
    uint16_t  u16;
    int16_t   i16;
    uint32_t  u32;
    int32_t   i32;
    double    d64;
    void*     ptr;
}fstd__u;














#endif
