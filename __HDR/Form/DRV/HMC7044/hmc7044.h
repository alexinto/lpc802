﻿/***************************************************************************//**
 * @file hmc7044.h.
 * @brief Драйвер микросхемы hmc7044.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef HMC7044_H_
#define HMC7044_H_
#include "System/events.h"
#include "DRV/spi.h"

// Настройки интерфейса SPI
#define HMC7044__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_3_MBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB)

#pragma pack(push,1)
typedef struct {
    uint8_t channel;                      // "High performance mode", "SYNC enable", "Slip enable", "Start-Up Mode[1:0]", "Multislip enable", "Channel enable"
    uint8_t ch_div[2];                    // "12-Bit Channel Divider"
    uint8_t a_delay;                      // "Fine Analog Delay"
    uint8_t d_delay;                      // "Coarse Digital Delay"
    uint8_t multi_delay[2];               // "12-Bit Multislip Digital Delay"
    uint8_t mux_mode;                     // "Output Mux Selection"
    uint8_t driver_mode;                  // "Force Mute", "Dynamic driver enable", "Driver Mode", "Driver Impedance"
    uint8_t zreserv1;
}hmc7044__out_struct_t;

#pragma pack(1)
typedef struct {
    uint8_t soft_reset;                    // 0x00 "Global soft reset control"
    uint8_t mode_ctrl[2];                  // 0x01 "Global request and mode control"
    uint8_t en_ctrl[2];                    // 0x03 "Global enable control"
    uint8_t glob_mode;                     // 0x05 "Global mode and enable control"
    uint8_t clr_alarms;                    // 0x06 "Global clear alarms"
    uint8_t zreserv[3];
    uint8_t clk_in_buff[4];                // 0x0A "CLKIN0 input buffer control", "CLKIN1 input buffer control", "CLKIN2 input buffer control", "CLKIN3 input buffer control"
    uint8_t oscin_buf_ctrl;                // 0x0E "OSCIN/OSCIN input buffer control"
    uint8_t zreserv1[5];
    uint8_t pll1_ctrl[8];                  // 0x14 "PLL1 reference priority control", "PLL1 loss of signal (LOS) control", "PLL1 holdover exit control", "PLL1 holdover DAC/ADC control", "PLL1 LOS mode control", "PLL1 charge pump control", "PLL1 PFD control"
    uint8_t clk_in_psc[4];                 // 0x1C "CLKIN0 input prescaler control", "CLKIN1 input prescaler control", "CLKIN2 input prescaler control", "CLKIN3 input prescaler control"
    uint8_t oscin_psc;                     // 0x20 "OSCIN/OSCIN Input prescaler control"
    uint8_t pll1_r1[2];                    // 0x21 "PLL1 reference divider control (R1)"
    uint8_t zreserv2[3];
    uint8_t pll1_n1[2];                    // 0x26 "PLL1 feedback divider control (N1)"
    uint8_t pll1_lock_det;                 // 0x28 "PLL1 lock detect control"
    uint8_t pll1_ref_sw;                   // 0x29 "PLL1 reference switching control"
    uint8_t pll1_holdoff;                  // 0x2A "PLL1 holdoff time control"
    uint8_t zreserv3[7];
    uint8_t pll2_doubler;                  // 0x32 "PLL2 frequency doubler control"
    uint8_t pll2_r2[2];                    // 0x33 "PLL2 reference divider control (R2)"
    uint8_t pll2_n2[2];                    // 0x35 "PLL2 feedback divider control (N2)"
    uint8_t pll2_charge_pump;              // 0x37 "PLL2 charge pump control"
    uint8_t pll2_pfd;                      // 0x38 "PLL2 PFD control"
    uint8_t oscout_path;                   // 0x39 "OSCOUTx path control"
    uint8_t oscout_drv[2];                 // 0x3A "OSCOUTx driver control"
    uint8_t zreserv4[10];
    uint8_t gpi_ctrl[10];                  // 0x46 "GPI1 control", "GPI2 control", "GPI3 control", "GPI4 control"
    uint8_t gpo_ctrl[4];                   // 0x50 "GPO1 control", "GPO2 control", "GPO3 control", "GPO4 control"
    uint8_t sdata;                         // 0x54 "SDATA control"
    uint8_t zreserv5[5];
    uint8_t pulse_gen;                     // 0x5A "Pulse generator control"
    uint8_t sync;                          // 0x5B "SYNC control"
    uint8_t sysref_tmr[2];                 // 0x5C "SYSREF timer control"
    uint8_t zreserv6[6];
    uint8_t vco_ctrl;                      // 0x64 "External VCO control"
    uint8_t analog_delay;                  // 0x65 "Analog delay common control"
    uint8_t zreserv7[10];
    uint8_t pll1_alarm_ctrl;               // 0x70 "PLL1 alarm mask control"
    uint8_t alarm_mask;                    // 0x71 "Alarm mask control"
    uint8_t zreserv8[6];
    uint8_t id[3];                         // 0x78 "Product ID"
    uint8_t alarm_sig;                     // 0x7B "Readback register"
    uint8_t pll1_alarm;                    // 0x7C "PLL1 alarm readback"
    uint8_t alarm_read[3];                 // 0x7D "Alarm readback", "Latched alarm readback", "Alarm readback miscellaneous"
    uint8_t zreserv9[2];
    uint8_t pll1_stat[6];                  // 0x82 "PLL1 status registers"
    uint8_t zreserv10[4];
    uint8_t pll2_stat[5];                  // 0x8C "PLL2 status registers"
    uint8_t sysref_stat;                   // 0x91 "SYSREF status register"
    uint8_t zreserv11[13];
    uint8_t clk_out_drv_pw[2];             // 0x9F "Clock output driver power setting"
    uint32_t zreserv12;
    uint8_t pll1_delay;                    // 0xA5 "PLL1 more delay"
    uint16_t zreserv13;
    uint8_t pll1_holdover;                 // 0xA8 "PLL1 holdover DAC gm setting"
    uint8_t zreserv14[7];
    uint8_t vtune_preset;                  // 0xB0 "VTUNE preset setting"
    uint8_t zreserv15;
    uint8_t bank_cap;                      // 0xB2 "Выбор конденсаторов"
    uint8_t zreserv16[21];
    hmc7044__out_struct_t out_ctrl[14];    // 0xC8 "Channel Output 0-13 control"
}hmc7044__reg_struct_t;

#pragma pack(pop)
// Макрос для получения адреса регистра HMC7044
#define HMC7044_REG (uint32_t)&((hmc7044__reg_struct_t*)0x00)

events__e hmc7044__read(int id, uint16_t addr, uint8_t* data, int size);
events__e hmc7044__write(int id, uint16_t addr, uint8_t* data, int size);

#endif
