﻿/***************************************************************************//**
 * @file spi.h.
 * @brief Модуль программного SPI.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>
#include "System/events.h"
// Настройки SPI - settings. Передаются при инициализации порта.
// Биты 0-1 определяют настройку режима работы модуля SPI
#define SPI__MODE_MASTER             (0x01UL << 0)
#define SPI__MODE_SLAVE              (0x02UL << 0)
#define SPI__MODE_MASK               (0x03UL << 0)
//  Биты 2-5 определяют настройку максимальной скорости обмена по интерфейсу 
#define SPI__BR_LOW_SPD              (0x01UL << 2)
#define SPI__BR_375_KBIT             (0x02UL << 2)
#define SPI__BR_750_KBIT             (0x03UL << 2)
#define SPI__BR_1500_KBIT            (0x04UL << 2)
#define SPI__BR_3_MBIT               (0x05UL << 2)
#define SPI__BR_6_MBIT               (0x06UL << 2)
#define SPI__BR_12_MBIT              (0x07UL << 2)
#define SPI__BR_24_MBIT              (0x08UL << 2)
#define SPI__BR_48_MBIT              (0x09UL << 2)
#define SPI__BR_96_MBIT              (0x0AUL << 2)
#define SPI__BR_HIGH_SPD             (0x0BUL << 2)
#define SPI__BR_MASK                 (0x0FUL << 2)
//  Биты 6-7 определяют настройку формата кадра
#define SPI__DATA_FRAME_7_BIT        (0x01UL << 6)
#define SPI__DATA_FRAME_8_BIT        (0x02UL << 6)
#define SPI__DATA_FRAME_16_BIT       (0x03UL << 6)
#define SPI__FRAME_MASK              (0x03UL << 6)
//  Биты 8-9 определяют настройку фазы и полярности тактового сигнала
#define SPI__CPHA_0_CPOL_0           (0x00UL << 8)
#define SPI__CPHA_0_CPOL_1           (0x01UL << 8)
#define SPI__CPHA_1_CPOL_0           (0x02UL << 8)
#define SPI__CPHA_1_CPOL_1           (0x03UL << 8)
#define SPI__CPHA_CPOL_MASK          (0x03UL << 8)
//  Биты 10-11 определяют настройку порядока передачи байта
#define SPI__LSB                     (0x01UL << 10)
#define SPI__MSB                     (0x02UL << 10)
#define SPI__LSBMSB_MASK             (0x03UL << 10)
//  Бит 12-19 определяет задержку после установки CS в активный уровень (0..256), мкс.
#define SPI__CS_DELAY_MASK           (0xFFUL << 12)
//  Бит 20 определяет настройку CS (инвертирует)
#define SPI__CS_INV                  (0x01UL << 20)
#define SPI__CS_MASK                 (0x01UL << 20)
//  Бит 21 определяет настройку CLK (1- постоянное тактирование, 0- CLK только при транзакциях)
#define SPI__CLK_ALWAYS              (0x01UL << 21)
#define SPI__CLK_MASK                (0x01UL << 21)
// Бит 22 Контекст вызова (выполнения), 0- в контексте задачи, 1- в контексте прерывания.
#define SPI__CALLBACK_ISR            (0x01 << 22)
#define SPI__CALLBACK_MASK           (0x01 << 22)

/***************************************************************************//**
 * @brief Прототип функции обратного вызова по окончании операции чтения/записи.
 *        Передается в фнкцию hw_spi__txrx
 * @param id - идентификатор порта.
 * @param event - результат выполнения операции.
 * @param buff - указатель на буфер с принятыми данными.
 * @param len - количество принятых данных, байт.
 * @return void.
 ******************************************************************************/
typedef void (*spi__cb_t)(int id, events__e code, uint8_t *buff, int len, void *ext_data);

/***************************************************************************//**
 * @brief Инициализация ресурса SPI.
 * @param spi_id - идентификатор порта SPI.
 * @param setting - настройки порта SPI (битовое поле). 
 * @return events__e.
 ******************************************************************************/
events__e spi__init(int spi_id, uint32_t settings);

/***************************************************************************//**
 * @brief Функция прекращения выполняемых SPI операций и освобождение ресурсов
 * @param spi_id - идентификатор порта.
 * @return void
 ******************************************************************************/
events__e spi__deinit(int spi_id);

/***************************************************************************//**
 * @brief Инициализация обмена по SPI.
 * @param spi_id - идентификатор порта SPI.
 * @param rx_buff - указатель на приемный буфер.
 *                  Если NULL, то в буфер ничего не пишем.
 * @param tx_buff - указатель на передающий буфер.
 *                  Если NULL, то передаем 0xFF.
 * @param len - длина обмена.
 * @param callback - указатель на функцию обратного вызова.
 * @param ext_data - указатель на пользовательские данные.
 * @return events__e.
 ******************************************************************************/
events__e spi__txrx(int spi_id, uint8_t *rx_buff, uint32_t rx_len, uint8_t *tx_buff, uint32_t tx_len, spi__cb_t callback, void *ext_data);

/***************************************************************************//**
 * @brief Функция прекращения приема/передачи данных
 * @param spi_id - идентификатор порта.
 * @return  EVENT__OK - прием/передача на указанном SPI остановлена.
 *          EVENT__PARAM_NA - неверный идентификатор SPI.
 ******************************************************************************/
events__e spi__break_txrx(int spi_id);

/***************************************************************************//**
* @brief Функция внешнего вызова для обработки текущего состояния драйвера SPI.
* Должна вызываться в суперцикле систем без ОС.
* @param void.
* @return void.
******************************************************************************/
void spi__cout(void);

#endif /* SPI_H_ */
