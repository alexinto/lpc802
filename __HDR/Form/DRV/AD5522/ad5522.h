/***************************************************************************//**
 * @file ad5522.h.
 * @brief Модуль, с описанием функций управления микрохемой AD5522. Тип данных (data), зависит от регистра reg:
 *        Для регистров AD5522_PMU - pmu;
 *        Для регистров AD5522_DAC, AD5522_DAC_GAIN, AD5522_DAC_OFFSET - dac;
 *        Для регистров AD5522_SCR - scr;
 *        Для регистров AD5522_ALARM - alarm;
 *        Для регистров AD5522_COMP - comp;
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef AD5522_H_
#define AD5522_H_

#include <stdint.h>
#include "System/events.h"

typedef enum {
    AD5522_PMU         = 0,
    AD5522_DAC         = 1,
    AD5522_DAC_GAIN    = 2,
    AD5522_DAC_OFFSET  = 3,
    AD5522_SCR         = 4,      // Общий для 4х каналов
    AD5522_ALARM       = 5,      // Общий для 4х каналов. Только для чтения!!!
    AD5522_COMP        = 6,      // Общий для 4х каналов. Только для чтения!!!
}ad5522__ch_reg_e;

#pragma pack(push,1)
typedef enum {
    AD5522_CMP_CURRENT  = 0, // compare current function is enabled on the selected channel.
    AD5522_CMP_VOLTAGE  = 1, // compare voltage function is enabled on the selected channel.
}ad5522__cmp_e;
typedef enum {
    AD5522_MEAS_I_SENSE  = 0, // MEASOUTx is connected to ISENSE.
    AD5522_MEAS_V_SENSE  = 1, // MEASOUTx is connected to VSENSE.
    AD5522_MEAS_TEMP     = 2, // MEASOUTx is connected to the temperature sensor.
    AD5522_MEAS_OFF      = 3, // MEASOUTx is high-Z (SW12 open).
}ad5522__meas_e;
typedef enum {
    AD5522_I_5uA         = 0, //
    AD5522_I_20uA        = 1, //
    AD5522_I_200uA       = 2, //
    AD5522_I_2mA         = 3, //
    AD5522_I_EXT         = 4, //
    AD5522_I_DIS         = 5, // Disable the always on mode for the external current range buffer
    AD5522_I_EN          = 6, // Enable the always on mode for the external current range buffer
}ad5522__i_rng_e;
typedef enum {
    AD5522_FORCE_FV      = 0, // FV and current clamp (if clamp is enabled).
    AD5522_FORCE_FI      = 1, // FI and voltage clamp (if clamp is enabled).
    AD5522_FORCE_V_HIZ   = 2, // high-Z FOHx voltage.
    AD5522_FORCE_I_HIZ   = 3, // high-Z FOHx current.
}ad5522__force_e;
typedef struct {
    uint32_t reserved       :5;    //
    uint32_t t_alrm         :1;    // TMPALM
    uint32_t t_alrm_clr     :1;    // LTMPALM
    ad5522__cmp_e cmp       :1;    // Compare V/I
    uint32_t comp_en        :1;    // CPOLH comparator enable
    uint32_t clamp_en       :1;    // CL clamp enable
    uint32_t sense_en       :1;    // SS0
    uint32_t force_en       :1;    // SF0
    uint32_t force_to_dac   :1;    // FIN 0 = input of the force amplifier switched to GND. 1 = input of the force amplifier connected to the FIN DAC output.
    ad5522__meas_e  measout :2;    // MEAS0 MEAS1
    ad5522__i_rng_e i_range :3;    // C0 C1 C2
    uint32_t reserved1      :1;    //
    ad5522__force_e force   :2;    // FORCE0 FORCE1
    uint32_t ch_en          :1;    // Channel enable
}ad5522__pmu_t;

typedef enum {
    AD5522_DAC_REG          = 0,   // Offset DAC X
    AD5522_FIN_I_5uA        = 8,   //
    AD5522_FIN_I_20uA       = 9,   //
    AD5522_FIN_I_200uA      = 10,  //
    AD5522_FIN_I_2mA        = 11,  //
    AD5522_FIN_I_EXT        = 12,  //
    AD5522_FIN_V_RNG        = 13,  //
    AD5522_CLL_I_RNG        = 20,  //
    AD5522_CLL_V_RNG        = 21,  //
    AD5522_CLH_I_RNG        = 28,  //
    AD5522_CLH_V_RNG        = 29,  //
    AD5522_CPL_I_5uA        = 32,  //
    AD5522_CPL_I_20uA       = 33,  //
    AD5522_CPL_I_200uA      = 34,  //
    AD5522_CPL_I_2mA        = 35,  //
    AD5522_CPL_I_EXT        = 36,  //
    AD5522_CPL_V_RNG        = 37,  //
    AD5522_CPH_I_5uA        = 40,  //
    AD5522_CPH_I_20uA       = 41,  //
    AD5522_CPH_I_200uA      = 42,  //
    AD5522_CPH_I_2mA        = 43,  //
    AD5522_CPH_I_EXT        = 44,  //
    AD5522_CPH_V_RNG        = 45,  //
}ad5522__dac_addr_e;
typedef struct {
    uint32_t data           :16;   // Contents of the addressed DAC register (X1, M, or C).
    ad5522__dac_addr_e addr :6;    //
}ad5522__dac_t;

typedef enum {
    AD5522_SHDN_130  = 0,          // Выключение при 130 градусах
    AD5522_SHDN_120  = 1,          // Выключение при 120 градусах
    AD5522_SHDN_110  = 2,          // Выключение при 110 градусах
    AD5522_SHDN_100  = 3,          // Выключение при 100 градусах
}ad5522__t_shdn_e;
typedef struct {
    uint32_t reserved       :2;    //
    uint32_t latched        :1;    //
    ad5522__t_shdn_e t_shdn :2;    //
    uint32_t t_shdn_en      :1;    //
    uint32_t gain           :2;    //
    uint32_t guard_en       :1;    //
    uint32_t int_10k_en     :1;    //
    uint32_t clamp_alrm     :1;    //
    uint32_t guard_alrm     :1;    //
    uint32_t dut_gnd_en     :1;    //
    uint32_t comp_en        :1;    // CPBIASEN
    uint32_t comp0_en       :1;    // CPOLH0
    uint32_t comp1_en       :1;    // CPOLH1
    uint32_t comp2_en       :1;    // CPOLH2
    uint32_t comp3_en       :1;    // CPOLH3
    uint32_t clamp0_en      :1;    // CL0
    uint32_t clamp1_en      :1;    // CL1
    uint32_t clamp2_en      :1;    // CL2
    uint32_t clamp3_en      :1;    // CL3
}ad5522__scr_t;

typedef enum {
    AD5522_UNLATCH       = 1,      //
    AD5522_LATCH         = 2,      //
    AD5522_UNLATCH_LATCH = 3,      //
}ad5522__latch_e;
typedef struct {
    uint32_t reserved       :4;    //
    ad5522__latch_e clamp3  :2;
    ad5522__latch_e clamp2  :2;
    ad5522__latch_e clamp1  :2;
    ad5522__latch_e clamp0  :2;
    ad5522__latch_e guard3  :2;
    ad5522__latch_e guard2  :2;
    ad5522__latch_e guard1  :2;
    ad5522__latch_e guard0  :2;
    ad5522__latch_e temp    :2;
}ad5522__alarm_t;

typedef enum {
    AD5522_COMP_HI     = 1,         //
    AD5522_COMP_LOW    = 2,         //
    AD5522_COMP_HI_LOW = 3,         //
}ad5522__comp_e;
typedef struct {
    uint32_t reserved       :14;    //
    ad5522__comp_e comp3    :2;     //
    ad5522__comp_e comp2    :2;     //
    ad5522__comp_e comp1    :2;     //
    ad5522__comp_e comp0    :2;     //
}ad5522__comp_t;

typedef union {
    ad5522__pmu_t pmu;
    ad5522__dac_t dac;
    ad5522__scr_t scr;
    ad5522__alarm_t alarm;
    ad5522__comp_t comp;
}ad5522__data_t;
#pragma pack(pop)


/***************************************************************************//**
 * @brief Пользовательский коллбэк по завершению операции чтения\записи регистров.
 * @param ch_id - идентификатор канала (0...255).
 * @param event - результат операции.
 * @param reg - тип регистра в канале.
 * @param data - содержимое регистра.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return void
 ******************************************************************************/
typedef void (*ad5522_cb_t)(uint8_t ch_id, events__e event, ad5522__ch_reg_e reg, ad5522__data_t* data, void* ext_data);

/***************************************************************************//**
 * @brief Коллбэк транспортного уровня SPI. Должен быть вызван пользователем по завершении операции чтения\записи
 *        регистров. Вызов должен производиться на потоке cout-ов.
 * @param spi_id - идентификатор интерфейса (0...63).
 * @param event - результат операции.
 * @param buff - указатель на буфер (при чтении).
 * @param len - размер данных, байт.
 * @return void
 ******************************************************************************/
typedef void (*ad5522__hw_cb_t)(uint8_t spi_id, events__e event, uint8_t* buff, int len);

/***************************************************************************//**
 * @brief Указатель на функцию транспортного уровня SPI. Должна быть реализована пользователем библиотеки!
 * @param spi_id - идентификатор интерфейса (0...63). На одном интерфейсе расположены 4 канала.
 * @param tx_buff - указатель на буфер с данными для отправки (может быть NULL).
 * @param tx_len - размер переданных данных, байт.
 * @param rx_buff - указатель на буфер с данными для приема (может быть NULL).
 * @param rx_len - размер принятых данных, байт.
 * @param cb - коллбэк по завершению операции.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
typedef events__e (*ad5522_hw_func_t)(uint8_t spi_id, uint8_t* tx_buff, int tx_len, uint8_t* rx_buff, int rx_len, ad5522__hw_cb_t cb);

/***************************************************************************//**
 * @brief Функция инициализации библиотеки. Должна вызываться вначале программы.
 * @param hw_transport - указатель на функцию транспортного уровня SPI.
 * @return EVENT__OK - операция завершена успешно.
 ******************************************************************************/
events__e ad5522__init(ad5522_hw_func_t hw_transport);

/***************************************************************************//**
 * @brief Функция чтения регистров ad5522.
 * @param сh_id - идентификатор канала (0...255).
 * @param reg - тип регистра в канале.
 * @param data - указатель на буфер для вычитанных данных.
 * @param cb - коллбэк по завершению операции.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
events__e ad5522__read(uint8_t ch_id, ad5522__ch_reg_e reg, ad5522__data_t* data, ad5522_cb_t cb, void* ext_data);

/***************************************************************************//**
 * @brief Функция записи регистров ad5522.
 * @param сh_id - идентификатор канала (0...255).
 * @param reg - тип регистра в канале.
 * @param data - данные для записи.
 * @param cb - коллбэк по завершению операции.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
events__e ad5522__write(uint8_t ch_id, ad5522__ch_reg_e reg, ad5522__data_t* data, ad5522_cb_t cb, void* ext_data);


#endif
