/***************************************************************************//**
 * @file ad7768.h.
 * @brief Модуль, с описанием функций управления микрохемой AD7768.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef AD7768_H_
#define AD7768_H_

#include <stdint.h>
#include "System/events.h"

#pragma pack(push,1)
typedef struct {
    struct {
       uint8_t ch0              :1;
       uint8_t ch1              :1;
       uint8_t ch2              :1;
       uint8_t ch3              :1;
       uint8_t ch4              :1;
       uint8_t ch5              :1;
       uint8_t ch6              :1;
       uint8_t ch7              :1;
    }ch_stdby;                     // 0x00
    struct {
       uint8_t dec_rate         :3;
       uint8_t filter           :1;
    }mode_a;                       // 0x01
    struct {
       uint8_t dec_rate         :3;
       uint8_t filter           :1;
    }mode_b;                       // 0x02
    struct {
       uint8_t ch0              :1;
       uint8_t ch1              :1;
       uint8_t ch2              :1;
       uint8_t ch3              :1;
       uint8_t ch4              :1;
       uint8_t ch5              :1;
       uint8_t ch6              :1;
       uint8_t ch7              :1;
    }ch_mode;                      // 0x03
    struct {
       uint8_t mclk_div         :2;
       uint8_t reserved         :1;
       uint8_t lvds_en          :1;
       uint8_t pwr              :2;
       uint8_t reserved1        :1;
       uint8_t sleep            :1;
    }pwr_mode;                     // 0x04
    struct {
       uint8_t vcm_sel          :2;
       uint8_t reserved         :2;
       uint8_t vcm_pd           :1;
       uint8_t retime_en        :1;
    }cfg;                          // 0x05
    struct {
       uint8_t spi_reset        :2;
       uint8_t reserved         :2;
       uint8_t single_shot_en   :1;
       uint8_t reserved1        :2;
       uint8_t spi_sync         :1;
    }data_ctrl;                    // 0x06
    struct {
       uint8_t dclk_div         :2;
       uint8_t crc_sel          :2;
    }itf_cfg;                      // 0x07
    struct {
       uint8_t ram_start        :1;
    }bist_ctrl;                    // 0x08
    struct {
       uint8_t bist_run         :1;
       uint8_t bist_pass        :1;
       uint8_t clk_err          :1;
       uint8_t chip_err         :1;
    }dev_status;                    // 0x09
    uint8_t revision;               // 0x0A
    uint8_t reserved[3];            // 0x0B
    struct {
       uint8_t dir              :5;
       uint8_t reserved         :2;
       uint8_t ugpio_en         :1;
    }gpio_ctrl;                     // 0x0E
    struct {
       uint8_t data             : 5;
    }gpio_write;                    // 0x0F
    struct {
       uint8_t data             : 5;
    }gpio_read;                     // 0x10
    struct {
        uint8_t ch0_pos_en      : 1;
        uint8_t ch0_neg_en      : 1;
        uint8_t ch1_pos_en      : 1;
        uint8_t ch1_neg_en      : 1;
        uint8_t ch2_pos_en      : 1;
        uint8_t ch2_neg_en      : 1;
        uint8_t ch3_pos_en      : 1;
        uint8_t ch3_neg_en      : 1;
        uint8_t ch4_pos_en      : 1;
        uint8_t ch4_neg_en      : 1;
        uint8_t ch5_pos_en      : 1;
        uint8_t ch5_neg_en      : 1;
        uint8_t ch6_pos_en      : 1;
        uint8_t ch6_neg_en      : 1;
        uint8_t ch7_pos_en      : 1;
        uint8_t ch7_neg_en      : 1;
    }prechrg_buff_en;                // 0x11
    struct {
        uint8_t ch0              :1;
        uint8_t ch1              :1;
        uint8_t ch2              :1;
        uint8_t ch3              :1;
        uint8_t ch4              :1;
        uint8_t ch5              :1;
        uint8_t ch6              :1;
        uint8_t ch7              :1;
    }pos_prechrg_buff;              // 0x13
    struct {
        uint8_t ch0              :1;
        uint8_t ch1              :1;
        uint8_t ch2              :1;
        uint8_t ch3              :1;
        uint8_t ch4              :1;
        uint8_t ch5              :1;
        uint8_t ch6              :1;
        uint8_t ch7              :1;
    }neg_prechrg_buff;              // 0x14
    uint8_t reserved1[9];           // 0x15
    struct {
        uint8_t ch0[3];
        uint8_t ch1[3];
        uint8_t ch2[3];
        uint8_t ch3[3];
        uint8_t ch4[3];
        uint8_t ch5[3];
        uint8_t ch6[3];
        uint8_t ch7[3];
    }offset;                        // 0x1E
    struct {
        uint8_t ch0[3];
        uint8_t ch1[3];
        uint8_t ch2[3];
        uint8_t ch3[3];
        uint8_t ch4[3];
        uint8_t ch5[3];
        uint8_t ch6[3];
        uint8_t ch7[3];
    }gain;                         // 0x36
    uint8_t sync_offset[8];        // 0x4E
    struct {
        uint8_t ch0              :1;
        uint8_t ch1              :1;
        uint8_t ch2              :1;
        uint8_t ch3              :1;
        uint8_t ch4              :1;
        uint8_t ch5              :1;
        uint8_t ch6              :1;
        uint8_t ch7              :1;
    }diag_rx;                      // 0x56
    struct {
        uint8_t grpa_sel         :3;
        uint8_t reserved         :1;
        uint8_t grpb_sel         :1;
    }diag_mux;                      // 0x57
    struct {
        uint8_t reserved         :2;
        uint8_t clk_mod_del_en   :2;
    }mod_delay;                     // 0x58
    struct {
        uint8_t grpb             :2;
        uint8_t grpa             :2;
    }chop_ctrl;                     // 0x59
}ad7768__ch_reg_t;
#pragma pack(pop)

// Макрос для получения адреса регистра AD7768
#define AD7768_REG (uint32_t)&((ad7768__ch_reg_t*)0x00)

/***************************************************************************//**
 * @brief Пользовательский коллбэк по завершению операции чтения\записи регистров.
 * @param id - идентификатор микросхемы (0...255).
 * @param event - результат операции.
 * @param addr - адрес регистра, получаемый при помощи макроса AD7768_REG.
 * @param len - размер данных, байт.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return void
 ******************************************************************************/
typedef void (*ad7768__cb_t)(uint8_t id, events__e event, uint32_t addr, int len, void* ext_data);

/***************************************************************************//**
 * @brief Коллбэк транспортного уровня SPI. Должен быть вызван пользователем по завершении операции чтения\записи
 *        регистров. Вызов должен производиться на потоке cout-ов.
 * @param spi_id - идентификатор интерфейса (0...63).
 * @param event - результат операции.
 * @param buff - указатель на буфер (при чтении).
 * @param len - размер данных, байт.
 * @return void
 ******************************************************************************/
typedef void (*ad7768__hw_cb_t)(uint8_t spi_id, events__e event, uint8_t* buff, int len);

/***************************************************************************//**
 * @brief Указатель на функцию транспортного уровня SPI. Должна быть реализована пользователем библиотеки!
 * @param spi_id - идентификатор интерфейса (0...63). На одном интерфейсе расположены 4 канала.
 * @param tx_buff - указатель на буфер с данными для отправки (может быть NULL).
 * @param tx_len - размер переданных данных, байт.
 * @param rx_buff - указатель на буфер с данными для приема (может быть NULL).
 * @param rx_len - размер принятых данных, байт.
 * @param cb - коллбэк по завершению операции.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
typedef events__e (*ad7768_hw_func_t)(uint8_t spi_id, uint8_t* tx_buff, int tx_len, uint8_t* rx_buff, int rx_len, ad7768__hw_cb_t cb);

/***************************************************************************//**
 * @brief Функция инициализации библиотеки. Должна вызываться вначале программы.
 * @param hw_transport - указатель на функцию транспортного уровня SPI.
 * @return EVENT__OK - операция завершена успешно.
 ******************************************************************************/
events__e ad7768__init(ad7768_hw_func_t hw_transport);

/***************************************************************************//**
 * @brief Функция возвращает указатель на структуру регистров AD7768.
 * @param id - идентификатор микросхемы (0...255).
 * @return ad7768__ch_reg_t* - указатель на структуру.
 ******************************************************************************/
ad7768__ch_reg_t* ad7768__get_regs(uint8_t id);

/***************************************************************************//**
 * @brief Функция чтения регистров ad7768.
 * @param id - идентификатор микросхемы (0...255).
 * @param addr - адрес регистра, получаемый при помощи макроса AD7768_REG.
 * @param len - длина данных, байт.
 * @param cb - коллбэк по завершению операции.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
events__e ad7768__read(uint8_t id, uint32_t addr, int len, ad7768__cb_t cb, void* ext_data);

/***************************************************************************//**
 * @brief Функция записи регистров ad7768.
 * @param сh_id - идентификатор микросхемы (0...255).
 * @param addr - адрес регистра, получаемый при помощи макроса AD7768_REG.
 * @param len - длина данных, байт.
 * @param cb - коллбэк по завершению операции.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
events__e ad7768__write(uint8_t id, uint32_t addr, int len, ad7768__cb_t cb, void* ext_data);


#endif
