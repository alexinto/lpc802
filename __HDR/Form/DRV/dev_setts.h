/*******************************************************************************
 * @file dev_setts.h.
 * @brief Модуль управления секцией настроек в энергонезависимой памяти
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DEV_SETTS_H_
#define DEV_SETTS_H_
#include "System/events.h"
#include "Types/DRV/dev_setts_types.h"
#include "DRV/flash.h"

/**
 * @defgroup GRP_DEV_SETTS Модуль настроек
 * @brief Модуль управления секцией настроек в энергонезависимой памяти
 * @{
 */

typedef void (*dev_setts__cb_t)(uint8_t id, events__e event, dev_setts__params_e param, uint8_t* buff, uint16_t len);

events__e dev_setts__init(uint8_t id, uint8_t dev_id, uint32_t addr, uint32_t size, uint32_t param_max, flash__exec_t func, dev_setts__cb_t cb, void* ext_data);

/*******************************************************************************
 * @brief Функция записи 1го параметра во флеш МК.
 * @param param - паараметр, который хотим записать.
 * @param buff - данные, которые сохраняются в параметре.
 * @param len - длина данных.
 * @return EVENT__OK - запись произведена успешно.
 *         EVENT__PARAM_NA - неверные параметры.
 ******************************************************************************/
events__e dev_setts__set(uint8_t id, dev_setts__params_e param, uint8_t* buff, uint16_t len);

/*******************************************************************************
 * @brief Функция чтения 1го параметра из флеш-памяти МК. При отсутствии параметра вернет 0.
 * @param param - параметр, который хотим считать.
 * @param buff - указатель на буфер.
 * @return uint32_t - длину параметра в байтах.
 ******************************************************************************/
uint32_t dev_setts__get(uint8_t id, dev_setts__params_e param, uint8_t* buff);

/*******************************************************************************
 * @brief Функция очистки параметров. Вызывается 1 раз при производстве изделия
 *        для инициализации flash- памяти.
 ******************************************************************************/
void dev_setts__clear(uint8_t id);

/**
 * @}
 */

#endif

