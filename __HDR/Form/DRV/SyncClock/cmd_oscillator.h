/***************************************************************************//**
 * @file cmd_oscillator.h.
 * @brief Модуль, реализующий управление генераторами частот
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_OSCILLATOR_H_
#define CMD_OSCILLATOR_H_

/**
 * @defgroup GRP_CMD_OSCILLATOR Модуль, реализующий управление генераторами частот
 * @brief
 * @{
 */

#include <stdio.h>
#include "System/events.h"
#include "Types/System/cmd_oscillator_types.h"

#define FREQ_IN_PARAM     8            /*!< Количество настроек частот в 1м параметре флеш-памяти*/

typedef enum {
    SYNCCLOCK__GEN1     = 0,           // Интерфейс 1го генератора HMC7044
    SYNCCLOCK__GEN2     = 1,           // Интерфейс 2го генератора HMC7044
    SYNCCLOCK__SWITCH   = 2,           // Выбор опорной тактовой частоты
    SYNCCLOCK__PULSE    = 3,           // Интерфейс формирования синхроимпульса
    SYNCCLOCK__CC       = 4,           // Интерфейс работы с флеш-памятью

    SYNCCLOCK__ITF_MAX,                // Количество интерфейсов в 1м модуле.
}syncclock__spi_id_e;

typedef enum {
    SYNCCLOCK__OP_WRITE = 0,  // Запись значения по адресу
    SYNCCLOCK__OP_READ  = 1,  // Чтение значения
}syncclock__spi_oper_e;

// Тип структуры для реализации в функции транспортного уровня oscillator__transport_t.
//
typedef union {
    struct {
        uint32_t* gen1;
        uint32_t* gen2;
        uint32_t* sw;
        uint32_t* pulse;
        uint32_t* cc;
    }addr;
    uint32_t* tbl[SYNCCLOCK__ITF_MAX];
}synclock__spi_t;

/***************************************************************************//**
 * @brief Функция транспортного уровня.
 * @param spi_id - идентификатор интерфейса. При использовании synclock__spi_t:
 *                 Экземпляр драйвера = spi_id / SYNCCLOCK__ITF_MAX.
 *                 Номер интерфейса = spi_id % SYNCCLOCK__ITF_MAX.
 * @param oper - запись/чтение.
 * @param addr - адрес регистра.
 * @param data - данные.
 * @param size - размер данных, байт.
 * @return EVENT__OK - операция произведена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
typedef events__e (*oscillator__transport_t)(int spi_id, syncclock__spi_oper_e oper, uint16_t addr, uint8_t* data, int size);

/***************************************************************************//**
 * @brief Функция иницализации модуля. Устанавливает параметры всех генераторов
 *        по- умолчанию. Частота 100Мгц, точность определяется группой OSCIL_SETUP__FDOM1.
 *        0..6 каналы в группе OSCIL_SETUP__FDOM1, 7..13 в группе OSCIL_SETUP__FDOM2.
 * @param oscil_hw_func - пользовательская функция транспортного уровня.
 * @return EVENT__OK - инициализация прошла успешно.
 ******************************************************************************/
events__e cmd_oscillator__init(oscillator__transport_t oscil_hw_func);

/***************************************************************************//**
 * @brief Функция группировки каналов. Назначает каналам группу.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch_mask - маска каналов, для которых назначается группа (group).
 * @param group - группа, в которую входят каналы (ch_mask).
 * @return EVENT__OK - назначение каналов прошло успешно.
 ******************************************************************************/
events__e cmd_oscillator__ch_grp(uint8_t oscil_id, uint32_t ch_mask, cmd_oscillator__ch_id_e group);

/***************************************************************************//**
 * @brief Функция- возвращает указатель на таблицу калибровки.
 * @param oscil_id - идентификатор SyncClock-а.
 * @return param__t* - указатель на таблицу калибровки.
 ******************************************************************************/
cmd_oscillator__pcal_t* cmd_oscillator__get_tbl(uint8_t oscil_id);

/***************************************************************************//**
 * @brief Функция настройки генератора.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param mode -  выбор опорной частоты.
 *                0- опорная частота 100Мгц (SYNC_CLK), по-умолчанию.
 *                1- внешний тактовый сигнал (DUT).
 * @param setup - режим работы генераторов (выбор выходной частоты). \ref cmd_oscillator__setup_e
 * @return EVENT__OK - настройки записаны успешно.
 ******************************************************************************/
events__e cmd_oscillator__set(uint8_t oscil_id, uint8_t mode, cmd_oscillator__setup_e setup);

/***************************************************************************//**
 * @brief Функция возвращает настройки генератора.
 * @param oscil_id - идентификатор SyncClock-а.
 * @return cmd_oscillator__t - текущие настройки.
 ******************************************************************************/
cmd_oscillator__t cmd_oscillator__get(uint8_t oscil_id);

/***************************************************************************//**
 * @brief Функция настройки канала генератора.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @param mode - режим работы канала.
 * @param freq - требуемая частота.
 * @return EVENT__OK - частота установлена.
 *         EVENT__ERROR - ошибка установки частоты.
 ******************************************************************************/
events__e cmd_oscillator__set_ch(uint8_t oscil_id, cmd_oscillator__ch_id_e ch, cmd_oscillator__ch_mode_e mode, uint32_t freq);

/***************************************************************************//**
 * @brief Функция возвращает настройки канала генератора.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @param real_freq - реальная частота канала. Может быть NULL.
 * @return cmd_oscillator__ch_t - текущие настройки канала.
 ******************************************************************************/
cmd_oscillator__ch_t* cmd_oscillator__get_ch(uint8_t oscil_id, cmd_oscillator__ch_id_e ch, uint32_t* real_freq);

/***************************************************************************//**
 * @brief Функция выравнивания фаз генераторов.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param mode - "ступень" синхронизации (1 или 2).
 * @return EVENT__OK - синхронизация прошла успешно.
 ******************************************************************************/
events__e cmd_oscillator__align(uint8_t oscil_id, uint8_t mode);

/***************************************************************************//**
 * @brief Функция формирования синхроимпульса.
 * @param mode - "ступень" синхронизации (1 или 2).
 * @return EVENT__OK - синхронизация прошла успешно.
 ******************************************************************************/
events__e cmd_oscillator__pulse(uint8_t mode);

/***************************************************************************//**
 * @brief Функция возвращает, считанные из микросхемы параметры.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @return cmd_oscillator__pcal_t - считанные параметры.
 ******************************************************************************/
cmd_oscillator__pcal_t  cmd_oscillator__get_pcal(uint8_t oscil_id, cmd_oscillator__ch_id_e ch);

/***************************************************************************//**
 * @brief Функция установки режима работы с калибровочными параметрами.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param mode - 0- нормальный режим работы, 1- отключение калибровочных параметров.
 * @return void.
 ******************************************************************************/
void cmd_oscillator__pcal_mode(uint8_t oscil_id, uint8_t mode);

/***************************************************************************//**
 * @brief Функция установки задержек канала.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param ch - номер канала.
 * @param d_delay - цифровая задержка.
 * @param a_delay - аналоговая задержка.
 * @return void.
 ******************************************************************************/
void cmd_oscillator__ch_delay(uint8_t oscil_id, cmd_oscillator__ch_id_e ch, uint8_t d_delay, uint8_t a_delay);

/***************************************************************************//**
 * @brief Функция возвращает делитель канала.
 * @param oscil_id - идентификатор SyncClock-а.
 * @param grp - группа каналов.
 * @return uint16_t делитель канала.
 ******************************************************************************/
uint16_t oscil_ch_div_get(uint8_t oscil_id, cmd_oscillator__ch_id_e grp);

events__e cmd_oscillator__cc_rd(uint8_t oscil_id, int param, cmd_oscillator__pcal_t* data);
events__e cmd_oscillator__cc_wr(uint8_t oscil_id, int param, cmd_oscillator__pcal_t* data);
uint32_t get_freq(uint32_t freq_num);

events__e oscil_mux_set(uint8_t oscil_id, cmd_oscillator__ch_id_e grp, uint16_t mux_value, uint16_t div_value);

/**
 * @}
 */

#endif
