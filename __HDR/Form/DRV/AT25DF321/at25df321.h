﻿/***************************************************************************//**
 * @file at25df321.с.
 * @brief Описание драйвера микросхемы флеш-памяти AT25DF321.
 *        Поддерживает работу на частоте SPI интерфейса до 50МГц.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef AD25DF321_H_
#define AD25DF321_H_

#include "System/events.h"
#include "DRV/flash.h"


events__e ad25df321__init(int flash_id, uint32_t settings, flash__hw_cb_t cb);

// Точка входа для запуска всех операций
events__e ad25df321__exec(int flash_id, flash__oper_e oper, uint32_t addr, uint8_t *buff, uint32_t len, flash__hw_cb_t cb);


#endif
