/***************************************************************************//**
 * @file HwRxiEthernet.h.
 * @brief Модуль, содержащий описание функций транспортного уровня протокола RxiEthernet.
 *        Функции должны быть реализована пользователем.
 * @author a.tushentsov.
 ******************************************************************************/

#ifndef HWRXIETHERNET_H_
#define HWRXIETHERNET_H_

#include "System/events.h"

/***************************************************************************//**
 * @brief Прототип функции обратного вызова транспортного уровня.
 * @param id - идентификатор экземпляра.
 * @param event - событие модуля.
 * @param buff  - указатель на буфер.
 * @param len  - длина данных.
 * @return EVENT__OK - ошибок нет.
 ******************************************************************************/
typedef void (*HwRxiEthernet__cb_t)(uint8_t id, events__e event, uint8_t* buff, uint32_t len, uint32_t addr, uint32_t port);

events__e HwRxiEthernet__init(uint8_t id, uint32_t addr, uint32_t port);

events__e HwRxiEthernet__rx(uint8_t id, HwRxiEthernet__cb_t cb);

events__e HwRxiEthernet__tx(uint8_t id, uint8_t* buff, int len, uint32_t addr, uint32_t port, HwRxiEthernet__cb_t cb);

events__e HwRxiEthernet__deinit(uint8_t id);

void HwRxiEthernet__cout();

#endif
