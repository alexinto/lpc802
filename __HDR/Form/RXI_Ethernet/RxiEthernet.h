﻿/***************************************************************************//**
 * @file RxiEthernet.h.
 * @brief Модуль, реализующий выборку команд процессора. Обработка команд осуществляется
 * функциями модуля выполнения команд (cmd.c). Функции должны быть реализована пользователем.
 * Модуль имеет встроенную функцию блокировки команд и командного процессора.
 * @author a.tushentsov.
 ******************************************************************************/

#ifndef RXIETHERNET_H_
#define RXIETHERNET_H_
#include "target.h"
#include "System/events.h"
#include "System/RxiTime.h"
#include "System/sw_fifo.h"
#include "Types/RXI_Ethernet/RxiEthernet_types.h"
#include "RXI_Ethernet/HwRxiEthernet.h"

// Константы протокола RXI
#ifndef RXIETHERNET__NUM                 // Количество экземпляров в системе
    #define RXIETHERNET__NUM 1
#endif
#ifndef RXIETHERNET__PROC_NUM            // Количество процессоров в инстансе
    #define RXIETHERNET__PROC_NUM 1
#endif
#ifndef RXIETHERNET_BUFF_RECEIVE
#define RXIETHERNET_BUFF_RECEIVE  1500   // размер приемного буфера
#endif
#ifndef RXIETHERNET_TRANSMIT
#define RXIETHERNET_TRANSMIT      1440   // размер передающего буфера
#endif

typedef enum {
    RXIETNERNET__IO_PROC  = 0,
#if (RXIETHERNET__PROC_NUM > 1)
    RXIETNERNET__CMD_PROC = 1,
#else
    RXIETNERNET__CMD_PROC = 0,
#endif
}RxiEthernet__proc_e;


// Структура команды протокола RXI
#pragma pack(push, 1)
typedef struct {
    uint8_t type;
    uint16_t id;
    uint8_t pkt_part_id;
}RxiEthernet__pkt_answer_hdr_t;
#pragma pack(pop)

// тип структуры служебного хэдера. Для внутреннего использования.
typedef struct {
    uint16_t len;
    uint16_t sock_id;
    uint32_t timestamp;
    uint32_t microseconds;
    RxiEthernet__pkt_answer_hdr_t hdr;    // Хэдер с ID принятого пакета.
}RxiEthernet__hdr_t;


/***************************************************************************//**
 * @brief Функция иницализации модуля. Должна вызываться 1 раз при запуске приложения.
 * @param id - идентификатор экземпляра.
 * @param addr - IP адрес, на который необходимо получать сообщения.
 * @param port - порт, на который необходимо получать сообщения.
 * @return EVENT__OK - успешное выполнение.
 ******************************************************************************/
events__e RxiEthernet__init(uint8_t id, uint32_t addr, uint32_t port);

/***************************************************************************//**
 * @brief Функция инициализации буферов fifo протоколом RXI_EThernet.
 * @param id          - идентификатор экземпляра.
 * @param proc        - идентификатор процессора.
 * @param fifo_cmd    - указатель на буфер FIFO с пакетами команд RXI. Должен быть проинициализирован пользователем.
 * @param fifo_answer - указатель на буфер FIFO для пакетов с ответами. Должен быть проинициализирован пользователем.
 * @return EVENT__OK  - инициализация произведена успешно.
 ******************************************************************************/
events__e RxiEthernet__init_fifo(uint8_t id, RxiEthernet__proc_e proc, fifo__struct_t* fifo_cmd, fifo__struct_t* fifo_answer);

/***************************************************************************//**
 * @brief Функция приема данных протоколом RXI_EThernet.
 *        До вызова этой функции все ethernet пакеты отбрасываются.
 * @param id   - идентификатор экземпляра.
 * @return EVENT__OK - запуск успешен.
 ******************************************************************************/
events__e RxiEthernet__receive(uint8_t id);

/***************************************************************************//**
 * @brief Функция деиницализации модуля. Отключает интерфейс. Обнуляет индексы.
 * @param id - идентификатор интерфейса.
 * @return EVENT__OK - успешное выполнение.
 ******************************************************************************/
events__e RxiEthernet__deinit(uint8_t id);

#endif
