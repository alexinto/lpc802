/***************************************************************************//**
 * @file cmd_tbl_pwr.h.
 * @brief Модуль описания функций управления подсистемой питания.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_PWR_H_
#define CMD_TBL_PWR_H_

#define CMD_TBL_PWR                                                                       \
    {CMD__PWR_MEASURE_V,               cmd__pwr_measure_v,                       },       \
    {CMD__PWR_MEASURE_I,               cmd__pwr_measure_i,                       },       \
    {CMD__PWR_STATUS_GET,              cmd__pwr_status_get,                      },       \
    {CMD__PWR_CTRL,                    cmd__pwr_ctrl,                            },       \

#endif
