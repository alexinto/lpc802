/***************************************************************************//**
 * @file cmd_tbl_pin400.h.
 * @brief Модуль описания функций специфичных для pin400.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_PIN400_H_
#define CMD_TBL_PIN400_H_

#define CMD_TBL_PIN400                                                                    \
    {CMD__PIN400_SETTINGS_WR,     cmd__pin400_settings_wr,                       },       \
    {CMD__PIN400_SETTINGS_RD,     cmd__pin400_settings_rd,                       },       \

#endif
