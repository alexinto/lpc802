/***************************************************************************//**
 * @file cmd_tbl_robo_cal.h.
 * @brief Модуль описания функций управления роботом.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_ROBOCAL_H_
#define CMD_TBL_ROBOCAL_H_

#define CMD_TBL_ROBOCAL                                                            \
    {CMD__ROBOCAL_MOVEXY,     cmd__robocal_movexy,                        },       \

#endif
