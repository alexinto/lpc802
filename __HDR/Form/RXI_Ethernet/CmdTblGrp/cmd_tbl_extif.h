/***************************************************************************//**
 * @file cmd_tbl_extif.h.
 * @brief Модуль описания функций управления роботом.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_EXTIF

#define CMD_TBL_EXTIF                                                              \
    {CMD__EXTIF_SPI_INIT,       cmd__ext_if_spi_init,    },                        \
    {CMD__EXTIF_SPI_TXRX,       cmd__ext_if_spi_txrx,    },                        \
    {CMD__EXTIF_GPIO_SET,       cmd__ext_if_gpio_set,    },                        \
    {CMD__EXTIF_EXT_PWR_CTRL,   cmd__ext_if_pwr_ctrl,    },                        \
    {CMD__EXTIF_I2C_INIT,       cmd__ext_if_i2c_init,    },                        \
    {CMD__EXTIF_I2C_START,      cmd__ext_if_i2c_start,   },                        \
    {CMD__EXTIF_I2C_WRITE,      cmd__ext_if_i2c_write,   },                        \
    {CMD__EXTIF_I2C_READ,       cmd__ext_if_i2c_read,    },                        \
    {CMD__EXTIF_I2C_READN,      cmd__ext_if_i2c_readn,   },                        \
    {CMD__EXTIF_I2C_STOP,       cmd__ext_if_i2c_stop,    },                        \

#endif
