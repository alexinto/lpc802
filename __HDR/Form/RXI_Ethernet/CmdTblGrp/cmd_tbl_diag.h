/***************************************************************************//**
 * @file cmd_tbl_diag.h.
 * @brief Модуль описания функций диагностики командного процессора.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_DIAG_H_
#define CMD_TBL_DIAG_H_

#define CMD_TBL_DIAG                                                                    \
    {CMD__DIAG_DCL,               cmd__diag_dcl,                       },               \
    {CMD__DIAG_DCL_STATE_GET,     cmd__diag_dcl_state_get,             },               \
    {CMD__DIAG_PG,                cmd__diag_pg,                        },               \
    {CMD__DIAG_PG_STATE_GET,      cmd__diag_pg_state_get,              },               \
    {CMD__DIAG_STATIC,            cmd__diag_static,                    },               \
    {CMD__DIAG_STATIC_STATE_GET,  cmd__diag_static_state_get,          },               \
    {CMD__DIAG_DINAMIC,           cmd__diag_dinamic,                   },               \
    {CMD__DIAG_DINAMIC_STATE_GET, cmd__diag_dinamic_state_get,         },               \
    {CMD__DIAG_PMU,               cmd__diag_pmu,                       },               \
    {CMD__DIAG_PMU_STATE_GET,     cmd__diag_pmu_state_get,             },               \
    {CMD__DIAG_MEAS,              cmd__diag_meas,                      },               \
    {CMD__DIAG_MEAS_STATE_GET,    cmd__diag_meas_state_get,            },               \

#endif
