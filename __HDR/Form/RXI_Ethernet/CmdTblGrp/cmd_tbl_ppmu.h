/***************************************************************************//**
 * @file cmd_tbl_ppmu.h.
 * @brief Модуль описания функций управления PPMU.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_PPMU_H_
#define CMD_TBL_PPMU_H_

#define CMD_TBL_PPMU                                                               \
    {CMD__PPMU_CONNECT,           cmd__ppmu_connect,                      },       \
    {CMD__PPMU_IS_CONNECT,        cmd__ppmu_is_connect,                   },       \
    {CMD__PPMU_FN_SET,            cmd__ppmu_fn_set,                       },       \
    {CMD__PPMU_FN_GET,            cmd__ppmu_fn_get,                       },       \
    {CMD__PPMU_FV_SET,            cmd__ppmu_fv_set,                       },       \
    {CMD__PPMU_FV_GET,            cmd__ppmu_fv_get,                       },       \
    {CMD__PPMU_FI_SET,            cmd__ppmu_fi_set,                       },       \
    {CMD__PPMU_FI_GET,            cmd__ppmu_fi_get,                       },       \
    {CMD__PPMU_FR_SET,            cmd__ppmu_fr_set,                       },       \
    {CMD__PPMU_FR_GET,            cmd__ppmu_fr_get,                       },       \
    {CMD__PPMU_MEAS_U,            cmd__ppmu_meas_u,                       },       \
    {CMD__PPMU_MEAS_I,            cmd__ppmu_meas_i,                       },       \

#endif
