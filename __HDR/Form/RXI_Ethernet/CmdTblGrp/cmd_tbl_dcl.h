/***************************************************************************//**
 * @file cmd_tbl_dcl.h.
 * @brief Модуль описания функций DCL командного процессора.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_DCL_H_
#define CMD_TBL_DCL_H_

#define CMD_TBL_DCL                                                              \
    {CMD__DRV_CFG_SET,        cmd__drv_cfg_set,                 },               \
    {CMD__DRV_CFG_GET,        cmd__drv_cfg_get,                 },               \
    {CMD__DRV_ID_GET,         cmd__drv_id_get,                  },               \
    {CMD__DRV_TEMP_GET,       cmd__drv_temp_get,                },               \
    {CMD__DRV_ALD_SET,        cmd__drv_ald_set,                 },               \
    {CMD__DRV_VRNG_SET,       cmd__drv_vrng_set,                },               \
    {CMD__DRV_VRNG_GET,       cmd__drv_vrng_get,                },               \
    {CMD__DRV_VIL_SET,        cmd__drv_vil_set,                 },               \
    {CMD__DRV_VIL_GET,        cmd__drv_vil_get,                 },               \
    {CMD__DRV_VIH_SET,        cmd__drv_vih_set,                 },               \
    {CMD__DRV_VIH_GET,        cmd__drv_vih_get,                 },               \
    {CMD__DRV_TMODE_SET,      cmd__drv_tmode_set,               },               \
    {CMD__DRV_TMODE_GET,      cmd__drv_tmode_get,               },               \
    {CMD__DRV_IOL_SET,        cmd__drv_iol_set,                 },               \
    {CMD__DRV_IOL_GET,        cmd__drv_iol_get,                 },               \
    {CMD__DRV_IOH_SET,        cmd__drv_ioh_set,                 },               \
    {CMD__DRV_IOH_GET,        cmd__drv_ioh_get,                 },               \
    {CMD__DRV_ILOAD_SET,      cmd__drv_iload_set,               },               \
    {CMD__DRV_ILOAD_GET,      cmd__drv_iload_get,               },               \
    {CMD__DRV_ROUT_SET,       cmd__drv_rout_set,                },               \
    {CMD__DRV_ROUT_GET,       cmd__drv_rout_get,                },               \
    {CMD__DRV_ROH_SET,        cmd__drv_roh_set,                 },               \
    {CMD__DRV_ROH_GET,        cmd__drv_roh_get,                 },               \
    {CMD__DRV_ROL_SET,        cmd__drv_rol_set,                 },               \
    {CMD__DRV_ROL_GET,        cmd__drv_rol_get,                 },               \
    {CMD__DRV_RVTT_SET,       cmd__drv_rvtt_set,                },               \
    {CMD__DRV_RVTT_GET,       cmd__drv_rvtt_get,                },               \
    {CMD__DRV_ROUTI_SET,      cmd__drv_routi_set,               },               \
    {CMD__DRV_ROUTI_GET,      cmd__drv_routi_get,               },               \
    {CMD__DRV_RIL_SET,        cmd__drv_ril_set,                 },               \
    {CMD__DRV_RIL_GET,        cmd__drv_ril_get,                 },               \
    {CMD__DRV_RIH_SET,        cmd__drv_rih_set,                 },               \
    {CMD__DRV_RIH_GET,        cmd__drv_rih_get,                 },               \
    {CMD__DRV_RIT_SET,        cmd__drv_rit_set,                 },               \
    {CMD__DRV_RIT_GET,        cmd__drv_rit_get,                 },               \
    {CMD__CMP_SET,            cmd__cmp_set,                     },               \
    {CMD__CMP_GET,            cmd__cmp_get,                     },               \
    {CMD__CMP_RNG_SET,        cmd__cmp_rng_set,                 },               \
    {CMD__CMP_RNG_GET,        cmd__cmp_rng_get,                 },               \
    {CMD__DRV_CPL_SET,        cmd__drv_cpl_set,                 },               \
    {CMD__DRV_CPL_GET,        cmd__drv_cpl_get,                 },               \
    {CMD__DRV_CPH_SET,        cmd__drv_cph_set,                 },               \
    {CMD__DRV_CPH_GET,        cmd__drv_cph_get,                 },               \
    {CMD__DRV_MODE_SET,       cmd__drv_mode_set,                },               \
    {CMD__DRV_MODE_GET,       cmd__drv_mode_get,                },               \
    {CMD__DRV_STATE_SET,      cmd__drv_state_set,               },               \
    {CMD__DRV_STATE_GET,      cmd__drv_state_get,               },               \



#endif
