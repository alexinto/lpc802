/***************************************************************************//**
 * @file cmd_tbl_pg.h.
 * @brief Модуль описания функций управления ГВП.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_PG_H_
#define CMD_TBL_PG_H_

#define CMD_TBL_PG                                                                        \
    {CMD__PG_FC_SET,               cmd__pg_fc_set,                       },               \
    {CMD__PG_FC_GET,               cmd__pg_fc_get,                       },               \
    {CMD__PG_ADDR_SET,             cmd__pg_addr_set,                     },               \
    {CMD__PG_ADDR_GET,             cmd__pg_addr_get,                     },               \
    {CMD__PG_PATTERN_SET,          cmd__pg_pattern_set,                  },               \
    {CMD__PG_PATTERN_GET,          cmd__pg_pattern_get,                  },               \
    {CMD__PG_FLAG_SET,             cmd__pg_flag_set,                     },               \
    {CMD__PG_FLAG_GET,             cmd__pg_flag_get,                     },               \
    {CMD__PG_STATE_GET,            cmd__pg_state_get,                    },               \
    {CMD__PG_BREAK_SET,            cmd__pg_break_set,                    },               \
    {CMD__PG_BREAK_GET,            cmd__pg_break_get,                    },               \
    {CMD__PG_FORMAT_SET,           cmd__pg_format_set,                   },               \
    {CMD__PG_FORMAT_GET,           cmd__pg_format_get,                   },               \
    {CMD__PG_CMP_SET,              cmd__pg_cmp_set,                      },               \
    {CMD__PG_CMP_GET,              cmd__pg_cmp_get,                      },               \
    {CMD__PG_MUX_MODE_SET,         cmd__pg_mux_mode_set,                 },               \
    {CMD__PG_MUX_MODE_GET,         cmd__pg_mux_mode_get,                 },               \
    {CMD__PG_DIFF_MODE_SET,        cmd__pg_diff_mode_set,                },               \
    {CMD__PG_DIFF_MODE_GET,        cmd__pg_diff_mode_get,                },               \
    {CMD__PG_ERR_RAM_MODE_SET,     cmd__pg_err_ram_mode_set,             },               \
    {CMD__PG_ERR_RAM_MODE_GET,     cmd__pg_err_ram_mode_get,             },               \
    {CMD__PG_FIRST_ERR_CLK_GET,    cmd__pg_first_err_clk_get,            },               \
    {CMD__PG_ERR_RAM_NUM_GET,      cmd__pg_err_ram_num_get,              },               \

#endif
