/***************************************************************************//**
 * @file cmd_tbl_addclock.h.
 * @brief Модуль описания функций мезонина AddClock.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_ADDCLOCK

#define CMD_TBL_ADDCLOCK                                                                      \
    {CMD__ADDCLOCK_SETTINGS_WR,        cmd__addclock_sett_wr                 },               \
    {CMD__ADDCLOCK_SETTINGS_RD,        cmd__addclock_sett_rd                 },               \
    {CMD__ADDCLOCK_SIN_SET,            cmd__addclock_sin_set                 },               \
    {CMD__ADDCLOCK_TRIG_CFG,           cmd__addclock_trig_cfg                },               \
    {CMD__ADDCLOCK_RECT_SET,           cmd__addclock_rect_set                },               \
    {CMD__ADDCLOCK_SYNC_SET,           cmd__addclock_sync_set                },               \
    {CMD__ADDCLOCK_GEN_CTRL,           cmd__addclock_gen_ctrl                },               \
    {CMD__ADDCLOCK_TEST,               cmd__addclock_test                    },               \



#endif
