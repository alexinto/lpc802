/***************************************************************************//**
 * @file cmd_awg2m.h.
 * @brief Модуль описания функций управления AWG2M.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_AWG2M_H_
#define CMD_TBL_AWG2M_H_

#define CMD_TBL_AWG2M                                                                           \
    {CMD__AWG2M_CH_PART_GET,            cmd__awg2m_ch_part_get,                        },       \
    {CMD__AWG2M_CH_IMG_GET,             cmd__awg2m_ch_img_get,                         },       \
    {CMD__AWG2M_CH_IMG_SET,             cmd__awg2m_ch_img_set,                         },       \
    {CMD__AWG2M_SYNCCLK_MUX_DIV_SET,    cmd__awg2m_syncclk_mux_div_set,                },       \
    {CMD__AWG2M_SYNCCLK_MUX_DIV_GET,    cmd__awg2m_syncclk_mux_div_get,                },       \
    {CMD__AWG2M_FPGA_MUX_DIV_SET,       cmd__awg2m_fpga_mux_div_set,                   },       \
    {CMD__AWG2M_FPGA_MUX_DIV_GET,       cmd__awg2m_fpga_mux_div_get,                   },       \
    {CMD__AWG2M_DAC_CLK_CHANGE_SET,     cmd__awg2m_dac_clk_change_set,                 },       \
    {CMD__AWG2M_DAC_CLK_CHANGE_GET,     cmd__awg2m_dac_clk_change_get,                 },       \
    {CMD__AWG2M_CH_CTRL,                cmd__awg2m_ch_ctrl,                            },       \
    {CMD__AWG2M_CH_DIFF_MODE_SET,       cmd__awg2m_ch_diff_mode_set,                   },       \
    {CMD__AWG2M_CH_DIFF_MODE_GET,       cmd__awg2m_ch_diff_mode_get,                   },       \
    {CMD__AWG2M_CH_RES_SET,             cmd__awg2m_ch_res_set,                         },       \
    {CMD__AWG2M_CH_RES_GET,             cmd__awg2m_ch_res_get,                         },       \
    {CMD__AWG2M_CH_V_OFFSET_SET,        cmd__awg2m_ch_v_offset_set,                    },       \
    {CMD__AWG2M_CH_V_OFFSET_GET,        cmd__awg2m_ch_v_offset_get,                    },       \
    {CMD__AWG2M_CH_RANGE_SET,           cmd__awg2m_ch_range_set,                       },       \
    {CMD__AWG2M_CH_RANGE_GET,           cmd__awg2m_ch_range_get,                       },       \
    {CMD__AWG2M_CH_DAC_CHG_SET,         cmd__awg2m_ch_dac_chg_set,                     },       \
    {CMD__AWG2M_CH_DAC_CHG_GET,         cmd__awg2m_ch_dac_chg_get,                     },       \
    {CMD__AWG2M_CH_LPF_SET,             cmd__awg2m_ch_lfp_set,                         },       \
    {CMD__AWG2M_CH_LPF_GET,             cmd__awg2m_ch_lfp_get,                         },       \
    {CMD__AWG2M_DAC_WR,                 cmd__awg2m_dac_wr,                             },       \
    {CMD__AWG2M_DAC_RD,                 cmd__awg2m_dac_rd,                             },       \
    {CMD__AWG2M_IMG_CTRL_SET,           cmd__awg2m_img_ctrl_set,                       },       \
    {CMD__AWG2M_IMG_CTRL_GET,           cmd__awg2m_img_ctrl_get,                       },       \
    {CMD__AWG2M_DDS_CTRL_SET,           cmd__awg2m_dds_ctrl_set,                       },       \
    {CMD__AWG2M_DDS_CTRL_GET,           cmd__awg2m_dds_ctrl_get,                       },       \
    {CMD__AWG2M_CFG_SET,                cmd__awg2m_cfg_set,                            },       \
    {CMD__AWG2M_CFG_GET,                cmd__awg2m_cfg_get,                            },       \
    {CMD__AWG2M_START,                  cmd__awg2m_start,                              },       \
    {CMD__AWG2M_STOP,                   cmd__awg2m_stop,                               },       \
    {CMD__AWG2M_IO_DIAG,                cmd__awg2m_io_diag,                            },       \
    {CMD__AWG2M_ACC_CTRL_SET,           cmd__awg2m_acc_ctrl_set,                       },       \
    {CMD__AWG2M_ACC_CTRL_GET,           cmd__awg2m_acc_ctrl_get,                       },       \
    {CMD__AWG2M_ACC_RANGE_SET,          cmd__awg2m_acc_range_set,                      },       \
    {CMD__AWG2M_ACC_RANGE_GET,          cmd__awg2m_acc_range_get,                      },       \
    {CMD__AWG2M_ACC_MEASURE,            cmd__awg2m_acc_measure,                        },       \
    {CMD__AWG2M_PWR_DIAG,               cmd__awg2m_pwr_diag,                           },       \
    {CMD__AWG2M_UNIT_DIAG,              cmd__awg2m_unit_diag,                          },       \
    {CMD__AWG2M_DC_DIAG,                cmd__awg2m_dc_diag,                            },       \
    {CMD__AWG2M_DC_DIAG_MEAS,           cmd__awg2m_dc_diag_meas,                       },       \
    {CMD__AWG2M_FAST_DIAG,              cmd__awg2m_fast_diag,                          },       \
    {CMD__AWG2M_ACC_TUNE,               cmd__awg2m_acc_tune,                           },       \
    {CMD__AWG2M_CH_TUNE,                cmd__awg2m_ch_tune,                            },       \
    {CMD__AWG2M_CH_CFG2_SET,            cmd__awg2m_ch_cfg2_set,                        },       \
    {CMD__AWG2M_CH_CFG2_GET,            cmd__awg2m_ch_cfg2_get,                        },       \
    {CMD__AWG2M_CH_FIX2,                cmd__awg2m_ch_fix2,                            },       \
    {CMD__AWG2M_CH_CTRL_GET,            cmd__awg2m_ch_ctrl_get,                        },       \


#endif
