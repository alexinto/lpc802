/**
 * @file cmd_tbl_dps.h
 * @author Alexander Tolstunov (tolstunov@form.ru)
 * @brief 
 * @version 0.1
 * @date 2023-02-16
 * 
 * @copyright Copyright (c) 2023
 * 
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _CMD_TBL_DPS_H_
#define _CMD_TBL_DPS_H_

#define CMD_TBL_DPS                                                     \
    {CMD__DPS_FV,               cmd__dps_fv,                        },  \
    {CMD__DPS_FN,               cmd__dps_fn,                        },  \
    {CMD__DPS_MV,               cmd__dps_mv,                        },  \
    {CMD__DPS_SET_PERF_MODE,    cmd__dps_set_perf_mode,             },  \
    {CMD__DPS_SET_LOAD,         cmd__dps_set_load,                  },  \
    {CMD__DPS_GET_PERF_MODE,    cmd__dps_get_perf_mode,             },  \
    {CMD__DPS_GET_MODE,         cmd__dps_get_mode,                  },  \
    {CMD__DPS_RANGEI,           cmd__dps_rangei,                    },  \
    {CMD__DPS_SLEW_RATE,        cmd__dps_slew_rate,                 },  \
    {CMD__DPS_GET_U,            cmd__dps_get_u,                     },  \
    {CMD__DPS_GET_LOAD,         cmd__dps_get_load,                  },  \
    {CMD__DPS_GET_ICLAMP_HI,    cmd__dps_get_iclamp_hi,             },  \
    {CMD__DPS_GET_ICLAMP_LO,    cmd__dps_get_iclamp_lo,             },  \
    {CMD__DPS_GET_ICLAMP_STATE, cmd__dps_get_iclamp_state,          },  \
    {CMD__DPS_MEAS_U,           cmd__dps_meas_u,                    },  \
    {CMD__DPS_MEAS_I,           cmd__dps_meas_i,                    },  \
    {CMD__DPS_SET_VOLTAGE,      cmd__dps_set_voltage,               },  \
    {CMD__DPS_SET_IRANGE,       cmd__dps_set_irange,                },  \
    {CMD__DPS_SET_ICLAMP_HI,    cmd__dps_set_iclamp_hi,             },  \
    {CMD__DPS_SET_ICLAMP_LO,    cmd__dps_set_iclamp_lo,             },  \
    {CMD__DPS_SET_SLEW,         cmd__dps_set_slew,                  },  \

#endif /* _CMD_TBL_DPS_H_ */
