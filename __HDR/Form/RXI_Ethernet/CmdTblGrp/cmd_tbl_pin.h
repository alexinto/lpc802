/***************************************************************************//**
 * @file cmd_tbl_pin.h.
 * @brief Модуль описания функций управления внешними выводами.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef CMD_TBL_PIN_H_
#define CMD_TBL_PIN_H_

#define CMD_TBL_PIN                                                             \
    {CMD__PIN_INIT,               cmd__pin_init,                       },       \
    {CMD__PIN_RESET,              cmd__pin_reset,                      },       \

#endif
