#ifndef COMMONCOMMAND_H_
#define COMMONCOMMAND_H_

#include <stdint.h>
#include "Types/Rxi_Ethernet/RxiEthernet_types.h"
#include "CmdGroup/SystemCommands.h"
#include "CmdGroup/HwCommands.h"
#include "CmdGroup/JournalCommands.h"
#include "CmdGroup/ScriptCommands.h"
#include "CmdGroup/TriggerCommands.h"
#include "CmdGroup/SyncClockCommands.h"
#include "CmdGroup/ExtIfCommands.h"
#include "CmdGroup/AddClockCommands.h"
#include "CmdGroup/PSPCommands.h"
#include "CmdGroup/MultisiteCommands.h"
#include "CmdGroup/EmulCommands.h"
#include "CmdGroup/FirmwareCommands.h"
#include "CmdGroup/TestCommands.h"
#include "CmdGroup/PGCommands.h"
#include "CmdGroup/PinCommands.h"
#include "CmdGroup/DCLCommands.h"
#include "CmdGroup/PPMUCommands.h"
#include "CmdGroup/PWRCommands.h"
#include "CmdGroup/DiagCommands.h"

#include "CmdGroup/RoboCal.h"

#include "CmdGroup/Ms20gtModule.h"
#include "CmdGroup/SyncmmModule.h"
#include "CmdGroup/Pin400Module.h"
#include "CmdGroup/DPSModuleCommands.h"
#include "CmdGroup/AWG2M.h"

#pragma pack(push, 1)


/**
 * \defgroup ModulesGroup Модули
 */

/**
 * \defgroup GRP_COMMANDS Группы команд
 */

/**
 * \defgroup IO_Commands Команды интерфейсного процессора
 * \ingroup GRP_COMMANDS
 */

/**
 * \defgroup CMD_Commands Команды командного процессора
 * \ingroup GRP_COMMANDS
 */

/**
 * \defgroup HwCommands Команды низкоуровневого доступа
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule Ms20gtModule PspModule Pin400Module AWGModule DPSModule
 */

/**
 * \defgroup SystemCommands Системные настройки
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule Ms20gtModule PspModule Pin400Module
 */

/**
 * \defgroup JournalCommands Запись-чтение журнала
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule PspModule Pin400Module
 */

/**
 * \defgroup ScriptCommands Загрузка/удаление/выполнение скриптов
 * \ingroup GRP_COMMANDS SyncmmModule
 */

/**
 * \defgroup TriggerCommands Настройка и управление триггерными сигналами
 * \ingroup GRP_COMMANDS SyncmmModule
 */

/**
 * \defgroup SyncClockCommands Настройка SYNC_CLOCK
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule Pin400Module
 */

/**
 * \defgroup ExtIfCommands Группа команд управления ExtIf
 * \ingroup GRP_COMMANDS Sync1200Module
 * \brief Позволяет управлять интерфейсами I2C, SPI, выводами общего назначения,
 *  а также дополнительными источниками питания мезонина Ext If.
 */

/**
 * \defgroup AddClockCommands Группа команд управления AddClock
 * \ingroup GRP_COMMANDS Sync1200Module
 */

/**
 * \defgroup PSPCommands Группа команд управления подсистемой питания
 * \ingroup GRP_COMMANDS PspModule
 */

/**
 * \defgroup MultisiteCommands Настройка и управление мультисайта
 * \ingroup GRP_COMMANDS Sync1200Module SyncmmModule Pin400Module
 */

/**
 * \defgroup EmulCommands Настройка и управление эмуляторами ГТП
 * \ingroup GRP_COMMANDS SyncmmModule
 */

/**
 * \defgroup PGCommands Настройка и управление ГТП
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup PinCommands Настройка внешних выводов (PIN-ов)
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup FirmwareCommands Группа команд работы с прошивками
 * \ingroup GRP_COMMANDS SyncmmModule Ms20gtModule Sync1200Module PspModule Pin400Module
 */

/**
 * \defgroup TestCommands Тестовые команды.
 * \ingroup GRP_COMMANDS SyncmmModule
 * \brief Выполняются только в тестовом режиме \ref CMD__OP_MODE_SET
 */

/**
 * \defgroup DCLCommands Группа команд управления устройствами подсистемы канальной электроники «DCL Control»
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup PPMUCommands Группа команд управления PPMU
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup PWRCommands Группа команд управления питанием PWR
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup DiagCommands Группа команд диагностики
 * \ingroup GRP_COMMANDS Pin400Module
 */

/**
 * \defgroup RoboCommands Группа команд управления роботом
 * \ingroup GRP_COMMANDS
 */



/**
 * \defgroup MeasurementModule Измерительные модули
 * \ingroup ModulesGroup
 */

/**
 * \defgroup SystemModule Системные модули
 * \ingroup ModulesGroup
 */

/**
 * \defgroup SyncmmModule SyncMM
 * \ingroup MeasurementModule
 */

/**
 * \defgroup Pin400Module Pin400M
 * \ingroup MeasurementModule
 */

/**
 * \defgroup Ms20gtModule MS20GT
 * \ingroup SystemModule
 */

/**
 * \defgroup Sync1200Module Sync1200
 * \ingroup SystemModule
 */

/**
 * \defgroup PspModule Подсистема питания
 * \ingroup SystemModule
 */

/**
 * \defgroup DPSModule DPS
 * \ingroup MeasurementModule
 */

/**
 * \defgroup AWGModule AWG2M
 * \ingroup MeasurementModule
 */
















#pragma pack(pop)

#endif
