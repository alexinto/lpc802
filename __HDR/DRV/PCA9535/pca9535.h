/***************************************************************************//**
 * @file pca9535.c.
 * @brief Драйвер PCA9535.
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef PCA9535_H_
#define PCA9535_H_

#include "System/events.h"

typedef enum {
    PCA9535__INPUT       = 0,
    PCA9535__OUTPUT      = 2,
    PCA9535__INVERSION   = 4,
    PCA9535__CONFIG      = 6,
}pca9535__reg_e;

typedef void (*pca9535__cb_t)(i32 id, events__e event, pca9535__reg_e reg, u16 data, void* ext_data);

events__e pca9535__init(i32 id, i32 i2c_id, u8 addr, pca9535__cb_t cb, void* ext_data);


events__e pca9535__read(i32 id, pca9535__reg_e reg, u16* data, pca9535__cb_t cb, void* ext_data);

events__e pca9535__write(i32 id, pca9535__reg_e reg, u16 data, pca9535__cb_t cb, void* ext_data);



#endif
