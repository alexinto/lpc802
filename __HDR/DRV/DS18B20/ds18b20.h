﻿/***************************************************************************//**
 * @file ds18b20.c.
 * @brief  Драйвер микросхемы DS18B20.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef DS18B20_H_
#define DS18B20_H_

#include "System/fstandard.h"
#include "System/events.h"
#include "DRV/onewire.h"

typedef void (*ds18b20__cb_t)(u8 id, events__e event, float temp, void* ext_data);


/***************************************************************************//**
 * @brief Функция- инициализации модуля DS18B20.
 * @param id - идентификатор микросхемы.
 * @param onewire_id - идентификатор интерфейса 1wire.
 * @return EVENT_OK - успешное выполнение.
 *         EVENT_ERROR - ошибка инициализации.
 ******************************************************************************/
events__e ds18b20__init(u8 id, u8 onewire_id);

/***************************************************************************//**
 * @brief Функция- преобразования DS18B20. Запускает преобразование и считывает
          температуру. Значение температуры можно получить при помощи функции ds18b20__get_temp().
 * @param id - идентификатор микросхемы.
 * @param cb - коллбэк окончания операции.
 * @param ext_data - пользовательские данные.
 * @return EVENT_OK - успешное выполнение.
 *         EVENT_ERROR - ошибка инициализации.
 ******************************************************************************/
events__e ds18b20__meas_temp(u8 id, ds18b20__cb_t cb, void* ext_data);

/***************************************************************************//**
 * @brief Возвращает температуру. Результат последнего преобразования ds18b20__meas_temp().
 * @param id - идентификатор микросхемы.
 * @return float - значение температуры, C.
 ******************************************************************************/
float ds18b20__get_temp(u8 id);

/***************************************************************************//**
 * @brief Возвращает валидность последнего результата преобразования ds18b20__meas_temp().
 * @param id - идентификатор микросхемы.
 * @return u8 - 1 - данные валидны
 *              0 - ошибка CRC
 ******************************************************************************/
u8 ds18b20__is_valid(u8 id);

/***************************************************************************//**
 * @brief Возвращает UUID микросхемы.
 * @param id - идентификатор микросхемы.
 * @return uint64_t - текущий UUID.
 *         0 - UUID отсутствует.
 ******************************************************************************/
uint64_t ds18b20__get_uuid(u8 id);

/***************************************************************************//**
 * @brief Устанавливает UUID микросхемы.
 * @param id - идентификатор микросхемы.
 * @param uuid - если 0, то работа с одним устройством. Этап адресации (ROM) пропускается.
 * @return EVENT_OK - успешное выполнение.
 ******************************************************************************/
events__e ds18b20__set_uuid(u8 id, u64 uuid);

/***************************************************************************//**
 * @brief Устанавливает режим преобразования. Параметр записывается в энергонезависимую память
 *        последующим вызовом функции ds18b20__meas_temp().
 * @param id - идентификатор микросхемы.
 * @param mode - режим преобразования:
 *               0 - 9-bit resolution
 *               1 - 10-bit resolution
 *               2 - 11-bit resolution
 *               3 - 12-bit resolution
 * @return EVENT_OK - успешное выполнение.
 ******************************************************************************/
events__e ds18b20__set_mode(u8 id, u8 mode);

#endif /* DS18B20_H_ */
