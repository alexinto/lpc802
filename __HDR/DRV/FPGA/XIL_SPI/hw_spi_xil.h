﻿/***************************************************************************//**
 * @file hw_spi_xil.h.
 * @brief Модуль аппаратного SPI IP-core Xilinx.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef HW_SPI_XIL_H_
#define HW_SPI_XIL_H_

#include "System/events.h"
#include "DRV/hw_spi.h"

events__e hw_spi_xil__func(hw_spi__cmd_e cmd, u8 spi_id, u8 cs_id, u32 sett_wr, u32 sett_rd, u8 *rx_buff, u32 rx_len, u8 *tx_buff, u32 tx_len, hw_spi__cb_t callback, void* ext_data);


#endif /* HW_SPI_H_ */
