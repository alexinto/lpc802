﻿/***************************************************************************//**
 * @file freg.h.
 * @brief Модуль, реализующий работу с регистрами ПЛИС. Запись одного регистра ПЛИС при
 *        помощи макроса FREG_SET занимает порядка 2мск (частота microblaze 100 Мгц).
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef FREG_H_
#define FREG_H_
#include "target.h"
#include "System/fstandard.h"

#ifdef FREG__DEBUG_EN
#include "System/fdebug.h"
#endif

extern volatile u32 freg__temp, freg__size;
#ifdef FREG__DEBUG_EN

// Чтение значения из регистра ПЛИС. reg - регистр (размер не более 4х байт), а не его адрес!
#define FREG_GET(reg) freg__temp = ((*((u32*)((u32)((u8*)&(reg)) & (~0x03))) >> (((u32)((u8*)&(reg)) & 0x03) * 8)) &  \
                      (0xFFFFFFFF >> ((4 - sizeof(reg)) * 8)));                                                                                \
                      FDEBUG(FDEBUG__LVL7, "AXI 0x%x -> 0x%x\n\r", (u32)(&(reg)) & (~0x03), freg__temp)

// Запись значения в регистр ПЛИС. reg - регистр (размер не более 4х байт), а не его адрес, может быть не выровнен по 4 байтам. value - значение
#define FREG_SET(reg, value)  do {                                                                                                      \
                                  freg__size = sizeof(value) > sizeof(reg) ? sizeof(reg) : sizeof(value);                               \
                                  if (freg__size > 4) freg__size = 4;                                                                   \
                                  memcpy(&freg__temp, (void*)&value, freg__size);                                                       \
                                  freg__temp = (*(u32*)((u32)(&(reg)) & (~0x03)) &                                            \
                                  (~((0xFFFFFFFF >> ((4 - freg__size) * 8)) << (((u32)(&(reg)) & 0x03) * 8)))) |                   \
                                  (((freg__temp) & (0xFFFFFFFF >> ((4 - freg__size) * 8))) << (((u32)(&(reg)) & 0x03) * 8));       \
                                  *(u32*)((u32)(&(reg)) & (~0x03)) = freg__temp;                                              \
                                  FDEBUG(FDEBUG__LVL7, "AXI 0x%x <- 0x%x\n\r", (u32)(&(reg)) & (~0x03), freg__temp);               \
                              } while(0)

#else

// Чтение значения из регистра ПЛИС. reg - регистр (размер не более 4х байт), а не его адрес!
#define FREG_GET(reg) ((*((volatile u32*)((u32)((u8*)&(reg)) & (~0x03))) >> (((u32)((u8*)&(reg)) & 0x03) * 8)) &        \
                      (0xFFFFFFFF >> ((4 - sizeof(reg)) * 8)))

// Запись значения в регистр ПЛИС. reg - регистр (размер не более 4х байт), а не его адрес, может быть не выровнен по 4 байтам. value - значение
#define FREG_SET(reg, value)  do {                                                                                                      \
                                  freg__size = sizeof(value) > sizeof(reg) ? sizeof(reg) : sizeof(value);                               \
                                  if (freg__size > 4) freg__size = 4;                                                                   \
                                  memcpy(&freg__temp, (void*)&(value), freg__size);                                                              \
                                  *(volatile u32*)((u32)(&(reg)) & (~0x03)) = (*(volatile u32*)((u32)(&(reg)) & (~0x03)) &            \
                                  (~((0xFFFFFFFF >> ((4 - freg__size) * 8)) << (((u32)(&(reg)) & 0x03) * 8)))) |                   \
                                  (((freg__temp) & (0xFFFFFFFF >> ((4 - freg__size) * 8))) << (((u32)(&(reg)) & 0x03) * 8));       \
                              } while(0)



#endif /* FREG__DEBUG_EN */


#endif /* FREG_H_ */
