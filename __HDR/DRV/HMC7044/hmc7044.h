﻿/***************************************************************************//**
 * @file hmc7044.h.
 * @brief Драйвер микросхемы hmc7044.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef HMC7044_H_
#define HMC7044_H_
#include "System/events.h"
#include "DRV/spi.h"

#pragma pack(push,1)
typedef struct {
    u8 channel;                      // "High performance mode", "SYNC enable", "Slip enable", "Start-Up Mode[1:0]", "Multislip enable", "Channel enable"
    u8 ch_div[2];                    // "12-Bit Channel Divider"
    u8 a_delay;                      // "Fine Analog Delay"
    u8 d_delay;                      // "Coarse Digital Delay"
    u8 multi_delay[2];               // "12-Bit Multislip Digital Delay"
    u8 mux_mode;                     // "Output Mux Selection"
    u8 driver_mode;                  // "Force Mute", "Dynamic driver enable", "Driver Mode", "Driver Impedance"
    u8 zreserv1;
}hmc7044__out_struct_t;

typedef struct {
    u8 soft_reset;                    // 0x00 "Global soft reset control"
    u8 mode_ctrl[2];                  // 0x01 "Global request and mode control"
    u8 en_ctrl[2];                    // 0x03 "Global enable control"
    u8 glob_mode;                     // 0x05 "Global mode and enable control"
    u8 clr_alarms;                    // 0x06 "Global clear alarms"
    u8 zreserv[3];
    u8 clk_in_buff[4];                // 0x0A "CLKIN0 input buffer control", "CLKIN1 input buffer control", "CLKIN2 input buffer control", "CLKIN3 input buffer control"
    u8 oscin_buf_ctrl;                // 0x0E "OSCIN/OSCIN input buffer control"
    u8 zreserv1[5];
    u8 pll1_ctrl[8];                  // 0x14 "PLL1 reference priority control", "PLL1 loss of signal (LOS) control", "PLL1 holdover exit control", "PLL1 holdover DAC/ADC control", "PLL1 LOS mode control", "PLL1 charge pump control", "PLL1 PFD control"
    u8 clk_in_psc[4];                 // 0x1C "CLKIN0 input prescaler control", "CLKIN1 input prescaler control", "CLKIN2 input prescaler control", "CLKIN3 input prescaler control"
    u8 oscin_psc;                     // 0x20 "OSCIN/OSCIN Input prescaler control"
    u8 pll1_r1[2];                    // 0x21 "PLL1 reference divider control (R1)"
    u8 zreserv2[3];
    u8 pll1_n1[2];                    // 0x26 "PLL1 feedback divider control (N1)"
    u8 pll1_lock_det;                 // 0x28 "PLL1 lock detect control"
    u8 pll1_ref_sw;                   // 0x29 "PLL1 reference switching control"
    u8 pll1_holdoff;                  // 0x2A "PLL1 holdoff time control"
    u8 zreserv3[7];
    u8 pll2_doubler;                  // 0x32 "PLL2 frequency doubler control"
    u8 pll2_r2[2];                    // 0x33 "PLL2 reference divider control (R2)"
    u8 pll2_n2[2];                    // 0x35 "PLL2 feedback divider control (N2)"
    u8 pll2_charge_pump;              // 0x37 "PLL2 charge pump control"
    u8 pll2_pfd;                      // 0x38 "PLL2 PFD control"
    u8 oscout_path;                   // 0x39 "OSCOUTx path control"
    u8 oscout_drv[2];                 // 0x3A "OSCOUTx driver control"
    u8 zreserv4[10];
    u8 gpi_ctrl[10];                  // 0x46 "GPI1 control", "GPI2 control", "GPI3 control", "GPI4 control"
    u8 gpo_ctrl[4];                   // 0x50 "GPO1 control", "GPO2 control", "GPO3 control", "GPO4 control"
    u8 sdata;                         // 0x54 "SDATA control"
    u8 zreserv5[5];
    u8 pulse_gen;                     // 0x5A "Pulse generator control"
    u8 sync;                          // 0x5B "SYNC control"
    u8 sysref_tmr[2];                 // 0x5C "SYSREF timer control"
    u8 zreserv6[6];
    u8 vco_ctrl;                      // 0x64 "External VCO control"
    u8 analog_delay;                  // 0x65 "Analog delay common control"
    u8 zreserv7[10];
    u8 pll1_alarm_ctrl;               // 0x70 "PLL1 alarm mask control"
    u8 alarm_mask;                    // 0x71 "Alarm mask control"
    u8 zreserv8[6];
    u8 id[3];                         // 0x78 "Product ID"
    u8 alarm_sig;                     // 0x7B "Readback register"
    u8 pll1_alarm;                    // 0x7C "PLL1 alarm readback"
    u8 alarm_read[3];                 // 0x7D "Alarm readback", "Latched alarm readback", "Alarm readback miscellaneous"
    u8 zreserv9[2];
    u8 pll1_stat[6];                  // 0x82 "PLL1 status registers"
    u8 zreserv10[4];
    u8 pll2_stat[5];                  // 0x8C "PLL2 status registers"
    u8 sysref_stat;                   // 0x91 "SYSREF status register"
    u8 zreserv11[13];
    u8 clk_out_drv_pw[2];             // 0x9F "Clock output driver power setting"
    u32 zreserv12;
    u8 pll1_delay;                    // 0xA5 "PLL1 more delay"
    u16 zreserv13;
    u8 pll1_holdover;                 // 0xA8 "PLL1 holdover DAC gm setting"
    u8 zreserv14[7];
    u8 vtune_preset;                  // 0xB0 "VTUNE preset setting"
    u8 zreserv15;
    u8 bank_cap;                      // 0xB2 "Выбор конденсаторов"
    u8 zreserv16[21];
    hmc7044__out_struct_t out_ctrl[14];    // 0xC8 "Channel Output 0-13 control"
}hmc7044__reg_struct_t;

#pragma pack(pop)
// Макрос для получения адреса регистра HMC7044
#define HMC7044_REG (u32)&((hmc7044__reg_struct_t*)0x00)

/***************************************************************************//**
 * @brief Функция инициализации модуля.
 * @param id - идентификатор экземпляра.
 * @return EVENT__OK - успешное выполнение
 *         EVENT__ERROR - ошибка
 ******************************************************************************/
events__e hmc7044__init(i32 id);

/***************************************************************************//**
 * @brief Функция чтения регистра HMC7044.
 * @param id - идентификатор экземпляра.
 * @param addr - адрес регистра. Может быть получен припомощи макроса HMC7044_REG.
 * @param data - указатель на буфер с прочитанными данными.
 * @param size - размер данных, байт.
 * @return EVENT__OK - успешное выполнение
 *         EVENT__ERROR - ошибка
 ******************************************************************************/
events__e hmc7044__read(i32 id, u16 addr, u8* data, i32 size);

/***************************************************************************//**
 * @brief Функция записи регистра HMC7044.
 * @param id - идентификатор экземпляра.
 * @param addr - адрес регистра. Может быть получен припомощи макроса HMC7044_REG.
 * @param data - указатель на буфер с данными для записи в регистр.
 * @param size - размер данных, байт.
 * @return EVENT__OK - успешное выполнение
 *         EVENT__ERROR - ошибка
 ******************************************************************************/
events__e hmc7044__write(i32 id, u16 addr, u8* data, i32 size);

#endif
