/***************************************************************************//**
 * @file ltc2500_types.h.
 * @brief Файл с типами данных микросхемы LTC2500.
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef LTC2500_TYPES_H_
#define LTC2500_TYPES_H_

typedef enum {
    LTC2500__EVENT_MCLK_OFF = 0,
    LTC2500__EVENT_MCLK_ON,
    LTC2500__EVENT_RDLA,
    LTC2500__EVENT_RDLB,
    LTC2500__EVENT_SYNC,
    LTC2500__EVENT_PRE,
    LTC2500__EVENT_MCLK_800,
    LTC2500__EVENT_MCLK_400,
    LTC2500__EVENT_MCLK_200,
    LTC2500__EVENT_MCLK_100,
    LTC2500__EVENT_GET_STATE,

}ltc2500__events_e;


#endif
