/***************************************************************************//**
 * @file isl55161.h.
 * @brief Модуль, с описанием функций управления миксрохемой ISL55161. Вся работа
 *        ведется со структурой, возвращаемой функцией isl55161__get_struct();
 *        Для того, чтобы записать данные в регистр, необходимо разместить их в структуре, а затем
 *        вызвать функцию isl55161__write();
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef ISL55161_H_
#define ISL55161_H_

#include <stdint.h>
#include "System/events.h"

#define ISL55161_OP_WR 0
#define ISL55161_OP_RD 1

/*******************************************************************************
 * Структура регистров миксрохемой ISL55161.
 ******************************************************************************/
#pragma pack(push,2)
typedef struct {
    uint16_t dvh;                         //
    uint16_t dvl;                         //
    uint16_t vtt;                         //
    uint16_t cva;                         //
    uint16_t cvb;                         //
    uint16_t cva_ppmu;                    //
    uint16_t cvb_ppmu;                    //
    uint16_t v_fv;                        //
    uint16_t v_fl;                        //
    uint16_t v_cl_hi;                     //
    uint16_t v_cl_low;                    //
    uint16_t i_src;                       //
    uint16_t i_sink;                      //
    uint16_t reserved[3];                 //
}isl55161_dac_t;

typedef struct {
    isl55161_dac_t level;                 // 0x00
    isl55161_dac_t offset;                // 0x10
    isl55161_dac_t gain;                  // 0x20
}isl55161_ram_t;

typedef struct {
    struct {
        uint16_t dz             :2;
        uint16_t w_dz           :1;
        uint16_t ez             :2;
        uint16_t w_ez           :1;
        uint16_t seld_bp        :1;
        uint16_t w_seld_bp      :1;
        uint16_t sel_en         :1;
        uint16_t w_sel_en       :1;
    } term;                               // 0x40
    struct {
        uint16_t seld             :1;
        uint16_t sel_en           :1;
        uint16_t reserved         :1;
        uint16_t w_seld           :1;
        uint16_t selsv_en         :1;
        uint16_t w_selsv_en       :1;
        uint16_t fhiz_en          :1;
        uint16_t w_fhiz_en        :1;
        uint16_t hiz_over         :1;
        uint16_t w_hiz_over       :1;
        uint16_t reserved1        :4;
        uint16_t cpu_xor          :1;
        uint16_t w_cpu_xor        :1;
    }cfg;                                 // 0x41
    struct {
        uint16_t selrt_d          :1;
        uint16_t w_selrt_d        :1;
        uint16_t selrt_en         :1;
        uint16_t w_selrt_en       :1;
        uint16_t selrt_sv         :1;
        uint16_t w_selrt_sv       :1;
        uint16_t dr_mode0         :1;
        uint16_t w_dr_mode0       :1;
        uint16_t dr_mode1         :1;
        uint16_t w_dr_mode1       :1;
        uint16_t reserved         :4;
        uint16_t hold_sv          :1;
        uint16_t w_hold_sv        :1;
    }ctrl;                                // 0x42
    struct {
        uint16_t d_fd             :4;
        uint16_t w_d_fd           :1;
        uint16_t reserved         :1;
        uint16_t seld_fd          :1;
        uint16_t w_seld_fd        :1;
        uint16_t d_cd             :5;
        uint16_t w_d_cd           :1;
        uint16_t seld_cd          :1;
        uint16_t w_seld_cd        :1;
    }data_deskew;                         // 0x43
    struct {
        uint16_t d_ffea           :4;
        uint16_t w_d_ffea         :1;
        uint16_t reserved         :1;
        uint16_t seld_ffea        :1;
        uint16_t w_seld_ffea      :1;
        uint16_t d_cfea           :4;
        uint16_t w_d_cfea         :1;
        uint16_t reserved1        :1;
        uint16_t seld_cfea        :1;
        uint16_t w_seld_cfea      :1;
    }data_falling_adj;                    // 0x44
    struct {
        uint16_t en_fd            :4;
        uint16_t w_en_fd          :1;
        uint16_t reserved         :1;
        uint16_t sel_en_fd        :1;
        uint16_t w_sel_en_fd      :1;
        uint16_t en_cd            :5;
        uint16_t w_en_cd          :1;
        uint16_t sel_en_cd        :1;
        uint16_t w_sel_en_cd      :1;
    }deskew_en;                           // 0x45
    struct {
        uint16_t en_ffea          :4;
        uint16_t w_en_ffea        :1;
        uint16_t reserved         :1;
        uint16_t sel_en_ffea      :1;
        uint16_t w_sel_en_ffea    :1;
        uint16_t en_cfea          :4;
        uint16_t w_en_cfea        :1;
        uint16_t reserved1        :1;
        uint16_t sel_en_cfea      :1;
        uint16_t w_sel_en_cfea    :1;
    }falling_adj_en;                      // 0x46
    struct {
        uint16_t ro_dvl           :4;
        uint16_t w_ro_dvl         :1;
        uint16_t ro_dvh           :4;
        uint16_t w_ro_dvh         :1;
        uint16_t ro_vtt           :4;
        uint16_t w_ro_vtt         :1;
    }out_impedance;                       // 0x47
    struct {
        uint16_t comp_off         :1;
        uint16_t pmu_comp_off     :1;
        uint16_t pmu_mi_off       :1;
        uint16_t w_pwr_opt        :1;
    }pwr_opt;                             // 0x48
}isl55161_driver_t;

typedef struct {
    struct {
        uint16_t fd               :4;
        uint16_t w_fd             :1;
        uint16_t reserved         :1;
        uint16_t sel_fd           :1;
        uint16_t w_sel_fd         :1;
        uint16_t cd               :5;
        uint16_t w_cd             :1;
        uint16_t sel_cd           :1;
        uint16_t w_sel_cd         :1;
    } deskew;
    struct {
        uint16_t ffea             :5;
        uint16_t w_ffea           :1;
        uint16_t reserved         :1;
        uint16_t sel_ffea         :1;
        uint16_t w_sel_ffea       :1;
    }falling;
}isl55161_comp_item_t;

typedef struct {
    struct {
        uint16_t sel_mu_a         :1;
        uint16_t sel_mu_b         :1;
        uint16_t w_sel_mu         :1;
        uint16_t reserved         :3;
        uint16_t sel_cpu_c        :1;
        uint16_t w_sel_cpu_c      :1;
        uint16_t cpu_ca           :1;
        uint16_t cpu_cb           :1;
        uint16_t w_cpu_ca_cb      :1;
        uint16_t sel_c_bp         :1;
        uint16_t w_sel_c_bp       :1;
    }cfg;                                 // 0x50
    isl55161_comp_item_t comp_a;          // 0x51
    isl55161_comp_item_t comp_b;          // 0x53
    struct {
        uint16_t ring_en          :1;
        uint16_t sel_c_diag       :1;
        uint16_t en_sv            :1;
        uint16_t edge_par         :1;
        uint16_t en_diag          :1;
        uint16_t d_diag           :1;
        uint16_t w_diag           :1;
        uint16_t cpu_pulse        :1;
        uint16_t w_cpu_pulse      :1;
        uint16_t reserved         :3;
        uint16_t rt_ca            :1;
        uint16_t rt_cb            :1;
        uint16_t pmu_ca           :1;
        uint16_t pmu_cb           :1;
    }diag;                                // 0x55
}isl55161_comp_t;

typedef struct {
    struct {
        uint16_t fi_fv            :1;
        uint16_t w_fi_fv          :1;
        uint16_t mi_mv            :1;
        uint16_t w_mi_mv          :1;
        uint16_t loop             :1;
        uint16_t w_loop           :1;
        uint16_t sel_mu           :1;
        uint16_t w_sel_mu         :1;
        uint16_t sel_mv           :1;
        uint16_t w_sel_mv         :1;
        uint16_t v_ci_en          :1;
        uint16_t w_v_ci_en        :1;
        uint16_t ppmu_en          :1;
        uint16_t w_ppmu_en        :1;
        uint16_t sel_mon_ref      :1;
        uint16_t w_sel_mon_ref    :1;
    }cfg;                                 // 0x60
    struct {
        uint16_t ir               :8;
        uint16_t w_ir             :1;
    }current_range;                       // 0x61
    struct {
        uint16_t en_ext_force     :1;
        uint16_t w_en_ext_force   :1;
        uint16_t en_ext_sense     :1;
        uint16_t w_en_ext_sense   :1;
        uint16_t sel_ext_sense    :1;
        uint16_t w_sel_ext_sense  :1;
        uint16_t sel_ext_dac      :1;
        uint16_t w_sel_ext_dac    :1;
    }sense_force;                         // 0x62
    struct {
        uint16_t mu_diag          :4;
        uint16_t w_mu_diag        :1;
        uint16_t reserved         :1;
        uint16_t sel_mu_diag      :1;
        uint16_t w_sel_mu_diag    :1;
    }diag;                                // 0x63
    struct {
        uint16_t drive            :2;
        uint16_t w_drive          :1;
        uint16_t comp             :2;
        uint16_t w_comp           :1;
        uint16_t fv               :2;
        uint16_t w_fv             :1;
        uint16_t ppmu             :2;
        uint16_t w_ppmu           :1;
        uint16_t v_mid            :2;
        uint16_t w_v_mid          :1;
    }dc_lvl_range;                        // 0x64
    struct {
        uint16_t source_adj       :3;
        uint16_t w_source_adj     :1;
        uint16_t sink_adj         :3;
        uint16_t w_sink_adj       :1;
    }load;                                // 0x65
}isl55161_pmu_t;

typedef struct {
    isl55161_driver_t drv;                // 0x40
    uint16_t reserved[7];                 // 0x49
    isl55161_comp_t comp;                 // 0x50
    struct {
        uint16_t d15          :6;
        uint16_t w_d15        :1;
        uint16_t d14          :5;
        uint16_t w_d14        :1;
        uint16_t coarse_adj   :2;
        uint16_t w_coarse_adj :1;
    }dac_calib_hi;                        // 0x56 - 0xD6 Перенесен с целью экономии памяти
    struct {
        uint16_t d13          :4;
        uint16_t w_d13        :1;
        uint16_t d12          :3;
        uint16_t w_d12        :1;
        uint16_t d11          :2;
        uint16_t w_d11        :1;
    }dac_calib_low;                       // 0x57 - 0xD7 Перенесен с целью экономии памяти
    uint16_t reserved1[8];                // 0x58
    isl55161_pmu_t pmu;                   // 0x60
}isl55161_reg_t;

typedef struct {
    struct {
        uint16_t mon_oe       :1;
        uint16_t w_mon_oe     :1;
        uint16_t sel_mon      :1;
        uint16_t w_sel_mon    :1;
        uint16_t reserved     :2;
        uint16_t cpu_ot       :1;
        uint16_t w_cpu_ot     :1;
    }over_temp;                           // 0x70
    struct {
        uint16_t cpu_d0       :1;
        uint16_t w_cpu_d0     :1;
        uint16_t cpu_en0      :1;
        uint16_t w_cpu_en0    :1;
        uint16_t cpu_sv0      :1;
        uint16_t w_cpu_sv0    :1;
        uint16_t reserved     :1;
        uint16_t cpu_d1       :1;
        uint16_t w_cpu_d1     :1;
        uint16_t cpu_en1      :1;
        uint16_t w_cpu_en1    :1;
        uint16_t cpu_sv1      :1;
        uint16_t w_cpu_sv1    :1;
    }cpu_force;                           // 0x71
    struct {
        uint16_t z            :2;
        uint16_t w_z          :1;
        uint16_t v_swing      :2;
        uint16_t w_v_swing    :1;
        uint16_t f_clamp      :2;
        uint16_t w_f_clamp    :1;
        uint16_t pll_dis      :1;
        uint16_t w_pll_dis    :1;
    }pll;                                 // 0x72
    uint16_t cpu_rst;                     // 0x73
    struct {
        uint16_t diff_a       :2;
        uint16_t w_diff_a     :1;
        uint16_t diff_b       :2;
        uint16_t w_diff_b     :1;
    }diff_comp;                           // 0x74
    uint16_t reserved[10];                // 0x75
    struct {
        uint16_t die_rev      :4;
        uint16_t prod_id      :12;
    }die_id;                     // 0x7F
}isl55161_cent_reg_t;

typedef struct {
    isl55161_ram_t ram;                   // 0x00 Содержимое ОЗУ
    uint16_t reserved[16];                // 0x30
    isl55161_reg_t regs;                  // 0x40 Регистры
    uint16_t reserved1[10];               // 0x66
    isl55161_cent_reg_t central_reg;      // 0x70 Центральные регистры
}isl55161__struct_t;
#pragma pack(pop)

// Макрос для получения адреса регистра микросхемы ISL55161
#define ISL55161_REG (uint16_t)&((isl55161__struct_t*)0x00)
// Размер регистра, байт
#define ISL55161_REG_LEN 2

/***************************************************************************//**
 * @brief Пользовательский коллбэк по завершению операции чтения\записи регистров.
 * @param id - идентификатор драйвера (0...255).
 * @param event - результат операции.
 * @param addr - смещение в структуре isl55161__struct_t (кратно 2).
 * @param len - размер данных в байтах.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return void
 ******************************************************************************/
typedef void (*isl55161_cb_t)(uint8_t id, events__e event, uint32_t addr, int len, void* ext_data);

/***************************************************************************//**
 * @brief Указатель на функцию транспортного уровня SPI.
 * @param spi_id - идентификатор интерфейса (0...127). На одном интерфейсе расположены 2 драйвера.
 * @param oper - код операции (0 - запись, 1 - чтение).
 * @param addr - адрес регистра в микросхеме isl55161.
 * @param buff - указатель на буфер для записи\чтения данных.
 * @param len - размер данных в байтах.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
typedef events__e (*isl55161_hw_func_t)(uint8_t spi_id, uint8_t oper, uint32_t addr, uint16_t* buff, int len);

/***************************************************************************//**
 * @brief Коллбэк транспортного уровня SPI. Должен быть вызван пользователем по завершении операции чтения\записи
 *        регистров. Вызов должен производиться на потоке cout-ов.
 * @param spi_id - идентификатор интерфейса (0...127). На одном интерфейсе расположены 2 драйвера.
 * @param event - результат операции.
 * @param addr - адрес регистра в микросхеме isl55161.
 * @param buff - указатель на буфер для записи\чтения данных.
 * @param len - размер данных в байтах.
 * @return void.
 ******************************************************************************/
void isl55161__rw_cb(uint8_t spi_id, events__e event, uint32_t addr, uint16_t* buff, int len);

/***************************************************************************//**
 * @brief Функция инициализации библиотеки. Должна вызываться вначале программы.
 * @param hw_transport - указатель на функцию транспортного уровня SPI.
 * @return EVENT__OK - операция завершена успешно.
 ******************************************************************************/
events__e isl55161__init(isl55161_hw_func_t hw_transport);

/***************************************************************************//**
 * @brief Функция возвращает указатель на структуру с данными для конкретного экземпляра драйвера
 *        (в каждой микросхеме isl55161 по 2 драйвера).
 * @param id - идентификатор драйвера (0..255).
 * @return isl55161__struct_t - указатель на структуру.
 ******************************************************************************/
isl55161__struct_t* isl55161__get_struct(uint8_t id);

/***************************************************************************//**
 * @brief Функция чтения регистров isl55161.
 * @param id - идентификатор драйвера (0...255).
 * @param addr - смещение в структуре isl55161__struct_t (кратно 2).
 * @param len - размер данных в байтах.
 * @param cb - коллбэк по завершению операции.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
events__e isl55161__read(uint8_t id, uint32_t addr, int len, isl55161_cb_t cb, void* ext_data);

/***************************************************************************//**
 * @brief Функция записи регистров isl55161.
 * @param id - идентификатор драйвера (0...255).
 * @param addr - смещение в структуре isl55161__struct_t (кратно 2).
 * @param len - размер данных в байтах.
 * @param cb - коллбэк по завершению операции.
 * @param ext_data - указатель на пользовательские данные. Может быть NULL.
 * @return EVENT__OK - операция выполнена успешно.
 *         EVENT__BUSY - интерфейс занят.
 ******************************************************************************/
events__e isl55161__write(uint8_t id, uint32_t addr, int len, isl55161_cb_t cb, void* ext_data);


#endif
