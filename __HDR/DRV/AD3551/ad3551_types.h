/***************************************************************************//**
 * @file ad3551_types.h.
 * @brief Файл с типами данных микросхемы DAC1101.
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef AD3551_TYPES_H_
#define AD3551_TYPES_H_

typedef enum {
    AD3551__EVENT_LDAC = 0,
}ad3551__events_e;


#endif
