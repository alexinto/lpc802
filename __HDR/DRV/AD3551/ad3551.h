/***************************************************************************//**
 * @file ad3551.h.
 * @brief Драйвер конфигурации и идентификационных регистров ПЛИС.
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef AD3551_H_
#define AD3551_H_

#include "System/events.h"
#include "DRV/AD3551/ad3551_types.h"

#pragma pack(push, 1)

typedef struct {
    uint8_t reset_lsb   :1;
    uint8_t reserved    :3;
    uint8_t sdo_active  :1;
    uint8_t addr_dir    :1;
    uint8_t reserved1   :1;
    uint8_t reset_msb   :1;
}ad3551__itf_cfg_a_t;

typedef struct {
    uint8_t reserved          :3;
    uint8_t short_istruction  :1;
    uint8_t reserved1         :3;
    uint8_t single_istruction :1;
}ad3551__itf_cfg_b_t;

typedef struct {
    uint8_t op_mode           :2;
    uint8_t custom_mode       :2;
    uint8_t dev_status        :4;
}ad3551__dev_cfg_t;

typedef struct {
    uint8_t reserved          :2;
    uint8_t stream_length     :1;
    uint8_t reserved1         :3;
    uint8_t multi_io          :2;
}ad3551__tx_reg_t;

typedef struct {
    uint8_t crc_en_b          :2;
    uint8_t reserved          :3;
    uint8_t strict_reg_access :1;
    uint8_t crc_en            :2;
}ad3551__itf_cfg_c_t;

typedef struct {
    uint8_t reg_invalid       :1;
    uint8_t partial_access    :1;
    uint8_t wr_to_ro_reg      :1;
    uint8_t no_crc            :1;
    uint8_t reserved          :1;
    uint8_t clk_err           :1;
    uint8_t reserved1         :1;
    uint8_t itf_not_ready     :1;
}ad3551__itf_status_a_t;

typedef struct {
    uint8_t spi_ddr           :1;
    uint8_t dual_spi_sync_en  :1;
    uint8_t sdio_strength     :2;
    uint8_t mem_crc_en        :1;
    uint8_t reserved          :1;
    uint8_t alert_pullup_en   :1;
    uint8_t reserved1         :1;
}ad3551__itf_cfg_d_t;

typedef struct {
    uint8_t ref_voltage       :2;
    uint8_t reserved          :4;
    uint8_t idump_fastmode    :1;
    uint8_t reserved1         :1;
}ad3551__ref_cfg_t;

typedef struct {
    uint8_t addr_invalid      :1;
    uint8_t partial_access    :1;
    uint8_t wr_to_ro_reg      :1;
    uint8_t crc_err           :1;
    uint8_t mem_crc           :1;
    uint8_t clk_err           :1;
    uint8_t ref_range         :1;
    uint8_t reserved          :1;
}ad3551__err_alarm_t;

typedef struct {
    uint8_t reset             :1;
    uint8_t reserved          :3;
    uint8_t mem_crc           :1;
    uint8_t dual_spi          :1;
    uint8_t ref_range         :1;
    uint8_t reserved1         :1;
}ad3551__err_status_t;

typedef struct {
    uint8_t reserved          :4;
    uint8_t ch0_dac           :1;
    uint8_t reserved1         :3;
}ad3551__pwr_down_cfg_t;

typedef struct {
    uint8_t ch0_offset8       :1;
    uint8_t reserved          :1;
    uint8_t ch0_offset_pol    :1;
    uint8_t ch0_gain_scal_p   :2;
    uint8_t ch0_gain_scal_n   :2;
    uint8_t ch0_rng_override  :1;
}ad3551__ch0_gain_t;

typedef struct {
    uint8_t reserved;
    uint16_t data;
}ad3551__dac_24b_t;

typedef struct {
    ad3551__itf_cfg_a_t itf_cfg_a;                 // 0x00
    ad3551__itf_cfg_b_t itf_cfg_b;                 // 0x01
    ad3551__dev_cfg_t dev_cfg;                     // 0x02
    uint8_t chip_type;                             // 0x03
    uint16_t product_id;                           // 0x04
    uint8_t chip_grade;                            // 0x06
    uint8_t reserved[3];
    uint8_t scratch_pad;                           // 0x0A
    uint8_t spi_revision;                          // 0x0B
    uint16_t vendor;                               // 0x0C
    uint8_t stream_mode;                           // 0x0E
    ad3551__tx_reg_t tx_reg;                       // 0x0F
    ad3551__itf_cfg_c_t itf_cfg_c;                 // 0x10
    ad3551__itf_status_a_t itf_status_a;           // 0x11
    uint8_t reserved1[2];
    ad3551__itf_cfg_d_t itf_cfg_d;                 // 0x14
    ad3551__ref_cfg_t ref_cfg;                     // 0x15
    ad3551__err_alarm_t err_alarm;                 // 0x16
    ad3551__err_status_t err_status;               // 0x17
    ad3551__pwr_down_cfg_t pwr_down_cfg;           // 0x18
    uint8_t ch0_output_range;                      // 0x19
    uint8_t reserved2;
    uint8_t ch0_offset;                            // 0x1B
    ad3551__ch0_gain_t ch0_gain;                   // 0x1C
    uint8_t reserved3[11];
    uint8_t hw_ldac_16b;                           // 0x28
    uint16_t ch0_dac_16b;                          // 0x29
    uint16_t reserved4;
    uint16_t dac_page_16b;                         // 0x2d
    uint8_t ch_select_16b;                         // 0x2f
    uint16_t input_page_16b;                       // 0x30
    uint8_t sw_ldac_16b;                           // 0x32
    uint16_t ch0_input_16b;                        // 0x33
    uint8_t reserved5[2];
    uint8_t hw_ldac_24b;                           // 0x37
    ad3551__dac_24b_t ch0_dac_24b;                 // 0x38
    uint8_t reserved6[3];
    ad3551__dac_24b_t dac_page_24b;                // 0x3e
    uint8_t ch_select_24b;                         // 0x41
    ad3551__dac_24b_t input_page_24b;              // 0x42
    uint8_t sw_ldac_24b;                           // 0x45
    ad3551__dac_24b_t ch0_input_24b;               // 0x46
}ad3551__reg_t;

#pragma pack(pop)

#define AD3551_REG (uint32_t)&((ad3551__reg_t*)0x00)

typedef enum {
    AD3551__REF_INT   = 0,
    AD3551__REF_EXT   = 2,
}ad3551__ref_e;

typedef enum {
    AD3551__GAIN_1     = 0,
    AD3551__GAIN_0_5   = 1,
    AD3551__GAIN_0_25  = 2,
    AD3551__GAIN_0_125 = 3,
}ad3551__gain_e;

typedef enum {
    AD3551__OFF_P  = 0,
    AD3551__OFF_N  = 1,
}ad3551__off_e;

typedef enum {
    AD3551__VRNG_0_p2    = 0,      // Requires RFB1_0 connection
    AD3551__VRNG_0_p5    = 1,      // Requires RFB1_0 connection
    AD3551__VRNG_0_p10   = 2,      // Requires RFB2_0 connection
    AD3551__VRNG_n5_p5   = 3,      // Requires RFB2_0 connection
    AD3551__VRNG_n10_p10 = 4,      // Requires RFB4_0 connection
}ad3551__vrng_e;

typedef enum {
    AD3551__MODE_PRESICION  = 0,
    AD3551__MODE_FAST       = 1,
}ad3551__mode_e;

typedef void (*ad3551__cb_t)(int dev_id, events__e event, uint32_t reg, uint32_t data, void* ext_data);

typedef void (*ad3551__handler_t)(int dev_id, ad3551__events_e event);

events__e ad3551__init(int dev_id, uint32_t settings, ad3551__cb_t cb, void* ext_data);

events__e ad3551__set_ref(int dev_id, ad3551__ref_e ref, ad3551__cb_t cb, void* ext_data);

events__e ad3551__set_vout(int dev_id, uint32_t data, ad3551__cb_t cb, void* ext_data);

events__e ad3551__get_vout(int dev_id, uint32_t* data, ad3551__cb_t cb, void* ext_data);

events__e ad3551__set_gain(int dev_id, ad3551__gain_e p_gain, ad3551__gain_e n_gain, ad3551__off_e polarity, ad3551__cb_t cb, void* ext_data);

events__e ad3551__set_vrng(int dev_id, ad3551__vrng_e range, ad3551__cb_t cb, void* ext_data);

events__e ad3551__set_voffset(int dev_id, int8_t offset, ad3551__cb_t cb, void* ext_data);

events__e ad3551__set_mode(int dev_id, ad3551__mode_e mode, ad3551__cb_t cb, void* ext_data);

events__e ad3551__write(int dev_id, uint32_t reg, uint32_t data, ad3551__cb_t cb, void* ext_data);

events__e ad3551__read(int dev_id, uint32_t reg, uint32_t* data, ad3551__cb_t cb, void* ext_data);

events__e ad3551__handler_set(ad3551__handler_t handler);

#endif
