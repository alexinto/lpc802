﻿/***************************************************************************//**
 * @file spi.h.
 * @brief Модуль программного SPI. Для работы с модулем необходимо определить кол-во
 *        устройств SPI при помощи дефайна SPI__DEV_COUNT в файле target.h.
 *        Для конфигурации должен быть реализован файл spi_cfg.c
 *        с таблицей конфигурации устройств SPI (spi__cfg_tbl_t).
 *        Пример: const spi__cfg_tbl_t spi_cfg__tbl[1] = {0};
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef SPI_H_
#define SPI_H_
#include "System/events.h"
#include "DRV/hw_spi.h"

// Тип данных для конфигурации SPI-устройств в файле spi_cfg.c
typedef struct {
    u8 spi_id;        // Идентификатор spi= экземпляр hw-spi.
    u8 cs_id;         // Идентификатор cs.
    hw_spi__func_t func;   // Указатель на функцию hw-уровня.
}spi__cfg_tbl_t;

// Перечень операций SPI, для установки битовых настроек settings. Устанавливаются функцией spi__set_cfg.
typedef enum {
    SPI__OP_READ  = 0,
    SPI__OP_WRITE = 1,
}spi__op_e;

// Настройки SPI - settings. Передаются при инициализации порта в функции spi__init.
// Биты 0-1 определяют настройку режима работы модуля SPI
#define SPI__MODE_POS                (0)
#define SPI__MODE_MASTER             (0x01UL << SPI__MODE_POS)
#define SPI__MODE_SLAVE              (0x02UL << SPI__MODE_POS)
#define SPI__MODE_MASK               (0x03UL << SPI__MODE_POS)
//  Биты 2-8 определяют частоту интерфейса, 0- 127 Мгц. 0- минимальная скорость. Частота 10Мгц Ex: (10 << SPI__BR_POS)
#define SPI__BR_POS                  (2)
#define SPI__BR_MASK                 (0x7FUL << SPI__BR_POS)
//  Биты 9-10 определяют настройку формата кадра
#define SPI__FRAME_POS               (9)
#define SPI__DATA_FRAME_8_BIT        (0x00UL << SPI__FRAME_POS)
#define SPI__DATA_FRAME_7_BIT        (0x01UL << SPI__FRAME_POS)
#define SPI__DATA_FRAME_16_BIT       (0x02UL << SPI__FRAME_POS)
#define SPI__DATA_FRAME_32_BIT       (0x03UL << SPI__FRAME_POS)
#define SPI__FRAME_MASK              (0x03UL << SPI__FRAME_POS)
//  Биты 11-12 определяют настройку фазы и полярности тактового сигнала
#define SPI__CPHA_CPOL_POS           (11)
#define SPI__CPHA_0_CPOL_0           (0x00UL << SPI__CPHA_CPOL_POS)
#define SPI__CPHA_1_CPOL_0           (0x01UL << SPI__CPHA_CPOL_POS)
#define SPI__CPHA_0_CPOL_1           (0x02UL << SPI__CPHA_CPOL_POS)
#define SPI__CPHA_1_CPOL_1           (0x03UL << SPI__CPHA_CPOL_POS)
#define SPI__CPHA_CPOL_MASK          (0x03UL << SPI__CPHA_CPOL_POS)
//  Бит 13 определяют настройку порядока передачи байта.
#define SPI__LSBMSB_POS              (13)
#define SPI__LSB                     (0x00UL << SPI__LSBMSB_POS)
#define SPI__MSB                     (0x01UL << SPI__LSBMSB_POS)
#define SPI__LSBMSB_MASK             (0x01UL << SPI__LSBMSB_POS)
//  Бит 14-21 определяет задержку после установки CS в активный уровень (0..255), мкс.
#define SPI__CS_DELAY_POS            (14)
#define SPI__CS_DELAY_MASK           (0xFFUL << SPI__CS_DELAY_POS)
//  Бит 22 определяет настройку CS (инвертирует)
#define SPI__CS_INV_POS              (22)
#define SPI__CS_INV                  (0x01UL << SPI__CS_INV_POS)
#define SPI__CS_INV_MASK             (0x01UL << SPI__CS_INV_POS)
//  Бит 23 определяет настройку CLK (1- постоянное тактирование, 0- CLK только при транзакциях)
#define SPI__CLK_POS                 (23)
#define SPI__CLK_ALWAYS              (0x01UL << SPI__CLK_POS)
#define SPI__CLK_MASK                (0x01UL << SPI__CLK_POS)
// Бит 24 Управление выводом CS. Оставляет CS в активном уровне, после транзакции TXRX.
#define SPI__CS_POS                  (24)
#define SPI__CS_ACTIVE               (0x01 << SPI__CS_POS)
// Бит 25 количество линий SPI, 1- трехпроводной, 0- четырехпроводной.
#define SPI__3WIRE_POS               (25)
#define SPI__3WIRE                   (0x01 << SPI__3WIRE_POS)
// Биты 26-29 задержка чтения SPI. Сдвиг CLK относительно MISO, часть периода.
#define SPI__RX_DELAY_POS            (26)
#define SPI__RX_DELAY_MASK           (0xFUL << SPI__RX_DELAY_POS)
// Биты 30-31. Определяют тип SPI.
#define SPI__TYPE_POS                (30)
#define SPI__TYPE_DDR                (0x1UL << SPI__TYPE_POS)  // 0- SDR, 1- DDR
#define SPI__TYPE_QUAD               (0x2UL << SPI__TYPE_POS)  // 0- SPI normal, 1- SPI QUAD
#define SPI__TYPE_MASK               (0x3UL << SPI__TYPE_POS)


/***************************************************************************//**
 * @brief Прототип функции обратного вызова по окончании операции чтения/записи.
 *        Передается в фнкцию hw_spi__txrx
 * @param dev_id - идентификатор устройства.
 * @param event - результат выполнения операции.
 * @param buff - указатель на буфер с принятыми данными.
 * @param len - количество принятых данных, байт.
 * @return void.
 ******************************************************************************/
typedef void (*spi__cb_t)(i32 dev_id, events__e code, u8 *buff, i32 len, void *ext_data);

/***************************************************************************//**
 * @brief Инициализация ресурса SPI.
 * @param dev_id - идентификатор устройства.
 * @param setting - настройки порта SPI (битовое поле). Общие и для записи и для чтения.
 * @return events__e.
 ******************************************************************************/
events__e spi__init(i32 dev_id, u32 settings);

/***************************************************************************//**
 * @brief Функция прекращения выполняемых SPI операций и освобождение ресурсов
 * @param dev_id - идентификатор устройства.
 * @return void
 ******************************************************************************/
events__e spi__deinit(i32 dev_id);

/***************************************************************************//**
 * @brief Инициализация обмена по SPI. Если указан tx_buff и rx_buff, то интерфейс
 *        работает в "полнодуплексном режиме". Каждый оттактированный бит на шине
 *        будет сохранен в rx_buff. Если SPI работает в режиме QUAD, то содержимое буфера
 *        tx_buff размером tx_len будет скопировано в буфер rx_buff.
 * @param dev_id - идентификатор устройства.
 * @param rx_buff - указатель на приемный буфер.
 *                  Если NULL, то в буфер ничего не пишем.
 * @param tx_buff - указатель на передающий буфер.
 *                  Если NULL, то передаем 0xFF.
 * @param len - длина обмена.
 * @param callback - указатель на функцию обратного вызова.
 * @param ext_data - указатель на пользовательские данные.
 * @return events__e.
 ******************************************************************************/
events__e spi__txrx(i32 dev_id, u8 *rx_buff, u32 rx_len, u8 *tx_buff, u32 tx_len, spi__cb_t callback, void *ext_data);

/***************************************************************************//**
 * @brief Функция записи настроек SPI
 * @param dev_id - идентификатор устройства.
 * @param op - тип операции, для которой устнавливаются настройки.
 * @param settings - битовая маска настроек.
 * @return  EVENT__OK - прием/передача на указанном SPI остановлена.
 *          EVENT__PARAM_NA - неверный идентификатор SPI.
 ******************************************************************************/
events__e spi__set_cfg(i32 dev_id, spi__op_e op, u32 settings);

/***************************************************************************//**
 * @brief Функция чтения настроек SPI
 * @param dev_id - идентификатор устройства.
 * @param op - тип операции, для которой считываются настройки.
 * @return  u32 - битовая маска настроек.
 ******************************************************************************/
u32 spi__get_cfg(i32 dev_id, spi__op_e op);

/***************************************************************************//**
 * @brief Функция прекращения приема/передачи данных
 * @param dev_id - идентификатор устройства.
 * @return  EVENT__OK - прием/передача на указанном SPI остановлена.
 *          EVENT__PARAM_NA - неверный идентификатор SPI.
 ******************************************************************************/
events__e spi__break_txrx(i32 dev_id);

// Набор фиксированных скоростей (для совместимости со старыми проектами).
#define SPI__BR_LOW_SPD              (0x00UL << SPI__BR_POS)
#define SPI__BR_375_KBIT             (0x01UL << SPI__BR_POS)
#define SPI__BR_750_KBIT             (0x02UL << SPI__BR_POS)
#define SPI__BR_1500_KBIT            (0x03UL << SPI__BR_POS)
#define SPI__BR_3_MBIT               (0x04UL << SPI__BR_POS)
#define SPI__BR_6_MBIT               (0x06UL << SPI__BR_POS)
#define SPI__BR_12_MBIT              (0x0CUL << SPI__BR_POS)
#define SPI__BR_24_MBIT              (0x18UL << SPI__BR_POS)
#define SPI__BR_33_MBIT              (0x21UL << SPI__BR_POS)
#define SPI__BR_48_MBIT              (0x30UL << SPI__BR_POS)
#define SPI__BR_66_MBIT              (0x42UL << SPI__BR_POS)
#define SPI__BR_96_MBIT              (0x60UL << SPI__BR_POS)
#define SPI__BR_HIGH_SPD             (0x7FUL << SPI__BR_POS)

#endif /* SPI_H_ */
