/***************************************************************************//**
 * @file ad5315.c.
 * @brief Драйвер PCA9535.
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef AD5315_H_
#define AD5315_H_

#include "System/events.h"

typedef enum {
    AD5315__DACA    = 1,
    AD5315__DACB    = 2,
    AD5315__DACC    = 4,
    AD5315__DACD    = 8,
}ad5315__dac_e;

typedef enum {
    AD5315__MODE_NORMAL    = 0,    // Normal Operation
    AD5315__MODE_1K        = 1,    // Power-Down (1 kΩ load to GND)
    AD5315__MODE_100K      = 2,    // Power-Down (100 kΩ load to GND)
    AD5315__MODE_HIZ       = 3,    // Power-Down (three-state output)
}ad5315__op_mode_e;


typedef void (*ad5315__cb_t)(int id, events__e event, ad5315__dac_e dac, uint16_t data, void* ext_data);

events__e ad5315__init(int id, int i2c_id, uint8_t addr, ad5315__cb_t cb, void* ext_data);

events__e ad5315__cfg(int id, ad5315__dac_e dac, ad5315__op_mode_e mode, ad5315__cb_t cb, void* ext_data);

events__e ad5315__write(int id, ad5315__dac_e dac, uint16_t data, ad5315__cb_t cb, void* ext_data);

events__e ad5315__read(int id, ad5315__dac_e dac, uint16_t* data, ad5315__cb_t cb, void* ext_data);



#endif
