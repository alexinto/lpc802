/***************************************************************************//**
 * @file key_code.h.
 * @brief ������ ���������� ���������� ����������� �� uart.
 * @author a.tushentsov
 ******************************************************************************/
#ifndef KEY_CODE_H_
#define KEY_CODE_H_
#include "System/events.h"

typedef enum {
	KEY__RELEASE = 0,
	KEY__PRESS   = 1,
}key_code__ctrl_e;

typedef enum {
	key_none  = 0x00,
	key_a     = 0x04,
	key_b     = 0x05,
	key_c     = 0x06,
	key_d     = 0x07,
	key_e     = 0x08,
	key_f     = 0x09,
	key_g     = 0x0A,
	key_h     = 0x0B,
	key_i     = 0x0C,
	key_j     = 0x0D,
	key_k     = 0x0E,
	key_l     = 0x0F,
	key_m     = 0x10,
	key_n     = 0x11,
	key_o     = 0x12,
	key_p     = 0x13,
	key_q     = 0x14,
	key_r     = 0x15,
	key_s     = 0x16,
	key_t     = 0x17,
	key_u     = 0x18,
	key_v     = 0x19,
	key_w     = 0x1A,
	key_x     = 0x1B,
	key_y     = 0x1C,
	key_z     = 0x1D,
	key_1     = 0x1E,
	key_2     = 0x1F,
	key_3     = 0x20,
	key_4     = 0x21,
	key_5     = 0x22,
	key_6     = 0x23,
	key_7     = 0x24,
	key_8     = 0x25,
	key_9     = 0x26,
	key_0     = 0x27,
	key_enter = 0x28,
	key_esc   = 0x29,
	key_del   = 0x2A,
	key_tab   = 0x2B,
	key_space = 0x2C,
	key_tire  = 0x2D,
	key_equal = 0x2E,
	key_ls    = 0x2F,
	key_rs    = 0x30,
	key_nslsh = 0x31,
	key_ntire = 0x32,
	key_ddot  = 0x33,
	key_tilda = 0x34,
	key_grave = 0x35,
	key_ndot  = 0x36,
	key_dot   = 0x37,
	key_slash = 0x38,
	key_cslck = 0x39,
	key_F1    = 0x3A,
	key_F2    = 0x3B,
	key_F3    = 0x3C,
	key_F4    = 0x3D,
	key_F5    = 0x3E,
	key_F6    = 0x3F,
	key_F7    = 0x40,
	key_F8    = 0x41,
	key_F9    = 0x42,
	key_F10   = 0x43,
	key_F11   = 0x44,
	key_F12   = 0x45,
	key_prnscr= 0x46,
	key_slock = 0x47,
	key_pause = 0x48,
	key_insert= 0x49,
	key_home  = 0x4A,
	key_pageup= 0x4B,
	key_delete= 0x4C,
	key_end   = 0x4D,
	key_pagedn= 0x4E,
	key_right = 0x4F,
	key_left  = 0x50,
	key_down  = 0x51,
	key_up    = 0x52,
	kpad_num  = 0x53,
	kpad_slsh = 0x54,
	kpad_star = 0x55,
	kpad_tire = 0x56,
	kpad_min  = 0x57,
	kpad_entr = 0x58,
	kpad_1    = 0x59,
	kpad_2    = 0x5A,
	kpad_3    = 0x5B,
	kpad_4    = 0x5C,
	kpad_5    = 0x5D,
	kpad_6    = 0x5E,
	kpad_7    = 0x5F,
	kpad_8    = 0x60,
	kpad_9    = 0x61,
	kpad_0    = 0x62,
	kpad_dot  = 0x63,
	key_n_US  = 0x64,
}key_code__e;

#pragma pack(push, 1)
typedef struct {
	uint8_t l_ctrl   :1;
	uint8_t l_shift  :1;
	uint8_t l_alt    :1;
	uint8_t l_gui    :1;
	uint8_t r_ctrl   :1;
	uint8_t r_shift  :1;
	uint8_t r_alt    :1;
	uint8_t r_gui    :1;
}key_code__mod_t;

typedef struct {
	key_code__mod_t modifier;
	uint8_t reserved;
	uint8_t key[12];       //key_code__e
}key_code__struct_t;
#pragma pack(pop)

events__e key_code__init(uint8_t itf);

events__e key_code__set(key_code__struct_t* keys);

char* key_code_get_name(key_code__e key);

void key_code__deinit();

#endif
