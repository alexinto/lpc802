﻿/***************************************************************************//**
 * @file lcd2004.c.
 * @brief  Драйвер дисплея lcd 2004.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef LCD2004_H_
#define LCD2004_H_

#include "System/framework.h"

typedef void (*lcd2004__cb_t)(int id, events__e event, void* ext_data);



events__e lcd2004__init(int id, int i2c_id, int addr, lcd2004__cb_t cd, void* ext_data);

/***************************************************************************//**
 * @brief Функция- инициализации модуля ADS1115. Задается опорное напряжение и скорость
 *        преобразования.
 * @param id - идентификатор микросхемы.
 * @param buff - строка, которую необходимо вывести на дисплей.
 * @param cb - коллбэк окончания операции.
 * @param ext_data - пользовательские данные.
 * @return EVENT_OK - успешное выполнение.
 *         EVENT_ERROR - ошибка инициализации.
 ******************************************************************************/
events__e lcd2004__set(int id, char* buff, int len, lcd2004__cb_t cb, void* ext_data);


// очистка экрана
events__e lcd2004__clr(int id, lcd2004__cb_t cb, void* ext_data);

#endif