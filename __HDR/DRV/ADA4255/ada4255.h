﻿/***************************************************************************//**
 * @file ada4255.h.
 * @brief Драйвер микросхемы ada4255.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef ADA4255_H_
#define ADA4255_H_
#include "System/events.h"
#include "DRV/spi.h"

// Настройки интерфейса SPI
#define ADA4255__SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_3_MBIT | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB)

#pragma pack(push,1)
typedef struct {
    uint8_t soft_reset;                    // 0x00 "Global soft reset control"
}ada4255__reg_struct_t;

#pragma pack(pop)
// Макрос для получения адреса регистра ADA4255
#define ADA4255_REG (uint32_t)&((ada4255__reg_struct_t*)0x00)

events__e ada4255__read(int id, uint16_t addr, uint8_t* data, int size);
events__e ada4255__write(int id, uint16_t addr, uint8_t* data, int size);

#endif
