/***************************************************************************//**
 * @file bootloader.h
 * @brief ������ ���������� (���������� ��).
 *       ������� ����������:
 * @author p.luchnikov
 ******************************************************************************/
#ifndef BOOTLOADER_H_
#define BOOTLOADER_H_

#include <stdint.h>
#include "standard.h"
#include "target.h"
#include "supervisor.h"
#include "sw.h"

//������� ��� ������
//    #define PRAGMA_OPTIMIZE_OFF _Pragma("optimize=none")
//    #define PRAGMA_OPTIMIZE_ON _Pragma("optimize=size high")

//#define BOOTLOADER__HEADER_SIZE                     128
//#define BOOTLOADER__STATUS_MAIN_FILE_WAIT           (1 << 0)        // ���� �������� �������� ��������� ���������� (1 - �������� ���������� �� ���������, 0 - �������� ���������� ���������.)

// ������� ���������� �� ��������� ����������.
typedef enum {
    BOOTLOADER__CMD_NONE                        =  0,   // ��� �������.
    BOOTLOADER__CMD_NEW_APL_LOAD                =  1,   // ������� ���������� �� ����� ������ ��������� ����������.
    BOOTLOADER__CMD_RESERVE_APL_LOAD            =  2,   // ������� ���������� �� ��������� ����� ��������� ����������.
    BOOTLOADER__CMD_NEW_BOOT_LOAD               =  3,   // ������� ���������� �� ����� ������ ����������.
    BOOTLOADER__CMD_ERR_CNT_RESET               =  4,   // ������� ������ �������� ������/������������.
    BOOTLOADER__CMD_BOOT_NEW_APL_LOAD           =  5,   // ������� �������������� ���������� �� ���������� ����������.
    BOOTLOADER__CMD_BOOT_RESERVE_APL_LOAD       =  6,   // ������� �������������� ���������� �� ���������� �� ���������� ����������.
} bootloader__cmd_e;

typedef enum
{
    BOOTLOADER__RESET_REASON_UNKNOWN        = SUPERVISOR__RESET_UNKNOWN,    // ������� ������������ ����������.
    BOOTLOADER__RESET_REASON_FIREWALL       = SUPERVISOR__RESET_FIREWALL,   // ������������������� ������ � ���������� ������.
    BOOTLOADER__RESET_REASON_OPT_BYTE       = SUPERVISOR__RESET_OPT_BYTE,   // ������������ ������������� ������������� OPT_BYTE (��������, ��� ����� � STDBY).
    BOOTLOADER__RESET_REASON_RESET_PIN      = SUPERVISOR__RESET_PIN,        // ������������ ����������� ���������� ������ �� ������ "Reset" ��.
    BOOTLOADER__RESET_REASON_BOR            = SUPERVISOR__RESET_BOR,        // ������������ �� ������� (������������ �������, ���������� �������).
    BOOTLOADER__RESET_REASON_RESET_SW       = SUPERVISOR__RESET_SW,         // ������������ �� ������� ������������� SYSRESETREQ bit.
    BOOTLOADER__RESET_REASON_WDT            = SUPERVISOR__RESET_WDT,        // ������������ �� WDT.
    BOOTLOADER__RESET_REASON_IN_SLEEP       = SUPERVISOR__RESET_IN_SLEEP,   // ������������ ��-�� ������������� ����� � ����� ����������������.
    BOOTLOADER__RESET_REASON_CLK_FAIL       = SUPERVISOR__RESET_CLK_FAIL,   // ������������ �� ������� ������������ ��������� ���������� ����������.

    BOOTLOADER__RESET_REASON_APL_UPD_OK         = 101,   // ������������ �� ��������� ���������� ��������� ����������.
    BOOTLOADER__RESET_REASON_APL_UPD_ERROR      = 102,   // ������������ �� �� ��������� ���������� ��������� ����������.
    BOOTLOADER__RESET_REASON_APL_RECOV_OK       = 103,   // ������������ �� ��������� ���������� �� ��������� ����� ��������� ����������.
    BOOTLOADER__RESET_REASON_APL_RECOV_ERROR    = 104,   // ������������ �� �� ��������� ���������� �� ��������� ����� ��������� ����������.
    BOOTLOADER__RESET_REASON_BOOT_UPD_OK        = 105,   // ������������ �� ��������� ���������� ����������.
    BOOTLOADER__RESET_REASON_BOOT_UPD_ERROR     = 106,   // ������������ �� �� ��������� ���������� ����������.
} bootloader__reset_reason_e;

typedef enum {
    BOOTLOADER__MAIN    = 0,
    BOOTLOADER__BOOT    = 1,
    BOOTLOADER__RESERVE = 2,
} bootloader__type_e;

// ��� �����
typedef struct {
    u32 max_addr;
    u32 addr;
    u32 size;
    const char build_date[12];
    const char build_time[8];
} bootloader__header_t;

typedef struct {
    bootloader__type_e instance;
    bootloader__cmd_e command;
    bootloader__reset_reason_e reason;
    u32 crc32_b[3];
    bootloader__header_t* header;
    sw__version_t version;
} bootloader__boot_info_t;

// ������� ����������.
typedef enum
{
    BOOTLOADER__EVENT_PARAM_NA                  = -2,   // �������� ���������.
    BOOTLOADER__EVENT_ERROR                     = -1,   // ������.
    BOOTLOADER__EVENT_OK                        =  0,   // OK.
    BOOTLOADER__EVENT_RESET                     =  1,   // ������������.
    BOOTLOADER__EVENT_MAIN_START                =  2,   // �������� ���������� ��������� ����������.
    BOOTLOADER__EVENT_FATAL_ERROR               =  3,   // �� ������� ����� � ��������� ��������������� ����������.
    BOOTLOADER__EVENT_CMD                       =  4,   // ����������� ������� ����������.
                                                        // ���.���.: bootloader__cmd_e
    BOOTLOADER__EVENT_FILE_IS_FAIL              =  5,   // �������� ����������� ��������� ����������.
                                                        // ���.���.: bootloader__file_e - ���� ��������
    BOOTLOADER__EVENT_FILE_COPY_ERROR           =  9,   // ������ ��� ����������� �����.
                                                        // ���.���.: bootloader__file_e - ���� ��������
} bootloader__event_e;

// ���� ������ ����������.
typedef enum {
    BOOTLOADER__FILE_NONE = -1,
    BOOTLOADER__FILE_BOOT,                              // ��������� � ������ ��
    BOOTLOADER__FILE_MAIN_APL,                          // ���������� � ������ ��
    BOOTLOADER__FILE_NEW_APL,                           // ���������� �� ���� ������
    BOOTLOADER__FILE_RESERVE_APL,                       // ��������� ���������� �� ���� ������
    BOOTLOADER__FILE_NEW_BOOT,                          // ��������� �� ���� ������
    BOOTLOADER__FILE_COPY_BOOT,                         // ����� ��������� � ������ �� (�������� ��� ����������� ���������� � ������ ��)
} bootloader__file_e;

// ���������, ����������� ��������� �����.
typedef struct bootloader__file_header_t{
    uint32_t crc;         // CRC32 ����� (�� ����������� ������ ���� CRC32).
    uint32_t addr;        // ��������� ����� ���������� � ��.
    uint32_t size;        // ������ ����� � ������.
} bootloader__file_header_t;

// ������� ������������������ ������ RAM.
typedef struct bootloader__no_init_area_t
{
    union
    {
        bootloader__cmd_e cmd;                      // ������� ����������.
        uint16_t __cmd;
    };
    union
    {
        bootloader__reset_reason_e reset_reason;    // ������� ��������� ������������, ���������� ����������� ��������� ����������.
        uint16_t __reset_reason;
    };
    uint16_t main_apl_start;                    // ���� ������������ ���������� ��������� ���������� (����� ��� ��������������� �������� ����������).
    uint16_t err_cnt;                           // ������� ������ // todo ���� �� ���������� (������).
    sw__version_t version;                      // ���������� � ������ �� ����������.
    uint8_t reserv[128];                        // ������
} bootloader_no_init_area_t;

/***************************************************************************//**
 * @brief �������� �������-����������� ������� ����������.
 * @param event - ��� �������.
 * @param data - ��������� �� �������������� ������ (������� �� ���� �������).
 * @return void.
 ******************************************************************************/
typedef void (*bootloader__event_handler_t)(bootloader__event_e event, void *data);


/***************************************************************************//**
 * @brief ������� ��������� ���������� � ������� ���������� ����������� ����������.
 * @param err_cnt - ���������, �� �������� ����� ���������� ���������� ������/������������ � ������� ������ �������).
 * @return ������� ����������� ����������.
 ******************************************************************************/
bootloader__reset_reason_e bootloader__reset_reason_get(uint16_t *err_cnt);

/***************************************************************************//**
 * @brief ������� ��������� ������� ����������� ���������� (���������� � ���� ������� �� �����������).
 * @param reason - ������� ����������� ����������
 * @return void
 ******************************************************************************/
void bootloader__reset_reason_set(bootloader__reset_reason_e reason);

/***************************************************************************//**
 * @brief ������� ��������� ���������� � ������ ����������.
 * @param void.
 * @return ������ ����������.
 ******************************************************************************/
sw__version_t bootloader__version_get(void);

/***************************************************************************//**
 * @brief ������� ������ ������� �������� ����������� ����������. ����� ����� ����������
 *        �������� supervisor__apl_reset(SUPERVISOR__RESET_WDT) ��� ��������
 *        ������� ����������.
 * @param cmd - �������.
 * @return void.
 ******************************************************************************/
void bootloader__cmd_set(bootloader__cmd_e cmd);

/***************************************************************************//**
 * @brief ������� ���������� ������� �����������.
 * @param void.
 * @return cmd - �������..
 ******************************************************************************/
bootloader__cmd_e bootloader__cmd_get();

/***************************************************************************//**
 * @brief ������� ��������� ���������� � ����� ��������� ����������.
 * @param ��� header-�: BOOTLOADER__MAIN ��� BOOTLOADER__BOOT.
 * @return ��������� �� header.
 ******************************************************************************/
bootloader__header_t* bootloader__header_get(bootloader__type_e header_type);

/***************************************************************************//**
 * @brief ������� �������� ����������� ��������� ���������� �� ������� flash.
 * @param void.
 * @return ��������� ��������:
 *                            BOOTLOADER__EVENT_FILE_IS_FAIL.
 *                            BOOTLOADER__EVENT_OK.
 ******************************************************************************/
bootloader__event_e bootloader__chk_app_img();

/***************************************************************************//**
 * @brief ������� ������ ��������� ���������� �� ������� ���� � ��.
 * @param void.
 * @return void.
******************************************************************************/
void bootloader__write_app_img_to_flash();

/***************************************************************************//**
 * @brief ������� ������ ���������� ���������� �� ������� ���� � ��.
 * @param void.
 * @return void.
******************************************************************************/
void bootloader__write_boot_img_to_flash();

/***************************************************************************//**
 * @brief ������� ������� ������ ���������� (������ ���������� � (������!) ������� ���������� 1 ��� �� ����� while(1)).
 * @param event_handler - ��������� �� ���������������� �������-���������� ������� ����������.
 *        ��������� �������:
 *          - BOOTLOADER__EVENT_RESET (� data ����� ������� ������������),
 *          - BOOTLOADER__EVENT_MAIN_START,
 *          - BOOTLOADER__EVENT_FATAL_ERROR,
 *          - BOOTLOADER__EVENT_CMD (� data ����� ��� ������� ����������)
 *          - BOOTLOADER__EVENT_MAIN_APL_IS_FAIL,
 *          - BOOTLOADER__EVENT_NEW_APL_IS_FAIL,
 *          - BOOTLOADER__EVENT_RESERVE_APL_IS_FAIL,
 *          - BOOTLOADER__EVENT_NEW_BOOT_IS_FAIL,
 *          - BOOTLOADER__EVENT_FILE_COPY_ERROR,
 * @return void.
 ******************************************************************************/
#ifdef BOOTLOADER__USE_HANDLER
void bootloader__init(bootloader__event_handler_t event_handler);
#else
void bootloader__init(void);
#endif

/***************************************************************************//**
 * @brief ������� ������ crc32-mpeg2 ����������, ����������.
 * @param img_type - ��� ����������, ��� ������ crc.
 * @param crc - crc32-mpeg2.
 * @return void.
******************************************************************************/
void bootloader__crc_set(bootloader__type_e img_type, u32 crc);

/***************************************************************************//**
 * @brief ������� ������ crc32-mpeg2 ����������, ����������.
 * @param img_type - ��� ����������, ��� ������ crc.
 * @return ��������� �� crc32-mpeg2.
******************************************************************************/
u32* bootloader__crc_get(bootloader__type_e img_type);

/*******************************************************************************
 * ������� ��������� ������ ������ ������: 0- main_appl, 1- bootloader.
 * ���������� ����� � ������ �����, ����� ����������� ��� ����������.
 * ��������� �������� � �������� ����������!!!
 ******************************************************************************/
void bootloader__set_instance(bootloader__type_e instance);

void bootloader__command_exec();

#endif /* BOOTLOADER_H_ */