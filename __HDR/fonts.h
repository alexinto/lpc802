#ifndef FONTS_H
#define FONTS_H

#include "stdint.h"

typedef struct {
   const uint8_t* data;
}fonts_t;

extern fonts_t font_8x4;


#endif