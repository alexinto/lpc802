
#ifndef __ADC_H
#define __ADC_H

#include "System/events.h"

#define ADC_0    0
#define ADC_1    1
#define ADC_2    2
#define ADC_3    3
#define ADC_4    4
#define ADC_5    5
#define ADC_6    6
#define ADC_7    7
#define ADC_8    8
#define ADC_9    9
#define ADC_10   10
#define ADC_11   11


/*******************************************************************************
 * Hander for reading ADC result from buffer.
 ******************************************************************************/
typedef void (*adc__handler_t)(events__e event);

/*******************************************************************************
 * ADC init ISR and power & clocks for ADC modul.
 ******************************************************************************/
events__e adc__init(uint16_t adc_ch, uint16_t* buff);

/*******************************************************************************
 * ADC start conversion. Result may be read from buff in ISR - adc_cb.
 * ex:
 ******************************************************************************/
events__e adc__start(adc__handler_t adc_cb);

/*******************************************************************************
 * Example: reinit ADC on timeout (uncorrected errors).
 ******************************************************************************/
void adc__deinit(void);


#endif