/***************************************************************************//**
 * @file mb_reg.h.
 * @brief �������� ������� � ����� ��������� Modbus RTU. ���� mb_reg.c � �������� ���������
          ������ ���� ���������� �������������.
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef MB_REG_H_
#define MB_REG_H_
#include "System/framework.h"


typedef enum {
    MB_REG__TYPE_DO = 0,        // ������-������	Discrete Output Coils
    MB_REG__TYPE_DI = 1,        // ������         	Discrete Input Contacts
    MB_REG__TYPE_AI = 2,        // ������	        Analog Input Registers
    MB_REG__TYPE_AO = 3,        // ������-������	Analog Output Holding Registers
}mb_reg__type_e;

typedef struct {
    uint16_t addr;
    uint16_t data;
}mb_reg__struct_t;


events__e mb_reg__get(u8 id, mb_reg__type_e type, uint16_t addr, uint16_t* data);

events__e mb_reg__set(u8 id, mb_reg__type_e type, uint16_t addr, uint16_t data);


#endif /* MB_REG_H_ */
