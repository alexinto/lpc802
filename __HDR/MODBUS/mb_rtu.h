/***************************************************************************//**
 * @file mb_rtu.h.
 * @brief ������� ������ � �������� Modbus RTU.
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef MB_RTU_H_
#define MB_RTU_H_
#include "System/events.h"
#include "MODBUS/mb_reg.h"
#include "DRV/uart.h"

typedef enum {
    MB_RTU__EX_NONE    = 0,         // No exception, OK
    MB_RTU__EX_FUNC    = 1,         // Illegal Function
    MB_RTU__EX_ADDR    = 2,         // Illegal Data Address
    MB_RTU__EX_DATA    = 3,         // Illegal Data Value
    MB_RTU__EX_DEVICE  = 4,         // An unrecoverable error occurred while the slave was attempting to perform the requested action.
    MB_RTU__EX_ACK     = 5,         // The slave has accepted the request and is processing it, but a long duration of time will be required to do so
    MB_RTU__EX_BUSY    = 6,         // The slave is engaged in processing a long-duration program command
    MB_RTU__EX_NACK    = 7,         // The slave cannot perform the program function received in the query.
    MB_RTU__EX_MEMORY  = 8,         // The slave attempted to read extended memory or record file, but detected a parity error in memory.
    MB_RTU__EX_GATEWAY = 10,        // Usually means the gateway is misconfigured or overloaded.
    MB_RTU__EX_TARGET  = 11,        // Usually means that the device is not present on the network
    MB_RTU__EX_UNCKNOWN= 12,        // Uncknown exception
}mb_rtu__ex_e;


typedef enum {
    MB_RTU__OP_NONE   = 0,
    MB_RTU__OP_RD_DO  = 1,
    MB_RTU__OP_RD_DI  = 2,
    MB_RTU__OP_RD_AO  = 3,
    MB_RTU__OP_RD_AI  = 4,
    MB_RTU__OP_WR_DO  = 5,
    MB_RTU__OP_WR_AO  = 6,
}mb_rtu__oper_e;

typedef enum {
    MB_RTU__CMD_STOP  = 0,
    MB_RTU__CMD_START = 1,
}mb_rtu__cmd_e;

typedef void (*mb_rtu__cb_t)(u8 id, events__e event, mb_rtu__ex_e exception, void* ext_data);

events__e mb_rtu__init(u8 id, u8 addr, int uart_id, uint32_t uart_settings, mb_rtu__cb_t cb, void* ext_data);

events__e mb_rtu__request(u8 id, uint8_t addr, mb_rtu__oper_e oper, u16 reg, u16 reg_cnt, mb_rtu__cb_t cb, void* ext_data);

events__e mb_rtu__cmd(u8 id, mb_rtu__cmd_e cmd);    // ����� � ���������

#endif /* MB_RTU_H_ */
