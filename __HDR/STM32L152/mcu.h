/***************************************************************************//**
 * @file mcu.h
 * @brief ���� � ��������� ����������������.
 *        ��: STM32L152RCT6.
 * @author a.tushentsov
 ******************************************************************************/
#ifndef MCU_H_
#define MCU_H_

#include "standard.h"
#include "gpio.h"

#define STM32L1XX_MDP    // ������ stm32l1xx.h

#include "stm32l1xx.h"

/*******************************************************************************
 * �������� GPIO ��.
 ******************************************************************************/
#define MCU__GPIO_PORT_A        (0x00)
#define MCU__GPIO_PORT_B        (0x01)
#define MCU__GPIO_PORT_C        (0x02)
#define MCU__GPIO_PORT_D        (0x03)
#define MCU__GPIO_PORT_E        (0x04)

#define MCU__GPIO_PIN_0         (0x00)
#define MCU__GPIO_PIN_1         (0x01)
#define MCU__GPIO_PIN_2         (0x02)
#define MCU__GPIO_PIN_3         (0x03)
#define MCU__GPIO_PIN_4         (0x04)
#define MCU__GPIO_PIN_5         (0x05)
#define MCU__GPIO_PIN_6         (0x06)
#define MCU__GPIO_PIN_7         (0x07)
#define MCU__GPIO_PIN_8         (0x08)
#define MCU__GPIO_PIN_9         (0x09)
#define MCU__GPIO_PIN_10        (0x0A)
#define MCU__GPIO_PIN_11        (0x0B)
#define MCU__GPIO_PIN_12        (0x0C)
#define MCU__GPIO_PIN_13        (0x0D)
#define MCU__GPIO_PIN_14        (0x0E)
#define MCU__GPIO_PIN_15        (0x0F)


#define MCU__GPIO_P_A_0         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_A_1         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_A_2         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_A_3         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_A_4         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_A_5         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_A_6         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_A_7         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_A_8         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_A_9         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_A_10        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_A_11        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_A_12        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_A_13        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_A_14        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_A_15        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_15)


#define MCU__GPIO_P_B_0         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_B_1         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_B_2         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_B_3         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_B_4         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_B_5         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_B_6         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_B_7         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_B_8         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_B_9         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_B_10        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_B_11        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_B_12        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_B_13        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_B_14        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_B_15        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_15)


#define MCU__GPIO_P_C_0         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_C_1         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_C_2         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_C_3         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_C_4         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_C_5         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_C_6         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_C_7         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_C_8         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_C_9         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_C_10        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_C_11        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_C_12        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_C_13        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_C_14        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_C_15        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_15)


#define MCU__GPIO_P_D_2         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_2)

/*******************************************************************************
 * �������� ����������� ��.
 ******************************************************************************/

#define MCU__UART_1 0
#define MCU__UART_2 1
#define MCU__UART_3 2
#define MCU__UART_COUNT 3

#define SPI_1 0
#define SPI_2 1
#define MCU__SPI_MAX 2

#define I2C_1 0
#define I2C_2 1
#define MCU__I2C_MAX 2

#define PULSE_CNT_1  0
#define PULSE_CNT_2  1

#endif /* MCU_H_ */
