/***************************************************************************//**
 * @file ic_flash_mx25.h
 * @brief �������������� ������� ������ MX25R6435F.
 * @authors a.tushentsov.
 ******************************************************************************/
#ifndef IC_FLASH_MX25_H_
#define IC_FLASH_MX25_H_

#include "flash.h"

// ����� ����� ��� ������� ���� ��������
ax_event__e ic_flash_mx25_exec(struct flash__t* flash_ptr, flash__op_e oper, uint32_t addr, uint8_t *buff, uint32_t len, flash__hw_cb cb);


#endif
