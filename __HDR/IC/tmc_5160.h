/***************************************************************************//**
 * @file tmc_5160.h.
 * @brief ������ ���������� ����������� TMC5160.
 * @author a.tushentsov.
 ******************************************************************************/
#ifndef TMC_5160_H_
#define TMC_5160_H_

#include "System/events.h"

#define TMC_PPR 25600
#define TMC_5160_SPI_SETTINGS (SPI__MODE_MASTER | SPI__BR_LOW_SPD | SPI__DATA_FRAME_8_BIT | SPI__CPHA_0_CPOL_0 | SPI__MSB)

typedef enum {
    TMC_5160_0 = 0,
    TMC_5160_1 = 1,
    TMC_5160_2 = 2,
    TMC_5160_3 = 3,
    TMC_5160_4 = 4,
    TMC_5160_5 = 5,
    TMC_5160_6 = 6,
    TMC_5160_7 = 7,
    TMC_5160_8 = 8,
    TMC_5160_9 = 9,
    TMC_5160_10 = 10,
    TMC_5160_11 = 11,
    TMC_5160_NONE = 12,
}tmc_5160__id_e;


typedef enum {
    TMC_I_HOLD         = 0,        // ��� ��������� 0..31
    TMC_I_RUN          = 1,        // ��� �������� 0..31
    TMC_STEALTH_SPEED  = 2,        // ��������, ��� ������� ����������� ��������� �����
    TMC_CUR_POS        = 3,        // ������� ������� (���������� � home-pos)
    TMC_DEST_POS       = 4,        // ����� ����������
    TMC_MIN_SPEED      = 5,        // ��������, � ������� �������� �����
    TMC_MAX_SPEED      = 6,        // ���� ��������. ��������� ��������.
    TMC_ACC            = 7,        // ���������
    TMC_DEC            = 8,        // ����������
    TMC_POS_REACHED    = 9,        // �������! (���� ���������� ���, ��� �����- �������� ����)
    TMC_TOFF           = 10,       // ���������� ��������� = 0
    TMC_STOP_SPEED     = 11,
    TMC_INITIALIZE     = 12,       // ��� ��� ���� - ������������ ��� �������������.
    TMC_SET_HOME_SPEED = 13,       // ���������� �������� ��� ����������
    TMC_RESTORE_SPEED  = 14,       // ������������ ��������
    TMC_PARAM_NONE     = 15,       // ���������� ���������
}tmc_5160__param_e;

typedef void (*tmc_5160__cb_t)(int tmc_id, events__e event, tmc_5160__param_e param_id, int param, void* ext_data);

/***************************************************************************//**
 * @brief ������� �������� ���������� � TMC5160.
 * @param tmc_id - ����� TMC5160 �� ���� SPI. ����� CS �������� � hw_spi.c.
 * @param param_id - ������������� ���������.
 * @param param - �������� ���������.
 * @param cb - ������� ���������� ��������.
 * @param ext_data - ���������������� ������.
 * @return EVENT_OK.
 ******************************************************************************/
events__e tmc_5160__set_param(int tmc_id, tmc_5160__param_e param_id, int param, tmc_5160__cb_t cb, void* ext_data);

/***************************************************************************//**
 * @brief ������� ������ ���������� �� TMC5160.
 * @param tmc_id - ����� TMC5160 �� ���� SPI. ����� CS �������� � hw_spi.c.
 * @param param_id - ������������� ���������. �������� ������������ � ��������.
 * @param cb - ������� ���������� ��������.
 * @param ext_data - ���������������� ������.
 * @return EVENT_OK.
 ******************************************************************************/
events__e tmc_5160__get_param(int tmc_id, tmc_5160__param_e param_id, tmc_5160__cb_t cb, void* ext_data);


events__e tmc_5160__deinit(int tmc_id, tmc_5160__cb_t cb, void* ext_data);





#endif
