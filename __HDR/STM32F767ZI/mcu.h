﻿/***************************************************************************//**
 * @file mcu.h
 * @brief Файл с описанием микроконтроллера.
 *        МК: STM32F767ZI.
 * @author a.tushentsov
 ******************************************************************************/
#ifndef MCU_H_
#define MCU_H_
#include <intrinsics.h>
#include "System/events.h"
#include "System/fstandard.h"
#include "DRV/gpio.h"
#include "DRV/uart.h"

#define stm32f767xx

#include "stm32f767xx.h"

#define WEAK_FUNC __weak

#define SET_BIT(REG, BIT)     ((REG) |= (BIT))
#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))
#define TOGGLE_BIT(REG, BIT)  ((REG) ^= (BIT))
#define READ_BIT(REG, BIT)    ((REG) & (BIT))
#define CLEAR_REG(REG)        ((REG) = (0x0))
#define WRITE_REG(REG, VAL)   ((REG) = (VAL))
#define READ_REG(REG)         ((REG))
#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))

#define CRITICAL_SECTION_ON                             \
{                                                       \
    __istate_t critical_section_state = __get_interrupt_state();         \
    __disable_interrupt();

#define CRITICAL_SECTION_OFF                            \
    __set_interrupt_state(critical_section_state);                       \
}

#define GPIO__DECLARE(port_No, pin_No) ( ((port_No) << 4) | ((pin_No) & 0xF) )


/*******************************************************************************
 * Перечень GPIO МК.
 ******************************************************************************/
#define MCU__GPIO_NONE 0xFF

#define MCU__GPIO_PORT_A        (0x00)
#define MCU__GPIO_PORT_B        (0x01)
#define MCU__GPIO_PORT_C        (0x02)
#define MCU__GPIO_PORT_D        (0x03)
#define MCU__GPIO_PORT_E        (0x04)
#define MCU__GPIO_PORT_F        (0x05)
#define MCU__GPIO_PORT_G        (0x06)
#define MCU__GPIO_PORT_H        (0x07)
#define MCU__GPIO_PORT_I        (0x08)
#define MCU__GPIO_PORT_J        (0x09)
#define MCU__GPIO_PORT_K        (0x0A)

#define MCU__GPIO_PIN_0         (0x00)
#define MCU__GPIO_PIN_1         (0x01)
#define MCU__GPIO_PIN_2         (0x02)
#define MCU__GPIO_PIN_3         (0x03)
#define MCU__GPIO_PIN_4         (0x04)
#define MCU__GPIO_PIN_5         (0x05)
#define MCU__GPIO_PIN_6         (0x06)
#define MCU__GPIO_PIN_7         (0x07)
#define MCU__GPIO_PIN_8         (0x08)
#define MCU__GPIO_PIN_9         (0x09)
#define MCU__GPIO_PIN_10        (0x0A)
#define MCU__GPIO_PIN_11        (0x0B)
#define MCU__GPIO_PIN_12        (0x0C)
#define MCU__GPIO_PIN_13        (0x0D)
#define MCU__GPIO_PIN_14        (0x0E)
#define MCU__GPIO_PIN_15        (0x0F)


#define MCU__GPIO_P_A_0         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_A_1         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_A_2         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_A_3         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_A_4         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_A_5         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_A_6         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_A_7         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_A_8         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_A_9         GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_A_10        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_A_11        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_A_12        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_A_13        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_A_14        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_A_15        GPIO__DECLARE(MCU__GPIO_PORT_A, MCU__GPIO_PIN_15)


#define MCU__GPIO_P_B_0         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_B_1         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_B_2         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_B_3         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_B_4         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_B_5         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_B_6         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_B_7         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_B_8         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_B_9         GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_B_10        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_B_11        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_B_12        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_B_13        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_B_14        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_B_15        GPIO__DECLARE(MCU__GPIO_PORT_B, MCU__GPIO_PIN_15)


#define MCU__GPIO_P_C_0         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_C_1         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_C_2         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_C_3         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_C_4         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_C_5         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_C_6         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_C_7         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_C_8         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_C_9         GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_C_10        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_C_11        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_C_12        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_C_13        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_C_14        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_C_15        GPIO__DECLARE(MCU__GPIO_PORT_C, MCU__GPIO_PIN_15)

#define MCU__GPIO_P_D_0         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_D_1         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_D_2         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_D_3         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_D_4         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_D_5         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_D_6         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_D_7         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_D_8         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_D_9         GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_D_10        GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_D_11        GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_D_12        GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_D_13        GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_D_14        GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_D_15        GPIO__DECLARE(MCU__GPIO_PORT_D, MCU__GPIO_PIN_15)

#define MCU__GPIO_P_E_0         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_E_1         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_E_2         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_E_3         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_E_4         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_E_5         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_E_6         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_E_7         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_E_8         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_E_9         GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_E_10        GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_E_11        GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_E_12        GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_E_13        GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_E_14        GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_E_15        GPIO__DECLARE(MCU__GPIO_PORT_E, MCU__GPIO_PIN_15)

#define MCU__GPIO_P_F_0         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_F_1         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_F_2         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_F_3         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_F_4         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_F_5         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_F_6         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_F_7         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_F_8         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_F_9         GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_F_10        GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_F_11        GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_F_12        GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_F_13        GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_F_14        GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_F_15        GPIO__DECLARE(MCU__GPIO_PORT_F, MCU__GPIO_PIN_15)

#define MCU__GPIO_P_G_0         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_G_1         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_G_2         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_G_3         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_G_4         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_G_5         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_G_6         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_G_7         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_G_8         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_G_9         GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_G_10        GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_G_11        GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_G_12        GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_G_13        GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_G_14        GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_G_15        GPIO__DECLARE(MCU__GPIO_PORT_G, MCU__GPIO_PIN_15)

#define MCU__GPIO_P_H_0         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_H_1         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_H_2         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_H_3         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_H_4         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_H_5         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_H_6         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_H_7         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_H_8         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_H_9         GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_H_10        GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_H_11        GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_H_12        GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_H_13        GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_H_14        GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_H_15        GPIO__DECLARE(MCU__GPIO_PORT_H, MCU__GPIO_PIN_15)

#define MCU__GPIO_P_I_0         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_I_1         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_I_2         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_I_3         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_I_4         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_I_5         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_I_6         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_I_7         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_I_8         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_I_9         GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_I_10        GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_I_11        GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_I_12        GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_I_13        GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_I_14        GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_I_15        GPIO__DECLARE(MCU__GPIO_PORT_I, MCU__GPIO_PIN_15)

#define MCU__GPIO_P_J_0         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_J_1         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_J_2         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_J_3         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_J_4         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_J_5         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_J_6         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_J_7         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_J_8         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_J_9         GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_J_10        GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_J_11        GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_J_12        GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_J_13        GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_J_14        GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_J_15        GPIO__DECLARE(MCU__GPIO_PORT_J, MCU__GPIO_PIN_15)

#define MCU__GPIO_P_K_0         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_0)
#define MCU__GPIO_P_K_1         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_1)
#define MCU__GPIO_P_K_2         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_2)
#define MCU__GPIO_P_K_3         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_3)
#define MCU__GPIO_P_K_4         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_4)
#define MCU__GPIO_P_K_5         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_5)
#define MCU__GPIO_P_K_6         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_6)
#define MCU__GPIO_P_K_7         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_7)
#define MCU__GPIO_P_K_8         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_8)
#define MCU__GPIO_P_K_9         GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_9)
#define MCU__GPIO_P_K_10        GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_10)
#define MCU__GPIO_P_K_11        GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_11)
#define MCU__GPIO_P_K_12        GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_12)
#define MCU__GPIO_P_K_13        GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_13)
#define MCU__GPIO_P_K_14        GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_14)
#define MCU__GPIO_P_K_15        GPIO__DECLARE(MCU__GPIO_PORT_K, MCU__GPIO_PIN_15)


/*******************************************************************************
 * Перечень интерфейсов МК.
 ******************************************************************************/
#define MCU__UART_1             (0 | UART__TYPE_HW)
#define MCU__UART_2             (1 | UART__TYPE_HW)
#define MCU__UART_3             (2 | UART__TYPE_HW)
#define MCU__UART_4             (3 | UART__TYPE_HW)
#define MCU__UART_5             (4 | UART__TYPE_HW)
#define MCU__UART_6             (5 | UART__TYPE_HW)
#define MCU__UART_COUNT          6

#define MCU__USB_COM_1          (0 | UART__TYPE_USB)
#define MCU__USB_COM_2          (1 | UART__TYPE_USB)
#define MCU__USB_COM_3          (2 | UART__TYPE_USB)
#define MCU__USB_COM_4          (3 | UART__TYPE_USB)
#define MCU__USB_COM_5          (4 | UART__TYPE_USB)
#define MCU__USB_COUNT           5

#define SPI_1 0
#define SPI_2 1
#define SPI_3 2
#define MCU__SPI_MAX SPI_3

#define I2C_1 0
#define I2C_2 1
#define I2C_3 2
#define MCU__I2C_MAX I2C_3

#endif /* MCU_H_ */
