#ifndef PWM_H
#define PWM_H

#include "LPC802/mcu.h"

#define PWM_MAX 5000

#define PWM0 LPC_CTIMER0->MR[0]
#define PWM1 LPC_CTIMER0->MR[1]
#define PWM2 LPC_CTIMER0->MR[2]

#define CH_0 0
#define CH_1 1
#define CH_2 2

/*******************************************************************************
 * ������� start. Freq = 12kHz. PWM count from 0 to 999.
 *                Example: pwm__start(CH_0, MCU__GPIO_PIN_17);
 *    Example for 50% PWM: PWM0 = 500;
 ******************************************************************************/
void pwm__start(int ch, int pin);
/*******************************************************************************
 * ������� stop. Example: pwm__stop(CH_0);
 ******************************************************************************/
void pwm__stop(int ch);
/*******************************************************************************
 * ������� deinit
 ******************************************************************************/
void pwm__deinit();























#endif