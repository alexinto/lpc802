/***************************************************************************//**
 * @file rtc.h.
 * @brief ������ ������� �������.
 * @authors luch1990
 *   https://www.cy-pr.com/tools/time/ - �������� ������ ��� unix �����
 ******************************************************************************/
#ifndef RTC_H_
#define RTC_H_

#include <stdint.h>
#include <time.h>
#include "sw_timer.h"
typedef enum 
{
    RTC__YMD,           // YYYY-MM-DD
    RTC__YMD_HMS,       // YYYY-MM-DD HH:MM:SS
    RTC__YMD_HMS_MS,    // YYYY-MM-DD HH:MM:SS.MS_
    RTC__DMY,           // DD-MM-YYYY
    RTC__DMY_HMS,       // DD-MM-YYYY HH:MM:SS
    RTC__DMY_HMS_MS,    // DD-MM-YYYY HH:MM:SS.MS_
    RTC__HMS,           // HH:MM:SS
    RTC__HMS_MS,        // HH:MM:SS.MS_
    RTC__YMD_UNKNOWN,   // 
} rtc__format_e;


// ������� ������ ������� �������.
typedef enum {
    RTC__EVENT_PARAM_NA     = -2,
    RTC__EVENT_ERR          = -1,
    RTC__EVENT_OK           =  0,
} rtc__event_e;

// ���� ��������������� ������.
typedef enum {
    RTC__CONVERT_TO_STRING,
    RTC__CONVERT_FROM_STRING,
} rtc__convert_e;

/***************************************************************************//**
 * @brief �������� �������-����������� ���������� ����������� ������� (���������� hw_timer-��).
 * @param ms - ���������� �����������, ������� �������� ���������� ������ (�� ������� ������������ �������� ���).
 * @return RTE_TIMER__EVENT_OK - �������� ���������� �����������.
 *         RTE_TIMER__EVENT_TIMER_TRIGGED - ������������� ������������ sw_timer-� (���������� ��� ������ � ��).
 ******************************************************************************/
typedef int32_t (*rtc_timer__event_handler_t)(rtc__event_e event);


/***************************************************************************//**
 * @brief ������������� ������� RTC �������
 * @param handler - ��������� �� ���������������� ���������� ������� (������)
 * @return RTC__EVENT_OK - ����� ��� ��� ������
 ******************************************************************************/
rtc__convert_e rtc__init(rtc_timer__event_handler_t handler);


/***************************************************************************//**
 * @brief ������� ��������� �������� ���������� ������� ����������� (timestamp).
 * @param cur_time - ��������� �� �����, ���� �������� ������� �����.
 * @param  ms - ��������� �� ����������, ���� �������� ������� ������������.
 * @return RTC__EVENT_OK - �������� ��������� �������.
 *         RTC__EVENT_ERR - ������/�������� ������� ������������.
 *         RTC__EVENT_PARAM_NA - �������� ���������.
 ******************************************************************************/
rtc__event_e rtc__local_timestamp_get(sw_timer__sys_time_t* cur_time);

/***************************************************************************//**
 * @brief ������� ��������� �������� UTC ������� ����������� (timestamp).
 * @param cur_time - ��������� �� �����, ���� �������� ������� �����.
 * @param  ms - ��������� �� ����������, ���� �������� ������� ������������.
 * @return ��������� �������� ��������� �������.
 ******************************************************************************/
rtc__event_e rtc__utc_timestamp_get(sw_timer__sys_time_t* cur_time);

/***************************************************************************//**
 * @brief ������� ��������� �������� ���������� ������� ����������� (timestamp).
 * @param cur_time - ��������� �� �����, ���� �������� ������� �����.
 * @param  ms - ��������� �� ����������, ���� �������� ������� ������������.
 * @return void
 ******************************************************************************/
void rtc__local_timestamp_get_void(sw_timer__sys_time_t* cur_time);

/***************************************************************************//**
 * @brief ������� ��������� �������� UTC ������� ����������� (timestamp).
 * @param cur_time - ��������� �� �����, ���� �������� ������� �����.
 * @param  ms - ��������� �� ����������, ���� �������� ������� ������������.
 * @return void
 ******************************************************************************/
void rtc__utc_timestamp_get_void(sw_timer__sys_time_t* cur_time);

//*******************************************************************************
/***************************************************************************//**
 * @brief ������� ��������� �������� ���������� ������� ����������� (������ "yyyy-mm-dd hh:mm:ss").
 * @param cur_time - ��������� �� ����� (�� ����� 20 ���� �����), ���� �������� ������� �����.
 * @return ��������� �������� ��������� �������.
 ******************************************************************************/
//rtc__event_e rtc__utc_string_get(char *cur_time);

/***************************************************************************//**
 * @brief ������� ��������� �������� UTC ������� ����������� (������ "yyyy-mm-dd hh:mm:ss").
 * @param cur_time - ��������� �� ����� (�� ����� 20 ���� �����), ���� �������� ������� �����.
 * @return ��������� �������� ��������� �������.
 ******************************************************************************/
//rtc__event_e rtc__local_string_get(char *cur_time);

/***************************************************************************//**
 * @brief ������� ��������������� UNIX ������� � ������ (������ �������� ������: "yyyy-mm-dd hh:mm:ss").
 * @param timestamp - ��������� ���������� UNIX �������.
 * @param buff - ��������� �� �������� ������.
 * @param len - ������ �������� ������.
 * @return RTC__EVENT_OK - �������� ���������������.
 *         RTC__EVENT_ERR - ������ ���������������.
 *         RTC__EVENT_PARAM_NA - �������� ���������.
 ******************************************************************************/
//rtc__event_e rtc__date_time_to_str(int32_t timestamp, char *buff, int len);

/***************************************************************************//**
 * @brief ������� ��������������� UNIX ������� � ������ (������ �������� ������: "yyyy-mm-dd").
 * @param timestamp - ��������� ���������� UNIX �������.
 * @param buff - ��������� �� �������� ������.
 * @param len - ������ �������� ������.
 * @return RTC__EVENT_OK - �������� ���������������.
 *         RTC__EVENT_ERR - ������ ���������������.
 *         RTC__EVENT_PARAM_NA - �������� ���������.
 ******************************************************************************/
//rtc__event_e rtc__date_to_str(int32_t timestamp, char *buff, int len);
//*******************************************************************************


/***************************************************************************//**
 * @brief ������� ��������������� ������ �� ����� � ������� UNIX (������ ������� ������: "yyyy-mm-dd hh:mm:ss").
 * @param buff - ��������� �� ������� ������.
 * @param timestamp - ��������� ���������� UNIX �������.
 * @return RTC__EVENT_OK - �������� ���������������.
 *         RTC__EVENT_ERR - ������ ���������������.
 *         RTC__EVENT_PARAM_NA - �������� ���������.
 ******************************************************************************/
//rtc__event_e rtc__str_to_date_time(char *buff, int32_t* timestamp);

/***************************************************************************//**
 * @brief ������� ��������������� ������ �� ����� � ������� UNIX (������ ������� ������: "yyyy-mm-dd").
 * @param buff - ��������� �� ������� ������.
 * @param timestamp - ��������� ���������� UNIX �������.
 * @return RTC__EVENT_OK - �������� ���������������.
 *         RTC__EVENT_ERR - ������ ���������������.
 *         RTC__EVENT_PARAM_NA - �������� ���������.
 ******************************************************************************/
//rtc__event_e rtc__str_to_date(char *buff, int32_t* timestamp);


/***************************************************************************//**
 * @brief ������� ��������������� ������ �� �����
 * @param str - ��������� �� ������� ������
 * @param format - ������ ������� ������
 * @param cur_time - �������� ��������� (�����)
 * @return - RTC__EVENT_OK
 *           RTC__EVENT_PARAM_NA
 ******************************************************************************/
rtc__event_e rtc__scanf(char *buff, rtc__format_e format, sw_timer__sys_time_t* cur_time);


/*******************************************************************************
 * @brief ������� ���������� struct tm �� UNIX time ��� ������ �������� (���������� ��� gmtime)
 * @param timestamp - ��������� ���������� UNIX �������.
 * @param time - �������� ���������
 * @return void
 ******************************************************************************/
void rtc__gmtime(int32_t timestamp, struct tm* time);
/*******************************************************************************
 * @brief ������� ��������� UNIX time �� struct tm ��� ������ �������� (���������� ��� mktime)
 * @param time - ������� ���������
 * @return UNIX time
 ******************************************************************************/
int32_t rtc__mktime(struct tm* time);

/***************************************************************************//**
 * ������� ��������������� UNIX ������� � ������ � ��������� �������
 * @param str - ��������� �� ������� ������
 * @param format ������ ������
 * @param cur_time - �����
 * @param ms - ������������.
 * @return - RTC__EVENT_OK
 *           RTC__EVENT_PARAM_NA
 ******************************************************************************/
rtc__event_e rtc__printf(char* out_str, int len, rtc__format_e format, sw_timer__sys_time_t* cur_time);

/***************************************************************************//**
 * ������� ��������������� ���������� ������� � ������ � ��������� �������
 * @param str - ��������� �� ������� ������
 * @param format ������ ������
 * @return - RTC__EVENT_OK
 *           RTC__EVENT_PARAM_NA
 ******************************************************************************/
rtc__event_e rtc__printf_now(char* out_str, int len, rtc__format_e format);

// todo: ���������, ��������� UTC � local, ��������������, ��������� ��������, ������� ���� ���� � �.�.
// dreams...
#endif /* RTC_H_ */

