#ifndef SSD1306_H
#define SSD1306_H

#define SSD1306_I2C_ADDR 0x78
#define SSD1306_WIDTH            128
#define SSD1306_HEIGHT           64
#define SSD1306_COLOR_BLACK 0x00
#define SSD1306_COLOR_WHITE 0x01

typedef enum {
    SMALL_SIZE = 0,
    BIG_SIZE,
}font_size_e;

typedef enum {
    NO_INIT = 0,
    IDLE,
    WRITE,
    CLEAR,
    SET_CURSOR,
    SET_CURSOR_X,
    WRITE_STR,
    WRITE_BIG,
} ssd1306_oper_e;

typedef enum {
    BUSY = -1,
    OK = 0,
} ssd1306_event_e;

void ssd1306_init(void);

ssd1306_event_e ssd1306_clr(void);

ssd1306_event_e ssd1306_putc(uint8_t* string, uint8_t x, uint8_t y, font_size_e font);


#endif